# Integrating NavG3 with ROS (... or other 3rd party tools)

In order to keep things relatively sane, all ROS packages are built through ROS's Catkin wrapper around CMake.  NavG3 uses cutting edge post-version 3.5 CMake packages like the cool new software project that it is.  Making these play nice turns out to be quite simple once you know how.  That's what this document describes.
  
Making ROS work with NavG3 is a two-step process.
1. Make NavG3 work (i.e., install NavG3)
1. Make your ROS plugin work (i.e., find and link against NavG3 and friends)
1. Make NavgG3 find your plugin

## This is CMake, can't I just rip off some example somwhere?

Check out the `ds_navg` package.

## Making NavG3 work

The first step is to install NavG3.  There are dependencies and stuff that should be documented elsewhere, but from this top level directly the following works:

```
mkdir build
cd build
cmake ..
make -j 10 
sudo make install
```

You can see what gets installed, but it all goes to somwhere in /usr/local.  The next step is build your shiny new ROS-based NavG plugin against these newly built/installed library.

Note that from here on out, you can start navG by simply running ```navg```

## Making your ROS-using NavG3 plugin work

Next you need to get your shiny new ROS-wielding NavG3 plugin to work.  This is a two-step process.  First, we have to make sure catkin can find stuff in the /usr/local/lib/cmake folder.  Add the following near the top of your package's CMakeLists.txt file:

```
list(INSERT CMAKE_MODULE_PATH 0 /usr/local/lib/cmake)
list(INSERT CMAKE_MODULE_PATH 0 ${PROJECT_SOURCE_DIR}/cmake)
```

Next, there are some package config files that need to get copied into your package's cmake directory.  Those are:

```
FindClangFormat.cmake
FindGEOS.cmake
FindProj.cmake
```

(author's aside: should we just change the navg CMakeLists.txt to install them directly into /usr/local/lib/cmake?)


With these done, cmake can now find navg as usual with:

```
find_package(navg REQUIRED)
```

Great! Easy!  Now all you have to do is add the navg library to your target_link_libraries the usual way:

```
target_link_libraries(navg_ros_navsat navg::navg_library)
```

Note that the `navg::navg_library` is very important.  The double colon tells CMake this is a package, and it will automatically add include directories, definitions, and whatever else is requried.  Magical, right??  Don't try to not using cmake's automatic packaging stuff.  It won't work.  Very unpleasant.

(author's aside: still need to figure out if this will Just Work (TM) with dslmap.  It should, right?)

## Making NavG3 Find your plugin

Now that you've successfully built your plugin, it's time to fire up navG and load it!  Start by making sure you've followed all the navG plugin tutorial stuff-- the name does start with a `navg_`, right?

Fortunatley, navG wants to be able to find your plugin.  In addition to all the places it normally looks, it will also look at any paths in the `NAVG_PLUGIN_PATH` environment variable.  You could make this work by simply running `export NAVG_PLUGIN_PATH=/path/to/your/workspace/devel/lib`, but that's too much to type every time.  Instead, its much better to setup your ROS package to automatically update that variable when you call setup.bash.  You can do that by adding an environment hook in your ROS package file.  Somewhere in the package's top-level CMakeLists.txt, simply add:

```
catkin_add_env_hooks(setup_navg_path SHELLS bash DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/env-hooks)
```

This will adding a shell script based on a macro in your package's "env-hooks" directory.  Here's an example of the macro that seems to work (`setup_navg_path.bash.em`):

```
export NAVG_PLUGIN_PATH=$NAVG_PLUGIN_PATH:@(CATKIN_DEVEL_PREFIX)/@(CATKIN_PACKAGE_LIB_DESTINATION)
```

Now, run a complete `catkin_make` and `source devel/setup.bash`.  `echo $NAVG_PLUGIN_PATH` should now show your workspace directory.

# ROS Package Changes Review

Just to review, the directory layout of our rospackage looks like:

```
your-ros-package
      |
      +---- CMakeLists.txt
      +---- package.xml
      +---- env-hooks
      |         +---- setup_navg_path.bash.em
      |
      +---- env-hooks
      |         +---- FindGEOS.cmake
      |         +---- FindProj.cmake
      |         +---- FindClangFormat.cmake
      |
      +---- everything else
```

The changes to the CMakeLists.txt are as follows.  Near the top:



```
list(INSERT CMAKE_MODULE_PATH 0 /usr/local/lib/cmake)
list(INSERT CMAKE_MODULE_PATH 0 ${PROJECT_SOURCE_DIR}/cmake)
find_package(navg REQUIRED)
```

Once you have a plugin target:

```
target_link_libraries(navg_your_plugin navg::navg_library)
```
