# libdslmap:  DSL Map Library

A Qt QGraphicsView-based mapping library tailored towards simple navigation UI use in software relating to the vehicles
of the Deep Submergence Labratory (DSL) at the Woods Hole Oceanographic Institution (WHOI).

## Required Dependencies

libdslmap targets the Ubuntu 18.04 LTS release and Ubuntu 20.04 LTS release.  All dependencies (both required and optional) can be installed using 
the standard ubuntu repositories.  The mapping library requires:

- C++14 support (clang 3.8, gcc 5.4 and above)
- Qt 5.5 (5.5 was the *target* release for Ubuntu 16.04. Later versions of Qt5 are supported.  It may work with earlier versions of Qt5)
- cmake (3.5 and above)
- PROJ4
- GDAL
- GEOS
- GDAL-BIN

### Optional Development Dependencies

libdslmap uses a few optional dependencies for development:

- doxygen (optional, for documentation)
- clang-format (optional, for source formatting during development)


### Installing Qt 5.x

Qt 5.5 is the Qt version associated with Ubuntu 16.04. Later versions apply for Ubuntu 18.04 and 20.04

If you need to install Qt 5.x manually, visit [Qt5.5 LTS](https://www1.qt.io/qt5-5/).  
By default, Qt will install into `/opt/Qt5.5.x`, where `x` is a minor version 
number.  You will then need to tell `CMake` where Qt is installed by adding 
`-DQt5_DIR=/opt/Qt5.5.x/5.x/gcc_64/lib/cmake/Qt5` to your `cmake` invocation.
  
### Installing CMake 3.5 or newer
CMake 3.5 is the CMake version associated with Ubuntu 16.04. Ubuntu 18.04 and Ubuntu 20.04 provide newer CMake versions and are supported.

If you need to install CMake manually, see 
[cmake.org](https://cmake.org/download/) to download a compatible version.

### Ubuntu 18.04 and 20.04 apt one-liner
There exist scripts to install required packages for your machine 
in navg3-git/ci/ named ubuntu1804-requirements.sh and ubuntu2004-requirements.sh. 
        
## Building libdslmap

Use the standard cmake build sequence to build libdslmap:

    mkdir build && cd $_
    cmake .. -DCMAKE_BUILD_TYPE=Debug
    make
    
If you want to compile a release version, omit the CMAKE_BUILD_TYPE define, but you'll lose some helpful information
in the logging output.

If Qt is installed in a non-standard location you may need to define `Qt5_DIR`.  For example,
if Qt is installed into `/opt/Qt5.5.0`:

    mkdir build && cd $_
    cmake .. -DQt5_DIR=/opt/Qt5.5.0/5.5/gcc_64/lib/cmake/Qt5
    make     

### Build Options

Testing is enabled by default.  To disable building tests, pass `-DBUILD_TESTS=OFF` when running `cmake`

## Using QtCreator

To use QtCreator with this project you will need to do the following:

- Download Qt 5.9 from [Qt5.9 LTS](https://www1.qt.io/qt5-9/).  Yes, you need 
  yet another version of Qt because 5.5 just does not have good enough cmake
  support.

The best way to do this is to clone the default kit and change what's needed.
If you need to add a non-system version of cmake, click the `CMake` tab and click the `Add` button.  
Then fill in details for where cmake is located on your machine.

Then you should be able to open the project in QtCreator using `File->Open 
File or Project` and select the `CMakeLists.txt` file in this directory.  
Use the new kit you just created when prompted to configure the project.
   
File browsing is a bit messy in Qt.  To get to the sources, look in the 
`<Source Directory>` folder for a target.  For example, `dslmap-><Source 
Directory>` will be the root directory of the `dslmap` sources.

## Additional documentation
(Doxygen does not seem to make the links correctly, check the 'Related Pages'
 tab if viewing the generated docs)

- [CHANGELOG.markdown](CHANGELOG.markdown)
- [USAGE.markdown](USAGE.markdown):  Using `libdslmap`
- [CONTRIBUTING.markdown](CONTRIBUTING.markdown):  Guidelines for making 
  contributions to `libdslmap`.
- [EXTENDING.markdown](EXTENDING.markdown):  How to extend `libdslmap`, 
  the design philosophy behind the class hierarchy.
  
