# Extending libDslMap

**Please look over [CONTRIBUTING.markdown](../CONTRIBUTING.markdown) before 
submitting any changes.**

`libdslmap` is built upon a hierarchy of components:


- `dslmap::MarkerItem`: A point feature with associated meta-data to display on 
  the map.  Any point-like geographic information should probably use this class.
  
- `dslmap::MarkerSymbol`:  A `QGraphicsPathItem`-based class that determines
  how a `dslmap::MarkerItem` (or possibly many) is displayed on the map.  A single
  `dslmap::MarkerSymbol` may be used by many `dslmap::MarkerItem`'s, but a `dslmap::MarkerItem`
  may only point to *one* `dslmap::MarkerSymbol`.
  
- `dslmap::MapLayer`: A `QGraphicsObject`-based abstract class that 
  all complex map-related objects should subclass.  Layers should group together a 
  logical collection of Map objects (target locations, vehicle trails, bathymetry contours, etc.)
  For example, this could be a collection of `dslmap::MarkerItem` objects all 
  sharing the same `dslmap::MarkerSymbol`

- `dslmap::MapScene`: A `QGraphicsScene`-based scene that owns all objects to be 
  displayed.
  
- `dslmap::MapView`:  A `QGraphicsView`-based view of the `dslmap::MapScene`
  
The library follows the traditional `QGraphicsView`/`QGraphicsScene` paring, 
where multiple views can share the same scene, allowing for multiple widgets 
to display the same underlying information in an efficient manor.

While this sounds a bit confusing (and frankly, it *is* confusing...), the 
intention of these objects is to provide a hierarchy whereby simple 
primitives can be combined into more complex pairings.

#  Marker Symbols

Map symbols are the term given to shapes used to draw point-based targets on 
the map.  This definition covers a wide range; from points of interest 
loaded from a CSV file to the current and past position of a acoustic beacon 
being tracked.  Marker Symbols represent the lowest-level and most-used 
graphical primitive on the map and deserve their own special description.

In `libdslmap`, Marker symbols are drawn using two closely coupled classes:

- `dslmap::MarkerSymbol`: Represents the symbol as drawn on the map
- `dslmap::MarkerItem`: Provides specific information about a point.

**Note**: the term 'item' in these classes is a reminder that they inherit 
from the `QGraphicsItem`, whereas the bulk of the other map 'primitives' have
 object in their name because they derive from `QGraphicsObject`.

The reason we don't have one class that contains information both on how to 
draw the symbol AND what information the symbol is supposed to represent is 
so that we can use a single `dslmap::MarkerSymbol` instance amongst *many* 
`dslmap::MarkerItem` instances.  This allows us to both limit some resource use 
and easily update how items are drawn on the map.  `dslmap::MarkerItem`'s are 
where the specifics for each item (position, metadata, label text, etc.) are 
stored.

So how does this work in practice?

If your layer will consist of many point-like targets that you want to draw 
on the map, then you'll have one `dslmap::MarkerSymbol` and *many* 
`dslmap::MarkerItem` objects.  You will control how the items are drawn on the 
map using the methods of `dslmap::MarkerSymbol`, and this single symbol 
object will be assigned to each `dslmap::MarkerItem` either using the 
constructor `dslmap::MarkerItem(QSharedPointer< MarkerSymbol > &, QGraphicsItem
 *)` or using the `dslmap::MarkerItem::setSymbol` method.
 
 
For example:

    using namespace dslmap;
    
    QSharedPointer<MarkerSymbol> symbol(new MarkerSymbol);
    // .. change how the symbol looks
    // Now add the markers (obviously you'd do whatever memory management you
    // need as well)
    auto marker0 = new MarkerItem(symbol);
    marker0->setLabelText("First marker!");
    
    auto marker1 = new MarkerItem(symbol);
    marker1->setAttribute("some_value", 32.4);
    
    auto marker2 = new MarkerItem(symbol);
    
    // Now all the markers share the same symbol.  Any change to the symbol 
    object will be refelcted in all the assocated markers.
    
**Note:** You CAN NOT add the `dslmap::MarkerSymbol` to the `MapScene`.  
`dslmap::MarkerSymbol` inherits from `QObject`, not `QGraphicsItem` nor 
`QGraphicsObject`!  This is by design.
    

# Map Layers

All map layers must use `dslmap::MapLayer` for their base class. 
Under the hood, `dslmap::MapLayer` is based on  based on`QGraphicsObject` with 
some extra layer-related logic added on top.  We use a `QGraphicsObject` 
to bring in the use of signals and slots for manipulating the layer from 
external sources (adding a point, changing a symbol color, etc.)

The map layer class will generally not have any drawn shape itself, but will 
contain child items that are drawn.  For example, the `dslmap::SymbolGroupObject` 
owns both a `dslmap::MarkerSymbol` and a bunch of `dslmap::MarkerItem` objects.
The `dslmap::MarkerItem`s are set as child items of the `dslmap::SymbolGroupObject`,
and are drawn by the `dslmap::MapView` using the associated  `dslmap::MarkerSymbol`, 
but the default behavior of  `dslmap::MapLayer::paint` is to do nothing because Qt 
automatically draws child objects.
