cmake_minimum_required(VERSION 3.5)
project(dslmap LANGUAGES CXX VERSION 1.0.0)

option(BUILD_TESTS "Build Tests" ON)
option(BUILD_DOCS "Build Documentation" ON)

list(INSERT CMAKE_MODULE_PATH 0 ${PROJECT_SOURCE_DIR}/cmake)

find_package(Qt5 5.5 REQUIRED COMPONENTS Widgets OpenGL)

find_package(GDAL REQUIRED)
find_package(GDALBIN REQUIRED)
find_package(GEOS REQUIRED)
find_package(Proj REQUIRED)
find_package(ClangFormat)
find_package(Doxygen)

if (NOT (PROJ_VERSION_MAJOR LESS 8))
  message( FATAL_ERROR "PROJ Version Major 8 and above incompatible." )
endif()

if (PROJ_VERSION_MAJOR LESS 6)
  add_library(GDAL::GDAL UNKNOWN IMPORTED)
else()
  add_compile_definitions(ACCEPT_USE_OF_DEPRECATED_PROJ_API_H)
  add_library(GDAL UNKNOWN IMPORTED)
endif()

set_target_properties(GDAL::GDAL PROPERTIES
  INTERFACE_INCLUDE_DIRS "${GDAL_INCLUDE_DIRS}"
  IMPORTED_LOCATION "${GDAL_LIBRARIES}"
  )

add_library(GEOS::GEOS UNKNOWN IMPORTED)
set_target_properties(GEOS::GEOS PROPERTIES
  INTERFACE_INCLUDE_DIRS "${GEOS_INCLUDE_DIR}"
  IMPORTED_LOCATION "${GEOS_LIBRARY}"
  )

add_library(Proj::Proj UNKNOWN IMPORTED)
set_target_properties(Proj::Proj PROPERTIES
  INTERFACE_INCLUDE_DIRS "${PROJ_INCLUDE_DIR}"
  IMPORTED_LOCATION "${PROJ_LIBRARY}"
  )

if(CLANG_FORMAT_FOUND)
  add_custom_target(dslmap-clang-format
    COMMENT "Run clang-format on all project C++ sources"
    WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
    COMMAND
      find .
      -iname '*.h' -o -iname '*.h.in' -o -iname '*.cpp'
      | xargs ${CLANG_FORMAT_EXECUTABLE} -i
    )
endif(CLANG_FORMAT_FOUND)

if(DOXYGEN_FOUND)
  find_file(QT_DOCUMENT_ROOT qtcore.tags
    PATHS /usr/share /usr/doc
    PATH_SUFFIXES qt5/doc/qtcore
    NO_DEFAULT_PATH
  )
  if(QT_DOCUMENT_ROOT)
    get_filename_component(QT_DOCUMENT_ROOT ${QT_DOCUMENT_ROOT} DIRECTORY)
    get_filename_component(QT_DOCUMENT_ROOT ${QT_DOCUMENT_ROOT} DIRECTORY)
  else(QT_DOCUMENT_ROOT)
    message("Unable to find Qt document tags, doxygen will not have links to Qt structures")
  endif(QT_DOCUMENT_ROOT)

  configure_file(Doxyfile.in ${PROJECT_BINARY_DIR}/Doxyfile @ONLY)
  add_custom_target(dslmap-doxygen
    COMMENT "Generate documentation with Doxygen"
    WORKING_DIRECTORY ${PROJECT_BINARY_DIR}
    COMMAND ${DOXYGEN_EXECUTABLE} ${PROJECT_BINARY_DIR}/Doxyfile
    COMMENT "Generating libdslmap API with doxygen"
    VERBATIM
  )

else(DOXYGEN_FOUND)
  message("Doxygen not found, skipping documents")
  set(BUILD_DOCS OFF)
endif(DOXYGEN_FOUND)

add_subdirectory(src)

if(BUILD_TESTS)
  include(CTest)
  add_subdirectory(tests)
endif(BUILD_TESTS)

