/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 7/31/18.
//

#include "WktMarkerSymbol.h"
#include "GeosUtils.h"
#include <QBrush>
#include <QPen>
#include <QSettings>

namespace dslmap {

class WktMarkerSymbolPrivate
{
public:
  QPainterPath m_path;
  QString m_wkt;
};

WktMarkerSymbol::WktMarkerSymbol(QObject* parent)
  : MarkerSymbol(parent)
  , d_ptr(new WktMarkerSymbolPrivate)
{
}

WktMarkerSymbol::WktMarkerSymbol(QString wkt, QObject* parent)
  : WktMarkerSymbol(parent)
{
  setPathFromWkt(wkt);
}

WktMarkerSymbol::~WktMarkerSymbol() = default;
bool
WktMarkerSymbol::operator==(const WktMarkerSymbol& other)
{
  if (typeid(*this) != typeid(other)) {
    return false;
  }

  return isEqual(other);
}
bool
WktMarkerSymbol::isEqual(const WktMarkerSymbol& other) const
{
  return wktString() == other.wktString() && MarkerSymbol::isEqual(other);
}
void
WktMarkerSymbol::setPathFromWkt(QString wkt)
{
  if (wkt.isEmpty()) {
    return;
  }

  const auto path = pathFromWkt(wkt);
  if (path.isEmpty()) {
    return;
  }
  Q_D(WktMarkerSymbol);
  d->m_path = path;
  d->m_wkt = std::move(wkt);
}

const QPainterPath&
WktMarkerSymbol::path() const
{
  const Q_D(WktMarkerSymbol);
  return d->m_path;
}

std::unique_ptr<WktMarkerSymbol>
WktMarkerSymbol::clone() const
{

  auto other = std::make_unique<WktMarkerSymbol>();
  other->m_clonePaintSettingsFrom(*this);
  other->setPathFromWkt(wktString());
  return other;
}

QString
WktMarkerSymbol::wktString() const
{
  const Q_D(WktMarkerSymbol);
  return d->m_wkt;
}

std::unique_ptr<WktMarkerSymbol>
WktMarkerSymbol::fromSettings(const QSettings& settings, QString prefix)
{

  auto symbol = std::make_unique<WktMarkerSymbol>();

  auto b_ptr = static_cast<MarkerSymbol*>(symbol.get());
  b_ptr->loadSettings(settings, prefix);

  symbol->setPathFromWkt(settings.value(prefix + "shape_wkt", "").toString());
  return symbol;
}

void
WktMarkerSymbol::saveSettings(QSettings& settings, QString prefix)
{
  settings.setValue(prefix + "shape_wkt", wktString());
  MarkerSymbol::saveSettings(settings, prefix);
}
}
