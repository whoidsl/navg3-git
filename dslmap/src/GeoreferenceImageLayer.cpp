/**
 * Copyright 2020 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "GeoreferenceImageLayer.h"
#include "GeneralLayerPropertiesWidget.h"
#include "Proj.h"
#include <QFileInfo>
#include <QMenu>

namespace dslmap {
Q_LOGGING_CATEGORY(dslmapGeoreferenceImageLayer, "dslmap.layer.geoimage")

class GeoreferenceImageLayerPrivate
{
public:
  QList<GeoreferenceImageObject*> m_objects;
  QRectF m_bounding_rect;
  QMenu* m_menu;
};

GeoreferenceImageLayer::GeoreferenceImageLayer(QGraphicsItem* parent)
  : MapLayer(parent)
  , d_ptr(new GeoreferenceImageLayerPrivate)
{
  Q_D(GeoreferenceImageLayer);
  setFlag(ItemHasNoContents, true);

  d->m_menu = MapLayer::contextMenu();
  auto export_action = d->m_menu->addAction(QStringLiteral("Export Image"));
  auto underlay_action = d->m_menu->addAction(QStringLiteral("Save Image Underlay in Settings"));
  connect(export_action, &QAction::triggered, this,
          [=]() { exportLayerImage(); });
  connect(underlay_action, &QAction::triggered, this,
          [=]() { saveUnderlay(); });
}

GeoreferenceImageLayer::~GeoreferenceImageLayer() = default;

GeoreferenceImageObject*
GeoreferenceImageLayer::addRasterFile(const QString& filename, int band)
{
  Q_D(GeoreferenceImageLayer);

  source_filename = filename;
  // Create an image object
  auto image = new GeoreferenceImageObject();
  if (!image->setRasterFile(filename)) {
    delete image;
    return nullptr;
  }

  std::shared_ptr<const Proj> proj;

  if (!d->m_objects.isEmpty()) {
    // if m_objects were to already have a projection...
    proj = d->m_objects.first()->projection();

  } else {
    proj = image->projection();
  }

  if (!proj) {
    delete image;
    return nullptr;
  }

  setProjection(proj);

  const auto name = QFileInfo(filename).baseName();

  image->setObjectName(name);

  // Set the layer name if this is the first raster added
  if (d->m_objects.isEmpty()) {
    setName(name);
  }

  // This takes ownership of the object
  image->setParentItem(this);
  d->m_bounding_rect |= mapRectFromItem(image, image->boundingRect());
  prepareGeometryChange();
  d->m_objects.append(image);
  return image;
}

GeoreferenceImageObject*
GeoreferenceImageLayer::addRasterFile(const QString& filename, QPointF top_left, QPointF bottom_right, int band)
{
  Q_D(GeoreferenceImageLayer);
  // Create an image object
  source_filename = filename;
  source_topleft = top_left;
  source_bottomright = bottom_right;
  auto image = new GeoreferenceImageObject();
  if (!image->setRasterFile(filename, top_left, bottom_right)) {
    delete image;
    return nullptr;
  }

  std::shared_ptr<const Proj> proj;

  if (!d->m_objects.isEmpty()) {
    // if m_objects were to already have a projection...
    proj = d->m_objects.first()->projection();

  } else {
    proj = image->projection();
  }

  if (!proj) {
    delete image;
    return nullptr;
  }

  setProjection(proj);

  const auto name = QFileInfo(filename).baseName();

  image->setObjectName(name);

  // Set the layer name if this is the first raster added
  if (d->m_objects.isEmpty()) {
    setName(name);
  }

  // This takes ownership of the object
  image->setParentItem(this);
  d->m_bounding_rect |= mapRectFromItem(image, image->boundingRect());
  prepareGeometryChange();
  d->m_objects.append(image);
  return image;
}

void
GeoreferenceImageLayer::showPropertiesDialog()
{
  auto widget = new GeoreferenceImageLayerPropertiesWidget();
  widget->setLayer(this);
  widget->show();
}

void
GeoreferenceImageLayer::setProjection(std::shared_ptr<const Proj> proj)
{
  qCDebug(dslmapGeoreferenceImageLayer) << "Setting maplayer projection FOR " << this->name();

  if (!proj) {
    return;
  }

  auto p = projectionPtr();
  Q_D(GeoreferenceImageLayer);
  if (p->isSame(*proj)) {
    qCDebug(dslmapGeoreferenceImageLayer) << "p is same as proj. returning";
    return;
  }

  d->m_bounding_rect = QRectF{};

  for (auto& image : d->m_objects) {
    image->setProjection(proj);
    d->m_bounding_rect |= mapRectFromItem(image, image->boundingRect());
  }

  prepareGeometryChange();
  MapLayer::setProjection(proj);
}

bool
GeoreferenceImageLayer::saveSettings(QSettings& settings)
{
  // Q_UNUSED(settings)
  return false;
}

bool
GeoreferenceImageLayer::loadSettings(QSettings& settings)
{
  // Q_UNUSED(settings)
  return false;
}

QList<GeoreferenceImageObject*>
GeoreferenceImageLayer::georeferenceImageObjects() const
{
  const Q_D(GeoreferenceImageLayer);
  return d->m_objects;
}

QRectF
GeoreferenceImageLayer::boundingRect() const
{
  const Q_D(GeoreferenceImageLayer);
  return d->m_bounding_rect;
}

void
GeoreferenceImageLayer::paint(QPainter*, const QStyleOptionGraphicsItem*,
                              QWidget*)
{
}

void
GeoreferenceImageLayer::exportLayerImage()
{
  Q_D(GeoreferenceImageLayer);

  qCInfo(dslmapGeoreferenceImageLayer) << "Exporting layer image";

  // get the contained image objects. in our case it's only one, but still
  // stored as a qlist of m_objects
  QImage myImg;
  for (auto obj : d->m_objects) {
    myImg = obj->getQImage();
  }
  QString imagePath =
    QFileDialog::getSaveFileName(0, tr("Save File"), QDir::homePath(),
                                 tr("Image Files (*.png *.jpg *.bmp)"));
  QImageWriter writer(imagePath);

  if (!writer.write(myImg)) {
    qDebug(dslmapGeoreferenceImageLayer) << writer.errorString();
  }
}

void
GeoreferenceImageLayer::saveUnderlay(){
  // there should only be one object anyway....
  
  if (!source_topleft.isNull() || !source_bottomright.isNull()){
    emit saveImageUnderlay(source_filename, source_topleft, source_bottomright);
  }

  else {
    emit saveGeoimageUnderlay(source_filename);
  }
}

} // dslmap
