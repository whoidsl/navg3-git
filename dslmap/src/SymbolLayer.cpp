/**
 * Copyright 2019 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// Created by zac on 10/26/17.
//

#include "SymbolLayer.h"
#include "MapScene.h"
#include "SymbolLayerPropertiesWidget.h"
#include "TargetEditDialog.h"
#include "TargetExportDialog.h"
#include <QFile>
#include <QMenu>
#include <QSettings>
#include <QTextStream>

namespace dslmap {

Q_LOGGING_CATEGORY(dslmapSymbolLayer, "dslmap.layer.symbol")

class SymbolLayerPrivate
{
public:
  explicit SymbolLayerPrivate()
    : m_symbol(std::make_shared<ShapeMarkerSymbol>())
    , m_prop(new SymbolLayerPropertiesWidget)
  {}

  ~SymbolLayerPrivate() = default;
  std::shared_ptr<ShapeMarkerSymbol> m_symbol;
  QList<MarkerItem*> m_markers;
  QRectF m_bounding_rect;
  QList<QMetaObject::Connection> m_symbol_connections;
  SymbolLayerPropertiesWidget* m_prop;
  QMenu* m_menu;
  // Maps attribute name to how many points share that attribute.
  QHash<QString, int> m_attribute_counts;
   //filename for if we use this as target underlay layer..
  QString source_filename = "";

  //assume we can use for underlays unless told otherwise for now
  bool isInUnderlayImportFormat = true;
};

SymbolLayer::SymbolLayer(QGraphicsItem* parent)
  : MapLayer(parent)
  , d_ptr(new SymbolLayerPrivate)
{
  setFlag(ItemHasNoContents, true);

  Q_D(SymbolLayer);
  d->m_menu = MapLayer::contextMenu();
  auto print_action = d->m_menu->addAction(QStringLiteral("Print Lat Lon"));
  connect(
    print_action, &QAction::triggered, this, [=]() { printLatLonLabels(); });
  auto edit_action =
    d->m_menu->addAction(QStringLiteral("Edit Targets"));
  connect(
    edit_action, &QAction::triggered, this, [=]() { editLatLonLabels(); });
  auto export_action =
    d->m_menu->addAction(QStringLiteral("Export Lat Lon to csv"));
  connect(
    export_action, &QAction::triggered, this, [=]() { exportLatLonLabels(); });
  auto underlay_action =  
    d->m_menu->addAction(QStringLiteral("Save Symbol Underlay in Settings"));
  connect(
    underlay_action, &QAction::triggered, this, [=]() { saveUnderlay(); });
  connect(this, &SymbolLayer::pointChanged, [this]() {
    this->setFileName(QString());
  });
}

SymbolLayer::~SymbolLayer()
{
  delete d_ptr;
}

QList<MarkerItem*>&
SymbolLayer::markers()
{
  Q_D(SymbolLayer);
  return d->m_markers;
}

void
SymbolLayer::showPropertiesDialog()
{
  Q_D(SymbolLayer);
  d->m_prop->setLayer(this);
  d->m_prop->show();
}

void SymbolLayer::setFileName(QString filename){
  Q_D(SymbolLayer);
  d->source_filename = filename;
}

QString SymbolLayer::getFileName(){
  Q_D(SymbolLayer);
  return d->source_filename;
}

std::shared_ptr<ShapeMarkerSymbol>
SymbolLayer::symbol() const
{
  const Q_D(SymbolLayer);
  return d->m_symbol;
}

ShapeMarkerSymbol*
SymbolLayer::symbolPtr() const
{
  const Q_D(SymbolLayer);
  return d->m_symbol.get();
}

void
SymbolLayer::setSymbol(std::shared_ptr<ShapeMarkerSymbol> symbol)
{
  if (!symbol) {
    return;
  }

  Q_D(SymbolLayer);
  for (auto& con : d->m_symbol_connections) {
    QObject::disconnect(con);
  }
  d->m_symbol_connections.clear();

  d->m_symbol = std::move(symbol);
  d->m_symbol_connections << QObject::connect(
    d->m_symbol.get(), &ShapeMarkerSymbol::symbolChanged, [this]() {
      prepareGeometryChange();
    });

  d->m_symbol_connections << QObject::connect(
    d->m_symbol.get(),
    &ShapeMarkerSymbol::outlineColorChanged,
    this,
    &SymbolLayer::outlineColorChanged);

  d->m_symbol_connections << QObject::connect(
    d->m_symbol.get(),
    &ShapeMarkerSymbol::fillColorChanged,
    this,
    &SymbolLayer::fillColorChanged);

  d->m_symbol_connections << QObject::connect(
    d->m_symbol.get(),
    &ShapeMarkerSymbol::labelColorChanged,
    this,
    &SymbolLayer::labelColorChanged);
}

ShapeMarkerSymbol::Shape
SymbolLayer::symbolShape() const
{
  const Q_D(SymbolLayer);
  return d->m_symbol->shape();
}

qreal
SymbolLayer::symbolSize() const
{
  const Q_D(SymbolLayer);
  return d->m_symbol->size();
}

QPainterPath
SymbolLayer::symbolPath() const
{
  const Q_D(SymbolLayer);
  return d->m_symbol->path();
}

MarkerItem*
SymbolLayer::addPoint(const QPointF& position,
                      const QString& label,
                      const MarkerItem::Attributes& attributes)
{
  return addPoint(position.x(), position.y(), label, attributes);
}

MarkerItem*
SymbolLayer::addPoint(qreal x,
                      qreal y,
                      const QString& label,
                      const MarkerItem::Attributes& attributes)
{
  Q_D(SymbolLayer);
  // No new_marker set for the layer yet?
  if (!d->m_symbol) {
    return nullptr;
  }

  const auto item_pos = mapFromScene(x, y);

  auto marker = new MarkerItem(d->m_symbol);

  marker->setParentItem(this);
  marker->setPos(item_pos);

  if (!label.isEmpty()) {
    marker->setLabelText(label);
  }

  // Update the list of attributes to either initialize a new one or increment
  // the counts of an existing one.
  if (!attributes.isEmpty()) {
    for (const QString key : attributes.keys()) {
      d->m_attribute_counts[key] += 1;
    }
    // Ensure attribute is initialized so the behavior matches what is displayed
    // in the widget's drop-down menu.
    if (d->m_symbol->gradientAttribute().isNull()) {
      d->m_symbol->setGradientAttribute(attributes.firstKey());
    }

    marker->setAttribute(attributes);
  }

  marker->setFlag(ItemIgnoresTransformations, isScaleInvariant());
  marker->setVisible(isVisible());
  d->m_markers.append(marker);
  d->m_bounding_rect |= mapRectFromItem(marker, marker->boundingRect());
  prepareGeometryChange();
  emit pointChanged();
  return marker;
}

bool
SymbolLayer::changeLonLatPoint(MarkerItem* marker, const QString& label)
{
  Q_D(SymbolLayer);
  if (marker == nullptr) {
    return false;
  }
  if (!d->m_markers.contains(marker)) {
    return false;
  }
  marker->setLabelText(label);
  //printLatLonLabels();
  prepareGeometryChange();
  emit pointChanged();

  return true;
}

MarkerItem*
SymbolLayer::addLonLatPoint(const QPointF& lonlat,
                            const QString& label,
                            const MarkerItem::Attributes& attributes)
{
  return addLonLatPoint(lonlat.x(), lonlat.y(), label, attributes);
}
MarkerItem*
SymbolLayer::addLonLatPoint(qreal lon,
                            qreal lat,
                            const QString& label,
                            const MarkerItem::Attributes& attributes)
{
  auto proj = projectionPtr();
  if (!proj) {
    return nullptr;
  }

  ProjWGS84::getInstance().transformTo(*proj, 1, &lon, &lat);
  return addPoint(lon, lat, label, attributes);
}

bool
SymbolLayer::removePoint(MarkerItem* marker)
{
  Q_D(SymbolLayer);
  if (marker == nullptr) {
    return false;
  }

  if (!d->m_markers.contains(marker)) {
    return false;
  }

  auto scene = marker->scene();
  if (scene != nullptr) {
    scene->removeItem(marker);
  }

  marker->setParentItem(nullptr);

  d->m_markers.removeAll(marker);

  // When removing a point, decrement the layer's corresponding attribute count
  // and remove any attributes that no longer have associated points.
  QList<QString> keys_to_be_removed;
  for (const QString key : marker->attributes().keys()) {
    if (!d->m_attribute_counts.contains(key)) {
      continue;
    }
    auto value = d->m_attribute_counts[key];
    value -= 1;
    if (value <= 0) {
      keys_to_be_removed.append(key);
    }
  }
  for (const QString key : keys_to_be_removed) {
    d->m_attribute_counts.remove(key);
  }
  if (!d->m_attribute_counts.contains(d->m_symbol->gradientAttribute())) {
    d->m_symbol->setGradientAttribute(QString());
  }

  d->m_bounding_rect = QRectF{};
  prepareGeometryChange();
  for (const auto& i : d->m_markers) {
    d->m_bounding_rect |= mapRectFromItem(i, i->boundingRect());
  }
  emit pointChanged();
  return true;
}

size_t
SymbolLayer::count() const
{
  const Q_D(SymbolLayer);
  return static_cast<size_t>(d->m_markers.count());
}

Unit
SymbolLayer::units() const
{
  const Q_D(SymbolLayer);
  return d->m_symbol->units();
}
qreal
SymbolLayer::minimumDrawSize() const
{
  const Q_D(SymbolLayer);
  return d->m_symbol->minimumSize();
}
void
SymbolLayer::setScaleInvariant(bool invariant)
{
  Q_D(SymbolLayer);
  if (d->m_symbol->isScaleInvariant() == invariant) {
    return;
  }
  d->m_symbol->setScaleInvariant(invariant);

  d->m_bounding_rect = QRectF{};
  for (auto& m : d->m_markers) {
    d->m_bounding_rect |= mapRectFromItem(m, m->boundingRect());
  }
  prepareGeometryChange();
}
bool
SymbolLayer::isScaleInvariant() const
{
  const Q_D(SymbolLayer);
  return d->m_symbol->isScaleInvariant();
}

void
SymbolLayer::setLabelVisible(bool visible)
{
  Q_D(SymbolLayer);
  d->m_symbol->setLabelVisible(visible);
}

bool
SymbolLayer::isLabelVisible() const
{
  const Q_D(SymbolLayer);
  return d->m_symbol->isLabelVisible();
}

void
SymbolLayer::setShape(ShapeMarkerSymbol::Shape shape, qreal size)
{

  Q_D(SymbolLayer);
  d->m_symbol->setShape(shape, size);
}

void
SymbolLayer::setUnits(Unit unit)
{
  if (unit == units()) {
    return;
  }

  Q_D(SymbolLayer);
  d->m_symbol->setUnits(unit);
  prepareGeometryChange();
}
void
SymbolLayer::setMinimumDrawSize(qreal pixels)
{
  Q_D(SymbolLayer);
  d->m_symbol->setMinimumSize(pixels);
}

void
SymbolLayer::setProjection(std::shared_ptr<const Proj> proj)
{
  if (!proj) {
    return;
  }

  auto currentProj = projectionPtr();
  if (currentProj->isSame(*proj)) {
    return;
  }

  Q_D(SymbolLayer);
  d->m_bounding_rect = QRectF{};
  for (auto& i : d->m_markers) {
    auto pos = i->scenePos();
    auto x = pos.x();
    auto y = pos.y();

    currentProj->transformTo(*proj, 1, &x, &y);

    pos = mapFromScene(x, y);
    i->setPos(pos);
    d->m_bounding_rect |= mapRectFromItem(i, i->boundingRect());
  }
  prepareGeometryChange();

  MapLayer::setProjection(proj);
}

bool
SymbolLayer::saveSettings(QSettings& settings)
{
  settings.setValue("name", name());
  Q_D(SymbolLayer);
  if (d->m_symbol) {
    d->m_symbol->saveSettings(settings, "symbol/");
  }

  return true;
}

std::unique_ptr<SymbolLayer>
SymbolLayer::fromSettings(QSettings& settings)
{

  auto layer = std::make_unique<SymbolLayer>();
  layer->loadSettings(settings);
  return layer;
}

QRectF
SymbolLayer::boundingRect() const
{
  const Q_D(SymbolLayer);
  return d->m_bounding_rect;
}
void
SymbolLayer::paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*)
{}

bool
SymbolLayer::loadSettings(QSettings& settings)
{
  setName(settings.value("name", "").toString());
  auto path = QPainterPath{};
  setSymbol(ShapeMarkerSymbol::fromSettings(settings, "symbol/"));
  return true;
}

std::shared_ptr<GdalColorGradient>
SymbolLayer::gradient()
{
  const Q_D(SymbolLayer);
  return d->m_symbol->gradient();
}

QList<QString>
SymbolLayer::attributeKeys() const
{
  const Q_D(SymbolLayer);
  return d->m_attribute_counts.keys();
}

QColor
SymbolLayer::outlineColor() const
{
  const Q_D(SymbolLayer);
  if (d->m_symbol) {
    return d->m_symbol->outlineColor();
  }
  return {};
}
QColor
SymbolLayer::labelColor() const
{
  const Q_D(SymbolLayer);
  if (d->m_symbol) {
    return d->m_symbol->labelColor();
  }
  return {};
}
qreal
SymbolLayer::labelSize() const
{
  const Q_D(SymbolLayer);
  if (d->m_symbol) {
    return d->m_symbol->labelSize();
  }
  return 12.0;
}
QColor
SymbolLayer::fillColor() const
{
  const Q_D(SymbolLayer);
  if (d->m_symbol) {
    return d->m_symbol->fillColor();
  }
  return {};
}

void
SymbolLayer::setOutlineColor(const QColor& color)
{
  Q_D(SymbolLayer);
  if (d->m_symbol) {
    d->m_symbol->setOutlineColor(color);
  }
}
void
SymbolLayer::setLabelColor(const QColor& color)
{
  Q_D(SymbolLayer);
  if (d->m_symbol) {
    d->m_symbol->setLabelColor(color);
  }
}
void
SymbolLayer::setLabelSize(const qreal points)
{
  Q_D(SymbolLayer);
  if (d->m_symbol) {
    d->m_symbol->setLabelSize(points);
  }
}
void
SymbolLayer::setFillColor(const QColor& color)
{
  Q_D(SymbolLayer);
  if (d->m_symbol) {
    d->m_symbol->setFillColor(color);
  }
}

bool
SymbolLayer::outlineVisible() const
{
  const Q_D(SymbolLayer);
  if (d->m_symbol) {
    return (d->m_symbol->outlineStyle() != Qt::PenStyle::NoPen);
  }
  return false;
}

bool
SymbolLayer::fillVisible() const
{
  const Q_D(SymbolLayer);
  if (d->m_symbol) {
    return (d->m_symbol->fillStyle() != Qt::BrushStyle::NoBrush);
  }
  return false;
}

void
SymbolLayer::setOutlineVisible(const bool visible)
{
  Q_D(SymbolLayer);
  if (d->m_symbol) {
    if (visible) {
      d->m_symbol->setOutlineStyle(Qt::PenStyle::SolidLine);
    } else {
      d->m_symbol->setOutlineStyle(Qt::PenStyle::NoPen);
    }
  }
}

void
SymbolLayer::setFillVisible(const bool visible)
{
  Q_D(SymbolLayer);
  if (d->m_symbol) {
    if (visible) {
      d->m_symbol->setFillStyle(Qt::BrushStyle::SolidPattern);
    } else {
      d->m_symbol->setFillStyle(Qt::BrushStyle::NoBrush);
    }
  }
}

QMenu*
SymbolLayer::contextMenu()
{
  Q_D(SymbolLayer);
  return d->m_menu;
}

void
SymbolLayer::printLatLonLabels()
{
  Q_D(SymbolLayer);
  auto proj = projectionPtr();
  if (!proj) {
    return;
  }
  qDebug() << "LAT | LON | LABEL     ";
  for (auto marker : d->m_markers) {
    qreal lat = marker->pos().y();
    qreal lon = marker->pos().x();
    proj->transformToLonLat(1, &lon, &lat);
    QString label = marker->label().text();
    qDebug() << lat << lon << label;
  }
}

void
SymbolLayer::editLatLonLabels()
{
  auto dlog = new TargetEditDialog();
  Q_D(SymbolLayer);
  auto proj = projectionPtr();
  if (!proj) {
    return;
  }
  QVector<QVector<QString>> tableData;
  QVector<QString> headers = { "Target #", "Lat", "Lon", "Label" };
  tableData.push_back(headers);
  int row_number = 0;
  for (auto marker : d->m_markers) {
    row_number++;
    qreal lat = marker->pos().y();
    qreal lon = marker->pos().x();
    proj->transformToLonLat(1, &lon, &lat);
    QString label = marker->label().text();
    QVector<QString> row{ QString::number(row_number),
                          QString::number(lat, 'f', 10),
                          QString::number(lon, 'f', 10),
                          label };
    tableData.push_back(row);
  }
  dlog->setTableData(tableData);
  if (!dlog->exec()) {
    qDebug() << "Target Edit Cancelled!";
    return;
  }

  QVector<QVector<QString>> newTableData;
  QVector<QString> hdrs = { "#", "Lat", "Lon", "Label" };
  newTableData.push_back(hdrs);
  int rowNum = 0;
  QString tgtNum(dlog->targetNum());
  QString latLonLabel(dlog->targetLabel());
  QString deleteTgtNum(dlog->tgtDeleteNum());

  std::string targetnum = tgtNum.toStdString();
  std::string targetlbl = latLonLabel.toStdString();

  for (auto marker : d->m_markers) {
    rowNum++;
    qreal lat = marker->pos().y();
    qreal lon = marker->pos().x();
    proj->transformToLonLat(1, &lon, &lat);
    QString label = marker->label().text();

    // Handle target deletes
    if (rowNum == deleteTgtNum.toInt()) {
      qDebug() <<"Got a target delete for # "<<rowNum;
      auto success = removePoint(marker);
      if (!success) {
        qDebug() <<"Failed to delete target # "<<rowNum;
      }
      dlog->accept();
      return;
    }
    
    //Handle target change requests
    if (rowNum == tgtNum.toInt()) {
      qDebug()<<"Got a tgt label change request for tgt # "<<tgtNum;
      if (latLonLabel==nullptr) {
        qDebug()<<"Lbl change request with no label, returning";
        return;
      }
      label = latLonLabel;
      QVector<QString> row{ QString::number(rowNum),
                          QString::number(lat, 'f', 10),
                          QString::number(lon, 'f', 10),
                          latLonLabel };
      newTableData.push_back(row);
      qDebug()<<"updated target info is: "<<row;
      auto success = changeLonLatPoint(marker, latLonLabel);
      if (!success) {
        qDebug()<<"Failed to change target: "<<latLonLabel;
      }
    }
    else {
    QVector<QString> row{ QString::number(rowNum),
                          QString::number(lat, 'f', 10),
                          QString::number(lon, 'f', 10),
                          label };
      newTableData.push_back(row);
    }
  }
  dlog->setTableData(newTableData);
  //printLatLonLabels();  
}

QVector<QVector<QString>>
SymbolLayer::getLatLonLabels()
{
  Q_D(SymbolLayer);
  QVector<QVector<QString>> tableData;
  QVector<QString> headers = { "#", "Lat", "Lon", "Label" };
  tableData.push_back(headers);
  auto proj = projectionPtr();
  if (!proj) {
    return tableData;
  }

  int row_number = 0;
  for (auto marker : d->m_markers) {
    row_number++;
    qreal lat = marker->pos().y();
    qreal lon = marker->pos().x();
    proj->transformToLonLat(1, &lon, &lat);
    QString label = marker->label().text();
    QVector<QString> row{ QString::number(row_number),
                          QString::number(lat, 'f', 10),
                          QString::number(lon, 'f', 10),
                          label };
    tableData.push_back(row);
  }
  return tableData;
}

void
SymbolLayer::exportLatLonLabels()
{
  auto dlog = new TargetExportDialog();
  Q_D(SymbolLayer);
  auto proj = projectionPtr();
  if (!proj) {
    return;
  }
  QVector<QVector<QString>> tableData;
  QVector<QString> headers = { "#", "Lat", "Lon", "Label" };
  tableData.push_back(headers);
  int row_number = 0;
  for (auto marker : d->m_markers) {
    row_number++;
    qreal lat = marker->pos().y();
    qreal lon = marker->pos().x();
    proj->transformToLonLat(1, &lon, &lat);
    QString label = marker->label().text();
    QVector<QString> row{ QString::number(row_number),
                          QString::number(lat, 'f', 10),
                          QString::number(lon, 'f', 10),
                          label };
    tableData.push_back(row);
  }
  dlog->setTableData(tableData);
  if (!dlog->exec()) {
    qDebug() << "Export cancelled!";
    return;
  }

  QFile file(dlog->filename());
  QString delimStr(dlog->delim());

  if (file.open(QFile::WriteOnly | QFile::Truncate)) {
    QTextStream stream(&file);
    for (auto row : tableData) {
      stream << row[0] << delimStr << row[1] << delimStr << row[2] << delimStr
             << row[3] << "\n";
    }
    file.close();
    qDebug() << "Exporting targets to..." << dlog->filename();
    setFileName(dlog->filename());
    setIsUnderlayFormat(true);

  } else {
    qWarning() << "Failed to open/write to..." << dlog->filename();
  }
}

void
SymbolLayer::rescaleGradient()
{
  Q_D(SymbolLayer);
  bool limits_changed = false;
  float min_val = std::numeric_limits<qreal>::max();
  float max_val = std::numeric_limits<qreal>::min();
  auto layer_attribute = d->m_symbol->gradientAttribute();
  foreach (auto item, d->m_markers) {
    auto item_attributes = item->attributes();
    if (item_attributes.contains(layer_attribute)) {
      bool ok;
      float value = item_attributes[layer_attribute].toFloat(&ok);
      if (ok) {
        if (value > max_val) {
          limits_changed = true;
          max_val = value;
        } else if (value < min_val) {
          limits_changed = true;
          min_val = value;
        }
      }
    }
  }
  if (limits_changed) {
    d->m_symbol->setGradientRange(min_val, max_val);
  }
}

void
SymbolLayer::saveUnderlay()
{
  Q_D(SymbolLayer);
  if (getFileName().isEmpty() || !d->isInUnderlayImportFormat) {
    qDebug()
      << "Filename is empty for target layer in actual symbol layer menu call";
    // prompt to use the export dialog and then save the filename proper
    exportLatLonLabels();
  }
  if (getFileName().isEmpty()) {
    qDebug() << "target filename is still empty after exporting. ***";
  }
  qDebug() << "Emitting savetarget underlay with filename of " << getFileName();

  emit saveTargetUnderlay(getFileName());
}

void
SymbolLayer::setIsUnderlayFormat(bool state)
{
  Q_D(SymbolLayer);
  d->isInUnderlayImportFormat = state;
}

} // namespace
