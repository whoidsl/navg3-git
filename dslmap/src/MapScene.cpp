/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 8/7/17.
//

#include "MapScene.h"
#include "MapLayer.h"
#include "Util.h"

#include "MapScenePrivate.h"

namespace dslmap {

Q_LOGGING_CATEGORY(dslmapMapScene, "dslmap.mapscene")

MapScene::MapScene(QObject* parent)
  : QGraphicsScene(parent)
  , d_ptr(new MapScenePrivate(this))
{
  connect(this, &MapScene::changed, this, &MapScene::extendSceneRect);
}

MapScene::MapScene(MapScenePrivate& pimpl, QObject* parent)
  : QGraphicsScene(parent)
  , d_ptr(&pimpl)
{
  connect(this, &MapScene::changed, this, &MapScene::extendSceneRect);
}

MapScene::~MapScene() = default;

MapLayer*
MapScene::removeLayer(const QUuid& uuid)
{

  // Nothing to do for null UUID's
  if (uuid.isNull()) {
    return nullptr;
  }

  Q_D(MapScene);
  const auto layer = d->m_layers_hash.take(uuid);

  // No matching layer?  Nothing further to do.
  if (layer == nullptr) {
    return nullptr;
  }

  // Otherwise we need to remove it from the scene before passing it back to the
  // caller.
  auto item = layer->toGraphicsObject();

  removeItem(item);
  emit layerRemoved(uuid);
  emit layerRemoved(layer);

  return layer;
}

void
MapScene::removeLayer(MapLayer* layer)
{

  // Nothing to do for null pointers
  if (layer == nullptr) {
    return;
  }

  // uuid starts as NULL
  auto uuid = QUuid{};

  // Need to find the layer first.  It's a bit backwards, but it's not too bad a
  // price to pay if we consider having UUID's as the layer keys.

  Q_D(MapScene);

  // Search through our layer
  for (auto i = std::begin(d->m_layers_hash); i != std::end(d->m_layers_hash);
       ++i) {
    if (i.value() == layer) {
      uuid = i.key();
      break;
    }
  }

  removeLayer(uuid);
}

QList<MapLayer*>
MapScene::removeLayer(const QString& name)
{

  auto uuids = QVector<QUuid>{};

  Q_D(MapScene);
  foreach (const auto& layer, d->m_layers_hash) {

    if (layer->name() == name) {
      uuids.append(layer->id());
    }
  }

  auto removed_layers = QList<MapLayer*>{};
  for (const auto uuid : uuids) {
    auto layer = removeLayer(uuid);
    if (layer != nullptr) {
      removed_layers.append(layer);
    }
  }

  return removed_layers;
}

QList<MapLayer*>
MapScene::removeAllLayers()
{
  Q_D(MapScene);
  const auto uuids = d->m_layers_hash.keys();

  auto removed_layers = QList<MapLayer*>{};
  removed_layers.reserve(uuids.count());

  for (const auto uuid : uuids) {
    auto layer = removeLayer(uuid);
    if (layer != nullptr) {
      removed_layers.append(layer);
    }
  }

  return removed_layers;
}

QUuid
MapScene::addLayer(MapLayer* layer)
{

  if (!layer) {
    qDebug() << "nullptr";
    return {};
  }

  qCInfo(dslmapMapScene) << "Adding map layer:" << layer->name()
                         << "(uuid:" << layer->id() << ")";
  const auto uuid = layer->id();
  if (uuid.isNull()) {
    qCCritical(dslmapMapScene) << "Invalid layer UUID";
    return {};
  }

  Q_D(MapScene);
  // Already have a valid projection?  Just add the layer;
  auto proj = projection();
  if (proj) {
    qCDebug(dslmapMapScene) << "Using map scene projection '"
                            << proj->definition() << "' to reproject map layer";
    layer->setProjection(proj);
    addItem(layer);

    d->m_layers_hash.insert(uuid, layer);

    emit layerAdded(uuid);
    emit layerAdded(layer);

    return uuid;
  }

  qCInfo(dslmapMapScene)
    << "No projection set for map scene.  Creating one using UTM";
  // No projection set yet for the scene.  Need to use either the layer's
  // projection, or pick a UTM projection that makes sense for the layer
  // objects.
  proj = layer->projection();
  if (!proj || proj->isLatLon()) {
    const auto bbox = layer->boundingRect();
    if (bbox.isNull()) {
      qCWarning(dslmapMapScene)
        << "Cannot add empty layer to a MapScene that has no projection";
      return {};
    }
    const auto center = bbox.center();
    // Make the proj4 projection from the
    proj = Proj::alvinXYFromOrigin(center.x(), center.y());

    // Invalid utm projection?  Give up.
    if (!proj) {
      qCWarning(dslmapMapScene)
        << "Unable to create Mercator projection centered"
        << "around lat:" << center.y() << "lon:" << center.x();

      auto err = Proj::lastError();
      qCWarning(dslmapMapScene) << "Proj4 error:" << err.first << err.second;
      return {};
    }

    qCInfo(dslmapMapScene) << "Mean lon/lat for layer " << layer->name() << "("
                           << center.x() << "," << center.y() << ")";
    qCInfo(dslmapMapScene) << "Using projection def:" << proj->definition();
    layer->setProjection(proj);
  }

  d->m_proj = proj;
  addItem(layer);

  d->m_layers_hash.insert(uuid, layer);

  emit layerAdded(uuid);
  emit layerAdded(layer);

  return uuid;
}

MapLayer*
MapScene::layer(const QUuid& uuid) const
{
  const Q_D(MapScene);
  return d->m_layers_hash.value(uuid);
}

MapScene::LayerHash
MapScene::layers() const
{

  const Q_D(MapScene);
  return d->m_layers_hash;
}

MapScene::LayerHash
MapScene::layers(MapItemType type) const
{

  auto matching_layers = MapScene::LayerHash{};

  const Q_D(MapScene);
  for (const auto layer : d->m_layers_hash) {
    if (layer->type() == type) {
      matching_layers.insert(layer->id(), layer);
    }
  }

  return matching_layers;
}

MapScene::LayerHash
MapScene::layers(const QString& name) const
{
  auto matching_layers = MapScene::LayerHash{};

  const Q_D(MapScene);
  for (const auto layer : d->m_layers_hash) {
    if (layer->name() == name) {
      matching_layers.insert(layer->name(), layer);
    }
  }

  return matching_layers;
}

QList<QUuid>
MapScene::uuids(const QString& name) const
{

  auto uuids = QList<QUuid>{};

  const Q_D(MapScene);
  for (const auto layer : d->m_layers_hash) {
    if (layer->name() == name) {
      uuids.append(layer->id());
    }
  }

  return uuids;
}

QString
MapScene::projectionString() const
{
  const Q_D(MapScene);
  if (!d->m_proj) {
    return {};
  }

  return d->m_proj->definition();
}

bool
MapScene::setProjection(std::shared_ptr<const Proj> projection)
{

  if (!projection) {
    qCWarning(dslmapMapScene) << "Ignoring null pointer for projection.";
    return false;
  }

  if (projection->isLatLon()) {
    qCWarning(dslmapMapScene) << "Ignoring definition:"
                              << projection->definition();
    qCWarning(dslmapMapScene) << "Geographic projections are not allowed.";
    return false;
  }

  Q_D(MapScene);

  // Swap in the new projection.
  d->m_proj.swap(projection);

  // Change all of the layers
  for (auto i = std::begin(d->m_layers_hash); i != std::end(d->m_layers_hash);
       ++i) {
    i.value()->setProjection(d->m_proj);
  }

  setSceneRect(itemsBoundingRect());
  emit projectionChanged(d->m_proj->definition());
  return true;
}

bool
MapScene::setProjection(const QString& definition)
{

  auto new_projection = Proj::fromDefinition(definition);

  // Failed to create a new projection
  if (!new_projection) {
    auto err = Proj::lastError();

    qCWarning(dslmapMapScene) << "Unable to set projection:" << definition;
    qCWarning(dslmapMapScene) << "proj4 error number:" << err.first
                              << err.second;
    return false;
  }

  return setProjection(new_projection);
}

std::shared_ptr<const Proj>
MapScene::projection()
{
  Q_D(MapScene);
  return d->m_proj;
}

const Proj*
MapScene::projectionPtr()
{
  Q_D(MapScene);
  return d->m_proj.get();
}

Unit
MapScene::projectionUnit() const
{
  const Q_D(MapScene);
  if (!d->m_proj) {
    return Unit::invalid;
  }
  return d->m_proj->units();
}

bool
MapScene::setGradient(std::shared_ptr<GdalColorGradient> gradient)
{
  Q_D(MapScene);
  d->m_gradient = gradient;
  emit gradientChanged();
  return true;
}

std::shared_ptr<GdalColorGradient>
MapScene::gradient() const
{
  const Q_D(MapScene);
  return d->m_gradient;
}

void
MapScene::extendSceneRect()
{
  auto items_rect = itemsBoundingRect();
  auto extended_rect = extendRectByNPercent(items_rect, 10.0);
  setSceneRect(extended_rect);
}

} // namespace
