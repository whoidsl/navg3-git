/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 7/31/18.
//

#include "MarkerSymbol.h"
#include "MapScene.h"
#include "MarkerItem.h"
#include "Proj.h"

#include <QMetaEnum>
#include <QPainter>
#include <QPointer>
#include <QSettings>
#include <QStyleOptionGraphicsItem>

namespace dslmap {

Q_LOGGING_CATEGORY(dslmapMarkerSymbol, "dslmap.markersymbol")

class MarkerSymbolPrivate
{
public:
  Qt::Alignment m_label_alignment = Qt::AlignLeft | Qt::AlignBottom;
  QPen m_pen;     // Draws the outline
  QBrush m_brush; // Draws the fill

  QPen m_label_pen; // Draws the label
  qreal m_label_size = 12;
  bool m_label_visible = true;
  qreal m_minimum_size = -1;
  QPointer<MapScene> m_scene;
  QMetaObject::Connection m_scene_projection_connection;
  qreal m_scene_scale_factor_to_meters = 1;
  qreal m_unit_scale_factor_to_meters = 1;
  Unit m_units = Unit::m;
  bool m_is_scale_invariant = true;
  QList<MarkerItem*> m_items;

  //
  // Variables used for controlling the color of items.
  //

  // Default to the old behavior, which was solid colors
  bool m_use_color_gradient = false;

  QColor m_solid_fill_color =
    QColor(Qt::white); // used by m_brush for the Solid mode
  QColor m_solid_outline_color =
    QColor(Qt::white); // used by m_pen for the Solid mode

  std::shared_ptr<GdalColorGradient> m_gdal_color_gradient;
  // Which attribute apply the color gradient based on. If it doesn't exist
  // (or can't be converted to a double), pen will default to colors defined
  // in Solid mode.
  QString m_gradient_attribute;
};

MarkerSymbol::MarkerSymbol(QObject* parent)
  : QObject(parent)
  , d_ptr(new MarkerSymbolPrivate)
{
  Q_D(MarkerSymbol);
  d->m_pen.setColor(Qt::white);
  d->m_pen.setCosmetic(true);
  d->m_pen.setStyle(Qt::SolidLine);

  d->m_label_pen = d->m_pen;
  d->m_brush.setStyle(Qt::NoBrush);

  d->m_gdal_color_gradient = std::make_shared<GdalColorGradient>();
  // NOTE(LEL): I would have liked to also disable the absChk checkbox in the
  //  GdalColorGradientPropertiesWidget, since the MarkerSymbol doesn't support
  //  relative coloring, but I didn't see a clean way to do that from the
  //  SymbolLayerPropertiesWidget.
  d->m_gdal_color_gradient->setAbsolute(true);
  d->m_gdal_color_gradient->setNoDataVisible(false);
  d->m_gdal_color_gradient->setGradient(
    GdalColorGradient::GradientPreset::Cool);

  // Connect individual property notification signals to the generic signal.
  connect(this, &MarkerSymbol::labelVisibleChanged, this,
          &MarkerSymbol::symbolChanged);
  connect(this, &MarkerSymbol::minimumSizeChanged, this,
          &MarkerSymbol::symbolChanged);
  connect(this, &MarkerSymbol::labelColorChanged, this,
          &MarkerSymbol::symbolChanged);
  connect(this, &MarkerSymbol::labelSizeChanged, this,
          &MarkerSymbol::symbolChanged);
  connect(this, &MarkerSymbol::outlineColorChanged, this,
          &MarkerSymbol::symbolChanged);
  connect(this, &MarkerSymbol::fillColorChanged, this,
          &MarkerSymbol::symbolChanged);
  connect(this, &MarkerSymbol::scaleInvariantChanged, this,
          &MarkerSymbol::symbolChanged);
  connect(this, &MarkerSymbol::useColorGradientChanged, this,
          &MarkerSymbol::symbolChanged);
  connect(this, &MarkerSymbol::gradientAttributeChanged, this,
          &MarkerSymbol::symbolChanged);
  // All changes to the gradient, whether through the GradientPropertiesWidget
  // or the accessors provided in this class cause this signal to be emitted.
  connect(d->m_gdal_color_gradient.get(),
          &GdalColorGradient::gdalColorTableChanged, this,
          &MarkerSymbol::symbolChanged);
  // Emitted in addition to GdalColorGradient::gdalColorTableChanged when the
  // source of the change was a direct call to setGradient{Preset, Min, Max}
  // rather than interaction through the GdalColorGradientPropertiesWidget.
  connect(this, &MarkerSymbol::gradientPresetChanged, this,
          &MarkerSymbol::symbolChanged);
  connect(this, &MarkerSymbol::gradientMinChanged, this,
          &MarkerSymbol::symbolChanged);
  connect(this, &MarkerSymbol::gradientMaxChanged, this,
          &MarkerSymbol::symbolChanged);
  connect(this, &MarkerSymbol::gradientRangeChanged, this,
          &MarkerSymbol::symbolChanged);
}

MarkerSymbol::~MarkerSymbol() = default;

bool
MarkerSymbol::operator==(const MarkerSymbol& other) const
{
  return isEqual(other);
}

bool
MarkerSymbol::operator!=(const MarkerSymbol& other) const
{
  return !isEqual(other);
}

bool
MarkerSymbol::isEqual(const MarkerSymbol& other) const
{
  return path() == other.path() &&
         // Can't directly test pen equality because pen color can change based
         // on item attributes. So, testing the provided controls on pens.
         outlineColor() == other.outlineColor() &&
         outlineStyle() == other.outlineStyle() &&
         outlineWidth() == other.outlineWidth() &&
         fillColor() == other.fillColor() && fillStyle() == other.fillStyle() &&
         labelColor() == other.labelColor() && labelPen() == other.labelPen() &&
         labelSize() == other.labelSize() &&
         labelAlignment() == other.labelAlignment() &&
         isLabelVisible() == other.isLabelVisible() &&
         minimumSize() == other.minimumSize() &&
         isScaleInvariant() == other.isScaleInvariant() &&
         units() == other.units() &&
         useColorGradient() == other.useColorGradient() &&
         gradientAttribute() == other.gradientAttribute() &&
         *gradient() == *other.gradient();
}

void
MarkerSymbol::updateColors(MarkerItem* item)
{
  Q_D(MarkerSymbol);

  if (useColorGradient()) {
    QVariantMap attributes = item->attributes();
    // If this point doesn't have the attribute, use noData params
    if (!attributes.contains(d->m_gradient_attribute)) {
      d->m_pen.setColor(d->m_gdal_color_gradient->noDataDisplayColor());
      d->m_brush.setColor(d->m_gdal_color_gradient->noDataDisplayColor());
      return;
    }
    auto it = attributes.find(d->m_gradient_attribute);
    QVariant variant = it.value();
    bool ok;
    // If attribute can't be mapped to a number, use noData params
    qreal value = variant.toReal(&ok);
    if (!ok) {
      d->m_pen.setColor(d->m_gdal_color_gradient->noDataDisplayColor());
      d->m_brush.setColor(d->m_gdal_color_gradient->noDataDisplayColor());
      return;
    }
    QColor color = d->m_gdal_color_gradient->colorAt(value);
    d->m_pen.setColor(color);
    d->m_brush.setColor(color);
  } else { // !useColorGradient()
    d->m_pen.setColor(d->m_solid_outline_color);
    d->m_brush.setColor(d->m_solid_fill_color);
  }
}

void
MarkerSymbol::removeItem(MarkerItem* item)
{
  Q_D(MarkerSymbol);
  d->m_items.removeAll(item);
}

void
MarkerSymbol::addItem(MarkerItem* item)
{
  Q_D(MarkerSymbol);
  d->m_items.append(item);
}

void
MarkerSymbol::renderItem(MarkerItem* item, QPainter* painter,
                         const QStyleOptionGraphicsItem* option,
                         QWidget* widget)
{
  Q_UNUSED(option);
  Q_UNUSED(widget);

  const auto path_ = path();
  Q_D(MarkerSymbol);

  if (!mapScene()) {
    const auto map_scene = qobject_cast<MapScene*>(item->scene());
    if (!map_scene) {
      return;
    }
    setMapScene(map_scene);
  }

  const auto world_transform = painter->worldTransform();
  auto symbol_box = world_transform.mapRect(path_.boundingRect());

  // Before plotting, update pen colors based on the attributes.
  updateColors(item);
  painter->setPen(d->m_pen);
  painter->setBrush(d->m_brush);

  painter->save();

// Scale symbol
#if 1
  if (!isScaleInvariant()) {
    // first scale the painter based on the symbol and scene units
    const auto unit_scale =
      d->m_unit_scale_factor_to_meters / d->m_scene_scale_factor_to_meters;
    painter->scale(unit_scale, unit_scale);

    // Then check for a minimum draw size.
    if (minimumSize() > 0) {
      symbol_box = painter->worldTransform().mapRect(path_.boundingRect());
      const auto factor =
        minimumSize() / qMax(symbol_box.height(), symbol_box.width());
      if (factor > 1) {
        painter->scale(factor, factor);
      }
    }
  }
#else
  // Check against minimum size
  if (minimumSize() > 0 && !isScaleInvariant()) {
    const auto factor =
      minimumSize() / qMin(symbol_box.height(), symbol_box.width());
    if (factor > 1) {
      painter->scale(factor, factor);
    }
  }
#endif

  painter->drawPath(path_);
  painter->restore();

  if (!isLabelVisible()) {
    return;
  }
  const auto label_text = item->labelText();
  if (label_text.isEmpty()) {
    return;
  }

  auto font = painter->font();
  font.setPointSizeF(labelSize());
  painter->setFont(font);

  const auto label_box =
    world_transform.mapRect(painter->fontMetrics().boundingRect(label_text));

  painter->save();
  // Reset the scale of the world transform
  painter->scale(1 / world_transform.m11(), 1 / world_transform.m22());

  auto x_offset = 0.0;
  auto y_offset = 0.0;

  // Horizontal offset
  if (d->m_label_alignment & Qt::AlignLeft) {
    x_offset = -label_box.width() - symbol_box.width() / 2;
  } else if (d->m_label_alignment & Qt::AlignHCenter) {
    x_offset = -label_box.width() / 2;
  } else if (d->m_label_alignment & Qt::AlignRight) {
    x_offset = symbol_box.width() / 2;
  }

  // Vertical offset
  if (d->m_label_alignment & Qt::AlignTop) {
    y_offset = -label_box.height() - symbol_box.height() / 2;
  } else if (d->m_label_alignment & Qt::AlignVCenter) {
    y_offset = -label_box.height() / 2;
  } else if (d->m_label_alignment & Qt::AlignBottom) {
    y_offset = symbol_box.height() / 2;
  }

  painter->translate(x_offset, y_offset);
  painter->setPen(d->m_label_pen);
  painter->drawStaticText(0, 0, item->label());

  painter->restore();
}
bool
MarkerSymbol::isScaleInvariant() const
{
  const Q_D(MarkerSymbol);
  return d->m_is_scale_invariant;
}
void
MarkerSymbol::setScaleInvariant(bool invariant)
{
  Q_D(MarkerSymbol);
  if (d->m_is_scale_invariant == invariant) {
    return;
  }

  d->m_is_scale_invariant = invariant;
  for (auto& item : d->m_items) {
    item->setFlag(QGraphicsItem::ItemIgnoresTransformations, invariant);
  }

  emit scaleInvariantChanged(invariant);
}

void
MarkerSymbol::setMinimumSize(qreal size)
{
  Q_D(MarkerSymbol);
  if (size == d->m_minimum_size) {
    return;
  }
  d->m_minimum_size = size;
  emit minimumSizeChanged(size);
}
qreal
MarkerSymbol::minimumSize() const
{
  const Q_D(MarkerSymbol);
  return d->m_minimum_size;
}

void
MarkerSymbol::setLabelVisible(bool visible)
{
  Q_D(MarkerSymbol);
  if (d->m_label_visible == visible) {
    return;
  }

  d->m_label_visible = visible;
  emit labelVisibleChanged(visible);
}

bool
MarkerSymbol::isLabelVisible() const
{
  const Q_D(MarkerSymbol);
  return d->m_label_visible;
}

Qt::Alignment
MarkerSymbol::labelAlignment() const
{
  const Q_D(MarkerSymbol);
  return d->m_label_alignment;
}

void
MarkerSymbol::setLabelAlignment(Qt::Alignment alignment)
{
  Q_D(MarkerSymbol);
  d->m_label_alignment = alignment;
}

void
MarkerSymbol::m_clonePaintSettingsFrom(const MarkerSymbol& other)
{
  setOutlineColor(other.outlineColor());
  setOutlineStyle(other.outlineStyle());
  setOutlineWidth(other.outlineWidth());
  setFillColor(other.fillColor());
  setFillStyle(other.fillStyle());
  setLabelColor(other.labelColor());
  setLabelSize(other.labelSize());
  setLabelAlignment(other.labelAlignment());
  setLabelVisible(other.isLabelVisible());
  setMinimumSize(other.minimumSize());
  setScaleInvariant(other.isScaleInvariant());
  setUnits(other.units());
  setUseColorGradient(other.useColorGradient());
  setGradientAttribute(other.gradientAttribute());
  setGradientPreset(other.gradientPreset());
  setGradientMax(other.gradientMax());
  setGradientMin(other.gradientMin());
}

void
MarkerSymbol::saveSettings(QSettings& settings)
{
  MarkerSymbol::saveSettings(settings, "");
}

void
MarkerSymbol::saveSettings(QSettings& settings, QString prefix)
{
  settings.setValue(prefix + "outline/color", outlineColor().name());
  settings.setValue(prefix + "outline/width", outlineWidth());
  settings.setValue(
    prefix + "outline/style",
    QMetaEnum::fromType<Qt::PenStyle>().valueToKey(outlineStyle()));

  settings.setValue(prefix + "fill/color", fillColor().name());
  settings.setValue(
    prefix + "fill/style",
    QMetaEnum::fromType<Qt::BrushStyle>().valueToKey(fillStyle()));
  settings.setValue(prefix + "label/color", labelColor().name());
  settings.setValue(prefix + "label/size", labelSize());
  settings.setValue(prefix + "label/visible", isLabelVisible());
  // settings.setValue(prefix + "label/alignment",
  // static_cast<int>(labelAlignment()));
  settings.setValue(prefix + "label/alignment",
                    QString{ QMetaEnum::fromType<Qt::Alignment>().valueToKeys(
                      labelAlignment()) });
  settings.setValue(prefix + "scale_invariant", isScaleInvariant());
  settings.setValue(prefix + "minimum_size", minimumSize());
  settings.setValue(prefix + "use_gradient", useColorGradient());
  settings.setValue(prefix + "gradient/preset",
                    static_cast<int>(gradientPreset()));
  settings.setValue(prefix + "gradient/attribute", gradientAttribute());
  settings.setValue(prefix + "gradient/min", gradientMin());
  settings.setValue(prefix + "gradient/max", gradientMax());
}

void
MarkerSymbol::loadSettings(const QSettings& settings, const QString& prefix)
{
  auto value = settings.value(prefix + "outline/color");
  if (value.isValid()) {
    const auto color = QColor{ value.toString() };
    if (color.isValid()) {
      setOutlineColor(color);
    }
  }

  value = settings.value(prefix + "outline/width");
  auto ok = false;
  if (value.isValid()) {
    const auto width = value.toDouble(&ok);
    if (ok) {
      setOutlineWidth(width);
    }
  }

  value = settings.value(prefix + "outline/style");
  if (value.isValid()) {
    const auto key = value.toString().toStdString();
    const auto style =
      QMetaEnum::fromType<Qt::PenStyle>().keyToValue(key.data(), &ok);
    if (ok) {
      setOutlineStyle(static_cast<Qt::PenStyle>(style));
    }
  }

  value = settings.value(prefix + "fill/color");
  if (value.isValid()) {
    const auto color = QColor{ value.toString() };
    if (color.isValid()) {
      setFillColor(color);
    }
  }

  value = settings.value(prefix + "fill/style");
  if (value.isValid()) {
    const auto key = value.toString().toStdString();
    const auto style =
      QMetaEnum::fromType<Qt::BrushStyle>().keyToValue(key.data(), &ok);
    if (ok) {
      setFillStyle(static_cast<Qt::BrushStyle>(style));
    }
  }

  value = settings.value(prefix + "label/color");
  if (value.isValid()) {
    const auto color = QColor{ value.toString() };
    if (color.isValid()) {
      setLabelColor(color);
    }
  }

  value = settings.value(prefix + "label/size");
  if (value.isValid()) {
    const auto size = value.toReal();
    setLabelSize(size);
  }

  value = settings.value(prefix + "label/visible");
  if (value.isValid()) {
    setLabelVisible(value.toBool());
  }

  value = settings.value(prefix + "label/alignment");
  if (value.isValid()) {
    const auto keys = value.toString().toStdString();
    const auto alignment =
      QMetaEnum::fromType<Qt::Alignment>().keysToValue(keys.data(), &ok);
    if (ok) {
      setLabelAlignment(static_cast<Qt::Alignment>(alignment));
    }
  }

  value = settings.value(prefix + "scale_invariant");
  if (value.isValid()) {
    setScaleInvariant(value.toBool());
  }

  value = settings.value(prefix + "minimum_size");
  if (value.isValid()) {
    const auto min_size = value.toDouble(&ok);
    if (ok) {
      setMinimumSize(min_size);
    }
  }

  value = settings.value(prefix + "use_gradient");
  if (value.isValid()) {
    setUseColorGradient(value.toBool());
  }

  value = settings.value(prefix + "gradient/attribute");
  if (value.isValid()) {
    const auto attribute = value.toString();
    setGradientAttribute(QString(attribute));
  }

  // NOTE(LEL): If I understand correctly, there's no need to propagate a signal
  // to the symbolLayerGUI -- the GdalColorGradientWidget won't have the layer
  // set until asked by a user, and loadSettings is called on startup (if ever).
  auto preset_value = settings.value(prefix + "gradient/preset");
  auto min_value = settings.value(prefix + "gradient/min");
  auto max_value = settings.value(prefix + "gradient/max");
  if (preset_value.isValid() && min_value.isValid() && max_value.isValid()) {
    bool preset_ok = false;
    const auto preset = static_cast<GdalColorGradient::GradientPreset>(
      preset_value.toInt(&preset_ok));
    bool min_ok = false;
    const auto min = min_value.toDouble(&min_ok);
    bool max_ok = false;
    const auto max = max_value.toDouble(&max_ok);
    if (preset_ok && min_ok && max_ok) {
      setGradientPreset(preset);
      setGradientMin(min);
      setGradientMax(max);
    }
  }
}

QColor
MarkerSymbol::outlineColor() const
{
  const Q_D(MarkerSymbol);
  return d->m_solid_outline_color;
}

QColor
MarkerSymbol::fillColor() const
{
  const Q_D(MarkerSymbol);
  return d->m_solid_fill_color;
}

bool
MarkerSymbol::useColorGradient() const
{
  const Q_D(MarkerSymbol);
  return d->m_use_color_gradient;
}

void
MarkerSymbol::setUseColorGradient(bool use_gradient)
{
  Q_D(MarkerSymbol);
  if (d->m_use_color_gradient == use_gradient) {
    return;
  }
  d->m_use_color_gradient = use_gradient;
  emit useColorGradientChanged(use_gradient);
}

QString
MarkerSymbol::gradientAttribute() const
{
  const Q_D(MarkerSymbol);
  return d->m_gradient_attribute;
}

void
MarkerSymbol::setGradientAttribute(QString attribute)
{
  Q_D(MarkerSymbol);
  if (d->m_gradient_attribute == attribute) {
    return;
  }
  d->m_gradient_attribute = attribute;
  emit gradientAttributeChanged(attribute);
}

GdalColorGradient::GradientPreset
MarkerSymbol::gradientPreset() const
{
  const Q_D(MarkerSymbol);
  return d->m_gdal_color_gradient->preset();
}

void
MarkerSymbol::setGradientPreset(GdalColorGradient::GradientPreset preset)
{
  Q_D(MarkerSymbol);
  if (d->m_gdal_color_gradient->preset() == preset) {
    return;
  }
  d->m_gdal_color_gradient->setGradient(preset);
  emit gradientPresetChanged(preset);
}

std::shared_ptr<GdalColorGradient const>
MarkerSymbol::gradient() const
{
  const Q_D(MarkerSymbol);
  return std::const_pointer_cast<GdalColorGradient const>(
    d->m_gdal_color_gradient);
}

std::shared_ptr<GdalColorGradient>
MarkerSymbol::gradient()
{
  const Q_D(MarkerSymbol);
  return d->m_gdal_color_gradient;
}

qreal
MarkerSymbol::gradientMin() const
{
  const Q_D(MarkerSymbol);
  return d->m_gdal_color_gradient->min();
}

void
MarkerSymbol::setGradientRange(qreal min, qreal max)
{
  Q_D(MarkerSymbol);
  if ((d->m_gdal_color_gradient->min() == min) &&
      (d->m_gdal_color_gradient->max() == max)) {
    return;
  }
  d->m_gdal_color_gradient->setRange(min, max);
  emit gradientRangeChanged(min, max);
}

void
MarkerSymbol::setGradientMin(qreal min)
{
  Q_D(MarkerSymbol);
  if (d->m_gdal_color_gradient->min() == min) {
    return;
  }
  d->m_gdal_color_gradient->setMin(min);
  emit gradientMinChanged(min);
}

qreal
MarkerSymbol::gradientMax() const
{
  const Q_D(MarkerSymbol);
  return d->m_gdal_color_gradient->max();
}

void
MarkerSymbol::setGradientMax(qreal max)
{
  Q_D(MarkerSymbol);
  if (d->m_gdal_color_gradient->max() == max) {
    return;
  }
  d->m_gdal_color_gradient->setMax(max);
  emit gradientMaxChanged(max);
}

qreal
MarkerSymbol::outlineWidth() const
{
  const Q_D(MarkerSymbol);
  return d->m_pen.widthF();
}

void
MarkerSymbol::setOutlineWidth(qreal width)
{
  Q_D(MarkerSymbol);
  if (d->m_pen.widthF() == width) {
    return;
  }

  d->m_pen.setWidthF(width);
  emit outlineWidthChanged(width);
}

QColor
MarkerSymbol::labelColor() const
{
  const Q_D(MarkerSymbol);
  return d->m_label_pen.color();
}
void
MarkerSymbol::setLabelColor(const QColor& color)
{

  Q_D(MarkerSymbol);
  if (!color.isValid() || color == d->m_label_pen.color()) {
    return;
  }

  d->m_label_pen.setColor(color);
  emit labelColorChanged(color);
}

qreal
MarkerSymbol::labelSize() const
{
  const Q_D(MarkerSymbol);
  return d->m_label_size;
}
void
MarkerSymbol::setLabelSize(const qreal size)
{

  Q_D(MarkerSymbol);
  if (size == d->m_label_size) {
    return;
  }

  d->m_label_size = size;
  emit labelSizeChanged(size);
}

void
MarkerSymbol::setFillColor(const QColor& color)
{
  Q_D(MarkerSymbol);
  if (!color.isValid() || color == d->m_solid_fill_color) {
    return;
  }
  d->m_solid_fill_color = color;
  emit fillColorChanged(color);
}

void
MarkerSymbol::setOutlineColor(const QColor& color)
{
  Q_D(MarkerSymbol);
  if (!color.isValid() || color == d->m_solid_outline_color) {
    return;
  }
  d->m_solid_outline_color = color;
  emit outlineColorChanged(color);
}

Qt::PenStyle
MarkerSymbol::outlineStyle() const
{
  const Q_D(MarkerSymbol);
  return d->m_pen.style();
}
Qt::BrushStyle
MarkerSymbol::fillStyle() const
{
  const Q_D(MarkerSymbol);
  return d->m_brush.style();
}
void
MarkerSymbol::setFillStyle(Qt::BrushStyle style)
{
  Q_D(MarkerSymbol);
  if (style == d->m_brush.style()) {
    return;
  }

  d->m_brush.setStyle(style);
  emit fillStyleChanged(style);
}
void
MarkerSymbol::setOutlineStyle(Qt::PenStyle style)
{

  Q_D(MarkerSymbol);
  if (style == d->m_pen.style()) {
    return;
  }

  d->m_pen.setStyle(style);
  emit outlineStyleChanged(style);
}

MapScene*
MarkerSymbol::mapScene() const
{
  const Q_D(MarkerSymbol);
  return d->m_scene;
}

void
MarkerSymbol::setMapScene(MapScene* scene)
{

  Q_D(MarkerSymbol);
  if (d->m_scene_projection_connection) {
    QObject::disconnect(d->m_scene_projection_connection);
  }

  if (!scene) {
    d->m_scene = nullptr;
    return;
  }

  const auto proj = scene->projectionPtr();
  if (proj) {
    d->m_scene_scale_factor_to_meters = proj->scaleFactorToMeters();
    const auto unit_scale =
      d->m_unit_scale_factor_to_meters / d->m_scene_scale_factor_to_meters;
    for (auto& item : d->m_items) {
      item->setScale(unit_scale);
    }
  }

  d->m_scene_projection_connection = QObject::connect(
    scene, &dslmap::MapScene::projectionChanged,
    [this](const QString& definition) {
      auto ok = false;
      const auto p = Proj::scaleFactorFromDefinition(definition, &ok);
      if (!ok) {
        qWarning()
          << "Failed to get map units from new projection for MarkerSymbol.";
        return;
      }

      Q_D(MarkerSymbol);
      d->m_scene_scale_factor_to_meters = p.second;
    });

  d->m_scene = scene;
}

Unit
MarkerSymbol::units() const
{
  const Q_D(MarkerSymbol);
  return d->m_units;
}

void
MarkerSymbol::setUnits(Unit unit)
{
  Q_D(MarkerSymbol);
  if (unit == d->m_units) {
    return;
  }
  d->m_unit_scale_factor_to_meters = scaleToMeters(unit);

  const auto unit_scale =
    d->m_unit_scale_factor_to_meters / d->m_scene_scale_factor_to_meters;
  for (auto& item : d->m_items) {
    item->setScale(unit_scale);
  }
  emit unitsChanged(unit);
}

QPen
MarkerSymbol::outlinePen() const noexcept
{
  const Q_D(MarkerSymbol);
  return d->m_pen;
}
QPen
MarkerSymbol::labelPen() const noexcept
{
  const Q_D(MarkerSymbol);
  return d->m_label_pen;
}
QBrush
MarkerSymbol::fillBrush() const noexcept
{
  const Q_D(MarkerSymbol);
  return d->m_brush;
}
}
