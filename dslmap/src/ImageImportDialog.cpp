/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ImageImportDialog.h"
#include "ui_ImageImportDialog.h"
#include <QBitmap>
#include <QFileDialog>
#include <QPointF>

namespace dslmap {

ImageImportDialog::ImageImportDialog(QWidget* parent)
  : QDialog(parent)
  , ui(new Ui::ImageImportDialog)
{
  ui->setupUi(this);
  ui->okBox->button(QDialogButtonBox::Ok)->setEnabled(false);
  ui->nameEdit->setPlaceholderText("Layer Name");

  ui->topLeftX->setPlaceholderText("Top Left Lon");
  ui->topLeftY->setPlaceholderText("Top Left Lat");
  ui->bottomRightX->setPlaceholderText("Bottom right Lon");
  ui->bottomRightY->setPlaceholderText("Bottom right Lat");
  
  valid_lon = new QDoubleValidator();
  valid_lon->setNotation(QDoubleValidator::StandardNotation);
  valid_lon->setRange(-180,180,7);

  valid_lat = new QDoubleValidator();
  valid_lat->setNotation(QDoubleValidator::StandardNotation);
  valid_lat->setRange(-90,90,7);

  ui->topLeftX->setValidator(valid_lon);
  ui->topLeftY->setValidator(valid_lat);
  ui->bottomRightX->setValidator(valid_lon);
  ui->bottomRightY->setValidator(valid_lat);

  connect(ui->fileBtn, &QPushButton::clicked, this,
          &ImageImportDialog::fileDlog);
  connect(ui->coordBtn, &QPushButton::clicked, this,
          &ImageImportDialog::coordFileDlog);
  connect(ui->previewBtn, &QPushButton::clicked, this,
          &ImageImportDialog::preview);
  connect(ui->clearImgButton, &QPushButton::clicked, [this]() { emit clearSavedImage(); });
  connect(ui->topLeftX, &QLineEdit::textChanged, this, &ImageImportDialog::coordTextChanged);
  connect(ui->topLeftY, &QLineEdit::textChanged, this, &ImageImportDialog::coordTextChanged);
  connect(ui->bottomRightX, &QLineEdit::textChanged, this, &ImageImportDialog::coordTextChanged);
  connect(ui->bottomRightY, &QLineEdit::textChanged, this, &ImageImportDialog::coordTextChanged);
}

ImageImportDialog::~ImageImportDialog()
{
  delete ui;
}

void
ImageImportDialog::coordTextChanged(QString text)
{
  // if any invalid, keep submit button disabled
  QDoubleValidator* val;
  QString str;
  int i;
  QValidator::State state;

  val = (QDoubleValidator*)ui->topLeftX->validator();
  str = ui->topLeftX->text();
  i = 0;
  state = val->validate(str, i);

  if (state != QValidator::Acceptable) {
    ui->okBox->button(QDialogButtonBox::Ok)->setEnabled(false);
    return;
  }

  val = (QDoubleValidator*)ui->bottomRightX->validator();
  str = ui->bottomRightX->text();
  i = 0;
  state = val->validate(str, i);

  if (state != QValidator::Acceptable) {
    ui->okBox->button(QDialogButtonBox::Ok)->setEnabled(false);
    return;
  }

  val = (QDoubleValidator*)ui->topLeftY->validator();
  str = ui->topLeftY->text();
  i = 0;
  state = val->validate(str, i);

  if (state != QValidator::Acceptable) {
    ui->okBox->button(QDialogButtonBox::Ok)->setEnabled(false);
    return;
  }

  val = (QDoubleValidator*)ui->bottomRightY->validator();
  str = ui->bottomRightY->text();
  i = 0;
  state = val->validate(str, i);

  if (state != QValidator::Acceptable) {
    ui->okBox->button(QDialogButtonBox::Ok)->setEnabled(false);
    return;
  }

  ui->okBox->button(QDialogButtonBox::Ok)->setEnabled(true);
}

void
ImageImportDialog::fileDlog()
{
  QString filename =
    QFileDialog::getOpenFileName(this, tr("Open Image"), "/home/",
                                 tr("Image Files (*.png *.jpg *.bmp *.tif)"));
  ui->fileEdit->setText(filename);
}

//Coordinate info file
void
ImageImportDialog::coordFileDlog(){
  
  QString filename =
    QFileDialog::getOpenFileName(this, tr("Open Coord Info File"), "/home/",
                                 tr("Text Files (*.txt)"));
  ui->coordEdit->setText(filename);
  populateCornerPoints(filename);
  
}

void
ImageImportDialog::populateCornerPoints(QString coordFileName)
{
  QFileInfo check_file(coordFileName);
  // check if file exists and if yes: Is it really a file and no directory?
  if (check_file.exists() && check_file.isFile()) {

    QSettings world_file_settings(coordFileName, QSettings::IniFormat);
    world_file_settings.setFallbacksEnabled(false);
    world_file_settings.beginGroup("CORNERS");
    const QString coordinate_type =
      world_file_settings.value("COORDINATE_TYPE", "GEOGRAPHIC").toString();
    const QStringList corner_strings = {
      "LL_LON", "LL_LAT", "UL_LON", "UL_LAT",
      "UR_LON", "UR_LAT", "LR_LON", "LR_LAT"
    };
    QList<double> corner_coords;
    QVariant value;
    for (int i = 0; i < corner_strings.count(); i++) {
      value = world_file_settings.value(corner_strings[i]);
      if (value.isValid()) // was !value.isValid() mjs-11/23/2014
      {
        corner_coords << value.toDouble();
      } else {
        qWarning() << "Warning! UNDERLAY WORLD_FILE_NAME=" << coordFileName
                   << " missing config KEY " << corner_strings[i];
      }
    }
    world_file_settings.endGroup();
    if (corner_coords.count() < corner_strings.count()) {
      qWarning() << "Warning! UNDERLAY: WORLD_FILE_NAME=" << coordFileName
                 << " missing required corner data!";
    }

    else {
      const QPointF lower_left(corner_coords[0], corner_coords[1]);
      const QPointF upper_left(corner_coords[2], corner_coords[3]);
      ui->topLeftX->setText(QString::number(corner_coords[2], 'f', 7));
      ui->topLeftY->setText(QString::number(corner_coords[3], 'f', 7));
      const QPointF upper_right(corner_coords[4], corner_coords[5]);
      const QPointF lower_right(corner_coords[6], corner_coords[7]);
      ui->bottomRightX->setText(QString::number(corner_coords[6], 'f', 7));
      ui->bottomRightY->setText(QString::number(corner_coords[7], 'f', 7));
    }
  }
}

void
ImageImportDialog::preview()
{
  QString filename = ui->fileEdit->text();
  if (filename == "") {
    ui->previewLbl->setText("No image file selected");
    return;
  }
  QPixmap pixmap(filename);
  ui->previewLbl->setPixmap(pixmap);
  ui->previewLbl->setMask(pixmap.mask());

  ui->previewLbl->show();
}

QString
ImageImportDialog::getFilename()
{
  return ui->fileEdit->text();
}

QString
ImageImportDialog::getLayerName()
{
  if (ui->nameEdit->text() == "") {
    return "New Image Layer";
  }
  return ui->nameEdit->text();
}

QRectF
ImageImportDialog::getRect()
{
  if (ui->topLeftX->text().isEmpty() || ui->topLeftY->text().isEmpty() ||
      ui->bottomRightX->text().isEmpty() ||
      ui->bottomRightY->text().isEmpty()){
        return QRectF();
      }
    double topLeftX = ui->topLeftX->text().toDouble();
  double topLeftY = ui->topLeftY->text().toDouble();
  double bottomRightX = ui->bottomRightX->text().toDouble();
  double bottomRightY = ui->bottomRightY->text().toDouble();

  QRectF ret{ QPointF{ topLeftX, topLeftY },
              QPointF{ bottomRightX, bottomRightY } };
  return ret;
}

bool
ImageImportDialog::getSaveIniState(){
  return ui->saveIniCheck->isChecked();
}

} // namespace
