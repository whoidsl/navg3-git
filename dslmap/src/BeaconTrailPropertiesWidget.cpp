/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 8/9/18.
//

#include "BeaconTrailPropertiesWidget.h"
#include "ui_BeaconTrailPropertiesWidget.h"

#include "BeaconLayer.h"
#include "ColorPickButton.h"

#include <QDoubleValidator>
#include <QMetaEnum>
#include <QPen>

namespace dslmap {
BeaconTrailPropertiesWidget::BeaconTrailPropertiesWidget(BeaconLayer* layer,
                                                         QWidget* parent)
  : QWidget(parent)
  , ui(std::make_unique<Ui::BeaconTrailPropertiesWidget>())
{
  setLayer(layer);
}

BeaconTrailPropertiesWidget::BeaconTrailPropertiesWidget(QWidget* parent)
  : QWidget(parent)
  , ui(std::make_unique<Ui::BeaconTrailPropertiesWidget>())
{
}

void
BeaconTrailPropertiesWidget::setLayer(BeaconLayer* layer)
{
  ui->setupUi(this);

  if (!layer) {
    return;
  }
  ui->edit_xshift->setValidator(new QDoubleValidator(ui->edit_xshift));
  ui->edit_yshift->setValidator(new QDoubleValidator(ui->edit_yshift));
  auto shift = layer->shiftOffset();
  ui->edit_xshift->setText(QString::number(shift.x()));
  ui->edit_yshift->setText(QString::number(shift.y()));

  connect(ui->edit_xshift, &QLineEdit::textChanged,
          [this, layer](QString xtext) {
            auto x = xtext.toDouble();
            auto y = ui->edit_yshift->text().toDouble();
            layer->setShiftOffset(QPointF{ x, y });
          });
  connect(ui->edit_yshift, &QLineEdit::textChanged,
          [this, layer](QString ytext) {
            auto y = ytext.toDouble();
            auto x = ui->edit_xshift->text().toDouble();
            layer->setShiftOffset(QPointF{ x, y });
          });
  connect(layer, &BeaconLayer::shiftOffsetChanged, [this](QPointF shift) {
    ui->edit_xshift->setText(QString::number(shift.x()));
    ui->edit_yshift->setText(QString::number(shift.y()));
  });

  // Trail Visibility controls
  ui->check_visible->setChecked(layer->trailVisible());
  connect(layer, &BeaconLayer::trailVisibilityChanged,
          [this](bool visible) { ui->check_visible->setChecked(visible); });

  connect(ui->check_visible, &QCheckBox::clicked,
          [this, layer](bool visible) { layer->setTrailVisible(visible); });

  auto meta_enum = QMetaEnum::fromType<dslmap::ShapeMarkerSymbol::Shape>();
  for (auto i = 0; i < meta_enum.keyCount(); ++i) {
    const auto key = meta_enum.key(i);
    const auto value =
      static_cast<dslmap::ShapeMarkerSymbol::Shape>(meta_enum.keyToValue(key));
    if (value == dslmap::ShapeMarkerSymbol::Shape::Invalid) {
      continue;
    }

    ui->combo_shape->addItem(key, QVariant::fromValue(value));
  }

  auto symbol =
    dynamic_cast<dslmap::ShapeMarkerSymbol*>(layer->trailSymbol().get());
  if (symbol) {
    ui->button_outline->setColor(symbol->outlineColor());
    ui->button_fill->setColor(symbol->fillColor());
    const auto shape_index =
      ui->combo_shape->findData(QVariant::fromValue(symbol->shape()));
    if (shape_index >= 0) {
      ui->combo_shape->setCurrentIndex(shape_index);
    } else {
      ui->combo_shape->setCurrentIndex(0);
    }

    ui->check_outline->setChecked(symbol->outlineStyle() != Qt::NoPen);
    ui->check_fill->setChecked(symbol->fillStyle() != Qt::NoBrush);
    ui->spinbox_size->setValue(symbol->size());
    ui->spinbox_length->setValue(layer->displayedLength());
    ui->spinbox_capacity->setValue(layer->capacity());

    // Trail Shape
    connect(symbol, &ShapeMarkerSymbol::shapeChanged,
            [this](dslmap::ShapeMarkerSymbol::Shape shape) {
              const auto index =
                ui->combo_shape->findData(QVariant::fromValue(shape));
              if (index < 0) {
                return;
              }

              ui->combo_shape->setCurrentIndex(index);
            });

    connect(
      ui->combo_shape,
      static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
      [this, symbol] {
        const auto variant = ui->combo_shape->currentData();
        if (!variant.isValid()) {
          return;
        }

        const auto shape = variant.value<dslmap::ShapeMarkerSymbol::Shape>();
        if (shape == dslmap::ShapeMarkerSymbol::Shape::Invalid) {
          return;
        }

        symbol->setShape(shape);
      });

    connect(symbol, &ShapeMarkerSymbol::sizeChanged, [this](qreal size) {

      if (size == ui->spinbox_size->value()) {
        return;
      }

      ui->spinbox_size->setValue(size);
    });

    connect(ui->spinbox_size,
            static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged),
            [this, symbol](int size) {
              if (size < 0) {
                return;
              }

              symbol->setSize(size);
            });
  }

  // Trail length
  connect(layer, &BeaconLayer::displayedLengthChanged,
          [this](int length) { ui->spinbox_length->setValue(length); });

  connect(ui->spinbox_length,
          static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged),
          [this, layer](int length) { layer->setDisplayedLength(length); });

  // Trail Capacity
  connect(layer, &BeaconLayer::capacityChanged,
          [this](int capacity) { ui->spinbox_capacity->setValue(capacity); });
  
  // Set Trail capacity upon "SET" button press
  connect(ui->set_max_button, &QPushButton::pressed, [this, layer]() {
    layer->setCapacity(ui->spinbox_capacity->value());
  });

  // Trail Outline Color
  connect(layer, &BeaconLayer::trailOutlineColorChanged,
          [this](QColor color) { ui->button_outline->setColor(color); });

  connect(layer, &BeaconLayer::trailOutlineStyleChanged,
          [this](Qt::PenStyle style) {
            ui->check_outline->setChecked(style != Qt::NoPen);
          });

  connect(ui->check_outline,
          static_cast<void (QCheckBox::*)(bool)>(&QCheckBox::toggled),
          [this, layer](bool checked) {
            if (checked) {
              layer->setTrailOutlineStyle(Qt::SolidLine);
            } else {
              layer->setTrailOutlineStyle(Qt::NoPen);
            }
          });

  connect(ui->button_outline, &dslmap::ColorPickButton::colorChanged,
          [this, layer](QColor color) { layer->setTrailOutlineColor(color); });

  // Trail Fill Color
  connect(layer, &BeaconLayer::trailFillStyleChanged,
          [this](Qt::BrushStyle style) {
            ui->check_fill->setChecked(style != Qt::NoBrush);
          });

  connect(ui->check_fill,
          static_cast<void (QCheckBox::*)(bool)>(&QCheckBox::toggled),
          [this, layer](bool checked) {
            if (checked) {
              layer->setTrailFillStyle(Qt::SolidPattern);
            } else {
              layer->setTrailFillStyle(Qt::NoBrush);
            }
          });

  connect(layer, &BeaconLayer::trailFillColorChanged,
          [this](QColor color) { ui->button_fill->setColor(color); });

  connect(ui->button_fill, &dslmap::ColorPickButton::colorChanged,
          [this, layer](QColor color) { layer->setTrailFillColor(color); });
}

BeaconTrailPropertiesWidget::~BeaconTrailPropertiesWidget() = default;
} // namespace
