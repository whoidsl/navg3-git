/**
 * Copyright 2020 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
//
//

#include "GeoreferenceImageObject.h"
#include "GdalColorGradient.h"
#include "GdalDataset.h"
#include "GdalUtils.h"
#include "MapItem.h"
#include "Proj.h"
#include "TemporaryGdalDataset.h"
#include "ImageTilingHelper.h"

#include <QBitmap>
#include <QPainter>
#include <gdal/gdal_priv.h>

namespace dslmap {

Q_LOGGING_CATEGORY(dslmapGeoreferenceImageObject, "dslmap.object.geoimage")

class GeoreferenceImageObjectPrivate
{
public:
  explicit GeoreferenceImageObjectPrivate()
  {
  }

  ~GeoreferenceImageObjectPrivate() = default;

  std::shared_ptr<const Proj> m_proj;

  std::shared_ptr<const Proj> m_backup_proj;

  std::shared_ptr<GdalDataset> m_backup_dataset;

  ///\brief Original bathy dataset on disk
  std::shared_ptr<GdalDataset> m_dataset;

  ///\brief Projected bathy dataset
  std::shared_ptr<GdalDataset> m_projected_dataset;

  /// \brief Original->Projected transform
  ///
  /// Maps dataset x,y -> map scene coordinates
  QTransform m_projection_transform;

  /// \brief Projected->Original transform
  ///
  /// Maps map scene coordinates -> dataset x,y
  QTransform m_inverse_projection_transform;

  /// \brief Current QColor to be used for image transparency
  QColor transColor;

  /// \brief Previously set transparent color.
  QColor prevTransColor;

  /// \brief Color table for generating QImage if applicable
  GDALColorTable m_color_table;

  std::vector<GDALColorInterp> m_band_interp;

  std::vector<GDALColorTable> m_band_table;

  /// \brief Flag value to determine whether qimage uses a color table.
  /// Assumes false, overwritten if color table is found in GDAL raster bands
  bool has_color_table = false;

  /// \brief Flag whether image should be interpreted as grayscale
  bool isGreyScale = false;

  int m_band = 1; ///< What layer to use from the dataset (default=1)

  QRectF m_projected_dataset_bounds = {};

  qreal m_dataset_min; ///< Minimum value of the dataset
  qreal m_dataset_max; ///< Maximum value of the dataset

  /// \brief QImage stored in object. Has getter function
  QImage m_image;

  /// \brief The bounds of the inner rectangle to crop around
  /// Bounds will be smaller than QImage original dimensions
  QRect cropRectangle;

  std::vector<QGraphicsPixmapItem*> m_projected_pixmaps;
};

GeoreferenceImageObject::GeoreferenceImageObject(QGraphicsItem* parent)
  : QGraphicsObject(parent)
  , d_ptr(new GeoreferenceImageObjectPrivate)
{

  Q_D(GeoreferenceImageObject);

  setFlag(ItemHasNoContents, true);
}

GeoreferenceImageObject::~GeoreferenceImageObject()
{
  delete d_ptr;
}

QImage
GeoreferenceImageObject::getQImage()
{
  Q_D(GeoreferenceImageObject);

  return d->m_image;
}

void
GeoreferenceImageObject::cropImage(int x0, int y0, int dx, int dy)
{
  Q_D(GeoreferenceImageObject);

  // QImage tempImg;
  QRect cropRect(x0, y0, dx - x0, dy - y0);
  d->cropRectangle = cropRect;
  qCDebug(dslmapGeoreferenceImageObject) << "Crop rect size is  "
                                         << cropRect.size();

  update(UpdateOption::UpdateCrop);
}

QImage
GeoreferenceImageObject::combineQImageBands(
  std::vector<QImage> images, std::vector<GDALRasterBand*> bands)
{
  Q_D(GeoreferenceImageObject);
  int channels = images.size();
  int rows = images[0].height();
  int cols = images[0].width();
  
  QImage combined = QImage(images[0].size(), QImage::Format_RGB888);

  QColor combinedColor;

  for (int i = 0; i < channels; i++) {
    // for each image

    QImage im = images[i];

    if (!im.isNull()) {

      GDALColorInterp colorInterp = bands[i]->GetColorInterpretation();

      if (colorInterp == GCI_PaletteIndex) {
        GDALColorTable* gdalTable = bands[i]->GetColorTable();
        if (gdalTable != nullptr) {

          QVector<QRgb> ColorTable;
          auto colorCount = gdalTable->GetColorEntryCount();
          ColorTable.resize(colorCount);

          // convert color table from dataset to usable color table for
          // qimage
          for (int f = 0; f < colorCount; ++f) {

            const GDALColorEntry* colorEntry = gdalTable->GetColorEntry(f);
            if (!colorEntry)
              continue;

            ColorTable[f] =
              qRgb(colorEntry->c1, colorEntry->c2, colorEntry->c3);
          }

          images[i] = images[i].convertToFormat(QImage::Format_Indexed8);
          images[i].setColorTable(ColorTable);
        } else {
          // no color table but still palette index
          images[i] = images[i].convertToFormat(QImage::Format_Grayscale8);
          // was likely passed in as gray scale but lets do the conversion to
          // make sure
        }
      }

   
    }
  }
  if (channels == 1) {
    return images[0];
  }

  else {
    //if need be use gdal translate to convert >3 or 4 bands into 3 band rgb
    // attempt to combine from multiple bands to rgb
    for (int i = 0; i < cols; i++) {
      for (int j = 0; j < rows; j++) {

        combinedColor = QColor(0, 0, 0, 0);
        for (int k = 0; k < channels; k++) {
          QImage im = images.at(k);

          // rgba 0-255
          if (!im.isNull()) {
            GDALColorInterp colorInterp = bands[k]->GetColorInterpretation();

            // formated only for rgb band order
            QColor color = im.pixel(i, j);
            if (colorInterp == GCI_RedBand) {
              combinedColor.setRed(color.red());
            }
            if (colorInterp == GCI_GreenBand) {
              combinedColor.setGreen(color.green());
            }
            if (colorInterp == GCI_BlueBand) {
              combinedColor.setBlue(color.blue());
            }
            if (colorInterp == GCI_AlphaBand){
              combinedColor.setAlpha(color.alpha());
            }
          }
        }
        combined.setPixel(i, j, combinedColor.rgb());
      }
    }
    return combined;
  }
}

QString
GeoreferenceImageObject::getProj4StringFromDataset(
  std::shared_ptr<GdalDataset> dataset)
{
  const char* projDef = dataset->get()->GetProjectionRef();
  QString projDefQString = QString::fromUtf8(projDef);

  if (projDefQString.isEmpty()) {
    qCInfo(dslmapGeoreferenceImageObject) << "getprojectionref is empty";
    projDef = dataset->get()->GetGCPProjection();
    projDefQString = QString::fromUtf8(projDef);
  }
  if (projDefQString.isEmpty()) {
    qCWarning(dslmapGeoreferenceImageObject)
      << "No projection found in dataset. Returning";
    return "";
  }

  qCInfo(dslmapGeoreferenceImageObject) << "projDef is------  " << projDef;

  OGRSpatialReference ogr(projDef);

  char* proj4String = NULL;
  ogr.exportToProj4(&proj4String);

  return QString::fromUtf8(proj4String);
}

bool
GeoreferenceImageObject::setRasterFile(const QString& filename,
                                       std::shared_ptr<Proj> proj)
{

  Q_D(GeoreferenceImageObject);

  auto dataset = std::make_shared<GdalDataset>(filename);

  if (!dataset->open()) {
    qCWarning(dslmapGeoreferenceImageObject) << "dataset not open:" << filename;
    return false;
  }

  if (proj) {
    qCInfo(dslmapGeoreferenceImageObject) << "Using projection override: "
                                          << proj->definition();

    dataset->setProjection(proj);

  } else if (!dataset->projection()) {
    qCInfo(dslmapGeoreferenceImageObject) << "Projection not set in dataset ";
    QString proj4QString = this->getProj4StringFromDataset(dataset);
    if (proj4QString.isEmpty()) {
      qCWarning(dslmapGeoreferenceImageObject)
        << "Proj4 String not found in dataset";

      return false;
    }

#if 0
      << "dataset has no preset projection. using own: ";
    QString baTest = "+proj=utm +ellps=GRS80 +zone=18 +towgs84=0,0,0,0,0,0,0 "
                     "+units=m +no_defs +datam=NAD83";
    QString ceaTest = "+proj=cea +lon_0=-117.333333333333 +lat_ts=33.75 +x_0=0 "
                      "+y_0=0 +datum=NAD27 +units=m +no_defs";
#endif

    QRegExp separator("[ =]");
    QStringList projStringList = proj4QString.split(separator);

    if (!projStringList.contains("+datum")) {
      // need to add datam nows

      qCWarning(dslmapGeoreferenceImageObject)
        << "proj4 string contains no datum";
      proj4QString.append("+datum=NAD83");
    }

    if (proj4QString.contains("+proj=cea")) {
      qCWarning(dslmapGeoreferenceImageObject)
        << "cea projections not supported currently. Returning";
      return false;
    }
#if 0
    for (auto arg : projStringList) {
      qCDebug(dslmapGeoreferenceImageObject) << "arg is " << arg;
    }
#endif

    qDebug(dslmapGeoreferenceImageObject) << "proj4 qstring: " << proj4QString;

    dataset->setProjection(Proj::fromDefinition(proj4QString));
  }

  d->m_dataset = std::move(dataset);

  d->m_proj = d->m_dataset->projection();

  d->m_backup_proj = d->m_dataset->projection();

  update(UpdateOption::UpdateAll);

  return true;
}

bool
GeoreferenceImageObject::setRasterFile(const QString& filename, QPointF top_left, QPointF bottom_right,
                                       std::shared_ptr<Proj> proj)
{

  Q_D(GeoreferenceImageObject);

  auto dataset = std::make_shared<GdalDataset>(filename);

  if (!dataset->open()) {
    qCWarning(dslmapGeoreferenceImageObject) << "dataset not open:" << filename;
    return false;
  }

  if (proj) {
    qCInfo(dslmapGeoreferenceImageObject) << "Using projection override: "
                                          << proj->definition();

    dataset->setProjection(proj);

  } else if (!dataset->projection()) {
    qCInfo(dslmapGeoreferenceImageObject) << "Projection not set in dataset ";
    dataset->setProjection(Proj::fromDefinition(PROJ_WGS84_DEFINITION));
    proj = Proj::fromDefinition(PROJ_WGS84_DEFINITION);
    
    QStringList opts;
    opts << "-a_ullr" << QString::number(top_left.x(), 'f', 7) << QString::number(top_left.y(), 'f', 7)
       << QString::number(bottom_right.x(), 'f', 7) << QString::number(bottom_right.y(), 'f', 7) ;
    dataset = dataset->translate(*proj, opts);

    }

  d->m_dataset = std::move(dataset);

  d->m_proj = d->m_dataset->projection();

  d->m_backup_proj = d->m_dataset->projection();

  update(UpdateOption::UpdateAll);

  return true;
}

bool
GeoreferenceImageObject::update(UpdateOptions options)
{
  qCDebug(dslmapGeoreferenceImageObject) << "Calling update with" << options;

  if (options == UpdateOption::UpdateNone) {
    qCDebug(dslmapGeoreferenceImageObject) << "UpdateNone";
    return true;
  }
  Q_D(GeoreferenceImageObject);

  if (options.testFlag(UpdateOption::UpdateProjection)) {
    qCDebug(dslmapGeoreferenceImageObject) << "UpdateProjection";

    if (!d->m_dataset->projection()) {
      qCWarning(dslmapGeoreferenceImageObject)
        << "Raster file:" << d->m_dataset->fileName()
        << "has no associated projection.";
      return false;
    }
    if (!d->m_proj) {
      qCDebug(dslmapGeoreferenceImageObject)
        << "No projection assigned to object.  Using "
           "projection read from dataset.";
      d->m_proj = d->m_dataset->projection();
    }

    if (d->m_dataset->projection()->isSame(*d->m_proj)) {
      qCDebug(dslmapGeoreferenceImageObject) << "No need to reproject dataset.";
      d->m_projected_dataset = d->m_dataset;

    } else {
  
      // can pass projection specific args with warp
      QStringList args;
      args << "-of"
           << "GTiff";

      auto proj_dataset =
        std::static_pointer_cast<GdalDataset>(d->m_dataset->warp_min(*d->m_proj, args));

      if (!proj_dataset) {
        qCWarning(dslmapGeoreferenceImageObject)
          << "Unable to reproject raster:" << d->m_dataset->fileName();
        return false;
      }

      qCDebug(dslmapGeoreferenceImageObject)
        << "Reprojected raster file:" << d->m_dataset->fileName()
        << "to file:" << proj_dataset->fileName();

      d->m_projected_dataset.swap(proj_dataset);
    }

    if (d->m_projected_dataset->boundingRect() !=
        d->m_projected_dataset_bounds) {

      prepareGeometryChange();
      d->m_projected_dataset_bounds =
        mapRectFromScene(d->m_projected_dataset->boundingRect());

    } else {
      qCDebug(dslmapGeoreferenceImageObject) << "bounding rects were same";
    }
    options |= UpdateOption::UpdateTransform;
  }

  if (options.testFlag(UpdateOption::UpdateData)) {
    qCDebug(dslmapGeoreferenceImageObject) << "UpdateData";
    // Get raster image size
    int rows = d->m_projected_dataset->get()->GetRasterYSize();
    int cols = d->m_projected_dataset->get()->GetRasterXSize();
    int channels = d->m_projected_dataset->get()->GetRasterCount();
    std::vector<GDALRasterBand*> bands;
    std::vector<std::vector<uchar>> band_data;

    for (int i = 0; i < channels; ++i) {
      band_data.push_back(std::vector<uchar>(rows * cols));
    } // Sizing vector this way to make sure each mat is explicility
    // constructed

    d->m_band_table.resize(channels);

    for (int i = 1; i < channels + 1; i++) {

      GDALRasterBand* band = d->m_projected_dataset->get()->GetRasterBand(i);
      bands.push_back(band);
      
      if (d->m_projected_dataset->get()->GetRasterBand(1)->GetColorTable()) {
        d->m_color_table =
          *d->m_projected_dataset->get()->GetRasterBand(1)->GetColorTable();

        d->has_color_table = true;
      }

      auto error =
        band->RasterIO(GF_Read, 0, 0, cols, rows,
                       band_data[i - 1].data(), // buffer for single band data
                       cols, rows, GDT_Byte, 0, 0);

      if (error) {
        qCWarning(dslmapGeoreferenceImageObject)
          << "Error reading raster data from dataset. Error : " << error;
        return false;
      }
    }
    std::vector<QImage> bandImages;

    QImage::Format format = QImage::Format_Grayscale8;
    if (d->has_color_table) {
      format = QImage::Format_Indexed8;
    } // change to indexed image format if we need to use a color table

    for (int i = 0; i < channels; i++) {
      bandImages.push_back(
        QImage(band_data[i].data(), cols, rows, cols, format));
    } // constructing qimage based off band data bytes. could refactor this to
    // use QImage.bits() as data buffer to. but that alters qimage
    // construction
    // eliminate having uchar * buffer directly

    QImage output = combineQImageBands(bandImages, bands);
    if (output.isNull()) {
      return false;
    }

    d->m_image = output.convertToFormat(QImage::Format_ARGB32);

    options |= UpdateOption::UpdatePixmap;
  }

  if (options.testFlag(UpdateOption::UpdateTransparency)) {

    qCDebug(dslmapGeoreferenceImageObject) << "UpdateTransparency";
    int cols = d->m_projected_dataset->get()->GetRasterXSize();
    int rows = d->m_projected_dataset->get()->GetRasterYSize();

    if (d->transColor.isValid()) {
      for (int x = 0; x < cols; x++) {
        for (int y = 0; y < rows; y++) {
          QColor pixel = d->m_image.pixel(x, y);

          if (pixel.rgb() == d->transColor.rgb()) {
            d->m_image.setPixel(x, y,
                                qRgba(pixel.red(), pixel.green(), pixel.blue(),
                                      0)); // 0 for alpha is full transparent
          }
          if (pixel.rgb() == d->prevTransColor.rgb()) {

            d->m_image.setPixel(
              x, y, qRgba(pixel.red(), pixel.green(), pixel.blue(), 255));
            // qrgba (red,green,blue,alpha) 0-255
          }
        }
      }
    }

    options |= UpdateOption::UpdatePixmap | UpdateOption::UpdateCrop;
  }

  if (options.testFlag(UpdateOption::UpdateCrop)) {
    qCDebug(dslmapGeoreferenceImageObject) << "UpdateCrop";
    auto imageSize = d->m_image.size();

    QImage tempImage(imageSize, d->m_image.format());
    QImage croppedImage = d->m_image.copy(d->cropRectangle);
    QPoint startingPoint(d->cropRectangle.topLeft());

    QPainter painter(&tempImage);
    painter.setCompositionMode(QPainter::CompositionMode_Source);
    painter.fillRect(tempImage.rect(), Qt::transparent);
    painter.drawImage(startingPoint, croppedImage);

    d->m_image = tempImage;

    options |= UpdateOption::UpdatePixmap;
  }
  if (options.testFlag(UpdateOption::UpdateGreyScale)) {
    qCDebug(dslmapGeoreferenceImageObject) << "UpdateGreyScale";
    if (d->isGreyScale) {

      int height = d->m_image.height();
      int width = d->m_image.width();
      for (int i = 0; i < height; i++) {
        QRgb* scanLine = (QRgb*)d->m_image.scanLine(i);
        for (int j = 0; j < width; j++) {
          QRgb pixel = *scanLine;
          uint ci = uint(qGray(pixel));
          *scanLine = qRgba(ci, ci, ci, qAlpha(pixel));
          ++scanLine;
        }
      }
    }

    options |= UpdateOption::UpdatePixmap;
  }

  if (options.testFlag(UpdateOption::UpdatePixmap)) {
    qCDebug(dslmapGeoreferenceImageObject) << "UpdatePixmap";
    // Create Pixmap from image";

    QPixmap mypix = QPixmap::fromImage(d->m_image);

    //d->m_projected_pixmap->setPixmap(mypix);
    
  }

  if (options.testFlag(UpdateOption::UpdateTransform)) {
    qCWarning(dslmapGeoreferenceImageObject) << "UpdateTransform";

    // Delete the old pixmap tiling items
    for (auto pixmap_item : d->m_projected_pixmaps) {
      pixmap_item->setParentItem(nullptr);
      delete pixmap_item;
    }

    d->m_projected_pixmaps.clear();

    // Get the projection transform

    auto transform = this->getProjectionTransform();

    d->m_projection_transform = transform;
    d->m_inverse_projection_transform = transform.inverted();
    qCInfo(dslmapGeoreferenceImageObject) << "Transform is " << transform;

    d->m_projected_pixmaps =
        ImageTilingHelper::getImageTiles(d->m_image, transform);
        
    for (auto pixmap_item : d->m_projected_pixmaps) {
      pixmap_item->setParentItem(this);
    }
  }
  return true;
}

QTransform
GeoreferenceImageObject::getProjectionTransform()
{
  Q_D(GeoreferenceImageObject);
  auto geo_transform = QVector<double>{ 0, 0, 0, 0, 0, 0 };
  auto transformError =
    d->m_projected_dataset->get()->GetGeoTransform(geo_transform.data());

  if (!transformError) {
    qCInfo(dslmapGeoreferenceImageObject) << "Using affine transformation";
  } else {
    qCInfo(dslmapGeoreferenceImageObject) << "Using gcp transformation";
    int gcpCount = d->m_projected_dataset->get()->GetGCPCount();
    qCInfo(dslmapGeoreferenceImageObject) << "gcp count is  " << gcpCount;
    auto gcps = d->m_projected_dataset->get()->GetGCPs();

    bool ok = true;
    int geotransform =
      GDALGCPsToGeoTransform(gcpCount, gcps, geo_transform.data(), ok);
    if (!ok) {
      qCDebug(dslmapGeoreferenceImageObject) << "ok is false ";
    }
    if (!geotransform) {
      qCDebug(dslmapGeoreferenceImageObject) << "geotransform is false ";
    }
  }

  // Create the transformation matrix
  auto ok = false;

  const auto transform = gdalGeometryToTransform(geo_transform, &ok);
  if (!ok) {
    qCWarning(dslmapGeoreferenceImageObject)
      << "Unable to calculate perspective transform given GDAL transform:"
      << geo_transform;
  }
  return transform;
}

void
GeoreferenceImageObject::setTransparentColor(QColor color)
{
  Q_D(GeoreferenceImageObject);

  qCInfo(dslmapGeoreferenceImageObject) << "Transparency changing ";

  d->prevTransColor = d->transColor;

  d->transColor = color;

  update(UpdateOption::UpdateTransparency);
}

bool
GeoreferenceImageObject::getGreyScaleState()
{
  Q_D(GeoreferenceImageObject);
  return d->isGreyScale;
}

void
GeoreferenceImageObject::toggleGreyScale(int state)
{
  Q_D(GeoreferenceImageObject);

  qCInfo(dslmapGeoreferenceImageObject) << "Grey scale state is " << state;

  state ? d->isGreyScale = true : d->isGreyScale = false;

  update(UpdateOption::UpdateGreyScale);
}

const char*
GeoreferenceImageObject::getFileMetaData(const QString& file)
{
// TESTING GDALDRIVER FUNCTIONS AND GETTING VARIOUS METADATA
// NOT USED IN PRODUCTION OR IN ANY MEANINGFUL WAY

#if 0
  GDALDataset* poDataset;
  GDALAllRegister();
  std::string str = file.toStdString();
  const char* p = str.c_str();
  poDataset = (GDALDataset*)GDALOpen(p, GA_ReadOnly);
  if (poDataset == NULL) {
  }
  QString des = poDataset->GetDriver()->GetDescription();
  // filetype

  QString meta = poDataset->GetDriver()->GetMetadataItem(GDAL_DMD_LONGNAME);

  auto colortable = poDataset->GetRasterBand(1)->GetColorTable();
  if (colortable) {
    qCInfo(dslmapGeoreferenceImageObject) << "we have a color table ";
  } else {
    qCInfo(dslmapGeoreferenceImageObject) << "no color table data found ";
  }
  // double geoTransform[6] = {0,0,0,0,0,0};
  // poDataset->SetGeoTransform(geoTransform);
  int gcpCount = poDataset->GetGCPCount();
  qCInfo(dslmapGeoreferenceImageObject) << "gcp count is  " << gcpCount;
  auto gcps = poDataset->GetGCPs();
  double adfGeoTransform[6];
  bool ok = false;
  int geotransform =
    GDALGCPsToGeoTransform(gcpCount, gcps, adfGeoTransform, ok);

  if (ok) {
    qCInfo(dslmapGeoreferenceImageObject) << "ok is success  ";
  } else {
    qCInfo(dslmapGeoreferenceImageObject) << "ok is fail  ";
  }
  if (geotransform) {
    qCInfo(dslmapGeoreferenceImageObject) << "geotransform success  ";
  } else {
    qCInfo(dslmapGeoreferenceImageObject) << "geotransform fail  ";
  }
  // const char *pszProjection = poDataset->GetAttrValue("PROJECTION");
  // qCInfo(dslmapGeoreferenceImageObject) << "get attr projection is   " <<
  // pszProjection;
  QString projref = poDataset->GetProjectionRef();

  auto error = poDataset->GetGeoTransform(adfGeoTransform);
  qCInfo(dslmapGeoreferenceImageObject) << "geotransform error is  " << error;

  auto gcpproj = poDataset->GetGCPProjection();
  qCInfo(dslmapGeoreferenceImageObject) << "GCP PROJECTION is  " << gcpproj;

  if (poDataset->GetGeoTransform(adfGeoTransform) == CE_None) {
    qCInfo(dslmapGeoreferenceImageObject)
      << "Origin = " << adfGeoTransform[0] << " " << adfGeoTransform[3]
      << "Pixel size is " << adfGeoTransform[1] << " " << adfGeoTransform[5];
  }
  // return projref;
  qCInfo(dslmapGeoreferenceImageObject)
    << "get file metadata projection ref is  " << poDataset->GetProjectionRef();
  // return poDataset->GetProjectionRef();

  qCInfo(dslmapGeoreferenceImageObject) << "File description is " << des;
  qCInfo(dslmapGeoreferenceImageObject) << "File metadata is " << meta;
  // qCInfo(dslmapGeoreferenceImageObject) << "File projection reference is " <<
  // projref;

  GDALRasterBand* poBand;
  int nBlockXSize, nBlockYSize;
  int bGotMin, bGotMax;
  double adfMinMax[2];
  poBand = poDataset->GetRasterBand(1);
  poBand->GetBlockSize(&nBlockXSize, &nBlockYSize);

  qCInfo(dslmapGeoreferenceImageObject) << "Block size X is " << nBlockXSize;
  qCInfo(dslmapGeoreferenceImageObject) << "Block size Y is " << nBlockYSize;
  printf("Block=%dx%d Type=%s, ColorInterp=%s\n", nBlockXSize, nBlockYSize,
         GDALGetDataTypeName(poBand->GetRasterDataType()),
         GDALGetColorInterpretationName(poBand->GetColorInterpretation()));

  adfMinMax[0] = poBand->GetMinimum(&bGotMin);
  adfMinMax[1] = poBand->GetMaximum(&bGotMax);
  if (adfMinMax[0] == 1)
    ;

  // auto myProj = poDataset->GetProjectionRef();
  // qCInfo(dslmapGeoreferenceImageObject) << "projection is : " << myProj;

  // dataset->setProjection(myProj)
#endif
  return "";
}

QRectF
GeoreferenceImageObject::boundingRect() const
{
  const Q_D(GeoreferenceImageObject);
  return d->m_projected_dataset_bounds;
}

void
GeoreferenceImageObject::paint(QPainter* painter,
                               const QStyleOptionGraphicsItem* option,
                               QWidget* widget)
{
  Q_UNUSED(painter);
  Q_UNUSED(option);
  Q_UNUSED(widget);
}

int
GeoreferenceImageObject::type() const
{
  return MapItemType::GeoreferenceImageObjectType;
}
//
const Proj*
GeoreferenceImageObject::datasetProjection() const
{

  const Q_D(GeoreferenceImageObject);
  if (!d->m_dataset) {
    return nullptr;
  }

  return d->m_dataset->projection().get();
}

void
GeoreferenceImageObject::setProjection(std::shared_ptr<const Proj> proj)
{

  Q_D(GeoreferenceImageObject);

  qCDebug(dslmapGeoreferenceImageObject,
          "Reaching image object setprojection.");

  if (!proj) {
    return;
  }

  if (!d->m_dataset) {
    d->m_proj = proj;
    return;
  }

  if (!d->m_proj) {
    qCDebug(dslmapGeoreferenceImageObject, "Dataset but not projection yet...");
  } else if (d->m_proj->isSame(*proj)) {
    qCDebug(dslmapGeoreferenceImageObject,
            "Projection is equivalent.. skipping warp.");
    return;
  }

  qCDebug(dslmapGeoreferenceImageObject,
          "Projection is not equivalent.. warping.");
  d->m_proj = std::move(proj);
  update(UpdateOption::UpdateProjection);
}

std::shared_ptr<const Proj>
GeoreferenceImageObject::projection() const noexcept
{
  const Q_D(GeoreferenceImageObject);
  return d->m_proj;
}

const Proj*
GeoreferenceImageObject::projectionPtr() const noexcept
{
  const Q_D(GeoreferenceImageObject);
  return d->m_proj.get();
}

} // dslmap
