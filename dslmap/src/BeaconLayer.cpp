/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 10/27/17.
//

#include "BeaconLayer.h"
#include "BeaconLayerPropertiesWidget.h"
#include "GeosUtils.h"
#include "MarkerItem.h"
#include "Proj.h"
#include "ShapeMarkerSymbol.h"
#include "WktMarkerSymbol.h"
#include <QAction>
#include <QGraphicsScene>
#include <QMenu>
#include <QSettings>

#include <BeaconLayer.h>
#include <memory>

namespace dslmap {
Q_LOGGING_CATEGORY(dslmapBeaconLayer, "dslmap.layer.beacon")

class BeaconLayerPrivate
{
public:
  explicit BeaconLayerPrivate()
    : m_trail_symbol(std::make_shared<ShapeMarkerSymbol>())
    , m_beacon_symbol(std::make_shared<ShapeMarkerSymbol>())
  {
  
  }

  ~BeaconLayerPrivate() = default;

  void updateBoundingRect(QGraphicsItem* parent)
  {
    auto rect = QRectF{};
    if (m_beacon_marker && m_beacon_visible) {
      rect = parent->mapRectFromItem(m_beacon_marker,
                                     m_beacon_marker->boundingRect());
    }

    if (m_trail_markers.isEmpty() == 0 || m_display_length == 0) {
      m_bounding_rect = rect;
      return;
    }

    if (m_trail_visible) {
      for (auto i = 0; i < m_trail_markers.count(); ++i) {
        if (i >= m_display_length) {
          break;
        }

        const auto& item = m_trail_markers.at(i);
        rect |= parent->mapRectFromItem(item, item->boundingRect());
      }
    }

    m_bounding_rect = rect;
  }

  void addMarker(QGraphicsItem* parent, MarkerItem* marker)
  {

    // Do nothing if the capacity is set to 0
    if (m_capacity == 0) {
      return;
    }

    // Remove the oldest marker if we're full.
    if (m_full) {
      auto old = m_trail_markers.takeLast();
      if (old->scene()) {
        old->scene()->removeItem(old);
      }

      old->setParentItem(nullptr);
      delete old;
    }

    // Add the new marker to the front.
    m_trail_markers.push_front(marker);

    const auto num_items = m_trail_markers.size();

    // Change visibility of the point that's shuffled back to the m_displayed
    // value
    if (m_display_length > 0 && num_items > m_display_length) {
      m_trail_markers.at(m_display_length)->setVisible(false);
    }

    // Set the full indicator.
    m_full = (num_items >= m_capacity);
    updateBoundingRect(parent);
  }

  std::shared_ptr<MarkerSymbol> m_trail_symbol;
  std::shared_ptr<MarkerSymbol> m_beacon_symbol;

  QRectF m_bounding_rect;

  QList<MarkerItem*> m_trail_markers;
  MarkerItem* m_beacon_marker = nullptr;

  int m_display_length = -1;
  int m_capacity = 5000;
  bool m_full = false;
  QPointF m_current_projected_pos = QPointF{};
  QPointF m_current_geodetic_pos = QPointF{};
  QPointF m_shift_offset = QPointF{};

  bool m_beacon_visible = true;
  bool m_trail_visible = true;
  QList<QMetaObject::Connection> m_beacon_symbol_connections;
  QList<QMetaObject::Connection> m_trail_symbol_connections;

  //default to false
  bool is_external_fix = false;
};

BeaconLayer::BeaconLayer(QGraphicsItem* parent)
  : MapLayer(parent)
  , d_ptr(new BeaconLayerPrivate)
{
  setFlag(ItemHasNoContents, true);
}

BeaconLayer::~BeaconLayer()
{
  delete d_ptr;
}

QList<const MarkerItem*> BeaconLayer::constTrailMarkers(){
    
  Q_D(BeaconLayer);
  //return QList<const MarkerItem*>(d->m_trail_markers.begin(), d->m_trail_markers.end()); // no such constructor
  QList<const MarkerItem*> view_markers;
  for (auto marker : d->m_trail_markers){
    view_markers.append(marker);
  }
  return view_markers;

}

void
BeaconLayer::showPropertiesDialog()
{
  auto widget = new BeaconLayerPropertiesWidget();
  widget->setLayer(this);
  widget->show();
}

void
BeaconLayer::updatePositionLonLat(qreal lon, qreal lat,
                                  const QDateTime& timestamp,
                                  const qreal* heading, const QString& label,
                                  const MarkerItem::Attributes& attributes)
{
  auto proj = projectionPtr();
  if (!proj) {
    return;
  }

  auto p = QVector<QPointF>{ { lon, lat } };
  const auto n = proj->transformFromLonLat(p, true);
  if (n != 1) {
    qCWarning(dslmapBeaconLayer) << "Could not transform lon/lat point:" << lon
                                 << lat
                                 << "to projection:" << proj->definition();
    return;
  }
  updatePosition(p.at(0).x(), p.at(0).y(), timestamp, heading, label,
                 attributes);
}

void
BeaconLayer::updatePosition(qreal x, qreal y, const QDateTime& timestamp,
                            const qreal* heading, const QString& label,
                            const MarkerItem::Attributes& attributes)
{
  Q_D(BeaconLayer);

  if (!d->m_beacon_marker) {
    d->m_beacon_marker = new MarkerItem;
    d->m_beacon_marker->setSymbol(d->m_beacon_symbol);
    d->m_beacon_marker->setVisible(beaconVisible());
    d->m_beacon_marker->setPos(
      mapFromScene(x + d->m_shift_offset.x(), y + d->m_shift_offset.y()));
    d->m_beacon_marker->setParentItem(this);
    d->m_beacon_marker->setAttribute(attributes);
    if (timestamp.isValid()) {
      d->m_beacon_marker->setAttribute("timestamp", timestamp);
    } else {
      d->m_beacon_marker->setAttribute("timestamp",
                                       QDateTime::currentDateTimeUtc());
    }
    d->m_beacon_marker->setLabelText(label);
    prepareGeometryChange();
    d->m_bounding_rect =
      mapRectFromItem(d->m_beacon_marker, d->m_beacon_marker->boundingRect());
    return;
  }

  if (d->m_display_length == 0) {
    d->m_beacon_marker->setPos(
      mapFromScene(x + d->m_shift_offset.x(), y + d->m_shift_offset.y()));
    d->m_beacon_marker->clearAttributes();
    d->m_beacon_marker->setAttribute(attributes);
    if (timestamp.isValid()) {
      d->m_beacon_marker->setAttribute("timestamp", timestamp);
    } else {
      d->m_beacon_marker->setAttribute("timestamp",
                                       QDateTime::currentDateTimeUtc());
    }
    d->m_beacon_marker->setLabelText(label);
    prepareGeometryChange();
    d->m_bounding_rect =
      mapRectFromItem(d->m_beacon_marker, d->m_beacon_marker->boundingRect());
    return;
  }
  
  auto marker = d->m_beacon_marker->clone();
  marker->setSymbol(d->m_trail_symbol);
  marker->setVisible(trailVisible());
  marker->setPos(d->m_beacon_marker->scenePos());
  marker->setParentItem(this);
  marker->stackBefore(d->m_beacon_marker);

  d->m_beacon_marker->setLabelText(label);

  // Set the timestamp attribute.
  marker->setAttribute(attributes);
  d->m_beacon_marker->setAttribute(attributes);
  if (timestamp.isValid()) {
    d->m_beacon_marker->setAttribute("timestamp", timestamp);
    marker->setAttribute("timestamp", timestamp);
  } else {
    d->m_beacon_marker->setAttribute("timestamp", QDateTime::currentDateTimeUtc());
    marker->setAttribute("timestamp", QDateTime::currentDateTimeUtc());
  }
  d->m_beacon_marker->setPos(
    mapFromScene(x + d->m_shift_offset.x(), y + d->m_shift_offset.y()));
  d->addMarker(this, marker);
  prepareGeometryChange();

  // Update heading.
  if (heading) {
    updateHeading(*heading);
  }

  d->m_current_projected_pos = d->m_beacon_marker->scenePos();
  x = d->m_current_projected_pos.x();
  y = d->m_current_projected_pos.y();
  emit currentPositionChanged(d->m_current_projected_pos);
  auto proj = projectionPtr();
  if (proj) {
    proj->transformToLonLat(1, &x, &y, true);
    d->m_current_geodetic_pos.setX(x);
    d->m_current_geodetic_pos.setY(y);
    emit currentLonLatPositionChanged(d->m_current_geodetic_pos);
  }
}

void
BeaconLayer::updateHeading(qreal heading)
{
  Q_D(BeaconLayer);
  if (!d->m_beacon_marker) {
    return;
  }
  d->m_beacon_marker->setRotation(-heading);
}

qreal
BeaconLayer::currentHeading() const noexcept
{
  const Q_D(BeaconLayer);
  if (!d->m_beacon_marker) {
    return {};
  }

  return -d->m_beacon_marker->rotation();
}

int
BeaconLayer::capacity() const
{
  const Q_D(BeaconLayer);
  return d->m_capacity;
}

int
BeaconLayer::displayedLength() const
{
  const Q_D(BeaconLayer);
  return d->m_display_length;
}

int
BeaconLayer::count() const noexcept
{
  const Q_D(BeaconLayer);
  return d->m_trail_markers.count();
}

void
BeaconLayer::setCapacity(int capacity)
{
  if (capacity < 0) {
    qCWarning(dslmapBeaconLayer) << "Ignoring negative capacity of" << capacity
                                 << "for BeaconLayer:" << name();
    return;
  }

  Q_D(BeaconLayer);
  if (capacity == d->m_capacity) {
    return;
  }

  if (capacity > d->m_capacity) {
    d->m_full = false;
    d->m_trail_markers.reserve(capacity);
    //removing a return statement that was here. Since it wouldn't update internal capacity. TJ 2022-02-22
  }

  if (capacity > d->m_trail_markers.size()) {
    d->m_full = false;
    d->m_trail_markers.reserve(capacity);
  }

  const auto to_discard = d->m_trail_markers.size() - capacity;
  auto _scene = scene();

  if (to_discard > 0) { 
  // check to explicitly indicate this logic block is for when we have trail fixes to discard

    if (_scene) {
      for (auto i = 0; i < to_discard; ++i) {
        auto item = d->m_trail_markers.takeLast();
        _scene->removeItem(item);
        item->setParentItem(nullptr);
        delete item;
      }
    } else {
      for (auto i = 0; i < to_discard; ++i) {
        auto item = d->m_trail_markers.takeLast();
        item->setParentItem(nullptr);
        delete item;
      }
    }
  } 
  d->updateBoundingRect(this);
  prepareGeometryChange();

  d->m_full = (d->m_trail_markers.size() >= capacity);
  d->m_capacity = capacity;
  emit capacityChanged(capacity);
}

void
BeaconLayer::setDisplayedLength(int length)
{
  Q_D(BeaconLayer);

  if (d->m_display_length == length) {
    return;
  }

  const auto visible = isVisible();
  if (length < 0) {
    for (auto& i : d->m_trail_markers) {
      i->setVisible(visible);
    }
    d->m_display_length = -1;
    return;
  }

  if (length > capacity()) {
    setCapacity(length);
  }

  auto index = 0;
  for (auto& i : d->m_trail_markers) {
    i->setVisible(visible && index < length);
    ++index;
  }

  d->m_display_length = length;
  d->updateBoundingRect(this);
  prepareGeometryChange();
  emit displayedLengthChanged(length);
}

std::shared_ptr<MarkerSymbol>
BeaconLayer::beaconSymbol() const
{
  const Q_D(BeaconLayer);
  return d->m_beacon_symbol;
}

MarkerSymbol*
BeaconLayer::beaconSymbolPtr() const
{
  const Q_D(BeaconLayer);
  return d->m_beacon_symbol.get();
}

void
BeaconLayer::setBeaconSymbol(std::shared_ptr<MarkerSymbol> symbol)
{
  Q_D(BeaconLayer);
  foreach (auto& con, d->m_beacon_symbol_connections) {
    QObject::disconnect(con);
  }
  d->m_beacon_symbol_connections.clear();

  auto ptr = symbol.get();

  if (d->m_beacon_marker) {
    d->m_beacon_marker->setSymbol(symbol);
    d->m_beacon_marker->update();
  }

  d->m_beacon_symbol = std::move(symbol);

  d->m_beacon_symbol_connections
    << QObject::connect(ptr, &MarkerSymbol::symbolChanged, [this]() {
         Q_D(BeaconLayer);
         d->updateBoundingRect(this);
         prepareGeometryChange();
       });

  // Color udpate signals
  d->m_beacon_symbol_connections
    << QObject::connect(ptr, &MarkerSymbol::outlineColorChanged, this,
                        &BeaconLayer::beaconOutlineColorChanged);

  d->m_beacon_symbol_connections
    << QObject::connect(ptr, &MarkerSymbol::fillColorChanged, this,
                        &BeaconLayer::beaconFillColorChanged);

  // Style update signals
  d->m_beacon_symbol_connections
    << QObject::connect(ptr, &MarkerSymbol::fillStyleChanged, this,
                        &BeaconLayer::beaconFillStyleChanged);

  d->m_beacon_symbol_connections
    << QObject::connect(ptr, &MarkerSymbol::outlineStyleChanged, this,
                        &BeaconLayer::beaconOutlineStyleChanged);

  d->updateBoundingRect(this);
  prepareGeometryChange();
}

std::shared_ptr<MarkerSymbol>
BeaconLayer::trailSymbol() const
{
  const Q_D(BeaconLayer);
  return d->m_trail_symbol;
}

MarkerSymbol*
BeaconLayer::trailSymbolPtr() const
{
  const Q_D(BeaconLayer);
  return d->m_trail_symbol.get();
}

void
BeaconLayer::setTrailSymbol(std::shared_ptr<MarkerSymbol> symbol)
{
  if (!symbol) {
    return;
  }

  Q_D(BeaconLayer);
  foreach (auto& con, d->m_trail_symbol_connections) {
    QObject::disconnect(con);
  }

  d->m_trail_symbol_connections.clear();

  const auto ptr = symbol.get();

  for (auto& i : d->m_trail_markers) {
    i->setSymbol(symbol);
    i->update();
  }

  d->m_trail_symbol = std::move(symbol);

  d->m_trail_symbol_connections
    << QObject::connect(ptr, &MarkerSymbol::symbolChanged, [this]() {
         Q_D(BeaconLayer);
         d->updateBoundingRect(this);
         prepareGeometryChange();
       });

  // Color udpate signals
  d->m_trail_symbol_connections
    << QObject::connect(ptr, &MarkerSymbol::outlineColorChanged, this,
                        &BeaconLayer::trailOutlineColorChanged);

  d->m_trail_symbol_connections
    << QObject::connect(ptr, &MarkerSymbol::fillColorChanged, this,
                        &BeaconLayer::trailFillColorChanged);

  // Style update signals
  d->m_trail_symbol_connections
    << QObject::connect(ptr, &MarkerSymbol::fillStyleChanged, this,
                        &BeaconLayer::trailFillStyleChanged);

  d->m_trail_symbol_connections
    << QObject::connect(ptr, &MarkerSymbol::outlineStyleChanged, this,
                        &BeaconLayer::trailOutlineStyleChanged);

  d->updateBoundingRect(this);
  prepareGeometryChange();
}

bool
BeaconLayer::saveSettings(QSettings& settings)
{

  Q_D(BeaconLayer);
  settings.setValue("name", name());
  settings.setValue("trail/capacity", capacity());
  settings.setValue("trail/length", displayedLength());
  if (d->m_beacon_symbol) {
    d->m_beacon_symbol->saveSettings(settings, "beacon/");
  }

  if (d->m_trail_symbol) {
    d->m_trail_symbol->saveSettings(settings, "trail/");
  }

  return true;
}

std::unique_ptr<BeaconLayer>
BeaconLayer::fromSettings(QSettings& settings)
{
  auto layer = std::make_unique<BeaconLayer>();
  layer->loadSettings(settings);
  return layer;
}

bool
BeaconLayer::loadSettings(QSettings& settings)
{
  setName(settings.value("name", "").toString());
  auto path = QPainterPath{};

  if (settings.contains("beacon/shape_wkt")) {
    setBeaconSymbol(WktMarkerSymbol::fromSettings(settings, "beacon/"));
  } else {
    setBeaconSymbol(ShapeMarkerSymbol::fromSettings(settings, "beacon/"));
  }

  if (settings.contains("trail/shape_wkt")) {
    setTrailSymbol(WktMarkerSymbol::fromSettings(settings, "trail/"));
  } else {
    setTrailSymbol(ShapeMarkerSymbol::fromSettings(settings, "trail/"));
  }
  setCapacity(settings.value("trail/capacity", 1000).toInt());
  setDisplayedLength(settings.value("trail/length", 1000).toInt());

  return true;
}

void
BeaconLayer::setProjection(std::shared_ptr<const Proj> proj)
{

  if (!proj) {
    return;
  }

  auto p = projectionPtr();

  if (proj->isSame(*p)) {
    return;
  }

  Q_D(BeaconLayer);
  if (d->m_beacon_marker) {
    auto pos = d->m_beacon_marker->scenePos();
    auto x = pos.x();
    auto y = pos.y();
    p->transformTo(*proj, 1, &x, &y);
    d->m_beacon_marker->setPos(mapFromScene(x, y));
    // d->m_bounding_rect =
    //   mapRectFromItem(d->m_beacon_marker,
    //   d->m_beacon_marker->boundingRect());
  }

  for (auto& i : d->m_trail_markers) {
    auto pos = i->scenePos();
    auto x = pos.x();
    auto y = pos.y();

    p->transformTo(*proj, 1, &x, &y);

    pos = mapFromScene(x, y);
    i->setPos(pos);
  }
  d->updateBoundingRect(this);
  prepareGeometryChange();
  MapLayer::setProjection(proj);
  emit currentPositionChanged(currentPosition());
  emit currentLonLatPositionChanged(currentLonLatPosition());
}

void
BeaconLayer::setShiftOffset(const QPointF shift)
{
  auto old_shift = shiftOffset();
  if (shift == old_shift) {
    return;
  }

  Q_D(BeaconLayer);
  d->m_shift_offset = shift;
  if (d->m_beacon_marker) {
    auto pos = d->m_beacon_marker->scenePos();
    auto x = pos.x() - old_shift.x() + d->m_shift_offset.x();
    auto y = pos.y() - old_shift.y() + d->m_shift_offset.y();
    d->m_beacon_marker->setPos(mapFromScene(x, y));
  }

  for (auto& i : d->m_trail_markers) {
    auto pos = i->scenePos();
    auto x = pos.x() - old_shift.x() + d->m_shift_offset.x();
    auto y = pos.y() - old_shift.y() + d->m_shift_offset.y();

    pos = mapFromScene(x, y);
    i->setPos(pos);
  }
  d->updateBoundingRect(this);
  prepareGeometryChange();
  emit currentPositionChanged(currentPosition());
  emit currentLonLatPositionChanged(currentLonLatPosition());
  emit shiftOffsetChanged(d->m_shift_offset);
}

QRectF
BeaconLayer::boundingRect() const
{
  const Q_D(BeaconLayer);
  return d->m_bounding_rect;
}

void
BeaconLayer::paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*)
{
}

QPointF
BeaconLayer::currentPosition() const noexcept
{
  const Q_D(BeaconLayer);
  if (!d->m_beacon_marker) {
    return {};
  }

  return d->m_beacon_marker->scenePos();
}

QPointF
BeaconLayer::currentLonLatPosition() const noexcept
{
  auto proj = projectionPtr();
  if (!proj) {
    return {};
  }
  auto p = QVector<QPointF>{ currentPosition() };
  proj->transformToLonLat(p, true);
  return p.at(0);
}

void
BeaconLayer::clearTrail()
{
  Q_D(BeaconLayer);
  if (d->m_trail_markers.isEmpty()) {
    return;
  }

  for (auto i : d->m_trail_markers) {
    auto scene = i->scene();
    if (scene) {
      scene->removeItem(i);
    }
  }
  qDeleteAll(d->m_trail_markers);
  d->m_trail_markers.clear();
  d->updateBoundingRect(this);
  prepareGeometryChange();
}

bool
BeaconLayer::trailVisible() const noexcept
{
  const Q_D(BeaconLayer);
  return d->m_trail_visible;
}
bool
BeaconLayer::beaconVisible() const noexcept
{
  const Q_D(BeaconLayer);
  return d->m_beacon_visible;
}

void
BeaconLayer::setBeaconVisible(bool visible)
{
  if (visible == beaconVisible()) {
    return;
  }

  Q_D(BeaconLayer);
  d->m_beacon_visible = visible;
  if (!d->m_beacon_marker) {
    return;
  }

  d->m_beacon_marker->setVisible(visible);
  d->updateBoundingRect(this);
  prepareGeometryChange();
}

void
BeaconLayer::setTrailVisible(bool visible)
{
  if (visible == trailVisible()) {
    return;
  }

  Q_D(BeaconLayer);
  d->m_trail_visible = visible;
  if (!visible) {
    for (auto& item : d->m_trail_markers) {
      item->setVisible(false);
    }
  } else if (d->m_display_length < 0) {
    for (auto& item : d->m_trail_markers) {
      item->setVisible(true);
    }
  }

  else {
    for (auto i = 0; i < d->m_trail_markers.count(); ++i) {
      d->m_trail_markers.at(i)->setVisible(i < d->m_display_length);
    }
  }
  emit trailVisibilityChanged(visible);
  prepareGeometryChange();
  d->updateBoundingRect(this);
}

QColor
BeaconLayer::trailOutlineColor() const
{
  const Q_D(BeaconLayer);
  if (d->m_trail_symbol) {
    return d->m_trail_symbol->outlineColor();
  }
  return {};
}
QColor
BeaconLayer::trailFillColor() const
{
  const Q_D(BeaconLayer);
  if (d->m_trail_symbol) {
    return d->m_trail_symbol->fillColor();
  }
  return {};
}
QColor
BeaconLayer::trailLabelColor() const
{
  const Q_D(BeaconLayer);
  if (d->m_trail_symbol) {
    return d->m_trail_symbol->labelColor();
  }
  return {};
}
bool
BeaconLayer::trailLabelVisible() const
{
  const Q_D(BeaconLayer);
  if (d->m_trail_symbol) {
    return d->m_trail_symbol->isLabelVisible();
  }
  return false;
}
QColor
BeaconLayer::beaconOutlineColor() const
{
  const Q_D(BeaconLayer);
  if (d->m_beacon_symbol) {
    return d->m_beacon_symbol->outlineColor();
  }
  return {};
}
QColor
BeaconLayer::beaconFillColor() const
{
  const Q_D(BeaconLayer);
  if (d->m_beacon_symbol) {
    return d->m_beacon_symbol->fillColor();
  }
  return {};
}
QColor
BeaconLayer::beaconLabelColor() const
{
  const Q_D(BeaconLayer);
  if (d->m_beacon_symbol) {
    return d->m_beacon_symbol->labelColor();
  }
  return {};
}
bool
BeaconLayer::beaconLabelVisible() const
{
  const Q_D(BeaconLayer);
  if (d->m_beacon_symbol) {
    return d->m_beacon_symbol->isLabelVisible();
  }
  return false;
}
void
BeaconLayer::setTrailOutlineColor(const QColor& color)
{

  Q_D(BeaconLayer);
  if (!color.isValid() || !d->m_trail_symbol) {
    return;
  }

  d->m_trail_symbol->setOutlineColor(color);
}
void
BeaconLayer::setTrailFillColor(const QColor& color)
{
  Q_D(BeaconLayer);
  if (!color.isValid() || !d->m_trail_symbol) {
    return;
  }

  d->m_trail_symbol->setFillColor(color);
}
void
BeaconLayer::setTrailLabelColor(const QColor& color)
{
  Q_D(BeaconLayer);
  if (!color.isValid() || !d->m_trail_symbol) {
    return;
  }

  d->m_trail_symbol->setLabelColor(color);
}
void
BeaconLayer::setBeaconLabelVisible(const bool visible)
{
  const Q_D(BeaconLayer);
  if (d->m_beacon_symbol) {
    return d->m_beacon_symbol->setLabelVisible(visible);
  }
  return;
}
void
BeaconLayer::setBeaconOutlineColor(const QColor& color)
{
  Q_D(BeaconLayer);
  if (!color.isValid() || !d->m_beacon_symbol) {
    return;
  }

  d->m_beacon_symbol->setLabelColor(color);
}
void
BeaconLayer::setBeaconFillColor(const QColor& color)
{
  Q_D(BeaconLayer);
  if (!color.isValid() || !d->m_beacon_symbol) {
    return;
  }

  d->m_beacon_symbol->setFillColor(color);
}
void
BeaconLayer::setBeaconLabelColor(const QColor& color)
{
  Q_D(BeaconLayer);
  if (!color.isValid() || !d->m_beacon_symbol) {
    return;
  }

  d->m_beacon_symbol->setLabelColor(color);
}
void
BeaconLayer::setTrailLabelVisible(const bool visible)
{
  const Q_D(BeaconLayer);
  if (d->m_trail_symbol) {
    return d->m_trail_symbol->setLabelVisible(visible);
  }
  return;
}
Qt::PenStyle
BeaconLayer::trailOutlineStyle() const
{
  const Q_D(BeaconLayer);
  if (d->m_trail_symbol) {
    return d->m_trail_symbol->outlineStyle();
  }
  return Qt::NoPen;
}
Qt::BrushStyle
BeaconLayer::trailFillStyle() const
{
  const Q_D(BeaconLayer);
  if (d->m_trail_symbol) {
    return d->m_trail_symbol->fillStyle();
  }
  return Qt::NoBrush;
}
Qt::PenStyle
BeaconLayer::beaconOutlineStyle() const
{
  const Q_D(BeaconLayer);
  if (d->m_beacon_symbol) {
    return d->m_beacon_symbol->outlineStyle();
  }
  return Qt::NoPen;
}
Qt::BrushStyle
BeaconLayer::beaconFillStyle() const
{
  const Q_D(BeaconLayer);
  if (d->m_beacon_symbol) {
    return d->m_beacon_symbol->fillStyle();
  }
  return Qt::NoBrush;
}

QPointF
BeaconLayer::shiftOffset() const noexcept
{
  const Q_D(BeaconLayer);
  return d->m_shift_offset;
}

void
BeaconLayer::setTrailFillStyle(Qt::BrushStyle style)
{
  auto ptr = trailSymbolPtr();
  if (!ptr || ptr->fillStyle() == style) {
    return;
  }

  ptr->setFillStyle(style);
}

void
BeaconLayer::setTrailOutlineStyle(Qt::PenStyle style)
{
  auto ptr = trailSymbolPtr();
  if (!ptr || ptr->outlineStyle() == style) {
    return;
  }

  ptr->setOutlineStyle(style);
}
void
BeaconLayer::setBeaconFillStyle(Qt::BrushStyle style)
{
  auto ptr = beaconSymbolPtr();
  if (!ptr || ptr->fillStyle() == style) {
    return;
  }

  ptr->setFillStyle(style);
}
void
BeaconLayer::setBeaconOutlineStyle(Qt::PenStyle style)
{
  auto ptr = beaconSymbolPtr();
  if (!ptr || ptr->outlineStyle() == style) {
    return;
  }

  ptr->setOutlineStyle(style);
}

bool 
BeaconLayer::isExternalFix(){
  const Q_D(BeaconLayer);
  return d->is_external_fix;
}

void 
BeaconLayer::setAsExternalFix(bool fix){
  Q_D(BeaconLayer);
  d->is_external_fix = fix;
}

} // namespace