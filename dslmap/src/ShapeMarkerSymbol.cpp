/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ShapeMarkerSymbol.h"
#include "MarkerItem.h"

#include <QDebug>
#include <QPainter>
#include <QSettings>
#include <QStyleOptionGraphicsItem>

namespace dslmap {

class ShapeMarkerSymbolPrivate
{

public:
  QPainterPath m_path;
  ShapeMarkerSymbol::Shape m_shape = ShapeMarkerSymbol::Shape::Invalid;
  double m_shape_size = 10;
};

ShapeMarkerSymbol::ShapeMarkerSymbol(QObject* parent)
  : MarkerSymbol(parent)
  , d_ptr(new ShapeMarkerSymbolPrivate)
{
}

ShapeMarkerSymbol::ShapeMarkerSymbol(ShapeMarkerSymbol::Shape shape,
                                     double size, QObject* parent)
  : ShapeMarkerSymbol(parent)
{
  if (size < 0) {
    return;
  }

  const auto path = pathFromShape(shape, size);

  Q_D(ShapeMarkerSymbol);
  d->m_shape = shape;
  d->m_path = std::move(path);
  d->m_shape_size = size;

  connect(this, &ShapeMarkerSymbol::shapeChanged, this,
          &ShapeMarkerSymbol::symbolChanged);
  connect(this, &ShapeMarkerSymbol::sizeChanged, this,
          &ShapeMarkerSymbol::symbolChanged);
}

ShapeMarkerSymbol::~ShapeMarkerSymbol() = default;

QPainterPath
ShapeMarkerSymbol::pathFromShape(ShapeMarkerSymbol::Shape shape, double size)
{
  QPainterPath path{};

  switch (shape) {
    case Shape::Circle:
      path.addEllipse({ 0, 0 }, size / 2, size / 2);
      break;
    case Shape::Square:
      path.addRect(-size / 2, -size / 2, size, size);
      break;
    case Shape::Diamond:
      path.moveTo(-size / 2, 0);
      path.lineTo(0, size / 2);
      path.lineTo(size / 2, 0);
      path.lineTo(0, -size / 2);
      path.closeSubpath();
      break;
    case Shape::Cross:
      path.moveTo(0, size / 2);
      path.lineTo(0, -size / 2);
      path.moveTo(-size / 2, 0);
      path.lineTo(size / 2, 0);
      break;
    case Shape::X:
      path.moveTo(-size / 2, size / 2);
      path.lineTo(size / 2, -size / 2);
      path.moveTo(-size / 2, -size / 2);
      path.lineTo(size / 2, size / 2);
      break;
    case Shape::Arrow:
      //as drawn. points up in default pos
      path.moveTo(-size / 2, 0);
      path.lineTo(0, -size / 2);
      path.lineTo(size / 2, 0);

      path.moveTo(0, size / 2);
      path.lineTo(0, -size / 2);
      break;    
    case Shape::Invalid:
      break;
  }

  return path;
}

bool
ShapeMarkerSymbol::operator==(const ShapeMarkerSymbol& other) const
{
  if (typeid(*this) != typeid(other)) {
    return false;
  }

  return isEqual(other);
}

bool
ShapeMarkerSymbol::operator!=(const ShapeMarkerSymbol& other) const
{
  if (typeid(*this) != typeid(other)) {
    return true;
  }
  return !isEqual(other);
}

const QPainterPath&
ShapeMarkerSymbol::path() const
{
  const Q_D(ShapeMarkerSymbol);
  return d->m_path;
}

std::unique_ptr<ShapeMarkerSymbol>
ShapeMarkerSymbol::clone() const
{
  auto other = std::make_unique<ShapeMarkerSymbol>(shape(), size());
  other->m_clonePaintSettingsFrom(*this);
  return other;
}

ShapeMarkerSymbol::Shape
ShapeMarkerSymbol::shape() const
{
  const Q_D(ShapeMarkerSymbol);
  return d->m_shape;
}

void
ShapeMarkerSymbol::setShape(Shape shape_)
{
  if (shape_ == Shape::Invalid) {
    return;
  }

  if (shape_ == shape()) {
    return;
  }

  auto size_ = size();
  if (size_ <= 0) {
    size_ = 10;
  }

  setShape(shape_, size_);
}

void
ShapeMarkerSymbol::setShape(Shape shape_, double size_)
{
  if (shape_ == Shape::Invalid) {
    return;
  }

  auto path = pathFromShape(shape_, size_);

  Q_D(ShapeMarkerSymbol);
  d->m_path = path;

  if (d->m_shape != shape_) {
    d->m_shape = shape_;
    emit shapeChanged(shape_);
  }

  if (d->m_shape_size != size_) {
    d->m_shape_size = size_;
    emit sizeChanged(size_);
  }
}

qreal
ShapeMarkerSymbol::size() const
{
  const Q_D(ShapeMarkerSymbol);
  return d->m_shape_size;
}

void
ShapeMarkerSymbol::setSize(qreal size_)
{
  if (size_ < 0) {
    return;
  }

  if (size_ == size()) {
    return;
  }

  const auto shape_ = shape();
  if (shape_ == Shape::Invalid) {
    return;
  }

  const auto path = pathFromShape(shape_, size_);

  Q_D(ShapeMarkerSymbol);
  d->m_path = path;
  d->m_shape_size = size_;
  emit sizeChanged(size_);
}

const QString
toString(ShapeMarkerSymbol::Shape shape)
{
  switch (shape) {
    case ShapeMarkerSymbol::Shape::Circle:
      return "circle";
    case ShapeMarkerSymbol::Shape::Square:
      return "square";
    case ShapeMarkerSymbol::Shape::Diamond:
      return "diamond";
    case ShapeMarkerSymbol::Shape::Cross:
      return "cross";
    case ShapeMarkerSymbol::Shape::X:
      return "x";
    case ShapeMarkerSymbol::Shape::Arrow:
      return "arrow";
    case ShapeMarkerSymbol::Shape::Invalid:
      return "invalid";
  }
  return "invalid";
}

ShapeMarkerSymbol::Shape
ShapeMarkerSymbol::shapeFromString(QString shape)
{
  shape = shape.toLower().trimmed();
  if (shape == "circle") {
    return ShapeMarkerSymbol::Shape::Circle;
  }
  if (shape == "square") {
    return ShapeMarkerSymbol::Shape::Square;
  }
  if (shape == "diamond") {
    return ShapeMarkerSymbol::Shape::Diamond;
  }
  if (shape == "cross") {
    return ShapeMarkerSymbol::Shape::Cross;
  }
  if (shape == "x") {
    return ShapeMarkerSymbol::Shape::X;
  }
   if (shape == "arrow") {
    return ShapeMarkerSymbol::Shape::Arrow;
  }

  return ShapeMarkerSymbol::Shape::Invalid;
}

std::unique_ptr<ShapeMarkerSymbol>
ShapeMarkerSymbol::fromSettings(const QSettings& settings, QString prefix)
{
  auto shape = shapeFromString(settings.value(prefix + "shape").toString());
  if (shape == Shape::Invalid) {
    shape = Shape::Circle;
  }

  const auto size = settings.value(prefix + "size", 10).toInt();
  auto symbol = std::make_unique<ShapeMarkerSymbol>(shape, size);
  auto b_ptr = static_cast<MarkerSymbol*>(symbol.get());
  b_ptr->loadSettings(settings, prefix);
  return symbol;
}

void
ShapeMarkerSymbol::saveSettings(QSettings& settings, QString prefix)
{
  const auto shape_ = shape();
  if (shape_ != Shape::Invalid) {
    settings.setValue(prefix + "shape", toString(shape_));
  }

  settings.setValue(prefix + "size", size());
  MarkerSymbol::saveSettings(settings, prefix);
}

bool
ShapeMarkerSymbol::isEqual(const ShapeMarkerSymbol& other) const
{
  return shape() == other.shape() && size() == other.size() &&
         MarkerSymbol::isEqual(other);
}
}