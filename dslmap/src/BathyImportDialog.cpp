/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
// Created by jvaccaro on 10-12-18

#include "BathyImportDialog.h"
#include "BathyLayer.h"
#include "MapScene.h"
#include "ui_BathyImportDialog.h"
#include <QFileDialog>

namespace dslmap {

BathyImportDialog::BathyImportDialog(QWidget* parent)
  : QDialog(parent)
  , ui(new Ui::BathyImportDialog)
{
  ui->setupUi(this);
  connect(ui->okBox, &QDialogButtonBox::accepted, this,
          &BathyImportDialog::accept);
  connect(ui->okBox, &QDialogButtonBox::rejected, this,
          &BathyImportDialog::reject);
  connect(ui->fileBtn, &QPushButton::clicked, [=]() {
    auto fn =
      QFileDialog::getOpenFileName(this,
                                   "Select Bathymetry File...",
                                   "",
                                   "Bathymetry files (*.grd);;ALL files (*.*)");

    ui->fileEdit->setText(fn);
  });
}

BathyImportDialog::~BathyImportDialog()
{
  delete ui;
}

void
BathyImportDialog::setFilename(const QString& name) noexcept
{
  ui->fileEdit->setText(name);
}

QString
BathyImportDialog::filename()
{
  return ui->fileEdit->text();
}

QString
BathyImportDialog::layername()
{
  return filename();
}
} // namespace dslmap
