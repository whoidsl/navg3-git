/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ContourLayer.h"
#include "ContourLayerPropertiesWidget.h"
#include "ContourLine.h"
#include "GdalColorGradient.h"
#include "GdalDataset.h"
#include "Proj.h"

#include <QMap>
#include <QMetaObject>
#include <QPen>
#include <QProcess>
#include <QSet>
#include <QTemporaryDir>
#include <QMenu>

#include <gdal/gdal_priv.h>
#include <memory>

namespace dslmap {
Q_LOGGING_CATEGORY(dslmapContourLayer, "dslmap.layer.contour")

class ContourLayerPrivate
{
public:
  std::shared_ptr<GdalColorGradient> m_gradient;
  QRectF m_bounding_rect;
  QSet<qreal> m_contour_levels;
  QList<ContourLine*> m_contour_lines;
  QMetaObject::Connection m_gradient_connection;
  QMenu* m_menu;
};

ContourLayer::ContourLayer(QGraphicsItem* parent)
  : MapLayer(parent)
  , d_ptr(new ContourLayerPrivate)
{
  Q_D(ContourLayer);
  setFlag(ItemHasNoContents, true);
  d->m_menu = MapLayer::contextMenu();
  auto underlay_action = d->m_menu->addAction(QStringLiteral("Save Contour Underlay in Settings"));
  connect(underlay_action, &QAction::triggered, this,
          [=]() { saveUnderlay(); });
}

ContourLayer::~ContourLayer() = default;

void
ContourLayer::showPropertiesDialog()
{
  auto widget = new ContourLayerPropertiesWidget();
  widget->setLayer(this);
  widget->show();
}

bool
ContourLayer::saveSettings(QSettings& settings)
{
  Q_UNUSED(settings)
  return false;
}

bool
ContourLayer::loadSettings(QSettings& settings)
{
  Q_UNUSED(settings)
  return false;
}

void
ContourLayer::updateContours()
{
  QPen pen;
  Q_D(ContourLayer);
  for (auto contour : d->m_contour_lines) {
    const auto value = contour->contourLevel();
    const auto color = d->m_gradient->colorAt(value);
    pen = contour->pen();
    pen.setColor(color);
    contour->setPen(pen);
  }
}

void
ContourLayer::setGradient(std::shared_ptr<GdalColorGradient> gradient)
{
  if (!gradient) {
    return;
  }

  Q_D(ContourLayer);
  d->m_gradient.swap(gradient);
  if (d->m_gradient_connection) {
    disconnect(d->m_gradient_connection);
  }

  d->m_gradient_connection = connect(
    d->m_gradient.get(), &dslmap::GdalColorGradient::gdalColorTableChanged,
    this, &ContourLayer::updateContours);
  updateContours();
}

std::shared_ptr<dslmap::GdalColorGradient>
ContourLayer::gradient() const
{
  const Q_D(ContourLayer);
  return d->m_gradient;
}

QSet<qreal>
ContourLayer::contours() const
{
  const Q_D(ContourLayer);
  return d->m_contour_levels;
}

std::unique_ptr<ContourLayer>
ContourLayer::fromShapefile(const QString& filename, QString elevation_attribute,
                            int id)
{
 
  const auto stdstr_filename = filename.toStdString();
  
  #if GDAL_VERSION_MAJOR < 2
    OGRDataSourceH hDS = OGROpen(stdstr_filename.data(), FALSE, NULL);
  #else 
    GDALDatasetH hDS = GDALOpenEx(stdstr_filename.data(), GDAL_OF_VERBOSE_ERROR, NULL, NULL, NULL);
  #endif

  auto error = CPLGetLastErrorMsg();

  if (!hDS) {
    qCWarning(dslmapContourLayer, "Unable to open shapefile: %s",
              stdstr_filename.data());
    qCWarning(dslmapContourLayer) << "Error returned: " << error;
    return {};
  }
  else {
      qDebug(dslmapContourLayer) << "Open shape file successful";
  }

  OGRLayerH hLayer = OGR_DS_GetLayer(hDS, id);
  if (!hLayer) {
    OGR_DS_Destroy(hDS);
    qCWarning(dslmapContourLayer,
              "Unable to open layer number %d in shapefile: %s", id,
              stdstr_filename.data());
    return {};
  }

  OGRFeatureH hFeature;
  OGR_L_ResetReading(hLayer);

  const auto stdstr_elevation_attribute = elevation_attribute.toStdString();
  auto layer = std::make_unique<ContourLayer>();
  auto pen = QPen{};
  pen.setWidth(2);
  pen.setCosmetic(true);
  pen.setColor(Qt::darkRed);

  auto feature = 0;
  while ((hFeature = OGR_L_GetNextFeature(hLayer)) != NULL) {
    ++feature;

    OGRGeometryH hGeometry = OGR_F_GetGeometryRef(hFeature);
    if (hGeometry == NULL) {
      continue;
    }

    const auto geom_type = OGR_G_GetGeometryType(hGeometry);
    if (geom_type != wkbLineString) {
      qCWarning(dslmapContourLayer,
                "Wrong geometry type for contour file.  Expected %d, found %d",
                wkbLineString, geom_type);
      OGR_F_Destroy(hFeature);
      OGR_DS_Destroy(hDS);
      return {};
    }

    const auto elevation_attribute_index =
      OGR_F_GetFieldIndex(hFeature, stdstr_elevation_attribute.data());
    const auto contour_depth =
      OGR_F_GetFieldAsDouble(hFeature, elevation_attribute_index);

    if (elevation_attribute_index < 0) {
      qCWarning(dslmapContourLayer, "Feature %d has no attribute named %s",
                feature, stdstr_elevation_attribute.data());
      OGR_F_Destroy(hFeature);
      continue;
    }
    QPainterPath path;

    const auto points_count = OGR_G_GetPointCount(hGeometry);
    auto x = OGR_G_GetX(hGeometry, 0);
    auto y = OGR_G_GetY(hGeometry, 0);
    path.moveTo(x, y);
    for (auto i = 1; i < points_count; ++i) {
      x = OGR_G_GetX(hGeometry, i);
      y = OGR_G_GetY(hGeometry, i);
      path.lineTo(x, y);
    }

    auto item = new ContourLine;
    item->setPath(path);
    item->setPen(pen);
    item->setParentItem(layer.get());
    item->setToolTip(QString::number(contour_depth));
    item->setContourLevel(contour_depth);
    layer->d_ptr->m_contour_lines.append(item);
    layer->d_ptr->m_contour_levels.insert(contour_depth);

    OGR_F_Destroy(hFeature);
  }
  layer->d_ptr->m_bounding_rect = layer->childrenBoundingRect();

  OGR_DS_Destroy(hDS);

  auto gradient = std::make_unique<GdalColorGradient>();
  gradient->setAbsolute(true);
  const auto minmax = layer->minMaxValues();
  gradient->setMin(minmax.first);
  gradient->setMax(minmax.second);
  qDebug() << gradient->stops();
  layer->setGradient(std::move(gradient));

  return layer;
}

std::unique_ptr<ContourLayer>
ContourLayer::fromGrdFile(const QString& filename, int interval, int band,
                          std::shared_ptr<Proj> proj)
{
  
  // Figure out what to do for a projection

  auto grd_dataset = std::make_unique<GdalDataset>(filename);

  if (!grd_dataset->open()) {
    qCWarning(dslmapContourLayer) << "Could not open GRD file: " << filename;
    return {};
  }

  auto grd_proj = std::shared_ptr<Proj>{};
  if (!proj) {
    if (grd_proj) {
      qCInfo(dslmapContourLayer)
        << "Contour layer will use existing GRD projection: "
        << grd_proj->definition();
      proj = grd_proj;
    } else {
      qCInfo(dslmapContourLayer)
        << "No projection for GRD file, assuming WGS84";
      proj = Proj::fromDefinition(PROJ_WGS84_DEFINITION);
    }
  } else {
    qCInfo(dslmapContourLayer) << "Using user-defined projection:"
                               << proj->definition();
  }

  QTemporaryDir shape_file;

   if (!shape_file.isValid()) {
     qCWarning(dslmapContourLayer)
       << "Unable to open temporary file for contour layer.";
     return {};
   }

  // const auto dst_proj4 = proj.definition();

  auto process = std::make_unique<QProcess>();
  const auto attribute_name = QString{ "elevation" };

  process->setProgram("gdal_contour");

  auto args = QStringList{ 
                           "-a",
                           attribute_name,
                           "-i",
                           QString::number(interval),
                           "-b",
                           QString::number(band),
                           "-f", "ESRI Shapefile",     
                           filename,
                           shape_file.path() };

  // #if GDAL_VERSION_MAJOR < 2
  //   const auto driver_short_name =
  //     GDALGetDriverShortName(d->m_dataset->GetDriver());
  // #else
  //const auto driver_short_name = grd_dataset->get()->GetDriverName();
  // #endif

  process->setArguments(args);
  qCDebug(dslmapProj) << "Running: " << process->program()
                      << process->arguments().join(' ');

  process->start();

  if (!process->waitForFinished(10000)) {
    qCWarning(dslmapProj) << "Timed out waiting for contour generation";
    process->kill();
    return {};
  }

  if (process->exitStatus() != QProcess::NormalExit) {
    qCWarning(dslmapProj) << "gdal_contour crashed during contour generation";
    return {};
  }

  const auto exit_code = process->exitCode();
  qCInfo(dslmapProj) << "gdal_contour returned with exit code:" << exit_code;
  if (exit_code != 0) {
    qCInfo(dslmapProj) << process->readAllStandardError();
    qCInfo(dslmapProj) << process->readAllStandardOutput();
    return {};
  }
  
  return ContourLayer::fromShapefile(shape_file.path(), attribute_name, 0);
}

QRectF
ContourLayer::boundingRect() const
{
  const Q_D(ContourLayer);
  return d->m_bounding_rect;
}

void
ContourLayer::paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*)
{
}

void
ContourLayer::setProjection(std::shared_ptr<const Proj> proj)
{
  if (!proj) {
    return;
  }

  if (projectionPtr() && projectionPtr()->isSame(*proj)) {
    return;
  }

  Q_D(ContourLayer);
  QPainterPath::Element element;
  for (auto item : d->m_contour_lines) {
    auto path = item->path();
    for (auto i = 0; i < path.elementCount(); ++i) {
      element = path.elementAt(i);
      if (element.isLineTo() || element.isMoveTo()) {
        projectionPtr()->transformTo(*proj, 1, &element.x, &element.y);
        path.setElementPositionAt(i, element.x, element.y);
      }
    }
    item->setPath(path);
  }

  d->m_bounding_rect = childrenBoundingRect();
  MapLayer::setProjection(proj);
}

qreal
ContourLayer::minValue() const
{
  return minMaxValues().first;
}
qreal
ContourLayer::maxValue() const
{
  return minMaxValues().second;
}

QPair<qreal, qreal>
ContourLayer::minMaxValues() const
{
  const Q_D(ContourLayer);
  if (d->m_contour_lines.empty()) {
    return {};
  }

  const auto minmax =
    std::minmax_element(d->m_contour_levels.begin(), d->m_contour_levels.end());
  return { *(minmax.first), *(minmax.second) };
}

void
ContourLayer::setSpacing(int spacing){
  source_spacing = spacing;
}

void
ContourLayer::setSourceFile(QString file){
  source_filename = file;
}

void
ContourLayer::saveUnderlay(){
      emit saveContourUnderlay(source_filename, source_spacing);
}

}
