/**
 * Copyright 2020 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "GeoreferenceImport.h"

#include "ColorPickButton.h"
#include "ui_GeoreferenceImport.h"
#include <QFileDialog>

namespace dslmap {

Q_LOGGING_CATEGORY(dslmapGeoreferenceImageImport,
                   "dslmap.object.geoimageimport")

GeoreferenceImport::GeoreferenceImport(QWidget* parent)
  : QDialog(parent)
  , ui(new Ui::GeoreferenceImport)
{
  ui->setupUi(this);
  connect(ui->openFile, &QPushButton::clicked, this,
          &GeoreferenceImport::openFile);
  connect(ui->confirmButton, &QPushButton::clicked, this,
          &GeoreferenceImport::confirmSettings);

  connect(ui->transColor, &ColorPickButton::colorChanged,
          [=](const QColor color) { emit transColorChanged(color); });

  connect(this, &GeoreferenceImport::transColorChanged, this,
          &GeoreferenceImport::setTransColor);
  connect(ui->cancelButton, &QPushButton::clicked, this,
          &GeoreferenceImport::cancelSettings);

  connect(ui->layerName, &QLineEdit::textChanged, this,
          &GeoreferenceImport::setLayerName);
}

QColor
GeoreferenceImport::getTransColor()
{
  return this->transColor;
}

void
GeoreferenceImport::setLayerName(QString name)
{
  this->layerName = name;
}

QString
GeoreferenceImport::getLayerName()
{
  if (this->layerName.isEmpty()){
    return "";
  }
  return this->layerName;
}

void
GeoreferenceImport::setTransColor(QColor color)
{
  this->transColor = color;
}

QString
GeoreferenceImport::openFile()
{

  auto filename = QFileDialog::getOpenFileName(
    this, "Open georeferenced image file...", "",
    "Image files (*.png *.tif *.kap);;All files (*.*)");
  if (filename.isEmpty()) {
    return "";
  }
  this->fileName = filename;
  updateFileNameText();

  return filename;
}
void
GeoreferenceImport::confirmSettings()
{
  QDialog::accept();
  // qdialog accepted
}

void
GeoreferenceImport::cancelSettings()
{
  QDialog::close();
}

void
GeoreferenceImport::updateFileNameText()
{
  ui->fileNameLine->setText(this->fileName);
}

QString
GeoreferenceImport::getFileName()
{
  if (this->fileName.isEmpty()){
    return "";
  }
  return this->fileName;
}

} // namespace dslmap
