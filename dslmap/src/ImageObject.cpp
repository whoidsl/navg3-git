/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 10/12/17.
//

#include "ImageObject.h"
#include "GdalUtils.h"
#include "Proj.h"
#include "TemporaryGdalDataset.h"

#include <gdal/gdal_priv.h>

namespace dslmap {

Q_LOGGING_CATEGORY(dslmapImageObject, "dslmap.object.image");

class ImageObjectPrivate
{

public:
  explicit ImageObjectPrivate()
    : m_proj(Proj::fromDefinition(PROJ_WGS84_DEFINITION))
    , m_pixmap(new QGraphicsPixmapItem)
  {
  }
  ~ImageObjectPrivate() = default;

  std::shared_ptr<const Proj> m_proj;

  QGraphicsPixmapItem* m_pixmap;

  /// Projected dataset
  std::shared_ptr<TemporaryGdalDataset> m_projected_dataset;

  QRectF m_projected_dataset_bounds;

  /// Original image dataset
  std::shared_ptr<TemporaryGdalDataset> m_dataset;

  void updatePixmapFromDataset();
};

void
ImageObjectPrivate::updatePixmapFromDataset()
{
  // Load the projected data from the data file.
  const auto projected_filename = m_projected_dataset->fileName();
  QPixmap pixmap(projected_filename, "PNG");

  auto adf_transform = QVector<double>{ 0, 0, 0, 0, 0, 0 };
  m_projected_dataset->get()->GetGeoTransform(adf_transform.data());

  // const auto size = QSize{ m_projected_dataset->get()->GetRasterXSize(),
  //                          m_projected_dataset->get()->GetRasterYSize() };

  // Set the pixmap.
  m_pixmap->setPixmap(pixmap);

  // Create the transformation matrix
  auto ok = false;
  const auto transform = gdalGeometryToTransform(adf_transform, &ok);
  if (!ok) {
    qCWarning(dslmapImageObject)
      << "Unable to calculate perspective transform given GDAL transform:"
      << adf_transform;
  }

  m_pixmap->setTransform(transform, false);
}

ImageObject::ImageObject(QGraphicsItem* parent)
  : QGraphicsObject(parent)
  , d_ptr(new ImageObjectPrivate)
{
  setFlag(ItemHasNoContents, true);
  Q_D(ImageObject);
  d->m_pixmap->setParentItem(this);
}

bool
ImageObject::setImage(const QString& filename, const QRectF& bounds,
                      std::shared_ptr<const Proj> proj)
{
  const auto poly =
    QPolygonF{ QVector<QPointF>{ bounds.topLeft(), bounds.topRight(),
                                 bounds.bottomRight(), bounds.bottomLeft() } };

  return setImage(filename, poly, proj);
}

bool
ImageObject::setImage(const QString& filename, const QPolygonF& bounds,
                      std::shared_ptr<const Proj> proj)
{
  QImage image(filename);
  if (image.isNull()) {
    qCWarning(dslmapImageObject) << "Unable to load image" << filename;
    return false;
  }
  qCInfo(dslmapImageObject) << "Loading raster layer from image" << filename;

  return setImage(image, bounds, proj);
}

bool
ImageObject::setImage(const QImage& image, const QRectF& bounds,
                      std::shared_ptr<const Proj> proj)
{
  const auto poly =
    QPolygonF{ QVector<QPointF>{ bounds.topLeft(), bounds.topRight(),
                                 bounds.bottomRight(), bounds.bottomLeft() } };

  return setImage(image, poly, proj);
}

bool
ImageObject::setImage(const QImage& image, const QPolygonF& bounds,
                      std::shared_ptr<const Proj> proj)
{

  Q_D(ImageObject);

  if (!proj) {
    qCWarning(dslmapImageObject) << "No projection supplied.";
    return false;
  }

  if (image.format() == QImage::Format_Invalid) {
    qCWarning(dslmapImageObject)
      << "Loaded image, but reported format is invalid";
    return false;
  }

  // Create a temporary dataset
  auto temp_dataset = std::make_shared<TemporaryGdalDataset>();

  qCDebug(dslmapImageObject)
    << "Opened temporary file:" << temp_dataset->fileName()
    << ", attempting to save temporary copy as PNG image to loading with GDAL";

  if (!image.save(temp_dataset->fileName(), "PNG", -1)) {
    qCWarning(dslmapImageObject)
      << "Unable to save temporary PNG copy of image to file:"
      << temp_dataset->fileName();
    return false;
  }

  // Now we have a temporary file with our image as a PNG file.  We can load
  // this as a GDAL dataset.

  qCDebug(dslmapImageObject) << "Attempting to load temporary PNG into GDAL";
  temp_dataset->fileName();
  if (!temp_dataset->open()) {
    qCWarning(dslmapImageObject)
      << "Unable to read PNG data in GDAL from temporary file:"
      << temp_dataset->fileName();
    return false;
  }

  // Add override our projection
  temp_dataset->setProjection(proj);

  // And the transform
  auto _dataset = temp_dataset->get();

  // Now.. let's set the projection and transform.
  auto adfTransform = polygonToAdfProjection(image.size(), bounds);
  qDebug() << adfTransform;
  if (adfTransform.isEmpty()) {
    qCWarning(dslmapImageObject)
      << "Problem creating adfTransform vector given bounding poly:" << bounds;
    return false;
  }

  _dataset->SetGeoTransform(adfTransform.data());

  const auto src_proj4 = proj->definition();

  qCDebug(dslmapImageObject) << "Using projection:" << src_proj4;

  if (_dataset->SetProjection(src_proj4.toLatin1()) != CE_None) {
    qCWarning(dslmapImageObject)
      << "Unable to set dataset projection using PROJ4 definition.";
    return false;
  }

// Keep this block around for now?  Example of how to convert from proj4 to
// wkt.  Maybe use it as a fallback
#if 0
  auto ogr_spatial_ref = OGRSpatialReference{};
  if(ogr_spatial_ref.importFromProj4(src_proj4.toLatin1()) != OGRERR_NONE) {
    qCWarning(dslmapImageObject)
      << "Could not import proj4 definition for OGR";
    return false;
  }

  auto src_wkt = static_cast<char*>(nullptr);
  // the export method claims to always return OGRERR_NONE, but this behavior
  // "may change in the future"
  if(ogr_spatial_ref.exportToWkt(&src_wkt) != OGRERR_NONE) {
    qCWarning(dslmapImageObject)
      << "Unexpected result exporting WKT projection, possible badness ensues";
  }

  qCDebug(dslmapImageObject)
    << "Proj4 definition converted to WKT:" << src_wkt;

  dataset->SetProjection(src_wkt);
#endif
  qCDebug(dslmapImageObject) << "Finished Loaded PNG copy in GDAL";

  // Now, if the provided projection and the layer projection are different
  // we'll need to reproject.
  auto proj_dataset = temp_dataset;

  // Do we have a projection set?
  if (d->m_proj) {
    // Is it different than the projection we gave for the image?
    if (!d->m_proj->isSame(*proj)) {
      qCInfo(dslmapImageObject) << "Projecting image into coordinates:"
                                << d->m_proj->definition();

      proj_dataset = temp_dataset->warp(*d->m_proj);
      if (!proj_dataset) {
        qCWarning(dslmapImageObject)
          << "Unable to reproject image into layer coordinates.";
        return false;
      }
    }
  }

  // Finally, save the two datasets
  d->m_dataset = temp_dataset;
  d->m_projected_dataset = proj_dataset;
  d->updatePixmapFromDataset();
  prepareGeometryChange();
  d->m_projected_dataset_bounds =
    mapRectFromItem(d->m_pixmap, d->m_pixmap->boundingRect());

  return true;
}

void
ImageObject::setProjection(std::shared_ptr<const Proj> proj)
{

  if (!proj) {
    qCWarning(dslmapImageObject)
      << "Cannot reproject layer; no valid projection provided";
    return;
  }

  Q_D(ImageObject);

  // No data to reproject.
  if (!d->m_dataset) {
    return;
  }

  auto projected = d->m_dataset->warp(*proj);

  if (!projected) {
    qCWarning(dslmapImageObject) << "Failed to reproject image";
    return;
  }

  // Swap in the new one.
  d->m_projected_dataset = projected;

  // And update the pixmap.
  d->updatePixmapFromDataset();
  prepareGeometryChange();
  d->m_projected_dataset_bounds =
    mapRectFromItem(d->m_pixmap, d->m_pixmap->boundingRect());
  d->m_proj = proj;
}

QRectF
ImageObject::boundingRect() const
{
  const Q_D(ImageObject);
  return d->m_projected_dataset_bounds;
}

void
ImageObject::paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*)
{
}

const TemporaryGdalDataset*
ImageObject::dataset() const
{
  const Q_D(ImageObject);
  return d->m_dataset.get();
}
const TemporaryGdalDataset*
ImageObject::projectedDataset() const
{
  const Q_D(ImageObject);
  return d->m_projected_dataset.get();
}

std::shared_ptr<const Proj>
ImageObject::projection() const noexcept
{
  const Q_D(ImageObject);
  return d->m_proj;
}
const Proj*
ImageObject::projectionPtr() const noexcept
{
  const Q_D(ImageObject);
  return d->m_proj.get();
}
}