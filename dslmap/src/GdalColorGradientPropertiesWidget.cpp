/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "GdalColorGradientPropertiesWidget.h"
#include "GdalColorGradient.h"
#include "ui_GdalColorGradientPropertiesWidget.h"
#include <QMetaEnum>

namespace dslmap {

GdalColorGradientPropertiesWidget::GdalColorGradientPropertiesWidget(
  QWidget* parent)
  : QWidget(parent)
  , ui(new Ui::GdalColorGradientPropertiesWidget)
{
  ui->setupUi(this);

  auto valid = new QDoubleValidator(this);
  ui->min->setValidator(valid);
  ui->max->setValidator(valid);

  auto metaenum =
    QMetaEnum::fromType<dslmap::GdalColorGradient::GradientPreset>();
  for (auto i = 0; i < metaenum.keyCount(); ++i) {
    ui->colorBox->addItem(metaenum.key(i), i);
  }
}

GdalColorGradientPropertiesWidget::~GdalColorGradientPropertiesWidget()
{
  delete ui;
}

void
GdalColorGradientPropertiesWidget::setGradient(
  std::shared_ptr<GdalColorGradient> grad)
{
  ui->absChk->setChecked(grad->isAbsolute());
  connect(ui->absChk, &QCheckBox::stateChanged,
          [grad](int abs) { grad->setAbsolute(static_cast<bool>(abs)); });

  ui->min->setText(QString::number(grad->min()));
  connect(ui->min, &QLineEdit::editingFinished,
          [grad, this]() { grad->setMin(ui->min->text().toDouble()); });

  ui->max->setText(QString::number(grad->max()));
  connect(ui->max, &QLineEdit::editingFinished,
          [grad, this]() { grad->setMax(ui->max->text().toDouble()); });

  ui->defaultChk->setChecked(grad->noDataVisible());
  connect(ui->defaultChk, &QCheckBox::stateChanged,
          [grad](int vis) { grad->setNoDataVisible(static_cast<bool>(vis)); });

  ui->defaultC->setColor(grad->noDataColor());
  connect(ui->defaultC, &ColorPickButton::colorChanged,
          [grad](QColor col) { grad->setNoDataColor(col); });

  // TODO (LEL): This doesn't seem to be working ... when I first bring up
  //  the dialog box, the displayed colors and dropdown selection don't match.
  //  Once I've changed it once, they match persistently.
  ui->colorBox->setCurrentIndex(static_cast<int>(grad->preset()));
  connect(ui->colorBox, static_cast<void (QComboBox::*)(int)>(
                          &QComboBox::currentIndexChanged),
          [grad](int val) {
            auto preset = static_cast<GdalColorGradient::GradientPreset>(val);
            if (preset != grad->preset()) {
              grad->setGradient(preset);
            }
          });

} // end setGradient
}