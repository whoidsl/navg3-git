/**
 * Copyright 2021 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// Created by tmjoyce on 06/03/21.
//

#include "ImageTilingHelper.h"

#include <QFile>
#include <QProcess>
#include <gdal/gdal_priv.h>
#include <string>

#define PIXEL_LIMIT 16777216 // 4096*4096
//#define PIXEL_LIMIT 90000 // 300*300
//#define PIXEL_LIMIT 22500 // 150*150

namespace dslmap {

bool
ImageTilingHelper::needsTiling(QImage image)
{
  return ((image.width() * image.height()) > PIXEL_LIMIT);
}

bool
ImageTilingHelper::needsTiling(std::vector<QGraphicsPixmapItem*> pix_items)
{

  for (auto item : pix_items) {
    int width = item->pixmap().width();
    int height = item->pixmap().height();
    if ((width * height) > PIXEL_LIMIT) {
      return true;
    }
  }
  return false;
}
bool
ImageTilingHelper::needsTiling(QGraphicsPixmapItem* item)
{
  int width = item->pixmap().width();
  int height = item->pixmap().height();
  if ((width * height) > PIXEL_LIMIT) {
    return true;
  }
  return false;
}

QPair<QGraphicsPixmapItem*, QGraphicsPixmapItem*>
ImageTilingHelper::cutImageVertically(QGraphicsPixmapItem* input_pix)
{
  int width = input_pix->pixmap().width();
  int height = input_pix->pixmap().height();
  int first_half_width = width / 2;
  int second_half_width = width - first_half_width;

  QTransform source_transform = input_pix->transform();
  QTransform translated_transform = source_transform;

  translated_transform.translate(first_half_width, 0);

  QPixmap first_pix = input_pix->pixmap().copy(QRect(0, 0, first_half_width, height));

  QPixmap second_pix =
    input_pix->pixmap().copy(QRect(first_half_width, 0, second_half_width, height));

  QGraphicsPixmapItem* first_half = new QGraphicsPixmapItem;
  QGraphicsPixmapItem* second_half = new QGraphicsPixmapItem;

  first_half->setPixmap(first_pix);
  first_half->setTransform(source_transform, false);

  second_half->setPixmap(second_pix);
  second_half->setTransform(translated_transform, false);

  return QPair<QGraphicsPixmapItem*, QGraphicsPixmapItem*>(first_half,
                                                           second_half);
}

QPair<QGraphicsPixmapItem*, QGraphicsPixmapItem*>
ImageTilingHelper::cutImageHorizontally(QGraphicsPixmapItem* input_pix)
{
  // what to do if the image dimension is an odd number?
  // obviously one half will have to be 1 pixel larger than the other
 
  int width = input_pix->pixmap().width();
  int height = input_pix->pixmap().height();
  int first_half_height = height / 2;
  int second_half_height = height - first_half_height;

  QTransform source_transform = input_pix->transform();
  QTransform translated_transform = source_transform;

  translated_transform.translate(0, first_half_height);
 
  QPixmap first_pix = input_pix->pixmap().copy(QRect(0, 0, width, first_half_height));

  QPixmap second_pix =
    input_pix->pixmap().copy(QRect(0, first_half_height, width, second_half_height));

  QGraphicsPixmapItem* first_half = new QGraphicsPixmapItem;
  QGraphicsPixmapItem* second_half = new QGraphicsPixmapItem;

  first_half->setPixmap(first_pix);
  first_half->setTransform(source_transform, false);

  second_half->setPixmap(second_pix);
  second_half->setTransform(translated_transform, false);

  return QPair<QGraphicsPixmapItem*, QGraphicsPixmapItem*>(first_half,
                                                           second_half);
}

std::vector<QGraphicsPixmapItem*>
ImageTilingHelper::getImageTiles(QGraphicsPixmapItem* source_pixmap)
{
  if (!source_pixmap) {
    return std::vector<QGraphicsPixmapItem*>();
  }
  if (!needsTiling(source_pixmap)) {
    return std::vector<QGraphicsPixmapItem*>() = { source_pixmap };
  }

  std::vector<QGraphicsPixmapItem*> vec;
  vec.push_back(source_pixmap);

  while (needsTiling(vec)) {
    std::vector<QGraphicsPixmapItem*> temp;

    for (auto item : vec) {
      QPair<QGraphicsPixmapItem*, QGraphicsPixmapItem*> pair;
      int width = item->pixmap().width();
      int height = item->pixmap().height();
      if (width > height) {
        pair = cutImageVertically(item);
      } else {
        pair = cutImageHorizontally(item);
      }

      temp.push_back(pair.first);
      temp.push_back(pair.second);
    }

    vec = temp;
  }

  return vec;
}

std::vector<QGraphicsPixmapItem*>
ImageTilingHelper::getImageTiles(QImage image, QTransform source_transform)
{
  QGraphicsPixmapItem* pix_item = new QGraphicsPixmapItem;
  pix_item->setTransform(source_transform, false);
  pix_item->setPixmap(QPixmap::fromImage(image));

  return getImageTiles(pix_item);
}
}
