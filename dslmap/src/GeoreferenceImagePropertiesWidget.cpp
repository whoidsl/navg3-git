/**
 * Copyright 2020 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "GeoreferenceImagePropertiesWidget.h"
#include "GeoreferenceImageObject.h"
#include "ui_GeoreferenceImagePropertiesWidget.h"

namespace dslmap {

Q_LOGGING_CATEGORY(dslmapGeoreferenceImagePropertiesWidget,
                   "dslmap.object.geoimageprops")

GeoreferenceImagePropertiesWidget::GeoreferenceImagePropertiesWidget(
  QWidget* parent)
  : QWidget(parent)
  , ui(new Ui::GeoreferenceImagePropertiesWidget)
{
  ui->setupUi(this);
  ui->cropButton->setEnabled(false);
  connect(ui->transColor, &ColorPickButton::colorChanged,
          [=](const QColor color) { emit transColorChanged(color); });
}

GeoreferenceImagePropertiesWidget::~GeoreferenceImagePropertiesWidget()
{
  delete ui;
}

void
GeoreferenceImagePropertiesWidget::setObject(GeoreferenceImageObject* obj)
{
  if (!obj) {
    qCWarning(dslmapGeoreferenceImagePropertiesWidget)
      << "no geoimage object. returning";
    return;
  }
  setupCropSpinBoxes(obj);
  connect(this, &GeoreferenceImagePropertiesWidget::transColorChanged, obj,
          &GeoreferenceImageObject::setTransparentColor);
  ui->greyCheckBox->setChecked(obj->getGreyScaleState());
  connect(ui->greyCheckBox, &QCheckBox::stateChanged, obj,
          &GeoreferenceImageObject::toggleGreyScale);



  connect(ui->cropButton, &QPushButton::clicked, [this]() {
    emit imageCropReady(ui->x0Spin->value(), ui->y0Spin->value(),
                        ui->dxSpin->value(), ui->dySpin->value());
  });
  

  connect(this, &GeoreferenceImagePropertiesWidget::imageCropReady, obj,
          &GeoreferenceImageObject::cropImage);
}

bool
GeoreferenceImagePropertiesWidget::validateCropText(int val)
{

  if (ui->x0Spin->value() < ui->dxSpin->value() &&
      ui->y0Spin->value() < ui->dySpin->value()) {
    ui->cropButton->setEnabled(true);
    return true;
  } else {
    ui->cropButton->setEnabled(false);
    return false;
  }
}

void
GeoreferenceImagePropertiesWidget::setupCropSpinBoxes(
  GeoreferenceImageObject* obj)
{
  QImage image = obj->getQImage();

  auto imgSize = image.size();

  ui->x0Spin->setRange(0, imgSize.width());
  ui->y0Spin->setRange(0, imgSize.height());

  ui->dxSpin->setRange(0, imgSize.width());
  ui->dySpin->setRange(0, imgSize.height());

  ui->dxSpin->setValue(imgSize.width());
  ui->dySpin->setValue(imgSize.height());


  connect(ui->x0Spin,
          static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), this,
          &GeoreferenceImagePropertiesWidget::validateCropText);

  connect(ui->y0Spin,
          static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), this,
          &GeoreferenceImagePropertiesWidget::validateCropText);

  connect(ui->dxSpin,
          static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), this,
          &GeoreferenceImagePropertiesWidget::validateCropText);

  connect(ui->dySpin,
          static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), this,
          &GeoreferenceImagePropertiesWidget::validateCropText);


}
} //dslmap
