/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 9/17/18.
//

#include "SymbolLayerPropertiesWidget.h"
#include "SymbolLayer.h"
#include "ui_SymbolLayerPropertiesWidget.h"

namespace dslmap {

Q_LOGGING_CATEGORY(dslmapSymbolLayerPropertiesWidget, "dslmap.layer.symbol")

SymbolLayerPropertiesWidget::SymbolLayerPropertiesWidget(QWidget* parent)
  : QWidget(parent)
  , ui(new Ui::SymbolLayerPropertiesWidget)
{
  ui->setupUi(this);
  setWindowTitle("Symbol Layer Properties");

  // Gradient controls should only be active if color gradient is enabled.
  ui->attribute_combobox->setEnabled(false);
  ui->gradient->setEnabled(false);
  connect(ui->enable_gradient_checkbox, &QCheckBox::stateChanged,
          [this](bool active) { ui->gradient->setEnabled(active); });

  connect(ui->enable_gradient_checkbox, &QCheckBox::stateChanged,
          [this](bool active) { ui->attribute_combobox->setEnabled(active); });

  connect(ui->okBox, &QDialogButtonBox::rejected, this, &SymbolLayerPropertiesWidget::close);
}

SymbolLayerPropertiesWidget::~SymbolLayerPropertiesWidget()
{
  delete ui;
}

void
SymbolLayerPropertiesWidget::setLayer(SymbolLayer* layer)
{
  ui->general->setLayer(qobject_cast<dslmap::MapLayer*>(layer));

  ui->symbol->setLabelVisible(layer->isLabelVisible());
  ui->symbol->setOutlineVisible(layer->outlineVisible());
  ui->symbol->setFillVisible(layer->fillVisible());
  ui->symbol->setOutlineColor(layer->outlineColor());
  ui->symbol->setFillColor(layer->fillColor());
  ui->symbol->setLabelColor(layer->labelColor());
  ui->symbol->setLabelSize(layer->labelSize());
  ui->symbol->setShape(layer->symbolShape());
  ui->symbol->setSymbolSize(layer->symbolSize());

  connect(ui->symbol, &SymbolPropertiesWidget::outlineColorChanged, layer,
          &SymbolLayer::setOutlineColor);
  connect(ui->symbol, &SymbolPropertiesWidget::fillColorChanged, layer,
          &SymbolLayer::setFillColor);
  connect(ui->symbol, &SymbolPropertiesWidget::labelColorChanged, layer,
          &SymbolLayer::setLabelColor);
  connect(ui->symbol, &SymbolPropertiesWidget::labelSizeChanged, layer,
          &SymbolLayer::setLabelSize);
  connect(ui->symbol, &SymbolPropertiesWidget::shapeChanged, layer,
          &SymbolLayer::setShape);
  connect(ui->symbol, &SymbolPropertiesWidget::labelVisibleChanged, layer,
          &SymbolLayer::setLabelVisible);
  connect(ui->symbol, &SymbolPropertiesWidget::fillVisibleChanged, layer,
          &SymbolLayer::setFillVisible);
  connect(ui->symbol, &SymbolPropertiesWidget::outlineVisibleChanged, layer,
          &SymbolLayer::setOutlineVisible);

  auto symbol = layer->symbol();
  ui->enable_gradient_checkbox->setChecked(symbol->useColorGradient());
  connect(ui->enable_gradient_checkbox, &QCheckBox::stateChanged,
          [layer](bool use_gradient) {
            layer->symbolPtr()->setUseColorGradient(use_gradient);
          });

  // Needs to be queried before re-building the attribute_combobox
  QString current_attribute = layer->symbolPtr()->gradientAttribute();

  // addLayer is called repeatedly on the same widget, so have to clear first.
  // NOTE: If the attribute list changes while the widget is open, changes
  //  will not be reflected in the widget until it is closed/reopened.
  ui->attribute_combobox->clear();
  Q_FOREACH (const auto& key, layer->attributeKeys()) {
    ui->attribute_combobox->addItem(key);
  }

  int index = ui->attribute_combobox->findText(current_attribute);
  if (index >= 0) {
    ui->attribute_combobox->setCurrentIndex(index);
  } else {
    // This could happen if the current attribute is unset (set to QString()).
    // Otherwise, it's a bug.
    qCWarning(dslmapSymbolLayerPropertiesWidget)
      << "Layer's current attribute " << current_attribute
      << " not found in list of available attributes ";
  }

  connect(ui->attribute_combobox, &QComboBox::currentTextChanged,
          [layer](QString attribute) {
            layer->symbolPtr()->setGradientAttribute(attribute);
          });

  auto grad = layer->gradient();
  ui->gradient->setGradient(grad);
}

} // dslmap
