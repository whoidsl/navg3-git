/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 8/8/18.
//

#include "PenPropertiesWidget.h"
#include "ui_PenPropertiesWidget.h"

namespace dslmap {

PenPropertiesWidget::PenPropertiesWidget(QWidget* parent)
  : QWidget(parent)
  , ui(new Ui::PenPropertiesWidget)
{
  ui->setupUi(this);

  ui->styleBox->addItem("NoPen");
  ui->styleBox->addItem("Solid");
  ui->styleBox->addItem("Dash");
  ui->styleBox->addItem("Dot");
  ui->styleBox->addItem("Dash Dot");
  ui->styleBox->addItem("Dash Dot Dot");

  connect(this, &PenPropertiesWidget::penChanged, ui->scribble,
          &PreviewScribbleWidget::setPen);

  connect(ui->colorBtn, &ColorPickButton::colorChanged,
          [=](const QColor color) {
            m_pen.setColor(color);
            emit penChanged(m_pen);
          });
  connect(ui->widthBox,
          static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged),
          [=](const int width) {
            m_pen.setWidthF(width);
            emit penChanged(m_pen);
          });
  connect(ui->styleBox, static_cast<void (QComboBox::*)(int)>(
                          &QComboBox::currentIndexChanged),
          [=](const int index) {
            auto style = static_cast<Qt::PenStyle>(index);
            m_pen.setStyle(style);
            emit penChanged(m_pen);
          });
}

PenPropertiesWidget::~PenPropertiesWidget()
{
  delete ui;
}

void
PenPropertiesWidget::setPen(QPen pen)
{
  m_pen = pen;
  auto index = static_cast<int>(pen.style());
  ui->styleBox->setCurrentIndex(index);
  ui->widthBox->setValue(m_pen.width());
  ui->colorBtn->setColor(m_pen.color());
}

void
PenPropertiesWidget::setName(QString name)
{
  ui->groupBox->setTitle(name);
}

} // namespace
