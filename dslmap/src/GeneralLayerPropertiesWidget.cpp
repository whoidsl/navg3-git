/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "GeneralLayerPropertiesWidget.h"
#include "ui_GeneralLayerPropertiesWidget.h"

#include "MapLayer.h"
#include <QDebug>

namespace dslmap {

GeneralLayerPropertiesWidget::GeneralLayerPropertiesWidget(QWidget* parent)
  : QWidget(parent)
  , ui(new Ui::GeneralLayerPropertiesWidget)
{
  ui->setupUi(this);
}

GeneralLayerPropertiesWidget::~GeneralLayerPropertiesWidget()
{
  delete ui;
}

void
GeneralLayerPropertiesWidget::setLayer(MapLayer* layer)
{
  // First, populate the form values
  auto item = layer->toGraphicsObject();
  ui->opacity->setValue(static_cast<int>(item->opacity() * 100));

  // Connect the opacity slider
  connect(ui->opacity, &QSlider::valueChanged, [item](auto value) {
    item->setOpacity(static_cast<qreal>(value) / 100.0);
  });

  ui->name->setText(layer->name());
  connect(ui->name, &QLineEdit::textChanged,
          [layer](auto name) { layer->setName(name); });

  ui->uuid->setText(layer->id().toString());

  ui->zval->setValue(static_cast<int>(item->zValue()));
  connect(ui->zval,
          static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged),
          [item](auto z_val) { item->setZValue(z_val); });
}
}
