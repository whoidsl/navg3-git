/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "SingleTarget.h"
#include "SymbolLayer.h"
#include "ui_SingleTarget.h"
#include "Util.h"

namespace dslmap {

SingleTarget::SingleTarget(QWidget* parent)
  : QDialog(parent)
  , ui(new Ui::SingleTarget)
{
  ui->setupUi(this);

  ui->tabWidget->hide();
  m_pointmap[ui->latlonBtn] = QPointF{};
  m_layermap[ui->newBtn] = nullptr;
  ui->newBtn->toggle();

  connect(ui->latlonBtn, &QRadioButton::clicked, ui->tabWidget, &QTabWidget::show);
  connect(ui->okBox, &QDialogButtonBox::accepted, this, &SingleTarget::accept);
  connect(ui->okBox, &QDialogButtonBox::rejected, this, &SingleTarget::reject);
}

SingleTarget::SingleTarget(QMap<QString, QPointF> position_options,
                           QWidget* parent)
  : SingleTarget(parent)
{
  setPositions(position_options);
}

void
SingleTarget::setPositions(QMap<QString, QPointF> position_options)
{
  for (const auto pos : position_options.keys()) {
    QRadioButton* btn = new QRadioButton(pos, this);
    ui->list_pos->addWidget(btn);
    m_pointmap[btn] = position_options[pos];
    //qDebug() << "pos is "<<pos;
    btn->toggle();
  }
}

SingleTarget::~SingleTarget()
{
  delete ui;
}

QString
SingleTarget::extractLayername()
{
  return ui->editNew->text();
}

QString
SingleTarget::extractLabeltext()
{
  return ui->editLabel->text();
}

void
SingleTarget::setScene(dslmap::MapScene* scene)
{
  if (scene == nullptr) {
    return;
  }
  m_scene = scene;
  if (m_scene->layers().isEmpty()) {
    return;
  }

  std::vector<qreal> z_vec;
  int max_z=0;
  for (auto layer : m_scene->layers()) {
    auto symbollayer = qobject_cast<SymbolLayer*>(layer);
    z_vec.push_back(layer->toGraphicsObject()->zValue());
  }
  max_z = *max_element(z_vec.begin(), z_vec.end());
  for (auto layer : m_scene->layers()) {
    auto symbollayer = qobject_cast<SymbolLayer*>(layer);
    if (symbollayer) {
      QRadioButton* btn = new QRadioButton(layer->name(), this);
      ui->list_layer->addWidget(btn);
      if (layer->toGraphicsObject()->zValue() == max_z) {
        m_layermap[btn] = symbollayer;
        btn->toggle();
      }
      else {
        m_layermap[btn] = symbollayer;
      }
    }
  }
}

dslmap::SymbolLayer*
SingleTarget::extractSymbolLayer()
{
  for (auto btn : m_layermap.keys()) {
    if (btn->isChecked()) {
      return m_layermap[btn];
    }
  }
  qDebug() << "No buttons selected!";
  return nullptr;
}

QPointF
SingleTarget::extractPos()
{
  if (ui->latlonBtn->isChecked()) {
    QPointF ret;
    if (ui->tabWidget->currentWidget() == ui->tab_2) {
      if (ui->lon_min->text().toDouble() > 60 || ui->lat_min->text().toDouble() > 60) return QPointF{};
      if (ui->lon_min->text().toDouble() < 0 || ui->lat_min->text().toDouble() < 0) return QPointF{};
      ret = QPointF{ dslmap::LatorLonDMToDD(ui->lon_deg->text().toInt(), ui->lon_min->text().toDouble(), 8),
                     dslmap::LatorLonDMToDD(ui->lat_deg->text().toInt(), ui->lat_min->text().toDouble(), 8) };
    } else {
      ret = QPointF{ ui->editLon->text().toDouble(),
                     ui->editLat->text().toDouble() };
    }
    if (m_scene->projection()) {
      double x_val(ret.x());
      double y_val(ret.y());
      m_scene->projection()->transformFromLonLat(1, &x_val, &y_val);
      ret.setX(x_val);
      ret.setY(y_val);
    }
    return ret;
  }
  for (auto btn : m_pointmap.keys()) {
    if (btn->isChecked()) {
      return m_pointmap[btn];
    }
  }
  return QPointF{};
}
} // namespace
