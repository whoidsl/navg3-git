/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 10/23/17.
//

#include "GdalColorGradient.h"
#include "GdalColorGradientPrivate.h"

#include <QPropertyAnimation>
#include <QTemporaryFile>
#include <QDebug>

namespace dslmap {

GdalColorGradientPrivate::GdalColorGradientPrivate(GdalColorGradient* grad)
  : m_interpolator(new QPropertyAnimation)
  , m_gdal_colortable(new QTemporaryFile)
  , q_ptr(grad)
{
  m_interpolator->setEasingCurve(QEasingCurve::Linear);
  m_gdal_colortable->open();
  m_preset = GdalColorGradient::GradientPreset::Rainbow;
  m_min = 0.0; // Needed these to be initialized so equality testing would work.
  m_max = 1.0;
  m_spread = m_max - m_min;
}

void
GdalColorGradientPrivate::updateGdalColortable()
{

  // m_gdal_colortable.reset(new QTemporaryFile);
  // m_gdal_colortable->open();
  // Truncate the existing file, clearing all data
  // 2022-09-30 ZB: Close before truncation, then reopen.  Observed during
  // AT50-03 that colormap file would not truncate when gradient was changed
  // in NavG3, causing errors in the displayed shading
  m_gdal_colortable->close();
  Q_ASSERT(m_gdal_colortable->resize(0));
  m_gdal_colortable->open();

  // GDAL colortables look like:
  // value red:green:blue:alpha
  //
  // value can either be an absolute number or a percentage.
  // The RGBA values are all ints between 0-255
  if (m_absolute) {
    qDebug() << "New absolute color table";
    foreach (auto& key, m_interpolator->keyValues()) {
      const auto depth = key.first * m_spread + m_min;
      const auto color = key.second.value<QColor>();

      const auto line = QString("%1 %2:%3:%4:%5\n")
                          .arg(depth)
                          .arg(color.red())
                          .arg(color.green())
                          .arg(color.blue())
                          .arg(color.alpha());

      qDebug() << line;
      m_gdal_colortable->write(line.toLatin1());
    }
  } else {
    qDebug() << "New relative color table";
    foreach (auto& key, m_interpolator->keyValues()) {
      const auto depth = static_cast<int>(key.first * 100);
      const auto color = key.second.value<QColor>();

      const auto line = QString("%1% %2:%3:%4:%5\n")
                          .arg(depth)
                          .arg(color.red())
                          .arg(color.green())
                          .arg(color.blue())
                          .arg(color.alpha());

      qDebug() << line;
      m_gdal_colortable->write(line.toLatin1());
    }
  }

  // A final 'no data' value
  if (m_nodata_color.isValid()) {
    int alpha = (m_nodata_visible) ? m_nodata_color.alpha() : 0;
    auto line = QString("nv %1:%2:%3:%4\n")
                  .arg(m_nodata_color.red())
                  .arg(m_nodata_color.green())
                  .arg(m_nodata_color.blue())
                  .arg(alpha);
    m_gdal_colortable->write(line.toLatin1());
  }
  m_gdal_colortable->flush();

  Q_Q(GdalColorGradient);

  emit q->gdalColorTableChanged();
}
}
