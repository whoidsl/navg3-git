/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 9/18/17.
//

#include "MapView.h"
#include "BathyLayer.h"
#include "ContourLine.h"
#include "MapLayer.h"
#include "MapScene.h"
#include "MarkerItem.h"
#include "SymbolLayer.h"
#include "Util.h"

#include <MapView.h>
#include <QContextMenuEvent>
#include <QDebug>
#include <QMenu>
#include <QUuid>
#include <cmath>
#include <QOpenGLWidget>

#define MAX_ZOOM_OUT_METERS 500000.0


namespace dslmap {

Q_LOGGING_CATEGORY(dslmapMapView, "dslmap.mapview")

struct MapViewPrivate
{

public:
  Q_DISABLE_COPY(MapViewPrivate)

  MapViewPrivate()
  {
    m_graticule_pen.setCosmetic(true);
    m_graticule_pen.setWidth(1);
  }

  bool drawGraticule(MapView* view, QPainter* painter, const QRectF& rect);

  int m_zoom_level = 0; //!< Zoom step level
  double m_max_zoom_out_meters = MAX_ZOOM_OUT_METERS;


  // Graticule member variables
  bool m_graticule_visible = true;
  int m_graticule_ticks = 5;
  QPen m_graticule_pen;
  std::shared_ptr<Proj> m_graticule_proj;
  double m_graticule_spacing_width;
  double m_graticule_spacing_height;

  LatLonFormat m_latlon_format = LatLonFormat::DD_NE;
};

bool
MapViewPrivate::drawGraticule(MapView* view, QPainter* painter,
                              const QRectF& rect)
{
  const auto scene = view->mapScene();

  // No scene?  Nothing to do..
  if (!scene) {
    qCDebug(dslmapMapView) << "Skipping drawGraticule: no MapScene";
    return false;
  }

  const auto scene_proj = scene->projectionPtr();

  // No scene projection?
  if (!scene_proj) {
    qCDebug(dslmapMapView) << "Skipping drawGraticule: no MapScene projection";
    return false;
  }

  // No graticule projection?  Create a copy of the scene projection
  if (!m_graticule_proj) {
    m_graticule_proj = Proj::fromDefinition(scene_proj->definition());
    // Error creating new projection?
    if (!m_graticule_proj) {
      qCWarning(dslmapMapView)
        << "Skipping drawGraticule:  could not copy MapScene projection: '"
        << scene_proj->definition() << "'";
      return false;
    }
    qCDebug(dslmapMapView) << "Using existing map scene projection '"
                           << scene_proj->definition() << "' for graticule";
  }

  // Now, map bounding rect in scene coordinates of the view into our graticule
  // coordinates
  const auto is_identity = m_graticule_proj->isSame(*scene_proj);
  const auto graticule_rect = [&]() {
    // Same projection, nothing to do.  Just return a copy of the scene rect.
    if (is_identity) {
      return rect;
    }

    // Scene rect TL, TR, BL, BR
    auto y =
      QVector<qreal>{ rect.top(), rect.top(), rect.bottom(), rect.bottom() };
    auto x =
      QVector<qreal>{ rect.left(), rect.right(), rect.left(), rect.right() };

    // Convert to graticule coordinates
    scene_proj->transformTo(*m_graticule_proj, x, y);

    // Get bounding rect
    auto it = std::minmax_element(std::cbegin(y), std::cend(y));
    const auto bottom = *it.first;
    const auto top = *it.second;

    it = std::minmax_element(std::cbegin(x), std::cend(x));
    const auto left = *it.first;
    const auto right = *it.second;

    // Return new RectF of the visable area in graticule coordinates.  Note that
    // the Y axis is inverted.
    return QRectF{ QPointF{ left, bottom }, QPointF{ right, top } };
  }();

  // First, calculate the tick marks that we want to draw.  This will return
  // a convoluted QPair<QPair<QVector, int>, QPair<QVector, int>> value that
  // contains the x and y tick marks, as well as the recommended precision
  // for the labels.
  auto ticks = graticuleTickMarks(graticule_rect, m_graticule_ticks);

  // Let's break up that structure into workable pieces.
  const auto& x_ticks = ticks.first.first;
  const auto x_precision = ticks.first.second;

  const auto& y_ticks = ticks.second.first;
  const auto y_precision = ticks.second.second;

  const auto n_x = x_ticks.size();
  const auto n_y = y_ticks.size();

  // These lines will be used to calculate the correct label positions for
  // the graticule.  Note that 'top' is the scene rectangle 'bottom', due to
  // the inverted y axis.
  const auto right_edge = QLineF{ rect.topRight(), rect.bottomRight() };
  const auto top_edge = QLineF{ rect.bottomLeft(), rect.bottomRight() };

  // This will hold a segment of the graticule.
  auto graticule_segment = QLineF{};

  // This will hold a computed label point.
  auto label_point = QPointF{};

  // This is our scene->viewport transform.
  auto transform = view->viewportTransform();

  // A list of labels and positions *IN VIEWPORT COORDINATES*
  auto x_label_positions = QList<QPair<QString, QPointF>>{};
  x_label_positions.reserve(n_x);

  // same for y labels.
  auto y_label_positions = QList<QPair<QString, QPointF>>{};
  y_label_positions.reserve(n_y);

  // Time to draw the graticule.  We do this in two steps: first we draw the
  // lines and record where we should place labels, then we place the labels.
  //
  // We do it like this to prevent thrashing the painter transform with a
  // bunch of save() and restore() calls (we need to draw the text
  // untransformed)

  // Time to draw the lines.
  painter->save();
  painter->setPen(m_graticule_pen);

  // Draw verticals.
  auto segment_x = QVector<qreal>{ 0, 0 };
  auto segment_y = QVector<qreal>{ 0, 0 };

  const auto graticule_proj_is_geodetic = m_graticule_proj->isLatLon();
  for (auto x : x_ticks) {

    for (auto i = 0; i < n_y - 1; i++) {
      if (is_identity) {
        graticule_segment.setLine(x, y_ticks.at(i), x, y_ticks.at(i + 1));
      } else {
        // Transform the graticule coordinates into scene coordinates, then draw
        // a line.
        segment_x = { x, x };
        segment_y = { y_ticks.at(i), y_ticks.at(i + 1) };

        m_graticule_proj->transformTo(*scene_proj, segment_x, segment_y);
        graticule_segment.setLine(segment_x.at(0), segment_y.at(0),
                                  segment_x.at(1), segment_y.at(1));
      }
      painter->drawLine(graticule_segment);
    }

    // Find the intersection along the top boarder.  Save it to our list.
    graticule_segment.intersect(top_edge, &label_point);
    if (graticule_proj_is_geodetic) {
      x_label_positions << QPair<QString, QPointF>{
        formatLongitude(x, m_latlon_format, x_precision),
        
        transform.map(label_point)
        
      };
    } else {
      x_label_positions << QPair<QString, QPointF>{
        QString::number(x, 'f', x_precision), transform.map(label_point)
      };
      
    }
  }

  double x0 = QString::number(x_ticks.at(0), 'f', x_precision).toDouble();
  double x1 = QString::number(x_ticks.at(1), 'f', x_precision).toDouble();
  double y0 = QString::number(y_ticks.at(0), 'f', y_precision).toDouble();
  double y1 = QString::number(y_ticks.at(1), 'f', y_precision).toDouble();

  m_graticule_spacing_width = m_graticule_proj->geodeticDistance(x0,y0,x1,y0);
  m_graticule_spacing_height = m_graticule_proj->geodeticDistance(x0,y0,x0,y1);

  // Draw horizontals
  for (const auto y : y_ticks) {
    for (auto i = 0; i < n_x - 1; i++) {
      if (is_identity) {
        graticule_segment.setLine(x_ticks.at(i), y, x_ticks.at(i + 1), y);
      } else {
        segment_x = { x_ticks.at(i), x_ticks.at(i + 1) };
        segment_y = { y, y };

        m_graticule_proj->transformTo(*scene_proj, segment_x, segment_y);
        graticule_segment.setLine(segment_x.at(0), segment_y.at(0),
                                  segment_x.at(1), segment_y.at(1));
      }
      painter->drawLine(graticule_segment);
    }

    // Find the intersection along the right edge.
    graticule_segment.intersect(right_edge, &label_point);
    if (graticule_proj_is_geodetic) {
      y_label_positions << QPair<QString, QPointF>{
        formatLatitude(y, m_latlon_format, y_precision),
        transform.map(label_point)
      };
    } else {
      y_label_positions << QPair<QString, QPointF>{
        QString::number(y, 'f', y_precision), transform.map(label_point)
      };
    }
  }

  painter->restore();

  painter->save();
  painter->setPen(m_graticule_pen);

  // Reset the transform to get back to viewport coordinates.
  painter->resetTransform();

  // We'll need to shift the labels off the very edge of the viewport..
  // otherwise we'd never see them.  To do this we can use the QFontMetrics
  // class to get info about drawn size of text.
  auto metric = view->fontMetrics();

  // the height is constant.
  const auto text_height = metric.height();

  // Label the x ticks.  We need to move these down off the top edge by the
  // constant text height (remember, +y is down)
  foreach (auto item, x_label_positions) {
    item.second.ry() += text_height;
    painter->drawText(item.second, item.first);
  }

  // Label the y ticks.  Need to move these LEFT off the right edge.
  foreach (auto item, y_label_positions) {
    item.second.rx() -= metric.width(item.first);
    item.second.ry() -= metric.height() * 0.25;
    painter->drawText(item.second, item.first);
  }
  // Restore the original painter transform.
  painter->restore();
  return true;
}

MapView::MapView(QWidget* parent)
  : QGraphicsView(parent)
  , d_ptr(new MapViewPrivate)
{

  // Qt's view coordinate system has an origin in the "top-left", so y increases
  // DOWN.  We flip the y coordinate here to match normal geographic coordinates
  scale(1, -1);

  // SS - added this call to use OpenGL for viewport widget.
  //      This call was previously omitted in the code, which was
  //      making the code default to a qwidget with qpainter for rendering
  setViewport(new QOpenGLWidget);

  // Enable full viewport updates by default.
  //setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
  //setViewportUpdateMode(QGraphicsView::MinimalViewportUpdate);
  // SS - commented out

  // Disable scrollbars by default
  setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
  setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

  // Set up the default colors
  setBackgroundBrush(Qt::black);

  // Now adjust some portions of the private structure
  Q_D(MapView);
  d->m_graticule_pen.setCosmetic(true);
  d->m_graticule_pen.setWidth(1);
  d->m_graticule_pen.setColor(Qt::lightGray);
}

MapView::~MapView() = default;

void
MapView::saveSettings(QSettings& settings)
{
  MapView::saveSettings(settings, "mapview/");
}

void
MapView::saveSettings(QSettings& settings, QString prefix)
{
  Q_D(MapView);
  settings.beginGroup("mapview");
  settings.setValue("zoom_threshold", QString::number(d->m_max_zoom_out_meters));
  settings.endGroup();
 
}

void
MapView::loadSettings(const QSettings* settings)
{
  Q_D(MapView);
  auto value = settings->value("mapview/zoom_threshold");
  auto ok = false;
  if (value.isValid()) {
    const auto valid_val = value.toDouble(&ok);
    if (ok) {
      setZoomThreshold(valid_val);
    }
  }
}

void
MapView::setZoomThreshold(double threshold){
  Q_D(MapView);
  if (threshold < 1) {
    // if less than 1 meter...don't even set it
    return;
  }
  if (threshold > MAX_ZOOM_OUT_METERS){
    threshold = MAX_ZOOM_OUT_METERS;
  }

  d->m_max_zoom_out_meters = threshold;
}

double
MapView::getZoomThreshold(){
  Q_D(MapView);
  return d->m_max_zoom_out_meters;

}


void
MapView::drawForeground(QPainter* painter, const QRectF& rect)
{

  // Draw the graticule
  Q_D(MapView);
  if (d->drawGraticule(this, painter, rect)){
    emit gridSpacingChanged(graticuleSpacingWidth(), graticuleSpacingHeight());
  }
  // QGraphicsView::drawForeground(painter, rect);
}

std::shared_ptr<Proj>
MapView::graticuleProjection() const noexcept
{
  const Q_D(MapView);
  return d->m_graticule_proj;
}

Proj*
MapView::graticuleProjectionPtr() const noexcept
{
  const Q_D(MapView);
  return d->m_graticule_proj.get();
}

void
MapView::setGraticuleProjection(std::shared_ptr<Proj> proj)
{
  if (!proj) {
    return;
  }

  Q_D(MapView);
  if (d->m_graticule_proj && d->m_graticule_proj->isSame(*proj)) {
    return;
  }

  d->m_graticule_proj = std::move(proj);
}

void
MapView::zoomExtents()
{

  const auto s = scene();
  if (s == nullptr) {
    return;
  }
  zoom(s->itemsBoundingRect().center(), 0, false);
}

int
MapView::zoomLevel() const noexcept
{
  const Q_D(MapView);
  return d->m_zoom_level;
}

QPointF
MapView::viewSceneCenter() const
{
  // from http://www.qtcentre.org/threads/8591-QGraphicsView-Current-Center
  return mapToScene(viewport()->rect()).boundingRect().center();
}
void
MapView::zoom(int steps, bool relative)
{

  Q_D(MapView);

  auto previous_zoom_level = d->m_zoom_level;

  bool emit_level = false;
  if (relative) {
    d->m_zoom_level += steps;
    if (steps != 0) {
      emit_level = true;
    }
  } else if (d->m_zoom_level != steps) {

    d->m_zoom_level = steps;
    emit_level = true;
  }

  const auto scale_factor = std::pow(1.2, static_cast<double>(d->m_zoom_level));

  // Figure out our aspect ratios.
  const auto viewport_rect = viewport()->rect();
  const auto scene_rect = scene()->sceneRect();
  const auto aspect_factor = qMin(viewport_rect.width() / scene_rect.width(),
                                  viewport_rect.height() / scene_rect.height());

  auto view_transform = QTransform::fromScale(aspect_factor * scale_factor,
                                              -aspect_factor * scale_factor);
  auto inv_view_transform = view_transform.inverted();

  QRectF transformed_rect;
  transformed_rect = inv_view_transform.mapRect(viewport_rect);
  auto transformed_rect_min = qMin(transformed_rect.width(), transformed_rect.height());

  // transformed rect min should be the smallest dimension of the displayed viewport window. (in meters)
  // can use for debugging
  
  /* the comparison check is to make sure we still allow to zoom in if for some reason
  ** we were still zoomed out further than the threshold
   */
  if (transformed_rect_min >=d->m_max_zoom_out_meters && d->m_zoom_level <= previous_zoom_level){
    d->m_zoom_level = previous_zoom_level;
    return;
  }

  setTransform(view_transform);
  if (emit_level) {
    emit zoomLevelChanged(d->m_zoom_level);
  }
}

void
MapView::zoom(const QPointF& center, int steps, bool relative)
{
  zoom(steps, relative);
  centerOn(center);
}

void
MapView::zoomToRect(const QRectF old_rect)
{
  Q_D(MapView);
  auto rect = old_rect.normalized();

  if (rect.isNull()){
    return;
  }

  // Figure out our aspect ratios.
  const auto viewport_rect = viewport()->rect();
  const auto scene_rect = scene()->sceneRect();

  const auto scene_factor = qMin(scene_rect.width() / rect.width(),
                                 scene_rect.height() / rect.height());
  const auto view_factor = qMin(viewport_rect.width() / rect.width(),
                                viewport_rect.height() / rect.height());

  auto view_transform = QTransform::fromScale(view_factor, -view_factor);
  /*
  // This block of code left commented out here would be the start of 
  // attempting to account for if the Rect we zoomed into was somehow larger
  // than navg3s mapscene projection could handle. The attempt was to
  // not zoom in fully if that were the case
  auto inv_view_transform = view_transform.inverted();

  QRectF transformed_rect;
  transformed_rect = inv_view_transform.mapRect(viewport_rect);
  auto transformed_rect_min = qMin(transformed_rect.width(), transformed_rect.height());

  while (transformed_rect_min >= d->m_max_zoom_out_meters){
    //scale the viewport zoom down here. We're 
    //not going to zoom in fully if it's too large
    //
    view_factor = view_factor - 0.1;
    qDebug() << "view_factor is " << view_factor;
    view_transform = QTransform::fromScale(view_factor, -view_factor);
    auto inv_view_transform = view_transform.inverted();

    transformed_rect = inv_view_transform.mapRect(viewport_rect);
    transformed_rect_min = qMin(transformed_rect.width(), transformed_rect.height());
  }
  */

  auto level = std::log(scene_factor) / std::log(1.2);
  d->m_zoom_level = static_cast<int>(level);
  bool emit_level = true;

  setTransform(view_transform);
  if (emit_level) {
  
    emit zoomLevelChanged(d->m_zoom_level);
  }
  //Theorectically if this rectangle is too large could cause zoom issues
  centerOn(rect.center());
}

bool
MapView::isGraticuleVisible() const
{
  const Q_D(MapView);
  return d->m_graticule_visible;
}
void
MapView::setGraticuleVisible(bool visible)
{
  if (visible == isGraticuleVisible()) {
    return;
  }

  Q_D(MapView);
  d->m_graticule_visible = visible;
  update();
  emit graticuleVisiblityChanged(visible);
}

QColor
MapView::graticuleColor() const
{
  const Q_D(MapView);
  return d->m_graticule_pen.color();
}

void
MapView::setGraticuleColor(const QColor& color)
{
  Q_D(MapView);
  if (d->m_graticule_pen.color() == color) {
    return;
  }
  d->m_graticule_pen.setColor(color);
  update();
  emit graticuleColorChanged(color);
}

Qt::PenStyle
MapView::graticuleStyle() const
{
  const Q_D(MapView);
  return d->m_graticule_pen.style();
}

void
MapView::setGraticuleStyle(const Qt::PenStyle style)
{
  Q_D(MapView);
  if (d->m_graticule_pen.style() == style) {
    return;
  }

  d->m_graticule_pen.setStyle(style);
  update();
  emit graticuleColorChanged(style);
}

int
MapView::graticuleWidth() const
{
  const Q_D(MapView);
  return d->m_graticule_pen.width();
}
void
MapView::setGraticuleWidth(int width)
{
  if (width < 0) {
    return;
  }

  Q_D(MapView);
  if (d->m_graticule_pen.width() == width) {
    return;
  }

  d->m_graticule_pen.setWidth(width);
  update();
}
double
MapView::graticuleSpacingWidth() const
{
  const Q_D(MapView);
  return d->m_graticule_spacing_width;
}

double
MapView::graticuleSpacingHeight() const
{
  const Q_D(MapView);
  return d->m_graticule_spacing_height;
}

int
MapView::graticuleTickNumber() const
{
  const Q_D(MapView);
  return d->m_graticule_ticks;
}

void
MapView::setGraticuleTickNumber(int ticks)
{
  if (ticks < 2) {
    return;
  }

  Q_D(MapView);
  if (d->m_graticule_ticks == ticks) {
    return;
  }

  d->m_graticule_ticks = ticks;
  update();
  emit graticuleTickNumberChanged(ticks);
}

QPen
MapView::graticulePen() const
{
  const Q_D(MapView);
  return d->m_graticule_pen;
}

void
MapView::setGraticulePen(const QPen& pen)
{
  auto _pen = QPen{ pen };
  _pen.setCosmetic(true);

  Q_D(MapView);
  if (d->m_graticule_pen == _pen) {
    return;
  }

  d->m_graticule_pen = _pen;
  update();
  emit graticulePenChanged(_pen);
}

MapScene*
MapView::mapScene() const
{
  return qobject_cast<MapScene*>(scene());
}

void
MapView::resizeEvent(QResizeEvent* event)
{
  // Resize, then rescale
  QGraphicsView::resizeEvent(event);

  const auto orig_anchor = transformationAnchor();
  // Not sure why we need to set AnchorUnderMouse here; seems like "NoAnchor"
  // should work fine
  setTransformationAnchor(QGraphicsView::AnchorUnderMouse);

  zoom(0, true);

  setTransformationAnchor(orig_anchor);
}

void
MapView::mouseMoveEvent(QMouseEvent* event)
{
  QGraphicsView::mouseMoveEvent(event);

  const auto map_proj = mapScene()->projectionPtr();
  if (!map_proj) {
    return;
  }

  const auto scene_coords = mapToScene(event->pos());
  emit scenePositionUnderMouseChanged(scene_coords);

  const auto graticule_proj = graticuleProjectionPtr();
  if (graticule_proj) {
    auto x = scene_coords.x();
    auto y = scene_coords.y();
    map_proj->transformTo(*graticule_proj, 1, &x, &y);
    emit graticulePositionUnderMouseChanged(QPointF{ x, y });
  }
  const auto depth = depthAt(event->pos());
  if (!qIsNaN(depth)) {
    emit depthUnderMouseChanged(depth);
  }
}

qreal
MapView::depthAt(QPoint pos) const noexcept
{

  const auto scene_ = scene();
  if (!scene_) {
    return qQNaN();
  }

  const auto scene_pos = mapToScene(pos);

  const auto items_ = scene_->items(scene_pos, Qt::IntersectsItemShape,
                                    Qt::DescendingOrder, viewportTransform());
  if (items_.isEmpty()) {
    return qQNaN();
  }

  auto it = std::begin(items_);
  do {
// Item is a BathyObject?
#if 0
    const auto parent_item = (*it)->parentItem();
#endif
    // if (const auto bathy_object = qgraphicsitem_cast<BathyObject*>(*it))
    if (MapItemType::BathyObjectType == (*it)->type()) {
      auto bathy_object = qgraphicsitem_cast<BathyObject*>(*it);
      return bathy_object->valueAtProjectedPos(scene_pos);
    }

#if 1
    // if (const auto contour_layer =
    // qgraphicsitem_cast<ContourLayer*>(parent_item))
    if (MapItemType::ContourLineType == (*it)->type()) {
      auto contour_line = dynamic_cast<ContourLine*>(*it);
      return contour_line->contourLevel();
    }
#endif
  } while (++it != std::end(items_));

  return qQNaN();
}

LatLonFormat
MapView::latLonFormat() const noexcept
{
  const Q_D(MapView);
  return d->m_latlon_format;
}

void
MapView::setLatLonFormat(LatLonFormat format) noexcept
{
  Q_D(MapView);
  if (format == d->m_latlon_format) {
    return;
  }

  d->m_latlon_format = format;
  emit latLonFormatChanged(format);
}
} // end dslmap
