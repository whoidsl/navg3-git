/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
// Created 10-12-18 by J Vaccaro

#include "BeaconLayerPropertiesWidget.h"
#include "BeaconLayer.h"
#include "ui_BeaconLayerPropertiesWidget.h"

namespace dslmap {

BeaconLayerPropertiesWidget::BeaconLayerPropertiesWidget(QWidget* parent)
  : QWidget(parent)
  , ui(new Ui::BeaconLayerPropertiesWidget)
{
  ui->setupUi(this);
}

BeaconLayerPropertiesWidget::~BeaconLayerPropertiesWidget()
{
  delete ui;
}

void
BeaconLayerPropertiesWidget::setLayer(BeaconLayer* layer)
{
  if (layer == nullptr) {
    return;
  }

  ui->general->setLayer(qobject_cast<MapLayer*>(layer));

  auto beacon_symbol =
    std::dynamic_pointer_cast<ShapeMarkerSymbol>(layer->beaconSymbol());
  ui->beacon->setLabelVisible(layer->beaconLabelVisible());
  ui->beacon->setOutlineVisible(layer->beaconOutlineStyle() !=
                                Qt::PenStyle::NoPen);
  ui->beacon->setFillVisible(layer->beaconFillStyle() !=
                             Qt::BrushStyle::NoBrush);
  ui->beacon->setOutlineColor(layer->beaconOutlineColor());
  ui->beacon->setFillColor(layer->beaconFillColor());
  ui->beacon->setLabelColor(layer->beaconLabelColor());
  ui->beacon->setPainterPath(layer->beaconSymbolPtr()->path());
  ui->beacon->setShape(beacon_symbol->shape());
  ui->beacon->setSymbolSize(beacon_symbol->size());

  connect(ui->beacon, &SymbolPropertiesWidget::outlineColorChanged, layer,
          &BeaconLayer::setBeaconOutlineColor);
  connect(ui->beacon, &SymbolPropertiesWidget::fillColorChanged, layer,
          &BeaconLayer::setBeaconFillColor);
  connect(ui->beacon, &SymbolPropertiesWidget::labelColorChanged, layer,
          &BeaconLayer::setBeaconLabelColor);
  //  connect(ui->beacon, &SymbolPropertiesWidget::shapeChanged,
  //          beacon_symbol, &ShapeMarkerSymbol::setShape);
  connect(ui->beacon, &SymbolPropertiesWidget::labelVisibleChanged, layer,
          &BeaconLayer::setBeaconLabelVisible);
  connect(ui->beacon, &SymbolPropertiesWidget::fillVisibleChanged,
          [=](bool visible) {
            if (visible) {
              layer->setBeaconFillStyle(Qt::BrushStyle::SolidPattern);
            } else {
              layer->setBeaconFillStyle(Qt::BrushStyle::NoBrush);
            }
          });
  connect(ui->beacon, &SymbolPropertiesWidget::outlineVisibleChanged,
          [=](bool visible) {
            if (visible) {
              layer->setBeaconOutlineStyle(Qt::PenStyle::SolidLine);
            } else {
              layer->setBeaconOutlineStyle(Qt::PenStyle::NoPen);
            }
          });

  ui->trail->setLayer(layer);
}
} // namespace dslmap
