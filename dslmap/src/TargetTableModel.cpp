/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "TargetTableModel.h"

namespace dslmap {
TargetTableModel::TargetTableModel(const int numColumns, const int numRows,
                                   QVector<QVector<QString>> data,
                                   QObject* parent)
  : QAbstractTableModel(parent)
  , m_numRows(numRows)
  , m_numColumns(numColumns)
  , m_data(data)
{
}

int
TargetTableModel::rowCount(const QModelIndex& parent = QModelIndex()) const
{
  Q_UNUSED(parent);
  return m_numRows;
}

int
TargetTableModel::columnCount(const QModelIndex& parent = QModelIndex()) const
{
  Q_UNUSED(parent);
  return m_numColumns;
}

int
TargetTableModel::getRow()
{
  return m_numRows;
}

int
TargetTableModel::getCol()
{
  return m_numColumns;
}

qreal
TargetTableModel::getRealAt(int row, int column, bool* ok)
{
  auto outData = m_data[row][column].toDouble(ok);
  //    Q_ASSERT (&outData != nullptr);
  return outData;
}

QString
TargetTableModel::getStringAt(int row, int column)
{
  QString outData;
  outData = m_data[row][column].toLatin1();
  // TODO:  Check if we've actually been given a valid pointer in 'ok'
  return outData;
}

QVariant
TargetTableModel::data(const QModelIndex& index,
                       int role = Qt::DisplayRole) const
{
  if (!index.isValid() || role != Qt::DisplayRole)
    return QVariant();

  // Return the data to which index points.
  return m_data[index.row()][index.column()];
}
} // END NAMESPACE DSLMAP
