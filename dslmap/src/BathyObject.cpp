/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 10/13/17.
//

#include "BathyObject.h"
#include "GdalColorGradient.h"
#include "GdalDataset.h"
#include "GdalUtils.h"
#include "MapItem.h"
#include "Proj.h"
#include "TemporaryGdalDataset.h"
#include "ImageTilingHelper.h"

#include <QBitmap>
#include <QMessageBox>
#include <QPainter>
#include <gdal/gdal_priv.h>

namespace dslmap {

Q_LOGGING_CATEGORY(dslmapBathyObject, "dslmap.object.bathy")

class BathyObjectPrivate
{
public:
  explicit BathyObjectPrivate()
    : m_gradient(std::make_shared<GdalColorGradient>())
    , m_gradient_change_connection(std::make_unique<QMetaObject::Connection>())
  {
  }

  ~BathyObjectPrivate() = default;

  std::shared_ptr<const Proj> m_proj;

  ///\brief Original bathy dataset on disk
  std::shared_ptr<GdalDataset> m_dataset;

  ///\brief Projected bathy dataset
  std::shared_ptr<GdalDataset> m_projected_dataset;

  ///\brief Projected dataset converted to a PNG using gdaldem color-relief
  std::shared_ptr<TemporaryGdalDataset> m_projected_relief_image_dataset;

  ///\brief Hillshade dataset crated using gdaldem hillshade
  std::shared_ptr<TemporaryGdalDataset> m_projected_hillshade_dataset;

  /// \brief Original->Projected transform
  ///
  /// Maps dataset x,y -> map scene coordinates
  QTransform m_projection_transform;

  /// \brief Projected->Original transform
  ///
  /// Maps map scene coordinates -> dataset x,y
  QTransform m_inverse_projection_transform;

  bool m_hillshade_enabled = true;

  int m_band = 1; ///< What layer to use from the dataset (default=1)

  QRectF m_projected_dataset_bounds = {};

  qreal m_dataset_min; ///< Minimum value of the dataset
  qreal m_dataset_max; ///< Maximum value of the dataset

  std::vector<QGraphicsPixmapItem*> m_projected_pixmaps;

  /// \brief Bathymetry gradient
  std::shared_ptr<GdalColorGradient> m_gradient;

  std::unique_ptr<QMetaObject::Connection> m_gradient_change_connection;
};

BathyObject::BathyObject(QGraphicsItem* parent)
  : QGraphicsObject(parent)
  , d_ptr(new BathyObjectPrivate)
{

  Q_D(BathyObject);

  // Update our shaded relief image when the colortable changes.
  if (d->m_gradient) {
    Q_D(BathyObject);
    *d->m_gradient_change_connection =
      connect(d->m_gradient.get(), &GdalColorGradient::gdalColorTableChanged,
              [this]() { this->update(UpdateOption::UpdateShadedRelief); });
  }

  setFlag(ItemHasNoContents, true);
}

BathyObject::~BathyObject()
{
  delete d_ptr;
}

void
BathyObject::setGradient(std::shared_ptr<GdalColorGradient> gradient)
{
  if (!gradient) {
    return;
  }

  Q_D(BathyObject);
  d->m_gradient.swap(gradient);

  // Disconnect the old gradient if present
  // TODO: Fix gradient connections
  if (gradient) {
    disconnect(*d->m_gradient_change_connection);
  }

  // Connect to the new gradient.
  *d->m_gradient_change_connection =
    connect(d->m_gradient.get(), &GdalColorGradient::gdalColorTableChanged,
            [this]() { this->update(UpdateOption::UpdateShadedRelief); });

  if (!d->m_projected_dataset) {
    return;
  }

  // Update the shaded relief image.
  update(UpdateOption::UpdateShadedRelief);
}

std::shared_ptr<GdalColorGradient>
BathyObject::gradient() const
{
  const Q_D(BathyObject);
  return d->m_gradient;
}

bool
BathyObject::setRasterFile(const QString& filename, int band,
                           std::shared_ptr<Proj> proj)
{

  Q_D(BathyObject);

  // First we load it.
  auto dataset = std::make_unique<GdalDataset>(filename);
  if (!dataset->open()) {
    qCWarning(dslmapBathyObject) << "Unable to load raster:" << filename;
    return false;
  }

  if (proj) {
    qCInfo(dslmapBathyObject) << "Using projection override: "
                              << proj->definition();
    dataset->setProjection(proj);
  } else if (!dataset->projection()) {
    qCInfo(dslmapBathyObject) << "Dataset has no projection, assuming WGS84";
    dataset->setProjection(Proj::fromDefinition(PROJ_WGS84_DEFINITION));
  }

  d->m_band = band;
  d->m_dataset = std::move(dataset);
  emit datasetChanged();
  update(UpdateOption::UpdateAll);

  return true;
}

void
BathyObject::setHillshadeEnabled(bool enabled)
{

  Q_D(BathyObject);
  if (enabled == d->m_hillshade_enabled) {
    return;
  }

  d->m_hillshade_enabled = enabled;

  // Do we need to generate a new hillshade dataset?
  if (enabled && !d->m_projected_hillshade_dataset) {
    update(UpdateOption::UpdateHillshade);
  } else {
    update(UpdateOption::UpdatePixmap);
  }
}

bool
BathyObject::isHillshadeEnabled() const noexcept
{
  const Q_D(BathyObject);
  return d->m_hillshade_enabled;
}

QRectF
BathyObject::boundingRect() const
{
  // return childrenBoundingRect();
  const Q_D(BathyObject);
  return d->m_projected_dataset_bounds;
}

void
BathyObject::paint(QPainter* painter, const QStyleOptionGraphicsItem* option,
                   QWidget* widget)
{
  Q_UNUSED(painter);
  Q_UNUSED(option);
  Q_UNUSED(widget);
}

int
BathyObject::type() const
{
  return MapItemType::BathyObjectType;
}

const Proj*
BathyObject::datasetProjection() const
{
  const Q_D(BathyObject);
  if (!d->m_dataset) {
    return nullptr;
  }

  return d->m_dataset->projection().get();
}

void
BathyObject::setProjection(std::shared_ptr<const Proj> proj)
{
  Q_D(BathyObject);

  if (!proj) {
    return;
  }

  if (!d->m_dataset) {
    d->m_proj = proj;
    return;
  }

  if (d->m_proj->isSame(*proj)) {
    qCDebug(dslmapBathyObject, "Projection is equivalent.. skipping warp.");
    return;
  }

  d->m_proj = std::move(proj);
  update(UpdateOption::UpdateProjection | UpdateOption::UpdateShadedRelief |
         UpdateOption::UpdateHillshade);

  // qCDebug(dslmapBathyObject, "Calling GdalDataset.warp ...");
  // auto proj_dataset = d->m_dataset->warp(*proj);
  // if (!proj_dataset) {
  //   return;
  // }

  // prepareGeometryChange();
  // if (!d->updateProjectedDataset(proj_dataset)) {
  //   return;
  // }
}
std::shared_ptr<const Proj>
BathyObject::projection() const noexcept
{
  const Q_D(BathyObject);
  return d->m_proj;
}

const Proj*
BathyObject::projectionPtr() const noexcept
{
  const Q_D(BathyObject);
  return d->m_proj.get();
}

GDALDataset*
BathyObject::dataset()
{
  Q_D(BathyObject);
  if (!d->m_dataset) {
    return nullptr;
  }
  return d->m_dataset->get();
}

int
BathyObject::band() const noexcept
{
  const Q_D(BathyObject);
  return d->m_band;
}

void
BathyObject::setBand(int band)
{
  Q_D(BathyObject);
  if (band == d->m_band) {
    return;
  }

  if (band < 1) {
    return;
  }

  d->m_band = band;

  update(UpdateOption::UpdateBand);
}

qreal
BathyObject::minValue() const noexcept
{
  const Q_D(BathyObject);
  return d->m_dataset_min;
}

qreal
BathyObject::maxValue() const noexcept
{
  const Q_D(BathyObject);
  return d->m_dataset_max;
}

QPair<qreal, qreal>
BathyObject::minMaxValues() const noexcept
{
  const Q_D(BathyObject);
  return { d->m_dataset_min, d->m_dataset_max };
}

bool
BathyObject::update(UpdateOptions options)
{
  qCDebug(dslmapBathyObject) << "Calling update with" << options;

  if (options == UpdateOption::UpdateNone) {
    return true;
  }
  Q_D(BathyObject);

  if (options.testFlag(UpdateOption::UpdateBand)) {
    qCDebug(dslmapBathyObject) << "Updating raster band";
    auto band = d->m_dataset->get()->GetRasterBand(d->m_band);
    if (!band) {
      qCWarning(dslmapBathyObject) << "Unable to switch to band:" << d->m_band;
      return false;
    }

    d->m_dataset_min = band->GetMinimum();
    d->m_dataset_max = band->GetMaximum();

    if (!d->m_gradient) {
      qCWarning(dslmapBathyObject) << "ERROR:  Gradient is null, this is a BUG";
      return false;
    }

    if (d->m_dataset_min < d->m_gradient->min() ||
        d->m_dataset_max > d->m_gradient->max()) {
      const auto _min = std::fmin(d->m_dataset_min, d->m_gradient->min());
      const auto _max = std::fmin(d->m_dataset_max, d->m_gradient->max());

      const auto orig = d->m_gradient->blockSignals(true);
      d->m_gradient->setRange(_min, _max);
      d->m_gradient->blockSignals(orig);

      qCInfo(dslmapBathyObject)
        << "Queueing update to shaded relief image due to gradient change.";
      options |= UpdateOption::UpdateShadedRelief;
    }
  }
  if (options.testFlag(UpdateOption::UpdateProjection)) {

    qCDebug(dslmapBathyObject) << "Updating projection";
    if (!d->m_dataset->projection()) {
      qCWarning(dslmapBathyObject) << "Raster file:" << d->m_dataset->fileName()
                                   << "has no associated projection.";
      return false;
    }

    if (!d->m_proj) {
      qCDebug(dslmapBathyObject) << "No projection assigned to object.  Using "
                                    "projection read from dataset.";
      d->m_proj = d->m_dataset->projection();
    }

    if (d->m_dataset->projection()->isSame(*d->m_proj)) {
      qCDebug(dslmapBathyObject) << "No need to reproject dataset.";
      d->m_projected_dataset = d->m_dataset;
    } else {
      // Need the cast here.  End the end we dont' care if the the dataset is
      // temporary or permanent for usage.
      auto proj_dataset = std::static_pointer_cast<GdalDataset>(
        d->m_dataset->warp(*d->m_proj));

      if (!proj_dataset) {
        qCWarning(dslmapBathyObject) << "Unable to reproject raster:"
                                     << d->m_dataset->fileName();
        return false;
      }

      qCDebug(dslmapBathyObject)
        << "Reprojected raster file:" << d->m_dataset->fileName()
        << "to file:" << proj_dataset->fileName();

      d->m_projected_dataset.swap(proj_dataset);
    }

    if (d->m_projected_dataset->boundingRect() !=
        d->m_projected_dataset_bounds) {
      prepareGeometryChange();
      d->m_projected_dataset_bounds =
        mapRectFromScene(d->m_projected_dataset->boundingRect());
    }

    qCInfo(dslmapBathyObject) << "Queuing updates to Shaded Relief and "
                                 "HillShade images due to projection change.";
    options |= UpdateOption::UpdateShadedRelief | UpdateOption::UpdateHillshade;
  }

  if (options.testFlag(UpdateOption::UpdateShadedRelief)) {
    qCDebug(dslmapBathyObject) << "Updating shaded relief image";

    if (!d->m_projected_dataset) {
      qCDebug(dslmapBathyObject)
        << "No projected dataset, skipping shaded relief calculation";
      return false;
    }

    auto proj_relief_image = d->m_projected_dataset->colorRelief(
      d->m_gradient->gdalColorTableFileName(),
      { "-of", "PNG", "-b", QString::number(d->m_band) });

    if (!proj_relief_image) {
      qCWarning(dslmapBathyObject) << "Unable to create shaded relief for file:"
                                   << d->m_projected_dataset->fileName();
      return false;
    }

    qCDebug(dslmapBathyObject) << "Created new shaded relief image:"
                               << proj_relief_image->fileName();

    // Swap in the new projected relief image.
    d->m_projected_relief_image_dataset.swap(proj_relief_image);
    options |= UpdateOption::UpdatePixmap;
  }

  if (options.testFlag(UpdateOption::UpdateHillshade)) {
    qCDebug(dslmapBathyObject) << "Updating hill shade image";
    if (!d->m_projected_dataset) {
      qCDebug(dslmapBathyObject)
        << "No projected dataset, skipping hillshade calculation";
      return false;
    }

    auto proj_hillshade_image = d->m_projected_dataset->hillshade(
      { "-of", "PNG", "-b", QString::number(d->m_band) });

    if (!proj_hillshade_image) {
      qCWarning(dslmapBathyObject) << "Unable to create hillshade layer.";
      return false;
    }
    d->m_projected_hillshade_dataset.swap(proj_hillshade_image);
    options |= UpdateOption::UpdatePixmap;
  }

  // Rebuild pixmap if needed
  // Load the projected data from the data file.
  if (options.testFlag(UpdateOption::UpdatePixmap)) {

    qCDebug(dslmapBathyObject) << "Recreating pixmap due to updates to shaded "
                                  "relief or hillshade images";

    // Get the projection transform
    auto adf_transform = QVector<double>{ 0, 0, 0, 0, 0, 0 };
    d->m_projected_relief_image_dataset->get()->GetGeoTransform(
      adf_transform.data());

    // Create the transformation matrix
    auto ok = false;
    const auto transform = gdalGeometryToTransform(adf_transform, &ok);
    if (!ok) {
      qCWarning(dslmapBathyObject)
        << "Unable to calculate perspective transform given GDAL transform:"
        << adf_transform;
      return false;
    }

    // Delete the old pixmap tiling items
    for (auto pixmap_item : d->m_projected_pixmaps) {
      pixmap_item->setParentItem(nullptr);
      delete pixmap_item;
    }

    d->m_projected_pixmaps.clear();

    d->m_projection_transform = transform;
    d->m_inverse_projection_transform = transform.inverted();

    QImage reliefImage(d->m_projected_relief_image_dataset->fileName(), "PNG");

    if (!isHillshadeEnabled()) {
      d->m_projected_pixmaps =
        ImageTilingHelper::getImageTiles(reliefImage, transform);
     // d->m_projected_pixmaps.push_back(
       // new QGraphicsPixmapItem(QPixmap::fromImage(reliefImage)));
    } else {
      // Compositing ripped from
      // http://doc.qt.io/qt-5/qtwidgets-painting-imagecomposition-example.html
      // example
      QImage hillshadeImage(d->m_projected_hillshade_dataset->fileName(),
                            "PNG");

      QImage result(hillshadeImage.size(), QImage::Format_ARGB32_Premultiplied);
      QPainter blendPainter(&result);

      blendPainter.setCompositionMode(QPainter::CompositionMode_Source);
      blendPainter.fillRect(result.rect(), Qt::transparent);
      blendPainter.setCompositionMode(QPainter::CompositionMode_SourceOver);
      blendPainter.drawImage(0, 0, reliefImage);
      blendPainter.setCompositionMode(QPainter::CompositionMode_Overlay);
      blendPainter.drawImage(0, 0, hillshadeImage);
      blendPainter.end();

      d->m_projected_pixmaps =
        ImageTilingHelper::getImageTiles(result, transform);
    }

    for (auto pixmap_item : d->m_projected_pixmaps) {
      pixmap_item->setParentItem(this);
    }
  }

  return true;
}

qreal
BathyObject::valueAtProjectedPos(QPointF pos)
{
  Q_D(BathyObject);
  if (!d->m_projected_dataset) {
    return qQNaN();
  }

  auto dataset_ptr = d->m_projected_dataset->get();
  auto band_ptr = dataset_ptr->GetRasterBand(band());
  if (!band_ptr) {
    return qQNaN();
  }

  qreal x;
  qreal y;
  d->m_inverse_projection_transform.map(pos.x(), pos.y(), &x, &y);

  auto value = double{ 0.0 };
  if (CE_None !=
      band_ptr->RasterIO(GF_Read, static_cast<int>(x), static_cast<int>(y), 1,
                         1, static_cast<void*>(&value), 1, 1, GDT_Float64, 1,
                         1)) {
    qCWarning(dslmapBathyObject)
      << "Unable to read data from projected dataset: " << pos << "->"
      << static_cast<int>(x) << static_cast<int>(y);
    return qQNaN();
  }

  return value;
}
}
