/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "BathyLayer.h"
#include "BathyLayerPropertiesWidget.h"
#include "BathyObject.h"
#include "GeneralLayerPropertiesWidget.h"
#include "Proj.h"

#include <QFileInfo>
#include <QMenu>

namespace dslmap {
Q_LOGGING_CATEGORY(dslmapBathyLayer, "dslmap.layer.bathy")

class BathyLayerPrivate
{
public:
  QList<BathyObject*> m_objects;
  QRectF m_bounding_rect;
  QMenu* m_menu;
};

BathyLayer::BathyLayer(QGraphicsItem* parent)
  : MapLayer(parent)
  , d_ptr(new BathyLayerPrivate)
{
  Q_D(BathyLayer);
  setFlag(ItemHasNoContents, true);
  d->m_menu = MapLayer::contextMenu();
  auto underlay_action = d->m_menu->addAction(QStringLiteral("Save Bathy Underlay in Settings"));
  connect(underlay_action, &QAction::triggered, this,
          [=]() { saveUnderlay(); });
  
  // a quick solution to set the default layer opacity. 
  // note this isnt done of the bathy object itself because the layer property tools take the "layer" values, not any child objects
  setOpacity(50/100.0);
}

BathyLayer::~BathyLayer() = default;

QList<BathyObject*>
BathyLayer::bathyObjects() const
{
  const Q_D(BathyLayer);
  return d->m_objects;
}

BathyObject*
BathyLayer::addRasterFile(const QString& filename, int band)
{
  source_filename = filename;
  auto bathy = new BathyObject;
  if (!bathy->setRasterFile(filename, band)) {
    delete bathy;
    return nullptr;
  }

  Q_D(BathyLayer);
  auto proj = projection();
  qCDebug(dslmapBathyLayer) << "Layer projection is:" << proj->definition();
  if (proj) {
    bathy->setProjection(proj);
  }

  const auto name = QFileInfo(filename).baseName();
  bathy->setObjectName(name);

  // Set the layer name if this is the first raster added
  if (d->m_objects.isEmpty()) {
    setName(name);
  }

  // This takes ownership of the object
  bathy->setParentItem(this);
  d->m_bounding_rect |= mapRectFromItem(bathy, bathy->boundingRect());
  prepareGeometryChange();
  d->m_objects.append(bathy);

  return bathy;
}

void
BathyLayer::showPropertiesDialog()
{
  auto widget = new BathyLayerPropertiesWidget();
  widget->setLayer(this);
  widget->show();
}

void
BathyLayer::setProjection(std::shared_ptr<const Proj> proj)
{
  if (!proj) {
    return;
  }

  auto p = projectionPtr();
  Q_D(BathyLayer);
  if (p->isSame(*proj)) {
    return;
  }

  d->m_bounding_rect = QRectF{};
  for (auto& bathy : d->m_objects) {
    bathy->setProjection(proj);
    d->m_bounding_rect |= mapRectFromItem(bathy, bathy->boundingRect());
  }

  prepareGeometryChange();
  MapLayer::setProjection(proj);
}

bool
BathyLayer::saveSettings(QSettings& settings)
{
  Q_UNUSED(settings)
  return false;
}

bool
BathyLayer::loadSettings(QSettings& settings)
{
  Q_UNUSED(settings)
  return false;
}

QRectF
BathyLayer::boundingRect() const
{
  const Q_D(BathyLayer);
  return d->m_bounding_rect;
}

void
BathyLayer::paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*)
{
}

std::shared_ptr<GdalColorGradient>
BathyLayer::gradient() const
{
  const Q_D(BathyLayer);
  if (d->m_objects.empty()) {
    return nullptr;
  }
  return d->m_objects[0]->gradient();
}

void
BathyLayer::setGradient(std::shared_ptr<GdalColorGradient> gradient)
{
  Q_D(BathyLayer);
  if (d->m_objects.empty()) {
    return;
  }
  d->m_objects[0]->setGradient(gradient);
  return;
}

qreal
BathyLayer::minValue() const noexcept
{
  const Q_D(BathyLayer);
  return d->m_objects[0]->minValue();
}

qreal
BathyLayer::maxValue() const noexcept
{
  const Q_D(BathyLayer);
  return d->m_objects[0]->maxValue();
}

QPair<qreal, qreal>
BathyLayer::minMaxValues() const noexcept
{
  const Q_D(BathyLayer);
  return { d->m_objects[0]->minValue(), d->m_objects[0]->maxValue() };
}

void
BathyLayer::saveUnderlay(){
    emit saveBathyUnderlay(source_filename);
}

}
