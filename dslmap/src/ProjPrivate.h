/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 10/19/17.
//

#ifndef NAVG_PROJPRIVATE_H
#define NAVG_PROJPRIVATE_H

#include "Proj.h"

#include <QMutex>
namespace dslmap {
class ProjPrivate
{
  Q_DISABLE_COPY(ProjPrivate)

  ProjPrivate(Proj* p, projPJ proj, std::shared_ptr<projCtx_> context);

  std::unique_ptr<projPJ_, void (*)(projPJ)> m_proj; ///< The projection struct.
  std::shared_ptr<projCtx_> m_ctx;                   ///< The context used.
  mutable int m_err = 0;                             ///< Last error.
  QString m_def;            ///< The proj4 string defintion.
  mutable QMutex m_mutex;   ///< Mutex for changing the projection settings.
  bool m_is_latlon = false; ///< Projection is geographic.
  Unit m_unit = Unit::custom;
  qreal m_scale_factor = 0;
  QVariantHash m_args;

private:
  Q_DECLARE_PUBLIC(Proj)
  Proj* q_ptr;
};
}

#endif // NAVG_PROJPRIVATE_H
