/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 12/13/18.
//

#include "TargetExportDialog.h"
#include "ui_TargetExportDialog.h"

#include "SymbolLayer.h"
#include "TargetTableModel.h"

#include <QFileDialog>

namespace dslmap {

TargetExportDialog::TargetExportDialog(QWidget* parent)
  : QDialog(parent)
  , ui(new Ui::TargetExportDialog)
{
  ui->setupUi(this);

  ui->delimEdit->setVisible(false);
  ui->delimBox->addItem("comma", ",");
  ui->delimBox->addItem("semi-colon", ";");
  ui->delimBox->addItem("tab", "\t");
  ui->delimBox->addItem("line", "\n");
  ui->delimBox->addItem("space", " ");
  ui->delimBox->addItem("other", "");
  // Open a file when the file button is clicked
  connect(ui->fileBtn, &QPushButton::clicked, [=]() {
    auto fileStr = QFileDialog::getSaveFileName(
      this, tr("Save Targets"), "",
      tr("Data Files (*.csv *.dat *.txt *.tgt);;All Files (*)"));
    ui->fileEdit->setText(fileStr);
  });
  connect(ui->delimBox, static_cast<void (QComboBox::*)(int)>(
                          &QComboBox::currentIndexChanged),
          [=]() { ui->delimEdit->setVisible(true); });
}

TargetExportDialog::~TargetExportDialog()
{
  delete ui;
}

void
TargetExportDialog::setLayer(SymbolLayer* layer)
{
  layer->printLatLonLabels();
  QVector<QVector<QString>> tableData;

  QVector<QString> headers{ "#", "Lat", "Lon", "Label" };
  tableData.push_back(headers);

  setTableData(tableData);
}

void
TargetExportDialog::setTableData(QVector<QVector<QString>> tableData)
{
  auto rows = tableData.size();
  if (rows < 1) {
    return;
  }
  auto columns = tableData[0].size();
  if (columns < 1) {
    return;
  }

  TargetTableModel* model = new TargetTableModel(columns, rows, tableData);
  auto* m = ui->preView->model();
  ui->preView->setModel(model);
  delete m; // Prevent memory leak of old models
}

QString
TargetExportDialog::filename()
{
  return ui->fileEdit->text();
}

QString
TargetExportDialog::delim()
{
  // Update delimiter to user selection. If "Other" use delimEdit
  int delimIndex = ui->delimBox->currentIndex();
  QString delimTxt = ui->delimBox->currentText();

  QString delimStr = ui->delimBox->itemData(delimIndex).toString();
  if (delimStr == "") {
    delimStr = ui->delimEdit->text();
  }
  return delimStr;
  //  ui->delimEdit->setVisible(true);
}

} // namespace
