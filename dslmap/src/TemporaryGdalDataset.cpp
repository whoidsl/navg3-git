/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 10/24/17.
//

#include "TemporaryGdalDataset.h"
#include "TemporaryGdalDatasetPrivate.h"

#include <QTemporaryFile>

namespace dslmap {

TemporaryGdalDataset::TemporaryGdalDataset(QObject* parent)
  : GdalDataset(*new TemporaryGdalDatasetPrivate(this), parent)
{
  Q_D(TemporaryGdalDataset);
  d->m_tempfile->open();
  d->m_filename = d->m_tempfile->fileName();
}

TemporaryGdalDataset::TemporaryGdalDataset(const QString& nameTemplate,
                                           QObject* parent)
  : GdalDataset(*new TemporaryGdalDatasetPrivate(this), parent)
{
  Q_D(TemporaryGdalDataset);
  d->m_tempfile->setFileTemplate(nameTemplate);
  d->m_tempfile->open();
  d->m_filename = d->m_tempfile->fileName();
}

TemporaryGdalDataset::TemporaryGdalDataset(TemporaryGdalDatasetPrivate& pimpl,
                                           QObject* parent)
  : GdalDataset(pimpl, parent)
{
}

TemporaryGdalDataset::~TemporaryGdalDataset() = default;

void
TemporaryGdalDataset::setFileName(const QString& name)
{
  Q_UNUSED(name);
}
}