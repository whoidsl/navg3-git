/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 10/23/17.
//

#include "GdalColorGradient.h"
#include "GdalColorGradientPrivate.h"

#include <GdalColorGradient.h>
#include <QColor>
#include <QLinearGradient>
#include <QPropertyAnimation>
#include <QTemporaryFile>
#include <QtDebug>
#include <QtGui/QtGui>

namespace dslmap {

Q_LOGGING_CATEGORY(dslmapGdalColorGradient, "dslmap.colorgradient")

GdalColorGradient::GdalColorGradient()
  : d_ptr(new GdalColorGradientPrivate(this))
{
  setStops({ { 0, Qt::black }, { 1, Qt::white } });
}

GdalColorGradient::GdalColorGradient(const QGradientStops& stops)
  : d_ptr(new GdalColorGradientPrivate(this))
{
  setStops(stops);
}

GdalColorGradient::GdalColorGradient(const QLinearGradient& gradient)
  : GdalColorGradient(gradient.stops())
{
}

GdalColorGradient::GdalColorGradient(GdalColorGradient::GradientPreset preset)
  : d_ptr(new GdalColorGradientPrivate(this))
{
  setGradient(preset);
}

bool
GdalColorGradient::operator==(const GdalColorGradient& other) const
{
  return isEqual(other);
}

bool
GdalColorGradient::operator!=(const GdalColorGradient& other) const
{
  return !isEqual(other);
}

bool
GdalColorGradient::isEqual(const GdalColorGradient& other) const
{
  return isAbsolute() == other.isAbsolute() && min() == other.min() &&
         max() == other.max() &&
         // no need to also check preset, since preset is only used to set stops
         stops() == other.stops() && noDataVisible() == other.noDataVisible() &&
         noDataColor() == other.noDataColor();
}

void
GdalColorGradient::setGradient(const QLinearGradient& gradient)
{

  const auto stops = gradient.stops();
  setStops(stops);
}

void
GdalColorGradient::setGradient(GdalColorGradient::GradientPreset preset)
{

  const auto stops = presetStops(preset);
  if (stops.empty()) {
    return;
  }
  setStops(stops);
  Q_D(GdalColorGradient);
  d->m_preset = preset;
}

GdalColorGradient::GradientPreset
GdalColorGradient::preset() const
{
  const Q_D(GdalColorGradient);
  return d->m_preset;
}

void
GdalColorGradient::setMin(qreal min)
{
  Q_D(GdalColorGradient);
  if (min == d->m_min) {
    return;
  }

  d->m_min = min;
  d->m_spread = d->m_max - d->m_min;
  d->updateGdalColortable();
}
qreal
GdalColorGradient::min() const
{
  const Q_D(GdalColorGradient);
  return d->m_min;
}

void
GdalColorGradient::setMax(qreal max)
{
  Q_D(GdalColorGradient);
  if (max == d->m_max) {
    return;
  }

  d->m_max = max;
  d->m_spread = d->m_max - d->m_min;
  d->updateGdalColortable();
}

qreal
GdalColorGradient::max() const
{
  const Q_D(GdalColorGradient);
  return d->m_max;
}

void
GdalColorGradient::setRange(qreal min, qreal max)
{
  Q_D(GdalColorGradient);
  if (min == d->m_min && max == d->m_max) {
    return;
  }

  d->m_min = min;
  d->m_max = max;
  d->m_spread = d->m_max - d->m_min;
  d->updateGdalColortable();
}
void
GdalColorGradient::setRange(const QPair<qreal, qreal>& range)
{
  setRange(range.first, range.second);
}

qreal
GdalColorGradient::spread() const
{
  const Q_D(GdalColorGradient);
  return d->m_spread;
}

QPair<qreal, qreal>
GdalColorGradient::range() const
{
  return { min(), max() };
}

QGradientStops
GdalColorGradient::stops() const
{
  return gradient().stops();
}

void
GdalColorGradient::setStops(const QGradientStops& stopPoints)
{

  Q_D(GdalColorGradient);

  auto interp = std::make_unique<QPropertyAnimation>();
  interp->setDuration(100);

  foreach (auto& stop, stopPoints) {
    interp->setKeyValueAt(stop.first, stop.second);
  }

  d->m_interpolator.swap(interp);
  d->updateGdalColortable();
}

QLinearGradient
GdalColorGradient::gradient() const
{

  const Q_D(GdalColorGradient);
  if (!d->m_interpolator) {
    return {};
  }

  auto grad = QLinearGradient{};
  foreach (auto& stop, d->m_interpolator->keyValues()) {
    grad.setColorAt(stop.first, stop.second.value<QColor>());
  }

  return grad;
}

QColor
GdalColorGradient::colorAt(qreal value) const
{
  const Q_D(GdalColorGradient);

  if (!d->m_interpolator) {
    return {};
  }

  if (d->m_absolute) {
    if (value > d->m_max) {
      value = d->m_max;
    }

    if (value < d->m_min) {
      value = d->m_min;
    }

    value = (value - d->m_min) / d->m_spread;
  }

  if (value < 0 || value > 1) {
    return {};
  }
  // hardcoded duration for the interpolator used in setStops
  d->m_interpolator->setCurrentTime(value * 100);
  return d->m_interpolator->currentValue().value<QColor>();
}

void
GdalColorGradient::setAbsolute(bool absolute)
{
  Q_D(GdalColorGradient);
  if (d->m_absolute == absolute) {
    return;
  }

  d->m_absolute = absolute;
}

bool
GdalColorGradient::isAbsolute() const
{
  const Q_D(GdalColorGradient);
  return d->m_absolute;
}

QString
GdalColorGradient::gdalColorTableFileName() const
{
  const Q_D(GdalColorGradient);
  return d->m_gdal_colortable->fileName();
}

void
GdalColorGradient::setNoDataVisible(const bool visible)
{
  Q_D(GdalColorGradient);
  if (d->m_nodata_visible == visible) {
    return;
  }
  d->m_nodata_visible = visible;
  d->updateGdalColortable();
}

bool
GdalColorGradient::noDataVisible() const noexcept
{
  const Q_D(GdalColorGradient);
  return d->m_nodata_visible;
}

void
GdalColorGradient::setNoDataColor(const QColor& color)
{
  Q_D(GdalColorGradient);
  if (!color.isValid()) {
    return;
  }
  if (d->m_nodata_color == color) {
    return;
  }
  d->m_nodata_color = color;
  d->updateGdalColortable();
}

QColor
GdalColorGradient::noDataColor() const noexcept
{
  const Q_D(GdalColorGradient);
  return d->m_nodata_color;
}

QColor
GdalColorGradient::noDataDisplayColor() const noexcept
{
  const Q_D(GdalColorGradient);
  QColor color = noDataColor();
  if (!noDataVisible()) {
    color.setAlpha(0);
  }
  return color;
}

QGradientStops
GdalColorGradient::presetStops(GdalColorGradient::GradientPreset preset)
{

  switch (preset) {
    case GradientPreset::Greyscale:
      return { { 0, Qt::black }, { 1, Qt::white } };
    case GradientPreset::Rainbow:
      return { { 0, Qt::red },
               { 0.3, Qt::yellow },
               { 0.5, Qt::green },
               { 0.8, Qt::cyan },
               { 1.0, Qt::blue } };
    case GradientPreset::DarkRainbow:
      return { { 0, Qt::darkRed },
               { 0.3, Qt::darkYellow },
               { 0.6, Qt::darkGreen },
               { 0.8, Qt::darkCyan },
               { 1.0, Qt::darkBlue } };
    case GradientPreset::Cool:
      return {
        { 0, Qt::darkGreen },  { 0.2, Qt::green }, { 0.4, Qt::cyan },
        { 0.6, Qt::darkCyan }, { 0.8, Qt::blue },  { 1.0, Qt::darkBlue }
      };
    case GradientPreset::Warm:
      return { { 0, Qt::darkRed },
               { 0.3, Qt::red },
               { 0.7, Qt::yellow },
               { 1.0, Qt::darkYellow } };
    case GradientPreset::Hot:
      return { { 0, QColor{ 0xFF833AB4 } },
               { 0.5, QColor{ 0xFFFD1D1D } },
               { 1, QColor{ 0xFFFCB045 } } };
    case GradientPreset::Atlas:
      return { { 0, QColor{ 0xFFFEAC5E } },
               { 0.5, QColor{ 0xFFC779D0 } },
               { 1.0, QColor{ 0xFF4BC0C8 } } };
    case GradientPreset::DarkBlues:
      return { { 0, QColor{ 0xFF360033 } }, { 1, QColor{ 0xFF0b8793 } } };
    case GradientPreset::HAXBY:
      return { { 0 / 32.0, QColor::fromRgbF(0.039216, 0.000000, 0.474510) },
               { 1 / 32.0, QColor::fromRgbF(0.156863, 0.000000, 0.588235) },
               { 2 / 32.0, QColor::fromRgbF(0.078431, 0.019608, 0.686275) },
               { 3 / 32.0, QColor::fromRgbF(0.000000, 0.039216, 0.784314) },
               { 4 / 32.0, QColor::fromRgbF(0.000000, 0.098039, 0.831373) },
               { 5 / 32.0, QColor::fromRgbF(0.000000, 0.156863, 0.878431) },
               { 6 / 32.0, QColor::fromRgbF(0.101961, 0.400000, 0.941176) },
               { 7 / 32.0, QColor::fromRgbF(0.050980, 0.505882, 0.972549) },
               { 8 / 32.0, QColor::fromRgbF(0.098039, 0.686275, 1.000000) },
               { 9 / 32.0, QColor::fromRgbF(0.196078, 0.745098, 1.000000) },
               { 10 / 32.0, QColor::fromRgbF(0.266667, 0.792157, 1.000000) },
               { 11 / 32.0, QColor::fromRgbF(0.380392, 0.882353, 0.941176) },
               { 12 / 32.0, QColor::fromRgbF(0.415686, 0.921569, 0.882353) },
               { 13 / 32.0, QColor::fromRgbF(0.486275, 0.921569, 0.784314) },
               { 14 / 32.0, QColor::fromRgbF(0.541176, 0.925490, 0.682353) },
               { 15 / 32.0, QColor::fromRgbF(0.674510, 0.960784, 0.658824) },
               { 16 / 32.0, QColor::fromRgbF(0.803922, 1.000000, 0.635294) },
               { 17 / 32.0, QColor::fromRgbF(0.874510, 0.960784, 0.552941) },
               { 18 / 32.0, QColor::fromRgbF(0.941176, 0.925490, 0.474510) },
               { 19 / 32.0, QColor::fromRgbF(0.968627, 0.843137, 0.407843) },
               { 20 / 32.0, QColor::fromRgbF(1.000000, 0.741176, 0.341176) },
               { 21 / 32.0, QColor::fromRgbF(1.000000, 0.627451, 0.270588) },
               { 22 / 32.0, QColor::fromRgbF(0.956863, 0.458824, 0.294118) },
               { 23 / 32.0, QColor::fromRgbF(0.933333, 0.313725, 0.305882) },
               { 24 / 32.0, QColor::fromRgbF(1.000000, 0.352941, 0.352941) },
               { 25 / 32.0, QColor::fromRgbF(1.000000, 0.486275, 0.486275) },
               { 26 / 32.0, QColor::fromRgbF(1.000000, 0.619608, 0.619608) },
               { 27 / 32.0, QColor::fromRgbF(0.960784, 0.701961, 0.682353) },
               { 28 / 32.0, QColor::fromRgbF(1.000000, 0.768627, 0.768627) },
               { 29 / 32.0, QColor::fromRgbF(1.000000, 0.843137, 0.843137) },
               { 30 / 32.0, QColor::fromRgbF(1.000000, 0.921569, 0.921569) },
               { 31 / 32.0, QColor::fromRgbF(1.000000, 1.000000, 1.000000) } };
    default:
      return {};
  }
}
}
