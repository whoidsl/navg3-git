/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 10/24/17.
//

#include "GdalDataset.h"
#include "Proj.h"
#include "TemporaryGdalDataset.h"

#include "GdalDatasetPrivate.h"

#include <QFile>
#include <QProcess>

#include <gdal/gdal_priv.h>
#include <gdal/ogr_spatialref.h>

namespace dslmap {

GdalDataset::GdalDataset(QObject* parent)
  : QObject(parent)
  , d_ptr(new GdalDatasetPrivate(this))
{
}

GdalDataset::GdalDataset(const QString& filename, QObject* parent)
  : GdalDataset(parent)
{
  Q_D(GdalDataset);
  d->m_filename = filename;
}

GdalDataset::GdalDataset(GdalDatasetPrivate& pimpl, QObject* parent)
  : QObject(parent)
  , d_ptr(&pimpl)
{
}

GdalDataset::~GdalDataset() = default;

bool
GdalDataset::isOpen() const noexcept
{
  const Q_D(GdalDataset);

  if (d->m_dataset) {
    return true;
  }

  return false;
}

GDALDataset*
GdalDataset::get() const noexcept
{

  const Q_D(GdalDataset);
  return d->m_dataset.get();
}

bool
GdalDataset::open()
{
  Q_D(GdalDataset);
  if (d->m_filename.isEmpty() || isOpen()) {
    return false;
  }

  auto ptr =
    static_cast<GDALDataset*>(GDALOpen(d->m_filename.toLatin1(), GA_ReadOnly));

  if (!ptr) {
    return false;
  }

  // Setup the projection
  resetProjection();

  d->m_dataset.reset(ptr);

  return true;
}

QString
GdalDataset::fileName() const noexcept
{
  const Q_D(GdalDataset);
  return d->m_filename;
}

void
GdalDataset::setFileName(const QString& name)
{
  if (isOpen()) {
    return;
  }

  Q_D(GdalDataset);
  d->m_filename = name;
}

void
GdalDataset::close()
{
  Q_D(GdalDataset);
  d->m_dataset.reset(nullptr);
}

QString
GdalDataset::proj4Definition()
{
  Q_D(GdalDataset);
  if (!d->m_proj) {
    return {};
  }

  return d->m_proj->definition();
}

QString
GdalDataset::wktDefinition(bool pretty)
{

  auto proj4_def = proj4Definition() + " +wktext";
  if (proj4_def.isEmpty()) {
    return {};
  }

  OGRSpatialReference ogr_srs;
  ogr_srs.importFromProj4(proj4_def.toLatin1());
  auto wkt_def_ptr = static_cast<char*>(nullptr);
  auto err = OGRERR_NONE;
  if (pretty) {
    err = ogr_srs.exportToPrettyWkt(&wkt_def_ptr);
  } else {
    err = ogr_srs.exportToWkt(&wkt_def_ptr);
  }

  if (err != OGRERR_NONE) {
    qCWarning(dslmapProj) << "Possible error exporting proj4 definition to WKT";
  }

  const auto wkt_def = QString{ wkt_def_ptr };
  CPLFree(wkt_def_ptr);
  return wkt_def;
}

bool
GdalDataset::resetProjection()
{

  Q_D(GdalDataset);

  // Clear the existing projection.
  d->m_proj.reset();

  if (!d->m_dataset) {
    return true;
  }

  // Setup the projection
  auto wkt_def = d->m_dataset->GetProjectionRef();
  if (strlen(wkt_def) == 0) {
    qCWarning(dslmapProj) << fileName() << "has no coordinate system defined";
    return false;
  }

  auto proj = std::shared_ptr<Proj>(nullptr);

  OGRSpatialReference ogr_srs(wkt_def);
  auto proj4_def = static_cast<char*>(nullptr);
  if (ogr_srs.exportToProj4(&proj4_def) != OGRERR_NONE) {
    qCWarning(dslmapProj)
      << "Possible error converting raster projection to proj4";
  }
  const auto result = QString{ proj4_def };
  CPLFree(proj4_def);
  proj = Proj::fromDefinition(result);

  if (!proj) {
    qCWarning(dslmapProj) << "Unable to create projection from" << result;
    return false;
  }
  qCDebug(dslmapProj) << "resetProjection: " << result;

  d->m_proj = proj;
  return true;
}

void
GdalDataset::setProjection(std::shared_ptr<const Proj> proj)
{

  Q_D(GdalDataset);
  if (proj) {
    d->m_proj = proj;
    return;
  }
}

std::shared_ptr<const Proj>
GdalDataset::projection() const
{
  const Q_D(GdalDataset);
  return d->m_proj;
}

std::shared_ptr<TemporaryGdalDataset>
GdalDataset::warp_min(const Proj& proj, const QStringList& opts)
{
  Q_D(GdalDataset);

  auto temp_dataset = std::make_shared<TemporaryGdalDataset>();

  const auto dst_proj4 = proj.definition();

  auto process = std::make_unique<QProcess>();
  process->setProgram("gdalwarp");
  
  auto args = QStringList{ opts };

  // #if GDAL_VERSION_MAJOR < 2
  //   const auto driver_short_name =
  //     GDALGetDriverShortName(d->m_dataset->GetDriver());
  // #else
  //   const auto driver_short_name = d->m_dataset->GetDriverName();
  // #endif
  args
   << "-s_srs" << d->m_proj->definition() 
   << "-t_srs" << dst_proj4 ;

  args << fileName() << temp_dataset->fileName();

  process->setArguments(args);
  qCDebug(dslmapProj) << "Running: " << process->program()
                      << process->arguments().join(' ');

  process->start();

  if (!process->waitForFinished(10000)) {
    qCWarning(dslmapProj) << "Timed out waiting for raster reprojection";
    process->kill();
    return {};
  }

  if (process->exitStatus() != QProcess::NormalExit) {
    qCWarning(dslmapProj) << "gdalwarp crashed during raster reprojection";
    return {};
  }

  const auto exit_code = process->exitCode();
  qCInfo(dslmapProj) << "gdalwarp returned with exit code:" << exit_code;
  if (exit_code != 0) {
    qCInfo(dslmapProj) << process->readAllStandardError();
    qCInfo(dslmapProj) << process->readAllStandardOutput();
    return {};
  }

  if (!temp_dataset->open()) {
    qCWarning(dslmapProj) << "Could not open temporary file"
                          << temp_dataset->fileName() << "as a GDALDataset";
    return {};
  }

  temp_dataset->setProjection(Proj::fromDefinition(dst_proj4));

  return temp_dataset;
}

std::shared_ptr<TemporaryGdalDataset>
GdalDataset::translate(const Proj& proj, const QStringList& opts)
{
  Q_D(GdalDataset);

  auto temp_dataset = std::make_shared<TemporaryGdalDataset>();

  const auto dst_proj4 = proj.definition();

  auto process = std::make_unique<QProcess>();
  process->setProgram("gdal_translate");

  auto args = QStringList{ opts };

  // #if GDAL_VERSION_MAJOR < 2
  //   const auto driver_short_name =
  //     GDALGetDriverShortName(d->m_dataset->GetDriver());
  // #else
  //   const auto driver_short_name = d->m_dataset->GetDriverName();
  // #endif

  args << "-of" << "GTiff" <<"-a_srs" << "EPSG:4326" << "-a_nodata" << "255";

  // GSBG doesn't support Float64 output
  if (d->m_dataset->GetRasterBand(1)->GetRasterDataType() == GDT_Float64) {
    args << "-ot"
         << "Float32";
  }

  args << fileName() << temp_dataset->fileName();

  process->setArguments(args);
  qCDebug(dslmapProj) << "Running: " << process->program()
                      << process->arguments().join(' ');

  process->start();

  if (!process->waitForFinished(10000)) {
    qCWarning(dslmapProj) << "Timed out waiting for raster reprojection";
    process->kill();
    return {};
  }

  if (process->exitStatus() != QProcess::NormalExit) {
    qCWarning(dslmapProj) << "gdal_translate crashed during raster reprojection";
    return {};
  }

  const auto exit_code = process->exitCode();
  qCInfo(dslmapProj) << "gdal_translate returned with exit code:" << exit_code;
  if (exit_code != 0) {
    qCInfo(dslmapProj) << process->readAllStandardError();
    qCInfo(dslmapProj) << process->readAllStandardOutput();
    return {};
  }

  if (!temp_dataset->open()) {
    qCWarning(dslmapProj) << "Could not open temporary file"
                          << temp_dataset->fileName() << "as a GDALDataset";
    return {};
  }

  temp_dataset->setProjection(Proj::fromDefinition(dst_proj4));

  return temp_dataset;
}

std::shared_ptr<TemporaryGdalDataset>
GdalDataset::warp(const Proj& proj, const QStringList& opts)
{
  Q_D(GdalDataset);

  auto temp_dataset = std::make_shared<TemporaryGdalDataset>();

  const auto dst_proj4 = proj.definition();

  auto process = std::make_unique<QProcess>();
  process->setProgram("gdalwarp");

  auto args = QStringList{ opts };

  // #if GDAL_VERSION_MAJOR < 2
  //   const auto driver_short_name =
  //     GDALGetDriverShortName(d->m_dataset->GetDriver());
  // #else
  //   const auto driver_short_name = d->m_dataset->GetDriverName();
  // #endif
  args << "-s_srs" << d->m_proj->definition() << "-t_srs" << dst_proj4 << "-of"
       << "GSBG"
       << "-dstnodata"
       << "nan";

  // GSBG doesn't support Float64 output
  if (d->m_dataset->GetRasterBand(1)->GetRasterDataType() == GDT_Float64) {
    args << "-ot"
         << "Float32";
  }

  args << fileName() << temp_dataset->fileName();

  process->setArguments(args);
  qCDebug(dslmapProj) << "Running: " << process->program()
                      << process->arguments().join(' ');

  process->start();

  if (!process->waitForFinished(10000)) {
    qCWarning(dslmapProj) << "Timed out waiting for raster reprojection";
    process->kill();
    return {};
  }

  if (process->exitStatus() != QProcess::NormalExit) {
    qCWarning(dslmapProj) << "gdalwarp crashed during raster reprojection";
    return {};
  }

  const auto exit_code = process->exitCode();
  qCInfo(dslmapProj) << "gdalwarp returned with exit code:" << exit_code;
  if (exit_code != 0) {
    qCInfo(dslmapProj) << process->readAllStandardError();
    qCInfo(dslmapProj) << process->readAllStandardOutput();
    return {};
  }

  if (!temp_dataset->open()) {
    qCWarning(dslmapProj) << "Could not open temporary file"
                          << temp_dataset->fileName() << "as a GDALDataset";
    return {};
  }

  temp_dataset->setProjection(Proj::fromDefinition(dst_proj4));

  return temp_dataset;
}

std::shared_ptr<TemporaryGdalDataset>
GdalDataset::colorRelief(const QString& colorTableFile, const QStringList& opts)
{

  auto temp_dataset = std::make_shared<TemporaryGdalDataset>();

  auto process = std::make_unique<QProcess>();
  process->setProgram("gdaldem");

  auto args = QStringList{ "color-relief",           fileName(), colorTableFile,
                           temp_dataset->fileName(), "-co",      "ALPHA=YES" };

  args << opts;

  process->setArguments(args);
  process->start();
  qCDebug(dslmapProj) << "Running: " << process->program()
                      << process->arguments().join(' ');

  if (!process->waitForFinished(10000)) {
    qCWarning(dslmapProj) << "Timed out waiting for color relief processing";
    process->kill();
    return {};
  }

  if (process->exitStatus() != QProcess::NormalExit) {
    qCWarning(dslmapProj) << "gdaldem crashed during shaded relief processing";
    return {};
  }

  const auto exit_code = process->exitCode();
  qCInfo(dslmapProj) << "gdaldem returned with exit code:" << exit_code;

  if (!temp_dataset->open()) {
    qCWarning(dslmapProj) << "Could not open temporary file as a GDALDataset";
    return {};
  }
  return temp_dataset;
}
std::shared_ptr<TemporaryGdalDataset>
GdalDataset::hillshade(const QStringList& opts)
{

  auto temp_dataset = std::make_shared<TemporaryGdalDataset>();

  auto process = std::make_unique<QProcess>();
  process->setProgram("gdaldem");

  auto args = QStringList{
    "hillshade", fileName(), temp_dataset->fileName(),
  };

  args << opts;

  process->setArguments(args);
  process->start();
  qCDebug(dslmapProj) << "Running: " << process->program()
                      << process->arguments().join(' ');

  if (!process->waitForFinished(10000)) {
    qCWarning(dslmapProj) << "Timed out waiting for hillshade processing";
    process->kill();
    return {};
  }

  if (process->exitStatus() != QProcess::NormalExit) {
    qCWarning(dslmapProj) << "gdaldem crashed during hillshade processing";
    return {};
  }

  const auto exit_code = process->exitCode();
  qCInfo(dslmapProj) << "gdaldem returned with exit code:" << exit_code;

  if (!temp_dataset->open()) {
    qCWarning(dslmapProj) << "Could not open temporary file as a GDALDataset";
    return {};
  }

  return temp_dataset;
}

QRectF
GdalDataset::boundingRect()
{
  auto dataset = get();
  if (!dataset) {
    return {};
  }

  auto pad_transform = std::array<double, 6>{};
  
  const auto ok = dataset->GetGeoTransform(pad_transform.data());
  if (ok != CPLErr::CE_None) {
    qCWarning(dslmapProj,
              "Error in GDALDataset->GetGeoTransform.  Error code %d: %s", ok,
              CPLGetLastErrorMsg());
    return {};
    
  }

  const auto width_p = dataset->GetRasterXSize();
  const auto height_p = dataset->GetRasterYSize();

  const auto top_left = QPointF{ pad_transform.at(0), pad_transform.at(3) };
  const auto bottom_right =
    QPointF{ top_left.x() + width_p * pad_transform.at(1) +
               height_p * pad_transform.at(2),
             top_left.y() + width_p * pad_transform.at(4) +
               height_p * pad_transform.at(5) };

  return { top_left, bottom_right };
}
}