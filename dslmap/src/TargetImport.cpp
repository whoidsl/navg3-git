/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "TargetImport.h"
#include "MapScene.h"
#include "MarkerItem.h"
#include "SymbolLayer.h"

#include "ui_TargetImport.h"
#include <QButtonGroup>
#include <QMessageBox>

namespace dslmap {

TargetImport::TargetImport(MapScene& scene, QWidget* parent)
  : QDialog(parent)
  , fileStr("")
  , delimStr(',')
  , ui(new Ui::TargetImport)
  , lastChk(new QCheckBox(this))
  , m_proj_button(new QButtonGroup(this))
  , m_scene(scene)
{
  // Machinery behind TargetImport UI
  // Users import and designate XY coords and string labels from an existing
  // data file
  //
  // Initial settings: File string empty, delimiter comma, X=column 1, Y= column
  // 2

  ui->setupUi(this);

  //chooseFile();

  ui->lblAlvinXY->hide();
  ui->editAlvinLat->hide();
  ui->editAlvinLon->hide();
  auto float_valid = new QDoubleValidator(-180.0, 180.0, 8);
  ui->editAlvinLat->setValidator(float_valid);
  ui->editAlvinLon->setValidator(float_valid);
  ui->lblProj->hide();
  ui->editProj->hide();
  ui->lblZoneContains->hide();

  //    Initial settings on user inputs
  ui->okBox->setEnabled(false);

  ui->colEdit->setPlaceholderText("e.g. 1-3, 5");
  ui->delimEdit->setVisible(false);
  ui->delimBox->addItem("comma", ",");
  ui->delimBox->addItem("semi-colon", ";");
  ui->delimBox->addItem("tab", "\t");
  ui->delimBox->addItem("line", "\n");
  ui->delimBox->addItem("space", " ");
  ui->delimBox->addItem("other", "");

  ui->xBox->setValue(1);
  ui->yBox->setValue(2);
  ui->xBox->setMinimum(1);
  ui->yBox->setMinimum(1);
  ui->lBox->setMinimum(1);
  ui->aBox->setMinimum(1);
  ui->lBox->setEnabled(false);
  ui->aBox->setEnabled(false);
  ui->aEdit->setEnabled(false);
  ui->aEdit->setText("Attribute 1");

  lastChk = ui->aChk;

  m_proj_button->addButton(ui->btnLatlon);
  m_proj_button->addButton(ui->btnLatLonMinutes);
  m_proj_button->addButton(ui->btnUtm);
  m_proj_button->addButton(ui->btnAlvinXY);
  m_proj_button->addButton(ui->btnProj);

  connect(m_proj_button, static_cast<void (QButtonGroup::*)(QAbstractButton*)>(
                           &QButtonGroup::buttonClicked),
          this, &TargetImport::projRadioClicked);

  // Lat Lon in decimal degrees is the default option
  ui->btnLatlon->setChecked(true);

  // Open a file when the file button is clicked
  connect(ui->fileBtn, &QPushButton::clicked, this, &TargetImport::chooseFile);

  // Toggle Label Column spin box when label checked
  connect(ui->aChk, &QCheckBox::stateChanged, this,
          &TargetImport::toggleColumn);
  connect(ui->lChk, &QCheckBox::stateChanged, this,
          &TargetImport::toggleColumn);

  // Update delimiter when box or text changes
  connect(ui->delimBox, static_cast<void (QComboBox::*)(int)>(
                          &QComboBox::currentIndexChanged),
          this, &TargetImport::delimSelect);
  connect(ui->delimEdit, &QLineEdit::textChanged, this,
          &TargetImport::delimSelect);

  // Update the table model preview when preview button is clicked
  /*
  connect(ui->previewBtn, &QPushButton::clicked, this,
          static_cast<void (TargetImport::*)(void)>(&TargetImport::preview));
          */
  connect(ui->previewBtn, &QPushButton::clicked, this,
          &TargetImport::preview);       

  // Add Attribute entry when a new Attribute is highlighted
  connect(ui->aChk, &QCheckBox::stateChanged, this,
          &TargetImport::addAttributeLine);
}

TargetImport::~TargetImport()
{
  delete ui;
}

TargetImport::TargetImport(MapScene& scene, int lon_col, int lat_col, QString file_string,QString layer_name, QWidget* parent)
: QDialog(parent)
  , fileStr(file_string)
  , delimStr(',')
  , ui(new Ui::TargetImport)
  , lastChk(new QCheckBox(this))
  , m_proj_button(new QButtonGroup(this))
  , m_scene(scene)
{
  // Machinery behind TargetImport UI
  // Users import and designate XY coords and string labels from an existing
  // data file
  //
  // Initial settings: File string empty, delimiter comma, X=column 1, Y= column
  // 2

  ui->setupUi(this);

  //chooseFile();

  ui->lblAlvinXY->hide();
  ui->editAlvinLat->hide();
  ui->editAlvinLon->hide();
  auto float_valid = new QDoubleValidator(-180.0, 180.0, 8);
  ui->editAlvinLat->setValidator(float_valid);
  ui->editAlvinLon->setValidator(float_valid);
  ui->lblProj->hide();
  ui->editProj->hide();
  ui->lblZoneContains->hide();



  //    Initial settings on user inputs
  ui->okBox->setEnabled(false);

  ui->colEdit->setPlaceholderText("e.g. 1-3, 5");
  ui->delimEdit->setVisible(false);
  ui->delimBox->addItem("comma", ",");
  ui->delimBox->addItem("semi-colon", ";");
  ui->delimBox->addItem("tab", "\t");
  ui->delimBox->addItem("line", "\n");
  ui->delimBox->addItem("space", " ");
  ui->delimBox->addItem("other", "");

  ui->xBox->setValue(1);
  ui->yBox->setValue(2);
  ui->xBox->setMinimum(1);
  ui->yBox->setMinimum(1);
  ui->lBox->setMinimum(1);
  ui->aBox->setMinimum(1);
  ui->lBox->setEnabled(false);
  ui->aBox->setEnabled(false);
  ui->aEdit->setEnabled(false);
  ui->aEdit->setText("Attribute 1");

  lastChk = ui->aChk;

  m_proj_button->addButton(ui->btnLatlon);
  m_proj_button->addButton(ui->btnLatLonMinutes);
  m_proj_button->addButton(ui->btnUtm);
  m_proj_button->addButton(ui->btnAlvinXY);
  m_proj_button->addButton(ui->btnProj);

  connect(m_proj_button, static_cast<void (QButtonGroup::*)(QAbstractButton*)>(
                           &QButtonGroup::buttonClicked),
          this, &TargetImport::projRadioClicked);

  // Lat Lon in decimal degrees is the default option
  ui->btnLatlon->setChecked(true);

  // Open a file when the file button is clicked
  connect(ui->fileBtn, &QPushButton::clicked, this, &TargetImport::chooseFile);

  // Toggle Label Column spin box when label checked
  connect(ui->aChk, &QCheckBox::stateChanged, this,
          &TargetImport::toggleColumn);
  connect(ui->lChk, &QCheckBox::stateChanged, this,
          &TargetImport::toggleColumn);

  // Update delimiter when box or text changes
  connect(ui->delimBox, static_cast<void (QComboBox::*)(int)>(
                          &QComboBox::currentIndexChanged),
          this, &TargetImport::delimSelect);
  connect(ui->delimEdit, &QLineEdit::textChanged, this,
          &TargetImport::delimSelect);

  // Update the table model preview when preview button is clicked
  /*
  connect(ui->previewBtn, &QPushButton::clicked, this,
          static_cast<void (TargetImport::*)(void)>(&TargetImport::preview));
          */
  connect(ui->previewBtn, &QPushButton::clicked, this,
          &TargetImport::preview); 

  // Add Attribute entry when a new Attribute is highlighted
  connect(ui->aChk, &QCheckBox::stateChanged, this,
          &TargetImport::addAttributeLine);

   //prefilled input from constructor
  ui->layerEdit->setText(layer_name);
  ui->xBox->setValue(lon_col);
  ui->yBox->setValue(lat_col);

  ui->lChk->setChecked(true);
  ui->lBox->setValue(4);

  ui->fileEdit->setText(fileStr);
  if (preview()){
    buildLayerAndClose();
  }
}

QString
TargetImport::getLayerName()
{
  //    Public because called after widget is closed
  return ui->layerEdit->text();
}

void
TargetImport::projRadioClicked(QAbstractButton* button_)
{
  ui->lblAlvinXY->hide();
  ui->editAlvinLat->hide();
  ui->editAlvinLon->hide();
  ui->lblProj->hide();
  ui->editProj->hide();
  ui->lblZoneContains->hide();
  const auto button = qobject_cast<QRadioButton*>(button_);

  if (button == ui->btnAlvinXY) {
    ui->lblAlvinXY->show();
    ui->editAlvinLat->show();
    ui->editAlvinLon->show();
  } else if (button == ui->btnProj) {
    ui->lblProj->show();
    ui->editProj->show();
  } else if (button == ui->btnUtm) {
    ui->lblZoneContains->show();
    ui->editAlvinLat->show();
    ui->editAlvinLon->show();
  }
}

std::shared_ptr<Proj>
TargetImport::extractProj()
{
  const auto button =
    qobject_cast<QRadioButton*>(m_proj_button->checkedButton());
  if (button == ui->btnLatlon) {
    return Proj::fromDefinition(PROJ_WGS84_DEFINITION);
  } else if (button == ui->btnLatLonMinutes) {
    return Proj::fromDefinition(PROJ_WGS84_DEFINITION);
  } else if (button == ui->btnUtm) {
    return Proj::utmFromLonLat(ui->editAlvinLon->text().toDouble(),
                               ui->editAlvinLat->text().toDouble());
  } else if (button == ui->btnAlvinXY) {
    bool lonOK = false;
    bool latOK = false;
    auto alvinXYproj =
      Proj::alvinXYFromOrigin(ui->editAlvinLon->text().toDouble(&lonOK),
                              ui->editAlvinLat->text().toDouble(&latOK));
    if (lonOK && latOK) {
      return alvinXYproj;
    }
  } else if (button == ui->btnProj) {
    auto proj = Proj::fromDefinition(ui->editProj->text());
    return proj;
  }
  return nullptr;
}

QList<std::tuple<QPointF, QString, QVariantMap>>
TargetImport::extractXY()
{
  //    Public because called after widget is closed
  auto my_model = qobject_cast<TargetTableModel*>(ui->preView->model());
  Q_ASSERT(my_model != nullptr); // Make sure the model exists.

  QList<std::tuple<QPointF, QString, QVariantMap>> returnList;
  int col_num = my_model->getCol();
  int row_num = my_model->getRow();
  if (col_num < 2) {
    return returnList;
  }

  int xCol = ui->xBox->value() -
             1; // Column number in view indexes from 1, but data indexes from 0
  int yCol = ui->yBox->value() - 1;
  int lCol = ui->lBox->value() - 1;

  bool xOK = true;
  bool yOK = true;

  QPointF newXY;
  QString newLabel = "label";
  QString newAName;
  QVariant newAVar;
  QVariantMap newAMap;
  auto aPairs = getAPairs();

  for (int k = 0; k < row_num; k++) {
    if (ui->btnLatLonMinutes->isChecked()) {
      QList<QString> x_minutes = my_model->getStringAt(k, xCol).split(" ");
      QList<QString> y_minutes = my_model->getStringAt(k, yCol).split(" ");
      if (x_minutes.size() != 2 || y_minutes.size() != 2) {
        continue;
      }
      x_minutes[0].remove(QRegExp("[^0-9.\\-]"));
      x_minutes[1].remove(QRegExp("[^0-9.\\-]"));
      y_minutes[0].remove(QRegExp("[^0-9.\\-]"));
      y_minutes[1].remove(QRegExp("[^0-9.\\-]"));
      newXY.setX(x_minutes[0].toDouble(&xOK) +
                 x_minutes[0].toDouble(&xOK) * x_minutes[1].toDouble(&xOK) /
                   (std::abs(x_minutes[0].toDouble(&xOK)) * 60.0));
      newXY.setY(y_minutes[0].toDouble(&yOK) +
                 y_minutes[0].toDouble(&xOK) * y_minutes[1].toDouble(&yOK) /
                   (std::abs(y_minutes[0].toDouble(&xOK)) * 60.0));
      qDebug() << "x: " << x_minutes[0] << " " << x_minutes[1];
      qDebug() << "x: " << newXY.x();
      qDebug() << "x: " << y_minutes[0] << " " << y_minutes[1];
      qDebug() << "y: " << newXY.y();
    } else {
      newXY.setX(my_model->getRealAt(k, xCol, &xOK));
      newXY.setY(my_model->getRealAt(k, yCol, &yOK));
    }
    if (!xOK || !yOK) {
      continue;
    }
    if (!ui->lBox->isEnabled()) { // If labels not being used
      newLabel = "";
    } else {
      newLabel = my_model->getStringAt(k, lCol);
    }
    QMapIterator<QString, int> i(aPairs);
    while (i.hasNext()) {
      i.next();
      newAName = i.key();
      newAVar = my_model->getStringAt(k, i.value() - 1);
      newAMap.insert(newAName, newAVar);
    }
    auto newPoint = std::make_tuple(newXY, newLabel, newAMap);
    newAMap.clear();
    returnList.push_back(newPoint);
  }
  return returnList;
}

void
TargetImport::chooseFile()
{
  fileStr = QFileDialog::getOpenFileName(
    this, tr("Open Target File"), "",
    tr("Data Files (*.csv *.dat *.txt *.tgt);;All Files (*)"));
  ui->fileEdit->setText(fileStr);
  preview();
}

void
TargetImport::toggleColumn()
{
  ui->lBox->setEnabled(ui->lChk->isChecked());
  ui->aBox->setEnabled(ui->aChk->isChecked());
  ui->aEdit->setEnabled(ui->aChk->isChecked());
  for (int i = 0; i < ui->aLayout->count(); i++) {
    QCheckBox* foo_chk = qobject_cast<QCheckBox*>(
      ui->aLayout->itemAt(i)->layout()->itemAt(0)->widget());
    bool use_row = foo_chk->isChecked();
    ui->aLayout->itemAt(i)->layout()->itemAt(1)->widget()->setEnabled(use_row);
    ui->aLayout->itemAt(i)->layout()->itemAt(2)->widget()->setEnabled(use_row);
  }
}

QMap<QString, int>
TargetImport::getAPairs()
{
  QMap<QString, int> aPairs;
  if (ui->aBox->isEnabled()) {
    aPairs.insert(ui->aEdit->text(), ui->aBox->value());
  }
  for (int i = 0; i < ui->aLayout->count(); i++) {
    QCheckBox* foo_chk = qobject_cast<QCheckBox*>(
      ui->aLayout->itemAt(i)->layout()->itemAt(0)->widget());
    if (foo_chk->isChecked()) {
      QLineEdit* foo_le = qobject_cast<QLineEdit*>(
        ui->aLayout->itemAt(i)->layout()->itemAt(1)->widget());
      QSpinBox* foo_spn = qobject_cast<QSpinBox*>(
        ui->aLayout->itemAt(i)->layout()->itemAt(2)->widget());
      aPairs.insert(foo_le->text(), foo_spn->value());
    }
  }
  return aPairs;
}

void
TargetImport::delimSelect()
{
  // Update delimiter to user selection. If "Other" use delimEdit
  int delimIndex = ui->delimBox->currentIndex();
  QString delimTxt = ui->delimBox->currentText();

  delimStr = ui->delimBox->itemData(delimIndex).toString();
  if (delimStr == "") {
    delimStr = ui->delimEdit->text();
  }
  ui->delimEdit->setVisible(true);
}

void
TargetImport::addAttributeLine()
{
  int oldARows = ui->aLayout->count();
  if (oldARows < 1) {
    connect(ui->aChk, &QCheckBox::stateChanged, this,
            &TargetImport::delAttributeLine);
  } else {
    QCheckBox* lastChk = qobject_cast<QCheckBox*>(
      ui->aLayout->itemAt(oldARows - 1)->layout()->itemAt(0)->widget());
    if (!lastChk->isChecked()) {
      return;
    }
    connect(lastChk, &QCheckBox::stateChanged, this,
            &TargetImport::delAttributeLine);
  }

  QCheckBox* fooChk = new QCheckBox();

  QLineEdit* fooEdit = new QLineEdit();
  fooEdit->setText("Attribute " + QString::number(oldARows + 2));
  fooEdit->setEnabled(false);
  QSpinBox* fooBox = new QSpinBox();
  fooBox->setMinimum(1);
  fooBox->setMaximum(ui->aBox->maximum());
  fooBox->setEnabled(false);

  QHBoxLayout* fooRow = new QHBoxLayout();

  fooRow->addWidget(fooChk);
  fooRow->addWidget(fooEdit);
  fooRow->addWidget(fooBox);

  //  Edit connections to reflect the new state
  connect(fooChk, &QCheckBox::stateChanged, this, &TargetImport::toggleColumn);
  connect(fooChk, &QCheckBox::stateChanged, this,
          &TargetImport::addAttributeLine);
  ui->aLayout->addRow(fooRow);
}

void
TargetImport::delAttributeLine()
{
  //    Deletes last attribute line from ui->scrollArea
  int oldARows = ui->aLayout->count();
  if (oldARows < 3) {
    return;
  }
  QCheckBox* backChk = qobject_cast<QCheckBox*>(
    ui->aLayout->itemAt(oldARows - 2)->layout()->itemAt(0)->widget());
  QCheckBox* lastChk = qobject_cast<QCheckBox*>(
    ui->aLayout->itemAt(oldARows - 1)->layout()->itemAt(0)->widget());
  if (backChk->isChecked() || lastChk->isChecked()) {
    return;
  }
  // This isn't available in Qt5.5
  // ui->aLayout->removeRow(oldARows - 1);

  // This should be a layout
  auto row = ui->aLayout->takeAt(oldARows - 1);
  auto row_layout = row->layout();
  if (row_layout) {
    // For each item in the layout...
    while (row_layout->count() > 0) {
      // Pull the widget out.
      auto item = row_layout->takeAt(0);
      if (item->widget()) {
        // And schedule it for deletion.
        item->widget()->deleteLater();
      }
    }
  }
  row_layout->deleteLater();
}

bool
TargetImport::preview()
{
  //    Wipe error status so old errors don't display
  ui->statusTxt->setText("good");
  //    Verify file name exists
  if (fileStr == "") {
    ui->statusTxt->setText("Error: no filename given");
    return false;
  }
  //    Verify file is ready to read
  QFile previewFile(fileStr);
  if (!previewFile.open(QIODevice::ReadOnly)) {
    qDebug() << previewFile.errorString();
    return false;
  }

  int skipLine = ui->rowBox->value();
  int lineSize = 0;
  QSet<int> column_set =
    TargetImport::readColumns(); // Take in which columns to read
  QVector<QVector<QString>> tableData;

  for (int i = 0; !previewFile.atEnd(); i++) { // For each row in the file...
    QVector<QString> currentLine;
    QString line = previewFile.readLine();
    if (i >= skipLine) { // Only add in wanted (i.e. not header) rows.
      auto thisList = line.split(delimStr);
      for (int j = 0; j < thisList.size(); j++) {
        if (column_set.contains(j + 1) ||
            column_set.empty()) { // If user wants this column, then add it to
                                  // the model. If empty, include all rows.
          currentLine.push_back(thisList[j]);
        }
      }
      if (i == skipLine) { // Get a desired line length from first included row
        lineSize = thisList.size();
        tableData.push_back(currentLine);
      } else if (thisList.size() == lineSize) { // Check to ensure each added
                                                // row has desired line length
                                                // to avoid Segfaults
        tableData.push_back(currentLine);
      }
    }
  }
  // If tableData isn't empty, update the table model
  if (tableData.empty()) {
    ui->statusTxt->setText(
      "Error: no rows read! Lower skip rows or change file");
    return false;
  } else {
    TargetImport::updateTableModel(tableData);
    ui->okBox->setEnabled(true);
  }
  return true;
}

QSet<int>
TargetImport::readColumns()
{
  //    Read and parse user desired columns. Does not know how many columns are
  //    in the file.
  QString cols = ui->colEdit->text();
  const QSet<int> fail_set = {};

  if (cols == "") {
    ui->statusTxt->setText("No columns specified, default to all columns");
    return fail_set;
  }

  QSet<int> col_set = {};
  // input error flags
  bool start_range_ok = true;
  bool end_range_ok = true;
  bool int_ok = true;

  // Case: nonempty entry
  QStringList entries = cols.split(','); // Split by comma delimiters
  for (int i = 0; i < entries.size(); i++) {
    QStringList range_list =
      entries[i].split('-');     // Check if it's a range of integers
    if (range_list.size() > 2) { // invalid quasi-range: flag an error
      start_range_ok = false;
      end_range_ok = false;
    }
    if (range_list.size() == 2) { // range: add all integers inclusive to set
      start_range_ok = false;
      end_range_ok = false;

      int j_start = range_list[0].toInt(&start_range_ok);
      int j_end = range_list[1].toInt(&end_range_ok) + 1;
      if (!start_range_ok || !end_range_ok || j_start > j_end) {
        continue;
      }
      for (int j = j_start; j < j_end; j++) {
        col_set.insert(j);
      }
    }
    if (range_list.size() == 1) { // integer: add to set
      int_ok = false;
      col_set.insert(entries[i].toInt(&int_ok));
    }
    if (!int_ok || !start_range_ok || !end_range_ok) {
      //            Catch flags and return when they occur
      ui->statusTxt->setText("Column syntax invalid, default to all columns");
      return fail_set;
    }
  }
  return col_set;
}

void
TargetImport::updateTableModel(QVector<QVector<QString>> table)
{
  int rows = table.size();
  int columns = table[0].size();
  // Keep spinboxes within actual column range
  ui->xBox->setMaximum(columns);
  ui->yBox->setMaximum(columns);
  ui->lBox->setMaximum(columns);
  ui->aBox->setMaximum(columns);
  for (int i = 0; i < ui->aLayout->count(); i++) {
    QSpinBox* foo_spn = qobject_cast<QSpinBox*>(
      ui->aLayout->itemAt(i)->layout()->itemAt(2)->widget());
    foo_spn->setMaximum(columns);
  }

  TargetTableModel* model = new TargetTableModel(columns, rows, table);
  auto* m = ui->preView->model();
  ui->preView->setModel(model);
  delete m; // Prevent memory leak of old models
}

// void
// TargetImport::setOrigin(QPointF origin)
//{
//  ui->editAlvinLat->setText(QString::number(origin.y(), 'f', 10));
//  ui->editAlvinLon->setText(QString::number(origin.x(), 'f', 10));
//}

bool
TargetImport::buildLayerAndClose()
{
  // Add the layer to the MapScene.
  layer = new dslmap::SymbolLayer();
  QString layerName = getLayerName();

  if (layerName.isEmpty()) {
    layer->setName(ui->layerEdit->placeholderText());
  } else {
    layer->setName(layerName);
  } // Always have a layer name, even if it's a dumb default

  // Create a red rectangle symbol to draw the points
  layer->setShape(dslmap::ShapeMarkerSymbol::Shape::Square, 10);
  auto symbol_ptr = layer->symbolPtr();
  symbol_ptr->setOutlineColor(Qt::red);
  symbol_ptr->setFillColor(Qt::red);
  symbol_ptr->setLabelColor(Qt::red);

  // Draw the markers scale invariant.
  layer->setScaleInvariant(true);
  auto proj_tgt = extractProj();
  if (proj_tgt) {
    qDebug() << "proj selected by user";
    layer->setProjection(proj_tgt);
  } else {
    qDebug() << "proj defaults to scene projection";
    layer->setProjection(m_scene.projection());
  }

  // Get points from TargetImport model. Imports location and label.
  auto pts = extractXY();

  if (pts.empty()) {
    QMessageBox::warning(this, tr("No points added."),
                         tr("No points were added to the layer."),
                         QMessageBox::StandardButton::Close);
    return false;
  }

  for (auto const& pt : pts) {
    layer->addPoint(std::get<0>(pt), std::get<1>(pt), std::get<2>(pt));
  }
  
  //navg::NavGMapScene *n_scene = qobject_cast<navg::NavGMapScene*>(m_scene);
  //auto n_scene = qobject_cast<navg::NavGMapScene&>(m_scene);
  //navg::NavGMapScene n_scene = dynamic_cast<navg::NavGMapScene&>(m_scene);
  /*
  connect(layer,
          &dslmap::SymbolLayer::saveTargetUnderlay,
          n_scene,
          &navg::NavGMapScene::setTargetPath);
          */

  // Add layer to scene and exit
  layer->setFileName(fileStr);
  //qDebug() << "Adding filename of " << fileStr << "to symbol layer";
  m_scene.addLayer(layer);
  return true;
}

void
TargetImport::accept()
{
  if (buildLayerAndClose()) {
    QDialog::accept();
  }
}

dslmap::SymbolLayer* 
TargetImport::targetLayer(){
  return layer;
}

bool
TargetImport::isInUnderlayFormatting(){
  return (ui->btnLatlon->isChecked() && ui->xBox->value() == 3 && ui->yBox->value() == 2);
}


} // END NAMESPACE DSLMAP
