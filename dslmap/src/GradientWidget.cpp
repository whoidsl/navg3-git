//
// Created by jvaccaro on 12/7/18.
//

#include "GradientWidget.h"
#include "GdalColorGradient.h"
#include <QMouseEvent>
#include <QPainter>

#include <QLoggingCategory>

namespace dslmap {

Q_LOGGING_CATEGORY(dslmap_gradient_widget, "dslmap.widget.gradient")

GradientWidget::GradientWidget(QWidget* parent)
  : QWidget(parent)
  , m_cursor_y(0)
  , m_legend_spacing(25)
  , m_depth_label_ratio(1)
  , m_textcolor(GradientWidget::TextColor::Auto)
{
}

GradientWidget::~GradientWidget()
{
}

void
GradientWidget::setGradient(std::shared_ptr<dslmap::GdalColorGradient> grad)
{
  if (!grad) {
    return;
  }
  m_gradient = grad;
  update();
  connect(m_gradient.get(), &dslmap::GdalColorGradient::gdalColorTableChanged,
          [=]() { update(); });
}

void
GradientWidget::paintEvent(QPaintEvent*)
{
  if (!m_gradient) {
    return;
  }
  QPainter painter(this);
  auto w = width();
  auto h = height();
  QLinearGradient linearGrad(QPointF(2 * w / 5, h), QPointF(2 * w / 5, 0));
  linearGrad.setStops(m_gradient->stops());

  QRect rect_linear(2 * w / 5, 0, w / 5, h);
  painter.fillRect(rect_linear, linearGrad);

  auto min = m_gradient->min();
  auto max = m_gradient->max();
  auto count = 0;
  for (float d = max; d > min; d -= m_legend_spacing) {
    auto hd = static_cast<int>(h - h * (d - min) / (max - min));
    auto color = m_gradient->colorAt(d);
    painter.setPen(color);
    painter.drawLine(QPoint{ 0, hd }, QPoint{ w / 2, hd });
    if (count % m_depth_label_ratio == 0) {
      setPenColor(&painter);
      painter.drawText(QPoint{ w / 5, hd }, QString::number(d));
    }
    count += 1;
  }

  qreal depth = min + (max - min) * (h - m_cursor_y) / h;
  auto color = m_gradient->colorAt(depth);
  QPen depth_pen{ color };
  depth_pen.setWidth(3);
  painter.setPen(depth_pen);
  painter.drawLine(QPoint{ 0, m_cursor_y }, QPoint{ w, m_cursor_y });
  QPoint depth_pos{ 3 * w / 5, m_cursor_y };
  setPenColor(&painter);
  painter.drawText(depth_pos, QString::number(depth, 'f', 2));
}

void
GradientWidget::mouseMoveEvent(QMouseEvent* e)
{
  QPainter painter(this);
  auto w = width();
  auto h = height();
  QRect rect_linear(2 * w / 5, 0, w / 5, h);
  auto pos = e->pos();
  if (!rect_linear.contains(pos) || !m_gradient) {
    return;
  }
  m_cursor_y = pos.y();
  update();
}

void
GradientWidget::mouseDoubleClickEvent(QMouseEvent* e)
{
  mouseMoveEvent(e);
}

void
GradientWidget::setLegendSpacing(float increment)
{
  m_legend_spacing = increment;
  qCDebug(dslmap_gradient_widget) << "Set legend spacing to: " << increment;
  update();
}

float
GradientWidget::legendSpacing()
{
  return m_legend_spacing;
}

void
GradientWidget::setDepthLabelRatio(int ratio)
{
  if (ratio < 1) {
    return;
  }
  m_depth_label_ratio = ratio;
  update();
}

int
GradientWidget::depthLabelRatio()
{
  return m_depth_label_ratio;
}

void
GradientWidget::setTextColor(GradientWidget::TextColor color)
{
  m_textcolor = color;
  update();
}

GradientWidget::TextColor
GradientWidget::textColor()
{
  return m_textcolor;
}

void
GradientWidget::setPenColor(QPainter* p)
{
  switch (m_textcolor) {
    case TextColor::Black:
      p->setPen(Qt::black);
      break;
    case TextColor::White:
      p->setPen(Qt::white);
      break;
    default:
      break;
  }
}

} // namespace dslmap
