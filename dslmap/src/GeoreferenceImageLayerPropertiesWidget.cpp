/**
 * Copyright 2020 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "GeoreferenceImageLayerPropertiesWidget.h"
#include "GeoreferenceImageLayer.h"
#include "GeoreferenceImagePropertiesWidget.h"
#include "ui_GeoreferenceImageLayerPropertiesWidget.h"

namespace dslmap {

Q_LOGGING_CATEGORY(dslmapGeoreferenceImageLayerPropertiesWidget,
                   "dslmap.object.geoimagepropertieswidget")

GeoreferenceImageLayerPropertiesWidget::GeoreferenceImageLayerPropertiesWidget(
  QWidget* parent)
  : QWidget(parent)
  , ui(new Ui::GeoreferenceImageLayerPropertiesWidget)
{
  ui->setupUi(this);
}

GeoreferenceImageLayerPropertiesWidget::
  ~GeoreferenceImageLayerPropertiesWidget()
{
  delete ui;
}

void
GeoreferenceImageLayerPropertiesWidget::setLayer(GeoreferenceImageLayer* layer)
{
  qCWarning(dslmapGeoreferenceImageLayerPropertiesWidget)
    << "setting properties layer";
  ui->general->setLayer(qobject_cast<MapLayer*>(layer));

  auto objs = layer->georeferenceImageObjects();
  for (auto obj : objs) {
    auto objWidg = new GeoreferenceImagePropertiesWidget(this);
    objWidg->setObject(obj);
    ui->tabWidget->addTab(objWidg, obj->objectName());
  }
}

} // namespace dslmap
