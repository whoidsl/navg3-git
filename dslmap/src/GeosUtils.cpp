/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "GeosUtils.h"

#include <QPainterPath>

#include <geos_c.h>
#include <memory>

namespace dslmap {

Q_LOGGING_CATEGORY(dslmapGeos, "dslmap.geos")

class GeosContext
{

public:
  static GEOSContextHandle_t& getInstance();

public:
  GeosContext(GeosContext const&) = delete;
  void operator=(GeosContext const&) = delete;

private:
  GeosContext();
  ~GeosContext();
  GEOSContextHandle_t m_handle;
};

GEOSContextHandle_t&
GeosContext::getInstance()
{
  static GeosContext context;
  return context.m_handle;
}

GeosContext::GeosContext()
  : m_handle(GEOS_init_r())
{
  GEOSContext_setNoticeMessageHandler_r(
    m_handle, [](const char* msg, void*) { qCInfo(dslmapGeos, "%s", msg); },
    nullptr);
  GEOSContext_setErrorMessageHandler_r(
    m_handle, [](const char* msg, void*) { qCCritical(dslmapGeos, "%s", msg); },
    nullptr);
}

GeosContext::~GeosContext()
{
  GEOS_finish_r(m_handle);
}

QPainterPath
pathFromWkt(const QString& wkt)
{

  if (wkt.isEmpty()) {
    return {};
  }

  const auto wkt_string = wkt.toStdString();

  auto& context = GeosContext::getInstance();

  auto reader_close = [&context](GEOSWKTReader_t* reader) {
    GEOSWKTReader_destroy_r(context, reader);
  };

  auto reader = std::unique_ptr<GEOSWKTReader_t, decltype(reader_close)>(
    GEOSWKTReader_create_r(context), reader_close);

  auto geometry_close = [&context](GEOSGeometry* geom) {
    GEOSGeom_destroy_r(context, geom);
  };

  auto geometry = std::unique_ptr<GEOSGeometry, decltype(geometry_close)>(
    GEOSWKTReader_read_r(context, reader.get(), wkt_string.data()),
    geometry_close);

  if (!geometry) {
    qCCritical(dslmapGeos) << "Unable to create geometry from:" << wkt;
    return {};
  }

  const auto num_geometries = GEOSGetNumGeometries_r(context, geometry.get());
  if (num_geometries < 0) {
    qCCritical(dslmapGeos) << "Unable to read number of geometries from WKT:"
                           << wkt;
    return {};
  }

  auto path = QPainterPath{};

  auto x = 0.0;
  auto y = 0.0;

  for (auto n = 0; n < num_geometries; ++n) {
    auto geom_n = GEOSGetGeometryN_r(context, geometry.get(), n);
    const auto geom_type = GEOSGeomTypeId_r(context, geom_n);
    if (geom_type != GEOS_LINESTRING) {
      const auto geom_name = GEOSGeomType_r(context, geometry.get());
      qCWarning(dslmapGeos, "Skipping geometry #%d (type: %d, %s)", n,
                geom_type, geom_name);
      free(geom_name);
      continue;
    }

    const auto coord_seq = GEOSGeom_getCoordSeq_r(context, geom_n);
    if (!coord_seq) {
      qCWarning(dslmapGeos,
                "Unable to get coordinate sequence for geometry #%d", n);
      continue;
    }

    auto length = static_cast<unsigned int>(0);
    if (!GEOSCoordSeq_getSize_r(context, coord_seq, &length)) {
      qCWarning(dslmapGeos, "Unable to get number of points for geom #%d", n);
      continue;
    }

    if (length < 2) {
      qCWarning(dslmapGeos, "Skipping geometry with %d points", length);
      continue;
    }

    // Move by the first point
    GEOSCoordSeq_getX_r(context, coord_seq, 0, &x);
    GEOSCoordSeq_getY_r(context, coord_seq, 0, &y);
    path.moveTo(x, y);
    for (unsigned int i = 1; i < length; ++i) {
      GEOSCoordSeq_getX_r(context, coord_seq, i, &x);
      GEOSCoordSeq_getY_r(context, coord_seq, i, &y);
      path.lineTo(x, y);
    }
    path.closeSubpath();
  }

  return path;
}
}
