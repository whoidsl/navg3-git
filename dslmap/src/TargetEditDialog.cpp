/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivandor on 4/22/22.
//

#include "TargetEditDialog.h"
#include "ui_TargetEditDialog.h"

#include "SymbolLayer.h"
#include "TargetTableModel.h"

#include <QFileDialog>
#include <iostream>

namespace dslmap {

TargetEditDialog::TargetEditDialog(QWidget* parent)
  : QDialog(parent)
  , ui(new Ui::TargetEditDialog)
{
  ui->setupUi(this);
  connect(ui->tgtDeleteBtn, &QPushButton::clicked, this,
          &TargetEditDialog::deleteTarget);
}

TargetEditDialog::~TargetEditDialog()
{
  delete ui;
}

void
TargetEditDialog::setLayer(SymbolLayer* layer)
{
  layer->printLatLonLabels();
  QVector<QVector<QString>> tableData;

  QVector<QString> headers{ "#", "Lat", "Lon", "Label" };
  tableData.push_back(headers);

  setTableData(tableData);
}

void
TargetEditDialog::setTableData(QVector<QVector<QString>> tableData)
{
  auto rows = tableData.size();
  if (rows < 1) {
    return;
  }
  auto columns = tableData[0].size();
  if (columns < 1) {
    return;
  }

  TargetTableModel* model = new TargetTableModel(columns, rows, tableData);
  auto* m = ui->preView->model();
  ui->preView->setModel(model);
  ui->preView->setShowGrid(false);
  ui->preView->horizontalHeader()->setVisible(false);
  ui->preView->verticalHeader()->setVisible(false);
  ui->preView->resizeColumnsToContents();
  delete m; // Prevent memory leak of old models
}

void
TargetEditDialog::deleteTarget() {
  const int result = QMessageBox::warning(
    this,
    "Delete Target",
    QString("You are about to delete target number %1.  Proceed?\n\n HIT OK AGAIN IN NEXT WINDOW TO CONFIRM").arg(this->ui->tgtNumEdit->text()),
    QMessageBox::Ok | QMessageBox::Cancel,
    QMessageBox::Cancel);
  if (result == QMessageBox::Ok) {
    target_num = ui->tgtNumEdit->text();
    emit tgtDeleteNum();
    qDebug() << "Deleting requested target";
  }
  else {
    target_num = nullptr;
    ui->tgtNumEdit->setPlaceholderText(QString::fromStdString("Enter Target # to Edit or Delete"));
    ui->tgtNumEdit->setText(QString::fromStdString(""));
    return;
  }
}

QString
TargetEditDialog::tgtDeleteNum()
{
  return target_num;
}

QString
TargetEditDialog::targetNum()
{
  return ui->tgtNumEdit->text();
}
QString
TargetEditDialog::targetLabel()
{
  return ui->tgtEdit->text();
}

} // namespace
