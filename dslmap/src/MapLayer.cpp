/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#include "MapLayer.h"
#include <Proj.h>

#include <MapLayer.h>
#include <QAction>
#include <QMenu>

namespace dslmap {

class MapLayerPrivate
{
public:
  MapLayerPrivate()
    : m_context_menu(new QMenu)
    , m_proj(Proj::fromDefinition(PROJ_WGS84_DEFINITION))
    , m_id(QUuid::createUuid())
  {
  }

  QScopedPointer<QMenu> m_context_menu;
  std::shared_ptr<const Proj> m_proj;
  QUuid m_id;
};

MapLayer::MapLayer(QGraphicsItem* parent)
  : QGraphicsObject(parent)
  , d_ptr(new MapLayerPrivate)
{
  Q_D(MapLayer);
  d->m_context_menu->setTitle(name());
  auto action = d->m_context_menu->addAction(QStringLiteral("Visible"));

  action->setCheckable(true);
  action->setChecked(isVisible());
  connect(action, &QAction::toggled, this,
          [=](bool visible) { setVisible(visible); });
  d->m_context_menu->addAction(action);

  action = d->m_context_menu->addAction(QStringLiteral("Properties"));
  connect(action, &QAction::triggered, this, [=]() { showPropertiesDialog(); });
  d->m_context_menu->addAction(action);

  connect(this, &MapLayer::objectNameChanged, this, &MapLayer::nameChanged);
  connect(this, &MapLayer::objectNameChanged, this, &MapLayer::setName);
}

MapLayer::~MapLayer()
{
  delete d_ptr;
}

QUuid
MapLayer::id() const
{
  const Q_D(MapLayer);
  return d->m_id;
}

QString
MapLayer::name() const noexcept
{
  return objectName();
}
std::shared_ptr<const Proj>
MapLayer::projection() const noexcept
{
  const Q_D(MapLayer);
  return d->m_proj;
}

const Proj*
MapLayer::projectionPtr() const noexcept
{
  const Q_D(MapLayer);
  return d->m_proj.get();
}

void
MapLayer::setProjection(std::shared_ptr<const Proj> proj)
{
  Q_D(MapLayer);
  d->m_proj = proj;
}

void
MapLayer::setName(const QString& name_) noexcept
{
  if (name_ == name()) {
    return;
  }
  setObjectName(name_);
  Q_D(MapLayer);
  d->m_context_menu->setTitle(name_);
}

QMenu*
MapLayer::contextMenu()
{
  Q_D(MapLayer);
  return d->m_context_menu.data();
}
}