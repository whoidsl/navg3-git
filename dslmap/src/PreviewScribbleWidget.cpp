/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 9/18/18.
//

#include "PreviewScribbleWidget.h"
#include <QPainter>
#include <QStaticText>

namespace dslmap {

PreviewScribbleWidget::PreviewScribbleWidget(QWidget* parent)
  : QWidget(parent)
{
  setUpdatesEnabled(true);
  m_brush.setColor(Qt::white);
  m_brush.setStyle(Qt::BrushStyle::NoBrush);
  m_pen.setStyle(Qt::PenStyle::SolidLine);
  m_pen.setColor(Qt::red);
  m_pen.setWidth(1);
  m_label_pen.setStyle(Qt::PenStyle::NoPen);
  m_label_pen.setColor(Qt::white);
  setBackgroundRole(QPalette::Base);
  m_path.addRect(20, 20, 60, 60);
  m_fontSizePoints = 12.0;
}

PreviewScribbleWidget::~PreviewScribbleWidget()
{
}

QSize
PreviewScribbleWidget::minimumSizeHint() const
{
  return QSize(50, 50);
}

QSize
PreviewScribbleWidget::sizeHint() const
{
  return QSize(100, 100);
}

void
PreviewScribbleWidget::setPen(QPen pen)
{
  m_pen = pen;
  update();
}

void
PreviewScribbleWidget::setColor(QColor color)
{
  if (!color.isValid()) {
    return;
  }
  if (color == m_pen.color()) {
    return;
  }
  m_pen.setColor(color);
  update();
}

void
PreviewScribbleWidget::setLabelSize(qreal points)
{
  if (points == m_fontSizePoints) {
    return;
  }
  m_fontSizePoints = points;
  update();
}

void
PreviewScribbleWidget::setLabelColor(QColor color)
{
  if (!color.isValid()) {
    return;
  }
  if (color == m_label_pen.color()) {
    return;
  }
  m_label_pen.setColor(color);
  update();
}

void
PreviewScribbleWidget::setFillColor(QColor color)
{
  if (!color.isValid()) {
    return;
  }
  if (color == m_brush.color()) {
    return;
  }
  m_brush.setColor(color);
  update();
}

void
PreviewScribbleWidget::setLabelVisible(bool visible)
{
  if (visible) {
    m_label_pen.setStyle(Qt::PenStyle::SolidLine);
  } else {
    m_label_pen.setStyle(Qt::PenStyle::NoPen);
  }
  update();
}

void
PreviewScribbleWidget::setFillVisible(bool visible)
{
  if (visible) {
    m_brush.setStyle(Qt::BrushStyle::SolidPattern);
  } else {
    m_brush.setStyle(Qt::BrushStyle::NoBrush);
  }

  update();
}

void
PreviewScribbleWidget::setPathVisible(bool visible)
{
  if (visible) {
    m_pen.setStyle(Qt::PenStyle::SolidLine);
  } else {
    m_pen.setStyle(Qt::PenStyle::NoPen);
  }
  update();
}

void
PreviewScribbleWidget::setPainterPath(QPainterPath path)
{
  path.translate(50, 50);
  m_path = path;
  update();
}

void
PreviewScribbleWidget::paintEvent(QPaintEvent* event)
{
  QWidget::paintEvent(event);

  QPainter painter(this);
  painter.fillRect(0, 0, 100, 100, Qt::black);
  painter.setBrush(m_brush);
  painter.setPen(m_pen);
  painter.drawPath(m_path);
  painter.setPen(m_label_pen);

  auto font = painter.font();
  font.setPointSizeF(m_fontSizePoints);
  painter.setFont(font);

  painter.drawStaticText(5, 75, QStaticText("Label"));
}
} // namespace
