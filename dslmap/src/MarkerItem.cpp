/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 9/21/17.
//

#include "MarkerItem.h"

#include <QDebug>
#include <QPainter>

namespace dslmap {

MarkerItem::MarkerItem(QGraphicsItem* parent)
  : QGraphicsItem(parent)
{
  // Enable hover events for all markers by default.
  setAcceptHoverEvents(true);
  setFlag(ItemIgnoresTransformations, true);
}

MarkerItem::MarkerItem(std::shared_ptr<ShapeMarkerSymbol>& symbol,
                       QGraphicsItem* parent)
  : MarkerItem(parent)
{
  setSymbol(symbol);
}

MarkerItem::~MarkerItem()
{
  if (m_symbol) {
    m_symbol->removeItem(this);
  }
}

MarkerItem*
MarkerItem::clone()
{
  auto other = new MarkerItem;
  other->setSymbol(symbol());
  other->setAttribute(attributes());
  other->setLabelText(labelText());
  return other;
}

QRectF
MarkerItem::boundingRect() const
{
  if (!m_symbol) {
    return mapRectFromScene(QRectF().translated(scenePos()));
  }

  // Explanation: Take the symbol bounding rect.  Translate it to the item's
  // scene
  // coordinates, then map the rectangle BACK into item coordinates.
  return mapRectFromScene(
    m_symbol->path().boundingRect().translated(scenePos()));
}

void
MarkerItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option,
                  QWidget* widget)
{

  if (!m_symbol) {
    return;
  }

  m_symbol->renderItem(this, painter, option, widget);
}

void
MarkerItem::setSymbol(std::shared_ptr<MarkerSymbol> symbol)
{
  if (m_symbol) {
    m_symbol->removeItem(this);
  }

  m_symbol = std::move(symbol);

  if (!m_symbol) {
    return;
  }

  m_symbol->addItem(this);
  setFlag(QGraphicsItem::ItemIgnoresTransformations,
          m_symbol->isScaleInvariant());
  prepareGeometryChange();
}

std::shared_ptr<MarkerSymbol>
MarkerItem::symbol() const
{
  return m_symbol;
}

MarkerItem::Attributes
MarkerItem::attributes() const
{
  return m_attributes;
}

void
MarkerItem::clearAttributes()
{
  m_attributes.clear();
}

void
MarkerItem::setAttribute(const QString& name, QVariant value)
{
  m_attributes.insert(name, value);
  setToolTip(formatAttributes());
}

void
MarkerItem::setAttribute(const MarkerItem::Attributes& attributes,
                         bool clear_existing)
{
  if (clear_existing) {
    m_attributes.clear();
  }

  for (auto it = std::begin(attributes); it != std::end(attributes); it++) {
    m_attributes.insert(it.key(), it.value());
  }
  setToolTip(formatAttributes());
}

void
MarkerItem::removeAttribute(const QString& name)
{
  m_attributes.remove(name);
  setToolTip(formatAttributes());
}

void
MarkerItem::removeAttribute(const QStringList& names)
{
  foreach (auto name, names) {
    m_attributes.remove(name);
  }
  setToolTip(formatAttributes());
}

QString
MarkerItem::formatAttributes()
{
  if (m_attributes.isEmpty()) {
    return QString();
  }

  auto attributes = QStringList{};
  attributes.reserve(m_attributes.size());

  for (auto item = std::begin(m_attributes); item != std::end(m_attributes);
       item++) {
    attributes << QString("%1: %2").arg(item.key(), item.value().toString());
  }

  return attributes.join("\n");
}

QStaticText&
MarkerItem::label()
{
  return m_label;
}

QString
MarkerItem::labelText() const
{
  return m_label.text();
}

void
MarkerItem::setLabelText(const QString& label)
{
  m_label.setText(label);
}
}
