/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 10/16/17.
//

#include "GdalUtils.h"
#include <QTransform>

namespace dslmap {
Q_LOGGING_CATEGORY(dslmapGdal, "dslmap.gdal")

QPolygonF
adfProjectionToPolygon(const QSize& size, const QVector<double>& adfTransform)
{
  if (adfTransform.size() < 6) {
    qCWarning(dslmapGdal)
      << "provided adfGeometryTransform too small.  Need 6 elements, got"
      << adfTransform.size();
    return {};
  }

  const auto size_x = size.width();
  const auto size_y = size.height();

  qDebug() << "Size:" << size;

  // Top-left is always 0,0  We'll translate it after creating the polygon.
  const auto top_left = QPointF{ 0, 0 };

  const auto top_right = QPointF{
    size_x * adfTransform[1], size_x * adfTransform[4],
  };

  const auto bottom_right =
    QPointF{ size_x * adfTransform[1] + size_y * adfTransform[2],
             size_x * adfTransform[4] + size_y * adfTransform[5] };

  const auto bottom_left =
    QPointF{ size_y * adfTransform[2], size_y * adfTransform[5] };

  auto raster_polygon = QPolygonF{ QVector<QPointF>{
    top_left, top_right, bottom_right, bottom_left } };

  raster_polygon.translate(adfTransform[0], adfTransform[3]);
  qDebug() << "Poly:" << raster_polygon;

  return raster_polygon;
}

QTransform
gdalGeometryToTransform(const QVector<double>& adfTransform, bool* ok)
{

  if (adfTransform.size() < 6) {
    if (ok) {
      *ok = false;
    }
    return {};
  }

  if (ok) {
    *ok = true;
  }

  return QTransform{ adfTransform[1], adfTransform[4], 0,
                     adfTransform[2], adfTransform[5], 0,
                     adfTransform[0], adfTransform[3], 1 };
}

QVector<double>
polygonToAdfProjection(const QSize& size, const QPolygonF& polygon)
{
  if (polygon.size() < 4) {
    qCWarning(dslmapGdal)
      << "provided polygon too small.  Need 4 verices, found" << polygon.size();
    return {};
  }

  auto ok = false;
  const auto transform = polygonTransform(size, polygon, &ok);

  if (!ok) {
    qCWarning(dslmapGdal)
      << "unable to calculate inverse perspective transform for given polygon.";
    return {};
  }

  return {
    transform.m31(), // X origin translation
    transform.m11(), // X scaling
    transform.m21(), // X sheering
    transform.m32(), // Y origin translation
    transform.m12(), // Y scaling
    transform.m22()  // Y sheering
  };
}

QTransform
polygonTransform(const QSize& size, const QPolygonF& polygon, bool* ok)
{

  auto transform = QTransform{};
  if (!QTransform::quadToSquare(polygon, transform)) {
    if (ok) {
      *ok = false;
    }
    return {};
  }

  // quadToSquare transforms the polygon onto a unit-square which is fine if
  // we've got a unit-square raster.  But we probably don't, so we need an
  // extra scaling adjusting by the width/height of our image.
  transform.scale(transform.m11() / size.width(),
                  transform.m22() / size.height());

  if (ok) {
    *ok = true;
  }

  return transform;
}
}
