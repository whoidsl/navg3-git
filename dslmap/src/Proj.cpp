/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 10/19/17.
//

#include "Proj.h"
#include "ProjPrivate.h"

#include <QMetaEnum>
#include <QPointF>
#include <QtMath>

#include <gdal/ogr_spatialref.h>
#include <geodesic.h>
#include <proj_api.h>

namespace dslmap {
Q_LOGGING_CATEGORY(dslmapProj, "dslmap.proj")

Proj::Proj(projPJ proj, std::shared_ptr<projCtx_> context)
  : d_ptr(new ProjPrivate(this, proj, context))
{
  Q_D(Proj);

  if (!d->m_proj) {
    return;
  }

  d->m_def = QString(pj_get_def(d->m_proj.get(), 0)).trimmed();
  foreach (auto& item, d->m_def.splitRef(' ')) {
    // This assumes that all proj definition arguments start with '+'
    const auto pair = item.mid(1, -1).split('=', QString::SkipEmptyParts);
    if (pair.size() > 1) {
      d->m_args.insert(pair[0].toString(), pair[1].toString());
    } else {
      d->m_args.insert(pair[0].toString(), true);
    }
  }
  d->m_is_latlon = static_cast<bool>(pj_is_latlong(d->m_proj.get()));
  auto ok = true;
  auto unit_and_scale = scaleFactorFromDefinition(d->m_def, &ok);
  if (!ok) {
    qWarning(dslmapProj)
      << "Unable to parse units and scale factor from definition" << d->m_def;
    return;
  }

  d->m_unit = unit_and_scale.first;
  d->m_scale_factor = unit_and_scale.second;
}

std::shared_ptr<Proj>
Proj::fromDefinition(const QString& def, std::shared_ptr<projCtx_> context)
{
  if (!context) {
    context = defaultContext();
  }

  auto ptr = pj_init_plus_ctx(context.get(), def.toLatin1());
  if (ptr == nullptr) {
    return {};
  }

  auto proj = std::shared_ptr<Proj>(new Proj(ptr, context));

  return proj;
}

Proj::~Proj() = default;

bool
Proj::isLatLon() const noexcept
{
  const Q_D(Proj);
  return d->m_is_latlon;
}

std::shared_ptr<Proj>
Proj::fromDefinition(const QStringList& definition,
                     std::shared_ptr<void> context)
{
  // Make sure each element starts with a '+'
  auto def = QString{};
  foreach (auto arg, definition) {
    if (!arg.trimmed().startsWith('+')) {
      def += "+";
    }
    def += arg;
  }
  return fromDefinition(def, context);
}

QString
Proj::definition() const noexcept
{
  const Q_D(Proj);
  return d->m_def;
}

bool
Proj::hasError() const noexcept
{
  const Q_D(Proj);
  return d->m_err != 0;
}

QPair<int, QString>
Proj::getError()
{

  Q_D(Proj);

  QMutexLocker locker(&d->m_mutex);
  const auto err = std::exchange(d->m_err, 0);
  const auto msg = QString{ pj_strerrno(err) };

  return { err, msg };
}

QPair<int, QString>
Proj::lastError(projCtx context)
{
  if (context == nullptr) {
    context = pj_get_default_ctx();
  }

  const auto err = pj_ctx_get_errno(context);
  return { err, pj_strerrno(err) };
}

std::shared_ptr<projCtx_>
Proj::context() noexcept
{
  Q_D(Proj);
  return d->m_ctx;
}

projPJ
Proj::projection() const noexcept
{
  const Q_D(Proj);
  return d->m_proj.get();
}

void
Proj::setContext(std::shared_ptr<projCtx_> context)
{
  if (!context) {
    return;
  }

  Q_D(Proj);
  QMutexLocker locker(&d->m_mutex);
  d->m_ctx = context;
  pj_set_ctx(d->m_proj.get(), d->m_ctx.get());
}

std::shared_ptr<projCtx_>
Proj::createContext()
{
  auto ptr = pj_ctx_alloc();
  return std::shared_ptr<projCtx_>(ptr, [](projCtx p) { pj_ctx_free(p); });
}

std::shared_ptr<projCtx_>
Proj::defaultContext()
{
  static auto ptr =
    std::shared_ptr<projCtx_>(pj_get_default_ctx(), [](projCtx) {});
  return ptr;
}

std::shared_ptr<Proj>
Proj::utmFromLonLat(qreal lon, qreal lat, std::shared_ptr<void> context)
{
  const auto zone =
    static_cast<quint8>(std::fmod(std::floor((lon + 180) / 6), 60) + 1);

  auto def =
    QString("+proj=utm +zone=%1 +ellps=WGS84 +datum=WGS84 +units=m").arg(zone);

  if (lat < 0) {
    def += " +south";
  }

  def += " +no_defs";
  return Proj::fromDefinition(def, context);
}

std::shared_ptr<Proj>
Proj::alvinXYFromOrigin(qreal lon, qreal lat, std::shared_ptr<void> context)
{
  const auto def =
    QString("+proj=tmerc +datum=WGS84 +units=m +lon_0=%1 +lat_0=%2")
      .arg(lon, 0, 'f', 10)
      .arg(lat, 0, 'f', 10);
  return Proj::fromDefinition(def, context);
}

std::shared_ptr<Proj>
Proj::latLongFromProj()
{
  Q_D(Proj);

  QMutexLocker locker(&d->m_mutex);

  auto ptr = pj_latlong_from_proj(d->m_proj.get());
  if (ptr == nullptr) {
    return {};
  }

  return std::shared_ptr<Proj>(new Proj(ptr, d->m_ctx));
}

Unit
Proj::units() const noexcept
{
  const Q_D(Proj);
  return d->m_unit;
}

qreal
Proj::scaleFactorToMeters() const noexcept
{
  const Q_D(Proj);
  return d->m_scale_factor;
}

QPair<Unit, qreal>
Proj::scaleFactorFromDefinition(const QString& definition, bool* ok)
{

  // Parsing has failed by default..
  if (ok) {
    *ok = false;
  }

  auto result = QPair<Unit, qreal>{ Unit::invalid, -1.0 };

  const auto fields = definition.split(' ', QString::SkipEmptyParts);

  const auto proj_it =
    std::find_if(std::begin(fields), std::end(fields),
                 [](auto field) { return field.startsWith("+proj="); });

  const auto unit_it =
    std::find_if(std::begin(fields), std::end(fields),
                 [](auto field) { return field.startsWith("+units="); });

  const auto factor_it =
    std::find_if(std::begin(fields), std::end(fields),
                 [](auto field) { return field.startsWith("+to_meter="); });

  // No projection field?  Well.. not much we can do here.
  if (proj_it == std::end(fields)) {
    return result;
  }

  // Geographic projection...
  if (proj_it->contains("lat")) {
    result.first = Unit::degrees;
    result.second = -1.0;
    if (ok) {
      *ok = true;
    }
    return result;
  }

  // Defaults
  auto unit = Unit::m;
  auto scale = 0.0;
  auto _ok = false; // local parsing check variable

  // If the unit field is present, try parsing it.
  if (unit_it != std::end(fields)) {
    unit = stringToUnit(unit_it->mid(7, -1));
    if (unit == Unit::invalid) {
      return result;
    }
  }
  // Nothing to do for the 'else' case here, it's covered by the
  // initialization where we assume meters.

  // If the scale field is present, try parsing it.
  if (factor_it != std::end(fields)) {
    scale = factor_it->midRef(10, -1).toDouble(&_ok);
    if (!_ok) {
      return result;
    }
    // And if the unit field was NOT present, then set the unit to custom.
    if (unit_it == std::end(fields)) {
      unit = Unit::custom;
    }
  } else {
    // Otherwise, just look up the scale for the parsed unit.
    scale = scaleToMeters(unit);
  }

  result.first = unit;
  result.second = scale;

  if (ok) {
    *ok = true;
  }

  return result;
}
int
Proj::transformTo(const Proj& other, QVector<double>& x, QVector<double>& y,
                  bool is_degrees) const
{
  auto z = QVector<double>{};
  return transformTo(other, x, y, z, is_degrees);
}

int
Proj::transformTo(const Proj& other, QVector<double>& x, QVector<double>& y,
                  QVector<double>& z, bool is_degrees) const
{
  auto num_points = x.size();
  if (x.size() != y.size()) {
    qCWarning(dslmapProj) << "provided x and y vectors of different lengths"
                          << "x:" << x.size() << "y:" << y.size();
    num_points = qMin(x.size(), y.size());
    qCWarning(dslmapProj) << "will transform" << num_points << "points";
  }

  if (!z.isEmpty() && (z.size() != x.size())) {
    qCWarning(dslmapProj) << "provided x and y vectors of different lengths"
                          << "x:" << x.size() << "z:" << z.size();
    num_points = qMin(z.size(), num_points);
    qCWarning(dslmapProj) << "will transform" << num_points << "points";
  }

  if (num_points == 0) {
    qCWarning(dslmapProj) << "No points to transform.";
    return 0;
  }

  auto x_data = x.data();
  auto y_data = y.data();
  auto z_data = z.isEmpty() ? nullptr : z.data();

  const auto err =
    transformTo(other, num_points, x_data, y_data, z_data, 0, is_degrees);

  if (err) {
    const Q_D(Proj);
    QMutexLocker locker(&d->m_mutex);
    d->m_err = pj_ctx_get_errno(d->m_ctx.get());
    qWarning(dslmapProj) << "Error transforming points:" << d->m_err
                         << pj_strerrno(d->m_err);
    return 0;
  }

  return num_points;
}

int
Proj::transformTo(const Proj& other, int count, double* x, double* y, double* z,
                  int offset, bool is_degrees) const
{

  // Convert to radians if needed
  if (isLatLon() && is_degrees) {
    if (z == nullptr) {
      auto xi = x;
      auto yi = y;
      for (auto i = 0; i < count; ++i) {
        *xi *= DEG_TO_RAD;
        *yi *= DEG_TO_RAD;
        xi++;
        yi++;
      }
    } else {
      auto xi = x;
      auto yi = y;
      auto zi = z;
      for (auto i = 0; i < count; ++i) {
        *xi *= DEG_TO_RAD;
        *yi *= DEG_TO_RAD;
        *zi *= DEG_TO_RAD;
        xi++;
        yi++;
        zi++;
      }
    }
  }

  auto pj_src = projection();
  auto pj_dst = const_cast<Proj&>(other).projection();
  auto err = pj_transform(pj_src, pj_dst, count, offset, x, y, z);

  if (other.isLatLon() && is_degrees) {
    if (z == nullptr) {
      auto xi = x;
      auto yi = y;
      for (auto i = 0; i < count; ++i) {
        *xi *= RAD_TO_DEG;
        *yi *= RAD_TO_DEG;
        xi++;
        yi++;
      }
    } else {
      auto xi = x;
      auto yi = y;
      auto zi = z;
      for (auto i = 0; i < count; ++i) {
        *xi *= RAD_TO_DEG;
        *yi *= RAD_TO_DEG;
        *zi *= RAD_TO_DEG;
        xi++;
        yi++;
        zi++;
      }
    }
  }

  return err;
}

int
Proj::transformFromLonLat(QVector<double>& lon, QVector<double>& lat,
                          bool is_degrees) const
{

  auto& latlon_proj = ProjWGS84::getInstance();
  return latlon_proj.transformTo(*this, lon, lat, is_degrees);
}

int
Proj::transformFromLonLat(QVector<QPointF>& lonlat, bool is_degrees) const
{
  auto lon = QVector<double>{};
  auto lat = QVector<double>{};

  lon.reserve(lonlat.size());
  lat.reserve(lonlat.size());

  foreach (auto xy, lonlat) {
    lon << xy.x();
    lat << xy.y();
  }
  const auto n = transformFromLonLat(lon, lat, is_degrees);
  if (n != lonlat.size()) {
    qCWarning(dslmapProj) << "Potential error in conversion."
                          << "Converted:" << n << "of" << lonlat.size();
  }

  for (auto i = 0; i < n; i++) {
    lonlat[i] = QPointF{ lon.at(i), lat.at(i) };
  }

  return n;
}

int
Proj::transformFromLonLat(int num, double* lon, double* lat,
                          bool is_degrees) const
{
  const auto& latlon_proj = ProjWGS84::getInstance();
  return latlon_proj.transformTo(*this, num, lon, lat, nullptr, 0, is_degrees);
}

int
Proj::transformToLonLat(int num, double* lon, double* lat,
                        bool is_degrees) const
{
  const auto& latlon_proj = ProjWGS84::getInstance();
  return transformTo(latlon_proj, num, lon, lat, nullptr, 0, is_degrees);
}

int
Proj::transformToLonLat(QVector<double>& x, QVector<double>& y,
                        bool is_degrees) const
{
  auto& latlon_proj = ProjWGS84::getInstance();
  return transformTo(latlon_proj, x, y, is_degrees);
}

int
Proj::transformToLonLat(QVector<QPointF>& xy, bool is_degrees) const
{

  auto x = QVector<double>{};
  auto y = QVector<double>{};

  x.reserve(xy.size());
  y.reserve(xy.size());

  foreach (auto point, xy) {
    x << point.x();
    y << point.y();
  }

  const auto n = transformToLonLat(x, y, is_degrees);
  if (n != xy.size()) {
    qCWarning(dslmapProj) << "Potential error in conversion."
                          << "Converted:" << n << "of" << xy.size();
  }

  for (auto i = 0; i < n; i++) {
    xy[i] = QPointF{ x.at(i), y.at(i) };
  }

  return n;
}

QVariant
Proj::parameter(const QString& parameter) const
{
  const Q_D(Proj);

  // Strip any leading '+'
  if (parameter.startsWith('+')) {
    return d->m_args.value(parameter.mid(1, -1));
  }

  return d->m_args.value(parameter);
}

QVariantHash
Proj::parameters() const
{
  const Q_D(Proj);
  return d->m_args;
}

bool
Proj::isSame(const Proj& other) const
{

  // Need to add the special "+wktext" extension here so that
  // OGRSpatialReference can handle grids in the comparison.
  const auto our_def = definition() + " +wktext";
  const auto other_def = other.definition() + " +wktext";

  OGRSpatialReference our_srs;
  OGRSpatialReference other_srs;

  auto err = our_srs.importFromProj4(our_def.toLatin1());
  if (err != OGRERR_NONE) {
    qCWarning(dslmapProj) << "Unable to import proj4 def (err:" << err
                          << "):" << our_def;
    return false;
  }

  err = other_srs.importFromProj4(other_def.toLatin1());
  if (err != OGRERR_NONE) {
    qCWarning(dslmapProj) << "Unable to import proj4 def (err:" << err
                          << "):" << other_def;
    return false;
  }

  return static_cast<bool>(our_srs.IsSame(&other_srs));
}

double
Proj::geodeticDistance(const QPointF& from, const QPointF& to,
                       double* from_bearing, double* to_bearing) const
{
  return geodeticDistance(from.x(), from.y(), to.x(), to.y(), from_bearing,
                          to_bearing);
}

double
Proj::geodeticDistance(double from_x, double from_y, double to_x, double to_y,
                       double* from_bearing, double* to_bearing) const
{
  if (isLatLon()) {
    auto is_degrees = units() == Unit::degrees;
    return ProjWGS84::geodeticDistance(from_x, from_y, to_x, to_y, from_bearing,
                                       to_bearing, is_degrees);
  }

  auto& wgs84 = ProjWGS84::getInstance();
  transformTo(wgs84, 1, &from_x, &from_y, nullptr, 0, true);
  transformTo(wgs84, 1, &to_x, &to_y, nullptr, 0, true);

  return ProjWGS84::geodeticDistance(from_x, from_y, to_x, to_y, from_bearing,
                                     to_bearing, true);
}

//
// ProjWGS84 methods
//

Proj&
ProjWGS84::getInstance()
{
  static ProjWGS84 instance;
  return *(instance.m_proj.get());
}

ProjWGS84::ProjWGS84()
  : m_proj(Proj::fromDefinition(PROJ_WGS84_DEFINITION))
{
}

double
ProjWGS84::geodeticDistance(const QPointF& from_LonLat,
                            const QPointF& to_LonLat, double* from_bearing,
                            double* to_bearing, bool is_degrees)
{
  return ProjWGS84::geodeticDistance(from_LonLat.x(), from_LonLat.y(),
                                     to_LonLat.x(), to_LonLat.y(), from_bearing,
                                     to_bearing, is_degrees);
}

double
ProjWGS84::geodeticDistance(double lon1, double lat1, double lon2, double lat2,
                            double* from_bearing, double* to_bearing,
                            bool is_degrees)
{
  struct geod_geodesic g;
  geod_init(&g, 6378137, 1 / 298.257223563);

  if (!is_degrees) {
    lon1 *= RAD_TO_DEG;
    lat1 *= RAD_TO_DEG;
    lon2 *= RAD_TO_DEG;
    lat2 *= RAD_TO_DEG;
  }

  auto d = 0.0;
  geod_inverse(&g, lat1, lon1, lat2, lon2, &d, from_bearing, to_bearing);
  return d;
}

ProjWGS84::~ProjWGS84() = default;

//
// Utility Functions
//

qreal
scaleToMeters(Unit unit)
{

  switch (unit) {

    case (Unit::m): {
      return 1;
    }

    case (Unit::km): {
      return 1000;
    }

    case (Unit::dm): {
      return 0.1;
    }

    case (Unit::cm): {
      return 0.01;
    }

    case (Unit::mm): {
      return 0.001;
    }

    case (Unit::kmi): {
      return 1852.0;
    }

    case (Unit::in): {
      return 0.0254;
    }

    case (Unit::ft): {
      return 0.3048;
    }

    case (Unit::yd): {
      return 0.9144;
    }

    case (Unit::mi): {
      return 1609.344;
    }

    case (Unit::fath): {
      return 1.8288;
    }

    case (Unit::ch): {
      return 20.1168;
    }

    case (Unit::link): {
      return 0.201168;
    }

    case (Unit::us_in): {
      return 1. / 39.37;
    }

    case (Unit::us_ft): {
      return 0.304800609601219;
    }

    case (Unit::us_yd): {
      return 0.914401828803658;
    }

    case (Unit::us_ch): {
      return 20.11684023368047;
    }

    case (Unit::us_mi): {
      return 1609.347218694437;
    }

    case (Unit::ind_yd): {
      return 0.91439523;
    }

    case (Unit::ind_ft): {
      return 0.30479841;
    }

    case (Unit::ind_ch): {
      return 20.11669506;
    }

    case Unit::degrees:
    case Unit::custom:
    case Unit::invalid: {
      return -1;
    }
  }

  return -1;
}

Unit DSLMAPLIB_EXPORT
stringToUnit(const QString& string)
{
  const auto unit = string.trimmed();

  if (unit == "km") {
    return Unit::km;
  }

  if (unit == "m") {
    return Unit::m;
  }

  if (unit == "dm") {
    return Unit::dm;
  }

  if (unit == "cm") {
    return Unit::cm;
  }

  if (unit == "mm") {
    return Unit::mm;
  }

  if (unit == "kmi") {
    return Unit::kmi;
  }

  if (unit == "in") {
    return Unit::in;
  }

  if (unit == "ft") {
    return Unit::ft;
  }

  if (unit == "yd") {
    return Unit::yd;
  }

  if (unit == "mi") {
    return Unit::mi;
  }

  if (unit == "fath") {
    return Unit::fath;
  }

  if (unit == "ch") {
    return Unit::ch;
  }

  if (unit == "link") {
    return Unit::link;
  }

  if (unit == "us_in") {
    return Unit::us_in;
  }

  if (unit == "us_ft") {
    return Unit::us_ft;
  }

  if (unit == "us_yd") {
    return Unit::us_yd;
  }

  if (unit == "us_ch") {
    return Unit::us_ch;
  }

  if (unit == "us_mi") {
    return Unit::us_mi;
  }

  if (unit == "ind_yd") {
    return Unit::ind_yd;
  }

  if (unit == "ind_ft") {
    return Unit::ind_ft;
  }

  if (unit == "ind_ch") {
    return Unit::ind_ch;
  }

  return Unit::invalid;
}
}
