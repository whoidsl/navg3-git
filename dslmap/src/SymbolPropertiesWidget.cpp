/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 9/18/18.
//

#include "SymbolPropertiesWidget.h"
#include "ui_SymbolPropertiesWidget.h"

namespace dslmap {

SymbolPropertiesWidget::SymbolPropertiesWidget(QWidget* parent)
  : QWidget(parent)
  , ui(new Ui::SymbolPropertiesWidget)
{
  ui->setupUi(this);

  setupShape();
  setupConnections();
}

SymbolPropertiesWidget::~SymbolPropertiesWidget()
{
  delete ui;
}

void
SymbolPropertiesWidget::setupShape()
{
  ui->shapeBox->addItem("Circle");
  ui->shapeBox->addItem("Square");
  ui->shapeBox->addItem("Diamond");
  ui->shapeBox->addItem("Cross");
  ui->shapeBox->addItem("X");
}

void
SymbolPropertiesWidget::setupConnections()
{
  // Preview color and shape changes
  connect(this, &SymbolPropertiesWidget::outlineColorChanged, ui->scribble,
          &PreviewScribbleWidget::setColor);
  connect(this, &SymbolPropertiesWidget::pathChanged, ui->scribble,
          &PreviewScribbleWidget::setPainterPath);
  connect(this, &SymbolPropertiesWidget::outlineVisibleChanged, ui->scribble,
          &PreviewScribbleWidget::setPathVisible);
  connect(this, &SymbolPropertiesWidget::labelSizeChanged, ui->scribble,
          &PreviewScribbleWidget::setLabelSize);
  connect(this, &SymbolPropertiesWidget::labelColorChanged, ui->scribble,
          &PreviewScribbleWidget::setLabelColor);
  connect(this, &SymbolPropertiesWidget::labelVisibleChanged, ui->scribble,
          &PreviewScribbleWidget::setLabelVisible);
  connect(this, &SymbolPropertiesWidget::fillVisibleChanged, ui->scribble,
          &PreviewScribbleWidget::setFillVisible);
  connect(this, &SymbolPropertiesWidget::fillColorChanged, ui->scribble,
          &PreviewScribbleWidget::setFillColor);

  // Visibility of Label, Fill, and Outline
  connect(ui->labelChk, &QCheckBox::toggled,
          [=](const bool& visible) { emit labelVisibleChanged(visible); });
  connect(ui->fillChk, &QCheckBox::toggled,
          [=](const bool& visible) { emit fillVisibleChanged(visible); });
  connect(ui->outlineChk, &QCheckBox::toggled,
          [=](const bool& visible) { emit outlineVisibleChanged(visible); });

  // Emit color change signals
  connect(ui->outlineColor, &ColorPickButton::colorChanged,
          [=](const QColor color) { emit outlineColorChanged(color); });
  connect(ui->fillColor, &ColorPickButton::colorChanged,
          [=](const QColor color) { emit fillColorChanged(color); });
  connect(ui->labelColor, &ColorPickButton::colorChanged,
          [=](const QColor color) { emit labelColorChanged(color); });

  // Emit Size/shape change signals
  connect(ui->labelSizeBox,
          static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged),
          [=](const int value) {
            m_labelSize = static_cast<qreal>(value);
            emit labelSizeChanged(m_labelSize);
          });
  connect(ui->symbolSizeBox,
          static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged),
          [=](const int value) {
            m_symbolSize = static_cast<qreal>(value);
            emit pathChanged(ShapeMarkerSymbol::pathFromShape(m_shape, m_symbolSize));
            emit shapeChanged(m_shape, m_symbolSize);
          });
  connect(ui->shapeBox, static_cast<void (QComboBox::*)(int)>(
                          &QComboBox::currentIndexChanged),
          [=](const int index) {
            m_shape = static_cast<ShapeMarkerSymbol::Shape>(index);
            emit pathChanged(ShapeMarkerSymbol::pathFromShape(m_shape, m_symbolSize));
            emit shapeChanged(m_shape, m_symbolSize);
          });
}

void
SymbolPropertiesWidget::setName(QString title)
{
  ui->groupBox->setTitle(title);
}

void
SymbolPropertiesWidget::setLabelVisible(bool visible)
{
  if (visible == ui->labelChk->isChecked()) {
    return;
  }
  ui->labelChk->setChecked(visible);
}

void
SymbolPropertiesWidget::setFillVisible(bool visible)
{
  if (visible == ui->fillChk->isChecked()) {
    return;
  }
  ui->fillChk->setChecked(visible);
}

void
SymbolPropertiesWidget::setOutlineVisible(bool visible)
{
  if (visible == ui->outlineChk->isChecked()) {
    return;
  }
  ui->outlineChk->setChecked(visible);
}

void
SymbolPropertiesWidget::setOutlineColor(QColor color)
{
  ui->outlineColor->setColor(color);
}

void
SymbolPropertiesWidget::setFillColor(QColor color)
{
  ui->fillColor->setColor(color);
}

void
SymbolPropertiesWidget::setLabelColor(QColor color)
{
  ui->labelColor->setColor(color);
}

void
SymbolPropertiesWidget::setShape(ShapeMarkerSymbol::Shape shape)
{
  m_shape = shape;
  ui->shapeBox->setCurrentIndex(static_cast<int>(shape));
}

void
SymbolPropertiesWidget::setLabelSize(qreal points)
{
  m_labelSize = points;
  ui->labelSizeBox->setValue(static_cast<int>(points));
}

void
SymbolPropertiesWidget::setSymbolSize(qreal size)
{
  m_symbolSize = size;
  ui->symbolSizeBox->setValue(static_cast<int>(size));
}

void
SymbolPropertiesWidget::setPainterPath(QPainterPath path)
{
  emit pathChanged(path);
}

} // namespace
