/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 7/25/18.
//

#include "RasterLayer.h"
#include "GeneralLayerPropertiesWidget.h"
#include "ImageObject.h"
#include "Proj.h"

namespace dslmap {
Q_LOGGING_CATEGORY(dslmapRasterLayer, "dslmap.layer.raster")

RasterLayer::RasterLayer(QGraphicsItem* parent)
  : MapLayer(parent)
{
}

RasterLayer::~RasterLayer() = default;

bool RasterLayer::saveSettings(QSettings& settings){return false;}
bool RasterLayer::loadSettings(QSettings& settings){return false;}

QRectF
RasterLayer::boundingRect() const
{
  return QRectF();
}

void
RasterLayer::paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*)
{
}

ImageObject*
RasterLayer::addImage(const QString& filename, const QRectF& bounds,
                      std::shared_ptr<Proj> proj)
{
  auto img = std::make_unique<ImageObject>();
  if (!img->setImage(filename, bounds, proj)) {
    return nullptr;
  }

  // This takes ownership of the object
  img->setParentItem(this);
  return img.release();
}

ImageObject*
RasterLayer::addImage(const QString& filename, const QPolygonF& bounds,
                      std::shared_ptr<Proj> proj)
{
  auto img = std::make_unique<ImageObject>();
  if (!img->setImage(filename, bounds, proj)) {
    return nullptr;
  }

  // This takes ownership of the object
  img->setParentItem(this);
  return img.release();
}

ImageObject*
RasterLayer::addImage(const QImage& image, const QRectF& bounds,
                      std::shared_ptr<Proj> proj)
{
  auto img = std::make_unique<ImageObject>();
  if (!img->setImage(image, bounds, proj)) {
    return nullptr;
  }

  // This takes ownership of the object
  img->setParentItem(this);
  return img.release();
}

ImageObject*
RasterLayer::addImage(const QImage& image, const QPolygonF& bounds,
                      std::shared_ptr<Proj> proj)
{

  auto img = std::make_unique<ImageObject>();
  if (!img->setImage(image, bounds, proj)) {
    return nullptr;
  }

  // This takes ownership of the object
  img->setParentItem(this);
  return img.release();
}

ImageObject*
RasterLayer::addImage(const QString& filename, const QRectF& bounds)
{
  qCDebug(dslmapRasterLayer) << "Adding image with filename " << filename << " and bounds " << bounds;
  auto proj = projection();
  qCDebug(dslmapRasterLayer) << "Layer projection is:" << proj->definition();
  if (!proj) {
    return nullptr;
  }

  auto img = std::make_unique<ImageObject>();
  if (!img->setImage(filename, bounds, proj)) {
    return nullptr;
  }

  // This takes ownership of the object
  img->setParentItem(this);
  return img.release();
}

ImageObject*
RasterLayer::addImage(const QString& filename, const QPolygonF& bounds)
{
  auto proj = projection();
  qCDebug(dslmapRasterLayer) << "Layer projection is:" << proj->definition();
  if (!proj) {
    return nullptr;
  }

  auto img = std::make_unique<ImageObject>();
  if (!img->setImage(filename, bounds, proj)) {
    return nullptr;
  }

  // This takes ownership of the object
  img->setParentItem(this);
  return img.release();
}

ImageObject*
RasterLayer::addImage(const QImage& image, const QRectF& bounds)
{
  auto proj = projection();
  qCDebug(dslmapRasterLayer) << "Layer projection is:" << proj->definition();
  if (!proj) {
    return nullptr;
  }

  auto img = std::make_unique<ImageObject>();
  if (!img->setImage(image, bounds, proj)) {
    return nullptr;
  }

  // This takes ownership of the object
  img->setParentItem(this);
  return img.release();
}

ImageObject*
RasterLayer::addImage(const QImage& image, const QPolygonF& bounds)
{
  auto proj = projection();
  qCDebug(dslmapRasterLayer) << "Layer projection is:" << proj->definition();
  if (!proj) {
    return nullptr;
  }

  auto img = std::make_unique<ImageObject>();
  if (!img->setImage(image, bounds, proj)) {
    return nullptr;
  }

  // This takes ownership of the object
  img->setParentItem(this);
  return img.release();
}

void
RasterLayer::showPropertiesDialog()
{
  auto widget = new GeneralLayerPropertiesWidget();
  widget->setLayer(this);
  widget->show();
}

void
RasterLayer::setProjection(std::shared_ptr<const Proj> proj)
{
  for (auto i : childItems()) {
    auto img = qgraphicsitem_cast<ImageObject*>(i);
    if (!img) {
      continue;
    }
    img->setProjection(proj);
  }

  MapLayer::setProjection(proj);
}
}
