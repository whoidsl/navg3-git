/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 9/20/17.
//

#include "Util.h"

#include <QtMath>
#include <Util.h>
#include <iostream>
#include <QDebug>

namespace dslmap {

#if 0
QPainterPath
pathFromShape(SymbolShape shape, qreal size)
{

  auto path = QPainterPath{};

  if (size <= 0) {
    return path;
  }

  switch (shape) {
    case SymbolShape::None:
    case SymbolShape::Custom:
      break;
    case SymbolShape::Circle: {
      path.addEllipse(0, 0, size, size);
      break;
    }
    case SymbolShape::Square:
      path.addRect(0, 0, size, size);
      break;
    case SymbolShape::Diamond:
      path.moveTo(0, size);
      path.lineTo(size, 0);
      path.lineTo(0, -size);
      path.lineTo(-size, 0);
      path.closeSubpath();
      break;
    case SymbolShape::Plus:
      path.moveTo(0, size);
      path.lineTo(0, -size);
      path.moveTo(-size, 0);
      path.lineTo(size, 0);
      break;
    case SymbolShape::Cross:
      path.moveTo(-size, size);
      path.lineTo(size, -size);
      path.moveTo(-size, -size);
      path.lineTo(size, size);
      break;
  }

  // Translate teh path so that it's center is at 0,0, instead of the
  // top-left corner.
  path.translate(-path.boundingRect().center());
  return path;
}
#endif

qreal
niceNum(qreal x, bool round)
{
  const auto exp = qFloor(log10(x));
  const auto f = x / qPow(10, exp);
  auto nf = 1.0;

  if (round) {
    if (f < 1.5) {
      nf = 1.0;
    } else if (f < 3) {
      nf = 2;
    } else if (f < 7) {
      nf = 5;
    } else {
      nf = 10;
    }
  } else {
    if (f < 1) {
      nf = 1;
    } else if (f <= 2) {
      nf = 2;
    } else if (nf <= 5) {
      nf = 5;
    } else {
      nf = 10;
    }
  }

  return nf * qPow(10, exp);
}

QPair<QVector<qreal>, int>
looseLabel(qreal min, qreal max, int nticks)
{

  if (nticks < 1) {
    return {};
  }

  const auto range = niceNum(max - min, false);
  const auto d = niceNum(range / (nticks - 1), true);
  const auto graph_min = qFloor(min / d) * d;
  const auto graph_max = qCeil(max / d) * d;
  const auto num_digits = static_cast<int>(qMax(-qFloor(log10(d)), 0));

  auto x = graph_min;
  auto ticks = QVector<qreal>{};
  ticks.reserve(nticks);

  while (x < graph_max + 0.5 * d) {
    ticks << x;
    x += d;
  }

  return { ticks, num_digits };
}
QPair<QPair<QVector<qreal>, int>, QPair<QVector<qreal>, int>>
graticuleTickMarks(const QRectF& rect, int nticks)
{

  // Get the x ticks
  auto x_graticule = looseLabel(rect.left(), rect.right(), nticks);

  // And the y ticks.  NOTE the inversion of 'top' and 'bottom' here, that's
  // because our inverted y axis (y+ down)
  auto y_graticule = looseLabel(rect.top(), rect.bottom(), nticks);

  const auto n_x = x_graticule.first.size();
  const auto n_y = y_graticule.first.size();

  // Should have at least 2 lines for each axis.. but if we don't, bail early
  if (n_x < 2 || n_y < 2) {
    return { x_graticule, y_graticule };
  }

  // We want square grid lines, so make sure the steps are the same.
  const auto x_d = x_graticule.first.at(1) - x_graticule.first.at(0);
  const auto y_d = y_graticule.first.at(1) - y_graticule.first.at(0);

  if (x_d == y_d) {
    return { x_graticule, y_graticule };
  }

  // make new y tick marks if the y interval is greater than x
  if (x_d < y_d) {
    auto y = qreal{ y_graticule.first.first() };
    const auto last = qreal{ y_graticule.first.last() };

    y_graticule.first.clear();
    while (y <= last) {
      y_graticule.first << y;
      y += x_d;
    }

    y_graticule.second = x_graticule.second;
  }
  // make new x tick marks if the x interval is greater than y
  else if (y_d < x_d) {
    auto x = qreal{ x_graticule.first.first() };
    const auto last = qreal{ x_graticule.first.last() };

    x_graticule.first.clear();
    while (x <= last) {
      x_graticule.first << x;
      x += y_d;
    }

    x_graticule.second = y_graticule.second;
  }

  return { x_graticule, y_graticule };
}

QRectF
extendRectByNPercent(QRectF start_rect, qreal percent)
{
  auto w = start_rect.width();
  auto h = start_rect.height();
  auto c = start_rect.center();
  auto r = start_rect |
           QRectF(c - QPointF((1 + percent) * w / 2, (1 + percent) * h / 2),
                  c + QPointF((1 + percent) * w / 2, (1 + percent) * h / 2));
  return r;
}

QString
formatLonLat(qreal longitude, qreal latitude, LatLonFormat format,
             int precision)
{
  return QString{ "%1 %2" }
    .arg(formatLatitude(latitude, format, precision))
    .arg(formatLongitude(longitude, format, precision));
}

QString
formatLatitude(qreal latitude, LatLonFormat format, int precision)
{
  if (format == LatLonFormat::DD_NE) {
    return QString("%1\u00b0 N").arg(latitude, 2, 'f', precision);
  }

  if (format == LatLonFormat::DD) {
    return QString("%1\u00b0 %2")
      .arg(std::abs(latitude), 2, 'f', precision)
      .arg(latitude >= 0 ? "N" : "S");
  }

  const auto degrees = static_cast<int>(latitude);
  const auto minutes = std::abs(latitude - degrees);
  const auto min_val = minutes * 60;
  qreal deg_val = degrees;

  if (format == LatLonFormat::DM_NE) {
    return QString("%1\u00b0 %2' N")
      .arg(latitude, 2, 'f', 0)
      .arg(minutes, 2, 'f', precision);
  }

  if (format == LatLonFormat::DM) {
    return QString("%1\u00b0 %2' %3")
      .arg(deg_val, 2, 'f', 0)
      .arg(min_val, 2, 'f', precision)
      .arg(latitude >= 0 ? "N" : "S");
  }
  return {};
}
QString
formatLongitude(qreal longitude, LatLonFormat format, int precision)
{
  if (format == LatLonFormat::DD_NE) {
    return QString("%1\u00b0 E").arg(longitude, 2, 'f', precision);
  }

  if (format == LatLonFormat::DD) {
    return QString("%1\u00b0 %2")
      .arg(std::abs(longitude), 2, 'f', precision)
      .arg(longitude >= 0 ? "E" : "W");
  }

  const auto degrees = static_cast<int>(longitude);
  const auto minutes = std::abs(longitude - degrees);
  const auto min_val = minutes * 60;
  qreal deg_val = degrees;

  if (format == LatLonFormat::DM_NE) {
    return QString("%1\u00b0 %2' E")
      .arg(longitude, 2, 'f', 0)
      .arg(minutes, 2, 'f', precision);
  }

  if (format == LatLonFormat::DM) {
    return QString("%1\u00b0 %2' %3")
      .arg(deg_val, 2, 'f', 0)
      .arg(min_val, 2, 'f', precision)
      .arg(longitude >= 0 ? "E" : "W");
  }
  return {};
}


double LatorLonDMToDD(int degrees, double minutes, int precision){
  int swap_signs = 1;
  if (degrees < 0) swap_signs = -1;
  double new_min = degrees + (swap_signs * (minutes /60)); 
  QString precise = QString::number(new_min,'f',precision);
  return precise.toDouble();
}

//haversine formula to calculate distance between two points
double
calculateDistance(const QPointF source_fix,
                                   const QPointF dest_fix)
{
  const double earthRadius = 6371000.0; // Earth's radius in meters

    double lat1 = source_fix.y() * M_PI / 180.0;
    double lon1 = source_fix.x() * M_PI / 180.0;
    double lat2 = dest_fix.y() * M_PI / 180.0;
    double lon2 = dest_fix.x() * M_PI / 180.0;

    double dLat = lat2 - lat1;
    double dLon = lon2 - lon1;

    double a = std::sin(dLat / 2) * std::sin(dLat / 2) +
               std::cos(lat1) * std::cos(lat2) *
               std::sin(dLon / 2) * std::sin(dLon / 2);

    double c = 2 * std::atan2(std::sqrt(a), std::sqrt(1 - a));

    double distance = earthRadius * c;

    return distance;
}

double calculateBearing(const QPointF source_fix, const QPointF dest_fix)
{
    double teta1 = source_fix.y() * DTOR;
    double teta2 = dest_fix.y() * DTOR;
    double delta2 = (dest_fix.x() - source_fix.x()) * DTOR;

    double y = sin(delta2) * cos(teta2);
    double x = cos(teta1) * sin(teta2) - sin(teta1) * cos(teta2) * cos(delta2);
    double brng = atan2(y, x);

    brng = brng * RTOD; // radians to degrees
    brng = fmod((brng + 360.0), 360.0);

    return brng;
}
}
