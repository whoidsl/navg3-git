# Using libdslmap

This document outlines at a very high level the basic assumptions and concepts used in the dslmap library.  More
detailed documentation should be present in the individual class docstrings.  Doxygen can be used to generate a nice
html version of the API documentation using the `dslmap-doxygen` compile target.

## Assumptions

`libdslmap`` is a QGraphicsView-based mapping library tailored for situational awareness during DSL vehicle operations.
As so, it makes a few compromises and assumptions:

- The underlying map scene will _always_ be a non-geodetic projection (e.g. not lat/lon)
- If unspecified, the map scene projection will default to a mercator projection with the centered on the first items.

## Initialization

`dslmap::init()` should be called **once** at the start of your program.  This call will handle initializing some of 
the libraries that `libdslmap` uses (such as gdal).  Calls to add data from files may fail if you do not call the
init function.

## dslmap::MapView

This is a wrapper around QGraphicsView that adds a few map-centric features, such as a graticule overlay and some basic
mouse interaction.  The MapView is attached to a MapScene, discussed below.  Multiple MapViews can share the same 
MapScene, which allows for multiple independent views into the same dataset without duplicating resources.

## dslmap::MapScene

This is a wrapper around QGraphicsScene that adds a few map-centric features, such as the scene's map projection and
an internal map layer registry.  While graphic items can be added directly to the scene with the standard 
QGraphicsScene::addItem method, doing so will ignore any projection changes.  The proper way to add map items to the 
scene is with the dslmap::MapScene::addLayer method.

## dslmap::MapScene objects

MapScene objects are the most basic "primitive" available for displaying on the map.  **They are not intended to be added
to maps on their own (that's what layers are for)**, but there is nothing stopping you from doing so.  Currently dslamp
provides the following:

- Point Items
  - `dslmap::MarkerItem`:  A single point-like target
  - `dslmap::MarkerSymbol`:  A way of displaying a MarkerItem on the map with a path
  - `dslmap::ShapeMarkerSymbol`:  A wrapper around MarkerSymbol that allows for selection from a set of simple paths
  - `dslmap::WktMarkerSymbol`:  A symbol with real physical size defined by a Well Known Text (WKT) string.
  
- Raster (2D) Items  
  - `dslmap::BathyObject`: Gridded bathymetry data
  - `dslmap::ImageObject`: Non-bathymetry data that has some geographic bounds (e.g. sidescan, geotiffs, etc.)
  
- Vector (2D) Items
  - `dslmap::ContourLine`:  A line with an associated depth.

### dslmap::MarkerItem and dslmap::MarkerSymbol

The point-like primitives deserve special mention.  Note that dslmap::MarkerItem has a position and the ability to
add metadata, but does **not** have any information on how to *display* the point.  Drawing a dslmap::MarkerItem on
the map is the job of the dslmap::MarkerSymbol and related classes, which know how to draw a MarkerItem but have no
direct knowledge of where that item is.  This separation is a deliberate design decision.  Much like a MapScene can
have multiple MapViews but each MapView only has one MapScene so can MarkerSymbols draw multiple MarkerItems but a
MarkerItem can only be assigned a single MarkerSymbol.

This split helps keep the required resources to a minimum as a single path can be used to draw many thousands of points.
It is expected that point-like targets will naturally be grouped into categories, and this design allows those categories
to take natural relationships in the code.  It also allows changing how point targets look on the map as easy as changing
a single MarkerSymbol instead of applying the same changes to all affected points individually.

The relationship is codified in MarkItem::paint(), which in turn calls the MarkerSymbol::renderItem() method to handle
the actual drawing.
  
## dslmap::MapLayer objects

dslmap::MapLayer is an abstract base class that defines the basic interface for map layers that can be added to a 
dslmap::MapScene.  Some details are taken care of for you, such as generating the UUID and getters/setters for the
layer name.  To create a new layer class you will need to override the pure-virtual methods in MapLayer with your
own implementations.

`libdslmap` has a few layers out of the box:

- Layers
  - `dslmap::BathyLayer`:  A layer for displaying gridded bathymetry data.
  - `dslmap::BeaconLayer`:  A layer for tracking a an object, with a current position and position trail.
  - `dslmap::ContourLayer`:  A layer for displaying contour lines from gridded bathymetry data.
  - `dslmap::SymbolLayer`:  A layer for showing a group of point targets using a common symbol.
  - `dslmap::RasterLayer`:  A layer for displaying 2D raster data that IS NOT bathymetry (e.g. sidescan, geotiffs)
