/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "MapScene.h"
#include <catch.hpp>

#include <QSignalSpy>

using namespace dslmap;

SCENARIO("Default MapScene projection")
{

  GIVEN("A new MapScene object")
  {
    auto scene = std::make_unique<MapScene>();

    THEN("the default projection is invalid")
    {
      const auto expected = QString{ "" };
      const auto actual = scene->projectionString();

      REQUIRE(actual == expected);
    }
  }
}

SCENARIO("Changing the map projection")
{

  GIVEN("A MapScene object")
  {
    auto scene = std::make_unique<MapScene>();
    const auto orig_projection = scene->projectionString();

    WHEN("the projection is changed using a valid definition")
    {

      auto spy = std::make_unique<QSignalSpy>(
        scene.get(), SIGNAL(projectionChanged(const QString&)));

      const auto new_projection =
        QString{ "+proj=merc +ellps=clrk66 +lat_ts=33" };
      const auto ok = scene->setProjection(new_projection);

      THEN("a signal is emitted")
      {
        REQUIRE(spy->count() == 1);
        REQUIRE(spy->takeFirst().at(0).toString() == new_projection);
      }

      AND_THEN("the new projection is set")
      {
        REQUIRE(ok);
        REQUIRE(new_projection == scene->projectionString());
      }
    }

    AND_WHEN("the projection is invalid")
    {

      auto spy = std::make_unique<QSignalSpy>(
        scene.get(), SIGNAL(projectionChanged(const QString&)));

      const auto new_projection = QString{ "+proj=somebull" };

      const auto ok = scene->setProjection(new_projection);

      THEN("no signal is emitted") { REQUIRE(spy->count() == 0); }

      AND_THEN("the original projection is retained")
      {
        REQUIRE_FALSE(ok);
        REQUIRE(scene->projectionString() == orig_projection);
      }
    }
  }
}

// TODO: Tests for actual reprojection of map items.