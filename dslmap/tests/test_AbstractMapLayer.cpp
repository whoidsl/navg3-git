/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "catch.hpp"

#include "DslMap.h"
#include "IMapLayer.h"
#include "MapScene.h"
#include "Proj.h"

#include "test_AbstractMapLayer.h"

#include <QSignalSpy>

using namespace dslmap;
SCENARIO("default IMapLayer behavior")
{
  GIVEN("A newly created layer")
  {
    auto layer = std::make_unique<TestMapLayer>();

    THEN("The layer type should be 'Invalid'")
    {
      REQUIRE(layer->layerType() == TestMapLayer::Invalid);
    }

    AND_THEN("there should be no items added to the group.")
    {
      REQUIRE(layer->childItems().isEmpty());
    }

    AND_THEN("the layer should have no name")
    {
      REQUIRE(layer->layerName().isNull());
    }

    AND_THEN("the layer should be visible") { REQUIRE(layer->isVisible()); }

    AND_THEN("the layer should have a valid UUID")
    {
      REQUIRE(!layer->layerId().isNull());
    }

    AND_THEN("the default layer projection is '+proj=latlong +ellps=WGS84'")
    {
      REQUIRE(layer->projection()->definition() == PROJ_WGS84_DEFINITION);
    }
  }
}

SCENARIO("layer signals emitted")
{
  GIVEN("A newly created layer")
  {
    auto scene = std::make_unique<MapScene>();
    auto layer = new TestMapLayer;
    scene->addItem(layer);

    WHEN("the name is changed")
    {
      const auto new_name = QString("new layer name");
      // QSignalSpy spy(layer.get(), SIGNAL(nameChanged(QString)));
      auto spy =
        std::make_unique<QSignalSpy>(layer, SIGNAL(nameChanged(QString)));

      layer->setLayerName(new_name);

      THEN("the layer takes on the new name")
      {
        REQUIRE(layer->layerName() == new_name);
      }

      AND_THEN("the 'nameChanged' signal is emitted with the new name")
      {
        REQUIRE(spy->count() == 1);
        REQUIRE(spy->takeFirst().at(0).toString() == new_name);
      }
    }

    AND_WHEN("the new name matches the old name")
    {
      const auto new_name = layer->layerName();

      auto spy =
        std::make_unique<QSignalSpy>(layer, SIGNAL(nameChanged(QString)));

      THEN("changing the name does not emit a signal")
      {
        REQUIRE(spy->count() == 0);
      }
    }

    WHEN("the layer visibility is changed")
    {
      const auto new_visibility = !layer->isVisible();
      auto spy = std::make_unique<QSignalSpy>(layer, SIGNAL(visibleChanged()));

      layer->setVisible(new_visibility);

      THEN("the layer visibility flag changes")
      {
        REQUIRE(layer->isVisible() == new_visibility);
      }

      AND_THEN(
        "the 'visibilityChanged' signal is emitted with the new visibility")
      {
        REQUIRE(spy->count() == 1);
      }
    }

    AND_WHEN("the new visibility matches the old")
    {
      const auto visibility = layer->isVisible();
      layer->setVisible(visibility);

      auto spy = std::make_unique<QSignalSpy>(layer, SIGNAL(visibleChanged()));

      THEN("changing the visibility does not emit a signal")
      {
        REQUIRE(spy->count() == 0);
      }
    }
  }
}
