/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "BathyObject.h"
#include "GdalColorGradient.h"
#include "MapItem.h"
#include "Proj.h"
#include "catch.hpp"

#include <QDir>
#include <QFile>

using namespace dslmap;

TEST_CASE("BathyObject Defaults")
{

  auto item = std::make_unique<BathyObject>();

  INFO("Layer has the proper MapGraphicsItemType");
  REQUIRE(item->type() == MapItemType::BathyObjectType);

  INFO("Default gradient runs from black to white");
  auto interp = item->gradient();
  auto stops = interp->gradient().stops();
  REQUIRE(stops.size() == 2);
  REQUIRE(stops.first().first == 0);
  REQUIRE(stops.first().second == QColor(Qt::black));

  REQUIRE(stops.last().first == 1);
  REQUIRE(stops.last().second == QColor(Qt::white));

  INFO("No projection by default")
  REQUIRE_FALSE(item->projection());

  INFO("No dataset projection by default");
  REQUIRE_FALSE(item->datasetProjection());

  INFO("No dataset");
  REQUIRE_FALSE(item->dataset());

  REQUIRE(item->boundingRect().isNull());

  INFO("Default band is 1");
  REQUIRE(item->band() == 1);
}

TEST_CASE("BathyObject")
{
  auto item = std::make_unique<BathyObject>();

  const auto cape_cod = QDir::temp().filePath("cape_cod.grd");

  QFile::copy(":/cape_cod.grd", cape_cod);
  item->setRasterFile(cape_cod, 1);

  REQUIRE(item->dataset() != nullptr);

  // From gdalinfo program
  const auto e_bb = QRectF{ QPointF{ -70.9820833, 41.9104167 },
                            QPointF{ -69.2479167, 40.7112500 } };
  const auto bb = item->boundingRect();

  CHECK(bb.topLeft().x() == Approx(e_bb.topLeft().x()));
  CHECK(bb.topLeft().y() == Approx(e_bb.topLeft().y()));

  CHECK(bb.bottomRight().x() == Approx(e_bb.bottomRight().x()));
  CHECK(bb.bottomRight().y() == Approx(e_bb.bottomRight().y()));
}

TEST_CASE("BathyObject Reprojection")
{
  auto item = std::make_unique<BathyObject>();

  const auto cape_cod = QDir::temp().filePath("cape_cod.grd");

  QFile::copy(":/cape_cod.grd", cape_cod);
  item->setRasterFile(cape_cod, 1);

  REQUIRE(item->dataset() != nullptr);

  const auto center_lonlat = item->boundingRect().center();
  auto utm_proj = Proj::utmFromLonLat(center_lonlat.x(), center_lonlat.y());

  item->setProjection(utm_proj);

  REQUIRE(utm_proj->isSame(*item->projection()));
}
