/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "Proj.h"
#include "catch.hpp"

#include <gdal/ogr_spatialref.h>

#include <QLineF>
#include <QString>

using namespace dslmap;

TEST_CASE("Projection equivalence")
{

  auto a = Proj::fromDefinition("+proj=latlong +datum=WGS84");
  auto b = Proj::fromDefinition("+proj=latlong +datum=WGS84");

  REQUIRE(a);
  REQUIRE(b);
  REQUIRE(a->isSame(*b));

  // auto c = Proj::fromDefinition("+datum=WGS84 +proj=latlong");
  // REQUIRE(c);
  // REQUIRE(a->isSame(*c));
}

SCENARIO("alvin XY transforms are used")
{

  GIVEN("an alvin XY origin")
  {
    const auto origin = QPointF{ -71, 41 };

    WHEN("the alvin XY transform is created")
    {
      auto alvin_proj = Proj::alvinXYFromOrigin(origin.x(), origin.y());
      REQUIRE(alvin_proj);

      const auto def = alvin_proj->definition();
      const auto fields = def.split(' ');
      INFO("AlvinXY def: " + def.toStdString());

      THEN("the underlying projection is transverse mercator")
      {
        auto param = alvin_proj->parameter("proj");
        REQUIRE(param.isValid());
        REQUIRE(param.toString() == "tmerc");
      }

      AND_THEN("the central latitude/longitude are at the origin")
      {
        auto ok = false;
        auto param = alvin_proj->parameter("lon_0");
        REQUIRE(param.isValid());
        auto value = param.toDouble(&ok);
        REQUIRE(ok);
        CHECK(value == Approx(origin.x()));

        param = alvin_proj->parameter("lat_0");
        REQUIRE(param.isValid());
        value = param.toDouble(&ok);
        REQUIRE(ok);
        CHECK(value == Approx(origin.y()));
      }

      AND_THEN("the lon/lat origin will map to xy 0,0")
      {
        auto pos = QVector<QPointF>{ origin };
        const auto n = alvin_proj->transformFromLonLat(pos);
        REQUIRE(n == 1);

        CHECK(pos[0].x() == Approx(0).margin(0.0001));
        CHECK(pos[0].y() == Approx(0).margin(0.0001));
      }

      AND_THEN("the xy point 0,0 maps to the origin lon/lat")
      {
        auto latlon_proj = alvin_proj->latLongFromProj();
        REQUIRE(latlon_proj);

        auto pos = QVector<QPointF>{ { 0, 0 } };
        const auto n = alvin_proj->transformToLonLat(pos);
        REQUIRE(n == 1);

        CHECK(pos[0].x() == Approx(origin.x()));
        CHECK(pos[0].y() == Approx(origin.y()));
      }
    }
  }
  GIVEN("An AlvinXY origin with a long decimal")
  {
    const auto origin = QPointF{ -71.1111111111, 41.2222222222 };
    WHEN("The AlvinXY projection definition is created")
    {
      auto alvin_proj = Proj::alvinXYFromOrigin(origin.x(), origin.y());
      REQUIRE(alvin_proj);
      const auto def = alvin_proj->definition();
      INFO("AlvinXY def: " + def.toStdString());
      THEN("The projection definition includes 10 digits of accuracy")
      {
        CHECK(def.toStdString() == "+proj=tmerc +datum=WGS84 +units=m "
                                   "+lon_0=-71.1111111111 +lat_0=41.2222222222 "
                                   "+ellps=WGS84 +towgs84=0,0,0");
      }
      THEN("the underlying projection is transverse mercator")
      {
        auto param = alvin_proj->parameter("proj");
        REQUIRE(param.isValid());
        REQUIRE(param.toString() == "tmerc");
      }

      AND_THEN("the central latitude/longitude are at the origin")
      {
        auto ok = false;
        auto param = alvin_proj->parameter("lon_0");
        REQUIRE(param.isValid());
        auto value = param.toDouble(&ok);
        REQUIRE(ok);
        CHECK(value == Approx(origin.x()));

        param = alvin_proj->parameter("lat_0");
        REQUIRE(param.isValid());
        value = param.toDouble(&ok);
        REQUIRE(ok);
        CHECK(value == Approx(origin.y()));
      }

      AND_THEN("the lon/lat origin will map to xy 0,0")
      {
        auto pos = QVector<QPointF>{ origin };
        const auto n = alvin_proj->transformFromLonLat(pos);
        REQUIRE(n == 1);

        CHECK(pos[0].x() == Approx(0).margin(0.0001));
        CHECK(pos[0].y() == Approx(0).margin(0.0001));
      }

      AND_THEN("the xy point 0,0 maps to the origin lon/lat")
      {
        auto latlon_proj = alvin_proj->latLongFromProj();
        REQUIRE(latlon_proj);

        auto pos = QVector<QPointF>{ { 0, 0 } };
        const auto n = alvin_proj->transformToLonLat(pos);
        REQUIRE(n == 1);

        CHECK(pos[0].x() == Approx(origin.x()));
        CHECK(pos[0].y() == Approx(origin.y()));
      }
    }
  }
}

TEST_CASE("WGS84 Distance Calculations (from sentinal)")
{
  // Results from https://geographiclib.sourceforge.io/cgi-bin/GeodSolve

  const auto from = QPointF{ -73.8, 40.6 };
  const auto to = QPointF{ 2.55000000, 49.01666667 };
  auto to_bearing = 0.0;
  auto from_bearing = 0.0;

  const auto expected = 5853226.256;
  const auto expected_from_bearing = 53.47021824;
  const auto expected_to_bearing = 111.59366952;

  const auto result =
    ProjWGS84::geodeticDistance(from, to, &from_bearing, &to_bearing, true);

  CHECK(result == Approx(expected));
  CHECK(from_bearing == Approx(expected_from_bearing));
  CHECK(to_bearing == Approx(expected_to_bearing));
}

TEST_CASE("WGS84 Distance Calculations (from Proj)")
{
  // Results from https://geographiclib.sourceforge.io/cgi-bin/GeodSolve

  const auto from = QPointF{ -73.8, 40.6 };
  const auto to = QPointF{ 2.55000000, 49.01666667 };
  auto to_bearing = 0.0;
  auto from_bearing = 0.0;

  const auto expected = 5853226.256;
  const auto expected_from_bearing = 53.47021824;
  const auto expected_to_bearing = 111.59366952;

  const auto result = ProjWGS84::getInstance().geodeticDistance(
    from, to, &from_bearing, &to_bearing);

  CHECK(result == Approx(expected));
  CHECK(from_bearing == Approx(expected_from_bearing));
  CHECK(to_bearing == Approx(expected_to_bearing));
}

TEST_CASE("Projected Distance Calculations")
{
  auto proj = Proj::utmFromLonLat(-73.8, 40.6);
  REQUIRE(!proj->isLatLon());

  const auto from = QPointF{ 0, 0 };
  const auto to = QPointF{ 3000, 0 };
  auto to_bearing = 0.0;
  auto from_bearing = 0.0;

  const auto expected = Approx(QLineF(from, to).length()).margin(20);
  const auto expected_from_bearing = Approx(90);
  const auto expected_to_bearing = Approx(90);

  const auto result =
    proj->geodeticDistance(from, to, &from_bearing, &to_bearing);

  CHECK(result == expected);
  CHECK(from_bearing == expected_from_bearing);
  CHECK(to_bearing == expected_to_bearing);
}
