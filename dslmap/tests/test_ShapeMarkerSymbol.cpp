/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#include "ShapeMarkerSymbol.h"
#include "catch.hpp"

#include <QMetaEnum>
#include <QPen>
#include <QSettings>

using namespace dslmap;

SCENARIO("ShapeMarkerSymbols can be cloned")
{
  GIVEN("a symbol")
  {
    auto symbol = std::make_unique<ShapeMarkerSymbol>();
    REQUIRE(symbol);

    symbol->setShape(ShapeMarkerSymbol::Shape::Circle, 20);
    symbol->setGradientMax(300);

    WHEN("the symbol is cloned")
    {
      auto clone = symbol->clone();
      REQUIRE(clone);

      THEN("the cloned symbol has the same characteristics")
      {
        REQUIRE(*symbol == *clone);
      }

      AND_THEN("the cloned symbol is independent of the original")
      {
        clone->setOutlineColor(Qt::blue);
        REQUIRE_FALSE(*symbol == *clone);
      }
    }
  }
}

SCENARIO("Testing equality of points")
{
  GIVEN("Two symbols")
  {
    auto symbol1 =
      std::make_unique<ShapeMarkerSymbol>(ShapeMarkerSymbol::Shape::Circle);
    auto symbol2 =
      std::make_unique<ShapeMarkerSymbol>(ShapeMarkerSymbol::Shape::Circle);
    REQUIRE(*symbol1 == *symbol2);

    WHEN("They differ in ... use_gradient")
    {
      symbol1->setUseColorGradient(false);
      symbol2->setUseColorGradient(true);
      REQUIRE(*symbol1 != *symbol2);
    }
    WHEN("They differ in ... color_gradient")
    {
      symbol1->setUseColorGradient(true);
      symbol2->setUseColorGradient(true);
      symbol1->setGradientPreset(GdalColorGradient::GradientPreset::Warm);
      symbol2->setGradientPreset(GdalColorGradient::GradientPreset::HAXBY);
      REQUIRE(*symbol1 != *symbol2);
    }
    WHEN("They differ in ... gradient_attribute")
    {
      symbol1->setUseColorGradient(true);
      symbol2->setUseColorGradient(true);
      symbol1->setGradientAttribute(QString("baz"));
      symbol2->setGradientAttribute(QString("foo"));
      REQUIRE(*symbol1 != *symbol2);
    }
    WHEN("They differ in ... gradient_min")
    {
      symbol1->setUseColorGradient(true);
      symbol2->setUseColorGradient(true);
      symbol1->setGradientMin(-200);
      symbol2->setGradientMin(-100);
      REQUIRE(*symbol1 != *symbol2);
    }
    WHEN("They differ in ... gradient_max")
    {
      symbol1->setUseColorGradient(true);
      symbol2->setUseColorGradient(true);
      symbol1->setGradientMax(100);
      symbol2->setGradientMax(200);
      REQUIRE(*symbol1 != *symbol2);
    }
  }
}

SCENARIO("Loading a ShapeMarkerSymbol from QSettings")
{
  auto alignmentEnum = QMetaEnum::fromType<Qt::Alignment>();
  GIVEN("settings in a QSettings object")
  {
    QSettings settings(QSettings::InvalidFormat, QSettings::UserScope, "Test");
    settings.clear();

    settings.setValue("shape", "diamond");
    settings.setValue("size", 30);
    settings.setValue("outline/color", "yellow");
    settings.setValue("outline/width", 3);
    settings.setValue("fill/color", "orange");
    settings.setValue("label/visible", true);
    settings.setValue("label/color", "red");
    settings.setValue("label/alignment", QString{ alignmentEnum.valueToKeys(
                                           Qt::AlignLeft | Qt::AlignTop) });
    settings.setValue("use_gradient", true);
    settings.setValue(
      "gradient/preset",
      static_cast<int>(GdalColorGradient::GradientPreset::HAXBY));
    settings.setValue("gradient/attribute", QString("testing_settings"));
    settings.setValue("gradient/min", -100);
    settings.setValue("gradient/max", 100);

    WHEN("a symbol is created with the settings")
    {
      const auto symbol = ShapeMarkerSymbol::fromSettings(settings, "");

      THEN("All symbol properties are set correctly")
      {
        REQUIRE(symbol);
        CHECK(symbol->shape() == ShapeMarkerSymbol::shapeFromString(
                                   settings.value("shape").toString()));
        auto result = symbol->outlineColor();
        auto expected =
          QColor(settings.value("outline/color").toString()).name();
        CHECK(result == expected);
        CHECK(symbol->size() == settings.value("size").toFloat());

        auto color = symbol->fillColor();
        result = color.name();
        expected = QColor(settings.value("fill/color").toString()).name();
        CHECK(result == expected);

        color = symbol->labelColor();
        result = color.name();
        expected = QColor(settings.value("label/color").toString()).name();
        CHECK(result == expected);

        CHECK(symbol->isLabelVisible() ==
              settings.value("label/visible").toBool());

        auto ok = false;
        const auto alignmentString =
          settings.value("label/alignment").toString().toLocal8Bit();
        auto alignment = alignmentEnum.keysToValue(alignmentString.data(), &ok);
        CHECK(ok);
        CHECK(symbol->labelAlignment() == alignment);

        CHECK(symbol->useColorGradient() ==
              settings.value("use_gradient").toBool());
        CHECK(static_cast<int>(symbol->gradientPreset()) ==
              settings.value("gradient/preset").toInt());
        CHECK(symbol->gradientAttribute() ==
              settings.value("gradient/attribute").toString());
        CHECK(symbol->gradientMin() ==
              settings.value("gradient/min").toFloat());
        CHECK(symbol->gradientMax() ==
              settings.value("gradient/max").toFloat());
      }
    }
  }
}

SCENARIO("Saving a ShapeMarkerSymbol to QSettings")
{
  auto alignmentEnum = QMetaEnum::fromType<Qt::Alignment>();
  GIVEN("A ShapeMarkerSymbol")
  {
    auto symbol =
      std::make_unique<ShapeMarkerSymbol>(ShapeMarkerSymbol::Shape::Circle, 20);
    symbol->setOutlineColor(Qt::magenta);
    symbol->setOutlineWidth(3);
    symbol->setFillColor(Qt::blue);
    symbol->setLabelColor(Qt::white);
    symbol->setLabelVisible(true);
    symbol->setLabelAlignment(Qt::AlignRight | Qt::AlignVCenter);
    symbol->setUseColorGradient(true);
    symbol->setGradientPreset(GdalColorGradient::GradientPreset::HAXBY);
    symbol->setGradientAttribute("testing_settings");
    symbol->setGradientMin(-100);
    symbol->setGradientMax(100);

    WHEN("The symbol is saved to a QSettings object")
    {

      QSettings settings(QSettings::InvalidFormat, QSettings::UserScope,
                         "Test");
      settings.clear();
      symbol->saveSettings(settings, "");

      THEN("All symbol properties are saved with the correct parameter names")
      {
        CHECK(settings.value("shape").toString() == toString(symbol->shape()));
        auto result = QColor(settings.value("outline/color").toString()).name();
        auto expected = symbol->outlineColor().name();
        CHECK(result == expected);
        CHECK(settings.value("size").toFloat() == symbol->size());

        expected = symbol->fillColor().name();
        result = QColor(settings.value("fill/color").toString()).name();
        CHECK(result == expected);

        expected = symbol->labelColor().name();
        result = QColor(settings.value("label/color").toString()).name();
        CHECK(result == expected);

        CHECK(symbol->isLabelVisible() ==
              settings.value("label/visible").toBool());
        auto ok = false;
        const auto alignmentString =
          settings.value("label/alignment").toString().toLocal8Bit();
        auto alignment = alignmentEnum.keysToValue(alignmentString.data(), &ok);
        CHECK(ok);
        CHECK(symbol->labelAlignment() == alignment);

        CHECK(symbol->useColorGradient() ==
              settings.value("use_gradient").toInt());
        CHECK(static_cast<int>(symbol->gradientPreset()) ==
              settings.value("gradient/preset").toInt());
        CHECK(symbol->gradientAttribute() ==
              settings.value("gradient/attribute").toString());
        CHECK(symbol->gradientMin() ==
              settings.value("gradient/min").toFloat());
        CHECK(symbol->gradientMax() ==
              settings.value("gradient/max").toFloat());
      }
    }
  }
}
