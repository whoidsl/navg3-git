/**
* Copyright 2023 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by timj on 6/5/23.
//

#include "Util.h"
#include "catch.hpp"

#include <QString>

using namespace dslmap;

//DM TO DD
SCENARIO("Given Degree and Minutes return correct Decimal Degrees")
{

  GIVEN("A pair of valid Degrees and Minutes at 4 decimal places")
  {
    const int degree = 32;
    const double minutes = 45.2357;

    WHEN("the conversion to DD is calculated")
    {
      double response = LatorLonDMToDD(degree,minutes,6);

      THEN("the result corresponds to precomputed correct answer")
      {
        double correct = 32.753928;
        CHECK(response == correct);

      }
    }
  }

  GIVEN("A pair of valid Degrees and Minutes at 2 decimal places with simple conversion")
  {
    const int degree = 29;
    const double minutes = 15;

    WHEN("the conversion to DD is calculated")
    {
      
      //RETURN UP TO 7 decimal precision.
      double response = LatorLonDMToDD(degree,minutes,7);

      THEN("the result corresponds to precomputed correct answer")
      {
        double correct = 29.2500000;
        CHECK(response == correct);
 
      }
    }
  }

  GIVEN("A negative pair of valid Degrees and Minutes")
  {
    const int degree = -32;
    const double minutes = 45.2357;

    WHEN("the conversion to DD is calculated")
    {
      double response = LatorLonDMToDD(degree,minutes,4);

      THEN("the result corresponds to precomputed correct answer")
      {
        double correct = -32.7539;
        CHECK(response == correct);

      }
    }
  }


  GIVEN("EXTRA CONVERSION SCENARIOS")
  {
    const int degree = 76;
    const double minutes = 12.233457;

    WHEN("the conversion to DD is calculated")
    {
      double response = LatorLonDMToDD(degree,minutes,7);

      THEN("the result corresponds to precomputed correct answer")
      {
        double correct = 76.2038910;
        CHECK(response == correct);

      }
    }
  }

    GIVEN("EXTRA CONVERSION SCENARIOS 2")
  {
    const int degree = -120;
    const double minutes = 23.0835822;

    WHEN("the conversion to DD is calculated")
    {
      double response = LatorLonDMToDD(degree,minutes,8);

      THEN("the result corresponds to precomputed correct answer")
      {
        double correct = -120.38472637;
        CHECK(response == correct);

      }
    }
  }

}

SCENARIO("Filtering out incoming position fixes based on distance")
{
    GIVEN("Initial and new positions")
    {

        WHEN("An incoming VFR position fix is within 1000km of the last fix")
        {
            // Set up the initial position fix
            QPointF initial_pos(-72.0, 42.0);

            // Set up a new position fix within 1000km
            QPointF new_pos(-71.0, 41.0);

            // Calculate the distance between the initial and new position fixes
            double distance = calculateDistance(initial_pos, new_pos);

            THEN("The distance should be less than 140km")
            {
                CHECK(distance < 140000);
            }

            THEN("The new position fix should be accepted")
            {
                bool should_accept = (distance <= 500000);
                CHECK(should_accept);
            }
        }

        WHEN("An incoming VFR position fix is over 1000km away from the last fix and there are no other named fixes")
        {
            // Set up the initial position fix
            QPointF initial_pos(-72.0, 42.0);

            // Set up a new position fix over 1000km away
            QPointF new_pos(-62.0, 32.0);

            // Calculate the distance between the initial and new position fixes
            double distance = calculateDistance(initial_pos, new_pos);

            THEN("The distance should be greater than 1000km")
            {
                CHECK(distance > 1000000);
            }

            THEN("The new position fix should be accepted since there are no other named fixes")
            {
                bool should_accept = true;
                CHECK(should_accept);
            }
        }

        WHEN("An incoming VFR position fix is over 1000km away from the last fix and there are other named fixes")
        {
            // Set up the initial position fix
            QPointF initial_pos(-72.0, 42.0);

            // Set up another named fix close to the initial fix
            QPointF other_pos(-72.1, 41.9);

            // Set up a new position fix over 1000km away
            QPointF new_pos(-62.0, 32.0);

            // Calculate the distance between the initial and new position fixes
            double distance = calculateDistance(initial_pos, new_pos);

            THEN("The distance should be greater than 1000km")
            {
                CHECK(distance > 1000000);
            }

            // Calculate the distance between the new position fix and the other named fix
            double other_distance = calculateDistance(other_pos, new_pos);

            THEN("The distance to the other named fix should be greater than 1000km")
            {
                CHECK(other_distance > 1000000);
            }

            THEN("The new position fix should be rejected since it is far from all other named fixes")
            {
                bool should_accept = (distance <= 500000 || other_distance <= 500000);
                CHECK_FALSE(should_accept);
            }
        }
        
        WHEN("An incoming VFR position fix is over 1000km away from the last fix and other named fixes have empty trails")
        {
            // Set up the initial position fix
            QPointF initial_pos(-72.0, 42.0);

            // Set up another named fix with an empty trail
            QPointF other_pos(-72.1, 41.9);

            // Set up a new position fix over 1000km away
            QPointF new_pos(-62.0, 32.0);

            // Calculate the distance between the initial and new position fixes
            double distance = calculateDistance(initial_pos, new_pos);

            THEN("The distance should be greater than 1000km")
            {
                CHECK(distance > 1000000);
            }

            THEN("The new position fix should be accepted since other named fixes have empty trails")
            {
                bool should_accept = true;
                CHECK(should_accept);
            }
        }
        
    }
}