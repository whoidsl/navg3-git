/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "Proj.h"
#include "catch.hpp"

#include <proj_api.h>

using namespace dslmap;
TEST_CASE("Proj4 Units can be converted from QStrings")
{

  const auto proj_pairs = QList<QPair<Unit, QString>>{
    { Unit::km, "km" },         { Unit::m, "m" },
    { Unit::dm, "dm" },         { Unit::cm, "cm" },
    { Unit::mm, "mm" },         { Unit::kmi, "kmi" },
    { Unit::in, "in" },         { Unit::ft, "ft" },
    { Unit::yd, "yd" },         { Unit::mi, "mi" },
    { Unit::fath, "fath" },     { Unit::ch, "ch" },
    { Unit::link, "link" },     { Unit::us_in, "us_in" },
    { Unit::us_ft, "us_ft" },   { Unit::us_yd, "us_yd" },
    { Unit::us_ch, "us_ch" },   { Unit::us_mi, "us_mi" },
    { Unit::ind_yd, "ind_yd" }, { Unit::ind_ft, "ind_ft" },
    { Unit::ind_ch, "ind_ch" },
  };

  SECTION("valid unit strings are converted")
  {
    foreach (auto pair, proj_pairs) {
      INFO("Testing " << pair.second.toStdString());
      auto result = stringToUnit(pair.second);
      REQUIRE(pair.first == result);
    }
  }

  SECTION("invalid unit strings fail to convert and return invalid")
  {
    auto result = stringToUnit("bad_unit");
    REQUIRE(result == Unit::invalid);
  }
}

TEST_CASE("WGS84 singleton can convert coordinates")
{

  auto wgs84_proj = Proj::fromDefinition("+proj=latlong +datum=WGS84 +no_defs");
  auto utm30_proj = Proj::fromDefinition(
    "+proj=utm +zone=30 +datum=WGS84 +ellips=WGS84 +units=m");

  REQUIRE(wgs84_proj);
  REQUIRE(utm30_proj);

  // Generate test coordinates
  const auto n_points = 1000;
  auto lon = QVector<double>{};
  auto lat = QVector<double>{};
  lon.reserve(n_points);
  lat.reserve(n_points);

  for (auto i = 0; i < n_points; i++) {
    lon << (qrand() / RAND_MAX * 4 - 4) * DEG_TO_RAD;
    lat << (qrand() / RAND_MAX * 30 + 10) * DEG_TO_RAD;
  }

  const auto orig_lon = lon;
  const auto orig_lat = lat;

  auto x = lon;
  auto y = lat;

  const auto n = wgs84_proj->transformTo(*utm30_proj, x, y);

  REQUIRE(n == n_points);

  SECTION("WGS84 singleton can convert FROM lat/lon")
  {
    const auto nn = ProjWGS84::getInstance().transformTo(*utm30_proj, lon, lat);

    REQUIRE(nn == n_points);

    for (auto i = 0; i < n_points; i++) {
      INFO("Testing lon/lat:" << orig_lon.at(i) << orig_lat.at(i));
      REQUIRE(lon.at(i) == Approx(x.at(i)));
      REQUIRE(lat.at(i) == Approx(y.at(i)));
    }
  }

  SECTION("WGS84 singleton can convert TO lat/lon")
  {

    auto& wgs_singleton = ProjWGS84::getInstance();
    const auto n = utm30_proj->transformTo(wgs_singleton, x, y);

    REQUIRE(n == n_points);
    for (auto i = 0; i < n_points; i++) {
      INFO("Testing lon/lat:" << orig_lon.at(i) << orig_lat.at(i));
      REQUIRE(lon.at(i) == Approx(x.at(i)));
      REQUIRE(lat.at(i) == Approx(y.at(i)));
    }
  }
}
