/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#include "MarkerItem.h"
#include "catch.hpp"

#include "MapScene.h"
#include "MapView.h"
#include "SymbolLayer.h"

using namespace dslmap;

SCENARIO("MarkerItems are drawn with the correct physical size")
{

  GIVEN("A Map view with a SymbolLayer")
  {

    auto view = std::make_unique<MapView>(new MapView);
    auto scene = std::make_unique<MapScene>(new MapScene);
    scene->setProjection(Proj::utmFromLonLat(0, 0));

    auto layer = new SymbolLayer;
    layer->setUnits(Unit::m);

    layer->setShape(ShapeMarkerSymbol::Shape::Square, 10);

    REQUIRE_FALSE(scene->addLayer(layer).isNull());

    WHEN("A item is added to the layer")
    {
      const auto desired_position = QPointF{ 0, 0 };

      auto item = layer->addPoint(desired_position);

      view->zoomExtents();

      THEN("The item is drawn centered on the desired point")
      {

        const auto item_center = item->sceneBoundingRect().center();
        REQUIRE(item_center.x() == desired_position.x());
        REQUIRE(item_center.y() == desired_position.y());
      }

      AND_THEN("the item is drawn at the desired size")
      {
        auto drawn_size = item->deviceTransform(view->viewportTransform())
                            .mapRect(item->boundingRect());

        // This is the size of the symbol in map units (degrees)
        auto scene_size =
          view->viewportTransform().inverted().mapRect(drawn_size);

        REQUIRE(scene_size.width() == Approx(10.0));
        REQUIRE(scene_size.height() == Approx(10.0));
      }
    }
  }
}
