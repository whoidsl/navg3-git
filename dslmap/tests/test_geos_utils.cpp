/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 10/17/17.
//

#include "GeosUtils.h"
#include "catch.hpp"

#include <QPointF>
#include <QVector>

using namespace dslmap;

SCENARIO("Converting a WKT LineString to a painter path.")
{

  GIVEN("A single LineString WKT")
  {

    const auto points =
      QVector<QPointF>{ { -10, -10 }, { -10, 10 }, { 10, 10 }, { 10, -10 } };
    auto wkt = QString{ "LINESTRING (" };
    for (auto& p : points) {
      wkt += QString("%1 %2,").arg(p.x()).arg(p.y());
    }

    wkt.replace(wkt.size() - 1, 1, ')');

    WHEN("the text is converted")
    {
      const auto path = pathFromWkt(wkt);

      THEN("The path is not null") { REQUIRE_FALSE(path.isEmpty()); }

      AND_THEN(
        "The number of elements matches the number of points in the linestring")
      {
        // 1 extra to close the subpath.
        REQUIRE(path.elementCount() == points.size() + 1);
      }

      AND_THEN("The first element is a moveTo the first point")
      {
        auto elem = path.elementAt(0);
        REQUIRE(elem.type == QPainterPath::ElementType::MoveToElement);
        REQUIRE(elem.x == points.at(0).x());
        REQUIRE(elem.y == points.at(0).y());
      }

      AND_THEN("The remaining elements are lineTo the corresponding point")
      {
        for (auto i = 1; i < points.size(); ++i) {
          INFO("Point:" << i);
          auto elem = path.elementAt(i);
          REQUIRE(elem.type == QPainterPath::ElementType::LineToElement);
          REQUIRE(elem.x == points.at(i).x());
          REQUIRE(elem.y == points.at(i).y());
        }
      }
    }
  }
}
