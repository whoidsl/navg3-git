/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "MapScene.h"
#include "SymbolLayer.h"
#include "catch.hpp"

#include <QApplication>
#include <QDebug>
#include <QSettings>
#include <QSignalSpy>

using namespace dslmap;

TEST_CASE("SymbolGroupObject Defaults")
{
  auto layer = std::make_unique<SymbolLayer>();

  INFO("Layer has the proper MapGraphicsItemType");
  REQUIRE(layer->type() == MapItemType::SymbolLayerType);

  INFO("Layer has no items")
  REQUIRE(layer->count() == 0);

  INFO("Layer has a valid uuid")
  REQUIRE_FALSE(layer->id().isNull());

  INFO("Layer had a non-null symbol");
  REQUIRE(layer->symbolPtr() != nullptr);

  auto proj = layer->projectionPtr();
  INFO("Layer projection defaults to WGS84")
  REQUIRE(proj);

  CHECK(proj->isSame(ProjWGS84::getInstance()));
}

SCENARIO("adding a point to a SymbolGroupObject")
{
  GIVEN("a SymbolGroupObject")
  {
    auto scene = std::make_unique<MapScene>();
    auto layer = new SymbolLayer;

    scene->addItem(layer);

    WHEN("a point is added to the layer")
    {
      const auto point = QPointF{ 10, 10 };
      const auto label = QString{ "marker label" };

      QSignalSpy spy(layer, SIGNAL(itemAdded(QGraphicsItem*)));

      auto marker = layer->addPoint(point.x(), point.y(), label, {});

      THEN("the new marker is returned to the caller")
      {
        REQUIRE(marker != nullptr);
        REQUIRE(marker->labelText() == label);
        REQUIRE(marker->scenePos() == point);
      }

      AND_THEN("the marker's parentItem has been set to the layer")
      {
        REQUIRE(marker->parentItem() == layer);
        REQUIRE(layer->childItems().contains(marker));
      }
    }
  }
}

SCENARIO("removing points from a SymbolGroupObject")
{
  GIVEN("a SymbolGroupObject")
  {
    auto scene = std::make_unique<MapScene>();
    auto layer = new SymbolLayer;

    scene->addItem(layer);

    WHEN("a point is added to and removed from the layer")
    {
      const auto point = QPointF{ 10, 10 };
      const auto label = QString{ "marker label" };

      auto marker = layer->addPoint(point.x(), point.y(), label, {});

      REQUIRE(marker != nullptr);

      layer->removePoint(marker);

      THEN("the marker's parent item is set NULL")
      {
        REQUIRE(marker->parentItem() == nullptr);
      }

      AND_THEN("the marker has been removed from the layer's scene")
      {
        REQUIRE(marker->scene() == nullptr);
      }
    }

    WHEN("Multiple points have been added to the layer")
    {
      const auto point = QPointF{ 10, 10 };
      const auto label = QString{ "marker label" };

      auto marker1 = layer->addPoint(point.x(), point.y(), label, {});
      auto marker2 = layer->addPoint(point.x(), point.y(), label, {});

      REQUIRE(marker1 != nullptr);
      REQUIRE(marker2 != nullptr);

      AND_THEN("The count decreases after each point is removed.")
      {
        layer->removePoint(marker1);
        REQUIRE(layer->count() == 1);
        layer->removePoint(marker2);
        REQUIRE(layer->count() == 0);
      }
    }
  }
}

SCENARIO(
  "testing that adding/removing points on layer starting with no attributes")
{
  GIVEN("a map scene with a symbol layer and a point with no attributes")
  {
    auto scene = std::make_unique<MapScene>();
    auto layer = new SymbolLayer;
    scene->addItem(layer);
    const auto point1 = QPointF{ 1, 1 };
    const auto label1 = QString{ "label 1" };
    auto marker1 = layer->addPoint(point1.x(), point1.y(), label1, {});

    THEN("the marker is added successfully") { REQUIRE(marker1 != nullptr); }
    AND_THEN("there is one marker in the layer")
    {
      REQUIRE(layer->count() == 1);
    }
    AND_THEN("The layer has no attributes")
    {
      REQUIRE(layer->attributeKeys().length() == 0);
    }

    WHEN("A second point WITHOUT attributes is added")
    {
      const auto point2 = QPointF{ 2, 2 };
      const auto label2 = QString{ "label 2" };
      auto marker2 = layer->addPoint(point2.x(), point2.y(), label2, {});
      THEN("the marker is added successfully") { REQUIRE(marker2 != nullptr); }
      AND_THEN("there are two markers in the layer")
      {
        REQUIRE(layer->count() == 2);
      }
    }

    AND_WHEN("A second point WITH an attribute is added")
    {
      const auto point2 = QPointF{ 2, 2 };
      const auto label2 = QString{ "label 2" };
      auto attributes2 = QVariantMap();
      attributes2.insert(attributes2.constBegin(), QString("attr1"),
                         QVariant(1.0));
      auto marker2 =
        layer->addPoint(point2.x(), point2.y(), label2, attributes2);
      THEN("the marker will be added") { REQUIRE(marker2 != nullptr); }
      AND_THEN("There will be 2 markers") { REQUIRE(layer->count() == 2); }
      AND_THEN("The layer's attributes include the new attribute")
      {
        REQUIRE(layer->attributeKeys().length() == 1);
      }

      AND_WHEN("The first point is removed")
      {
        layer->removePoint(marker1);
        THEN("There is one marker in the layer")
        {
          REQUIRE(layer->count() == 1);
        }
        AND_THEN("the layer has the second point's attributes")
        {
          REQUIRE(layer->attributeKeys().length() == 1);
          REQUIRE(layer->attributeKeys().count("attr1") == 1);
        }
      }

      AND_WHEN("The second point is removed")
      {
        layer->removePoint(marker2);
        THEN("There is one marker in the layer")
        {
          REQUIRE(layer->count() == 1);
        }
        AND_THEN("The layer has no attributes")
        {
          REQUIRE(layer->attributeKeys().length() == 0);
        }
      }
    }
  } // end GIVEN
} // end SCENARIO starting with no attributes

SCENARIO(
  "testing that adding/removing points on layer starting with one attribute")
{
  GIVEN("a map scene with a symbol layer and a point with one attribute")
  {
    auto scene = std::make_unique<MapScene>();
    auto layer = new SymbolLayer;
    scene->addItem(layer);
    const auto point1 = QPointF{ 1, 1 };
    const auto label1 = QString{ "label 1" };
    auto attributes1 = QVariantMap();
    attributes1.insert(attributes1.constBegin(), QString("attr1"),
                       QVariant(1.0));
    auto marker1 = layer->addPoint(point1.x(), point1.y(), label1, attributes1);

    THEN("The point was added successfully ") { REQUIRE(marker1 != nullptr); }
    AND_THEN("The layer has one marker") { REQUIRE(layer->count() == 1); }
    AND_THEN("The layer has one attribute")
    {
      REQUIRE(layer->attributeKeys().length() == 1);
    }

    AND_WHEN("A second point with the same attribute is added")
    {
      const auto point2 = QPointF{ 2, 2 };
      const auto label2 = QString{ "label 2" };
      auto attributes2 = QVariantMap();
      attributes2.insert(attributes2.constBegin(), QString("attr1"),
                         QVariant(2.0));
      auto marker2 =
        layer->addPoint(point2.x(), point2.y(), label2, attributes2);

      THEN("The point was added successfully") { REQUIRE(marker2 != nullptr); }
      AND_THEN("The layer has two markers") { REQUIRE(layer->count() == 2); }
      AND_THEN("but the attribute hasn't been duplicated")
      {
        REQUIRE(layer->attributeKeys().length() == 1);
      }

      AND_WHEN("The first point is removed")
      {
        layer->removePoint(marker1);
        THEN("The layer has a single marker") { REQUIRE(layer->count() == 1); }
        AND_THEN("and still has the attribute")
        {
          REQUIRE(layer->attributeKeys().length() == 1);
          REQUIRE(layer->attributeKeys().count("attr1") == 1);
        }
      }
    }

    AND_WHEN("A second point is added with the same attribute name, but "
             "different QVariant type")
    {
      const auto point2 = QPointF{ 2, 2 };
      const auto label2 = QString{ "label 2" };
      auto attributes2 = QVariantMap();
      attributes2.insert(attributes2.constBegin(), QString("attr1"),
                         QVariant("test"));
      auto marker2 =
        layer->addPoint(point2.x(), point2.y(), label2, attributes2);
      THEN("The point is added successfully") { REQUIRE(marker2 != nullptr); }
      AND_THEN("The layer has two markers") { REQUIRE(layer->count() == 2); }
      AND_THEN("The layer has one attribute")
      {
        REQUIRE(layer->attributeKeys().length() == 1);
      }
    }

    AND_WHEN("A second point without attributes is added")
    {
      const auto point2 = QPointF{ 2, 2 };
      const auto label2 = QString{ "label 2" };
      auto marker2 = layer->addPoint(point2.x(), point2.y(), label2, {});
      THEN("The point is added") { REQUIRE(marker2 != nullptr); }
      AND_THEN("The layer has 2 markers") { REQUIRE(layer->count() == 2); }
      AND_THEN("The layer still has a single attribute")
      {
        REQUIRE(layer->attributeKeys().length() == 1);
      }
    }

    AND_WHEN("A second point with a different attribute is added")
    {
      const auto point2 = QPointF{ 5, 2 };
      const auto label2 = QString{ "label 2" };
      auto attributes2 = QVariantMap();
      attributes2.insert(attributes2.constBegin(), QString("attr2"),
                         QVariant("test"));
      auto marker2 =
        layer->addPoint(point2.x(), point2.y(), label2, attributes2);
      THEN("The point is added") { REQUIRE(marker2 != nullptr); }
      AND_THEN("The layer has 2 markers") { REQUIRE(layer->count() == 2); }
      AND_THEN("The layer has the additional attribute")
      {
        REQUIRE(layer->attributeKeys().length() == 2);
      }

      AND_WHEN("The first point is removed")
      {
        layer->removePoint(marker1);
        THEN("The layer has a single point") { REQUIRE(layer->count() == 1); }
        AND_THEN("The layer only has the second point's attribute")
        {
          REQUIRE(layer->attributeKeys().length() == 1);
          REQUIRE(layer->attributeKeys().count("attr2") == 1);
        }
      }

      AND_WHEN("The second point is removed")
      {
        layer->removePoint(marker2);
        THEN("The layer has a single point") { REQUIRE(layer->count() == 1); }
        AND_THEN("The layer only has the first point's attribute")
        {
          REQUIRE(layer->attributeKeys().length() == 1);
          REQUIRE(layer->attributeKeys().count("attr1") == 1);
        }
      }
    }

    AND_WHEN("A second point whose attributes are a superset is added")
    {
      const auto point2 = QPointF{ 2, 2 };
      const auto label2 = QString{ "label 2" };
      auto attributes2 = QVariantMap();
      attributes2.insert(attributes2.constBegin(), QString("attr1"),
                         QVariant(1));
      attributes2.insert(attributes2.constBegin(), QString("attr2 "),
                         QVariant(2));
      auto marker2 =
        layer->addPoint(point2.x(), point2.y(), label2, attributes2);
      THEN("The point is added") { REQUIRE(marker2 != nullptr); }
      AND_THEN("The layer has 2 markers") { REQUIRE(layer->count() == 2); }
      AND_THEN("The layer's attributes include the new one")
      {
        REQUIRE(layer->attributeKeys().length() == 2);
      }

      AND_WHEN("The first point is removed")
      {
        layer->removePoint(marker1);
        THEN("The layer has only one point") { REQUIRE(layer->count() == 1); }
        AND_THEN("The layer still has both attributes")
        {
          REQUIRE(layer->attributeKeys().length() == 2);
        }
      }

      AND_WHEN("The second point is removed")
      {
        layer->removePoint(marker2);
        THEN("The layer has only one point") { REQUIRE(layer->count() == 1); }
        AND_THEN("The layer only has the first point's attribute")
        {
          REQUIRE(layer->attributeKeys().length() == 1);
          REQUIRE(layer->attributeKeys().count("attr1") == 1);
        }
      }
    }
  }
}

SCENARIO("The projection changes.")
{

  GIVEN("A SymbolLayer with a projection and points")
  {
    // Make 100 lat/lon coordinates near the scene UTM zone.
    auto positions = QList<QPointF>{};
    auto mean_lon = 0.0;
    auto mean_lat = 0.0;
    const auto num_points = 100;
    for (auto i = 0; i < num_points; i++) {
      auto lon = static_cast<qreal>(qrand() / RAND_MAX * 4 - 4);
      auto lat = static_cast<qreal>(qrand() / RAND_MAX * 85);
      mean_lon += lon / num_points;
      mean_lat += lat / num_points;
      positions << QPointF{ lon, lat };
    }

    const auto scene_origin = QPointF{ mean_lon, mean_lat };

    auto proj = Proj::fromDefinition(PROJ_WGS84_DEFINITION);
    REQUIRE(proj);

    auto layer = new SymbolLayer;
    layer->setProjection(proj);

    auto items = QList<QGraphicsItem*>{};
    items.reserve(num_points);

    // Create "markers" at the generated positions.
    for (auto it = std::begin(positions); it != std::end(positions); it++) {
      items << layer->addPoint(*it);
    }

    WHEN("the layer projection is changed.")
    {
      auto utm_proj = Proj::utmFromLonLat(scene_origin.x(), scene_origin.y());
      REQUIRE(utm_proj);
      layer->setProjection(utm_proj);

      THEN("the layer items are reprojected to the scene projection")
      {

        const auto new_proj = layer->projection();

        // The layer projection string should now match the scene projection.
        REQUIRE(new_proj);
        REQUIRE(new_proj->isSame(*utm_proj));

        for (auto i = 0; i < items.count(); i++) {
          const auto new_pos = items.at(i)->scenePos();
          auto lon = positions.at(i).x();
          auto lat = positions.at(i).y();

          proj->transformTo(*utm_proj, 1, &lon, &lat);

          REQUIRE(lat == new_pos.y());
          REQUIRE(lon == new_pos.x());
        }
      }
    }
  }
}

SCENARIO("Creating a SymbolLayer from settings")
{
  GIVEN("A valid group of settings")
  {
    QSettings settings(QSettings::InvalidFormat, QSettings::UserScope, "Test");
    settings.setValue("name", "Example Layer");
    settings.setValue("symbol/stroke/color", QColor{ Qt::darkBlue });
    settings.setValue("symbol/stroke/width", 2);
    settings.setValue("symbol/fill/color", QColor{ Qt::blue });
    settings.setValue("symbol/shape", "square");
    settings.setValue("symbol/size", "15");

    WHEN("A BeaconLayer is created with the settings")
    {
      auto layer = SymbolLayer::fromSettings(settings);

      THEN("The layer creation is successful")
      {
        REQUIRE(layer);
        REQUIRE_FALSE(layer->id().isNull());
      }

      AND_THEN("All layer properties are set correctly")
      {
        auto symbol =
          std::dynamic_pointer_cast<ShapeMarkerSymbol>(layer->symbol());
        REQUIRE(symbol);
        auto expected_symbol =
          ShapeMarkerSymbol::fromSettings(settings, "symbol/");
        REQUIRE(expected_symbol);

        CHECK(symbol->shape() == expected_symbol->shape());
        CHECK(symbol->size() == expected_symbol->size());
        CHECK(symbol->outlinePen().color().name() ==
              expected_symbol->outlinePen().color().name());
        CHECK(symbol->outlinePen().widthF() ==
              expected_symbol->outlinePen().widthF());
        CHECK(symbol->fillBrush().color().name() ==
              expected_symbol->fillBrush().color().name());
        CHECK(symbol->labelPen().color().name() ==
              expected_symbol->labelPen().color().name());
        CHECK(symbol->isLabelVisible() == expected_symbol->isLabelVisible());
        CHECK(symbol->labelAlignment() == expected_symbol->labelAlignment());

        CHECK(layer->name() == settings.value("name").toString());
      }
    }
  }
}

SCENARIO("Saving SymbolLayer properites")
{
  GIVEN("A SymbolLayer with two ShapeMarkerSymbols")
  {
    auto layer = std::make_unique<SymbolLayer>();
    layer->setName("Symbollayer");
    auto symbol = std::make_unique<ShapeMarkerSymbol>(
      ShapeMarkerSymbol::Shape::Diamond, 25);
    symbol->setOutlineColor(Qt::red);
    symbol->setOutlineWidth(3);
    symbol->setLabelColor(Qt::black);
    symbol->setFillColor(Qt::yellow);
    symbol->setLabelVisible(false);
    symbol->setLabelAlignment(Qt::AlignHCenter);

    layer->setSymbol(std::move(symbol));

    WHEN("The settings are saved to a QSettings object")
    {
      QSettings settings(QSettings::InvalidFormat, QSettings::UserScope,
                         "Test");
      layer->saveSettings(settings);

      THEN("All settings are saved correctly")
      {
        auto symbol =
          std::dynamic_pointer_cast<ShapeMarkerSymbol>(layer->symbol());
        REQUIRE(symbol);
        auto expected_symbol =
          ShapeMarkerSymbol::fromSettings(settings, "symbol/");
        REQUIRE(expected_symbol);

        CHECK(symbol->shape() == expected_symbol->shape());
        CHECK(symbol->size() == expected_symbol->size());
        CHECK(symbol->outlinePen().color().name() ==
              expected_symbol->outlinePen().color().name());
        CHECK(symbol->outlinePen().widthF() ==
              expected_symbol->outlinePen().widthF());
        CHECK(symbol->fillBrush().color().name() ==
              expected_symbol->fillBrush().color().name());
        CHECK(symbol->labelPen().color().name() ==
              expected_symbol->labelPen().color().name());
        CHECK(symbol->isLabelVisible() == expected_symbol->isLabelVisible());
        CHECK(symbol->labelAlignment() == expected_symbol->labelAlignment());

        CHECK(layer->name() == settings.value("name").toString());
      }
    }
  }
}
