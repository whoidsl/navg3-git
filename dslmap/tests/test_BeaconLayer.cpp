/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "BeaconLayer.h"
#include "WktMarkerSymbol.h"
#include "catch.hpp"

#include <QPen>
#include <QSettings>

using namespace dslmap;

namespace test_BeaconLayer {
std::unique_ptr<BeaconLayer>
createTestLayer()
{
  auto layer = std::make_unique<BeaconLayer>();
  auto symbol =
    std::make_unique<ShapeMarkerSymbol>(ShapeMarkerSymbol::Shape::Diamond, 25);
  symbol->setOutlineColor(Qt::red);
  symbol->setOutlineWidth(3);
  symbol->setLabelColor(Qt::black);
  symbol->setFillColor(Qt::yellow);
  symbol->setLabelVisible(false);
  symbol->setLabelAlignment(Qt::AlignHCenter);

  layer->setBeaconSymbol(std::move(symbol));

  symbol =
    std::make_unique<ShapeMarkerSymbol>(ShapeMarkerSymbol::Shape::Square, 5);
  symbol->setOutlineColor(Qt::blue);
  symbol->setOutlineWidth(3);
  symbol->setLabelColor(Qt::green);
  symbol->setFillColor(Qt::magenta);
  symbol->setLabelVisible(true);
  symbol->setLabelAlignment(Qt::AlignVCenter);

  layer->setTrailSymbol(std::move(symbol));

  return layer;
}
}

SCENARIO("Creating a BeaconLayer from settings")
{
  GIVEN("A valid group of settings")
  {
    QSettings settings(QSettings::InvalidFormat, QSettings::UserScope, "Test");
    settings.setValue("name", "Example Layer");
    settings.setValue("beacon/stroke/color", QColor{ Qt::darkBlue });
    settings.setValue("beacon/stroke/width", 2);
    settings.setValue("beacon/fill/color", QColor{ Qt::blue });
    settings.setValue("beacon/shape", "square");
    settings.setValue("beacon/size", "15");
    settings.setValue("trail/stroke/color", QColor{ Qt::darkYellow });
    settings.setValue("trail/stroke/width", 1);
    settings.setValue("trail/fill/color", QColor{ Qt::yellow });
    settings.setValue("trail/shape", "circle");
    settings.setValue("trail/size", 5);
    settings.setValue("trail/displayed_length", 1000);
    settings.setValue("trail/capacity", 5000);

    WHEN("A BeaconLayer is created with the settings")
    {
      auto layer = BeaconLayer::fromSettings(settings);

      THEN("The layer creation is successful")
      {
        REQUIRE(layer);
        REQUIRE_FALSE(layer->id().isNull());
      }

      AND_THEN("All layer properties are set correctly")
      {
        auto symbol =
          std::dynamic_pointer_cast<ShapeMarkerSymbol>(layer->beaconSymbol());
        REQUIRE(symbol);
        auto expected_symbol =
          ShapeMarkerSymbol::fromSettings(settings, "beacon/");
        REQUIRE(expected_symbol);

        CHECK(symbol->shape() == expected_symbol->shape());
        CHECK(symbol->size() == expected_symbol->size());
        CHECK(symbol->outlinePen().color().name() ==
              expected_symbol->outlinePen().color().name());
        CHECK(symbol->outlinePen().widthF() ==
              expected_symbol->outlinePen().widthF());
        CHECK(symbol->fillBrush().color().name() ==
              expected_symbol->fillBrush().color().name());
        CHECK(symbol->labelPen().color().name() ==
              expected_symbol->labelPen().color().name());
        CHECK(symbol->isLabelVisible() == expected_symbol->isLabelVisible());
        CHECK(symbol->labelAlignment() == expected_symbol->labelAlignment());

        symbol =
          std::dynamic_pointer_cast<ShapeMarkerSymbol>(layer->trailSymbol());
        REQUIRE(symbol);
        expected_symbol = ShapeMarkerSymbol::fromSettings(settings, "trail/");
        REQUIRE(expected_symbol);
        CHECK(symbol->shape() == expected_symbol->shape());
        CHECK(symbol->size() == expected_symbol->size());
        CHECK(symbol->outlinePen().color().name() ==
              expected_symbol->outlinePen().color().name());
        CHECK(symbol->outlinePen().widthF() ==
              expected_symbol->outlinePen().widthF());
        CHECK(symbol->fillBrush().color().name() ==
              expected_symbol->fillBrush().color().name());
        CHECK(symbol->labelPen().color().name() ==
              expected_symbol->labelPen().color().name());
        CHECK(symbol->isLabelVisible() == expected_symbol->isLabelVisible());
        CHECK(symbol->labelAlignment() == expected_symbol->labelAlignment());

        CHECK(layer->name() == settings.value("name").toString());
        CHECK(layer->displayedLength() ==
              settings.value("trail/displayed_length").toInt());
        CHECK(layer->capacity() == settings.value("trail/capacity").toInt());
      }
    }
  }

  GIVEN("A valid group of settings with a WKT beacon shape")
  {
    QSettings settings(QSettings::InvalidFormat, QSettings::UserScope, "Test");
    settings.setValue("name", "Example Layer");
    settings.setValue("beacon/stroke/color", QColor{ Qt::darkBlue });
    settings.setValue("beacon/stroke/width", 2);
    settings.setValue("beacon/fill/color", QColor{ Qt::blue });
    settings.setValue("beacon/shape_wkt",
                      "LINESTRING (-10 -10, -10 10, 10 10, 10 -10)");
    settings.setValue("beacon/size", "15");
    settings.setValue("trail/stroke/color", QColor{ Qt::darkYellow });
    settings.setValue("trail/stroke/width", 1);
    settings.setValue("trail/fill/color", QColor{ Qt::yellow });
    settings.setValue("trail/shape", "circle");
    settings.setValue("trail/size", 5);
    settings.setValue("trail/displayed_length", 1000);
    settings.setValue("trail/capacity", 5000);

    WHEN("A BeaconLayer is created with the settings")
    {
      auto layer = BeaconLayer::fromSettings(settings);

      THEN("The layer creation is successful")
      {
        REQUIRE(layer);
        REQUIRE_FALSE(layer->id().isNull());
      }

      AND_THEN("All layer properties are set correctly")
      {
        auto wkt_symbol =
          std::dynamic_pointer_cast<WktMarkerSymbol>(layer->beaconSymbol());
        REQUIRE(wkt_symbol);
        auto expected_wkt_symbol =
          WktMarkerSymbol::fromSettings(settings, "beacon/");
        REQUIRE(expected_wkt_symbol);

        CHECK(wkt_symbol->wktString() == expected_wkt_symbol->wktString());
        CHECK(wkt_symbol->outlinePen().color().name() ==
              expected_wkt_symbol->outlinePen().color().name());
        CHECK(wkt_symbol->outlinePen().widthF() ==
              expected_wkt_symbol->outlinePen().widthF());
        CHECK(wkt_symbol->fillBrush().color().name() ==
              expected_wkt_symbol->fillBrush().color().name());
        CHECK(wkt_symbol->labelPen().color().name() ==
              expected_wkt_symbol->labelPen().color().name());
        CHECK(wkt_symbol->isLabelVisible() ==
              expected_wkt_symbol->isLabelVisible());
        CHECK(wkt_symbol->labelAlignment() ==
              expected_wkt_symbol->labelAlignment());

        auto symbol =
          std::static_pointer_cast<ShapeMarkerSymbol>(layer->trailSymbol());
        REQUIRE(symbol);
        auto expected_symbol =
          ShapeMarkerSymbol::fromSettings(settings, "trail/");
        REQUIRE(expected_symbol);
        CHECK(symbol->shape() == expected_symbol->shape());
        CHECK(symbol->size() == expected_symbol->size());
        CHECK(symbol->outlinePen().color().name() ==
              expected_symbol->outlinePen().color().name());
        CHECK(symbol->outlinePen().widthF() ==
              expected_symbol->outlinePen().widthF());
        CHECK(symbol->fillBrush().color().name() ==
              expected_symbol->fillBrush().color().name());
        CHECK(symbol->labelPen().color().name() ==
              expected_symbol->labelPen().color().name());
        CHECK(symbol->isLabelVisible() == expected_symbol->isLabelVisible());
        CHECK(symbol->labelAlignment() == expected_symbol->labelAlignment());

        CHECK(layer->name() == settings.value("name").toString());
        CHECK(layer->displayedLength() ==
              settings.value("trail/displayed_length").toInt());
        CHECK(layer->capacity() == settings.value("trail/capacity").toInt());
      }
    }
  }
}

SCENARIO("Saving BeaconLayer properites")
{
  GIVEN("A BeaconLayer with symbol shapes defined")
  {
    auto layer = std::make_unique<BeaconLayer>();
    layer->setName("Beacon layer");
    auto symbol = std::make_unique<ShapeMarkerSymbol>(
      ShapeMarkerSymbol::Shape::Diamond, 25);
    symbol->setOutlineColor(Qt::red);
    symbol->setOutlineWidth(3);
    symbol->setLabelColor(Qt::black);
    symbol->setFillColor(Qt::yellow);
    symbol->setLabelVisible(false);
    symbol->setLabelAlignment(Qt::AlignHCenter);

    layer->setBeaconSymbol(std::move(symbol));

    symbol =
      std::make_unique<ShapeMarkerSymbol>(ShapeMarkerSymbol::Shape::Square, 5);
    symbol->setOutlineColor(Qt::blue);
    symbol->setOutlineWidth(3);
    symbol->setLabelColor(Qt::green);
    symbol->setFillColor(Qt::magenta);
    symbol->setLabelVisible(true);
    symbol->setLabelAlignment(Qt::AlignVCenter);

    layer->setTrailSymbol(std::move(symbol));

    layer->setDisplayedLength(-1);
    layer->setCapacity(300);

    WHEN("The settings are saved to a QSettings object")
    {
      QSettings settings(QSettings::InvalidFormat, QSettings::UserScope,
                         "Test");
      layer->saveSettings(settings);

      THEN("All settings are saved correctly")
      {
        auto symbol =
          std::dynamic_pointer_cast<ShapeMarkerSymbol>(layer->beaconSymbol());
        REQUIRE(symbol);
        auto expected_symbol =
          ShapeMarkerSymbol::fromSettings(settings, "beacon/");
        REQUIRE(expected_symbol);

        CHECK(symbol->shape() == expected_symbol->shape());
        CHECK(symbol->size() == expected_symbol->size());
        CHECK(symbol->outlinePen().color().name() ==
              expected_symbol->outlinePen().color().name());
        CHECK(symbol->outlinePen().widthF() ==
              expected_symbol->outlinePen().widthF());
        CHECK(symbol->fillBrush().color().name() ==
              expected_symbol->fillBrush().color().name());
        CHECK(symbol->labelPen().color().name() ==
              expected_symbol->labelPen().color().name());
        CHECK(symbol->isLabelVisible() == expected_symbol->isLabelVisible());
        CHECK(symbol->labelAlignment() == expected_symbol->labelAlignment());

        symbol =
          std::dynamic_pointer_cast<ShapeMarkerSymbol>(layer->trailSymbol());
        REQUIRE(symbol);
        expected_symbol = ShapeMarkerSymbol::fromSettings(settings, "trail/");
        REQUIRE(expected_symbol);
        CHECK(symbol->shape() == expected_symbol->shape());
        CHECK(symbol->size() == expected_symbol->size());
        CHECK(*symbol == *expected_symbol);

        CHECK(layer->name() == settings.value("name").toString());
        CHECK(layer->displayedLength() ==
              settings.value("trail/length").toInt());
        CHECK(layer->capacity() == settings.value("trail/capacity").toInt());
      }
    }
  }
}

SCENARIO("Changing BeaconLayer maximum trail length to a negative value")
{
  GIVEN("A BeaconLayer with two symbols")
  {

    auto layer = test_BeaconLayer::createTestLayer();
    REQUIRE(layer->capacity() > 0);

    const auto positions = QVector<QPointF>{ { 0, 0 }, { 0, 1 }, { 0, 2 } };
    for (const auto& pos : positions) {
      layer->updatePosition(pos.x(), pos.y());
    }

    REQUIRE(layer->count() == positions.size() - 1);

    WHEN("The trail capacity is changed to a negative value")
    {
      const auto orig_cap = layer->capacity();
      layer->setCapacity(-10);

      THEN("The trail capacity does not change")
      {
        CHECK(layer->capacity() == orig_cap);
      }
    }
  }
}

SCENARIO("Changing BeaconLayer maximum trail length to 0")
{

  GIVEN("A BeaconLayer with two symbols")
  {

    auto layer = test_BeaconLayer::createTestLayer();
    REQUIRE(layer->capacity() > 0);

    const auto positions = QVector<QPointF>{ { 0, 0 }, { 0, 1 }, { 0, 2 } };
    for (const auto& pos : positions) {
      layer->updatePosition(pos.x(), pos.y());
    }

    REQUIRE(layer->count() == positions.size() - 1);

    WHEN("The trail capacity is set to 0")
    {
      REQUIRE(layer->count() > 0);
      REQUIRE(layer->capacity() > 0);

      layer->setCapacity(0);

      THEN("The trail will be removed") { CHECK(layer->count() == 0); }

      AND_WHEN("The position is updated")
      {
        const auto new_pos = QPointF{ 0, 4 };
        layer->updatePosition(new_pos.x(), new_pos.y());

        THEN("No new point is added to the trail")
        {
          CHECK(layer->count() == 0);
        }

        AND_THEN("The current position will have changed")
        {
          const auto pos = layer->currentPosition();
          CHECK(pos.x() == new_pos.x());
          CHECK(pos.y() == new_pos.y());
        }
      }
    }
  }
}

SCENARIO("Changing BeaconLayer maximum trail length is reduced to less than "
         "the existing trail size")
{

  GIVEN("A BeaconLayer with two symbols")
  {

    auto layer = test_BeaconLayer::createTestLayer();
    REQUIRE(layer->capacity() > 0);

    const auto positions = QVector<QPointF>{ { 0, 0 }, { 0, 1 }, { 0, 2 } };
    for (const auto& pos : positions) {
      layer->updatePosition(pos.x(), pos.y());
    }

    REQUIRE(layer->count() == positions.size() - 1);

    WHEN("The trail capacity is reduced")
    {
      const auto new_capacity = 1;
      REQUIRE(new_capacity != layer->capacity());

      layer->setCapacity(new_capacity);

      THEN("The trail capacity will be as expected")
      {
        CHECK(layer->capacity() == new_capacity);
      }

      AND_THEN("The trail marker length will equal the new capacity")
      {
        CHECK(layer->count() == layer->capacity());
      }
    }
  }
}
