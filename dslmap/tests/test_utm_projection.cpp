/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include <catch.hpp>

#include "Proj4Utils.h"

using namespace dslmap;

SCENARIO("You want the UTM zone for a longitude")
{

  GIVEN("Some longitude values")
  {
    auto expected_lon_utm_pairs = QList<QPair<qreal, quint8>>{
      { -180, 1 }, { 179.9, 60 }, { 0, 31 },
    };

    WHEN("the UTM zone for a value is requested")
    {

      auto calculated_utm_values = QList<quint8>{};
      for (auto it = std::begin(expected_lon_utm_pairs);
           it != std::end(expected_lon_utm_pairs); it++) {
        calculated_utm_values << utmZoneFromLongitude(it->first);
      }

      THEN("the correct UTM zones are returned")
      {
        for (auto i = 0; i < calculated_utm_values.count(); i++) {
          REQUIRE(expected_lon_utm_pairs.at(i).second ==
                  calculated_utm_values.at(i));
        }
      }
    }
  }
}
