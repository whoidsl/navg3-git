/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "catch.hpp"

#include "ImageObject.h"

#include "GdalUtils.h"
#include "Proj.h"

#include <gdal/gdal_priv.h>

using namespace dslmap;

TEST_CASE("ImageObject Defaults")
{

  auto item = std::make_unique<ImageObject>();

  INFO("Item has the proper MapGraphicsItemType");
  REQUIRE(item->type() == MapItemType::ImageObjectType);

  INFO("Item has a WGS84 projection");
  auto proj = item->projectionPtr();
  REQUIRE(proj);
  CHECK(proj->isSame(ProjWGS84::getInstance()));
}

SCENARIO("Loading a basic image into a ImageObject")
{

  GIVEN("a ImageObject and a projection")
  {
    auto item = std::make_unique<ImageObject>();
    const auto bounds = QRectF{ 0, 0, 1, 1 };
    auto proj = Proj::fromDefinition(PROJ_WGS84_DEFINITION);

    WHEN("a PNG is loaded, without providing a projection")
    {
      const QImage image(":/whoi-logo.png");
      const auto ok = item->setImage(image, bounds, proj);
      REQUIRE(ok);

      THEN("the projected dataset should point to the original.")
      {
        const auto orig = item->dataset();
        const auto proj = item->projectedDataset();
        REQUIRE(orig == proj);
      }

      AND_THEN("the dataset's transform should match expected values")
      {
        const auto poly = QPolygonF{ QVector<QPointF>{
          bounds.topLeft(), bounds.topRight(), bounds.bottomRight(),
          bounds.bottomLeft() } };

        const auto expected_adf = polygonToAdfProjection(image.size(), poly);

        auto adf_transform = QVector<double>{ 0, 0, 0, 0, 0, 0 };
        item->dataset()->get()->GetGeoTransform(adf_transform.data());

        REQUIRE(expected_adf == adf_transform);
      }
    }

    WHEN("a JPG is loaded, without providing a projection")
    {
      const QImage image(":/whoi-logo.jpg");
      const auto ok = item->setImage(image, bounds, proj);
      REQUIRE(ok);

      THEN("the projected dataset should point to the original.")
      {
        const auto orig = item->dataset();
        const auto proj = item->projectedDataset();
        REQUIRE(orig == proj);
      }

      AND_THEN("the dataset's transform should match expected values")
      {
        const auto poly = QPolygonF{ QVector<QPointF>{
          bounds.topLeft(), bounds.topRight(), bounds.bottomRight(),
          bounds.bottomLeft() } };

        const auto expected_adf = polygonToAdfProjection(image.size(), poly);

        auto adf_transform = QVector<double>{ 0, 0, 0, 0, 0, 0 };
        item->dataset()->get()->GetGeoTransform(adf_transform.data());

        REQUIRE(expected_adf == adf_transform);
      }
    }
  }
}