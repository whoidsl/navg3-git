/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "MapScene.h"
#include "MapView.h"
#include "Proj.h"
#include "ShapeMarkerSymbol.h"
#include "SymbolLayer.h"
#include "catch.hpp"

#include <QDebug>
#include <QGraphicsView>
#include <QtTest/QTest>

#include <memory>

using namespace dslmap;

SCENARIO("Scale-invariant markers are drawn the same size regardless of zoom")
{

  GIVEN("A SymbolGroupObject with a scale-invariant marker")
  {

    auto view = std::make_unique<MapView>();
    auto scene = new MapScene;
    view->setScene(scene);

    const auto rect = QRectF{ -5, -5, 10, 10 };

    auto path = QPainterPath{};
    path.addRect(rect);

    auto layer = new SymbolLayer;
    layer->setScaleInvariant(true);
    layer->setShape(ShapeMarkerSymbol::Shape::Square, 10);

    auto item = layer->addPoint(0, 0, {}, {});

    scene->addItem(layer);

    // Now get the drawn size of the marker (use the bounding rectangle)
    const auto orig_size = item->deviceTransform(view->viewportTransform())
                             .mapRect(item->boundingRect());

    REQUIRE(orig_size.width() == rect.width());
    REQUIRE(orig_size.height() == rect.height());

    WHEN("the viewport is zoomed in")
    {

      view->scale(2, 2);

      THEN("the marker is drawn the same size")
      {

        auto new_size = item->deviceTransform(view->viewportTransform())
                          .mapRect(item->boundingRect());

        REQUIRE(new_size.width() == orig_size.width());
        REQUIRE(new_size.height() == orig_size.height());
      }
    }

    WHEN("the viewport is zoomed out")
    {

      view->scale(0.25, 0.25);
      THEN("the marker is drawn the same size")
      {
        auto new_size = item->deviceTransform(view->viewportTransform())
                          .mapRect(item->boundingRect());

        REQUIRE(new_size.width() == orig_size.width());
        REQUIRE(new_size.height() == orig_size.height());
      }
    }
  }
}

SCENARIO("Markers with physical dimensions are drawn the correct size")
{

  GIVEN("a square marker with dimensions in meters covering 1deg square")
  {

    auto view = std::make_unique<QGraphicsView>(new QGraphicsScene);
    auto scene = new MapScene;
    view->setScene(scene);

    auto layer = new SymbolLayer;
    layer->setScaleInvariant(false);
    layer->setShape(ShapeMarkerSymbol::Shape::Square, 10);
    layer->setUnits(Unit::m);

    auto rect = layer->symbolPath().boundingRect();

    auto item = layer->addPoint(0, 0, {}, {});

    scene->addItem(layer);

    // const auto scene_units = scene->projectionUnit();
    // const auto scene_unit_scale_factor = scaleToMeters(scene_units);

    // Now get the drawn size of the marker (use the bounding rectangle)
    const auto orig_size = item->deviceTransform(view->viewportTransform())
                             .mapRect(item->boundingRect());

    // This is the size of the symbol in map units (degrees)
    auto orig_scene_size =
      view->viewportTransform().inverted().mapRect(orig_size);

    // The symbol should be about 1deg square
    REQUIRE(orig_scene_size.width() == Approx(rect.width()));
    REQUIRE(orig_scene_size.height() == Approx(rect.height()));

    WHEN("the viewport is zoomed in 2x")
    {
      view->scale(2.0, 2.0);

      auto new_size = item->deviceTransform(view->viewportTransform())
                        .mapRect(item->boundingRect());

      THEN("the marker should be drawn at double the size")
      {
        REQUIRE(new_size.width() == Approx(2 * orig_size.width()));
        REQUIRE(new_size.height() == Approx(2 * orig_size.height()));
      }

      AND_THEN("still represent the same physical size")
      {
        auto new_scene_size =
          view->viewportTransform().inverted().mapRect(new_size);
        REQUIRE(new_scene_size.width() == Approx(rect.width()));
        REQUIRE(new_scene_size.height() == Approx(rect.height()));
      }
    }

    AND_WHEN("the viewport is zoomed in 0.5x")
    {
      view->scale(0.5, 0.5);

      auto new_size = item->deviceTransform(view->viewportTransform())
                        .mapRect(item->boundingRect());
      THEN("the marker should be drawn at half the size")
      {
        REQUIRE(new_size.width() == Approx(0.5 * orig_size.width()));
        REQUIRE(new_size.height() == Approx(0.5 * orig_size.height()));
      }

      AND_THEN("still represent the same physical size")
      {
        // Still needs to represent the same physical coverage
        auto new_scene_size =
          view->viewportTransform().inverted().mapRect(new_size);
        REQUIRE(new_scene_size.width() == Approx(rect.width()));
        REQUIRE(new_scene_size.height() == Approx(rect.height()));
      }
    }
  }
}
