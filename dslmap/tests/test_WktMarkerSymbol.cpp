/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#include "WktMarkerSymbol.h"
#include "catch.hpp"

#include <QPen>
#include <QSettings>

using namespace dslmap;

SCENARIO("Setting a WktMarkerSymbol from a MULTILINESTRING")
{
  GIVEN("a valid WKT MULTILINESTRING definition")
  {
    const auto points = QList<QVector<QPointF>>{
      {
        { 0.0, -7.0 },
        { 2.0, -4.0 },
        { 2.0, 4.0 },
        { 1.0, 5.5 },
        { 0.0, 6.0 },
        { -1.0, 5.5 },
        { -2.0, 4.0 },
        { -2.0, -4.0 },
      },
      {
        { 2.0, -4.0 }, { 4.0, -4.0 }, { 4.0, -3.0 }, { 2.0, -2.0 },
      },
      {
        { 4.0, -4.0 }, { -2.0, -4.0 }, { -2.0, -2.0 }, { -4.0, -3.0 },
      },
      {
        { -4.0, 2.0 }, { -2.0, 2.0 }, { -2.0, 4.0 }, { -4.0, 3.0 },
      }

    };

    auto wkt_string = QString{ "MULTILINESTRING (" };
    for (const auto& line : points) {
      wkt_string += "(";
      for (const auto& point : line) {
        wkt_string.append(QString("%1 %2,").arg(point.x()).arg(point.y()));
      }

      auto it = std::prev(wkt_string.end());
      *it = ')';
      wkt_string.append(',');
    }
    auto it = std::prev(wkt_string.end());
    *it = ')';

    INFO("WKT string is: " << wkt_string.toStdString());

    WHEN("a WktMarkerSymbol is created")
    {
      WktMarkerSymbol wkt_marker;
      wkt_marker.setPathFromWkt(wkt_string);

      auto path = wkt_marker.path();
      THEN("The new WKT marker path is not empty") { REQUIRE(!path.isEmpty()); }
      AND_THEN("The path points are as expected")
      {
        auto i = 0;
        for (const auto& line : points) {
          auto expected_type = QPainterPath::MoveToElement;
          for (const auto& point : line) {
            INFO("Path element #" << i << " (" << point.x() << "," << point.y()
                                  << ")");
            const auto element = path.elementAt(i);
            CHECK(element.type == expected_type);
            CHECK(element.x == point.x());
            CHECK(element.y == point.y());
            expected_type = QPainterPath::LineToElement;
            ++i;
          }

          INFO("Close subpath, repeat first element element #"
               << i << " (" << line.first().x() << "," << line.first().y()
               << ")");
          const auto element = path.elementAt(i);
          CHECK(element.type == QPainterPath::LineToElement);
          CHECK(element.x == line.first().x());
          CHECK(element.y == line.first().y());
          expected_type = QPainterPath::LineToElement;
          ++i;
        }
      }
      AND_THEN("The original WKT string can be retrieved from the marker")
      {
        CHECK(wkt_string == wkt_marker.wktString());
      }
    }
  }
}
