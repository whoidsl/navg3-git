# This CMAKE script made to halt build process if essential
# gdal-bin executables are not found

#by tmjoyce

if(UNIX)
   
    set(GDALBIN_DIR "/usr/bin")
    set(GDALBIN_ROOT "/usr/bin")
    find_program(GDAL_CONTOUR gdal_contour
        HINTS
          GDALBIN_DIR
          GDALBIN_ROOT
    )
    find_program(GDAL_WARP gdalwarp
        HINTS
          GDALBIN_DIR
          GDALBIN_ROOT
    )

    if(NOT (GDAL_CONTOUR AND GDAL_WARP) )
        message(FATAL_ERROR "-- GDALBIN not found!")
    else()
        message("-- Found GDALBIN")

    endif()

endif ()
