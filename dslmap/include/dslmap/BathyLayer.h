/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 7/13/18.
//

#ifndef DSLMAP_BATHYLAYER_H
#define DSLMAP_BATHYLAYER_H

#include "BathyObject.h"
#include "DslMap.h"
#include "MapLayer.h"

#include <QLoggingCategory>

namespace dslmap {

Q_DECLARE_LOGGING_CATEGORY(dslmapBathyLayer)

class BathyLayerPrivate;
class GdalColorGradient;

class DSLMAPLIB_EXPORT BathyLayer : public MapLayer
{
  //  Q_INTERFACES(IMapLayer)
  Q_OBJECT
  Q_DECLARE_PRIVATE(BathyLayer)

public:
  explicit BathyLayer(QGraphicsItem* parent = nullptr);
  ~BathyLayer() override;

  // MapLayer overrides
  int type() const override { return MapItemType::BathymetryLayerType; }

  BathyObject* addRasterFile(const QString& filename, int band = 1);
  void showPropertiesDialog() override;

  void setProjection(std::shared_ptr<const Proj> proj) override;
  bool saveSettings(QSettings& settings) override;
  bool loadSettings(QSettings& settings) override;

  QRectF boundingRect() const override;
  void paint(QPainter* painter, const QStyleOptionGraphicsItem* option,
             QWidget* widget) override;

  QList<BathyObject*> bathyObjects() const;

  ///\brief Set the gradient used in shading the bathymetry.
  void setGradient(std::shared_ptr<GdalColorGradient> gradient);

  ///\brief Get the gradient used in shading the bathymetry.
  std::shared_ptr<GdalColorGradient> gradient() const;

  /// \brief Get the minimum value of the band
  ///
  /// \return
  qreal minValue() const noexcept;

  /// \breif Get the maximum value of the band.
  qreal maxValue() const noexcept;

  /// \brief Get the min, max value of the band.
  ///
  /// \return
  QPair<qreal, qreal> minMaxValues() const noexcept;

  void saveUnderlay();

signals:
  void saveBathyUnderlay(QString);

private:
  BathyLayerPrivate* d_ptr;

  QString source_filename;
};
}
#endif // DSLMAP_BATHYLAYER_H
