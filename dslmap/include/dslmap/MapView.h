/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 9/18/17.
//

#ifndef DSLMAP_MAPVIEW_H
#define DSLMAP_MAPVIEW_H

#include "DslMap.h"
#include <QGraphicsView>
#include <QLoggingCategory>
#include <QScopedPointer>
#include <QSettings>

#include <memory>

namespace dslmap {

class MapViewPrivate;
class MapScene;
class Proj;

Q_DECLARE_LOGGING_CATEGORY(dslmapMapView)
/// \brief A QGraphicsView extension for MapScenes
///
///
class DSLMAPLIB_EXPORT MapView : public QGraphicsView
{
  Q_OBJECT
  Q_DECLARE_PRIVATE(MapView)

  Q_PROPERTY(bool graticuleVisible READ isGraticuleVisible WRITE
               setGraticuleVisible NOTIFY graticuleVisiblityChanged)

  Q_PROPERTY(QColor graticuleColor READ graticuleColor WRITE setGraticuleColor
               NOTIFY graticuleColorChanged)

  Q_PROPERTY(Qt::PenStyle graticuleStyle READ graticuleStyle WRITE
               setGraticuleStyle NOTIFY graticuleStyleChanged)

  Q_PROPERTY(int graticuleWidth READ graticuleWidth WRITE setGraticuleWidth
               NOTIFY graticuleWidthChanged)

  Q_PROPERTY(int graticuleTickNumber READ graticuleTickNumber WRITE
               setGraticuleTickNumber NOTIFY graticuleTickNumberChanged)

  Q_PROPERTY(QPen graticulePen READ graticulePen WRITE setGraticulePen NOTIFY
               graticulePenChanged)

signals:
  ///\brief Emitted when the visiblity of the graticule has changed.
  void graticuleVisiblityChanged(bool);

  ///\brief Emitted when the color of the graticule has changed.
  void graticuleColorChanged(const QColor&);

  ///\brief Emitted when the line style of the graticule has changed.
  void graticuleStyleChanged(Qt::PenStyle);

  ///\brief Emitted when the line width of the graticule has changed.
  void graticuleWidthChanged(int);

  ///\brief Emitted when the desired number of graticule lines have changed.
  void graticuleTickNumberChanged(int);

  ///\brief Emitted when the pen of graticule has changed.
  void graticulePenChanged(const QPen&);

  ///\brief Emitted when the zoom level has changed.
  void zoomLevelChanged(int level);

  ///\brief Emitted when the mouse moves over the map.
  ///
  /// \param pos Graticule value under mouse.
  void graticulePositionUnderMouseChanged(QPointF pos);

  ///\brief Emitted when the mouse moves over the map.
  ///
  /// \param pos Map projection value under mouse
  void scenePositionUnderMouseChanged(QPointF pos);

  ///\brief Emitted when the mouse moves over the map.
  ///
  /// \param depth Depth under mouse.
  void depthUnderMouseChanged(qreal depth);

  ///\brief Emitted when the grid spacing changes.
  ///
  /// \param spacing value between box ticks
  void gridSpacingChanged(qreal width, qreal height);

  /// @brief Emitted when the lat/lon format changes
  ///
  /// \param format
  void latLonFormatChanged(LatLonFormat format);

public:
  explicit MapView(QWidget* parent = Q_NULLPTR);
  ~MapView() override;

   virtual void saveSettings(QSettings& settings, QString prefix);
  
   void saveSettings(QSettings& settings);
 
   void loadSettings(const QSettings *settings);

  /// \brief Get the graticule visibility
  ///
  /// \return
  bool isGraticuleVisible() const;

  /// \brief Get the graticule color
  ///
  /// \return
  QColor graticuleColor() const;

  /// \brief Get the graticule style.
  ///
  /// \return
  Qt::PenStyle graticuleStyle() const;

  /// \brief Get the graticule width
  ///
  /// \return
  int graticuleWidth() const;

  double graticuleSpacingWidth() const;

  double graticuleSpacingHeight() const;

  /// \brief Get the desired number of graticule lines
  ///
  /// \return
  int graticuleTickNumber() const;

  /// \brief Get a copy of graticule projection
  ///
  /// \return
  std::shared_ptr<Proj> graticuleProjection() const noexcept;

  ///\brief Access the raw pointer for the graticule projection.
  ///
  /// \return
  Proj* graticuleProjectionPtr() const noexcept;

  /// \brief Set the graticule pen
  ///
  /// \return
  QPen graticulePen() const;

  MapScene* mapScene() const;

  /// \brief Get the depth for the provided position
  ///
  /// \param pos Viewport position.
  /// \return
  qreal depthAt(QPoint pos) const noexcept;

  /// @brief Get the format used for lat/lon coordinates in the graticule.
  ///
  /// \return
  LatLonFormat latLonFormat() const noexcept;

public slots:
  /// \brief Reset map scale to show all objects in scene.
  void zoomExtents();

  /// \brief Set the graticule visible or not
  ///
  /// \param visible
  void setGraticuleVisible(bool visible);

  /// \brief Set the graticule color
  ///
  /// \param color
  void setGraticuleColor(const QColor& color);

  /// \brief Set the graticule style.
  ///
  /// \param style
  void setGraticuleStyle(const Qt::PenStyle style);

  /// \brief Set the graticule width
  ///
  /// \param width
  void setGraticuleWidth(int width);

  /// \bief Set the desired number of graticule lines
  void setGraticuleTickNumber(int ticks);

  /// \brief Set the graticule pen
  ///
  /// Note:  The pen will automatically be converted to a cosmetic pen if
  /// needed.
  ///
  /// \param pen
  void setGraticulePen(const QPen& pen);

  /// \brief Set the graticule projection
  ///
  /// \param proj
  void setGraticuleProjection(std::shared_ptr<Proj> proj);

  /// \brief Change the map zoom level
  ///
  /// Changes the map zoom level.  Positive step values zoom in, negative step
  /// values
  /// zoom out.  A zoom level of 0 is the default level and should show all map
  /// objects in the view.
  ///
  /// If `relative` is true the zoom level will be incremented/decremented by
  /// the provided number of steps.  If `relative` is false the zoom level will
  /// be treated
  /// as an absolute value.
  ///
  /// \param steps
  /// \param relative
  void zoom(int steps = 0, bool relative = false);

  /// \brief Change the map zoom level
  ///
  /// Centers the map at the provided *scene* coordinates after changing the
  /// zoom level.
  ///
  /// \param center
  /// \param steps
  /// \param relative
  void zoom(const QPointF& center, int steps = 0, bool relative = false);

  /// \brief Get the current zoom level
  ///
  /// \return
  int zoomLevel() const noexcept;

  /// \brief Zoom to a desired rectangle
  ///
  /// \param rect
  void zoomToRect(const QRectF);

  /// \brief Get the viewport center in scene coordinates
  ///
  /// \return
  QPointF viewSceneCenter() const;

  /// @brief Set the string format for lat/lon projections in the graticule
  ///
  /// \param format
  void setLatLonFormat(LatLonFormat format) noexcept;

  /// @brief Sets the threshold for zooming out of mapview
  /// 
  /// \param threshold
  void setZoomThreshold(double threshold);
  
  /// \brief Get the current set zoom threshold
  ///
  /// \return
  double getZoomThreshold();
  

protected:
  //
  // QGraphicsView overrides
  //
  void resizeEvent(QResizeEvent* event) override;
  void drawForeground(QPainter* painter, const QRectF& rect) override;
  void mouseMoveEvent(QMouseEvent* event) override;

private:
  QScopedPointer<MapViewPrivate> d_ptr;
};
}

#endif // DSLMAP_MAPVIEW_H
