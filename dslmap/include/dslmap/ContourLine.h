/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 1/10/19.
//

#ifndef DSLMAP_CONTOURLINE_H
#define DSLMAP_CONTOURLINE_H

#include "MapItem.h"

#include <QGraphicsPathItem>

namespace dslmap {

/// \brief Contour Line item
///
/// A thin wrapper for QGraphicsPathItem that uses QPainterPathStroker
/// to return the contour path for shape(), which in turn is used by Qt
/// for collision detection (or, selection by mouse)
///
/// NOTE:  While type() will return dslmap::MapItemType::ContourLineType,
/// using qgrpahicsitem_cast<dslmap::ContourLine*> WILL FAIL due to apparent
/// limitations in Qt.  However, using dynamic_cast<dslmap::ContourLine*> will
/// work!
class ContourLine : public QGraphicsPathItem
{
public:
  explicit ContourLine(QGraphicsItem* parent = nullptr);
  QPainterPath shape() const override;
  int type() const override { return MapItemType::ContourLineType; }
  void setContourLevel(qreal value) noexcept;
  qreal contourLevel() const noexcept;
};
}

#endif // DSLMAP_CONTOURLINE_H
