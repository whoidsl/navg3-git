/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef DSLMAP_SHAPEMARKERSYMBOLITEM_H
#define DSLMAP_SHAPEMARKERSYMBOLITEM_H

#include "MarkerSymbol.h"

#include <QGraphicsItem>
#include <QPainterPath>
#include <memory>

class QSettings;

namespace dslmap {

enum class Unit;
class ShapeMarkerSymbolPrivate;
class MarkerItem;

/// \brief A symbol to be drawn on the map.
///
/// **This class is NOT meant placed on the map itself.  You want MapItem for
/// that**
///
/// The MapItemSymbol and MapItem classes are closely related.  The
/// MapItemSymbol class handles the task of drawing a mark on the map.  The
/// MapItem class handles the unique properties of each marker, such as the
/// label text and extra attributes.  Typically one MapItemSymbol object is
/// shared by *many* MapItem items, allowing quick updates to a whole group
/// of markers on the map by changing a single MapItemSymbol instance.
///
/// The important method of this class is MapItemSymbol::renderItem, which
/// handles drawing the provided MapItem instance.
///
class DSLMAPLIB_EXPORT ShapeMarkerSymbol : public MarkerSymbol
{
  Q_OBJECT
  Q_DECLARE_PRIVATE(ShapeMarkerSymbol)
  Q_PROPERTY(qreal size READ size WRITE setSize NOTIFY sizeChanged)

public:
  enum class Shape
  {
    Circle = 0,
    Square,
    Diamond,
    Cross,
    X,
    Arrow,
    Invalid,
  };

  Q_ENUM(Shape);

  //
  // Constructors
  //

  explicit ShapeMarkerSymbol(QObject* parent = Q_NULLPTR);

  /// \brief Construct a MapItemSymbol using the provided path.
  explicit ShapeMarkerSymbol(Shape shape, double size = 10,
                             QObject* parent = Q_NULLPTR);

  ~ShapeMarkerSymbol() override;

  bool operator==(const ShapeMarkerSymbol& other) const;
  bool operator!=(const ShapeMarkerSymbol& other) const;

  const QPainterPath& path() const override;

  std::unique_ptr<ShapeMarkerSymbol> clone() const;

  //
  // Public Methods
  //

  /// \brief Get the marker shape
  ///
  /// \return
  Shape shape() const;

  /// \brief Get the desired drawn size of the symbol
  ///
  /// The size units (meters, pixels, etc.) are determined by the
  /// MarkerItem instance
  ///
  /// \param size
  qreal size() const;

  static QPainterPath pathFromShape(Shape shape, double size);
  static Shape shapeFromString(QString shape);
  static std::unique_ptr<ShapeMarkerSymbol> fromSettings(
    const QSettings& settings, QString prefix = "");

public slots:

  /// \brief Set the marker shape and size
  ///
  ///
  /// The size units (meters, pixels, etc.) are determined by the
  /// MarkerItem instance
  ///
  /// size values <= 0 are ignored.
  /// \param shape
  /// \param size
  void setShape(Shape shape, double size);

  /// \brief Set the marker shape
  ///
  ///
  /// \param shape
  void setShape(Shape shape);

  /// \brief Set the desired drawn size of the symbol
  ///
  /// The size units (meters, pixels, etc.) are determined by the
  /// MarkerItem instance
  ///
  /// size values <= 0 are ignored.
  /// \param size
  void setSize(qreal size);

  void saveSettings(QSettings& settings, QString prefix) override;

signals:

  ///@brief Emitted whenever the symbol shape changes.
  void shapeChanged(Shape shape);

  ///@brief Emitted whenever the symbol size changes.
  void sizeChanged(double size);

protected:
  bool isEqual(const ShapeMarkerSymbol& other) const;

private:
  Q_PROPERTY(Shape shape READ shape WRITE setShape NOTIFY shapeChanged)
  QScopedPointer<ShapeMarkerSymbolPrivate> d_ptr;
};

const QString toString(ShapeMarkerSymbol::Shape shape);
}
#endif // DSLMAP_SHAPEMARKERSYMBOLITEM_H
