/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 9/18/18.
//

#ifndef NAVG_PREVIEWSCRIBBLEWidget_H
#define NAVG_PREVIEWSCRIBBLEWidget_H

#include <QColor>
#include <QPainterPath>
#include <QPen>
#include <QWidget>

namespace dslmap {

/// \brief preview visual updates
///
/// This Widget is designed to display a preview of
/// visual property updates on a black background.
///

class PreviewScribbleWidget : public QWidget
{
  Q_OBJECT
public:
  explicit PreviewScribbleWidget(QWidget* parent);
  ~PreviewScribbleWidget();
  QSize minimumSizeHint() const override;
  QSize sizeHint() const override;

protected:
  void paintEvent(QPaintEvent* event) override;

public:
  void setPainterPath(QPainterPath path);
  void setPen(QPen pen);

  void setColor(QColor color);
  void setLabelVisible(bool visible);
  void setFillVisible(bool visible);
  void setPathVisible(bool visible);
  void setLabelColor(QColor color);
  void setLabelSize(qreal points);
  void setFillColor(QColor color);

private:
  QPen m_pen;
  QPainterPath m_path;
  QPen m_label_pen;
  QBrush m_brush;
  qreal m_fontSizePoints;
};
}

#endif // NAVG_PREVIEWSCRIBBLEWidget_H
