/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 9/21/17.
//

#ifndef DSLMAP_MARKERITEM_H
#define DSLMAP_MARKERITEM_H

#include "DslMap.h"
#include "MapItem.h"
#include "ShapeMarkerSymbol.h"

#include <QGraphicsItem>
#include <QSharedPointer>
#include <QStaticText>

namespace dslmap {

/// \brief An item drawn on the map using a MapItemSymbol
///
/// **This class does NOT have the actual symbol that is drawn on the
/// map.  You want MapItemSymbol for that***
///
/// The MapItemSymbol and MapItem classes are closely related.  The
/// MapItemSymbol class handles the task of drawing a mark on the map.  The
/// MapItem class handles the unique properties of each marker, such as the
/// label text and extra attributes.  Typically one MapItemSymbol object is
/// shared by *many* MapItem items, allowing quick updates to a whole group
/// of markers on the map by changing a single MapItemSymbol instance.
///
/// The MapItem class does not draw it's own symbol, that task is delegated
/// to the MapItemSymbol object assigned with the MapItem::setSymbol function.
class DSLMAPLIB_EXPORT MarkerItem : public QGraphicsItem
{

public:
  MarkerItem(QGraphicsItem* parent = Q_NULLPTR);

  /// \brief Construct a MapItem using the provided symbol object.
  MarkerItem(std::shared_ptr<ShapeMarkerSymbol>& symbol,
             QGraphicsItem* parent = Q_NULLPTR);

  ~MarkerItem() override;
  /// \brief Symbol attributes
  ///
  /// Symbol attributes are stored as QString/QVariant pairs.  They are
  /// unique to each MapItem object and can be used to store metadata.
  using Attributes = QVariantMap;

  MarkerItem* clone();
  /// \brief Set the drawn symbol for this item
  ///
  /// The Symbol is passed as a shared pointer because we are not using Qt's
  /// parent/child relationship here, but we do need to handle memory book
  /// keeping.
  ///
  /// \param symbol
  void setSymbol(std::shared_ptr<MarkerSymbol> symbol);

  /// \brief Get the MapSymbolItem for this item.
  ///
  /// \return
  std::shared_ptr<MarkerSymbol> symbol() const;

  /// \brief Return a full copy of the marker attributes
  ///
  /// \return MapItem attributes
  Attributes attributes() const;

  /// \brief Clear marker attributes
  void clearAttributes();

  /// \brief Add an attribute, or change an existing entry.
  void setAttribute(const QString& name, QVariant value);

  /// \brief Add multiple attributes, or change exiting entries
  ///
  /// \param attributes
  /// \param clear_existing  Clear existing attributes before assigning new
  /// ones.
  void setAttribute(const Attributes& attributes, bool clear_existing = true);

  /// \brief Remove an attribute.
  void removeAttribute(const QString& name);

  /// \brief Remove multiple attributes
  void removeAttribute(const QStringList& names);

  /// \brief Return a string to display in tool tips.
  ///
  /// The default implementation returns a series of 'key: value' pairs
  /// separated by newlines.
  /// \return
  virtual QString formatAttributes();

  //
  //  Label Methods
  //

  QStaticText& label();

  /// \brief Get a copy of the label text.
  ///
  /// \return Label string
  QString labelText() const;

  /// \brief Set the marker label text.
  ///
  /// An empty string clears the label
  ///
  /// \param label
  void setLabelText(const QString& label = QString());

  //
  // QGraphicsItem overrides
  //

  QRectF boundingRect() const override;

  void paint(QPainter* painter, const QStyleOptionGraphicsItem* option,
             QWidget* widget) override;

  int type() const override { return MapItemType::MarkerItemType; }

protected:
private:
  QStaticText m_label;                    ///< Label text item
  std::shared_ptr<MarkerSymbol> m_symbol; ///< The symbol to use

  Attributes m_attributes; ///< Our attributes
};
}

#endif // DSLMAP_MARKERITEM_H
