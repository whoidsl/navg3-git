/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 9/18/18.
//

#ifndef NAVG_SYMBOLPROPERTIESWIDGET_H
#define NAVG_SYMBOLPROPERTIESWIDGET_H

#include "ShapeMarkerSymbol.h"
#include <QWidget>

namespace dslmap {
namespace Ui {
class SymbolPropertiesWidget;
}

///
/// Widget for editing ShapeMarkerSymbols
/// Emits many signals, which need to be externally connected
/// to the symbol container (e.g. SymbolLayer)
///
class DSLMAPLIB_EXPORT SymbolPropertiesWidget : public QWidget
{
  Q_OBJECT
public:
  explicit SymbolPropertiesWidget(QWidget* parent = 0);
  ~SymbolPropertiesWidget();

  /// \brief Set optional title for clarity
  void setName(QString title);

  /// Setters allow incoming conditions to initialize correctly.
  /// \brief set label visibility
  void setLabelVisible(bool visible);

  /// \brief set fill visible/invisible
  void setFillVisible(bool visible);

  /// \brief set outline visible/invisible
  void setOutlineVisible(bool visible);

  /// \brief set symbol outline color
  void setOutlineColor(QColor color);

  /// \brief set symbol fill brush color
  void setFillColor(QColor color);

  /// \brief set label text color
  void setLabelColor(QColor color);

  /// \brief set symbol size
  void setSymbolSize(qreal size);

  /// \brief set label size
  void setLabelSize(qreal points);

  /// \brief set symbol shape
  void setShape(ShapeMarkerSymbol::Shape shape);

  /// \brief set Painter Path for visualizer
  void setPainterPath(QPainterPath path);

private:
  /// \brief Widgets and layout to interface with
  Ui::SymbolPropertiesWidget* ui;

  /// \brief Current size of symbol (pixels)
  qreal m_symbolSize;

  /// \brief Current size of label (pixels)
  qreal m_labelSize;

  /// \brief current shape of symbol marker
  ShapeMarkerSymbol::Shape m_shape;

  /// \brief Adds shape enum values to QComboBox
  void setupShape();

  /// \brief Creates signal/slot connections for clarity
  void setupConnections();

signals:
  /// signals propagate edits to symbol appearances

  /// \brief visibility of a component changed
  void labelVisibleChanged(const bool visible);
  void fillVisibleChanged(const bool visible);
  void outlineVisibleChanged(const bool visible);

  /// \brief color of a component changed
  void outlineColorChanged(const QColor color);
  void fillColorChanged(const QColor color);
  void labelColorChanged(const QColor color);

  /// \brief label size changed
  void labelSizeChanged(qreal size);

  /// \brief symbol shape and/or size changed
  void shapeChanged(ShapeMarkerSymbol::Shape shape, qreal size);
  void pathChanged(QPainterPath path);
};
} // namespace

#endif // NAVG_SYMBOLPROPERTIESWIDGET_H
