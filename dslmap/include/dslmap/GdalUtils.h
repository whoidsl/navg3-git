/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 10/16/17.
//

#ifndef NAVG_GDALUTILS_H
#define NAVG_GDALUTILS_H

#include "DslMap.h"
#include <QLoggingCategory>
#include <QSize>

namespace dslmap {

Q_DECLARE_LOGGING_CATEGORY(dslmapGdal)

/// \brief Generate a polygon from the provided size and GDAL transform.
///
/// This method can be used to generate a corresponding perspective
/// transformation using QTransform::squareToQuad()
///
/// GDAL defines the geometry tranform thus:
///
/// Fetches the coefficients for transforming between pixel/line (P,L) raster
/// space, and projection coordinates (Xp,Yp) space.
///
///    Xp = adfTransform[0] + P*adfTransform[1] + L*adfTransform[2];
///    Yp = adfTransform[3] + P*adfTransform[4] + L*adfTransform[5];
///
/// \param size           Size of the image in P,L units.
/// \param adfTransform   adfTransform vector
/// \return  An open QPolygonF with four vertices.
DSLMAPLIB_EXPORT QPolygonF
adfProjectionToPolygon(const QSize& size, const QVector<double>& adfTransform);

/// \brief Generate a perspective transform from the provided size and GDAL
/// transform.
///
/// The 'adfTransformation' vector is defined as:
///
///   Xp = adfTransform[0] + P*adfTransform[1] + L*adfTransform[2];
///   Yp = adfTransform[3] + P*adfTransform[4] + L*adfTransform[5];
///
///  where P is pixel number and L is line number of a raster.
///
/// If the provided vector contains less than 6 elements, `ok` will be set
/// false.
///
/// \param size            Size of the image in P,L units
/// \param adfTransform    adfTransformation vector.
/// \param ok
/// \return
DSLMAPLIB_EXPORT QTransform
gdalGeometryToTransform(const QVector<double>& adfTransform, bool* ok);

/// \brief Generate an 'adfGeometryTransform' vector from a perspective polygon.
///
/// This is the inverse of adfProjectionToPolygon(), converting an open
/// QPolygonF of 4 vertices (the 4 corners of the projected plane) to a
/// 6-element adfTransform vector.
///
/// This function can fail if the provided QPolygonF cannot be mapped to a
/// unit square.  The returned QVector will be empty in this case.
///
/// \param size
/// \param polygon
/// \return
DSLMAPLIB_EXPORT QVector<double> polygonToAdfProjection(
  const QSize& size, const QPolygonF& polygon);

/// \brief Generate a perspective transform from the provided polygon
///
/// This is the equivalent of gdalGeometryToTransform() using a bounding
/// polygon instead of the adfProjection vector.
///
/// This is also equivalent to just calling QTransform::quadToSquare() on the
/// given polygon.
///
/// \param size       Rectangular size of the raster in (Pixel,Line) coords
/// \param polygon    Physical size of raster in projection space.
/// \param ok
/// \return
DSLMAPLIB_EXPORT QTransform polygonTransform(const QSize& size,
                                             const QPolygonF& polygon,
                                             bool* ok = nullptr);
}

#endif // NAVG_GDALUTILS_H
