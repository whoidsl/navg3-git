/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 7/31/18.
//

#ifndef DSLMAP_MARKERSYMBOL_H
#define DSLMAP_MARKERSYMBOL_H

#include "DslMap.h"
#include "GdalColorGradient.h"

#include <QColor>
#include <QObject>
#include <QPainterPath>
#include <memory>

class QPainter;
class QStyleOptionGraphicsItem;
class QSettings;

namespace dslmap {

class MarkerSymbolPrivate;
class MarkerItem;
class MapScene;
enum class Unit;

class DSLMAPLIB_EXPORT MarkerSymbol : public QObject
{
  Q_OBJECT
  Q_DECLARE_PRIVATE(MarkerSymbol)

  ///@brief Show or hide label text.
  Q_PROPERTY(bool labelVisible READ isLabelVisible WRITE setLabelVisible NOTIFY
               labelVisibleChanged)

  ///@brief The symbol's minimum drawn size
  Q_PROPERTY(qreal minimumSize READ minimumSize WRITE setMinimumSize NOTIFY
               minimumSizeChanged)

  ///@brief The symbol's label (pen) color
  Q_PROPERTY(QColor labelColor READ labelColor WRITE setLabelColor NOTIFY
               labelColorChanged)

  ///@brief The symbol's label (font) size
  Q_PROPERTY(qreal labelSize READ labelSize WRITE setLabelSize NOTIFY
               labelSizeChanged)

  ///@brief The symbol's outline (pen) style
  Q_PROPERTY(Qt::PenStyle outlineStyle READ outlineStyle WRITE setOutlineStyle
               NOTIFY outlineStyleChanged)

  ///@brief The symbol's outline (pen) width
  Q_PROPERTY(qreal outlineWidth READ outlineWidth WRITE setOutlineWidth NOTIFY
               outlineWidthChanged)

  ///@brief The symbol's fill (brush) style
  Q_PROPERTY(Qt::BrushStyle fillStyle READ fillStyle WRITE setFillStyle NOTIFY
               fillStyleChanged)

  ///@brief The symbol's units
  Q_PROPERTY(Unit units READ units WRITE setUnits NOTIFY unitsChanged)

  ///@brief Whether the symbol is drawn invariant to map scale
  Q_PROPERTY(bool scaleInvariant READ isScaleInvariant WRITE setScaleInvariant
               NOTIFY scaleInvariantChanged)

  ///@brief Whether to use solid colors or a gradient color mapping
  Q_PROPERTY(bool use_color_gradient READ useColorGradient WRITE
               setUseColorGradient NOTIFY useColorGradientChanged)

  ///@brief The symbol's outline (pen) color
  Q_PROPERTY(QColor solid_outline_color READ outlineColor WRITE setOutlineColor
               NOTIFY outlineColorChanged)

  ///@brief The symbol's fill (brush) color
  Q_PROPERTY(QColor solid_fill_color READ fillColor WRITE setFillColor NOTIFY
               fillColorChanged)

  ///@brief Which preset colormap to use
  Q_PROPERTY(
    GdalColorGradient::GradientPreset gradient_preset READ gradientPreset WRITE
      setGradientPreset NOTIFY gradientPresetChanged)

  ///@brief Which of the item's attributes' value to use for looking up color
  Q_PROPERTY(QString gradient_attribute READ gradientAttribute WRITE
               setGradientAttribute NOTIFY gradientAttributeChanged)

  ///@brief Value mapping to minimum of gradient's range
  Q_PROPERTY(qreal gradient_min READ gradientMin WRITE setGradientMin NOTIFY
               gradientMinChanged)

  ///@brief Value mapping to maximum of gradient's range
  Q_PROPERTY(qreal gradient_max READ gradientMax WRITE setGradientMax NOTIFY
               gradientMaxChanged)

  friend class MarkerItem;

public:
  explicit MarkerSymbol(QObject* parent = Q_NULLPTR);
  ~MarkerSymbol() override;

  bool operator==(const MarkerSymbol& other) const;
  bool operator!=(const MarkerSymbol& other) const;

  virtual /// \brief Render a MapItem using this symbol.
    ///
    /// Renders a MapItem using this symbol.  This method is called from
    /// MapItem::paint to handle actually drawing the symbol on the map.
    ///
    /// \param painter
    /// \param option
    /// \param widget
    void
    renderItem(MarkerItem* item, QPainter* painter,
               const QStyleOptionGraphicsItem* option, QWidget* widget);

  /// \brief Returns the item's path as a QPainterPath.
  ///
  /// If no item has been set, an empty QPainterPath is returned.
  virtual const QPainterPath& path() const = 0;

  /// \brief Return whether the symbol is using GdalColorGradient.
  bool useColorGradient() const;

  /// \brief Return name of attribute used for the color gradient.
  QString gradientAttribute() const;

  /// \brief Return which predefined color gradient is used to map value to
  /// color.
  GdalColorGradient::GradientPreset gradientPreset() const;

  /// \brief Return the minimum range for the color gradient.
  ///
  /// Any plotted attributes with values below this will saturate.
  /// \return qreal min
  qreal gradientMin() const;

  /// \brief Return the maximum range for the color gradient.m
  ///
  /// Any plotted attributes with values above this will saturate.
  /// \return qreal max
  qreal gradientMax() const;
  std::shared_ptr<GdalColorGradient> gradient();

  // Used for equality testing of the gradient.
  std::shared_ptr<GdalColorGradient const> gradient() const;

  /// \brief Get the color used for drawing the symbol outline
  QColor outlineColor() const;

  /// \breif Get the outline width
  qreal outlineWidth() const;

  /// \brief Get the outline style.
  Qt::PenStyle outlineStyle() const;

  /// \brief Get the color used for filling the symbol shape
  QColor fillColor() const;

  /// \brief Get the fill style
  Qt::BrushStyle fillStyle() const;

  /// \brief Get the color used for writing label text
  QColor labelColor() const;

  /// \brief Get the size used for writing label text
  qreal labelSize() const;

  /// \brief Get the label alignment;
  ///
  /// \return
  Qt::Alignment labelAlignment() const;

  /// \brief Get the minimum drawn size of the symbol.
  ///
  /// \return
  qreal minimumSize() const;

  /// \brief Get the label visibility.
  ///
  /// \return
  bool isLabelVisible() const;

  /// \brief Get the units for the symbol.
  ///
  /// The symbol units are used to set the drawn size within the MapView.
  /// If the symbol is scale invariant (isScaleInvariant() returns true), the
  /// units are ignored and the shape is drawn at the same size regardless of
  /// map scale.
  /// \return
  Unit units() const;

  /// \brief Set the symbol scale invariant.
  ///
  /// Scale-invariant symbols are drawn the same size regardless of map view
  /// scale.
  /// This is useful for non-physical markers (such as trails).
  ///
  /// \return
  bool isScaleInvariant() const;

  /// \brief Save symbol settings
  ///
  /// Saves the following key/value pairs:
  ///
  /// outline/color:   outline color
  /// outline/width:   outline width
  /// outline/style:
  /// fill/color:
  /// fill/style:
  /// label/color:
  /// label/visible:
  /// label/alignment:
  /// scale_invariant:
  /// minimum_size:
  /// use_gradient:
  /// gradient/preset:
  /// gradient/attribute:
  /// gradient/min:
  /// gradient/max:
  /// \param settings
  /// \param prefix
  virtual void saveSettings(QSettings& settings, QString prefix);
  void saveSettings(QSettings& settings);

  void loadSettings(const QSettings& settings, const QString& prefix = "");

  QPen outlinePen() const noexcept;
  QPen labelPen() const noexcept;
  QBrush fillBrush() const noexcept;
public slots:

  /// \brief Set label visibility
  ///
  /// \param visible
  void setLabelVisible(bool visible);

  /// \brief Set the minimum drawn size of the symbol.
  ///
  /// \param size
  void setMinimumSize(qreal size);

  /// \brief Set the text color for labels
  void setLabelColor(const QColor& color);

  /// \brief Set the text size for labels
  void setLabelSize(qreal points);

  /// \brief Set the label position
  ///
  /// \param alignment
  void setLabelAlignment(Qt::Alignment alignment);

  /// \brief Set the brush style.
  void setFillStyle(Qt::BrushStyle style);

  /// \brief Set the fill color for symbols
  void setFillColor(const QColor& color);

  /// \brief Set the pen style
  void setOutlineStyle(Qt::PenStyle style);

  /// \biref Set the color used for drawing symbol outlines.
  void setOutlineColor(const QColor& color);

  /// \brief Set the outline stroke width (in pixels)
  void setOutlineWidth(qreal width);

  void setUnits(Unit unit);

  void setScaleInvariant(bool invariant);

  void setUseColorGradient(bool use_gradient);
  void setGradientPreset(GdalColorGradient::GradientPreset preset);

  // NOTE(LEL): Using these setters will NOT cause an already-open
  //  GdalcolorGradientPropertiesWidge embedded in the
  //  SymbolLayerPropertiesWidget to update its displayed values to match.
  //  They're intended for initial configuration of the gradient, and then are
  //  triggered by the properties widget.

  /// \brief Set which attribute the symbols are colored based on
  void setGradientAttribute(QString attribute);
  /// \brief Set minimum of range that's mapped to a color
  void setGradientMin(qreal min);
  /// \brief Set maximum of range that's mapped to a color
  void setGradientMax(qreal max);
  /// \brief Set range for mapping to color
  void setGradientRange(qreal min, qreal max);

signals:

  /// @brief Emitted whenever the symbol label visiblity changes.
  void labelVisibleChanged(bool visible);

  /// @brief Emitted whenever the symbol minimum size changes.
  void minimumSizeChanged(qreal size);

  /// @brief Emitted whenever a symbol property changes.
  void symbolChanged();

  void labelColorChanged(QColor color);

  void labelSizeChanged(qreal points);

  void outlineStyleChanged(Qt::PenStyle);

  void fillStyleChanged(Qt::BrushStyle);

  void unitsChanged(Unit unit);

  void scaleInvariantChanged(bool invariant);

  void outlineWidthChanged(qreal width);

  void useColorGradientChanged(bool use_gradient);
  void outlineColorChanged(QColor color);
  void fillColorChanged(QColor color);
  void gradientAttributeChanged(QString attribute);
  void gradientPresetChanged(GdalColorGradient::GradientPreset preset);
  void gradientMinChanged(qreal min);
  void gradientMaxChanged(qreal max);
  void gradientRangeChanged(qreal min, qreal max);

protected:
  /// \brief Clone paint settings from another symbol.
  ///
  /// Clones the following from the other symbol:
  ///
  /// - Pen
  /// - Brush
  /// - Label Pen
  /// - Label Size
  /// - Label Alignment
  /// - Label visibility
  ///
  /// \return
  void m_clonePaintSettingsFrom(const MarkerSymbol& other);

  void setMapScene(MapScene* scene);

  void removeItem(MarkerItem* item);
  void addItem(MarkerItem* item);
  MapScene* mapScene() const;

  bool isEqual(const MarkerSymbol& other) const;

private:
  /// \brief Update the pen/brush colors before rendering item
  ///
  /// This enables changing colors based on attributes
  void updateColors(MarkerItem* item);

  QScopedPointer<MarkerSymbolPrivate> d_ptr;
};
}
#endif // DSLMAP_ABSTRACTMARKERSYMBOL_H
