/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 9/20/17.
//
#ifndef DSLMAP_UTIL_H
#define DSLMAP_UTIL_H

#include "DslMap.h"

#include <QPainterPath>

#define RADIANS		(57.295779513082320876798154814105)		/* degrees/radian */
#define RTOD         RADIANS
#define DTOR         (1.0/RTOD)

namespace dslmap {

#if 0
/// \brief Generate a QPainterPath for one of the default shapes.
///
/// Using SymbolShape::None or SymbolShape::Custom will return an invalid
/// shape (e.g. QPainterPath())
///
/// The size parameter scales the shape.  For example using size=10 with the
/// SymbolShape::Circle will return a circle with a radius of 10.
///
/// \param shape  Desired shape
/// \param size   Size of shape.
/// \return
DSLMAPLIB_EXPORT QPainterPath pathFromShape(SymbolShape shape, qreal size);
#endif

/// \brief Return a "nice" number approx. equal to x
///
///
/// From: *Badouel, D (1990) in A Glassner (ed.), Graphics Gems I.
/// Academic Press.*
///
/// \param x
/// \param round round if true, take ceiling if false
/// \return
DSLMAPLIB_EXPORT qreal niceNum(qreal x, bool round);

/// \brief Create a vector of nice labels between min and max
///
/// From: *Badouel, D (1990) in A Glassner (ed.), Graphics Gems I.
/// Academic Press.*
///
/// \param min
/// \param max
/// \param nticks
///
/// \return A pair with a vector of tick marks and a number of digits to
/// display.
DSLMAPLIB_EXPORT QPair<QVector<qreal>, int> looseLabel(qreal min, qreal max,
                                                       int nticks = 5);

DSLMAPLIB_EXPORT QPair<QPair<QVector<qreal>, int>, QPair<QVector<qreal>, int>>
graticuleTickMarks(const QRectF& rect, int nticks = 5);

/// \brief Create a QRectF extended by some percentage of its proportions
///
/// \param start_rect
/// \param percent decimal, not out of 100
/// \return A larger rect centered in the same location
QRectF extendRectByNPercent(QRectF start_rect, qreal percent = 0.2);

/// @brief Format a longitude/latitude pair into a string given the desired
/// format type
///
/// Returns a string representation of the latitude/longitude pair
///
/// \param longitude Longitude
/// \param latitude Latitude
/// \param format Format type
/// \param precision  Precision
/// \return
DSLMAPLIB_EXPORT QString formatLonLat(qreal longitude, qreal latitude,
                                      LatLonFormat format, int precision = 6);

/// @brief String representation of a Longitude value given the provided format
///
/// \param longitude
/// \param format
/// \param precision
/// \return
DSLMAPLIB_EXPORT QString formatLongitude(qreal longitude, LatLonFormat format,
                                         int precision = 6);
/// @brief String representation of a latitude value given the provided format
///
/// \param longitude
/// \param format
/// \param precision
/// \return
DSLMAPLIB_EXPORT QString formatLatitude(qreal longitude, LatLonFormat format,
                                        int precision = 6);

/// @brief  Convert either a latitude or longitude Decimal Minutes to decimal degrees
DSLMAPLIB_EXPORT double LatorLonDMToDD(int degrees, double minutes, int precision = 6);

/// @brief. Distance in meters between 2 lat lon dd points
DSLMAPLIB_EXPORT double calculateDistance(const QPointF source_fix,const QPointF dest_fix);

///@brief calculate bearing between two lat lon points
DSLMAPLIB_EXPORT double calculateBearing(const QPointF source_fix, const QPointF dest_fix);

}

#endif // DSLMAP_UTIL_H
