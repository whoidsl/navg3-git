/**
 * Copyright 2020 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef GEOREFERENCEIMAGEPROPERTIESWIDGET_H
#define GEOREFERENCEIMAGEPROPERTIESWIDGET_H

#include "ColorPickButton.h"
#include "GeoreferenceImageObject.h"
#include <QSpinBox>
#include <QWidget>
#include <QtGlobal>

namespace dslmap {

Q_DECLARE_LOGGING_CATEGORY(dslmapGeoreferenceImagePropertiesWidget)

namespace Ui {
class DSLMAPLIB_EXPORT GeoreferenceImagePropertiesWidget;
}

class GeoreferenceImage;

class GeoreferenceImagePropertiesWidget : public QWidget
{
  Q_OBJECT

public:
  explicit GeoreferenceImagePropertiesWidget(QWidget* parent = 0);
  ~GeoreferenceImagePropertiesWidget();
  
  /// \brief connects ui elements to object slots
  /// \param obj pointer to georeferenceimageobject to connect ui
  void setObject(GeoreferenceImageObject* obj);
protected slots:
  
  /// \brief checks whether image spin box values are valid
  /// sets cropbutton to enabled or disabled
  /// \param val the new value of spin box emitted by QSpinBox::valueChanged
  /// \return returns true if spinbox values in correct range. false if not
  bool validateCropText(int val);

signals:
  /// \brief signal that transparent QColor has changed
  void transColorChanged(QColor color);

  /// \brief signal thrown when crop button clicked on gui
  void imageCropReady(int, int, int, int);

private:
  Ui::GeoreferenceImagePropertiesWidget* ui;

  /// \brief Set starting values for spin boxes and connect slots
  void setupCropSpinBoxes(GeoreferenceImageObject* obj);
};
}

#endif // GEOREFERENCEIMAGEPROPERTIESWIDGET_H
