/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef NAVG_GEOREFERENCEIMAGEOBJECT_H
#define NAVG_GEOREFERENCEIMAGEOBJECT_H

#include "DslMap.h"

#include "GdalDataset.h"
#include <QGraphicsObject>
#include <QLoggingCategory>
#include <QSharedPointer>
#include <gdal/ogr_spatialref.h>
#include <gdal/gdal_priv.h>


#include <memory>

class GDALDataset;

namespace dslmap {

class GeoreferenceImageObjectPrivate;
class Proj;

Q_DECLARE_LOGGING_CATEGORY(dslmapGeoreferenceImageObject)

/// \brief A Georeferenced image item

///
class GeoreferenceImageObject : public QGraphicsObject
{
  Q_OBJECT
  Q_DECLARE_PRIVATE(GeoreferenceImageObject)

signals:

  void transColorChanged(QColor color);

public:
  enum UpdateOption
  {
    UpdateNone = 0x0,       //!< No-op
    UpdateProjection = 0x1, //!< Update to a new projection
    UpdateData = 0x2,
    UpdatePixmap = 0x4, //!< Update the composited pixmap
    UpdateTransparency,
    UpdateTransform,
    UpdateGreyScale,
    UpdateRotation,
    UpdateCrop,
    UpdateAll = UpdateProjection | UpdateData | UpdatePixmap |

                UpdateTransform
  };

  Q_DECLARE_FLAGS(UpdateOptions, UpdateOption)

  explicit GeoreferenceImageObject(QGraphicsItem* parent = nullptr);
  ~GeoreferenceImageObject() override;

  //
  // QGraphicsItem Overrides
  //
  int type() const override;
  QRectF boundingRect() const override;
  void paint(QPainter* painter, const QStyleOptionGraphicsItem* option,
             QWidget* widget) override;

  ///
  /// \return
  const Proj* datasetProjection() const;

  std::shared_ptr<const Proj> projection() const noexcept;

  const Proj* projectionPtr() const noexcept;

  /// \brief Test if d->m_image is currently set to grayscale
  ///
  /// Image properties widget checks this on opening to determine grayscale
  /// checkbox toggle
  bool getGreyScaleState();

  /// \brief Getter used to return copy of d->m_image
  QImage getQImage();

protected:
  QString getProj4StringFromDataset(std::shared_ptr<GdalDataset> dataset);

  QTransform getProjectionTransform();

public slots:
  /// \brief Load a raster dataset from disk.
  ///
  /// Open a raster bathymetry source in read-only mode.  `proj` can be used
  /// to provide a projection if the dataset lacks one (the projection
  /// will NOT be saved with the file).
  ///
  /// Returns true if the raster was succesfully loaded.
  ///
  /// \param filename Filename of dataset.
  /// \param band Raster band to create image from.
  /// \param proj Source projection override.
  /// \return
  bool setRasterFile(const QString& filename, std::shared_ptr<Proj> proj = {});

  bool setRasterFile(const QString& filename, QPointF top_left, QPointF bottom_right, std::shared_ptr<Proj> proj = {});

  /// \brief Saves transparent color to use in privateGeoimageObject
  void setTransparentColor(QColor color);

  /// \brief Set image to grayscale. Turn on or off
  /// \param state positive value toggles to grayscale
  void toggleGreyScale(int state);

  /// \brief Takes coordinates for an inner rectangle to be cropped from
  /// original dataset m_image
  ///
  /// Slot called when submitting coordinates in Geoimage properties
  void cropImage(int x0, int y0, int dx, int dy);

  /// \brief Set the projection
  ///
  /// Change the displayed projection.  (NOT the projection of the original
  /// dataset)
  ///
  /// \param proj
  void setProjection(std::shared_ptr<const Proj> proj);

  /// Update the shaded relief due to a change in the colortable.
  bool update(UpdateOptions options);

  /// \brief Testing operation to display gdal metadata from a dataset. Used for
  /// debugging
  /// \param file The name of the georeferenced image file to be operated on
  const char* getFileMetaData(const QString& file);

private:
  /// \brief Combines qimages generated from rasterIO buffer. QPixmap is set
  /// from this
  /// \param images Vector of single banded QImages
  /// \return QImage with correct QImage::Format and combined if images was >1
  QImage combineQImageBands(std::vector<QImage> images, std::vector<GDALRasterBand*> bands);
  GeoreferenceImageObjectPrivate* d_ptr;
};
}

Q_DECLARE_OPERATORS_FOR_FLAGS(dslmap::GeoreferenceImageObject::UpdateOptions)

#endif // DSLMAP_GEOREFERENCEIMAGEOBJECT_H