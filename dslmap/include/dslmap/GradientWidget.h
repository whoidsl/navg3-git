//
// Created by jvaccaro on 12/7/18.
//

#ifndef NAVG_GRADIENTWIDGET_H
#define NAVG_GRADIENTWIDGET_H

#include <QWidget>
#include <memory>

namespace dslmap {
class GdalColorGradient;

class GradientWidget : public QWidget
{
  Q_OBJECT

public:
  enum TextColor
  {
    Auto = 0,
    Black,
    White
  };
  Q_ENUM(TextColor)

  GradientWidget(QWidget* parent = nullptr);
  ~GradientWidget();

  void setGradient(std::shared_ptr<dslmap::GdalColorGradient> grad);

  void setLegendSpacing(float increment);
  float legendSpacing();

  void setDepthLabelRatio(int ratio);
  int depthLabelRatio();

  void setTextColor(TextColor color);
  TextColor textColor();

protected:
  void paintEvent(QPaintEvent* e) override;
  void mouseMoveEvent(QMouseEvent* event) override;
  void mouseDoubleClickEvent(QMouseEvent* event) override;

private:
  void setPenColor(QPainter* p);
  std::shared_ptr<dslmap::GdalColorGradient> m_gradient;
  int m_cursor_y;
  float m_legend_spacing;
  int m_depth_label_ratio;
  TextColor m_textcolor;
};

} // namespace dslmap

#endif // NAVG_GRADIENTWIDGET_H
