/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 10/27/17.
//

#ifndef NAVG_BEACONLAYER_H
#define NAVG_BEACONLAYER_H

#include "DslMap.h"
#include "MapLayer.h"
#include "MarkerItem.h"

#include <QDateTime>
#include <QLoggingCategory>

namespace dslmap {

Q_DECLARE_LOGGING_CATEGORY(dslmapBeaconLayer)

class ShapeMarkerSymbol;
class BeaconLayerPrivate;

/// \brief Tracks a movable source and leaves a trail of previous positions.
class DSLMAPLIB_EXPORT BeaconLayer : public MapLayer
{
  Q_OBJECT
  Q_DECLARE_PRIVATE(BeaconLayer)

  Q_PROPERTY(QColor beaconOutlineColor READ beaconOutlineColor WRITE
               setBeaconOutlineColor NOTIFY beaconOutlineColorChanged)
  Q_PROPERTY(QColor beaconFillColor READ beaconFillColor WRITE
               setBeaconFillColor NOTIFY beaconFillColorChanged)
  Q_PROPERTY(QColor beaconLabelColor READ beaconLabelColor WRITE
               setBeaconLabelColor NOTIFY beaconLabelColorChanged)
  Q_PROPERTY(Qt::PenStyle beaconOutlineStyle READ beaconOutlineStyle WRITE
               setBeaconOutlineStyle NOTIFY beaconOutlineStyleChanged)
  Q_PROPERTY(Qt::BrushStyle beaconFillStyle READ beaconFillStyle WRITE
               setBeaconFillStyle NOTIFY beaconFillStyleChanged)

  Q_PROPERTY(QColor trailOutlineColor READ trailOutlineColor WRITE
               setTrailOutlineColor NOTIFY trailOutlineColorChanged)
  Q_PROPERTY(QColor trailFillColor READ trailFillColor WRITE setTrailFillColor
               NOTIFY trailFillColorChanged)
  Q_PROPERTY(QColor trailLabelColor READ trailLabelColor WRITE
               setTrailLabelColor NOTIFY trailLabelColorChanged)
  Q_PROPERTY(Qt::PenStyle trailOutlineStyle READ trailOutlineStyle WRITE
               setTrailOutlineStyle NOTIFY trailOutlineStyleChanged)
  Q_PROPERTY(Qt::BrushStyle trailFillStyle READ trailFillStyle WRITE
               setTrailFillStyle NOTIFY trailFillStyleChanged)

  Q_PROPERTY(
    int capacity READ capacity WRITE setCapacity NOTIFY capacityChanged)

  Q_PROPERTY(int displayedLength READ displayedLength WRITE setDisplayedLength
               NOTIFY displayedLengthChanged)

  Q_PROPERTY(QPointF shiftOffset READ shiftOffset WRITE setShiftOffset NOTIFY
               shiftOffsetChanged)

signals:
  void currentPositionChanged(const QPointF& xy);
  void currentLonLatPositionChanged(const QPointF& lonlat);

  void trailOutlineColorChanged(QColor color);
  void trailFillColorChanged(QColor color);
  void trailLabelColorChanged(QColor color);
  void trailOutlineStyleChanged(Qt::PenStyle style);
  void trailFillStyleChanged(Qt::BrushStyle style);
  void trailVisibilityChanged(bool visible);

  void displayedLengthChanged(int length);
  void capacityChanged(int length);

  void beaconOutlineColorChanged(QColor color);
  void beaconFillColorChanged(QColor color);
  void beaconLabelColorChanged(QColor color);
  void beaconFillStyleChanged(Qt::BrushStyle style);
  void beaconOutlineStyleChanged(Qt::PenStyle style);
  void shiftOffsetChanged(QPointF shift);

public:
  explicit BeaconLayer(QGraphicsItem* parent = nullptr);
  ~BeaconLayer() override;

  // MapLayer overrides
  int type() const override { return MapItemType::BeaconLayerType; }

  // TrailObject access
  /// @brief Get the trail capacity
  ///
  /// The trail capacity is the maximum possible displayed length for the beacon
  /// trail.
  /// \return
  int capacity() const;

  QList<const MarkerItem*> constTrailMarkers();
  /// \brief for Navest layer sorting purposes, store whether the fix source 
  /// for this layer is an external fix. Return this value
  bool isExternalFix();

  void setAsExternalFix(bool fix);

  /// @brief Get the displayed length of the trail
  ///
  /// This is the number of points drawn for the trail.  It can be between 0 and
  /// the value
  /// returned by `capacity()`.  For a length of `N`, the `N` most recent points
  /// will be drawn.
  ///
  /// The special value of -1 will show all possible points.
  ///
  /// \return
  int displayedLength() const;

  /// @brief Return number of trail markers in layer
  ///
  /// \return
  int count() const noexcept;

  /// \brief Get the trail's MapItemSymbol
  ///
  /// The layer retains ownership of the symbol.
  ///
  /// \return
  std::shared_ptr<MarkerSymbol> trailSymbol() const;
  MarkerSymbol* trailSymbolPtr() const;

  /// \brief Return the visibility of the beacon marker.
  ///
  /// \param visible
  bool trailVisible() const noexcept;

  QColor trailOutlineColor() const;

  QColor trailFillColor() const;

  Qt::PenStyle trailOutlineStyle() const;

  Qt::BrushStyle trailFillStyle() const;

  QColor trailLabelColor() const;

  bool trailLabelVisible() const;

  /// \brief Get the current position symbol.
  ///
  /// return
  std::shared_ptr<MarkerSymbol> beaconSymbol() const;
  MarkerSymbol* beaconSymbolPtr() const;

  /// \brief Return the visibility of the beacon marker.
  ///
  /// \param visible
  bool beaconVisible() const noexcept;

  QColor beaconOutlineColor() const;

  QColor beaconFillColor() const;

  Qt::PenStyle beaconOutlineStyle() const;

  Qt::BrushStyle beaconFillStyle() const;

  QColor beaconLabelColor() const;

  bool beaconLabelVisible() const;

  /// @brief Save layer settings to a QSettings object
  ///
  /// Creates the following keys in the provided settings object:
  ///
  /// * `name`:              Name of layer
  /// * `marker_color`:      Color of the beacon symbol
  /// * `trail_color`:       Color of the trail symbol
  /// * `capacity`:          Trail capacity
  /// * `length`:            Displayed trail length
  ///
  /// \param settings
  /// \return
  bool saveSettings(QSettings& settings) override;

  bool loadSettings(QSettings& settings) override;

  /// @brief Create a new layer using values from the provided QSettings object.
  ///
  /// Creates a new layer using the following keys in the provided settings
  /// object:
  ///
  /// * `name`:              Name of layer
  /// * `marker_color`:      Color of the beacon symbol
  /// * `trail_color`:       Color of the trail symbol
  /// * `capacity`:          Trail capacity
  /// * `length`:            Displayed trail length
  ///
  /// \param settings
  /// \return
  static std::unique_ptr<BeaconLayer> fromSettings(QSettings& settings);

  void setProjection(std::shared_ptr<const Proj> proj) override;
  QRectF boundingRect() const override;

  void paint(QPainter* painter, const QStyleOptionGraphicsItem* option,
             QWidget* widget) override;

  /// @brief Return the current position of the beacon in projected coordinates
  ///
  /// \return
  QPointF currentPosition() const noexcept;

  /// @brief Return the current position of the beacon in Geodetic coordinates.
  ///
  /// \return
  QPointF currentLonLatPosition() const noexcept;

  /// @brief Return the current headingof the beacon
  qreal currentHeading() const noexcept;

  /// @brief Shift offset in meters for display purposes
  QPointF shiftOffset() const noexcept;

public slots:
  void showPropertiesDialog() override;

  /// @brief Clear fix history trail.  Current position remains unchanged.
  ///
  void clearTrail();

  /// \brief Update the beacon location from a lon/lat coordinate
  ///
  /// The current location will be added to the beacon trail before the
  /// symbol is moved to the new location.
  ///
  /// This method will convert geographic coordinates (longitude/latitude) to
  /// whatever projection is being used by the layer.
  ///
  /// \param lon
  /// \param lat
  /// \param timestamp
  /// \param heading
  /// \param label
  /// \param attributes
  void updatePositionLonLat(qreal lon, qreal lat,
                            const QDateTime& timestamp = {},
                            const qreal* heading = nullptr,
                            const QString& label = {},
                            const MarkerItem::Attributes& attributes = {});

  /// \brief Update the beacon location using the native projection
  ///
  /// The current location will be added to the beacon trail before the
  /// symbol is moved to the new location.
  ///
  /// This method assumes the new position is in the layer's current
  /// projection coordinates.  If you want to add a longitude/latitude pair,
  /// use BeaconLayer::updatePositionLonLat().
  ///
  /// \param x
  /// \param y
  /// \param timestamp
  /// \param heading
  /// \param label
  /// \param attributes
  void updatePosition(qreal x, qreal y, const QDateTime& timestamp = {},
                      const qreal* heading = nullptr, const QString& label = {},
                      const MarkerItem::Attributes& attributes = {});

  /// \brief Update the current heading.
  ///
  /// \param heading
  void updateHeading(qreal heading);

  /// \brief Set the maximum number of points held in the trail.
  ///
  /// \param capacity
  void setCapacity(int capacity);

  /// \brief Set how many points are shown.
  ///
  /// All available points are shown when this value is -1
  ///
  /// \param length
  void setDisplayedLength(int length);

  /// \brief Update the map symbol used for the current beacon position.
  ///
  /// \param symbol
  void setBeaconSymbol(std::shared_ptr<MarkerSymbol> symbol);

  /// \brief Change the beacon visibility
  ///
  /// \param visible
  void setBeaconVisible(bool visible);

  /// \brief Update the map symbol used for the beacon trail
  ///
  /// \param symbol
  void setTrailSymbol(std::shared_ptr<MarkerSymbol> symbol);

  /// \brief Change beacon trail visibility
  ///
  /// \param visible
  void setTrailVisible(bool visible);

  void setTrailOutlineColor(const QColor& color);

  void setTrailFillColor(const QColor& color);

  void setTrailLabelColor(const QColor& color);

  void setTrailLabelVisible(const bool visible);

  void setTrailFillStyle(Qt::BrushStyle style);

  void setTrailOutlineStyle(Qt::PenStyle style);

  void setBeaconOutlineColor(const QColor& color);

  void setBeaconFillColor(const QColor& color);

  void setBeaconFillStyle(Qt::BrushStyle style);

  void setBeaconOutlineStyle(Qt::PenStyle style);

  void setBeaconLabelColor(const QColor& color);

  void setBeaconLabelVisible(const bool visible);

  void setShiftOffset(const QPointF shift);

private:
  BeaconLayerPrivate* d_ptr;
};
}

#endif // NAVG_BEACONLAYER_H
