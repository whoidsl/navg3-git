/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef TARGETTABLEMODEL_H
#define TARGETTABLEMODEL_H
#include <QAbstractTableModel>
#include <QString>
#include <QVector>

// TODO:  Put class in dslmap namespace
namespace dslmap {
namespace Ui {
class TargetTableModel;
}

/// \brief Table model from QVector array of QStrings
/// Data can be extracted by user as a QString or as a qreal.
/// Created for use in dslmap::TargetImport tableview preView.
/// \param tablemodel
class TargetTableModel : public QAbstractTableModel
{
  Q_OBJECT

public:
  TargetTableModel(const int numColumns, const int numRows,
                   QVector<QVector<QString>> data, QObject* parent = nullptr);

  /// \brief returns number of rows to View
  int rowCount(const QModelIndex& parent) const override;

  /// \brief returns number of columns to View
  int columnCount(const QModelIndex& parent) const override;

  /// \brief returns data at index to View
  QVariant data(const QModelIndex& index, int role) const override;

  /// \brief returns number of rows to user
  int getRow();

  /// \brief returns number of columns to user
  int getCol();

  /// \brief returns qreal casted from QString at given index, sets bool to true
  /// if cast successful
  /// Used in TableView::ExtractData()
  /// \param real
  qreal getRealAt(const int row, const int column, bool* ok = nullptr);

  /// \brief returns QString at given index
  /// Used in TableView::ExtractData()
  /// \param string
  QString getStringAt(const int row, const int column);

private:
  int m_numRows;
  int m_numColumns;
  QVector<QVector<QString>> m_data;
};
} // END NAMESPACE DSLMAP
#endif // TARGETTABLEMODEL_H
