/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef TARGETIMPORT_H
#define TARGETIMPORT_H
#include "Proj.h"
#include "TargetTableModel.h"
#include <QAbstractTableModel>
#include <QCheckBox>
#include <QDebug>
#include <QDialog>
#include <QFile>
#include <QFileDialog>
#include <QFormLayout>
#include <QHash>
#include <QHeaderView>
#include <QPointer>
#include <QString>
#include <QStringList>
#include <QStringListModel>
#include <QVector>

namespace dslmap {
class MapScene;
class SymbolLayer;
namespace Ui {
class TargetImport;
}

// TODO:  General notes:
//  - Choose a naming convention and stick with it.  (Rest of the library is
//    using 'lowerCameCase' to follow Qt conventions)
//    (see FDOpen, getLayerName)

/// \brief dialog for importing location targets from text file with labels
///
/// Users will be prompted to select a text delimited file with location data,
/// then update a table preview. Users must update preview before accepting
/// data.
///
/// Users may specify which columns represent which data, and opt to ignore
/// header rows.
/// They may also create a layer title for when the targets get imported.
///
/// \param target-import
class TargetImport : public QDialog
{
  Q_OBJECT

public:
  explicit TargetImport(MapScene& scene, QWidget* parent = nullptr);
  ~TargetImport();

  explicit TargetImport(MapScene& scene, int lon_col, int lat_col, QString file_string = "", QString layer_name = "", QWidget* parent = nullptr);

  /// \brief exports point information to target layer
  /// Called when TargetImport window dialog "ok" clicked
  /// Reformats the X and Y data from strings to points
  /// Packs up attribute QVariantMap
  /// \param points
  QList<std::tuple<QPointF, QString, QVariantMap>> extractXY();

  std::shared_ptr<Proj> extractProj();

  /// \brief gets layer name for layer paint
  QString getLayerName();
  
  //allow the layer to be extracted from here.
  // all the layer logic is stuck in here
  dslmap::SymbolLayer* targetLayer();
  
  //used for autoloading of target underlay
  bool isInUnderlayFormatting();

protected:
  /// \brief adds attribute line to widget
  void addAttributeLine();

  /// \brief deletes last attrbute line from widget
  void delAttributeLine();

  /// \brief toggles Label column spin box
  void toggleColumn();

  /// \brief accesses attribute pairs of "name", column
  /// Called within extractXY()
  /// \param attributes
  QMap<QString, int> getAPairs();

  /// \brief Allows user to select a file.
  void chooseFile();

  /// \brief updates table preview of target data
  /// Called on preview button click
  /// Opens and parses file
  /// Calls: ReadColumns()
  ///        UpdateTableModel()
  /// \param preview
  bool preview();

  /// \brief updates delimiter for target file
  void delimSelect();

  /// \brief parses column edit
  /// Called within Preview()
  /// Reads in and parses user suggestions using comma separated lists and
  /// ranges
  /// If parsing successful, returns the set of columns
  /// If no columns specified or if parsing unsuccessful, returns empty set.
  /// \param column
  QSet<int> readColumns();

  /// \brief updates table model to current user options
  /// Called within Preview()
  /// Takes in Vector Array of strings and instantiates a Target Table model
  /// \param table
  void updateTableModel(QVector<QVector<QString>> table);

public:
  void accept() override;

private slots:
  void projRadioClicked(QAbstractButton* button);

private:
  /// Attempt to build a MapScene layer with the given dialog options.
  ///
  /// If the layer is created successfully the dialog will close with
  /// DialogBox::Accepted.  If a layer is unable to be created an error
  /// box will be displayed, allowing the user to adjust parameters and
  /// try again or give up
  bool buildLayerAndClose();

  QString fileStr;
  QString delimStr;
  Ui::TargetImport* ui;
  QPointer<QCheckBox> lastChk;
  QButtonGroup* m_proj_button;
  MapScene& m_scene;
  SymbolLayer *layer;
};
}
#endif // TARGETIMPORT_H
