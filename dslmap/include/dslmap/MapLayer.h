/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 9/13/17.
//

#ifndef DSLMAP_IMAPLAYER_H
#define DSLMAP_IMAPLAYER_H

#include "DslMap.h"
#include "MapItem.h"

#include <QGraphicsObject>
#include <QUuid>

#include <memory>

class QMenu;
class QSettings;

namespace dslmap {

class Proj;
class MapLayerPrivate;

/// \brief Map Layer base class.
///
/// This class cannot be instantiated on it's own, it serves as a convenience
/// class that can handle most of the usual book-keeping required by a map
/// layer (like setting getting the name, setting the visibility, etc.)
///
///
class DSLMAPLIB_EXPORT MapLayer : public QGraphicsObject
{
  Q_OBJECT
  Q_DECLARE_PRIVATE(MapLayer)

signals:
  void nameChanged(const QString& name);

public:
  explicit MapLayer(QGraphicsItem* parent = nullptr);
  ~MapLayer() override;

  /// \brief Display layer properties dialog box
  ///
  /// This method must be overridden in any concrete subclass.
  ///
  /// This method should open a "properties"-style dialog for the class,
  /// allowing interactive adjustment of the various layer settings by the
  /// user.  It is *required* for all layers in the library.
  virtual void showPropertiesDialog() = 0;

  /// \brief Get the unique layer id.
  ///
  /// The layer id is assigned when the layer is instantiated and cannot be
  /// changed by the user.
  ///
  /// \return The unique id.
  virtual QUuid id() const;

  virtual QString name() const noexcept;

  /// \brief Create a context menu for the layer.
  ///
  /// Creates a context menu for the layer.  This menu may be used, for
  /// example, when the user right-clicks on an item that the layer owns to
  /// provide access to layer-level options, such as displaying the layer
  /// properties dialog.
  ///
  /// By default, the context menu provides options for:
  ///
  /// - Setting layer visibility
  /// - Showing the layer properties dialog.
  /// \return
  virtual QMenu* contextMenu();

  /// @brief Save layer details to a QSettings object
  ///
  /// A hook to save layer details to the provided QSettings object.  These
  /// settings could be used later to restore a series of properties with one
  /// call or to create a new layer using the same settings.
  ///
  /// \param settings
  /// \return
  virtual bool saveSettings(QSettings& settings) = 0;
  virtual bool loadSettings(QSettings& settings) = 0;

  std::shared_ptr<const Proj> projection() const noexcept;

  const Proj* projectionPtr() const noexcept;

public slots:
  virtual void setProjection(std::shared_ptr<const Proj> proj);

  virtual void setName(const QString& name) noexcept;

private:
  MapLayerPrivate* d_ptr;
};
}

#endif // DSLMAP_IMAPLAYER_H
