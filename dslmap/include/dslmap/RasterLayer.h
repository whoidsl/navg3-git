/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 7/25/18.
//

#ifndef NAVG_RASTERLAYER_H
#define NAVG_RASTERLAYER_H

#include "DslMap.h"
#include "ImageObject.h"
#include "MapLayer.h"

#include <QLoggingCategory>

namespace dslmap {

Q_DECLARE_LOGGING_CATEGORY(dslmapRasterLayer)

class DSLMAPLIB_EXPORT RasterLayer : public MapLayer
{
  Q_OBJECT

public:
  explicit RasterLayer(QGraphicsItem* parent = nullptr);
  ~RasterLayer() override;


  // MapLayer overrides
  int type() const override { return MapItemType::RasterLayerType; }

  bool saveSettings(QSettings& settings) override;
  bool loadSettings(QSettings& settings) override;

  QRectF boundingRect() const override;
  void paint(QPainter* painter, const QStyleOptionGraphicsItem* option,
             QWidget* widget) override;

  ImageObject* addImage(const QString& filename, const QRectF& bounds,
                        std::shared_ptr<Proj> proj);

  ImageObject* addImage(const QString& filename, const QPolygonF& bounds,
                        std::shared_ptr<Proj> proj);

  ImageObject* addImage(const QImage& image, const QRectF& bounds,
                        std::shared_ptr<Proj> proj);

  ImageObject* addImage(const QImage& image, const QPolygonF& bounds,
                        std::shared_ptr<Proj> proj);

  ImageObject* addImage(const QString& filename, const QRectF& bounds);

  ImageObject* addImage(const QString& filename, const QPolygonF& bounds);

  ImageObject* addImage(const QImage& image, const QRectF& bounds);

  ImageObject* addImage(const QImage& image, const QPolygonF& bounds);

  void showPropertiesDialog() override;

  void setProjection(std::shared_ptr<const Proj> proj) override;
};
}

#endif // NAVG_RASTERLAYER_H
