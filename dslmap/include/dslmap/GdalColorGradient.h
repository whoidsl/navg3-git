/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 10/23/17.
//

#ifndef NAVG_INTERPOLATEDCOLORGRADIENT_H
#define NAVG_INTERPOLATEDCOLORGRADIENT_H

#include <QColor>
#include <QLinearGradient>
#include <QSharedPointer>

namespace dslmap {
class GdalColorGradientPrivate;

///\brief Color gradient with color/value lookup.
class GdalColorGradient : public QObject
{

  Q_OBJECT
  Q_DECLARE_PRIVATE(GdalColorGradient)

signals:
  void gdalColorTableChanged();

public:
  ///\brief Preset color gradients
  enum GradientPreset
  {
    Greyscale = 0, //!< Greyscale colortable
    Rainbow,       //!< Rainbow colortable
    DarkRainbow,   //!< Darker rainbow colortable
    Cool,          //!< Cool colortable
    Warm,          //!< Warm colortable
    Hot,
    Atlas,
    DarkBlues,
    HAXBY,
    UserDefined //!< User defined presets
  };
  Q_ENUM(GradientPreset)

  explicit GdalColorGradient();
  explicit GdalColorGradient(const QGradientStops& stops);
  explicit GdalColorGradient(const QLinearGradient& gradient);
  explicit GdalColorGradient(GradientPreset);

  bool operator==(const GdalColorGradient& other) const;
  bool operator!=(const GdalColorGradient& other) const;

  /// \brief Replaces the current gradient
  ///
  /// \param gradient
  void setGradient(const QLinearGradient& gradient);

  /// \brief Replace the current gradient with a preset colortable
  ///
  /// \param preset
  void setGradient(GradientPreset preset);

  /// \brief Returns the most accurate preset gradient
  ///
  /// \return
  GradientPreset preset() const;

  QGradientStops stops() const;
  /// \brief Replaces the current set of stop points with the given stopPoints.
  ///
  /// These are the fixed values that the interpolator performs a linear
  /// interpolation between, and the positions of the points must be in the
  /// range 0 to 1 and must be sorted with the lowest point first.
  ///
  /// \param stops
  void setStops(const QGradientStops& stopPoints);

  /// \brief Return the current gradient used.
  ///
  /// Note that in Qt, gradients range from 0 to 1, so to attach significance
  /// to the returned gradient you should also look at
  /// InterpolatedColorGradient::min() and InterpolatedColorGradient::max()
  ///
  /// \return
  QLinearGradient gradient() const;

  /// \brief Set the minimum value of the gradient.
  ///
  /// Used in conjunction with InterpolatedColorGradient::colorAt().  Sets
  /// the minimum clipped value for the gradient.
  ///
  /// \param min
  void setMin(qreal min);
  qreal min() const;

  void setMax(qreal max);
  qreal max() const;

  void setRange(qreal min, qreal max);
  void setRange(const QPair<qreal, qreal>& range);

  qreal spread() const;
  QPair<qreal, qreal> range() const;

  /// \brief Set the gradient to use absolute values
  ///
  /// If set true, the gradient will use absolute values (set by setMin() and
  /// setMax()) to determine colors.  If false, the gradient uses relative
  /// values and ignores the bounds set by setMin() and setMax(), instead
  /// expecting the caller to have mapped the full range of data onto [0,1].
  ///
  /// \param absolute
  void setAbsolute(bool absolute);

  /// \brief Get whether the gradient measures absolute values or relative.
  ///
  /// See GdalColorGradient::setAbsolute()
  ///
  /// \return
  bool isAbsolute() const;

  // QUESTION(LEL): What is the No Data behavior? Is this for the case where
  //     the gradient is set to use absolute values, and the input data is
  //     is out of that range? If false, what happens? Is the data hidden,
  //     does it saturate at the ends of the colormap, ...? If isAbsolute is
  //     true, does this become meaningless?
  bool noDataVisible() const noexcept;

  void setNoDataVisible(const bool visible);

  void setNoDataColor(const QColor& color);

  QColor noDataColor() const noexcept;

  QColor colorAt(qreal value) const;

  // Return the color that should be displayed in case of no data
  // (Combines nodataColor and nodataVisible)
  QColor noDataDisplayColor() const noexcept;

  QString gdalColorTableFileName() const;

  static QGradientStops presetStops(GradientPreset preset);

protected:
  bool isEqual(const GdalColorGradient& other) const;

private:
  QSharedPointer<GdalColorGradientPrivate> d_ptr;
};
}

#endif // NAVG_INTERPOLATEDCOLORGRADIENT_H
