/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 10/20/18.
//

#ifndef DSLMAP_CONTOURLAYER_H
#define DSLMAP_CONTOURLAYER_H

#include "MapLayer.h"
#include <gdal/ogr_api.h>

#include <QLoggingCategory>

namespace dslmap {

Q_DECLARE_LOGGING_CATEGORY(dslmapContourLayer)

class GdalColorGradient;
class ContourLayerPrivate;
class Proj;

class DSLMAPLIB_EXPORT ContourLayer : public MapLayer
{

  // Q_INTERFACES(IMapLayer)
  Q_OBJECT
  Q_DECLARE_PRIVATE(ContourLayer)

public:
  explicit ContourLayer(QGraphicsItem* parent = nullptr);
  ~ContourLayer() override;

  // MapLayer overrides
  int type() const override { return MapItemType::ContourLayerType; }

  void showPropertiesDialog() override;
  bool saveSettings(QSettings& settings) override;
  bool loadSettings(QSettings& settings) override;
  void setProjection(std::shared_ptr<const Proj> proj) override;
  QSet<qreal> contours() const;

  void setGradient(std::shared_ptr<GdalColorGradient> gradient);
  std::shared_ptr<dslmap::GdalColorGradient> gradient() const;

  qreal minValue() const;
  qreal maxValue() const;
  QPair<qreal, qreal> minMaxValues() const;

  QRectF boundingRect() const override;
  void paint(QPainter* painter, const QStyleOptionGraphicsItem* option,
             QWidget* widget) override;

  static std::unique_ptr<ContourLayer> fromShapefile(
    const QString& filename, QString elevation_attribute = "elevation",
    int id = 0);

  static std::unique_ptr<ContourLayer> fromGrdFile(
    const QString& filename, int interval, int band = 1,
    std::shared_ptr<Proj> proj = {});

void saveUnderlay();
void setSpacing(int spacing);
void setSourceFile(QString file);

signals:
  void saveContourUnderlay(QString,int);

private slots:
  void updateContours();

private:
  ContourLayerPrivate* d_ptr;

  QString source_filename;
  int source_spacing=0;
};
}
#endif // DSLMAP_CONTOURLAYER_H
