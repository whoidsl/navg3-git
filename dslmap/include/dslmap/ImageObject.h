/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 10/12/17.
//

#ifndef NAVG_IMAGEOBJECT_H
#define NAVG_IMAGEOBJECT_H

#include "DslMap.h"
#include "MapItem.h"
#include "TemporaryGdalDataset.h"

#include <QGraphicsObject>
#include <QLoggingCategory>
#include <QSharedPointer>
#include <gdal/gdal_priv.h>

#include <memory>
class GDALDataset;

namespace dslmap {

class Proj;
enum class LayerType;
enum class Unit;

// Forward declaration of private impl.
class ImageObjectPrivate;

Q_DECLARE_LOGGING_CATEGORY(dslmapImageObject)

/// \brief A image-based raster object.
///
/// This class allows displaying arbitrary images on the map (at least,
/// anything QImage can load).  You need to provide both the projection and
/// the physical bounds of the image in order to correctly place the image on
/// the map scene.
class DSLMAPLIB_EXPORT ImageObject : public QGraphicsObject
{
  Q_OBJECT
  Q_DECLARE_PRIVATE(ImageObject)

public:
  /// \brief Create a new RasterLayer
  ///
  /// \param parent
  explicit ImageObject(QGraphicsItem* parent = Q_NULLPTR);

  //
  // QGraphicsItem overrides.
  //
  int type() const override { return MapItemType::ImageObjectType; }
  QRectF boundingRect() const override;
  void paint(QPainter* painter, const QStyleOptionGraphicsItem* option,
             QWidget* widget) override;

  /// \brief Load an image as raster data
  ///
  /// The projection is assumed to geodetic (lon/lat) if proj is `nullptr`.
  ///
  /// Uses QImage to load data from a plain image file.  By default, it
  /// should be able to read the following formats:
  ///
  ///  - BMP	Windows Bitmap	Read/write
  ///  - GIF	Graphic Interchange Format (optional)	Read
  ///  - JPG	Joint Photographic Experts Group	Read/write
  ///  - JPEG	Joint Photographic Experts Group	Read/write
  ///  - PNG	Portable Network Graphics	Read/write
  ///  - PBM	Portable Bitmap	Read
  ///  - PGM	Portable Graymap	Read
  ///  - PPM	Portable Pixmap	Read/write
  ///  - XBM	X11 Bitmap	Read/write
  ///  - XPM	X11 Pixmap	Read/write
  ///
  /// You need to provide the geographic bounds of the data, and the
  /// projection used.  The bounds are used to calculate the geographic size
  /// of each pixel.
  ///
  /// This method uses rectangular bounds.  For a more general perspective,
  /// use
  ///
  /// RasterLayer::setImage(
  ///     const QString& filename, const QPolygonF& bounds,
  /// std::shared_ptr<Proj> proj)
  ///
  ///
  /// \param filename  Filename to load from disk.
  /// \param bounds    Bounds of the image in projected coordinates
  /// \param proj      Projection to use for the image.
  ///
  /// \return true if successful.
  bool setImage(const QString& filename, const QRectF& bounds,
                std::shared_ptr<const Proj> proj);

  /// \brief Load an image as raster data
  ///
  /// The projection is assumed to geodetic (lon/lat) if proj is `nullptr`.
  ///
  /// Uses QImage to load data from a plain image file.  By default, it
  /// should be able to read the following formats:
  ///
  ///  - BMP	Windows Bitmap	Read/write
  ///  - GIF	Graphic Interchange Format (optional)	Read
  ///  - JPG	Joint Photographic Experts Group	Read/write
  ///  - JPEG	Joint Photographic Experts Group	Read/write
  ///  - PNG	Portable Network Graphics	Read/write
  ///  - PBM	Portable Bitmap	Read
  ///  - PGM	Portable Graymap	Read
  ///  - PPM	Portable Pixmap	Read/write
  ///  - XBM	X11 Bitmap	Read/write
  ///  - XPM	X11 Pixmap	Read/write
  ///
  /// You need to provide the geographic bounds of the data, and the
  /// projection used.  The bounds are used to calculate the geographic size
  /// of each pixel.
  ///
  /// This method can map a general perspective transform provided by the
  /// QPolygonF argument, which should be the 4 vertices of the bounds of the
  /// image, starting from the top-left corner and proceeding clockwise:
  ///
  ///   top-left, top-right, bottom-right, bottom-left
  ///
  /// If your image bounds are rectangular, you can use the simpler version:
  ///
  /// RasterLayer::setImage(
  ///     const QString& filename, const QRectF& bounds,
  ///     std::shared_ptr<Proj> proj)
  ///
  ///
  /// \param filename  Filename to load from disk.
  /// \param bounds    Bounds of the image in projected coordinates
  /// \param proj      Projection to use for the image.
  ///
  /// \return true if successful.
  bool setImage(const QString& filename, const QPolygonF& bounds,
                std::shared_ptr<const Proj> proj);

  /// \brief Load an image as raster data
  ///
  /// See RasterLayer::setImage(
  ///    const QString& filename, const QRectF& bounds,
  ///    std::shared_ptr<Proj> proj);
  ///
  /// This method uses rectangular bounds.  For a more general perspective,
  /// use
  ///
  /// RasterLayer::setImage(
  ///     const QString& filename, const QPolygonF& bounds,
  ///     std::shared_ptr<Proj> proj)
  ///
  /// \param image     Image data to use.
  /// \param bounds    Bounds of the image in projected coordintes
  /// \param proj      Projection to use for the image.
  ///
  /// \return true if successful.
  bool setImage(const QImage& image, const QRectF& bounds,
                std::shared_ptr<const Proj> proj);

  /// \brief Load an image as raster data
  ///
  /// See RasterLayer::setImage(
  ///    const QString& filename, const QPolygonF& bounds,
  ///    std::shared_ptr<Proj> proj);
  ///
  /// This method can map a general perspective transform provided by the
  /// QPolygonF argument, which should be the 4 vertices of the bounds of the
  /// image, starting from the top-left corner and proceeding clockwise:
  ///
  ///   top-left, top-right, bottom-right, bottom-left
  ///
  /// If your image bounds are rectangular, you can use the simpler version:
  ///
  /// RasterLayer::setImage(
  ///     const QImage& image, const QRectF& bounds, std::shared_ptr<Proj> proj)
  ///
  /// \param image     Image data to use.
  /// \param bounds    Bounds of the image in projected coordintes
  /// \param proj      Projection to use for the image.
  ///
  /// \return true if successful.
  bool setImage(const QImage& image, const QPolygonF& bounds,
                std::shared_ptr<const Proj> proj);

  QImage combineQImageBands(std::vector<QImage> images, std::vector<GDALRasterBand*> bands);


  void setProjection(std::shared_ptr<const Proj> proj);
  std::shared_ptr<const Proj> projection() const noexcept;
  const Proj* projectionPtr() const noexcept;

  /// \breif Return a handle to the original dataset
  ///
  /// \return
  const TemporaryGdalDataset* dataset() const;

  /// \breif Return a handle to the projected dataset
  ///
  /// \return
  const TemporaryGdalDataset* projectedDataset() const;

private:
  ImageObjectPrivate* d_ptr;
};
}

#endif // NAVG_RASTERLAYER_H
