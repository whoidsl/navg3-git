/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 10/26/17.
//

#ifndef DSLMAP_SYMBOLLAYER_H
#define DSLMAP_SYMBOLLAYER_H

#include "DslMap.h"
#include "MapLayer.h"
#include "MarkerItem.h"
#include "Proj.h"
#include "ShapeMarkerSymbol.h"

#include <QLoggingCategory>

namespace dslmap {

class SymbolLayerPrivate;

Q_DECLARE_LOGGING_CATEGORY(dslmapSymbolLayer)

class SymbolLayer : public MapLayer
{
  Q_OBJECT
  Q_DECLARE_PRIVATE(SymbolLayer)

  Q_PROPERTY(QColor outlineColor READ outlineColor WRITE setOutlineColor NOTIFY
               outlineColorChanged)
  Q_PROPERTY(
    QColor fillColor READ fillColor WRITE setFillColor NOTIFY fillColorChanged)
  Q_PROPERTY(QColor labelColor READ labelColor WRITE setLabelColor NOTIFY
               labelColorChanged)
  Q_PROPERTY(qreal labelSize READ labelSize WRITE setLabelSize NOTIFY
                 labelSizeChanged)

signals:
  void outlineColorChanged(QColor color);
  void fillColorChanged(QColor color);
  void labelColorChanged(QColor color);
  void labelSizeChanged(qreal points);
  void saveTargetUnderlay(QString);
  void pointChanged();
public:
  explicit SymbolLayer(QGraphicsItem* parent = nullptr);
  ~SymbolLayer() override;

  int type() const override { return MapItemType::SymbolLayerType; }
  QMenu* contextMenu() override;

  void showPropertiesDialog() override;

  /// \brief Save layer's settings (but not the information displayed)
  bool saveSettings(QSettings& settings) override;
  bool loadSettings(QSettings& settings) override;
  static std::unique_ptr<SymbolLayer> fromSettings(QSettings& settings);

  std::shared_ptr<GdalColorGradient> gradient();

  void setProjection(std::shared_ptr<const Proj> proj) override;

  QRectF boundingRect() const override;
  void paint(QPainter* painter, const QStyleOptionGraphicsItem* option,
             QWidget* widget) override;
  /// \brief Get the pointer to MapItemSymbol
  ///
  /// The layer retains ownership of the symbol.
  ///
  /// \return
  std::shared_ptr<ShapeMarkerSymbol> symbol() const;
  ShapeMarkerSymbol* symbolPtr() const;

  /// \brief get list of attribute keys used by points in this layer.
  QList<QString> attributeKeys() const;

  /// \brief Get the shape used for this layer.
  ///
  /// \return
  ShapeMarkerSymbol::Shape symbolShape() const;

  /// \brief Get the symbol size for this layer.
  ///
  /// \return
  qreal symbolSize() const;

  /// \brief Get the path used for the symbol shape
  ///
  /// \return
  QPainterPath symbolPath() const;

  /// \brief Add a new projected point to the layer.
  ///
  /// \param position    Position of the new point
  /// \param label       Label string
  /// \param attributes  Additional attributes
  ///
  /// This method uses the layer's current projection.  To add a new point
  /// using geodetic coordinates, use SymbolLayer::addLonLatPoint
  ///
  /// The layer retains ownership of the created marker.  The signal,
  /// SymbolLayer::itemAdded, is emitted after the marker is added.
  ///
  /// Will return nullptr if the layer doesn't yet have a symbol.
  ///
  /// \return Pointer to the new marker.
  MarkerItem* addPoint(const QPointF& position, const QString& label = {},
                       const MarkerItem::Attributes& attributes = {});

  /// \brief Add a new projected point to the layer.
  ///
  /// \param x           X coordinate of new point
  /// \param y           Y coordinate of new point
  /// \param label       Label string
  /// \param attributes  Additional attributes
  ///
  ///
  /// This method uses the layer's current projection.  To add a new point
  /// using geodetic coordinates, use SymbolLayer::addLonLatPoint
  ///
  /// The layer retains ownership of the created marker.  The signal,
  /// SymbolLayer::markerAdded, is emitted after the marker is added.
  ///
  /// \return Pointer to the new marker.
  MarkerItem* addPoint(qreal x, qreal y, const QString& label = {},
                       const MarkerItem::Attributes& attributes = {});

  /// \brief Add a new geodetic point to the layer.
  ///
  /// \param lonlat      Longitude/Latitude of point IN DEGREES
  /// \param label       Label string
  /// \param attributes  Additional attributes
  ///
  /// This method accepts geodetic coordinates and projects them to the
  /// layer's coordinate system.
  ///
  /// The layer retains ownership of the created marker.  The signal,
  /// SymbolLayer::itemAdded, is emitted after the marker is added.
  ///
  /// \return Pointer to the new marker.
  MarkerItem* addLonLatPoint(const QPointF& lonlat, const QString& label = {},
                             const MarkerItem::Attributes& attributes = {});

  /// \brief Add a new geodetic point to the layer.
  ///
  /// \param lon         Longitude of new point IN DEGREES
  /// \param lat         Latitude of new point IN DEGREES
  /// \param label       Label string
  /// \param attributes  Additional attributes
  ///
  ///
  /// This method accepts geodetic coordinates and projects them to the
  /// layer's coordinate system.
  ///
  /// The layer retains ownership of the created marker.  The signal,
  /// SymbolLayer::markerAdded, is emitted after the marker is added.
  ///
  /// \return Pointer to the new marker.
  MarkerItem* addLonLatPoint(qreal lon, qreal lat, const QString& label = {},
                             const MarkerItem::Attributes& attributes = {});

  /// \brief Change the label of a point from the layer
  ///
  /// \param marker
  /// \param label
  /// \return whether the label was successfully changed
  bool changeLonLatPoint(MarkerItem* marker, const QString& label={});

  /// \brief Remove a point from the layer
  ///
  /// \param marker
  /// \return whether marker was found in layer and removed (false if not found)
  ///
  /// Ownership of the QGraphicsItem* is returned to the caller after;
  bool removePoint(MarkerItem* marker);

  /// \brief Get the number of points in this layer.
  ///
  /// \return Number of points.
  size_t count() const;

  /// \brief the outline color used for symbols
  QColor outlineColor() const;

  /// \brief set the text color for labels
  QColor labelColor() const;

  /// \brief set the text size for labels
  qreal labelSize() const;

  /// \brief the fill color used for symbols
  QColor fillColor() const;

  /// \brief Get the symbol units
  ///
  /// \return
  Unit units() const;

  /// \brief Get the symbol minimum draw size
  ///
  /// See MapItemSymbol::minimumDrawSize
  ///
  /// \return
  qreal minimumDrawSize() const;

  /// \brief Draw the symbol the same size regardless of map scale.
  ///
  /// If true, the symbol will be drawn at the same size regardless of map
  /// scale.  The 'units' of the symbol will be taken as pixels.
  ///
  /// If false, the symbol will be drawn at a size representing it's true
  /// physical extent given the current map scale.  The drawn size is
  /// determined by the current layer units (see SymbolLayer::units()) and
  /// the minimum draw size (SymbolLayer::minimumDrawSize()).
  ///
  /// \param invariant
  void setScaleInvariant(bool invariant);

  /// \brief Are map symbols scale invariant?
  ///
  /// Return whether the map symbols are scale invariant.  See
  /// SymbolLayer::setScaleInvariant()
  ///
  /// \return
  bool isScaleInvariant() const;

  /// \brief Show or hide map item labels
  ///
  /// Show or hide labels associated with MapItem objects that have been
  /// given a label.  Labels can be given to items using
  /// MapItem::setLabelText() or by providing a label string when using one
  /// of the SymbolLayer::addPoint() or SymbolLayer::adLonLatPoint()
  ///
  /// \param visible
  void setLabelVisible(bool visible);

  /// \brief Are map labels visible?
  ///
  /// Return whether map labels are visible or not.  See
  /// SymbolLayer::setLabelVisible()
  ///
  /// \return
  bool isLabelVisible() const;

  /// \brief returns the symbol outline visibility
  ///
  /// \return
  bool outlineVisible() const;

  /// \brief returns the symbol fill brush visibility
  ///
  /// \return
  bool fillVisible() const;

public slots:
  void setShape(ShapeMarkerSymbol::Shape shape, qreal size);

  /// \brief Set the symbol units
  ///
  /// \param unit
  void setUnits(Unit unit);

  /// \brief Set the outline color used for symbols
  void setOutlineColor(const QColor& color);

  /// \brief Set the outline visible
  void setOutlineVisible(const bool visible);

  /// \brief Set the text color for labels
  void setLabelColor(const QColor& color);

  /// \brief Set the text size for labels
  void setLabelSize(qreal points);

  /// \brief Set the symbol fill color
  void setFillColor(const QColor& color);

  /// \brief Set the symbol fill visible
  void setFillVisible(const bool visible);

  /// \brief Set the minimum draw size for the symbol
  ///
  /// See MapItemSymbol::setMinimumDrawSize
  ///
  /// \param pixels
  void setMinimumDrawSize(qreal pixels);

  /// \brief Update the map symbol used for the current beacon position.
  ///
  /// \param symbol
  void setSymbol(std::shared_ptr<ShapeMarkerSymbol> symbol);

  void printLatLonLabels();

  void editLatLonLabels();

  QVector<QVector<QString>> getLatLonLabels();
  
  void exportLatLonLabels();

  /// \brief Update the gradient min/max to match the currently chosen attribute
  void rescaleGradient();

  void saveUnderlay();

  void setFileName(QString filename);

  QString getFileName();

  void setIsUnderlayFormat(bool state);

protected:
  QList<MarkerItem*>& markers();

private:
  SymbolLayerPrivate* d_ptr;
};
}

#endif // DSLMAP_SYMBOLLAYER_H
