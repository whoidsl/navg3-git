/**
 * Copyright 2021 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// Created by tmjoyce on 06/03/21.
//

#ifndef NAVG_IMAGETILINGHELPER_H
#define NAVG_IMAGETILINGHELPER_H

#include "DslMap.h"

#include "GdalDataset.h"
#include "Proj.h"
//

#include <QGraphicsPixmapItem>
#include <QGridLayout>
#include <QImage>
#include <QLabel>
#include <QLoggingCategory>
#include <QObject>
#include <QPair>
#include <QPixmap>
#include <QPointF>
#include <QRectF>
#include <QSharedPointer>
#include <QString>
#include <vector>

#include <memory>

namespace dslmap {

class DSLMAPLIB_EXPORT ImageTilingHelper : public QObject
{
  Q_OBJECT

public:
  static std::vector<QGraphicsPixmapItem*> getImageTiles(
    QImage image,
    QTransform source_transform);

  static std::vector<QGraphicsPixmapItem*> getImageTiles(
    QGraphicsPixmapItem* source_pixmap);

  static bool needsTiling(QImage image);
  static bool needsTiling(QGraphicsPixmapItem* item);
  static bool needsTiling(std::vector<QGraphicsPixmapItem*> pix_items);

private:
  static QPair<QGraphicsPixmapItem*, QGraphicsPixmapItem*> cutImageVertically(
    QGraphicsPixmapItem* input_pix);
  static QPair<QGraphicsPixmapItem*, QGraphicsPixmapItem*> cutImageHorizontally(
    QGraphicsPixmapItem* input_pix);
};
}

#endif // NAVG_IMAGETILINGHELPER_H
