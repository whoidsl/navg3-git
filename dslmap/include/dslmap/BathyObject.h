/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 10/13/17.
//

#ifndef NAVG_RASTERBATHYLAYER_H
#define NAVG_RASTERBATHYLAYER_H

#include "DslMap.h"
#include <QGraphicsObject>
#include <QLoggingCategory>
#include <QSharedPointer>
#include <memory>

class GDALDataset;

namespace dslmap {

class BathyObjectPrivate;
class Proj;
class GdalColorGradient;

Q_DECLARE_LOGGING_CATEGORY(dslmapBathyObject)

/// \brief A bathymetry raster map item
///
/// The BathyItem class provides a means to both load a GDAL-supported
/// bathymetry raster and change it's displayed projection on the MapScene.
/// It attempts to be as memory-efficient as possible.  As such, all of the
/// actual raster data is kept on disk (and out of RAM) in temporary files
/// (GdalDataset and TemporaryGdalDataset wrapped versions).  Only two
/// QGraphicsPixmapItems are kept in memory to display the raster on the map.
///
/// The original raster is opened read-only and cannot be modified.  The
/// original raster is warped to the map projection using 'gdalwarp'.  Shaded
/// relief and hillshaded versions are created using 'gdaldem' and used for
/// the two in-memory pixmaps.
///
/// Building the full BathyItem object may be expensive (the data must load,
/// be projected, be shaded, have the hillshade calculated, then the pixmaps
/// updated), but needs only to be done once.  Smaller partial-updates are
/// required whenever the desired map projection changes (with
/// BathyItem::setProjection()) or the color gradient changes
/// (BathyItem::setGradient())
///
class BathyObject : public QGraphicsObject
{
  Q_OBJECT
  Q_DECLARE_PRIVATE(BathyObject)

signals:

  /// \brief Emitted when the dataset for this object changes
  void datasetChanged();

  /// \brief Emitted when the projected band changes.
  void bandChanged(int band);

public:
  enum UpdateOption
  {
    UpdateNone = 0x00,         //!< No-op
    UpdateBand = 0x01,         //!< Update to a new raster band
    UpdateProjection = 0x02,   //!< Update to a new projection
    UpdateShadedRelief = 0x04, //!< Update the shaded relief image
    UpdateHillshade = 0x08,    //!< Update the hillshade image
    UpdatePixmap = 0x10, //!< Update the relief/hillshade composited pixmap
    UpdateAll = UpdateBand | UpdateProjection | UpdateShadedRelief |
                UpdateHillshade |
                UpdatePixmap
  };

  Q_DECLARE_FLAGS(UpdateOptions, UpdateOption)

  explicit BathyObject(QGraphicsItem* parent = nullptr);
  ~BathyObject() override;

  //
  // QGraphicsItem Overrides
  //
  int type() const override;
  QRectF boundingRect() const override;
  void paint(QPainter* painter, const QStyleOptionGraphicsItem* option,
             QWidget* widget) override;

  ///\brief Get the gradient used in shading the bathymetry.
  std::shared_ptr<GdalColorGradient> gradient() const;

  /// \brief Check if hillshading is enabled.
  bool isHillshadeEnabled() const noexcept;

  /// \brief  Get the original dataset projection.
  ///
  /// This is the projection of the dataset as loaded from disk.  It cannot
  /// be modified.
  ///
  /// \return
  const Proj* datasetProjection() const;

  std::shared_ptr<const Proj> projection() const noexcept;

  const Proj* projectionPtr() const noexcept;

  /// \brief Raw GDAL Dataset pointer
  ///
  /// \return
  GDALDataset* dataset();

  /// \brief Get the displayed band.
  ///
  /// \return
  int band() const noexcept;

  /// \brief Get the minimum value of the band
  ///
  /// \return
  qreal minValue() const noexcept;

  /// \breif Get the maximum value of the band.
  qreal maxValue() const noexcept;

  /// \brief Get the min, max value of the band.
  ///
  /// \return
  QPair<qreal, qreal> minMaxValues() const noexcept;

  /// \brief Returnt the raster data for the provided point.
  ///
  /// The position should be in the current projection.
  ///
  /// \param pos
  /// \return
  qreal valueAtProjectedPos(QPointF pos);

public slots:
  /// \brief Load a raster dataset from disk.
  ///
  /// Open a raster bathymetry source in read-only mode.  `proj` can be used
  /// to provide a projection if the dataset lacks one (the projection
  /// will NOT be saved with the file).
  ///
  /// Returns true if the raster was succesfully loaded.
  ///
  /// \param filename Filename of dataset.
  /// \param band Raster band to create image from.
  /// \param proj Source projection override.
  /// \return
  bool setRasterFile(const QString& filename, int band,
                     std::shared_ptr<Proj> proj = {});

  ///\brief Set the gradient used in shading the bathymetry.
  void setGradient(std::shared_ptr<GdalColorGradient> gradient);

  /// \brief Enable/Disable hillshading of bathymetry
  ///
  /// \param enabled
  void setHillshadeEnabled(bool enabled);

  /// \breif Set the projection
  ///
  /// Change the displayed projection.  (NOT the projection of the original
  /// dataset)
  ///
  /// \param proj
  void setProjection(std::shared_ptr<const Proj> proj);

  /// \brief Set the displayed band.
  ///
  /// Bands are 1-based.
  ///
  /// \param band
  void setBand(int band);

  /// Update the shaded relief due to a change in the colortable.
  bool update(UpdateOptions options);

private:
  BathyObjectPrivate* d_ptr;
};
}

Q_DECLARE_OPERATORS_FOR_FLAGS(dslmap::BathyObject::UpdateOptions)

#endif // NAVG_RASTERBATHYLAYER_H
