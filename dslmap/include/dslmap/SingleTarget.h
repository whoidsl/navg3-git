/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef SINGLETARGET_H
#define SINGLETARGET_H

#include "MapScene.h"
#include "Proj.h"
#include "SymbolLayer.h"
#include <QDialog>
#include <QRadioButton>

namespace dslmap {
namespace Ui {
class SingleTarget;
}

/// \brief Dialog for dropping a target into a MapScene.
/// Depending on the specific MapView implementation, dialog will populate with
/// position and layer options. There will always be a lat/lon input option,
/// and there will always be a create new SymbolLayer option.

class DSLMAPLIB_EXPORT SingleTarget : public QDialog
{
  Q_OBJECT

public:
  /// Create the dialog
  explicit SingleTarget(QWidget* parent = 0);
  explicit SingleTarget(QMap<QString, QPointF> position_options = {},
                        QWidget* parent = 0);

  /// Delete the dialog
  ~SingleTarget();

  /// setScene populates existing SymbolLayers into the dialog
  void setScene(dslmap::MapScene* scene);

  /// setPositions populates position location options into the dialog
  void setPositions(QMap<QString, QPointF> position_options = {});

  /// Returns an existing layer or a nullptr if a new SymbolLayer is requested
  dslmap::SymbolLayer* extractSymbolLayer();

  /// Returns the position in the MapScene's coordinates of the new Target
  /// location.
  QPointF extractPos();

  /// Gets the new SymbolLayer's name
  /// This value won't be used if an existing SymbolLayer was returned in
  /// extractSymbolLayer
  QString extractLayername();

  /// Returns the new label, or an empty QString
  QString extractLabeltext();

private:
  Ui::SingleTarget* ui;

  /// This object maps the radio buttons to the correct position
  QMap<QRadioButton*, QPointF> m_pointmap;

  /// This object maps the radio buttons to the corresponding SymbolLayer
  QMap<QRadioButton*, dslmap::SymbolLayer*> m_layermap;

  /// This value gives access to existing SymbolLayers and the existing Proj
  dslmap::MapScene* m_scene;
};
}

#endif // SINGLETARGET_H
