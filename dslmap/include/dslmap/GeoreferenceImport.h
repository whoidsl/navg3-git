/**
 * Copyright 2020 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef GEOREFERENCEIMPORT_H
#define GEOREFERENCEIMPORT_H

#include "MapScene.h"
#include "Proj.h"
#include "DslMap.h"
#include <QDialog>
#include <QRadioButton>

namespace dslmap {
namespace Ui {
class GeoreferenceImport;
}

/// \brief Dialog for adding new geoimages to new or existing layers.
Q_DECLARE_LOGGING_CATEGORY(dslmapGeoreferenceImageImport)

class GeoreferenceImport : public QDialog
{
  Q_OBJECT

public:
  /// Create the dialog
  explicit GeoreferenceImport(QWidget* parent = 0);
  
  /// \brief Returns stored private filename. Empty string if not set
  QString getFileName();

  /// \brief Returns transparent QColor whether valid or not 
  QColor getTransColor();

  /// \brief Returns name of layer. Empty string if layer name not set
  QString getLayerName();

protected slots:
  
  void setLayerName(QString name);
  void cancelSettings();
  /// Transparant color to apply to geoimage
  void setTransColor(QColor color);
  void confirmSettings();
  void updateFileNameText();
  QString openFile();


signals:
  /// \brief signal thrown when ColorPickButton QColor is changed
  void transColorChanged(QColor color);

private:
  Ui::GeoreferenceImport* ui;
  QString fileName;
  QColor transColor;
  QString layerName;
};
}
#endif // GEOREFERENCEIMPORT_H