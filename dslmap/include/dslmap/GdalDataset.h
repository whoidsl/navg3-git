/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 10/24/17.
//

#ifndef NAVG_READONLYGDALDATASET_H
#define NAVG_READONLYGDALDATASET_H

#include "DslMap.h"

#include <QObject>
#include <QPointF>
#include <QRectF>
#include <QSharedPointer>
#include <QString>

#include <memory>

class GDALDataset;

namespace dslmap {

class GdalDatasetPrivate;
class TemporaryGdalDataset;
class Proj;

/// \brief A read-only leak-free wrapper around a GDALDataset object.
class DSLMAPLIB_EXPORT GdalDataset : public QObject
{
  Q_OBJECT
  Q_DECLARE_PRIVATE(GdalDataset)

public:
  explicit GdalDataset(QObject* parent = nullptr);
  explicit GdalDataset(const QString& filename, QObject* parent = nullptr);

  ~GdalDataset() override;

  ///\brief Get the name of the dataset.
  ///
  /// \return
  virtual QString fileName() const noexcept;

  ///\brief Set the name of the file.
  ///
  /// Do not call this function if the file is open (doing so will have no
  /// effect).
  ///
  /// \param name
  virtual void setFileName(const QString& name);

  ///\brief Open the dataset.
  ///
  /// Uses the filename provided either by GdalDatset::setFileName or using
  /// the constructor. The dataset will be opened read-only.
  ///
  /// Takes no action and returns false if a dataset is already open.
  ///
  /// Returns true if the operation was successful.
  /// \return
  virtual bool open();

  bool isOpen() const noexcept;

  ///\brief Close the dataset
  ///
  /// Closes the dataset (calls GDALClose on the owned pointer) and releases
  /// any GDAL resources.  This will invalidate all pointers returned by the
  /// GdalDatset::get() method.
  void close();

  ///\brief Get the raw GDALDataset object.
  ///
  /// \return
  GDALDataset* get() const noexcept;

  /// \brief Get the proj4 definition string for the dataset
  QString proj4Definition();

  /// \brief Get the WKT definition string for the dataset
  ///
  /// \param pretty Return a human-readable pretty-print version
  /// \return
  QString wktDefinition(bool pretty = false);

  /// \brief Override the dataset projection
  ///
  /// Note, this does NOT change the projection as stored in the GDAL dataset
  /// (no modifcations are made to the actual file)
  ///
  /// Use this method to give projections to datasets lacking projection
  /// information.
  ///
  /// \param proj
  void setProjection(std::shared_ptr<const Proj> proj);

  /// \brief Get the projection for the dataset
  ///
  /// \return
  std::shared_ptr<const Proj> projection() const;

  /// \brief Reset projetion using information stored in dataset.
  ///
  /// This clears any projection overrides provided by the
  /// Proj::setProjection() method.
  ///
  /// Note: Using this method may leave the dataset without a projection if the
  /// dataset does not provide projection information.
  ///
  /// Returns true if a projection could be created using information within
  /// the dataset.  Returns false if the dataset contains no projection
  /// information, or if the information could not be used to create a Proj
  /// object.
  ///
  /// \return
  bool resetProjection();

  /// \brief Reproject the GDAL dataset given the provided projection
  std::shared_ptr<TemporaryGdalDataset> warp(const Proj& proj,
                                             const QStringList& opts = {});

  /// \brief Reproject GDAL dataset but assuming less default gdalwarp arguments
  std::shared_ptr<TemporaryGdalDataset> warp_min(const Proj& proj,
                                             const QStringList& opts = {});

  /// \brief Translates raster dataset to geotiff format                                         
  std::shared_ptr<TemporaryGdalDataset> translate(const Proj& proj,
                                                  const QStringList& opts = {});

  /// \brief Use gdaldem tool to create a shaded relief
  ///
  /// \param colorTableFile
  /// \param opts
  /// \return
  std::shared_ptr<TemporaryGdalDataset> colorRelief(
    const QString& colorTableFile, const QStringList& opts = {});

  /// \brief Use gdaldem tool to create a hillshade image
  ///
  /// \param opts
  /// \return
  std::shared_ptr<TemporaryGdalDataset> hillshade(const QStringList& opts = {});

  QRectF boundingRect();

protected:
  explicit GdalDataset(GdalDatasetPrivate& pimpl, QObject* parent);

  // need to use a shared pointer here due to a 7(?) year old bug with Qt?
  // https://bugreports.qt.io/browse/QTBUG-6922?page=com.atlassian.jira.plugin.system.issuetabpanels%3Achangehistory-tabpanel
  //
  // No idea why ScopedPointer works with MapLayer....
  QSharedPointer<GdalDatasetPrivate> d_ptr;
};
}

#endif // NAVG_READONLYGDALDATASET_H
