/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 9/19/18.
//

#ifndef NAVG_SYMBOLLAYERPROPERTIESWIDGET_H
#define NAVG_SYMBOLLAYERPROPERTIESWIDGET_H

#include <QWidget>
#include <QDialog>

namespace dslmap {

class SymbolLayer;

namespace Ui {
class SymbolLayerPropertiesWidget;
}

/// Widget for displaying and editing SymbolLayer display options
/// Also includes and connects GeneralLayerProperties Widget
///
class SymbolLayerPropertiesWidget : public QWidget
{
  Q_OBJECT

public:
  explicit SymbolLayerPropertiesWidget(QWidget* parent = nullptr);
  ~SymbolLayerPropertiesWidget();

  /// \brief set the layer to display/edit
  ///
  /// initializes values with current layer properties
  /// connects signals in widgets to slots in properties
  ///
  /// NOTE: Will be called every time showPropertiesDialog is activated,
  /// without the widget being recreated, so widget state persists across calls.
  ///
  /// \param layer
  void setLayer(SymbolLayer* layer);

private:
  Ui::SymbolLayerPropertiesWidget* ui;
};

} // namespace

#endif // NAVG_SYMBOLLAYERPROPERTIESWIDGET_H
