/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef DSLMAP_GEOREFERENCEIMAGELAYER_H
#define DSLMAP_GEOREFERENCEIMAGELAYER_H

#include "DslMap.h"
#include "GeoreferenceImageObject.h"
#include "GeoreferenceImageLayerPropertiesWidget.h"
#include "MapLayer.h"
#include <QDir>
#include <QFileDialog>
#include <QImageWriter>
#include <QLoggingCategory>

namespace dslmap {

Q_DECLARE_LOGGING_CATEGORY(dslmapGeoreferenceImageLayer)

// forward declaration for class defined in cpp
class GeoreferenceImageLayerPrivate;
class GdalColorGradient;

class DSLMAPLIB_EXPORT GeoreferenceImageLayer : public MapLayer
{
  //  Q_INTERFACES(IMapLayer)
  Q_OBJECT
  Q_DECLARE_PRIVATE(GeoreferenceImageLayer)

public:
  explicit GeoreferenceImageLayer(QGraphicsItem* parent = nullptr);
  ~GeoreferenceImageLayer() override;

  // MapLayer overrides
  // int type() const override { return MapItemType::BathymetryLayerType; }
  // Return GeoImage Object that supports many types
  GeoreferenceImageObject* addRasterFile(const QString& filename, int band = 1);

  // Return GeoImage Object creates from an image with corner points given
  GeoreferenceImageObject* addRasterFile(const QString& filename, QPointF top_left, QPointF bottom_right, int band = 1);

  // Map layer overrides
  void showPropertiesDialog() override;

  void setProjection(std::shared_ptr<const Proj> proj) override;
  bool saveSettings(QSettings& settings) override;
  bool loadSettings(QSettings& settings) override;
  
  /// \brief Returns QRectF of bounding points to use for map scene projection
  QRectF boundingRect() const override;
  void paint(QPainter* painter, const QStyleOptionGraphicsItem* option,
             QWidget* widget) override;

  QList<GeoreferenceImageObject*> georeferenceImageObjects() const;
  /// \brief Opens QDialog box and allows saving of current layer image to png
  void exportLayerImage();
  void saveUnderlay();

signals:
  void saveGeoimageUnderlay(QString);
  void saveImageUnderlay(QString,QPointF,QPointF);

private:
  GeoreferenceImageLayerPrivate* d_ptr;
  QString source_filename;
  QPointF source_topleft;
  QPointF source_bottomright;
};
}
#endif // DSLMAP_GEOREFERENCEIMAGELAYER_H
