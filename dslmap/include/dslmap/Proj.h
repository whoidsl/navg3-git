/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 10/19/17.
//

#ifndef NAVG_PROJ_H
#define NAVG_PROJ_H

#include "DslMap.h"

#include <QLoggingCategory>
#include <QString>

#include <memory>

using projPJ = void*;
using projPJ_ = void;

using projCtx_ = void;
using projCtx = void*;

#define PROJ_WGS84_DEFINITION                                                  \
  "+proj=latlong +datum=WGS84 +ellps=WGS84 +towgs84=0,0,0 +no_defs"

namespace dslmap {

Q_DECLARE_LOGGING_CATEGORY(dslmapProj)

class ProjPrivate;
enum class Unit;

/// \brief C++ Wrapper around Proj4 C API
///
/// This class provides a safe wrapper around the Proj4 C API.  Instances can
/// only be created as shared pointers, providing safe machinery for sharing
/// projection instances amongst multiple objects without reinitializing many
/// of the same projections.
///
/// This class tries to ensure only valid projections can be instantiated.
/// To achieve this there is no direct constructors, only static factory
/// functions that provide shared pointers to the created instance.
///
/// Factory methods:
///
/// - fromDefintion(const QString& def):
///     Create a projection from a proj4  "init plus" string.
///
/// - utmFromLonLat(qreal lon, qreal lat):
///     Create a UTM projection using the proper zone for the given coordinate
///
/// - alvinXYFromLonLat(qreal lon, qreal lat):
///     Create a transverse mercator projection centered at the given origin.
class Proj
{
  Q_DECLARE_PRIVATE(Proj)

public:
  /// \brief Create a new Proj4 projection from the provided definition.
  ///
  /// The default proj4 context will be used if no `context` is provided.
  ///
  /// The returned shared pointer will be empty if there was a problem
  /// creating the projection.  Try using Proj::lastError() to get some more
  /// information on why it failed.
  ///
  /// \param def
  /// \param context
  /// \return
  static std::shared_ptr<Proj> fromDefinition(
    const QString& def, std::shared_ptr<projCtx_> context = {});

  /// \brief Create a new Proj4 projection from the provided definition.
  ///
  /// The default proj4 context will be used if no `context` is provided.
  ///
  /// This method allows creating a projection from a list of definition
  /// arguments.  Leading '+' characters will be prepended if missing.
  ///
  /// The returned shared pointer will be empty if there was a problem
  /// creating the projection.  Try using Proj::lastError() to get some more
  /// information on why it failed.
  ///
  /// \param def
  /// \param context
  /// \return
  static std::shared_ptr<Proj> fromDefinition(
    const QStringList& definition, std::shared_ptr<projCtx_> context = {});

  ~Proj();

  /// \brief Transform coordinates between projections
  ///
  /// Transforms points in place inside the x, y, and z vectors (z being
  /// optional and can be empty).  Transforms the minimum number of points if
  /// the vectors differ in length.
  ///
  /// Geographic coordinates require transformations in radians.  If
  /// is_degrees is true this conversion is done for you, both converting to
  /// radians before transforming and converting to degrees after.  This can
  /// be disabled by seting `is_degrees=false`
  ///
  /// \param other
  /// \param x
  /// \param y
  /// \param z
  /// \param is_degrees  True if radial units are in degrees.  False if in
  /// radians
  /// \return number of points transformed.
  int transformTo(const Proj& other, QVector<double>& x, QVector<double>& y,
                  QVector<double>& z, bool is_degrees = true) const;

  /// \brief Transform coordinates between projections
  ///
  /// This is a thin wrapper around `pj_transform` that may be more efficient
  /// than using other methods, since you can use custom data packing.  Be
  /// warned, by using raw pointers you lose all the safeguards for bounds
  /// checking!
  ///
  /// Transforms points in place inside the x, y, and z vectors (z being
  /// optional).
  ///
  /// Geographic coordinates require transformations in radians.  If
  /// is_degrees is true this conversion is done for you, both converting to
  /// radians before transforming and converting to degrees after.  This can
  /// be disabled by seting `is_degrees=false`
  ///
  /// \param other       Projection to transform into
  /// \param count       Number of points in each vector
  /// \param offset      Offset from the last value for the next in each vector
  /// \param x
  /// \param y
  /// \param z
  /// \param is_degrees  True if radial units are in degrees.  False if in
  /// radians
  /// \return
  int transformTo(const Proj& other, int count, double* x, double* y,
                  double* z = nullptr, int offset = 0,
                  bool is_degrees = true) const;

  /// \brief Transform coordinates between projections
  ///
  /// Transforms points in place inside the x, y, and z vectors (z being
  /// optional and can be empty).  Transforms the minimum number of points if
  /// the vectors differ in length.
  ///
  /// Geographic coordinates require transformations in radians.  If
  /// is_degrees is true this conversion is done for you, both converting to
  /// radians before transforming and converting to degrees after.  This can
  /// be disabled by seting `is_degrees=false`
  ///
  /// \param other
  /// \param x
  /// \param y
  /// \param z
  /// \param is_degrees  True if radial units are in degrees.  False if in
  /// radians
  /// \return number of points transformed.
  int transformTo(const Proj& other, QVector<double>& x, QVector<double>& y,
                  bool is_degrees = true) const;

  /// \brief Transform from WGS84 lon/lat using this projection
  ///
  /// \param lon
  /// \param lat
  /// \param num
  /// \param is_degrees  True if radial units are in degrees.  False if in
  /// radians
  /// \return
  int transformFromLonLat(int num, double* lon, double* lat,
                          bool is_degrees = true) const;

  /// \brief Transform from WGS84 lon/lat using this projection
  ///
  /// \param lon
  /// \param lat
  /// \param is_degrees  True if radial units are in degrees.  False if in
  /// radians
  /// \return
  int transformFromLonLat(QVector<double>& lon, QVector<double>& lat,
                          bool is_degrees = true) const;

  /// \brief Transform from WGS84 lon/lat using this projection
  ///
  /// This method requires copying the QPointF vector into two vectors of
  /// doubles.
  ///
  /// \param lonlat
  /// \param is_degrees
  /// \return
  int transformFromLonLat(QVector<QPointF>& lonlat,
                          bool is_degrees = true) const;

  /// \brief Transform to WGS84 lon/lat using this projection
  ///
  /// \param lon
  /// \param lat
  /// \param num
  /// \param is_degrees  True if radial units are in degrees.  False if in
  /// radians
  /// \return
  int transformToLonLat(int num, double* lon, double* lat,
                        bool is_degrees = true) const;

  /// \brief Transform to WGS84 lon/lat using this projection
  ///
  /// \param x
  /// \param y
  /// \param is_degrees  True if radial units are in degrees.  False if in
  /// radians
  /// \return
  int transformToLonLat(QVector<double>& x, QVector<double>& y,
                        bool is_degrees = true) const;

  /// \brief Transform to WGS84 lon/lat using this projection
  ///
  /// This method requires copying the QPointF vector into two vectors of
  /// doubles.
  ///
  /// \param xy
  /// \param is_degrees  True if radial units are in degrees.  False if in
  /// radians
  /// \return
  int transformToLonLat(QVector<QPointF>& xy, bool is_degrees = true) const;

  /// \brief Check if a proj4 error has been reported.
  ///
  /// Returns true if getError() would return a proj4 error number and string.
  ///
  /// \return
  bool hasError() const noexcept;

  /// \brief Get the last reported proj4 error generated by this instance.
  ///
  /// If no error has been reported, returns QPair<0, QString()>
  ///
  /// Using Proj::getError() clears the internal error record.
  ///
  /// \return
  QPair<int, QString> getError();

  /// \brief Get the last proj4 error on the provided context.
  ///
  /// The default proj4 context is used by default.
  ///
  /// This static method can be useful to help figure out why creating a Proj
  /// instance from Proj::fromDefinition() failed.  Just be sure to provide
  /// the came context as you did with the previous call.
  ///
  /// \param context
  /// \return
  static QPair<int, QString> lastError(projCtx context = nullptr);

  /// \brief The projection is geographic
  ///
  /// If true, the projection coordinates are geographic (lat/lon)
  ///
  /// \return
  bool isLatLon() const noexcept;

  /// \brief Get the wrapped Proj4 projection object.
  ///
  /// You can use this raw pointer to run other proj4 API methods not covered
  /// by the wrapper
  ///
  /// Note, you should not change the projection context, or free the
  /// projection using this pointer.  These allocated structures are managed
  /// by the wrapping class.
  ///
  /// \return
  projPJ projection() const noexcept;

  /// \brief Get the proj4 string defintion for this projection.
  ///
  /// \return
  QString definition() const noexcept;

  /// \brief Get a specific proj4 parameter
  ///
  /// Proj4 parameters here lack the leading '+' character, so if the
  /// definition contains '+zone=19' you'd use Proj::parameter('zone')
  ///
  /// Returns an invalid QVariant if the desired parameter is missing.
  /// Parameters with no associated value (e.g. +no_defs) return a boolean
  /// set true.
  ///
  /// \param parameter
  /// \return
  QVariant parameter(const QString& parameter) const;

  /// \brief Get all the proj4 parameters
  ///
  /// \return
  QVariantHash parameters() const;

  /// \brief Return the geographic coordinate system underlying this one.
  ///
  /// This is equivalent to the proj4 function: pj_latlong_from_proj
  ///
  /// \return
  std::shared_ptr<Proj> latLongFromProj();

  /// \brief Get the context used by this projection.
  ///
  /// Proj4 contexts are mainly used for thread-safe error reporting.  When
  /// used, it is assumed that each thread will have it's own context
  /// structure to be used with all the projections.  It is unclear what
  /// happens if projections with different contexts are used in the same
  /// transformation.
  ///
  /// Actual transformation operations are protected within proj4 with
  /// mutexes and are thread-safe.
  ///
  /// When in doubt, just use the default context provided by
  /// Proj::defaultContext().  This is the proj4 default global context.
  ///
  /// \return
  std::shared_ptr<projCtx_> context() noexcept;

  /// \brief Set the context used by the projection.
  ///
  /// Proj4 contexts are mainly used for thread-safe error reporting.  When
  /// used, it is assumed that each thread will have it's own context
  /// structure to be used with all the projections.  It is unclear what
  /// happens if projections with different contexts are used in the same
  /// transformation.
  ///
  /// Actual transformation operations are protected within proj4 with
  /// mutexes and are thread-safe.
  ///
  /// When in doubt, just use the default context provided by
  /// Proj::defaultContext().  This is the proj4 default global context.
  /// This is also the default context used by Proj::fromDefinition() if none
  /// is supplied at construction.
  ///
  /// This is equivalent to the proj4 function: pj_set_ctx
  /// \param context
  void setContext(std::shared_ptr<projCtx_> context);

  /// \brief Return the units of the current projection
  ///
  /// \return
  Unit units() const noexcept;
  /// \brief Return the scale factor to convert the projection units to meters.
  ///
  /// For example, if Proj::units()  returns Proj4Unit::km, then
  /// Proj::scaleFactorToMeters() returns 0.001
  ///
  /// \return
  qreal scaleFactorToMeters() const noexcept;

  /// \brief Are two projections the same?
  ///
  /// Uses OGRSpatialRef::IsSame() under the hood.
  ///
  /// \param other
  /// \return
  bool isSame(const Proj& other) const;

  /// \brief Return the geodetic distance between two points
  ///
  ///
  /// Calculates the geodetic distance (and bearings) between two points.
  /// For projected coordinate systems this requires first converting the two
  /// points to geodetic coordinates (lat/lon).
  ///
  /// The returned distance is in meters.
  ///
  /// \param from  (x1, y1)
  /// \param to    (x2, y2)
  /// \param from_bearing
  /// \return  distance
  double geodeticDistance(const QPointF& from, const QPointF& to,
                          double* from_bearing = nullptr,
                          double* to_bearing = nullptr) const;

  /// \brief Return the distance bewteen two points
  ///
  /// Calculates the geodetic distance (and bearings) between two points.
  /// For projected coordinate systems this requires first converting the two
  /// points to geodetic coordinates (lat/lon).
  ///
  /// The returned distance is in meters.
  ///
  /// \param from_x
  /// \param from_y
  /// \param to_x
  /// \param to_y
  /// \param from_bearing
  /// \return
  double geodeticDistance(double from_x, double from_y, double to_x,
                          double to_y, double* from_bearing = nullptr,
                          double* to_bearing = nullptr) const;

  /// \brief Create a new proj4 context.
  ///
  /// Proj4 contexts are used in multi-threaded applications.  The context is
  /// used mainly to provide error statuses in a thead-safe manor.  Contexts
  /// do not seem to be required to safely run transformations in a
  /// multi-threaded applications.
  ///
  /// \return
  static std::shared_ptr<projCtx_> createContext();

  /// \brief Get the default (global) proj4 context
  ///
  /// Returns a shared pointer to the global proj4 context.
  /// \return
  static std::shared_ptr<projCtx_> defaultContext();

  /// \brief Create a UTM projection for the provided lon/lat point.
  ///
  /// \param lon (+ is E)
  /// \param lat (+ is N)
  /// \param context
  /// \return
  static std::shared_ptr<Proj> utmFromLonLat(
    qreal lon, qreal lat, std::shared_ptr<void> context = {});

  /// \brief Createa an AlvinXY projection for the givin origin.
  ///
  /// The AlvinXY projection is based on a transverse mercator projection
  /// (+proj=tmerc) centered at the provided origin (+lon_0=origin_lon,
  /// +lat_0=origin_lat)
  ///
  /// \param lon (+ is E)
  /// \param lat (+ is N)
  /// \param context
  /// \return
  static std::shared_ptr<Proj> alvinXYFromOrigin(
    qreal lon, qreal lat, std::shared_ptr<void> context = {});

  /// \brief Get the units and scale factor from a definition string.
  ///
  /// This method looks through a proj4 definition string and attempts to
  /// recover both the units of the projection and the scale factor needed to
  /// convert these units to meters.
  ///
  /// If this method fails, ok will be set false and a pair {invalid, -1}
  /// will be returned.
  ///
  /// Conventions:
  ///    - if +proj= is geographic, set units to degrees and scale to -1
  ///    - if only +units= is provided, then the scale factor is looked up.
  ///    - if only +to_meters= is provided, then the value is returned as the
  ///        scale factor and the units are set to Units::custom
  ///    - if +units= and +to_meters= are both provided, then both are parsed
  ///        used for the units and scale factor respectively.
  ///    - if +units= cannot be parsed, ok is set false.
  ///    - if +to_meters cannot be converted to a qreal, ok is set false.
  ///
  /// \param definition  proj4 definition string
  /// \param ok
  /// \return
  static QPair<Unit, qreal> scaleFactorFromDefinition(const QString& definition,
                                                      bool* ok = nullptr);

protected:
  /// \brief Protected constructor.
  ///
  /// Users should use one of the 'from*' static methods to create
  /// projections.
  Proj(projPJ proj, std::shared_ptr<projCtx_> context);

private:
  QScopedPointer<ProjPrivate> d_ptr;
};

/// \brief Singleton class for a WGS84 geographic projection
///
/// This class allows easy access to a *global* WGS84 geographic projection
/// instance.  This is a projection created with the definition as defined as
/// PROJ_WGS84_DEFINITION in Proj4Utils.h:
///
///    "+proj=latlon +datum=WGS84 +ellps=WGS84 +towgs84=0,0,0 +no_defs"
///
///
/// To access the transform, use the ProjWGS84::getInstance() method:
///
///    auto& proj = ProjWGS84::getInstance();
class DSLMAPLIB_EXPORT ProjWGS84
{

public:
  // Deleted copy and copy-assign.  You cannot instantiate this class directly.
  ProjWGS84(const ProjWGS84&) = delete;
  void operator=(const ProjWGS84&) = delete;

  /// \brief Get a reference for the WGS84 transform
  ///
  /// Use this method to get a reference to the WGS84 transform instance.
  /// Remember, this reference is global and will be used by any other code
  /// calling ProjWGS84::getInstance()
  ///
  /// To access the transform, use the ProjWGS84::getInstance() method:
  ///
  ///    auto& proj = ProjWGS84::getInstance();
  ///
  /// \return
  static Proj& getInstance();

  /// \brief Calculate the distance between two ponts on the WGS84 ellipsoid
  ///
  /// internally calls `geod_inverse()` on the provided points.  Returned
  /// distance
  /// is in meters.
  ///
  /// \param from_LonLat
  /// \param to_LonLat
  /// \param from_bearing
  /// \param to_bearing
  /// \param is_degrees
  /// \return
  static double geodeticDistance(const QPointF& from_LonLat,
                                 const QPointF& to_LonLat,
                                 double* from_bearing = nullptr,
                                 double* to_bearing = nullptr,
                                 bool is_degrees = true);

  /// \brief Calculate the distance between two ponts on the WGS84 ellipsoid
  ///
  /// internally calls `geod_inverse()` on the provided points.  Returned
  /// distance
  /// is in meters.
  ///
  /// \param lon1
  /// \param lat1
  /// \param lon2
  /// \param lat2
  /// \param from_bearing
  /// \param to_bearing
  /// \param is_degrees
  /// \return
  static double geodeticDistance(double lon1, double lat1, double lon2,
                                 double lat2, double* from_bearing = nullptr,
                                 double* to_bearing = nullptr,
                                 bool is_degrees = true);

private:
  ProjWGS84();
  ~ProjWGS84();

  std::shared_ptr<Proj> m_proj;
};

enum class Unit
{
  km,     ///< Kilometer
  m,      ///< Meter
  dm,     ///< Decimeter
  cm,     ///< Centimeter
  mm,     ///< Millimeter
  kmi,    ///< International Nautical Mile
  in,     ///< International Inch
  ft,     ///< International Foot
  yd,     ///< International Yard
  mi,     ///< International Statute Mile
  fath,   ///< International Fathom
  ch,     ///< International Chain
  link,   ///< International Link
  us_in,  ///< U.S. Surveyor's Inch
  us_ft,  ///< U.S. Surveyor's Foot
  us_yd,  ///< U.S. Surveyor's Yard
  us_ch,  ///< U.S. Surveyor's Chain
  us_mi,  ///< U.S. Surveyor's Statute Mile
  ind_yd, ///< Indian Yard
  ind_ft, ///< Indian Foot
  ind_ch, ///< Indian Chain

  // units below are not part of proj4
  invalid, ///< We're unable to figure out the units of a projection
  degrees, ///< This is a geographic projection
  custom,  ///< Custom scalar unit (should have a +to_meter defined)
};

/// \brief Get the conversion factor to meters for given unit.
///
/// \param unit Proj4 unit type
/// \return
qreal DSLMAPLIB_EXPORT scaleToMeters(Unit unit);

Unit DSLMAPLIB_EXPORT stringToUnit(const QString& string);
}

#endif // NAVG_PROJ_H
