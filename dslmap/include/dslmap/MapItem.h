/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 7/26/18.
//

#ifndef DSLMAP_MAPITEM_H
#define DSLMAP_MAPITEM_H

#include <QGraphicsItem>

namespace dslmap {

///\brief DslMap map item types
///
/// Allows introspection and casting using qgraphicsitem_cast() and
/// qobject_cast()
enum MapItemType
{
  // Individual
  MarkerItemType = QGraphicsItem::UserType + 1, ///< Bare MarkerItem
  BathyObjectType,                              ///< BathyObject raster
  ImageObjectType,           ///< Non-bathy ImageObject rasters
  SymbolGroupObjectType,     ///< SymbolGropuObject, group of MarkerItem's
                             ///< sharing a single Symbol
  SentryTracklineObjectType, ///< Trackline object for sentrytracks
  ContourLineType,           ///< Contour line

  // Layers
  AbstractMapLayerType,     ///< AbstractMapLayer
  SymbolLayerType,          ///< SymbolLayer
  BeaconLayerType,          ///< BeaconLayer
  RasterLayerType,          ///< Non-bathymetry raster (sidescan, images, etc.)
  BathymetryLayerType,      ///< BathyLayer
  NavestLayerType,          ///< Used by plugin navg-navest
  SentryTracklineLayerType, ///< Used by plugin navg/sentrytracks
  ContourLayerType,         ///<
  GeoreferenceImageLayerType,
  GeoreferenceImageObjectType,

  // User type
  UserType, ///< Custom layers beyond this index.
  RosBeaconLayerType,
};
}
#endif // DSLMAP_MAPITEM_H
