/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 8/7/17.
//

#ifndef DSLMAP_MAPSCENE_H
#define DSLMAP_MAPSCENE_H

#include "DslMap.h"
#include "MapLayer.h"

#include <QGraphicsScene>
#include <QLoggingCategory>
#include <QObject>
#include <QUuid>

#include <memory>

namespace dslmap {

class MapScenePrivate;
class MapLayer;
class Proj;
class GdalColorGradient;

enum class Unit;

Q_DECLARE_LOGGING_CATEGORY(dslmapMapScene)

/// \brief A QGraphicsScene extension for map layers
///
/// The MapScene extends QGraphicsScene for use with AbstractMapLayer
/// objects.  Layers can be added and removed from the scene using
/// MapScene::addLayer() and MapScene::removeLayer() respectively.  The
/// layer's unique ID (returned by AbstractMapLayer::layerId()) is used
/// internally to keep track of layer/object parings.
///
/// The other major difference between QGraphicsScene and MapScene are the
/// notion of a map projection and map units.  The MapScene's coordinate
/// system is set using a projection transform.  Any valid Proj4 projection
/// string can be used to change the MapScene's coordinate system (and also
/// that of any layer that's been added to the scene).  If a projection is
/// left unspecified, the MapScene will default to a UTM projection of the
/// proper zone including the mean location of the first layer added.  It's
/// worth repeating:
///
/// The MapScene coordinate system is PROJECTED, NOT GEOGRAPHIC, but the
/// projection can be left unspecified until you add layers to display.  At
/// any time the projection can be changed using MapScene::setProjection, but
/// geographic (lat/lon) projections are *not* allowed.
///
/// Following Proj4 conventions, the units for the projection are assumed to be
/// meters if they are left unspecified in the Proj4 definition string.  The
/// current map projection can be retrieved using MapScene::projection()
class DSLMAPLIB_EXPORT MapScene : public QGraphicsScene
{
  Q_OBJECT
  Q_DECLARE_PRIVATE(MapScene)

signals:
  /// \brief A layer has been added to the MapScene
  ///
  /// The MapScene object retains ownership of the layer.
  ///
  /// \param layer Added layer
  void layerAdded(dslmap::MapLayer* layer);

  /// \brief A layer has been added to the MapScene
  ///
  /// \param uuid Unique identifier of the added layer.
  void layerAdded(const QUuid& uuid);

  /// \brief A layer has been removed from the MapScene
  ///
  /// \param layer Removed layer
  void layerRemoved(MapLayer* layer);

  /// \brief A layer has been removed from the MapScene
  ///
  /// \param uuid Unique identifier of the added layer.
  void layerRemoved(const QUuid& uuid);

  /// \breif The map projection has changed.
  ///
  /// \param definition  A Proj4 string representing the new projection.
  void projectionChanged(const QString& definition);

  /// \brief The shared linear gradient has changed.
  ///
  void gradientChanged();

public:
  /// \brief Registry of UUID-Layer pairings
  using LayerHash = QHash<QUuid, MapLayer*>;

  explicit MapScene(QObject* parent = Q_NULLPTR);

  ~MapScene() override;

  /// \brief Add a Map Layer to the Map Scene
  ///
  /// \param layer object
  /// \return  UUID object identifying the layer
  ///
  /// The scene takes ownership of given layer object.  If adding the layer
  /// fails for any reason the returned UUID object will be null.
  virtual QUuid addLayer(MapLayer* layer);

  /// \brief Remove a Map Layer from the Map Scene
  ///
  /// Removes the layer from the map scene.  Ownership of the layer object is
  /// passed to the caller.
  ///
  /// \param layer  Map layer object
  virtual void removeLayer(MapLayer* layer);

  /// \brief Remove a Map Layer from the Map Scene by UUID
  ///
  /// Removes the layer from the map scene.  Ownership of the layer object is
  /// passed to the caller.  If no matching UUID is found, the returned
  /// pointer will be null
  ///
  /// \param uuid   UUID of the layer to remove
  virtual MapLayer* removeLayer(const QUuid& uuid);

  /// \brief Remove a Map Layer from the Map Scene by name.
  ///
  /// Removes all layers matching the provided name from the map scene.
  /// Ownership of the layer objects is passed to the caller.
  ///
  /// \param name  Map Layer name.
  virtual QList<MapLayer*> removeLayer(const QString& name);

  /// \brief Remove all Map Layers from the Map Scene.
  ///
  /// Removes all map layers map scene.  Ownership of the layer objects are
  /// passed to the caller.
  virtual QList<MapLayer*> removeAllLayers();

  /// \brief Get a listing of all layers in the scene
  ///
  /// The MapScene retains ownership of all layers returned
  ///
  /// \return Listing of all layers in the scene.
  virtual LayerHash layers() const;

  /// \brief Get a listing of all layers matching the provided type
  ///
  /// The MapScene retains ownership of all layers returned
  ///
  /// \param type  layer type to return
  ///
  /// \return listing of all layers matching `type`
  virtual LayerHash layers(MapItemType type) const;

  /// \brief Get layers by Name
  ///
  /// The MapScene retains ownership of all layers returned
  ///
  /// \param name Name of the layer to retrieve
  /// \return layer objects
  virtual LayerHash layers(const QString& name) const;

  /// \brief Get a layer object by UUID
  ///
  /// \param uuid UUID of the desired layer
  /// \return layer object
  ///
  /// The returned layer object will be NULL if `uuid` matches no layers.
  /// The MapScene retains ownership of all layers returned
  virtual MapLayer* layer(const QUuid& uuid) const;

  /// \brief Get UUIDs of layers by name
  ///
  /// \param name Name of layer
  /// \return UUIDs
  virtual QList<QUuid> uuids(const QString& name) const;

  /// \brief Get a copy of the Proj4 projection object
  ///
  /// \return
  virtual std::shared_ptr<const Proj> projection();

  /// \brief Get a pointer to the Proj4 projection
  ///
  /// Avoids creating a new shared_ptr object
  ///
  /// \return
  const Proj* projectionPtr();

  Unit projectionUnit() const;

  /// \brief Get the Proj4 definition string for the scene projection
  virtual QString projectionString() const;

  /// \brief Set the scene projection using a Proj4 definition string
  ///
  /// Geodetic projections are not used (no definitions using +proj=latlon)
  ///
  /// \param definition
  /// \param OK
  virtual bool setProjection(const QString& definition);

  /// \brief Set the scene projection
  ///
  /// Geodetic projections are not supported.  The scene retains a copy of
  /// the proj object.
  ///
  /// \param definition
  /// \param OK
  virtual bool setProjection(std::shared_ptr<const Proj> projection);

  virtual std::shared_ptr<GdalColorGradient> gradient() const;

  virtual bool setGradient(std::shared_ptr<GdalColorGradient>);

protected:
  /// \brief update sceneRect when there's a change
  /// When the items in a scene change, update the
  /// sceneRect to enclose all items plus 10% on each side
  /// triggered by signal QGraphicsScene::changed()
  void extendSceneRect();

  /// \brief Protected constructor for subclasses.
  ///
  /// Classes inheriting from AbstractMapLayer can use this protected
  /// constructor to correctly instantiate the PIMPL object.  See
  /// https://wiki.qt.io/D-Pointer for more info.
  ///
  /// \param pimpl    PIMPL object inheriting from AbstractMapLayerPrivate
  /// \param parent   Parent graphics item.
  explicit MapScene(MapScenePrivate& pimpl, QObject* parent = Q_NULLPTR);
  QScopedPointer<MapScenePrivate> d_ptr; ///< Private IMPL
};
}

#endif // DSLMAP_MAPSCENE_H
