## What is the purpose of these scripts?

The ubuntugis-unstable ppa for Ubuntu 20.04 has been found to cause compatibility issues with
NavG3. An update has been applied where NavG will no longer build with the unstable ppa by design.

If you are using the mentioned ppa, and would like to downgrade to a compatible version...
some handy scripts to do so are provides here.

*** Warning. Please review the scripts before applying actions. These will use apt and aptitude to 
remove and install system packages. Some of the removed packages may also be part of a wider package group. 
One such example is "ros-noetic-desktop-full". The packages removed by these scripts may also associate with other
packages from that group, and REMOVE them. 

*These scripts are for user convenience of downgrading system packages that were part of a either the ubuntugis-stable,
 or ubuntugis-unstable ppa, use at your own risk. Thank you!
 
### How to downgrade ubuntugis-stable or ubuntugis-unstable ppas easily to mainline ubuntu 20.04 repos.
Run the following

sudo sh remove_ppas.sh
sudo sh reinstall.sh
