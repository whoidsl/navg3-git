#NavG TODO

- Finalize Plugin Architecture
  - Multiple kinds of Plugin(?)
    - Map-related:  Helps interact with the map.  Adds new features to the map
    - IO Related: Adds new inputs to the map
    - Non-windowed?  How do we handle plugins that don't need a window, or for which a window
      isn't necessary.
  - Settings Persistence
  - Hiding/Showing windows.

- NavG Settings
  - Two-stage process?
    - Machine written settings (QSettings output)
    - Human written settings override?
    
  - Default settings?
  
  
- NavG Core Plugins
  Required for reproducing existing capability
  
  - Layer Widget
    - Displays available layers and provides access to customization dialog
    - Methods to add new layers (bathymetry, targets, etc.)
    - Methods to hide/show layers
    - Method to reorder "Z-depth" of layers.
  
  - Navest VPR/VFR
    - Receive/Parse VFR/VPR strings from Navest
    - Create beacon trail layers for each provided ID
    - Allow settings names, colors, trail length, etc. from GUI (though layer widget?)
      - Provide ability to Save/Restore settings
    
  - Target Import/Export
    - Achieved through layer widget?
    - Hooks into already-developed Target Import dialog
    - Target export:  save set(s) of targets to file

  - Bathymetry Import/Export
    - Acheived through layer widget?
    
  - Range/Bearing calculation/interaction with map.
    - Additional hooks on mouse interaction with map.
    
  - Map Graticule Toggle (Alvin X/Y,  UTM, Lat/Lon)
  
