# NavG Application + Mapping Library Project Plan

This document provides a brief project sketch for the proposed update to the NavG real-time navigation application used by the NDSF vehicles Alvin, Jason, and Sentry.  Within this document the term **Library** refers to the core mapping library that will be responsible for displaying the real-time map and it's associated layers of bathymetry, targets, vehicle tracks, etc.  The term **Application** refers to the user application that will incorporate the Library as well as a host of user and vehicle-specific control widgets.  Using these terms the current iteration of NavG is both the Application and the Library due to the fact that the map display and the application logic are inseparable.

Key decision points or development topics are listed below.  An approximate estimate of the difficulty in resolving each task is provided in parenthesis after each title.  This task list is *not* meant to be exhaustive, but should encompass all of the major "known-unknowns": those tasks that I acknowledge not having a firm grasp of the actual difficulty of implementation, and represent areas of Qt that I have little or no practical experience in.  

**Target Platform and Development Tool-chain  (easy):**  A target platform and development toolchain should be identified before any serious code is written, but *after* serious thought is given to the desired architecture of the Library and Application.  Minimum verions of Qt, C++ standard, and Linux distribution are required at the least.  These choices will drive subsequent requirements for the compile version and other various system libraries.  This target is labeled 'easy' due to the fact that I mostly have these specifications outlined already, but they should be passed around the group.

**QWidget/QML paradigm choice (moderate):**  Qt is moving towards a different design paradigm using 'QtQuick', which uses QML as a declarative layout model much different that what we are used to.  The main possible benefit of the new scheme is the OpenGL-optimized "Scene Graph" used to draw the applications which is purportedly much more efficient that the traditional QGraphicsView-based model.  However, as of Qt 5.9, QML/Scene Graph does not seem viable for a complex desktop application such as NavG.  This combined with the difficulty in learning a large chunk of new syntax has me leaning against adoption at this point.  This decision needs to be discussed amongst other primary software developers.

One option is to design the Library using QGraphicsView and the individual dialogs/control widgets using QML.  It appears *very* easy to design 'static', stand-alone interfaces with QML and Qt has the ability to incorporate QML 'views' within traditional Qt widgets.

Resources:

- "Should you still be using QGraphicsView?": <http://blog.qt.io/blog/2017/01/19/should-you-be-using-qgraphicsview/>
- "The Curse of Choice (Video)": <https://www.youtube.com/watch?v=WIRRoPxIerc>

**Library Importing of Bathymetry (easy):**  Importing bathymetry from native formats is a requirement for the Library.  The "GDAL" library can import most common native formats.

Resources:

- "GDAL": <http://www.gdal.org/>

**Library Displaying of Bathymetry (moderate):**  There are at least two options for how the bathymetry data is displayed by the library.  The current NavG application uses OpenGL Textures and previous work investigating NavG upgrades have used QPixmap.  Both have their benefits and drawbacks.  If one method is not obviously better than the other, careful design of the Bathemtry layer implementation should make it possible to switch between Textures and Pixmaps at runtime or with minimal coding effort.

**Library Symbol Hierarchy (moderate):**  The map library will be required to display a large number of symbolic markers, updating in real-time, with various annotations and scaling effects.  Regardless of the decision to pursue a "traditional" QGraphicsView-based implementation or use the newer QtQuick style implementation, the core symbol hierarchy will need to be both computationally efficient while providing expressive behaviors.  Prior work has provided a fair amount of insight using traditional QGraphicsItems for use with the QGraphicsView method, but no equivalent experience is available for the newer "Scene Graph" methods using QSGNode objects.

**Library public API (easy-moderate):**  Providing a public API for interacting with the library will serve to decouple the map display logic from the Application logic as well as enable the map library to be used in future software applications with minimal extra effort.  Creating an API Interface object in Qt is straight forward; the hard part will be deciding how that API is expressed.  The proposed API will likely change while the mapping library is under development and should not be seen as blocking the development of the Application itself.

**Application layout modifiable by users (easy):**  A major source of user feedback from NavG involves the inability to change various widget sizes or locations, or even wanting to hide certain widgets that users don't care about.  This is impossible in the current NavG without editing the source and recompiling; which may affect users of other vehicle systems.  Qt already supports the machinery for dockable widgets; widgets that you can move and "dock" around the boarders of the main window, or pop out of the main window, or hide entirely.  Moving to such a system for the Application would enable users to customize their experience do to their personal needs.  The application would obviously have to provide methods to restore hidden widgets.

**Application plugin interface (hard):**  Inserting a new control is one of the major difficulties in supporting and extending the current NavG application.  Where the control is initialized, how it interacts with other controls, and where it can fit on the screen are all fragile decisions that require a fair amount of trial and error to get working and are subject to breaking when other unrelated code is modified.  There is no ability to change the application layout to suit user preferences without editing the source code and recompiling.

A possible alternative strategy would see the various control widgets developed as separate plugins that are available to the Application at run-time.  The Application would provide plugins a means to discover what data resources are available and ways to communicate with both the map itself and other loaded plugins.  Separating the vehicle-specific control logic from the Application simplifies development of the control widgets and the Application itself.  This task is labled 'hard' more due to unique challenge of designing this decoupled architecture, but there are examples to draw inspiration from.  For example, a 'resource interface' to the control widgets could be modeled after the nUI core 'register/unregistered' LCM framework, and Qt supports extending applications through a plugin interface already.

Developing this interface can proceed independently of the mapping engine.  Toy examples can be created with mocked-up control widgets as a stand-alone investigation with the knowledge gained applied to the Application later.

Resources:

- "Qt Plugins": <http://doc.qt.io/qt-5/plugins-howto.html>
- "Qt Plugin Example": <http://doc.qt.io/qt-5/qtwidgets-tools-plugandpaint-app-example.html>

**Application configuration (moderate-hard):**  This task is labeled 'moderate-hard' due to the complications added by having the control widgets loaded as plugins and having the main Application layout modifiable by the user, but the underlying methods of saving and restoring layouts is already present in Qt.  Some way of passing configuration settings to the control widget plugins will be needed since those widgets are now decoupled from the main Application.  This should be handled when the widgets are loaded via. the plugin interface.

Resources:

- "Qt QMainWindow saveState method": <http://doc.qt.io/qt-5/qmainwindow.html#saveState>

**Touch-enabled Application (moderate-hard):**  The Application *must* be usable from an interface lacking a physical mouse and keyboard to be used within the Alvin submersible.  Touch integration should be one of the primary focuses of both the Library and Application development.

Resources:

- "Qt Virtual Keyboard":  <http://doc.qt.io/qt-5/qtvirtualkeyboard-index.html>
- "Qt on embedded (tips for touch-enabled devices)": <http://doc.qt.io/qt-5/embedded-linux.html>
- "Qt Touch events": <http://doc.qt.io/qt-5/qtouchevent.html>

