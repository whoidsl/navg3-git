# NavG version 3

Nothing is here yet except a few documents and toy examples.

## Subdirectories

- `planning`:  various planning documents
  - [User Stories](docs/planning/user_stories.markdown)
  - [Project Planning](docs/planning/project_plan.markdown)
  - [Architecture Outline](docs/planning/architecture_design.markdown)
- `playground`: playground for testing NavG 3 ideas.
