# User Stories for Development

This is a slightly organized collection of user stories for the development of the next version of 
`NavG`.  The development effort will be split between two projects:  a core, reusable mapping engine
libarary and the primary user application `NavG`.


# Uncategorized
These stories have yet to be organized.


# Mapping Library
These stories are to be implemented within the core mapping library and availble to any application.

- As a user, I can import existing bathymetry from native formats and adjust it's display properties.
- As a user, I can interactively add and modify targets and their associated metadata.
- As a user, I can quickly check a tracked marker's position, depth, altitude, and heading.
- As a user, I can designate external sources of geo-referenced data to be added to the map in real-time.
- As a user, I can track the position of an target over time and see the history of position fixes with any associated metadata.
- As a user, I can change the length of a beacon track by marker count or time.
- As a user, I can retrieve the depth of a point beneath the cursor if bathymetry information is available.
- As a user, I can retrieve the geospatial coordinate of a point beneath the cursor (lat/lon, UTM, AlvinXY)
- As a user, I can produce range/bearing indicators for designated pairs of targets.
- As a user, I can interactively show the range and bearing to two arbitrary points on the map
- As a user, I can interact with the map in an intuitive way with mouse and touch controls.
- As a user, I am able to categorize existing targets in to groups and easily modify those groups.
- As a user, I can import lists of targets and metadata from delimited sources (e.g. CSV files)
- As a user, I can organize displayed information into layers and change the layer display order and name.
- As a user, I can display a coordinate grid on the map using Lat/Lon, UTM, or Alvin XY systems.
- As a user, I can determine how a target's draw size changes with the map scale and choose between 
- As a user, I can see the bathymetry profile along a track drawn with the mouse.
  scale-invariant, physical-size, and minimum-drawn-size.
- As a user, I can choose the projection to use for the displayed map.
- As a developer, I access all desired map objects/settings through a well-documented API.

# NavG
These stories are to be implemented specifically within the NavG application.

- As a user, I can look at the display and instantly tell the status of the vehicle:  
  depth, heading, position relative to the ship / depressor / terrain / targets, and (if available) altitude.
- As a user, I can enable a compass-rose display for the vehicle and designate targets to be shown around the perimeter.
- As a developer, I can create modular tools to interact with the map.
- As a user, I can restore the configured state of the application upon restarting
- As a user, I can save the data shown and restore it at a later time.
- As a user, I can save and load different application profiles using human-editable configuration files.
- As a Jason user, I can mark a "drive-to" target and enable/disable "drive-to" mode.
- As a Jason user, I can mark a "watch annulus" attached to the wire end.
- As a Jason user, I can reset the DVL position to a desired fix location.
- As a Jason user, I can modify the active DVL's angular offset.
- As a Jason user, I can choose the primary DVL source.
- As a Jason user, I can command the ship's DP system.
- As a Alvin user, I can interact with the application using touch controls without a mouse or keyboard.
- As a Sentry user, I can visualize the shifted tracklines before applying a shift.
- As a Sentry user, I can send accoustic commands to the vehicle from NavG (e.g. shift, abort, etc.)
- As a Sentry user, I can add and modify simple mission blocks on the map.
- As a Sentry user, I can track the mission progress of the vehicle while underway.
- As a Sentry user, I can easily compute navigation shifts for the vehicle.
- As a Sentry user, I can easily estimate the time to surface/time to botom for the vehicle as it ascends and descends.
- As a Sentry user, I can show vehicle trends over time (e.g. depth, altitude, speed)
- As a user, I do not want to be prevented from interacting with the map due to pop-ups or dialog boxes.
