# NavG Application + Mapping Library Architecture Outline

This document presents a few "pre-development" decisions that must be made before any code is written, as well as a rough
implementation guideline for both the Mapping Library and NavG application.  It is intended to be a sketch of desirable
practices and features, not as a concrete roadmap of code for the final products.  It is expected that this document
will need to be revised as the project progresses as new features are added or desired capabilities are
refined.

## Major pre-development Decisions

These are a few decisions that will drive large portions of the overall project architecture once they're made.

### Toolchain Specification

- Ubuntu target:  14.04 minimum, 16.04 desired.
- Qt Version: 5.9.  Stand-alone installer, tagged as LTS:  <https://www.qt.io/qt5-9/>
- C++ Standard: C++11 minimum, C++14 desired.  Must use `clang` compiler ver. 3.8 and newer with ubuntu 14.04 to enable 
  C++14, see: <http://en.cppreference.com/w/cpp/compiler_support>
- cmake or qmake:  CMake prefered; don't know of a hard minimum-version requirement.

### QWidget vs. QtQuick/QML

QtQuick/QML is being pushed as the "Future" of Qt, with QWidgets seen as done and entering a maitenence mode of upkeep.  
The new "Sceen Graph" drawing backend associated with QtQuick promises better hardware acceleration (it's ONLY OpenGL, 
no software), but is still in flux.  Additionally, much of the QtQuick/QML focus is on tablet/mobile platforms with 
less emphasis on complex desktop applications.  The promise of greater GUI performance is attractive, however there are 
some major apparent drawbacks to adopting the new QtQuick paradigm:

- "Hover ToolTips" only on QQuickItem-based objects, which are heavy QObject-based classes.  Performance wise it is 
impractical to have "many thousands" of QtQuickItems just as it is to have many QGraphicsObject items.
- No easy way to draw text labels without again resorting to QQuickItem-based objects.
- No "QPathItem" equivalent for the QSGGeometryNode (the QGraphicsItem equivalent); custom geometry is harder to 
implement (but not impossible).  See <http://doc.qt.io/qt-5/qtquick-scenegraph-customgeometry-example.html>
- Maybe use QQuickPaintedItem?  It uses standard QPaint-based drawing methods at some performance cost.
- No docking widget support within QML at the moment.  Apparently there are hacks to get around this, but why settle for hacks?

The first two points alone seem to rule out using the newer display model for the mapping library.  Having light-weight 
scene objects is an absolute must for the map display.  The lack of docking support seems to rule out using QML for the 
wrapping NavG application, though it may be possible (and even preferable) to design the individual control widgets 
using QML.

Resources:

- "Should you still be using QGraphicsView?": <http://blog.qt.io/blog/2017/01/19/should-you-be-using-qgraphicsview/>
- "The Curse of Choice (Video)": <https://www.youtube.com/watch?v=WIRRoPxIerc>

### NavG Control Widgets as Application Plugins

Inserting a new control widget is one of the major difficulties in supporting and extending the current NavG 
application.  Where in the code the control is initialized, how it interacts with other controls, and where it can fit 
on the screen are all fragile decisions that require a fair amount of trial and error to get working and are subject to 
breaking when the code is modified.  There is no ability to change the application layout to suit user preferences 
without editing the source code and recompiling.

A possible alternative strategy would see the various control widgets developed as separate plugins that are available 
to the NavG at run-time.  The NavG would provide plugins a means to discover what data resources are available and ways 
to communicate with both the map itself and other loaded plugins.  Separating the vehicle-specific control logic from 
NavG simplifies development of the control widgets, NavG itself, and provides a means to prevent code implemented for 
different vehicles from inadvertently clashing.  Enabling such behavior would require defining at least one Qt 
'Interface' against which new control widgets are designed.  There are plenty of examples describing how to go about 
this.  Developing this interface can proceed in loose collaboration with the mapping engine.

A trickier task is designing a way for various control widgets to advertise "abilities" that other widgets can use or 
subscribe to.  Ideally communication between everything should be using Qt's signals and slots, so there needs to be a 
method of dynamically connecting various widgets at compile time.  Modeling this behavior after a pub/sub model, or the 
ROS idea of 'topics' might be the way to go, but the actual implementation is completely up in the air at this time.  
Toy examples can be created with mocked-up control widgets as a stand-alone investigation with the knowledge gained 
applied to the NavG later in the development process (see next section)

Resources:

- "Qt Plugins": <http://doc.qt.io/qt-5/plugins-howto.html>
- "Qt Plugin Example": <http://doc.qt.io/qt-5/qtwidgets-tools-plugandpaint-app-example.html>


#### NavG Plugin Example Implementation

See the `widgets_as_plugins` example in the `playground/` directory.  This is meant to be a toy example,
a proof-of-concept, and not a final plugin implementation.  It does allow separately-designed widgets to be connected
together by NavG without any additional coding beyond the widgets themselves.
 

## Mapping Library Implementation

### Stable ABI via. PIMPL/D-Pointer

Maintaining a stable library ABI ensures that updates/fixes within the map library do not require recompiling/relinking 
dependant applications (NavG).  There are a few general guidelines for preserving the library ABI, and there are many 
gotchas and pitfalls.  The problem can be greatly simplified by following the PIMPL or D-Pointer architecture which 
hides the actual implementation within private code at the cost of extra memory allocations.

This is probably feasible for most hierarchy objects EXCEPT the basic map symbol class, for which having many thousands 
of such objects should not be an unexpected occurrence.  Having to allocate the private logic on the heap with each 
instantiation is probably not the greatest idea.  Keeping the symbol class as a concrete object can be done while 
preserving ABI, one just needs to be careful.

- "Policies/Binary Compatibility Issues With C++": 
  <https://community.kde.org/Policies/Binary_Compatibility_Issues_With_C%2B%2B>
- "Compilation Firewalls": <https://herbsutter.com/gotw/_100/>
- "D-Pointer pattern": <https://wiki.qt.io/D-Pointer>
- <http://stackoverflow.com/questions/25250171/how-to-use-the-qts-pimpl-idiom>

### Dialogs Included

The library should provide intuitive dialogs for changing or editing properties of layers and otherwise interacting
with the map.  For example, a  "Load Targets From File" dialog should be provided by the library in addition to the
the actual hooks to implement the feature.

### Extendable Layers

The library should provide a variety of building blocks for layers, allowing new layer types to be easily composed of
multiple primitives.  For example, a "Vehicle Trail" layer may consist of two distinct "blocks":  A vehicle shape marker
that is updated with each new position fix, and a group of past fixes that all share the same marker symbol.  This "group
of fixes sharing a marker symbol" could be the same base layer type used when loading pre-existing targets from a file.

### Map API Overview Draft

This is a rough draft of the capabilities a mapping library interface should expose.  It should not be taken as a 
comprehensive and final list.

- MapScene
    - Add/Remove Layer
    - Reorder Layer
    - Show/Hide Layer
    - Get Layer by ID (for modification)
    - Show/Hide Overlay
    - Edit Overlay
    - Show/Hide Cursor
    - Get/Set Cursor

- All Layers
    - Set/Get name
    - Get ID
    - Get/Set display order

- Raster Layer
    - New From Bathymetry (GRD, XYZ, etc.)
    - Set/Get Extents (X, Y, and Z)
    - Set/Get colorscale

- Symbols
    - Get/Set shape
    - Set/Get Pen (stroke color, thickness, etc.)
    - Set/Get Brush (fill color, pattern, etc)
    - Set/Get Tooltip
    - Enable/Disable movable.
    - Set/Get local transform (Scale, rotate, etc.)
    - Get/Set scalability (Physical-sized, scale invariant, minimum-size, etc.)
    - Get/Set label

- Geometry Layer
    - Add geometry (e.g. point, line)
    - Get/Set geometry symbols

- Trail Layer
    - Composite layer of a marker symbol and a list of trail symbols.
    - Set/Get Marker name
    - Show/Hide marker name
    - Set/Get Marker shape
    - Set/Get symbol shape
    - Set/Get maximum trail history
    - Set/Get shown trail history

- Tracks Layer

- Overlay
    - Enable/Disable grid
    - Set grid type (Geographic, UTM, XY)

## NavG Application Overview
![Architecture Overview Image](navg3_architecture_overview.svg)

Key Concepts:

  - Widgets are constrained to specific functional logic
  - Widgets implement at least one of the `Plugin Interface(s)`, through which NavG can coordinate inter-widget 
    communication
  - The Map implements the `Map Interface`, through which Widgets can interact with map objects.
  - NavG provides Widgets with desired configuration and placement at run time.
  - NavG provides Widgets with the active `Map Interface`
  - **NO DIRECT INTERACTION** between individual Widgets and the actual Map View (only through the `Map Interface` 
    abstraction)
  - **NO DIRECT INTERACTION** between individual Widgets and the NavG application (only through the various 
    `Plugin Interface(s)` abstraction)

### NavG as a Limited Container

Following the 'plugin' approach above, NavG itself (in absence of any modular control widgets) would be reduced to a 
container with a limited scope of tasks:

- Exposing the map library API to the user and control widgets.
- Configuring, Saving, and Restoring the desired set of control widgets, their configuration state and their layout 
  within the application.
- Providing message passing 'hub' or other method through which widgets can interact with each other and the map.

That's it.  Under this model, all specialized application logic (parsers, receiving/sending messages on the network, 
etc.) should be contained within plugin widgets with very specific scope.

Conceptually this is not all that different than what we could get to with NavG as it exists now.  We could spend time 
to further isolate the various widgets in an attempt to improve the maintainability of loading the correct widgets for 
each use case.  However this requires knowledge of not only how each widget works but also which widget is used for
each vehicle and *how* each widget is used for each vehicle.  

By using a plugin architecture, developers are forced to design widgets that conform to an agreed upon set 
of behaviors defined by the interface itself.  This interface can also define how various widgets can interact with 
each other, leaving plumbing logic to NavG itself instead of the developer.  All of this simplifies the development
of new features for NavG as well as eases the maintainability of the code moving forward.


### NavG Plugin Example Story

As an example, consider how we currently get information from Navest into NavG:  there is a hard-coded socket within 
the main NavG widget that handles listening and parsing the VPR/VFR messages, then adding them to a prebuilt array of 
target trails according to "navest id".  In the plugin model, all the Navest-related *listening* and *parsing* code is 
self-contained within it's own widget.  That widget is provided access to the map API and can use that 
it to populate the trails on the map.  This widget would make available several signals or channels through which 
navest-related messages and data products could be used by other widgets.  Since the "navest position source" widget is 
self-contained, all of it's various behaviors can be tested in isolation without having to start up the whole NavG 
application.

If later we want to parse raw NMEA sources and have them displayed on the map as well, then all that would require is 
developing a new widget with similar behaviors but distinct parsers for the new position sentences.  This new ability 
can be developed and tested in complete isolation without having to modify any other code beyond making the new plugin 
available to the NavG application when ready. 

### NavG Plugin API

A general plugin interface needs to provide at least the following:

  - A method to load settings to put the plugin into a desired configuration.
  - A method to save settings in order to later restore the current configuration.
  - A method to register interest in communicating with other plugins signals/slots
  - A method to hook into the map widget.
  
Some questions that don't quite need answering yet:

  - Do we want multiple types of plugins to allow specialization while maintaining some
    sense of categorization?  (e.g. a "CorePlugin", "WidgetPlugin", "PositionSourcePlugin", etc.)
    
  - Do we want some plugins to have no visible window?  Again, the position source example from
    above doesn't really need it's own visible window as long as it's data is available to other
    widgets.

Resources:

  - "Qt Plugins": <http://doc.qt.io/qt-5/plugins-howto.html>
  - "Qt Plugin Example": <http://doc.qt.io/qt-5/qtwidgets-tools-plugandpaint-app-example.html>
  - `playground/widgets_as_plugins` toy example in this repo is a proof-of-concept.

### Widget Layout

NavG widgets will be wrapped as `QDockWidgets`.  This allows widgets to be moved around by users into holding 
containers around the boarder of the NavG application. This also enables the layout of the widgets to be stored within 
the NavG configuration file (see `QMainWindow::saveState` and `QMainWindow::restoreState`).  The idea here is that how 
NavG looks to each user can be determined *by the user*, instead of hard-coded within the NavG application.  Different 
stock layouts will be provided (e.g. for Sentry, various Jason roles, Alvin).

Resources
  - `playground/widgets_as_plugins` toy example in this repo is a proof-of-concept.

### Touchscreen Integration (For Alvin)

Touch events should be supported by all widgets and the map for Alvin use.  I don't have much to add here except that
this is probably tricky.

- Qt Virtual Keyboard:  <http://doc.qt.io/qt-5/qtvirtualkeyboard-index.html>
- Qt on embedded (tips for touch-enabled devices): <http://doc.qt.io/qt-5/embedded-linux.html>
- Qt Touch events: <http://doc.qt.io/qt-5/qtouchevent.html>
