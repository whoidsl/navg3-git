# Using NavG

## Initial Setup
The first time NavG is started we will be presented with a small, not-very-useful window containing an empty map
and not much else.  This is NavG's default state.  NavG by itself is not much more than a simple map display, without
any way to report incoming data.  That functionality is the job of NavG's plugins, which we'll set up in the next
section.

Once we have the desired plugins loaded and configured we'll want to adjust 

### Loading Desired Plugins
To add functionality to NavG we need to load the desired set of plugins, and to do that we consult the list of 
available plugins.   In the `Plugins` menu, select `About Plugins...` to bring up a dialog window showing all of the 
available plugins.  Select the plugins that we would like to load by enabling the check marks to the left of their 
name, then select `OK`.

Nothing much will seem different, but if we go back into the `Plugins` menu we will have new menu entries, one
for each of the just-loaded plugins.  If the plugin provides a dockable widget there will be a checkable `Visible`
menu entry for that plugin.  To display the widget, ensure the `Visible` is checked.  Plugins may have more dialog
windows besides just the docked widget; many have Settings dialog boxes that allow changing how the plugin behaves.
Be sure to examine the plugin's menu options to make sure everything is set up as we need (port numbers, for example).

### Adjusting NavG's Lawet

So now we have all the plugins loaded that add the functionality that we need, but NavG does not look the way we want.
Probably all of the plugin widgets are crammed on one side, making things unreadable or unusuable.  Now is the time 
when we can change how the plugin windows are arranged.  You can drag the individual widgets to any side of the map,
top, bottom, left or right.  You can float windows so that they're not attached to a boarder at all.  You can group
windows together such that only one is visible at a time, selected by tabs.  Or you can hide the window all together.

Drag things around, set up the application in a way that makes the most sense.  Once we are satisfied, move to the next
section.

### Saving the Current Layout

TODO:  Add step explaining how to lock a NavG layout once that feature has been added.

Once all the plugins are loaded, set up, and laid out it is time to save the NavG settings.  NavG settings include both
the individual plugin settings (such as port numbers) **AND** the application window layout into a plain text file that
is easily version controlled.  Additionally, NavG will automatically try to load the last settings file it used, so we
do not have to load the same settings file each time we start NavG.  NavG was designed with the intention that it should
"just work" after the initial setup is done, with minimal user effort.

To save the settings, use `Settings -> Save As...` and give it a location and filename.  Note that this *does not* set
the newly saved file to automatically load upon restart (incase you just wanted to save an alternate layout).  To set
the config file that will be loaded on start, use `Settings -> Import...` and select the desired file.  NavG will prompt
you for a restart, after which NavG should come back up with the desired layout.  If NavG fails to come up properly,
try restarting manually.  There seems to be a bug with the default Ubuntu 16.04 window manager (Unity) that prevents
this behavior for working as intended (it works with the Gnome desktop).

## Managing NavG Layouts

NavG settings are saved as plain-text, ini-style files.  While they can be edited by hand there are a few peculiarities
in how Qt handles saving array values that are easily messed up by human edits.  Other values, such as the window layout,
are unintelligible ascii strings that are completely opaque to users.  But it is all plain text, so it can all be easily
version controlled.

NavG is *designed* to be configured through the GUI.  It *should* be possible to take someone else's saved config file
and use it to bring up an identical session of NavG *as long as all the required plugins are installed*.
