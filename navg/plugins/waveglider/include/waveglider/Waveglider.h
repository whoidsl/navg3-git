/**
 * Copyright 2021 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef NAVG_WAVEGLIDER_H
#define NAVG_WAVEGLIDER_H

#include <dslmap/MapScene.h>
#include <dslmap/MarkerItem.h>
#include <navg/CorePluginInterface.h>

#include <QAction>
#include <QHash>
#include <QHostAddress>
#include <QJsonObject>
#include <QLoggingCategory>
#include <QObject>
#include <QPushButton>
#include <QTime>
#include <QWidget>
#include <QtNetwork>
#include <QLineF>
#include <QTableView>
#include <QUuid>
#include <dslmap/MapItem.h>
#include <dslmap/MapLayer.h>
#include <dslmap/MapScene.h>
#include <dslmap/Proj.h>
#include <math.h>
#include <memory>
#include <navg/CorePluginInterface.h>
#include <../navest/Navest.h>
#include <../navest/NavestLayer.h>
#include <navg/NavGMapView.h>
#include <navg/NavGMapScene.h>
#include "WavegliderSettings.h"
#include <QMessageBox>

#include <memory>


namespace navg {
namespace plugins {
namespace waveglider {

namespace Ui {
class Waveglider;
}
class Waveglider
  : public QWidget
  , public CorePluginInterface
{
  Q_OBJECT
  Q_INTERFACES(navg::CorePluginInterface)
  Q_PLUGIN_METADATA(IID CorePluginInterface_iid FILE "Waveglider.json")

signals:

public:
  explicit Waveglider(QWidget* parent = nullptr);
  ~Waveglider() override;

  //
  // CorePluginInterface Implementation
  //
  void loadSettings(QSettings* settings) override;
  void saveSettings(QSettings& settings) override;

  void connectPlugin(const QObject* plugin,
                     const QJsonObject& metadata) override;
  void setMapView(QPointer<dslmap::MapView> view) override;
  QList<QAction*> pluginMenuActions() override;
  QList<QAction*> pluginToolbarActions() override { return {}; }

  using LayerHash = QHash<QUuid, dslmap::MapLayer*>;

public slots:
  /// \brief Show the settings dialog
  ///
  void showSettingsDialog();

protected:
protected slots:

  // void handleMessage(int, int, int);
  void receivePositionFix(const QPointF fix);
  void setupFixes();

  void sceneLayerAdded(dslmap::MapLayer* layer);

  void setCursorWaypoint();
  void powerOnWavegliderUmodemFunction();
  void powerOffWavegliderUmodemFunction();
  void enableWaveglider();

  double calculateRange(const QPointF source_fix, const QPointF dest_fix);
  double calculateBearing(const QPointF source_fix, const QPointF dest_fix);

private:
  //std::unique_ptr<Ui::Waveglider> ui;
  QHostAddress host_address;
  QString address;
  int port=13044;

  //navest ids
  int waveglider_id = 5;
  int ship_id = 2;

  QPointer<QUdpSocket> socket;

  Ui::Waveglider* ui;
  QPointer<dslmap::MapView> m_view;
  QPointer<navg::NavGMapView> n_view;

  QAction* m_action_show_settings = nullptr;

  // tracked latest current position fixes
  QPointF waveglider_position;
  QPointF ship_position;
};

// Q_DECLARE_LOGGING_CATEGORY(plugin_waveglider)
} // waveglider
} // plugins
} // navg

#endif // NAVG_WAVEGLIDER_H
