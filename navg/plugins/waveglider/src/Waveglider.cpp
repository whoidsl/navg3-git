/**
 * Copyright 2019 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "Waveglider.h"
#include "ui_Waveglider.h"

#include <QFileDialog>
#include <QSettings>
#include <QWheelEvent>

namespace navg {
namespace plugins {
namespace waveglider {

Q_LOGGING_CATEGORY(plugin_waveglider, "navg.plugins.waveglider")

Waveglider::Waveglider(QWidget* parent)
  : QWidget(parent)
  , ui(new Ui::Waveglider)
  , m_action_show_settings(new QAction("&Settings", this))
{
  ui->setupUi(this);
  socket = new QUdpSocket(this);

  connect(m_action_show_settings,
          &QAction::triggered,
          this,
          &Waveglider::showSettingsDialog);
}

Waveglider::~Waveglider()
{
  delete socket;
}

void
Waveglider::showSettingsDialog()
{

  QScopedPointer<WavegliderSettings> dialog(new WavegliderSettings);

  dialog->setAddress(address);
  dialog->setPort(port);

  dialog->setWGId(waveglider_id);
  dialog->setShipId(ship_id);

  dialog->setModal(true);
  if (!dialog->exec()) {
    return;
  }

  address = dialog->getAddress();
  port = dialog->getPort();

  waveglider_id = dialog->getWGId();
  ship_id = dialog->getShipId();
}

void
Waveglider::setMapView(QPointer<dslmap::MapView> view)
{
  m_view = view;
  n_view = qobject_cast<NavGMapView*>(view);
  auto scene = m_view->mapScene();

  connect(scene,
          static_cast<void (dslmap::MapScene::*)(dslmap::MapLayer * layer)>(
            &dslmap::MapScene::layerAdded),
          this,
          &Waveglider::sceneLayerAdded,
          Qt::QueuedConnection);
  connect(ui->wg_cursor_button,
          &QPushButton::clicked,
          this,
          &Waveglider::setCursorWaypoint);

  connect(ui->wg_reenable_button,
          &QPushButton::clicked,
          this,
          &Waveglider::enableWaveglider);

  connect(ui->wg_umodem_off_button,
          &QPushButton::clicked,
          this,
          &Waveglider::powerOffWavegliderUmodemFunction);

  connect(ui->wg_umodem_on_button,
          &QPushButton::clicked,
          this,
          &Waveglider::powerOnWavegliderUmodemFunction);

  setupFixes();
}
void
Waveglider::enableWaveglider()
{
  char wgcmd[128];
  int len = snprintf(wgcmd, 128, "WGCMD ENABLE");

  host_address.setAddress(address);
  const int result = QMessageBox::warning(
    this,
    "WaveGlider uModem",
    QString("Sending (re)enable waveglider coordination"
            "\nProceed?"),
    QMessageBox::Ok | QMessageBox::Cancel,
    QMessageBox::Cancel);
  if (result == QMessageBox::Ok) {

    qDebug(plugin_waveglider)
      << "Sending command to (re)enable waveglider coordination (" << address
      << ":" << port << "): " << wgcmd;
    socket->writeDatagram(wgcmd, len, host_address, port);
  }
}

void
Waveglider::powerOffWavegliderUmodemFunction()
{
  char wgcmd[128];
  int len = snprintf(wgcmd, 128, "WGCMD UMODEM OFF");

  // send to waveglider coordinator.  Any socket can be used to send.

  host_address.setAddress(address);
  const int result = QMessageBox::warning(
    this,
    "WaveGlider uModem",
    QString("You are about to send command to turn off wg umodem"
            "\nProceed?"),
    QMessageBox::Ok | QMessageBox::Cancel,
    QMessageBox::Cancel);
  if (result == QMessageBox::Ok) {
    qDebug(plugin_waveglider)
      << "Sending command to turn off umodem on waveglider (" << address << ":"
      << port << "): " << wgcmd;
    socket->writeDatagram(wgcmd, len, host_address, port);
  }
}
void
Waveglider::powerOnWavegliderUmodemFunction()
{
  char wgcmd[128];
  int len = snprintf(wgcmd, 128, "WGCMD UMODEM ON");

  // send to waveglider coordinator.  Any socket can be used to send.

  host_address.setAddress(address);
  const int result = QMessageBox::warning(
    this,
    "WaveGlider uModem",
    QString("You are about to send command to turn on wg umodem"
            "\nProceed?"),
    QMessageBox::Ok | QMessageBox::Cancel,
    QMessageBox::Cancel);
  if (result == QMessageBox::Ok) {
    qDebug(plugin_waveglider)
      << "Sending command to turn on umodem on waveglider (" << address << ":"
      << port << "): " << wgcmd;
    socket->writeDatagram(wgcmd, len, host_address, port);
  }
}
void
Waveglider::setCursorWaypoint()
{
  // get cursor location
  if (n_view) {
    auto m_scene = m_view->mapScene();
    if (m_scene && m_scene->projection()) {
      const auto cursor = n_view->getTargetCursor();
      if (cursor == nullptr)
        return;
      auto cursor_position = cursor->pos();

      double lon = cursor_position.x();
      double lat = cursor_position.y();
      m_scene->projection()->transformToLonLat(1, &lon, &lat);

      // send it to sv3 coordinator land
      // Compose and send WGWPT message.  Format must match that in
      // sv3coordinator.py:
      // WGWPT %lf %lf %lf %lf %lf %lf %lf %lf %d   %u  %lf %lf %lf %lf
      //      lat lon   dep alt hdg u   v   w   abrt trk xg  yg  zg  batt
      char wgwpt[512];
      int len = snprintf(
        wgwpt, 512, "WGWPT %.5f %.5f 0 0 0 0 0 0 0 0 0 0 0 0", lat, lon);
      host_address.setAddress(address);
      const int result =
        QMessageBox::warning(this,
                             "Send Waveglider Waypoint",
                             QString("You are about to send a new waypoint to "
                                     "\nLat: %1\nLon: %2\nProceed?")
                               .arg(lat)
                               .arg(lon),
                             QMessageBox::Ok | QMessageBox::Cancel,
                             QMessageBox::Cancel);
      if (result == QMessageBox::Ok) {
        socket->writeDatagram(wgwpt, len, host_address, port);
        qDebug(plugin_waveglider) << "Sending WGWPT to waveglider coordinator ("
                                  << address << ":" << port << "): " << wgwpt;
      }
    }
  }
}

void
Waveglider::connectPlugin(const QObject* plugin, const QJsonObject& metadata)
{

  if (!plugin) {
    return;
  }

  const auto name =
    metadata.value("MetaData").toObject().value("name").toString();
  if (name.isEmpty()) {
    return;
  }
}

void
Waveglider::setupFixes()
{
  auto scene = m_view->mapScene();
  if (!scene) {
    qDebug(plugin_waveglider) << "no scene ...returning";
    return;
  }

  LayerHash nav_solution_layers =
    scene->layers(dslmap::MapItemType::NavestLayerType);

  // LayerHash converted;
  QHash<QUuid, dslmap::MapLayer*>::iterator i;
  for (i = nav_solution_layers.begin(); i != nav_solution_layers.end(); ++i) {
    // for now just add the name of editor to prove concept

    dslmap::MapLayer* m = i.value();
    navg::plugins::navest::NavestLayer* curr_nav_layer =
      dynamic_cast<navg::plugins::navest::NavestLayer*>(m);

    if (curr_nav_layer->navestBeaconId() == waveglider_id ||
        curr_nav_layer->navestBeaconId() == ship_id) {

      connect(curr_nav_layer,
              &navg::plugins::navest::NavestLayer::currentPositionChanged,
              this,
              &Waveglider::receivePositionFix,
              Qt::QueuedConnection);
    }
  }
}

void
Waveglider::sceneLayerAdded(dslmap::MapLayer* layer)
{
  setupFixes();
}

void
Waveglider::receivePositionFix(QPointF position)
{
  // lets just take the positions from the layer
  // get sender of fix so we know which layer sent it.

  dslmap::MapLayer* layer = qobject_cast<dslmap::MapLayer*>(sender());
  navg::plugins::navest::NavestLayer* curr_nav_layer =
    dynamic_cast<navg::plugins::navest::NavestLayer*>(layer);

  if (curr_nav_layer->navestBeaconId() == ship_id) {
    ship_position = position;
  }
  if (curr_nav_layer->navestBeaconId() == waveglider_id) {
    waveglider_position = position;
  }

  if (ship_position.isNull() || waveglider_position.isNull()) {
    return;
  }
  auto range = calculateRange(ship_position, waveglider_position);
  auto bearing = calculateBearing(waveglider_position, ship_position);

  ui->wg_range->setText(QString::number(range, 'f', 1) + "m");
  ui->wg_bearing->setText(QString::number(bearing, 'f', 1) + "°");
}

double
Waveglider::calculateRange(const QPointF source_fix, const QPointF dest_fix)
{
  auto scene = m_view->mapScene();
  auto proj = scene->projection();

  if (!proj) {
    qCDebug(plugin_waveglider) << "no proj returning 0 ";
    return 0;
  }
  auto range = proj->geodeticDistance(source_fix, dest_fix);

  return range;
}

double
Waveglider::calculateBearing(const QPointF source_fix, const QPointF dest_fix)
{
  auto scene = m_view->mapScene();
  auto proj = scene->projection();
  if (!scene) {
    qCDebug(plugin_waveglider) << "bearing needs a scene to calculate ";
    return 0;
  }
  if (!proj) {
    qCDebug(plugin_waveglider) << "no proj returning 0 ";
    return 0;
  }
  const auto range_line = QLineF{ m_view->mapToScene((source_fix.toPoint())),
                                  m_view->mapToScene((dest_fix.toPoint())) };
  if (range_line.length() == 0) {

    return 0;
  }
  float angle = atan2(range_line.p1().y() - range_line.p2().y(),
                      range_line.p1().x() - range_line.p2().x());
  angle = std::fmod(((angle * 180 / M_PI) + 90), 360);

  // some negative correction...
  if (angle < 0) {
    angle += 360;
  }

  return angle;
}

void
Waveglider::loadSettings(QSettings* settings)
{
  auto value = settings->value("port");
  auto ok = false;
  if (value.isValid()) {
    const auto valid_val = value.toInt(&ok);
    if (ok) {
      port = valid_val;
    }
  }
  
  value = settings->value("wg_id");
  ok = false;
  if (value.isValid()) {
    const auto valid_val = value.toInt(&ok);
    if (ok) {
      waveglider_id = valid_val;
    }
  }

  value = settings->value("ship_id");
  ok = false;
  if (value.isValid()) {
    const auto valid_val = value.toInt(&ok);
    if (ok) {
      ship_id = valid_val;
    }
  }

  address = settings->value("address").toString();
}
void
Waveglider::saveSettings(QSettings& settings)
{
  settings.setValue("persist", 1);
  settings.setValue("address", address);

  settings.setValue("port", port);

  settings.setValue("wg_id", waveglider_id);
  settings.setValue("ship_id", ship_id);
}

QList<QAction*>
Waveglider::pluginMenuActions()
{
  return { m_action_show_settings };
}

} // waveglider
} // plugins
} // navg
