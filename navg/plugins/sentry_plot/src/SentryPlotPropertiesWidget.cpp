/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "SentryPlotPropertiesWidget.h"
#include "SentryPlot.h"
#include "ui_SentryPlotPropertiesWidget.h"

namespace navg {
namespace plugins {
namespace sentry_plot {

SentryPlotPropertiesWidget::SentryPlotPropertiesWidget(QWidget* parent)
  : QWidget(parent)
  , ui(new Ui::SentryPlotPropertiesWidget)
{
  ui->setupUi(this);
  ui->penDepth->setName("Depth");
  ui->penSeafloor->setName("Seafloor");
  ui->penAlt->setName("Altitude");
}

SentryPlotPropertiesWidget::~SentryPlotPropertiesWidget()
{
  delete ui;
}

void
SentryPlotPropertiesWidget::setSentryPlot(SentryPlot* p)
{
  if (p == nullptr) {
    return;
  }
  // Depth
  auto d = p->graphDepth();
  if (d) {
    ui->penDepth->setPen(d->pen());
    connect(ui->penDepth, &dslmap::PenPropertiesWidget::penChanged, d,
            &QCPGraph::setPen);
  }
  // Floor
  auto f = p->graphFloor();
  if (f) {
    ui->penSeafloor->setPen(f->pen());
    connect(ui->penSeafloor, &dslmap::PenPropertiesWidget::penChanged, f,
            &QCPGraph::setPen);
  }
  // Altitude
  auto a = p->graphAlt();
  if (a) {
    ui->penAlt->setPen(a->pen());
    connect(ui->penAlt, &dslmap::PenPropertiesWidget::penChanged, a,
            &QCPGraph::setPen);
  }
  //   Plot surface
  auto ps = p->plotSurface();
  if (ps) {
    //    ui->backgroundColor_surface->setColor(ps->brush().color());
    connect(ui->backgroundColor_surface, &dslmap::ColorPickButton::colorChanged,
            [=](QColor color) { ps->setBackground(color); });
    ui->labelColor_surface->setColor(ps->yAxis->labelColor());
    connect(ui->labelColor_surface, &dslmap::ColorPickButton::colorChanged,
            [=](QColor color) {
              ps->yAxis->setLabelColor(color);
              ps->yAxis->setTickLabelColor(color);
              ps->xAxis->setLabelColor(color);
              ps->xAxis->setTickLabelColor(color);
            });
    ui->xAxisColor_surface->setColor(ps->xAxis->basePen().color());
    connect(ui->xAxisColor_surface, &dslmap::ColorPickButton::colorChanged,
            [=](QColor color) {
              ps->xAxis->setBasePen(QPen{ color });
              ps->xAxis->setTickPen(QPen{ color });
            });
    ui->yAxisColor_surface->setColor(ps->yAxis->basePen().color());
    connect(ui->yAxisColor_surface, &dslmap::ColorPickButton::colorChanged,
            [=](QColor color) {
              ps->yAxis->setBasePen(QPen{ color });
              ps->yAxis->setTickPen(QPen{ color });
            });
  }

  // Plot floor
  auto pf = p->plotFloor();
  if (pf) {
    connect(ui->backgroundColor_floor, &dslmap::ColorPickButton::colorChanged,
            [=](QColor color) { pf->setBackground(color); });
    ui->labelColor_floor->setColor(pf->yAxis->labelColor());
    connect(ui->labelColor_floor, &dslmap::ColorPickButton::colorChanged,
            [=](QColor color) {
              pf->yAxis->setLabelColor(color);
              pf->yAxis->setTickLabelColor(color);
              pf->xAxis->setLabelColor(color);
              pf->xAxis->setTickLabelColor(color);
            });
    ui->xAxisColor_floor->setColor(pf->xAxis->basePen().color());
    connect(ui->xAxisColor_surface, &dslmap::ColorPickButton::colorChanged,
            [=](QColor color) {
              pf->xAxis->setBasePen(QPen{ color });
              pf->xAxis->setTickPen(QPen{ color });
            });
    ui->yAxisColor_floor->setColor(pf->yAxis->basePen().color());
    connect(ui->yAxisColor_surface, &dslmap::ColorPickButton::colorChanged,
            [=](QColor color) {
              pf->yAxis->setBasePen(QPen{ color });
              pf->yAxis->setTickPen(QPen{ color });
            });
  }
}

QColor
SentryPlotPropertiesWidget::floorBackgroundColor()
{
  return ui->backgroundColor_floor->color();
}

QColor
SentryPlotPropertiesWidget::surfaceBackgroundColor()
{
  return ui->backgroundColor_surface->color();
}

void
SentryPlotPropertiesWidget::setFloorBackgroundColor(QColor color)
{
  ui->backgroundColor_floor->setColor(color);
}

void
SentryPlotPropertiesWidget::setSurfaceBackgroundColor(QColor color)
{
  ui->backgroundColor_surface->setColor(color);
}

} // namespace
}
}
