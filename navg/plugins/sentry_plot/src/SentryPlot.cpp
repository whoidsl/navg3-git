/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "SentryPlot.h"
#include "SentryPlotPropertiesWidget.h"
#include "ui_SentryPlot.h"

namespace navg {
namespace plugins {
namespace sentry_plot {

Q_LOGGING_CATEGORY(plugin_sentry_plot, "navg.plugins.sentry_plot")

SentryPlot::SentryPlot(QWidget* parent)
  : QWidget(parent)
  , ui(new Ui::SentryPlot)
  , m_plot_surface(new QCustomPlot)
  , m_plot_floor(new QCustomPlot)
{
  ui->setupUi(this);
  ui->verticalLayout->addWidget(m_plot_surface);
  ui->verticalLayout->addWidget(m_plot_floor);
  connect(ui->surfaceBtn, &QRadioButton::toggled, [=]() {
    m_plot_surface->show();
    m_plot_floor->hide();
  });
  connect(ui->floorBtn, &QRadioButton::toggled, [=]() {
    m_plot_surface->hide();
    m_plot_floor->show();
  });
  ui->surfaceBtn->toggle();

  m_graph_depth = m_plot_surface->addGraph();
  m_graph_depth->setPen(QPen(QColor(250, 0, 0)));
  m_graph_floor = m_plot_surface->addGraph();
  m_graph_floor->setPen(QPen(QColor(0, 0, 250)));
  m_graph_alt = m_plot_floor->addGraph();
  m_graph_alt->setPen(QPen(QColor(0, 250, 0)));

  m_properties = new SentryPlotPropertiesWidget();
  m_properties->setSentryPlot(this);
  m_properties->hide();
  setContextMenuPolicy(Qt::CustomContextMenu);
  connect(this, &SentryPlot::customContextMenuRequested, this,
          &SentryPlot::showContextMenu);
  m_plot_surface->setContextMenuPolicy(Qt::CustomContextMenu);
  connect(m_plot_surface, &QCustomPlot::customContextMenuRequested, this,
          &SentryPlot::showContextMenu);
  m_plot_floor->setContextMenuPolicy(Qt::CustomContextMenu);
  connect(m_plot_floor, &QCustomPlot::customContextMenuRequested, this,
          &SentryPlot::showContextMenu);

  QSharedPointer<QCPAxisTickerDateTime> surface_dateTicker(
    new QCPAxisTickerDateTime);
  surface_dateTicker->setDateTimeFormat("hh:mm");
  m_plot_surface->xAxis->setTicker(surface_dateTicker);
  m_plot_surface->yAxis->setLabel("Depth");

  QSharedPointer<QCPAxisTickerDateTime> floor_dateTicker(
    new QCPAxisTickerDateTime);
  floor_dateTicker->setDateTimeFormat("hh:mm");
  m_plot_floor->xAxis->setTicker(floor_dateTicker);
  m_plot_floor->yAxis->setLabel("Alt");

  //  m_graph_depth->data()->set(m_depths);
  //  plot->graph(1)->data()->set(m_alts);
  m_plot_surface->replot();
  m_plot_floor->replot();

  m_plot_surface->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom |
                                  QCP::iSelectPlottables | QCP::iMultiSelect);
  m_plot_floor->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom |
                                QCP::iSelectPlottables | QCP::iMultiSelect);
}

SentryPlot::~SentryPlot()
{
  delete ui;
}

void
SentryPlot::loadSettings(QSettings* settings)
{
  if (m_graph_depth) {
    m_graph_depth->setPen(loadPenSettings(settings, "pens/depth/"));
  }
  if (m_graph_alt) {
    m_graph_alt->setPen(loadPenSettings(settings, "pens/alt/"));
  }
  if (m_graph_floor) {
    m_graph_floor->setPen(loadPenSettings(settings, "pens/floor/"));
  }
  QColor color;
  if (m_plot_surface) {
    if (m_properties) {
      color = QColor{
        settings->value("plots/surface/backgroundColor", "#000000").toString()
      };
      if (color.isValid()) {
        m_properties->setSurfaceBackgroundColor(color);
      }
    }
    color = QColor{
      settings->value("plots/surface/xAxisColor", "#FFFFFF").toString()
    };
    if (color.isValid()) {
      m_plot_surface->xAxis->setBasePen(QPen{ color });
      m_plot_surface->xAxis->setTickPen(QPen{ color });
    }
    color = QColor{
      settings->value("plots/surface/yAxisColor", "#FFFFFF").toString()
    };
    if (color.isValid()) {
      m_plot_surface->yAxis->setBasePen(QPen{ color });
      m_plot_surface->yAxis->setTickPen(QPen{ color });
    }
    color = QColor{
      settings->value("plots/surface/labelColor", "#FFFFFF").toString()
    };
    if (color.isValid()) {
      m_plot_surface->yAxis->setLabelColor(color);
      m_plot_surface->yAxis->setTickLabelColor(color);
      m_plot_surface->xAxis->setLabelColor(color);
      m_plot_surface->xAxis->setTickLabelColor(color);
    }
  }
  if (m_plot_floor) {
    if (m_properties) {
      color = QColor{
        settings->value("plots/floor/backgroundColor", "#000000").toString()
      };
      if (color.isValid()) {
        m_properties->setFloorBackgroundColor(color);
      }
    }
    color =
      QColor{ settings->value("plots/floor/xAxisColor", "#FFFFFF").toString() };
    if (color.isValid()) {
      m_plot_floor->xAxis->setBasePen(QPen{ color });
      m_plot_floor->xAxis->setTickPen(QPen{ color });
    }
    color =
      QColor{ settings->value("plots/floor/yAxisColor", "#FFFFFF").toString() };
    if (color.isValid()) {
      m_plot_floor->yAxis->setBasePen(QPen{ color });
      m_plot_floor->yAxis->setTickPen(QPen{ color });
    }
    color =
      QColor{ settings->value("plots/floor/labelColor", "#FFFFFF").toString() };
    if (color.isValid()) {
      m_plot_floor->yAxis->setLabelColor(color);
      m_plot_floor->yAxis->setTickLabelColor(color);
      m_plot_floor->xAxis->setLabelColor(color);
      m_plot_floor->xAxis->setTickLabelColor(color);
    }
  }
  m_properties->setSentryPlot(this);
}

void
SentryPlot::saveSettings(QSettings& settings)
{

  if (m_graph_depth) {
    savePenSettings(m_graph_depth->pen(), &settings, "pens/depth/");
  }
  if (m_graph_alt) {
    savePenSettings(m_graph_alt->pen(), &settings, "pens/alt/");
  }
  if (m_graph_floor) {
    savePenSettings(m_graph_floor->pen(), &settings, "pens/floor/");
  }
  if (m_plot_surface) {
    if (m_properties) {
      settings.setValue("plots/surface/backgroundColor",
                        m_properties->surfaceBackgroundColor().name());
    }
    settings.setValue("plots/surface/xAxisColor",
                      m_plot_surface->xAxis->basePen().color().name());
    settings.setValue("plots/surface/yAxisColor",
                      m_plot_surface->yAxis->basePen().color().name());
    settings.setValue("plots/surface/labelColor",
                      m_plot_surface->yAxis->labelColor().name());
  }
  if (m_plot_floor) {
    if (m_properties) {
      settings.setValue("plots/floor/backgroundColor",
                        m_properties->floorBackgroundColor().name());
    }
    settings.setValue("plots/floor/xAxisColor",
                      m_plot_surface->xAxis->basePen().color().name());
    settings.setValue("plots/floor/yAxisColor",
                      m_plot_surface->yAxis->basePen().color().name());
    settings.setValue("plots/floor/labelColor",
                      m_plot_surface->yAxis->labelColor().name());
  }
}

void
SentryPlot::savePenSettings(QPen pen, QSettings* settings, QString prefix)
{
  settings->setValue(prefix + "width", pen.width());
  settings->setValue(prefix + "color", pen.color().name());
  settings->setValue(
    prefix + "style",
    QMetaEnum::fromType<Qt::PenStyle>().valueToKey(pen.style()));
}

QPen
SentryPlot::loadPenSettings(QSettings* settings, QString prefix)
{
  QPen pen;
  bool ok = false;
  pen.setWidth(settings->value(prefix + "width").toInt());

  auto value = settings->value(prefix + "color");
  if (value.isValid()) {
    const auto color = QColor{ value.toString() };
    if (color.isValid()) {
      pen.setColor(color);
    }
  }

  value = settings->value(prefix + "style");
  if (value.isValid()) {
    const auto key = value.toString().toStdString();
    const auto style =
      QMetaEnum::fromType<Qt::PenStyle>().keyToValue(key.data(), &ok);
    if (ok) {
      pen.setStyle(static_cast<Qt::PenStyle>(style));
    }
  }
  return pen;
}

QList<QAction*>
SentryPlot::pluginMenuActions()
{
  return {};
}

QList<QAction*>
SentryPlot::pluginToolbarActions()
{
  return {};
}

void
SentryPlot::connectPlugin(const QObject* plugin, const QJsonObject& metadata)
{
  if (!plugin) {
    return;
  }

  const auto name =
    metadata.value("MetaData").toObject().value("name").toString();
  if (name.isEmpty()) {
    return;
  }

  if (name == "sentry_acomms") {
    if (!connect(plugin, SIGNAL(hdgDepthAltUpdated(int, int, int)), this,
                 SLOT(updateData(int, int, int)))) {
      qCCritical(plugin_sentry_plot,
                 "Unable to connect to signal: "
                 "SentryAcomms::hdgDepthAltUpdated(int, int, int)");
      Q_ASSERT(false);
    } else {
      qCInfo(plugin_sentry_plot,
             "Connected to SentryAcomms::hdgDepthAltUpdated");
    }
  }

  if (name == "vehicle_usbl") {
    if (!connect(plugin, SIGNAL(depthUpdated(double)), this,
                 SLOT(updateDepth(double)))) {
      qCCritical(plugin_sentry_plot, "Unable to connect to signal: "
                                     "SentryUsbl::depthUpdated(double)");
      Q_ASSERT(false);
    } else {
      qCInfo(plugin_sentry_plot, "Connected to SentryUsbl::depthUpdated");
    }
  }
}

void SentryPlot::setMapView(QPointer<dslmap::MapView>)
{
}

void
SentryPlot::updateData(int hdg, int depth, int alt)
{
  auto t = QDateTime::currentDateTime().toTime_t();

  m_graph_depth->addData(t, -depth);
  m_graph_alt->addData(t, alt);
  m_graph_floor->addData(t, -depth - alt);

  m_plot_surface->xAxis->rescale();
  m_plot_floor->xAxis->rescale();
  m_graph_depth->rescaleValueAxis(false, true);
  m_graph_floor->rescaleValueAxis(true, true);
  m_graph_alt->rescaleValueAxis(false, true);
  m_plot_surface->xAxis->setRange(m_plot_surface->xAxis->range().upper, 100,
                                  Qt::AlignRight);
  m_plot_floor->xAxis->setRange(m_plot_floor->xAxis->range().upper, 100,
                                Qt::AlignRight);

  m_plot_surface->replot();
  m_plot_floor->replot();
}

void
SentryPlot::updateDepth(double depth)
{
  auto t = QDateTime::currentDateTime().toTime_t();

  m_graph_depth->addData(t, -depth);

  m_plot_surface->xAxis->rescale();
  m_graph_depth->rescaleValueAxis(false, true);
  m_plot_surface->xAxis->setRange(m_plot_surface->xAxis->range().upper, 100,
                                  Qt::AlignRight);
  m_plot_surface->replot();
}

void
SentryPlot::showContextMenu(const QPoint& pos)
{
  QPoint launch;
  auto s = qobject_cast<QWidget*>(QObject::sender());
  if (s) {
    launch = s->mapToGlobal(pos);
  } else {
    launch = mapToGlobal(pos);
  }

  QMenu contextMenu(this);
  QAction* act = new QAction("Properties", this);
  connect(act, &QAction::triggered, [=]() {
    m_properties->show();
    m_properties->raise();
  });
  contextMenu.addAction(act);
  contextMenu.exec(launch);
}

} // namespace
}
}
