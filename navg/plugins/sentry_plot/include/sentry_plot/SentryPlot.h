/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef SENTRYPLOT_H
#define SENTRYPLOT_H

#include "../QCustomPlot/qcustomplot.h"
#include "CorePluginInterface.h"
#include <QLoggingCategory>
#include <QWidget>

namespace navg {
namespace plugins {
namespace sentry_plot {

class SentryPlotPropertiesWidget;

namespace Ui {
class SentryPlot;
}

class SentryPlot : public QWidget, public CorePluginInterface
{
  Q_OBJECT
  Q_INTERFACES(navg::CorePluginInterface)
  Q_PLUGIN_METADATA(IID CorePluginInterface_iid FILE "SentryPlot.json")

public:
  explicit SentryPlot(QWidget* parent = 0);
  ~SentryPlot();

  //
  // CorePluginInterface Implementation
  //
  void connectPlugin(const QObject* plugin,
                     const QJsonObject& metadata) override;
  void setMapView(QPointer<dslmap::MapView> view) override;
  void loadSettings(QSettings* settings) override;
  void saveSettings(QSettings& settings) override;
  QList<QAction*> pluginMenuActions() override;
  QList<QAction*> pluginToolbarActions() override;

  QCustomPlot* plotFloor() { return m_plot_floor; }
  QCustomPlot* plotSurface() { return m_plot_surface; }
  QCPGraph* graphDepth() { return m_graph_depth; }
  QCPGraph* graphAlt() { return m_graph_alt; }
  QCPGraph* graphFloor() { return m_graph_floor; }

protected:
  void savePenSettings(QPen pen, QSettings* settings, QString prefix);
  QPen loadPenSettings(QSettings* settings, QString prefix);
  void showContextMenu(const QPoint& pos);

  //  void customContextMenuEvent(QPoint pos);

private slots:
  void updateData(int hdg, int depth, int alt);
  void updateDepth(double depth);

private:
  Ui::SentryPlot* ui;
  QCustomPlot *m_plot_surface, *m_plot_floor;
  QCPGraph *m_graph_depth, *m_graph_alt, *m_graph_floor;
  SentryPlotPropertiesWidget* m_properties;
};

} // namespace
}
}

#endif // SENTRYPLOT_H
