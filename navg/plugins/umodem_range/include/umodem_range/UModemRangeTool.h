/**
 * Copyright 2021 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// Created by tmjoyce
//

#ifndef NAVG_UMODEMRANGETOOL_H
#define NAVG_UMODEMRANGETOOL_H

#include <navg/CorePluginInterface.h>

#include "dslmap/MapView.h"
#include "dslmap/SymbolLayer.h"
#include "dslmap/MapLayer.h"
#include "dslmap/Proj.h"
#include <QLoggingCategory>
#include <QObject>
#include <QDateTime>
#include <QWidget>
#include <QAction>
#include <QJsonDocument>
#include <QJsonObject>
#include <QHostAddress>
#include <QUdpSocket>
#include <memory>
#include <QDateTime>
#include "UModemRangeSettings.h"
#include <../../navest/include/navest/Messages.h>
#include "RangeLayer.h"


#include <navg/NavGMapView.h>

namespace navg {
namespace plugins {
namespace umodem_range {



class UModemRangeTool
  : public QWidget
  , public CorePluginInterface
{
  Q_OBJECT
  Q_INTERFACES(navg::CorePluginInterface)
  Q_PLUGIN_METADATA(IID CorePluginInterface_iid FILE "UModemRangeTool.json")

signals:

public:
  explicit UModemRangeTool(QWidget* parent = nullptr);
  ~UModemRangeTool() override;

  //
  // CorePluginInterface Implementation
  //
  void loadSettings(QSettings* settings) override;
  void saveSettings(QSettings& settings) override;

  // This plugin connects to ....
  void connectPlugin(const QObject* plugin,
                     const QJsonObject& metadata) override;
  void setMapView(QPointer<dslmap::MapView> view) override;
  QList<QAction*> pluginMenuActions() override;
  QList<QAction*> pluginToolbarActions() override { return {}; }

public slots:
  /// \brief Show the settings dialog
  ///
  void showSettingsDialog();

protected:
protected slots:

  void drawRangeCircle(double, double, double, QDateTime);
  void drawRangeCircle(QPointF, double, QDateTime);
  void dataPending();
  /// \brief Handle multiple incoming message types from navest
  void updateFromNavestMessage(NavestMessageType type, QVariant message);

private:
  QPointer<dslmap::MapView> m_view;
  QPointer<navg::NavGMapView> n_view;

  QHostAddress address;
  int in_socket_port;
  int in_fix_id;
  int in_num_pings;
  double modem_range_radius;

  QDateTime fix_timestamp;
  QString fix_source;
  int vehicle_id;
  double lon, lat, depth;

  QPointer<QUdpSocket> socket;
  QAction* m_action_show_settings = nullptr;
  
  QPointer<RangeLayer> m_layer;

  QPointF center_point;


};

Q_DECLARE_LOGGING_CATEGORY(plugin_umodem_range)
} // target_receiver
} // plugins
} // navg

#endif // UMODEMRANGE_H
