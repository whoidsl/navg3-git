/**
 * Copyright 2021 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// Created by tmjoyce
//

#ifndef NAVG_UMODEMRANGELAYER_H
#define NAVG_UMODEMRANGELAYER_H

#include <navg/CorePluginInterface.h>

#include "UModemRangeSettings.h"
#include "dslmap/MapLayer.h"
#include "dslmap/MapView.h"
#include "dslmap/Proj.h"
#include <QAction>
#include <QDateTime>
#include <QGraphicsPathItem>
#include <QLoggingCategory>
#include <QObject>
#include <QWidget>
#include <memory>

#include <navg/NavGMapView.h>

namespace navg {
namespace plugins {
namespace umodem_range {

class RangeLayer : public dslmap::MapLayer
{
  Q_OBJECT

signals:

public:
  explicit RangeLayer(QGraphicsItem* parent = nullptr);
  ~RangeLayer() override;

  QRectF boundingRect() const override;
  void setProjection(std::shared_ptr<const dslmap::Proj> proj) override;

  bool saveSettings(QSettings& settings) override;
  bool loadSettings(QSettings& settings) override;

  void showPropertiesDialog() override;
  void paint(QPainter* painter, const QStyleOptionGraphicsItem* option,
             QWidget* widget) override;

public slots:

  void drawRangeCircle(double, double, double, QDateTime);
  void drawRangeCircle(QPointF, double, QDateTime);

protected:
protected slots:

private:
  QPointer<dslmap::MapView> m_view;
  QPointer<navg::NavGMapView> n_view;

  // start with just one...
  QGraphicsPathItem* circle_item = nullptr;
};

Q_DECLARE_LOGGING_CATEGORY(plugin_umodem_range_layer)
} // target_receiver
} // plugins
} // navg

#endif // UMODEMRANGE_H
