/**
 * Copyright 2021 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// Created by tmjoyce 08/25/21.
//

#include "RangeLayer.h"
#include <NavGMapScene.h>
#include <QGraphicsPathItem>
#include <QMessageBox>
#include <QSettings>
#include <dslmap/Proj.h>

namespace navg {
namespace plugins {
namespace umodem_range {

Q_LOGGING_CATEGORY(plugin_umodem_range_layer, "navg.plugins.umodem_range_layer")

RangeLayer::RangeLayer(QGraphicsItem* parent)
  : dslmap::MapLayer(parent)
{}

RangeLayer::~RangeLayer()
{
  delete circle_item;
}

QRectF
RangeLayer::boundingRect() const
{

  if (circle_item) {
    return circle_item->boundingRect();
  }
  return QRectF();
}

void
RangeLayer::setProjection(std::shared_ptr<const dslmap::Proj> proj)
{

  if (!proj) {
    qCDebug(plugin_umodem_range_layer) << "no proj exists,setting default ";

    return;
  }

  auto p = projectionPtr();

  if (p && p->isSame(*proj)) {
    qCDebug(plugin_umodem_range_layer)
      << "projectionptr exists and is same as passed in proj";
    return;
  }

  MapLayer::setProjection(proj);
}

bool
RangeLayer::saveSettings(QSettings& settings)
{
  Q_UNUSED(settings)
  return false;
}

bool
RangeLayer::loadSettings(QSettings& settings)
{
  Q_UNUSED(settings)
  return false;
}

void
RangeLayer::showPropertiesDialog()
{
  // not made
}

void
RangeLayer::paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*)
{}

void
RangeLayer::drawRangeCircle(double lon,
                            double lat,
                            double radius,
                            QDateTime time)
{

  auto timestr = time.toString("yyyy/MM/dd\nHH:mm:ss.z");
  auto latstr = QString::number(lat);
  auto lonstr = QString::number(lon);
  auto radiusstr = QString::number(radius);

  auto pos = QPointF(lon, lat);

  auto proj = projectionPtr();
  if (!proj) {
    return;
  }

  if (proj) {
    double x_val(pos.x());
    double y_val(pos.y());
    proj->transformFromLonLat(1, &x_val, &y_val);
    pos.setX(x_val);
    pos.setY(y_val);
  }

  QPainterPath path;
  path.addEllipse(pos, radius, radius);

  if (!circle_item) {
    auto pen = QPen{ Qt::blue };
    pen.setWidth(4);
    circle_item = new QGraphicsPathItem(path, this);
    circle_item->setPen(pen);
    circle_item->setToolTip("Timestamp: "+timestr+"\n"+"Ctr pt. Lat: "+latstr+"\n"+"Ctr pt Lon: "+lonstr+"\n"+"Range: "+radiusstr);
  } else {
    circle_item->setPath(path);
    circle_item->setToolTip("Timestamp: "+timestr+"\n"+"Ctr pt. Lat: "+latstr+"\n"+"Ctr pt Lon: "+lonstr+"\n"+"Range: "+radiusstr);
  }
}

void
RangeLayer::drawRangeCircle(QPointF center, double radius, QDateTime time)
{

  auto timestr = time.toString("yyyy/MM/dd\nHH:mm:ss.z");
  auto ctrxstr = QString::number(center.x());
  auto ctrystr = QString::number(center.y());
  auto radiusstr = QString::number(radius);

  auto pos = center;

  auto proj = projectionPtr();
  if (!proj) {
    return;
  }

  if (proj) {
    double x_val(pos.x());
    double y_val(pos.y());
    proj->transformFromLonLat(1, &x_val, &y_val);
    pos.setX(x_val);
    pos.setY(y_val);
  }

  QPainterPath path;
  path.addEllipse(pos, radius, radius);

  if (!circle_item) {
    auto pen = QPen{ Qt::blue };
    pen.setWidth(3);
    pen.setCosmetic(true);
    circle_item = new QGraphicsPathItem(path, this);
    circle_item->setPen(pen);
    circle_item->setToolTip("Timestamp: "+timestr+"\n"+"Ctr pt X: "+ctrxstr+"\n"+"Ctr pt Y: "+ctrystr+"\n"+"Range: "+radiusstr);

  } else {
    circle_item->setPath(path);
    circle_item->setToolTip("Timestamp: "+timestr+"\n"+"Ctr pt X: "+ctrxstr+"\n"+"Ctr pt Y: "+ctrystr+"\n"+"Range: "+radiusstr);

  }
}

} // umodem_range
} // plugins
} // navg
