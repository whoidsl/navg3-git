/**
 * Copyright 2021 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// Created by tmjoyce 08/25/21.
//

#include "UModemRangeTool.h"
#include <NavGMapScene.h>
#include <QMessageBox>
#include <QSettings>
#include <dslmap/Proj.h>
#include <QGraphicsPathItem>

namespace navg {
namespace plugins {
namespace umodem_range {

Q_LOGGING_CATEGORY(plugin_umodem_range, "navg.plugins.umodem_range")

UModemRangeTool::UModemRangeTool(QWidget* parent)
  : QWidget(parent)
  , m_action_show_settings(new QAction("&Settings", this))
  , m_layer(nullptr)
{

  socket = new QUdpSocket(this);

  in_socket_port = 0;
  in_fix_id = 2;
  in_num_pings = 5;

  connect(m_action_show_settings,
          &QAction::triggered,
          this,
          &UModemRangeTool::showSettingsDialog);

  QObject::connect(socket,
                   SIGNAL(readyRead()),
                   this,
                   SLOT(dataPending()),
                   Qt::UniqueConnection);
}

UModemRangeTool::~UModemRangeTool()
{
  delete socket;
}

void
UModemRangeTool::setMapView(QPointer<dslmap::MapView> view)
{
  m_view = view;
  n_view = qobject_cast<NavGMapView*>(view);
}

void
UModemRangeTool::connectPlugin(const QObject* plugin,
                              const QJsonObject& metadata)
{
  if (!plugin) {
    return;
  }

  const auto name =
    metadata.value("MetaData").toObject().value("name").toString();
  if (name.isEmpty()) {
    return;
  }

  if (name == "navest") {

    if (!connect(plugin,
                 SIGNAL(messageReceived(NavestMessageType, QVariant)),
                 this,
                 SLOT(updateFromNavestMessage(NavestMessageType, QVariant)))) {
      qCCritical(plugin_umodem_range,
                 "Unable to connect to signal: "
                 "Navest::messageReceived(NavestMessageType, "
                 "QVariant)");
    } else {
      qCInfo(plugin_umodem_range, "Connected to Navest::messageReceived");
    }
    
  }
}

void
UModemRangeTool::loadSettings(QSettings* settings)
{

  auto value = settings->value("in_port");
  auto ok = false;
  if (value.isValid()) {
    const auto valid_val = value.toInt(&ok);
    if (ok) {
      in_socket_port = valid_val;
    }
  }

  if (!socket->bind(in_socket_port,
                    QUdpSocket::ShareAddress | QUdpSocket::ReuseAddressHint)) {
    qDebug(plugin_umodem_range) << "could not bind to socket";
  } else {
    QObject::connect(socket,
                     SIGNAL(readyRead()),
                     this,
                     SLOT(dataPending()),
                     Qt::UniqueConnection);
  }
  auto value2 = settings->value("topside_fix_id");
  ok = false;
  if (value2.isValid()) {
    const auto valid_val = value2.toInt(&ok);
    if (ok) {
      in_fix_id = valid_val;
    }
  }

  auto value3 = settings->value("num_pings");
  ok = false;
  if (value3.isValid()) {
    const auto valid_val = value3.toInt(&ok);
    if (ok) {
      in_num_pings = valid_val;
    }
  }
}

void
UModemRangeTool::showSettingsDialog()
{

  QScopedPointer<UModemRangeSettings> dialog(new UModemRangeSettings);
  qDebug(plugin_umodem_range) << "port value we are setting to dialog is " << in_socket_port;
  qDebug(plugin_umodem_range) << "topside fix id value we are setting to dialog is " << in_fix_id;
  qDebug(plugin_umodem_range) << "num pings value we are setting to dialog is " << in_num_pings;

  dialog->setPort(in_socket_port);
  dialog->setFixNum(in_fix_id);
  dialog->setNumPings(in_num_pings);

  dialog->setModal(true);
  if (!dialog->exec()) {
    return;
  }

  int temp = dialog->getPort();
  if (in_socket_port != temp) {
    in_socket_port = temp;
    socket->close();
    if (!socket->bind(in_socket_port,
                      QUdpSocket::ShareAddress |
                        QUdpSocket::ReuseAddressHint)) {
      qDebug(plugin_umodem_range) << "could not bind to socket";
    } else {
      QObject::connect(socket,
                       SIGNAL(readyRead()),
                       this,
                       SLOT(dataPending()),
                       Qt::UniqueConnection);
    }
  }
  qDebug(plugin_umodem_range) << "port value received from dialog is " << in_socket_port;

  int temp2 = dialog->getFixNum();
  if (in_fix_id != temp2) {
    in_fix_id = temp2;
  }
  qDebug(plugin_umodem_range) << "topside fix id received from dialog is " << in_fix_id;
  
  int temp3 = dialog->getNumPings();
  if (in_num_pings != temp) {
    in_num_pings = temp3;
  }
  qDebug(plugin_umodem_range) << "num pings received from dialog is " << in_num_pings;
}

void
UModemRangeTool::saveSettings(QSettings& settings)
{
  settings.setValue("persist", 1);
  settings.setValue("in_port", in_socket_port);
  settings.setValue("topside_fix_id", in_fix_id);
  settings.setValue("num_pings", in_num_pings);
}

QList<QAction*>
UModemRangeTool::pluginMenuActions()
{
  return { m_action_show_settings };
}

void
UModemRangeTool::dataPending()
{
  qDebug(plugin_umodem_range) <<"in dataPending";
  QUdpSocket* theSocket = (QUdpSocket*)sender();

  while (theSocket->hasPendingDatagrams()) {

    QByteArray buffer(theSocket->pendingDatagramSize(), 0);
    qint64 bytesRead = theSocket->readDatagram(buffer.data(), buffer.size());
    qDebug(plugin_umodem_range) << "Bytes read: " << bytesRead;

    int timestamp;
    int modem_src_id;
    double modem_horz_range_val;
    int args = sscanf(buffer.data(), "PRR %d %d %lf",
            &timestamp,
            &modem_src_id,
            &modem_horz_range_val);
    if (args < 3)
    {
      qDebug(plugin_umodem_range) << "PRR parsing error";
      return;
    }
    else {
      qDebug(plugin_umodem_range) << "Valid umodem ping range rx'ed: "<<modem_horz_range_val;
      modem_range_radius = modem_horz_range_val;
      if (!center_point.isNull()) {
      	drawRangeCircle(center_point, modem_range_radius, fix_timestamp);
      }
      else {
	qDebug(plugin_umodem_range) <<"Got valid range, but no VFR";
      }
    }
  }
}

void
UModemRangeTool::drawRangeCircle(double lon, double lat, double horizontal_dist, QDateTime timestamp)
{

  auto my_scene = m_view->mapScene();
  if (!my_scene)
    return;

  auto pos = QPointF(lon, lat);

  if(!m_layer){
    m_layer = new RangeLayer();
    m_layer->drawRangeCircle(pos, horizontal_dist, timestamp);
    my_scene->addLayer(m_layer);
  }

  else {
    m_layer->drawRangeCircle(pos, horizontal_dist, timestamp);
  }
 

  
  
}

void
UModemRangeTool::drawRangeCircle(QPointF center, double  horizontal_dist, QDateTime timestamp)
{
  auto my_scene = m_view->mapScene();
  if (!my_scene)
    return;

  if(!m_layer){
    m_layer = new RangeLayer();
    m_layer->drawRangeCircle(center, horizontal_dist, timestamp);
    m_layer->setName("UModem Horizontal Range");
    my_scene->addLayer(m_layer);
  }

  else {
    m_layer->drawRangeCircle(center, horizontal_dist, timestamp);
  }



  
}

void
UModemRangeTool::updateFromNavestMessage(navg::NavestMessageType type, QVariant message)
{
  qDebug(plugin_umodem_range) <<"updating from navest message";
  if (type == navg::NavestMessageType::VFR) {
    auto vfr_message = message.value<NavestMessageVfr>();
    fix_timestamp = vfr_message.timestamp;
    vehicle_id = vfr_message.vehicle_number;
    fix_source = vfr_message.fix_source;
    lon = vfr_message.lon;
    lat = vfr_message.lat;
    depth = vfr_message.depth;

    if (vehicle_id != in_fix_id) {
      qDebug(plugin_umodem_range) <<"VFR vehicle ID != fix ID";
      return;
    }

    center_point.setX(lon);
    center_point.setY(lat);

    //drawRangeCircle(center_point,200.0);
  }
  else {
    return;
  }
  center_point.setX(lon);
  center_point.setY(lat);

  //test drawing circle
  //updating the drawing will be done on udp received and not this.
  //drawRangeCircle(center_point, 200.0, fix_timestamp);

}

} // umodem_range
} // plugins
} // navg
