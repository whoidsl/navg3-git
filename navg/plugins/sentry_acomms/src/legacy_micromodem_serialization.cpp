/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "legacy_micromodem_serialization.h"

#include <QDebug>
#include <cmath>
#include <cstdio>

namespace navg {
namespace plugins {
namespace sentry_acomms {
namespace legacy_micromodem {

static unsigned int rx_packet_kount = 0;

/* ----------------------------------------------------------------------

   UMODEM_PACKET_TYPE_INT_TO_NAME

   Modification History:
   DATE       AUTHOR  COMMENT
   2009/10/19 LLW     Created and written

---------------------------------------------------------------------- */
const char*
UMODEM_DATA_PACKET_TYPE_INT_TO_NAME(int num)
{
#define make_entry(flag)                                                       \
  case flag:                                                                   \
    name = #flag;                                                              \
    break

  const char* name;

  switch (num) {
    make_entry(UMODEMREMUSSTATEXY);
    make_entry(UMODEMHROVSTATEXY);
    make_entry(UMODEMHROVSTATEXYRESET);
    make_entry(UMODEMHROVJASONTALK);
    make_entry(UMODEMHROVACOMMSSTATUS01);
    make_entry(UMODEMHROVSCIENCE01);
    make_entry(UMODEMHROVSCIENCE02);

    default:
      name = "UNKOWN UMODEM DATA PACKET TYPE";
  }

  return name;
#undef make_entry
}

/* ------------------------------------------------------------

Function to parse and convert NMEA UMODEM message type and return it as an
integer

returns zero on success
returns nonzero on failure

2009-05-13   LLW     Created
------------------------------------------------------------ */
int
UMODEM_Decode_CARXD_Message_Type(const char* nmea_str, int* message_type)
{
  unsigned int umodem_message_type;
  int parse_status;
  int a, b, c, d;

  /* parse the header */
  // 2009/10/22 LLW added temp args to avoid borland compiler bug
  parse_status = sscanf(nmea_str, "$CARXD,%d,%d,%d,%d,%02X", &a, &b, &c, &d,
                        &umodem_message_type);

  // check to see if the umodem_message_type converted and assigned ok
  // 2009/10/22 LLW corrected arg check to 5 from 1
  if (parse_status != 5)
    return (-3);

  // parsed ok, so assign to output argument and...
  *message_type = umodem_message_type;

  // ...return with success status
  return (0);
}

/* ------------------------------------------------------------
Function to decode character string

2008-10-08   SCM     Ported from LLW Code
2009-03-25   SEW     Added to umodem_msg_util.cpp from rapvla
2009-04-02   SEW&LLW Corrected length passed to snprintf
------------------------------------------------------------ */
int
decode_char(char* data_out, int num_bytes_to_decode, const char*,
            int hex_str_in_max_len)
{
  int i;
  int len = 0;
  // int args = 0;
  unsigned int tmp_uint = 0;
  // unsigned char * unsigned_data_out;
  if (num_bytes_to_decode <= 0)
    return (0);
  if (num_bytes_to_decode > (hex_str_in_max_len - 1))
    return (0);

  for (i = 0; i < num_bytes_to_decode; i++) {
    // args = std::sscanf(&hex_str_in[2 * i], "%2X", &tmp_uint);

    // decode two hex characters as one character
    len +=
      std::snprintf(&data_out[len], hex_str_in_max_len - i - 1, "%c", tmp_uint);
  }

  Q_UNUSED(tmp_uint);

  // strcpy((unsigned char *)data_out,  unsigned_data_out,len);

  return (len);
}

/* ------------------------------------------------------------
Function to encode signed int

2008-07-20   LLW     Created
2009-03-25   SEW     Copied correction from rapvla to length arg of snprintf
2009-04-02   SEW&LLW Corrected length arg of snprintf
------------------------------------------------------------ */
static int
encode_signed_int(int* data_in, int num_bytes_to_encode, char* hex_str_out,
                  int hex_str_out_max_len)
{
  int i;
  unsigned int* uptr = (unsigned int*)data_in;
  int len = 0;

  if (num_bytes_to_encode <= 0)
    return (0);
  if (num_bytes_to_encode > 4)
    return (0);
  if (num_bytes_to_encode > (hex_str_out_max_len - 1))
    return (0);

  for (i = 0; i < num_bytes_to_encode; i++) {
    // encode one byte as two hex characters
    len += snprintf(&hex_str_out[len], hex_str_out_max_len - i - 1, "%02X",
                    (((*uptr) >> (i * 8)) & 0xFF));
  }

  return (len);
}

/* ------------------------------------------------------------
Function to encode unsigned int

2008-07-20   LLW     Created
------------------------------------------------------------ */

static int
encode_unsigned_int(unsigned int* data_in, int num_bytes_to_encode,
                    char* hex_str_out, int hex_str_out_max_len)
{
  return encode_signed_int((int*)data_in, num_bytes_to_encode, hex_str_out,
                           hex_str_out_max_len);
}

/* ------------------------------------------------------------
Function to encode double as int

2008-07-20   LLW     Created
------------------------------------------------------------ */
static int
encode_double_as_int(double data_in_double, double int_units,
                     int num_bytes_to_encode, char* hex_str_out,
                     int hex_str_out_max_len)
{
  double tmp_double;
  int data_in_int;

  tmp_double = (data_in_double / int_units) + 0.5;

  data_in_int = std::floor(tmp_double);

  data_in_int = std::floor((data_in_double / int_units) + 0.5);

  return encode_signed_int(&data_in_int, num_bytes_to_encode, hex_str_out,
                           hex_str_out_max_len);
}

/* ------------------------------------------------------------
Function to decode unsigned int

2008-07-20   LLW     Created
2009-03-25   SEW     Changed return(0) to return(num_bytes...*2)
------------------------------------------------------------ */
static int
decode_unsigned_int(unsigned int* data_out, int num_bytes_to_decode,
                    const char* hex_str_in, int hex_str_in_max_len)

{
  int i;
  int args = 0;
  unsigned int tmp_uint;
  unsigned int tmp_data_out = 0;
  unsigned char* out_ptr = (unsigned char*)(&tmp_data_out);

  if (num_bytes_to_decode <= 0)
    return (-1);
  if (num_bytes_to_decode > 4)
    return (-2);
  if (num_bytes_to_decode > (hex_str_in_max_len))
    return (-3);

  for (i = 0; i < num_bytes_to_decode; i++) {
    // decode one byte from two hex characters
    args = sscanf(&hex_str_in[2 * i], "%2X", &tmp_uint);

    // check to see if the hex converted properly
    if (args != 1)
      return (-4);

    // assign it
    out_ptr[i] = tmp_uint;
  }

  // no errors, so assign the output variable
  *data_out = tmp_data_out;

  return (num_bytes_to_decode * 2);
}

/* ------------------------------------------------------------
Function to decode signed int

2008-07-20   LLW     Created
2009-03-25   SEW     Changed return(0) to return(num_bytes...*2)
------------------------------------------------------------ */
static int
decode_signed_int(int* data_out, int num_bytes_to_decode,
                  const char* hex_str_in, int hex_str_in_max_len)

{
  int i;
  unsigned char* ucptr = (unsigned char*)data_out;
  int int_is_negative;
  int status;

  // get the data bytes
  status = decode_unsigned_int((unsigned*)data_out, num_bytes_to_decode,
                               hex_str_in, hex_str_in_max_len);
  if (status <= 0)
    return (status);

  // if we have not assigned all the bytes in the integer, then
  // do so now with proper sign extension
  if (num_bytes_to_decode < 4) {
    // check sign bit of the highest decoded byte.
    // 1 means negative
    // 0 means positive
    int_is_negative = (ucptr[num_bytes_to_decode - 1] & 0x80) != 0;

    // negative numbers msbs  1
    // positive numbers msbs  0
    if (int_is_negative) {
      for (i = num_bytes_to_decode; i < 4; i++)
        ucptr[i] = 0xFF;
    }
  }

  return (num_bytes_to_decode * 2);
}

/* ------------------------------------------------------------
Function to decode double encoded as int

2008-07-20   LLW     Created
------------------------------------------------------------ */
static int
decode_double_as_int(double* data_out_double, double int_units,
                     int num_bytes_to_decode, const char* hex_str_in,
                     int hex_str_in_max_len)
{
  int data_out_int;
  int status = 0;

  status = decode_signed_int(&data_out_int, num_bytes_to_decode, hex_str_in,
                             hex_str_in_max_len);

  if (status <= 0)
    return (status);

  *data_out_double = ((double)data_out_int) * int_units;

  return (num_bytes_to_decode * 2);
}

/* ------------------------------------------------------------
Function to convert
data structure
to
hex encoded 128 byte hex encoded ascii string.


2008-07-20   LLW     Created
------------------------------------------------------------ */
int
UMODEM_Encode_HROVStateXYData(umodem_hrov_statexy_t* hrovdata, char* hex_str,
                              int max_hex_str_len)
{
  int len = 0;
  unsigned int umodem_code = UMODEMHROVSTATEXY;

  // only works for 128 character hex srings
  if (max_hex_str_len < 128)
    return (-1);

  // encode message type code as 8 bit unsigned integer
  // i.e. 0 to 255
  len +=
    encode_unsigned_int(&umodem_code, 1, &hex_str[len], max_hex_str_len - len);

  // Encode  top of second and sync nav mode. The umodems process will overwrite
  // his in sync nav mode.
  unsigned int sync_nav_encoded_byte;
  sync_nav_encoded_byte =
    ((hrovdata->tos & 0x3F) << 2) | (hrovdata->sync_nav_mode & 0x03);
  len += encode_unsigned_int(&sync_nav_encoded_byte, 1, &hex_str[len],
                             max_hex_str_len - len);

  // encode X and Y as 24 bit signed integer deci-meters
  // i.e. -838,860.8 to +838,860.7 meters
  len += encode_double_as_int(hrovdata->x, 0.1, 3, &hex_str[len],
                              max_hex_str_len - len);

  len += encode_double_as_int(hrovdata->y, 0.1, 3, &hex_str[len],
                              max_hex_str_len - len);

  // encode depth as 16 bit signed integer 0.5 meter units
  // i.e. -16,384 to +16,383.5
  len += encode_double_as_int(hrovdata->depth, 0.5, 2, &hex_str[len],
                              max_hex_str_len - len);

  // encode altitude as 16 bit signed integer 0.5 meter units
  // i.e. -16,384 to +16,383.5
  len += encode_double_as_int(hrovdata->alt, 0.5, 2, &hex_str[len],
                              max_hex_str_len - len);

  // encode heading as 16 bit signed integer deci-degrees
  // i.e. -3,276.8 to +3,276.7
  len += encode_double_as_int(hrovdata->heading_deg, 0.1, 2, &hex_str[len],
                              max_hex_str_len - len);

  // encode goal XYX as 16 bit unsigned integer in units of 0.5m
  // i.e. -16,384 to +16,383.5
  len += encode_double_as_int(hrovdata->x_goal, 0.5, 3, &hex_str[len],
                              max_hex_str_len - len);

  len += encode_double_as_int(hrovdata->y_goal, 0.5, 3, &hex_str[len],
                              max_hex_str_len - len);

  len += encode_double_as_int(hrovdata->z_goal, 0.5, 3, &hex_str[len],
                              max_hex_str_len - len);

  // encode XYZ velocity as 8 bit signed integer in units of 0.025 m/s
  // i.e. -3.175 to +3.175 m/s
  len += encode_double_as_int(hrovdata->u_vel, 0.025, 1, &hex_str[len],
                              max_hex_str_len - len);

  len += encode_double_as_int(hrovdata->v_vel, 0.025, 1, &hex_str[len],
                              max_hex_str_len - len);

  len += encode_double_as_int(hrovdata->w_vel, 0.025, 1, &hex_str[len],
                              max_hex_str_len - len);

  // encode abort status as 16 bit integer
  // i.e. -32768 to 32767
  len += encode_signed_int(&hrovdata->abort_status, 2, &hex_str[len],
                           max_hex_str_len - len);

  // encode control configuration as 8 bit unsigned integer
  // i.e. 0 to 255
  len += encode_unsigned_int(&hrovdata->cfg, 1, &hex_str[len],
                             max_hex_str_len - len);

  // encode battery percent as 8 bit signed integer percent
  // i.e. -128 to +127 percent
  len += encode_double_as_int(hrovdata->batt_percent, 0.1, 2, &hex_str[len],
                              max_hex_str_len - len);

  // encode time remaining is encoded in in units of 10 minutes
  // max time is 255 600-second intervals (25.5 hours)
  unsigned time_remaining_int;
  time_remaining_int = ((int)(hrovdata->batt_time_remaining / 600.0));

  // 2009/10/15 18:47:20 LLW added saturation to time_remaining_int
  if (time_remaining_int > 255)
    time_remaining_int = 255;

  len += encode_unsigned_int(&time_remaining_int, 1, &hex_str[len],
                             max_hex_str_len - len);

  // encode message TX count as 8 bit unsigned integer
  //  i.e. 0 to 255
  len += encode_unsigned_int(&hrovdata->tx_count, 1, &hex_str[len],
                             max_hex_str_len - len);

  // encode message RX count as 8 bit unsigned integer
  // i.e. 0 to 255
  len += encode_unsigned_int(&hrovdata->rx_count, 1, &hex_str[len],
                             max_hex_str_len - len);

  // pack high 8 bits with num ping replies
  // 0 to 255
  // 2009/10/15   LLW     changed num_ping_replies field in statexy to dvl
  // status
  len += encode_unsigned_int(&hrovdata->dvl_status, 1, &hex_str[len],
                             max_hex_str_len - len);

  // ZB: 12-04-2014   Added current trackline number
  len += encode_unsigned_int(&hrovdata->trackline_number, 2, &hex_str[len],
                             max_hex_str_len - len);

#ifdef DEBUG_UMODEM_MSGS
  int encoded_len = len;
#endif

  // need to generate a 128 character hex string, so pad the remaining bytes
  // with zeros
  while (len < 128) {
    hex_str[len++] = '0';
  }

  // null terminate
  hex_str[len++] = 0;

// 2009-04-04 LLW debug messages
#ifdef DEBUG_UMODEM_MSGS
  stderr_printf("UMODEM_MSG_UTIL.CPP: Encoded STATEXY    message using %d of "
                "64 bytes, %d of 128 hexidecimal characters \"%s\"\n",
                (encoded_len / 2), encoded_len, hex_str);
#endif

  return (len);
}

/* ------------------------------------------------------------
Function to convert
hex encoded 128 byte hex encoded ascii string
to
data structure



2008-07-20   LLW     Created
------------------------------------------------------------ */
int
UMODEM_Decode_HROVStateXYData(umodem_hrov_statexy_t* hrovdata,
                              const char* hex_str, int max_hex_str_len)

{
  int len = 0;
  unsigned umodem_code;

  // only works for 128 character hex srings
  if (max_hex_str_len < 128) {
    qCritical("UMODEM_MSG_UTIL.CPP: ERROR in "
              "UMODEM_Decode_HROVStateXYData: rx packet data len of %d is "
              "less than the required 128 chars",
              max_hex_str_len);

    return (-1);
  }

  // first byte value must be UMODEMHROVSTATEXY (see header file)
  len +=
    decode_unsigned_int(&umodem_code, 1, &hex_str[len], max_hex_str_len - len);

  if (umodem_code != UMODEMHROVSTATEXY) {
    qCritical("UMODEM_MSG_UTIL.CPP: ERROR data packet ccl code was %d (%s) "
              "expected %d (%s)",
              umodem_code, UMODEM_DATA_PACKET_TYPE_INT_TO_NAME(umodem_code),
              UMODEMHROVSTATEXY,
              UMODEM_DATA_PACKET_TYPE_INT_TO_NAME(UMODEMHROVSTATEXY));
    return (-2);
  } else {
    qInfo("UMODEM_MSG_UTIL.CPP: Decoding data packet type %d (%s)",
          UMODEMHROVSTATEXY,
          UMODEM_DATA_PACKET_TYPE_INT_TO_NAME(UMODEMHROVSTATEXY));
  }

  // Decode top of second and sync nav mode.
  unsigned int sync_nav_encoded_byte;

  len += decode_unsigned_int(&sync_nav_encoded_byte, 1, &hex_str[len],
                             max_hex_str_len - len);

  hrovdata->tos = ((sync_nav_encoded_byte >> 2) & 0x3F);
  hrovdata->sync_nav_mode = (sync_nav_encoded_byte & 0x03);

  // decode X and Y as 24 bit signed integer deci-meters
  // i.e. -838,860.8 to +838,860.7 meters
  len += decode_double_as_int(&(hrovdata->x), 0.1, 3, &hex_str[len],
                              max_hex_str_len - len);

  len += decode_double_as_int(&hrovdata->y, 0.1, 3, &hex_str[len],
                              max_hex_str_len - len);

  // decode depth as 16 bit signed integer 0.5 meter units
  // i.e. -16,384 to +16,383.5
  len += decode_double_as_int(&hrovdata->depth, 0.5, 2, &hex_str[len],
                              max_hex_str_len - len);

  // decode altitude as 16 bit signed integer 0.5 meter units
  // i.e. -16,384 to +16,383.5
  len += decode_double_as_int(&hrovdata->alt, 0.5, 2, &hex_str[len],
                              max_hex_str_len - len);

  // decode heading as 16 bit signed integer deci-degrees
  // i.e. -3,276.8 to +3,276.7
  len += decode_double_as_int(&hrovdata->heading_deg, 0.1, 2, &hex_str[len],
                              max_hex_str_len - len);

  // decode goal X,Y,Z as 16 bit unsigned integer in units of 0.5m
  // i.e. -16,384 to +16,383.5
  len += decode_double_as_int(&hrovdata->x_goal, 0.5, 3, &hex_str[len],
                              max_hex_str_len - len);

  len += decode_double_as_int(&hrovdata->y_goal, 0.5, 3, &hex_str[len],
                              max_hex_str_len - len);

  len += decode_double_as_int(&hrovdata->z_goal, 0.5, 3, &hex_str[len],
                              max_hex_str_len - len);

  // decode XYZ velocity as 8 bit signed integer in units of 0.025 m/s
  // i.e. -3.175 to +3.175 m/s
  len += decode_double_as_int(&hrovdata->u_vel, 0.025, 1, &hex_str[len],
                              max_hex_str_len - len);

  len += decode_double_as_int(&hrovdata->v_vel, 0.025, 1, &hex_str[len],
                              max_hex_str_len - len);

  len += decode_double_as_int(&hrovdata->w_vel, 0.025, 1, &hex_str[len],
                              max_hex_str_len - len);

  // decode abort status as 16 bit integer
  // i.e. -32768 to 32767
  len += decode_signed_int(&hrovdata->abort_status, 2, &hex_str[len],
                           max_hex_str_len - len);

  // decode control configuration as 8 bit unsigned integer
  // i.e. 0 - 255
  len += decode_unsigned_int(&hrovdata->cfg, 1, &hex_str[len],
                             max_hex_str_len - len);

  // decode battery percent as 8 bit signed integer percent
  // i.e. -128 to +127 percent
  len += decode_double_as_int(&hrovdata->batt_percent, 0.1, 2, &hex_str[len],
                              max_hex_str_len - len);

  // decode time remaining is encoded in in units of 6 minutes
  // max time is 255 600-second intervals (25.5 hours)
  unsigned time_remaining_int;
  len += decode_unsigned_int(&time_remaining_int, 1, &hex_str[len],
                             max_hex_str_len - len);

  hrovdata->batt_time_remaining = 600.0 * time_remaining_int;

  // decode message TX count as 8 bit unsigned integer
  // i.e. 0 to 255
  len += decode_unsigned_int(&hrovdata->tx_count, 1, &hex_str[len],
                             max_hex_str_len - len);

  // decode message RX count as 8 bit unsigned integer
  // i.e. 0 to 255
  len += decode_unsigned_int(&hrovdata->rx_count, 1, &hex_str[len],
                             max_hex_str_len - len);

  // decode num ping replies as 8 bit unsigned integer
  // i.e. 0 to 255
  len += decode_unsigned_int(&hrovdata->dvl_status, 1, &hex_str[len],
                             max_hex_str_len - len);

  len += decode_unsigned_int(&hrovdata->trackline_number, 2, &hex_str[len],
                             max_hex_str_len - len);

  return (0);
}

/* ------------------------------------------------------------
Function to convert binary data structure to NMEA $CATXD message

200-07-20   LLW     Created
------------------------------------------------------------ */
int
UMODEM_WriteHROVStateXYMsg(char* msg, int max_msg_len, int src, int dest,
                           int ack, umodem_hrov_statexy_t* hrovdata)
{
  int len = 0;

  /* snprint the header */
  len += snprintf(&msg[len], max_msg_len - len - 1, "$CCTXD,%d,%d,%d,", src,
                  dest, ack);

  /* encode the hex data */
  len +=
    UMODEM_Encode_HROVStateXYData(hrovdata, &msg[len], max_msg_len - len - 1);

  // add the NMEA checksum
  // 2009-05-27 LLW Removed, not needed
  //  len += sprintf(&msg[len],"*%02X\r\n",nmea_check_sum(&msg[1]));

  // add cr lf terminator
  len += snprintf(&msg[len], max_msg_len - len - 1, "\r\n");

  // log it
  qInfo("UMODEM_MSG_UTIL: encoded STATEXY as: %s", msg);

  return (len);
}

/* ------------------------------------------------------------
Function to convert NMEA $CARXD message to binary data structure

200-07-20   LLW     Created
------------------------------------------------------------ */

int
UMODEM_ReadHROVStateXYMsg(const char* msg, int msg_len, double time_now,
                          umodem_hrov_statexy_t* hrovdata)
{
  int arg_num = 0;
  int tmp_src;
  int tmp_dest;
  int tmp_ack;
  int tmp_frame_number;
  umodem_hrov_statexy_t tmp_hrovdata;
  int hex_str_begin = -1;
  int status;

  // clear the tmp data struct */
  memset(&tmp_hrovdata, 0, sizeof(umodem_hrov_statexy_t));

  /* parse the header */
  arg_num = sscanf(msg, "$CARXD,%d,%d,%d,%d,%n", &tmp_src, &tmp_dest, &tmp_ack,
                   &tmp_frame_number, &hex_str_begin);

  if (arg_num != 4)
    return (-1);

  if (hex_str_begin == -1)
    return (-2);

  qInfo("UMODEM_MSG_UTIL.CPP: hex str is   %s", &msg[hex_str_begin]);

  status = UMODEM_Decode_HROVStateXYData(&tmp_hrovdata, &msg[hex_str_begin],
                                         msg_len - hex_str_begin);

  // if we did not parse successfully, abort
  if (status != 0)
    return (-1);

  // ok, no errors, so assign the data
  // *src          = tmp_src;
  // *dest         = tmp_dest;
  // *ack          = tmp_ack;
  // *frame_number = tmp_frame_number;

  /* assign header info */
  tmp_hrovdata.source = tmp_src;
  tmp_hrovdata.dest = tmp_dest;
  tmp_hrovdata.ack_requested = tmp_ack;
  tmp_hrovdata.frame_number = tmp_frame_number;

  /* additional info */
  tmp_hrovdata.time_stamp_received = time_now;
  tmp_hrovdata.rx_packet_count = ++rx_packet_kount;

  /* assume first byte is already checked by the calling function and has value
     UMODEMHROVSTATEXY */

  /* assign data */
  *hrovdata = tmp_hrovdata;

  return (0);
}

/* ------------------------------------------------------------
   Function to convert
   data structure
   to
   hex encoded 128 byte hex encoded ascii string.

   2009-10-19   LLW     Created
   ------------------------------------------------------------ */
int
UMODEM_Encode_HROV_JasonTalk_ASCII_Data(umodem_jasontalk_ascii_t* data,
                                        char* hex_str, int max_hex_str_len)
{
  unsigned int i = 0;
  int len = 0;
  unsigned int umodem_code = UMODEMHROVJASONTALK;
  // int data_str_len = strlen(data->jt_str);;

  // get the string length

  // only works for 128 character hex srings
  if (max_hex_str_len < 128)
    return (-1);

  // encode message type code as 8 bit unsigned integer
  // i.e. 0 to 255
  len +=
    encode_unsigned_int(&umodem_code, 1, &hex_str[len], max_hex_str_len - len);

  // Encode  top of second and sync nav mode. The umodems process will overwrite
  // his in sync nav mode.
  unsigned int sync_nav_encoded_byte;
  sync_nav_encoded_byte =
    ((data->tos & 0x3F) << 2) | (data->sync_nav_mode & 0x03);
  qInfo("UMODEM_MSG_UTIL: sync_nav_encoded_byte=0x%02X, tos=0x%02X, "
        "sync_nav_mode=0x%02X",
        sync_nav_encoded_byte, data->tos, data->sync_nav_mode);

  len += encode_unsigned_int(&sync_nav_encoded_byte, 1, &hex_str[len],
                             max_hex_str_len - len);
  i = 0;
  // hex encode ascii string
  while ((len < max_hex_str_len) && (len < 128) && (0 != data->jt_str[i]) &&
         (i < MAX_UMODEM_JASONTALK_ASCII_STRING_LENGTH)) {
    len +=
      snprintf(&hex_str[len], max_hex_str_len - len, "%02X", data->jt_str[i]);
    i++;
  }

// need to generate a 128 character hex string, so pad the remaining bytes with
// zeros
#ifdef DEBUG_UMODEM_MSGS
  int encoded_len = len;
#endif

  while (len < 128) {
    hex_str[len++] = '0';
  }

  // null terminate
  hex_str[len++] = 0;

// 2009-04-04 LLW debug messages
#ifdef DEBUG_UMODEM_MSGS
  stderr_printf("UMODEM_MSG_UTIL.CPP: Encoded Jasontalk  message using %d of "
                "64 bytes, %d of 128 hexidecimal characters \"%s\"\n",
                (encoded_len / 2), encoded_len, hex_str);
#endif

  return (len);
}

/* ------------------------------------------------------------
   Function to convert
   hex encoded 128 byte hex encoded ascii string
   to
   data structure

   2009-10-19   LLW     Created
   ------------------------------------------------------------ */
int
UMODEM_Decode_HROV_JasonTalk_ASCII_Data(umodem_jasontalk_ascii_t* data,
                                        const char* hex_str,
                                        int max_hex_str_len)

{
  unsigned int i;
  int len = 0;
  unsigned umodem_code = 0;
  unsigned tmp_unsigned;

  // only works for 128 character hex srings
  if (max_hex_str_len < 128) {
    qCritical("UMODEM_MSG_UTIL.CPP: ERROR in "
              "UMODEM_Decode_HROV_JasonTalk_ASCII_Data: rx packet data len "
              "of %d is less than the required 128",
              max_hex_str_len);
    return (-1);
  }

  // first byte value must be UMODEMHROVJASONTALK (see header file)
  len +=
    decode_unsigned_int(&umodem_code, 1, &hex_str[len], max_hex_str_len - len);

  if (umodem_code != UMODEMHROVJASONTALK) {
    qCritical("UMODEM_MSG_UTIL.CPP: ERROR data packet ccl code was %d (%s) "
              "expected %d (%s)",
              umodem_code, UMODEM_DATA_PACKET_TYPE_INT_TO_NAME(umodem_code),
              UMODEMHROVJASONTALK,
              UMODEM_DATA_PACKET_TYPE_INT_TO_NAME(UMODEMHROVJASONTALK));
    return (-2);
  } else {
    qInfo("UMODEM_MSG_UTIL.CPP: Decoding data packet type %d (%s)\n",
          UMODEMHROVJASONTALK,
          UMODEM_DATA_PACKET_TYPE_INT_TO_NAME(UMODEMHROVJASONTALK));
  }

  // Decode top of second and sync nav mode.
  unsigned sync_nav_encoded_byte = 0;
  len += decode_unsigned_int(&sync_nav_encoded_byte, 1, &hex_str[len],
                             max_hex_str_len - len);

  data->tos = (double)((sync_nav_encoded_byte >> 2) & 0x3F);
  data->sync_nav_mode = (double)(sync_nav_encoded_byte & 0x03);

  // decode ascii string
  for (i = 0; (i < MAX_UMODEM_JASONTALK_ASCII_STRING_LENGTH); i++) {
    sscanf(&hex_str[4 + (2 * i)], "%02X", &tmp_unsigned);
    data->jt_str[i] = tmp_unsigned & 0xFF;
  }

  // null terminate
  data->jt_str[MAX_UMODEM_JASONTALK_ASCII_STRING_LENGTH] = 0;

  // done
  return (0);
}
/* ------------------------------------------------------------
   Function to convert
   data structure
   to
   hex encoded 128 byte hex encoded ascii string.

   2009-10-19   LLW     Created
   ------------------------------------------------------------ */
int
UMODEM_Encode_HROV_JasonTalk_ASCII_Msg(umodem_jasontalk_ascii_t* hrovdata,
                                       char* hex_str, int max_hex_str_len)
{
  int i = 0;
  int len = 0;
  unsigned int umodem_code = UMODEMHROVJASONTALK;
  // int data_str_len = strlen(hrovdata->jt_str);;

  // get the string length

  // only works for 128 character hex srings
  if (max_hex_str_len < 128)
    return (-1);

  // encode message type code as 8 bit unsigned integer
  // i.e. 0 to 255
  len +=
    encode_unsigned_int(&umodem_code, 1, &hex_str[len], max_hex_str_len - len);

  // Encode  top of second and sync nav mode. The umodems process will overwrite
  // his in sync nav mode.
  unsigned int sync_nav_encoded_byte;
  sync_nav_encoded_byte =
    ((hrovdata->tos & 0x3F) << 2) | (hrovdata->sync_nav_mode & 0x03);
  qDebug("UMODEM_MSG_UTIL: sync_nav_encoded_byte=0x%02X, tos=0x%02X, "
         "sync_nav_mode=0x%02X",
         sync_nav_encoded_byte, hrovdata->tos, hrovdata->sync_nav_mode);

  len += encode_unsigned_int(&sync_nav_encoded_byte, 1, &hex_str[len],
                             max_hex_str_len - len);
  i = 0;
  // hex encode ascii string
  while ((len < max_hex_str_len) && (len < 128) && (0 != hrovdata->jt_str[i]) &&
         (i < static_cast<int>(MAX_UMODEM_JASONTALK_ASCII_STRING_LENGTH))) {
    len += snprintf(&hex_str[len], max_hex_str_len - len, "%02X",
                    hrovdata->jt_str[i]);
    i++;
  }

// need to generate a 128 character hex string, so pad the remaining bytes with
// zeros
#ifdef DEBUG_UMODEM_MSGS
  int encoded_len = len;
#endif

  while (len < 128) {
    hex_str[len++] = '0';
  }

  // null terminate
  hex_str[len++] = 0;

// 2009-04-04 LLW debug messages
#ifdef DEBUG_UMODEM_MSGS
  stderr_printf("UMODEM_MSG_UTIL.CPP: Encoded Jasontalk  message using %d of "
                "64 bytes, %d of 128 hexidecimal characters \"%s\"\n",
                (encoded_len / 2), encoded_len, hex_str);
#endif

  return (len);
}

/* ------------------------------------------------------------
   Function to convert
   hex encoded 128 byte hex encoded ascii string
   to
   data structure

   2009-10-19   LLW     Created
   ------------------------------------------------------------ */
int
UMODEM_Decode_HROV_JasonTalk_ASCII_Msg(umodem_jasontalk_ascii_t* hrovdata,
                                       const char* hex_str, int max_hex_str_len)

{
  int i;
  int len = 0;
  unsigned umodem_code = 0;
  unsigned tmp_unsigned;

  // only works for 128 character hex srings
  if (max_hex_str_len < 128) {
    qCritical("UMODEM_MSG_UTIL.CPP: ERROR in "
              "UMODEM_Decode_HROV_JasonTalk_ASCII_Data: rx packet data len "
              "of %d is less than the required 128",
              max_hex_str_len);
    return (-1);
  }

  // first byte value must be UMODEMHROVJASONTALK (see header file)
  len +=
    decode_unsigned_int(&umodem_code, 1, &hex_str[len], max_hex_str_len - len);

  if (umodem_code != UMODEMHROVJASONTALK) {
    qCritical("UMODEM_MSG_UTIL.CPP: ERROR data packet ccl code was %d (%s) "
              "expected %d (%s)",
              umodem_code, UMODEM_DATA_PACKET_TYPE_INT_TO_NAME(umodem_code),
              UMODEMHROVJASONTALK,
              UMODEM_DATA_PACKET_TYPE_INT_TO_NAME(UMODEMHROVJASONTALK));
    return (-2);
  } else {
    qInfo("UMODEM_MSG_UTIL.CPP: Decoding data packet type %d (%s)\n",
          UMODEMHROVJASONTALK,
          UMODEM_DATA_PACKET_TYPE_INT_TO_NAME(UMODEMHROVJASONTALK));
  }

  // Decode top of second and sync nav mode.
  unsigned sync_nav_encoded_byte = 0;
  len += decode_unsigned_int(&sync_nav_encoded_byte, 1, &hex_str[len],
                             max_hex_str_len - len);

  hrovdata->tos = (double)((sync_nav_encoded_byte >> 2) & 0x3F);
  hrovdata->sync_nav_mode = (double)(sync_nav_encoded_byte & 0x03);

  // decode ascii string
  for (i = 0; (i < static_cast<int>(MAX_UMODEM_JASONTALK_ASCII_STRING_LENGTH));
       i++) {
    sscanf(&hex_str[4 + (2 * i)], "%02X", &tmp_unsigned);
    hrovdata->jt_str[i] = tmp_unsigned & 0xFF;
  }

  // null terminate
  hrovdata->jt_str[MAX_UMODEM_JASONTALK_ASCII_STRING_LENGTH] = 0;

  // done
  return (0);
}

/* ------------------------------------------------------------
   Function to write a fake CARXD message.

   For testing.

   CCRXD messages are normally generated by the modem upon receipt
   of an acoustic data packate cycle init and data packet pair.

   2009-10-19   LLW     Created
   ----------------------------------------------------------- */

/* ------------------------------------------------------------
   Function to convert binary data structure to NMEA $CATXD message

   2009-10-19   LLW     Created
   ------------------------------------------------------------ */
int
UMODEM_WriteHROV_JasonTalk_ASCII_Msg(char* msg, int max_msg_len, int src,
                                     int dest, int ack,
                                     umodem_jasontalk_ascii_t* data)
{
  int len = 0;

  /* snprint the header */
  len += snprintf(&msg[len], max_msg_len - len - 1, "$CCTXD,%d,%d,%d,", src,
                  dest, ack);

  /* encode the hex data */
  len += UMODEM_Encode_HROV_JasonTalk_ASCII_Data(data, &msg[len],
                                                 max_msg_len - len - 1);

  // add the NMEA checksum
  // 2009-05-27 LLW Removed, not needed
  //  len += sprintf(&msg[len],"*%02X\r\n",nmea_check_sum(&msg[1]));

  // add cr lf terminator
  len += snprintf(&msg[len], max_msg_len - len - 1, "\r\n");

  // log it
  qInfo("UMODEM_MSG_UTIL: encoded JASONTALK01 as: %s", msg);

  return (len);
}

/* ------------------------------------------------------------
   Function to convert NMEA $CARXD message to binary data structure

   2009-10-19   LLW     Created
   ------------------------------------------------------------ */

int
UMODEM_ReadHROV_JasonTalk_ASCII_Msg(const char* msg, int msg_len,
                                    double time_now,
                                    umodem_jasontalk_ascii_t* data)
{
  int arg_num = 0;
  int tmp_src;
  int tmp_dest;
  int tmp_ack;
  int tmp_frame_number;
  umodem_jasontalk_ascii_t tmp_data;
  int hex_str_begin = -1;
  int status;
  // static int kount = 0;

  // clear the tmp data struct */
  memset(&tmp_data, 0, sizeof(umodem_jasontalk_ascii_t));

  /* parse the header */
  arg_num = sscanf(msg, "$CARXD,%d,%d,%d,%d,%n", &tmp_src, &tmp_dest, &tmp_ack,
                   &tmp_frame_number, &hex_str_begin);

  if (arg_num != 4)
    return (-1);

  if (hex_str_begin == -1)
    return (-2);

  qInfo("UMODEM_MSG_UTIL: hex str %s", &msg[hex_str_begin]);

  status = UMODEM_Decode_HROV_JasonTalk_ASCII_Data(
    &tmp_data, &msg[hex_str_begin], msg_len - hex_str_begin);

  // if we did not parse successfully, abort
  if (status != 0)
    return (-1);

  // assign header info
  tmp_data.source = tmp_src;
  tmp_data.dest = tmp_dest;
  tmp_data.ack_requested = tmp_ack;
  tmp_data.frame_number = tmp_frame_number;

  // additional info
  tmp_data.time_stamp_received = time_now;
  tmp_data.rx_packet_count = ++rx_packet_kount;

  /* assume first byte is already checked by the calling function and has value
     UMODEMHROVSTATEXY */

  /* assign data */
  *data = tmp_data;

  return (0);
}

void
hrov_statexy_to_sentry_vehicle_state(const umodem_hrov_statexy_t& hrov,
                                     QVariantHash& state)
{
  state["x"] = static_cast<float>(hrov.x);
  state["y"] = static_cast<float>(hrov.y);
  state["heading"] = static_cast<float>(hrov.heading_deg);
  state["depth"] = static_cast<float>(hrov.depth);
  state["alt"] = static_cast<float>(hrov.alt);
  state["goal_x"] = static_cast<float>(hrov.x_goal);
  state["goal_y"] = static_cast<float>(hrov.y_goal);
  state["goal_z"] = static_cast<float>(hrov.z_goal);
  state["h_vel"] = static_cast<float>(hrov.u_vel);
  state["v_vel"] = static_cast<float>(hrov.w_vel);
  state["abort"] = static_cast<uint8_t>(hrov.abort_status);
  state["battery"] = static_cast<float>(hrov.batt_percent);
  state["trackline"] = static_cast<uint16_t>(hrov.trackline_number);
}

void
sentry_vehicle_state_to_hrov_statexy(const QVariantHash& state,
                                     umodem_hrov_statexy_t& hrov)
{
  memset(static_cast<void*>(&hrov), 0, sizeof(hrov));

  hrov.x = state["x"].toFloat();
  hrov.y = state["y"].toFloat();

  hrov.heading_deg = state["heading"].toFloat();
  hrov.depth = state["depth"].toFloat();
  hrov.alt = state["alt"].toFloat();

  hrov.x_goal = state["goal_x"].toFloat();
  hrov.y_goal = state["goal_y"].toFloat();
  hrov.z_goal = state["goal_z"].toFloat();

  hrov.u_vel = state["h_vel"].toFloat();
  hrov.w_vel = state["v_vel"].toFloat();

  hrov.abort_status = state["abort"].toBool();

  hrov.batt_percent = state["battery"].toFloat();
  hrov.trackline_number = state["trackline"].toUInt();
}
}
}
}
}