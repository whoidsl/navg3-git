/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 1/8/19.
//

#include "SentryLatestStatus.h"
#include "SentryAcomms.h"
#include "Serialization.h"
#include "ui_SentryLatestStatus.h"

#include <QTimer>
#include <QWheelEvent>

namespace navg {
namespace plugins {
namespace sentry_acomms {

SentryLatestStatus::SentryLatestStatus(QWidget* parent)
  : QWidget(parent)
  , ui(new Ui::SentryLatestStatus)
  , m_t_umodem(-1)
  , m_t_sms(-1)
  , m_t_wg(-1)
  , m_fontsize(10)
{
  ui->setupUi(this);
  m_font = ui->label->font();

  QTimer* t = new QTimer(this);
  connect(t, &QTimer::timeout, [=]() {
    if (m_t_sms != -1) {
      ui->age_sms->setText("Age: " + QString::number(m_t_sms));
      if (m_t_sms == 120) {
        ui->groupBox_sms->setStyleSheet(
          "QGroupBox { border:1px solid yellow} ");
      }
      m_t_sms++;
    }
    if (m_t_umodem != -1) {
      ui->age_umodem->setText("Age: " + QString::number(m_t_umodem));
      if (m_t_umodem == 120) {
        ui->groupBox_umodem->setStyleSheet(
          "QGroupBox { border:1px solid yellow} ");
      }
      m_t_umodem++;
    }
    if (m_t_wg != -1) {
      ui->age_waveglider->setText("Age: " + QString::number(m_t_wg));
      if (m_t_wg == 120) {
        ui->groupBox_waveglider->setStyleSheet(
          "QGroupBox { border:1px solid yellow} ");
      }
      m_t_wg++;
    }
  });
  t->start(1000);
}

SentryLatestStatus::~SentryLatestStatus()
{
  delete ui;
}

void
SentryLatestStatus::receiveMessage(int modem, const QVariantHash message)
{
  auto type = static_cast<MessageType>(message["type"].toInt());
  auto queue = static_cast<QueueType>(message["queue"].toInt());
  if (!type == MessageType::DataQueue || !queue == QueueType::State) {
    return;
  }

  auto source = static_cast<SentryAcomms::Modem>(modem);

  switch (source) {
    case SentryAcomms::Modem::AvTrak:
      if (m_t_sms == -1 || m_t_sms >= 120) {
        ui->groupBox_sms->setStyleSheet("QGroupBox { border:1px solid green} ");
      }
      m_t_sms = 0;
      break;
    case SentryAcomms::Modem::MicroModem:
      if (m_t_umodem == -1 || m_t_umodem >= 120) {
        ui->groupBox_umodem->setStyleSheet(
          "QGroupBox { border:1px solid green} ");
      }
      m_t_umodem = 0;
      break;
    case SentryAcomms::Modem::Waveglider:
      if (m_t_wg == -1 || m_t_wg >= 120) {
        ui->groupBox_waveglider->setStyleSheet(
          "QGroupBox { border:1px solid green} ");
      }
      m_t_wg = 0;
      break;
    default:
      break;
  }
  ui->alt->setText(QString::number(message["alt"].toDouble()));
  ui->depth->setText(QString::number(message["depth"].toDouble()));
  ui->pos_x->setText(QString::number(message["x"].toDouble()));
  ui->pos_y->setText(QString::number(message["y"].toDouble()));
  ui->vel_h->setText(QString::number(message["h_vel"].toDouble()));
  ui->vel_z->setText(QString::number(message["v_vel"].toDouble()));
  ui->goal_x->setText(QString::number(message["goal_x"].toDouble()));
  ui->goal_y->setText(QString::number(message["goal_y"].toDouble()));
  ui->goal_z->setText(QString::number(message["goal_z"].toDouble()));
  ui->battery->setText(QString::number(message["battery"].toDouble()));
  ui->trackline->setText(QString::number(message["trackline"].toInt()));
  ui->hdg->setText(QString::number(message["heading"].toDouble()));
}

void
SentryLatestStatus::wheelEvent(QWheelEvent* e)
{
  QPoint numDegrees = e->angleDelta() / 24;
  if (!numDegrees.isNull()) {
    setFontsize(fontsize() + numDegrees.y());
    e->accept();
    qDebug() << "wheelEvent accepted! " << numDegrees;
  } else {
    e->ignore();
    qDebug() << "wheelEvent ignored!";
  }
}

void
SentryLatestStatus::setFontsize(int fontsize)
{
  if (fontsize == m_fontsize) {
    return;
  } else if (fontsize < 5) {
    m_fontsize = 5;
  } else if (fontsize > 25) {
    m_fontsize = 25;
  } else {
    m_fontsize = fontsize;
  }

  QFont font(m_font.family(), m_fontsize, m_font.style());
  ui->alt->setFont(font);
  ui->depth->setFont(font);
  ui->pos_x->setFont(font);
  ui->pos_y->setFont(font);
  ui->vel_h->setFont(font);
  ui->vel_z->setFont(font);
  ui->goal_x->setFont(font);
  ui->goal_y->setFont(font);
  ui->goal_z->setFont(font);
  ui->battery->setFont(font);
  ui->trackline->setFont(font);
  ui->hdg->setFont(font);
}

} // namespace
}
}
