/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
///@file Old CCL-style encoding from ROV for Sentry
///
/// This file contains serialization routines for the StateXY
/// and JasonTalk message types transmitted via Whoi Micromodem
/// as set in the old ROV code.
///
/// These functions are intended to be transitory -- that is, they
/// are the temporary middlewhere to make the subsea ROS infrastructure
/// compatible with the old topside system until topside can be converted
/// later.
///
/// Files poached from rov:
///
///  - umodem_msg_util.h (.cpp)

#ifndef PROJECT_LEGACY_MICROMODEM_SERIALIZATION_H
#define PROJECT_LEGACY_MICROMODEM_SERIALIZATION_H

#include <QVariantHash>
#include <stdint.h>

namespace navg {
namespace plugins {
namespace sentry_acomms {
namespace legacy_micromodem {

enum MessageType
{
  UMODEMREMUSSTATEXY = 33,
  UMODEMHROVSTATEXY = 100,
  UMODEMHROVSTATEXYRESET = 101,
  UMODEMHROVJASONTALK = 102,
  UMODEMHROVACOMMSSTATUS01 = 103,
  UMODEMHROVSCIENCE01 = 104,
  UMODEMHROVSCIENCE02 = 105,
};

const static size_t MAX_ACCOM_MSG_LEN = 128;
const static size_t MAX_UMODEM_JASONTALK_ASCII_STRING_LENGTH = 62;

const char* UMODEM_DATA_PACKET_TYPE_INT_TO_NAME(int num);

/// Function to parse and convert NMEA UMODEM message type and return it as an
/// integer
///
/// returns zero on success
/// returns nonzero on failure
///
/// 2009-05-13   LLW     Created
int UMODEM_Decode_CARXD_Message_Type(const char* nmea_str, int* message_type);

typedef struct
{
  /* binary data to be encoded/decoded */
  double x;           /* X (meters) */
  double y;           /* Y (meters) */
  double depth;       /* depth (meters) */
  double alt;         /* altitude (meters) */
  double heading_deg; /* heading in degrees */

  double x_goal; /* X goal (meters) */
  double y_goal; /* Y goal (meters) */
  double z_goal; /* Z goal (meters) */

  double u_vel; /* u vel  (meters/sec) */
  double v_vel; /* v vel  (meters/sec) */
  double w_vel; /* w vel  (meters/sec) */

  int abort_status; /* abort status */

  unsigned int cfg; /* HROV flight mode */

  double batt_percent;        /* battery percent */
  double batt_time_remaining; /* seconds until batteries are drained to zero */

  unsigned int
    tx_count; /* number of data packets TX'd by sender (including this one) */
  unsigned int rx_count; /* number of data packets RX'd by sender  */

  // 2009/10/15   LLW     changed num_ping_replies field in statexy to dvl
  // status (6 bits)
  unsigned int dvl_status; /* encoded as 6 bits */

  unsigned int tos;           /* integer second 0-59, encoded as 8 bits */
  unsigned int sync_nav_mode; /* 0, 1, 2, 3  only, encoded as two bits */

  /* packet header information for the NMEA modem string */
  int source;
  int dest;
  int ack_requested;
  int frame_number;

  /* additional info */
  double time_stamp_received; /* time of last successful decoded rx */
  unsigned rx_packet_count;   /* total number of packets decided rx */

  double servoAngle[2]; // 2013-12-15 JCK - added servo angles for Sentry

  unsigned int trackline_number;
} umodem_hrov_statexy_t;

void hrov_statexy_to_sentry_vehicle_state(const umodem_hrov_statexy_t& hrov,
                                          QVariantHash& state);

void sentry_vehicle_state_to_hrov_statexy(const QVariantHash& state,
                                          umodem_hrov_statexy_t& hrov);

/// Function to convert binary data structure to NMEA $CATXD message
/// 200-07-20   LLW     Created
int UMODEM_WriteHROVStateXYMsg(char* msg, int max_msg_len, int src, int dest,
                               int ack, umodem_hrov_statexy_t* hrovdata);

/// Function to convert NMEA $CARXD message to binary data structure
/// 200-07-20   LLW     Created
int UMODEM_ReadHROVStateXYMsg(const char* msg, int msg_len, double time_stamp,
                              umodem_hrov_statexy_t* hrovdata);

/// Function to convert data structure to hex encoded 128 byte hex
/// encoded ascii string.
///
/// 2008-07-20   LLW     Created
int UMODEM_Encode_HROVStateXYData(umodem_hrov_statexy_t* hrovdata,
                                  char* hex_str, int max_hex_str_len);

/// Function to convert  hex encoded 128 byte hex encoded ascii string  to data
/// structure
/// 2008-07-20   LLW     Created
int UMODEM_Decode_HROVStateXYData(umodem_hrov_statexy_t* hrovdata,
                                  const char* hex_str, int max_hex_str_len);

// 2009/10/19 LLW Jasontalk Ascii
typedef struct
{
  unsigned int tos;           //  6 bits integer second 0-59, encoded as 8 bits
  unsigned int sync_nav_mode; //  2 bits (0, 1, 2, 3) only, encoded as two bits
  //  1 total byte

  // null terminated string
  // 31 total bytes = 30 byte string + 1 NULL terminator
  // only the first 30 bytes are transmitted by the modem
  char jt_str[MAX_UMODEM_JASONTALK_ASCII_STRING_LENGTH + 128];

  /* packet header information for the NMEA modem string */
  int source;
  int dest;
  int ack_requested;
  int frame_number;

  /* additional info */
  double time_stamp_received; /* time of last successful decoded rx */
  unsigned rx_packet_count;   /* total number of packets decided rx */

} umodem_jasontalk_ascii_t;

/// Function to convert data structure to hex encoded 128 byte hex encoded ascii
/// string.
/// 2009-10-19   LLW     Created
int UMODEM_Encode_HROV_JasonTalk_ASCII_Msg(umodem_jasontalk_ascii_t* hrovdata,
                                           char* hex_str, int max_hex_str_len);

/// Function to convert hex encoded 128 byte hex encoded ascii string to
/// data structure
///
/// 2009-10-19   LLW     Created
int UMODEM_Decode_HROV_JasonTalk_ASCII_Msg(umodem_jasontalk_ascii_t* hrovdata,
                                           const char* hex_str,
                                           int max_hex_str_len);

/// Function to convert binary data structure to NMEA $CATXD message
///
/// 2009-10-19   LLW     Created
int UMODEM_WriteHROV_JasonTalk_ASCII_Msg(char* msg, int max_msg_len, int src,
                                         int dest, int ack,
                                         umodem_jasontalk_ascii_t* hrovdata);

/// Function to convert NMEA $CARXD message to binary data structure
/// 2009-10-19   LLW     Created
int UMODEM_ReadHROV_JasonTalk_ASCII_Msg(const char* msg, int msg_len,
                                        double time_stamp,
                                        umodem_jasontalk_ascii_t* hrovdata);
}
}
}
}
#endif // PROJECT_LEGACY_MICROMODEM_SERIALIZATION_H
