/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "SentryAcommsLog.h"
#include "ui_SentryAcommsLog.h"

namespace navg {
namespace plugins {
namespace sentry_acomms {

SentryAcommsLog::SentryAcommsLog(int modem, QWidget* parent)
  : QDialog(parent)
  , ui(std::make_unique<Ui::SentryAcommsLog>())
  , m_modem(modem)
{
  ui->setupUi(this);
  connect(ui->clearButton, &QPushButton::clicked, ui->log,
          &QPlainTextEdit::clear);
  connect(ui->sendButton, &QPushButton::clicked, [this]() {
    const auto bytes = ui->outgoingMessage->text().toUtf8();
    if (bytes.isEmpty()) {
      return;
    }
    emit sendMessage(bytes);
  });
}

SentryAcommsLog::~SentryAcommsLog() = default;

void
SentryAcommsLog::setModem(int modem)
{
  m_modem = modem;
}

void
SentryAcommsLog::setPlaceholderText(const QString& text)
{
  ui->log->setPlaceholderText(text);
}
void
SentryAcommsLog::logReceivedMessage(int modem, const QByteArray& bytes)
{
  if (modem != m_modem) {
    return;
  }
  ui->log->appendPlainText(
    "RX: " + QString(bytes).replace("\n", "\\n").replace("\r", "\\r"));
}

void
SentryAcommsLog::logTransmittedMessage(int modem, const QByteArray& bytes)
{
  if (modem != m_modem) {
    return;
  }

  ui->log->appendPlainText("TX: " + QString(bytes).replace("\n", "\\n"));
}

} // namespace navest
} // namespace plugins
} // namespace navg
