/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#include "SentryAcomms.h"
#include "SentryAcommsLog.h"
#include "SentryAcommsSettings.h"
#include "Serialization.h"

#include "SentryLatestStatus.h"
#include <QAction>
#include <QByteArray>
#include <QGridLayout>
#include <QMap>
#include <QRegularExpression>
#include <QSettings>
#include <QSignalMapper>
#include <QUdpSocket>

namespace navg {
namespace plugins {
namespace sentry_acomms {

Q_LOGGING_CATEGORY(plugin_sentry_acomms, "navg.plugins.sentry_acomms")

SentryAcomms::SentryAcomms(QWidget* parent)
  : QWidget(parent)
  , m_ready_read(new QSignalMapper(this))
  , m_action_show_settings(new QAction("&Settings", this))
  , m_action_show_avtrak_log(new QAction("&AvTrak Log", this))
  , m_action_show_micromodem_log(new QAction("&µModem Log", this))
  , m_action_show_waveglider_log(new QAction("Waveglider Log", this))
  //  , m_action_show_latest_status(new QAction("&Status Display", this))
  , m_log_avtrak(new SentryAcommsLog(Modem::AvTrak))
  , m_log_micromodem(new SentryAcommsLog(Modem::MicroModem))
  , m_log_waveglider(new SentryAcommsLog(Modem::Waveglider))
  , m_latest_status(new SentryLatestStatus(this))
{
  auto grid = new QGridLayout(this);
  grid->addWidget(m_latest_status, 0, 0);

  connect(m_ready_read,
          static_cast<void (QSignalMapper::*)(int)>(&QSignalMapper::mapped),
          this, &SentryAcomms::handleIncomingMessage);

  connect(m_action_show_settings, &QAction::triggered, this,
          &SentryAcomms::showSettingsDialog);

  connect(m_action_show_avtrak_log, &QAction::triggered, m_log_avtrak,
          &QWidget::show);

  connect(m_action_show_micromodem_log, &QAction::triggered, m_log_micromodem,
          &QWidget::show);

  connect(m_action_show_waveglider_log, &QAction::triggered, m_log_waveglider,
          &QWidget::show);

  //  connect(m_action_show_latest_status, &QAction::triggered, m_latest_status,
  //          &QWidget::show);

  m_log_avtrak->setWindowTitle("AvTrak Message Log");
  m_log_avtrak->setPlaceholderText("AvTrak Message Log");
  connect(
    this,
    static_cast<void (SentryAcomms::*)(SentryAcomms::Modem, const QByteArray&)>(
      &SentryAcomms::messageReceived),
    m_log_avtrak, &SentryAcommsLog::logReceivedMessage);

  m_log_micromodem->setWindowTitle("µModem Message Log");
  m_log_micromodem->setPlaceholderText("µModem Message Log");

  connect(
    this,
    static_cast<void (SentryAcomms::*)(SentryAcomms::Modem, const QByteArray&)>(
      &SentryAcomms::messageReceived),
    m_log_micromodem, &SentryAcommsLog::logReceivedMessage);

  m_log_waveglider->setWindowTitle("Waveglider Message Log");
  m_log_waveglider->setPlaceholderText("Waveglider Message Log");
  connect(
    this,
    static_cast<void (SentryAcomms::*)(SentryAcomms::Modem, const QByteArray&)>(
      &SentryAcomms::messageReceived),
    m_log_waveglider, &SentryAcommsLog::logReceivedMessage);

  m_latest_status->setWindowTitle("Sentry Status");
  connect(this, static_cast<void (SentryAcomms::*)(SentryAcomms::Modem,
                                                   const QVariantHash)>(
                  &SentryAcomms::messageReceived),
          m_latest_status, &SentryLatestStatus::receiveMessage);
  connect(this, static_cast<void (SentryAcomms::*)(SentryAcomms::Modem,
                                                   const QVariantHash)>(
                  &SentryAcomms::messageReceived),
          this, &SentryAcomms::updateNavestHeading);
}

SentryAcomms::~SentryAcomms()
{
  qDeleteAll(m_udp);
  m_log_avtrak->deleteLater();
  m_log_micromodem->deleteLater();
  m_log_waveglider->deleteLater();
}

int
SentryAcomms::listenPort(SentryAcomms::Modem modem) const
{

  const auto udp = m_udp.value(modem, nullptr);
  if (!udp) {
    return -1;
  }

  return udp->localPort();
}

int
SentryAcomms::remotePort(SentryAcomms::Modem modem) const
{
  return m_remote_port.value(modem, -1);
}

QHostAddress
SentryAcomms::remoteAddress(SentryAcomms::Modem modem) const
{
  return m_remote_address.value(modem, {});
}

void
SentryAcomms::setListenPort(SentryAcomms::Modem modem, int port)
{
  auto udp = m_udp.value(modem, nullptr);
  if (!udp) {
    udp = new QUdpSocket;
    connect(udp, SIGNAL(readyRead()), m_ready_read, SLOT(map()));
    m_ready_read->setMapping(udp, modem);
    m_udp[modem] = udp;
  }

  if (udp->localPort() == port || port < 0) {
    return;
  }

  udp->close();
  if (!udp->bind(QHostAddress{ QHostAddress::Any }, port,
                 QAbstractSocket::ShareAddress |
                   QAbstractSocket::ReuseAddressHint)) {
    qCCritical(plugin_sentry_acomms, "Unable to bind local port: %d", port);
  }

  qCInfo(plugin_sentry_acomms)
    << "Modem:" << modem << "listening on:" << udp->localAddress().toString()
    << ":" << port;
  emit listenPortChanged(modem, port);
}

void
SentryAcomms::setRemotePort(SentryAcomms::Modem modem, int port)
{
  if (port == m_remote_port.value(modem, -1) || port < 0) {
    return;
  }

  m_remote_port[modem] = port;
  emit remotePortChanged(modem, port);
}
void
SentryAcomms::setRemoteAddress(SentryAcomms::Modem modem,
                               const QHostAddress& address)
{
  if (address == m_remote_address.value(modem, {}) || address.isNull()) {
    return;
  }

  m_remote_address[modem] = address;
  emit remoteAddressChanged(modem, address);
}

void
SentryAcomms::setRemoteAddress(SentryAcomms::Modem modem,
                               const QString& address)
{
  return setRemoteAddress(modem, QHostAddress{ address });
}

void
SentryAcomms::sendMessage(SentryAcomms::Modem modem, const QByteArray& message)
{
  auto udp = m_udp.value(modem, nullptr);

  if (!udp) {
    return;
  }

  const auto& host = m_remote_address.value(modem, {});
  const auto port = m_remote_port.value(modem, -1);

  if (host.isNull()) {
    qCWarning(plugin_sentry_acomms,
              "Cannot send message for modem %d, remote address is invalid",
              modem);
    return;
  }

  if (port < 0) {
    qCWarning(plugin_sentry_acomms,
              "Cannot send message for modem %d, remote port is invalid",
              modem);
  }

  udp->writeDatagram(message, host, port);
  emit messageSent(modem, message);
}

QVariantHash
SentryAcomms::handleAvTrakMessage(QString& buf)
{

  const auto pos = std::find(std::begin(buf), std::end(buf), QChar('|'));
  if (pos == std::end(buf)) {
    return {};
  }

  const auto size = buf.size();

  const auto n_skip = std::distance(std::begin(buf), pos) + 1;
  if (n_skip >= size) {
    return {};
  }

  const auto sms_data = buf.mid(n_skip, size - n_skip);

  // qCDebug(plugin_sentry_acomms) << "SMS data:" << sms_data;
  return deserialize(sms_data);
}

QVariantHash
SentryAcomms::handleWavegliderMessage(QString& buf)
{

  return handleMicroModemMessage(buf);
}

QVariantHash
SentryAcomms::handleMicroModemMessage(QString& buf)
{

  static QVector<QString> frames;
  static bool rx_done;

  const auto start = buf.indexOf('$');
  if (start < 0) {
    return {};
  }

  auto message = buf.mid(start);

  if (message.startsWith("$CACYC")) {
    if (!rx_done) {
      qCWarning(plugin_sentry_acomms) << "Unexpected restart of transmit "
                                         "cycle; did not finish last reception";
    }

    rx_done = false;
    auto regex = QRegularExpression{
      "\\$CACYC,(\\d+),(\\d+),(\\d+),(\\d+),(\\d+),(\\d+)\\*(\\d+)"
    };
    const auto match = regex.match(message);
    if (!match.hasMatch()) {
      qCWarning(plugin_sentry_acomms) << "Invalid cycle init message:"
                                      << message.trimmed();
      rx_done = true;
      return {};
    }

    const auto num_frames = match.captured(6).toUInt();
    frames.clear();
    frames.resize(num_frames);
    qCDebug(plugin_sentry_acomms) << "Rx cycle started with" << num_frames
                                  << "frames:" << message.trimmed();
    return {};
  }

  if (message.startsWith("$CARXD")) {
    if (rx_done) {
      qCWarning(plugin_sentry_acomms,
                "Unexpected frame reception without preceding cycle init");
      return {};
    }

    auto regex = QRegularExpression{
      "\\$CARXD,(\\d+),(\\d+),(\\d+),(\\d+),([0-9a-fA-F]+)?\\*(\\d+)"
    };
    const auto match = regex.match(message);

    if (!match.hasMatch()) {
      qCWarning(plugin_sentry_acomms) << "Invalid frame reception message:"
                                      << message.trimmed();
      return {};
    }

    const auto frame_number = match.captured(4).toUInt() - 1;
    if (frame_number >= static_cast<unsigned int>(frames.size())) {
      qCWarning(plugin_sentry_acomms)
        << "Invalid data rx message, frame number too large. (" << frame_number
        << ">=" << frames.size() << message.trimmed();
      return {};
    }

    frames[frame_number] = match.captured(5);
    if (frame_number + 1 != static_cast<unsigned int>(frames.size())) {
      return {};
    }

    // Join frames frames.
    auto hex_bytes = QString{};
    for (const auto& f : frames) {
      hex_bytes += f;
    }

    const auto result = deserializeMicroModem(hex_bytes);
    qCDebug(plugin_sentry_acomms) << result;
    rx_done = false;
    return result;
  }

  return {};
}

void
SentryAcomms::handleIncomingMessage(int modem)
{
  auto udp = m_udp.value(static_cast<Modem>(modem), nullptr);
  if (!udp) {
    qCCritical(plugin_sentry_acomms)
      << "Invalid UDP port called for modem" << modem
      << "within message handler.  THIS IS A BUG.";
    return;
  }

  static QByteArray buffer;

  while (udp->hasPendingDatagrams()) {
    const auto num_bytes = udp->pendingDatagramSize();
    if (buffer.size() < num_bytes) {
      buffer.resize(num_bytes);
    }

    udp->readDatagram(buffer.data(), num_bytes);

    // qCDebug(plugin_sentry_acomms) << "Received:" << buffer.left(num_bytes);
    auto buf_string = QString{ buffer.left(num_bytes) };

    auto result = QVariantHash{};

    switch (modem) {
      case Modem::AvTrak:
        result = handleAvTrakMessage(buf_string);
        break;
      case Modem::MicroModem:
        result = handleMicroModemMessage(buf_string);
        break;
      case Modem::Waveglider:
        result = handleWavegliderMessage(buf_string);
        break;
      default:
        qCCritical(plugin_sentry_acomms, "Invalid modem id: %d.  THIS IS A BUG",
                   modem);
        return;
    }

    emit messageReceived(static_cast<Modem>(modem), buffer.left(num_bytes));

    if (result.isEmpty()) {
      return;
    }

    emit messageReceived(static_cast<Modem>(modem), result);

    if (result["type"] == MessageType::DataQueue &&
        result["queue"] == QueueType::State) {
      auto ok = true;
      const auto trackline = result["trackline"].toUInt(&ok);
      if (ok) {
        setCurrentTrackline(trackline);
      }
      const auto is_aborting = result["abort"].toUInt(&ok);
      if (ok) {
        emit abortStatusUpdated(is_aborting);
      }
      bool ok_hdg, ok_depth, ok_alt;
      const auto hdg = result["heading"].toInt(&ok_hdg);
      const auto depth = result["depth"].toInt(&ok_depth);
      const auto alt = result["alt"].toInt(&ok_alt);
      if (ok_hdg && ok_depth && ok_alt) {
        emit hdgDepthAltUpdated(hdg, depth, alt);
      }
      bool ok_hvel, ok_vvel;
      const auto h_vel = result["h_vel"].toFloat(&ok_hvel);
      const auto v_vel = result["v_vel"].toFloat(&ok_vvel);
      if (ok_hvel && ok_vvel) {
        emit vehicleVelocityUpdated(h_vel, v_vel);
      }
      // The Science Data plugin will work better if all of the
      // relevant SDQ0 fields are represented in one callback.
      // While we're at it, I don't know why hdg/depth/alt are ints
      // in the hdgDepthAltUpdated() SLOT.
      const auto float_hdg = result["heading"].toFloat(&ok_hdg);
      const auto float_depth = result["depth"].toFloat(&ok_depth);
      const auto float_alt = result["alt"].toInt(&ok_alt);
      if (ok_hdg && ok_depth && ok_alt && ok_hvel && ok_vvel) {
        emit vehicleStateUpdated(float_hdg, float_depth, float_alt, h_vel,
                                 v_vel);
      }

    } else if (result["type"] == MessageType::DataQueue &&
               result["queue"] == QueueType::ScalarScience) {
      bool ok_oxy, ok_obs, ok_orp, ok_temperature, ok_salinity, ok_depth;
      const auto oxygen_concentration =
        result["oxygen_concentration"].toFloat(&ok_oxy);
      const auto obs_raw = result["obs_raw"].toFloat(&ok_obs);
      const auto orp_raw = result["orp_raw"].toFloat(&ok_orp);
      const auto ctd_temperature =
        result["ctd_temperature"].toFloat(&ok_temperature);
      const auto ctd_salinity = result["ctd_salinity"].toFloat(&ok_salinity);
      const auto paro_depth = result["paro_depth"].toFloat(&ok_depth);
      if (ok_oxy && ok_obs && ok_orp && ok_temperature && ok_salinity &&
          ok_depth) {
        emit scalarScienceUpdated(oxygen_concentration, obs_raw, orp_raw,
                                  ctd_temperature, ctd_salinity, paro_depth);
      }
    }
  }
}
unsigned int
SentryAcomms::currentTrackline() const
{
  return m_current_trackline;
}

void
SentryAcomms::setCurrentTrackline(unsigned int trackline)
{
  if (trackline == m_current_trackline) {
    return;
  }

  m_current_trackline = trackline;
  emit currentTracklineChanged(trackline);
}

void
SentryAcomms::loadSettings(QSettings* settings)
{
  if (settings == nullptr) {
    return;
  }

  auto ok = bool{ false };
  auto port = settings->value("sonardyne_pan_listen_port").toInt(&ok);
  if (ok) {
    setListenPort(Modem::AvTrak, port);
  } else {
    qCWarning(plugin_sentry_acomms)
      << "Unable to load 'sonardyne_pan_listen_port' from settings";
  }

  port = settings->value("sonardyne_pan_remote_port").toInt(&ok);
  if (ok) {
    setRemotePort(Modem::AvTrak, port);
  } else {
    qCWarning(plugin_sentry_acomms)
      << "Unable to load 'sonardyne_pan_remote_port' from settings";
    setRemotePort(Modem::AvTrak, 0);
  }

  auto remote_addr =
    settings->value("sonardyne_pan_remote_address", "127.0.0.1").toString();
  setRemoteAddress(Modem::AvTrak, remote_addr);

  port = settings->value("micromodem_listen_port").toInt(&ok);
  if (ok) {
    setListenPort(Modem::MicroModem, port);
  } else {
    qCWarning(plugin_sentry_acomms)
      << "Unable to load 'micromodem_listen_port' from settings";
    setRemotePort(Modem::MicroModem, 0);
  }

  port = settings->value("micromodem_remote_port").toInt(&ok);
  if (ok) {
    setRemotePort(Modem::MicroModem, port);
  } else {
    qCWarning(plugin_sentry_acomms)
      << "Unable to load 'micromodem_remote_port' from settings";
  }

  remote_addr =
    settings->value("micromodem_remote_address", "127.0.0.1").toString();
  setRemoteAddress(Modem::MicroModem, remote_addr);

  ///
  port = settings->value("waveglider_listen_port").toInt(&ok);
  if (ok) {
    setListenPort(Modem::Waveglider, port);
  } else {
    qCWarning(plugin_sentry_acomms)
      << "Unable to load 'waveglider_listen_port' from settings";
    setRemotePort(Modem::Waveglider, 0);
  }

  port = settings->value("waveglider_remote_port").toInt(&ok);
  if (ok) {
    setRemotePort(Modem::Waveglider, port);
  } else {
    qCWarning(plugin_sentry_acomms)
      << "Unable to load 'waveglider_remote_port' from settings";
  }

  remote_addr =
    settings->value("waveglider_remote_address", "127.0.0.1").toString();
  setRemoteAddress(Modem::Waveglider, remote_addr);

  bool temp = settings->value("use_acomm_heading").toBool();
  port = settings->value("navest_vehicle_id").toInt(&ok);
  if (ok) {
    setAcommHeading(temp,port);
  } else {
    qCWarning(plugin_sentry_acomms)
      << "Unable to load 'navest_vehicle_id' from settings";
  }
  
}

void
SentryAcomms::saveSettings(QSettings& settings)
{
  auto port = -1;
  if ((port = listenPort(Modem::AvTrak)) >= 0) {
    settings.setValue("sonardyne_pan_listen_port", port);
  }

  if ((port = remotePort(Modem::AvTrak)) >= 0) {
    settings.setValue("sonardyne_pan_remote_port", port);
  }

  settings.setValue("sonardyne_pan_remote_address",
                    remoteAddress(Modem::AvTrak).toString());

  if ((port = listenPort(Modem::MicroModem)) >= 0) {
    settings.setValue("micromodem_listen_port", port);
  }

  if ((port = remotePort(Modem::MicroModem)) >= 0) {
    settings.setValue("micromodem_remote_port", port);
  }

  settings.setValue("micromodem_remote_address",
                    remoteAddress(Modem::MicroModem).toString());

  //
  if ((port = listenPort(Modem::Waveglider)) >= 0) {
    settings.setValue("waveglider_listen_port", port);
  }

  if ((port = remotePort(Modem::Waveglider)) >= 0) {
    settings.setValue("waveglider_remote_port", port);
  }

  settings.setValue("waveglider_remote_address",
                    remoteAddress(Modem::Waveglider).toString());

  settings.setValue("use_acomm_heading", use_acomm_heading);

  settings.setValue("navest_vehicle_id", navest_vehicle_id);
}

void
SentryAcomms::connectPlugin(const QObject* plugin , const QJsonObject& metadata)
{
  if (!plugin) {
    return;
  }

  const auto name =
    metadata.value("MetaData").toObject().value("name").toString();
  if (name.isEmpty()) {
    return;
  }

  if (name == "navest") {
    if (!connect(this,
                 SIGNAL(headingToggleChanged(bool, int)),
                 plugin,
                 SLOT(setHeadingMode(bool, int)))) {
      qCCritical(plugin_sentry_acomms, "failed to connect to navest plugin navest");
    }
    else {
      setAcommHeading(use_acomm_heading, navest_vehicle_id, true);
    }
  }
}

void SentryAcomms::setMapView(QPointer<dslmap::MapView> view)
{
  m_view = view;
}
QList<QAction*>
SentryAcomms::pluginMenuActions()
{
  return { m_action_show_settings,
           m_action_show_avtrak_log,
           m_action_show_micromodem_log,
           m_action_show_waveglider_log,
           m_action_show_latest_status };
}

void
SentryAcomms::showSettingsDialog()
{
  QScopedPointer<SentryAcommsSettings> dialog(new SentryAcommsSettings);
  dialog->setListenPort(Modem::AvTrak, listenPort(Modem::AvTrak));
  dialog->setRemotePort(Modem::AvTrak, remotePort(Modem::AvTrak));
  dialog->setRemoteAddress(Modem::AvTrak, remoteAddress(Modem::AvTrak));

  dialog->setListenPort(Modem::MicroModem, listenPort(Modem::MicroModem));
  dialog->setRemotePort(Modem::MicroModem, remotePort(Modem::MicroModem));
  dialog->setRemoteAddress(Modem::MicroModem, remoteAddress(Modem::MicroModem));

  dialog->setListenPort(Modem::Waveglider, listenPort(Modem::Waveglider));
  dialog->setRemotePort(Modem::Waveglider, remotePort(Modem::Waveglider));
  dialog->setRemoteAddress(Modem::Waveglider, remoteAddress(Modem::Waveglider));

  // navest settings
  dialog->setNavestHeadingToggle(use_acomm_heading);
  dialog->setVehicleId(navest_vehicle_id);

  dialog->setModal(true);
  if (!dialog->exec()) {
    return;
  }

  setAcommHeading(dialog->navestHeadingToggle(), dialog->vehicleId());

  setRemoteAddress(Modem::AvTrak, dialog->remoteAddress(Modem::AvTrak));
  setRemotePort(Modem::AvTrak, dialog->remotePort(Modem::AvTrak));
  setListenPort(Modem::AvTrak, dialog->listenPort(Modem::AvTrak));

  setRemoteAddress(Modem::MicroModem, dialog->remoteAddress(Modem::MicroModem));
  setRemotePort(Modem::MicroModem, dialog->remotePort(Modem::MicroModem));
  setListenPort(Modem::MicroModem, dialog->listenPort(Modem::MicroModem));

  setRemoteAddress(Modem::Waveglider, dialog->remoteAddress(Modem::Waveglider));
  setRemotePort(Modem::Waveglider, dialog->remotePort(Modem::Waveglider));
  setListenPort(Modem::Waveglider, dialog->listenPort(Modem::Waveglider));
}

void
SentryAcomms::setAcommHeading(bool heading_toggle, int id, bool force){

// if input is different from internal heading OR new vehicle id OR Force set(force navest signal to be emitted no matter state)
  if (use_acomm_heading != heading_toggle || navest_vehicle_id != id || force){
    use_acomm_heading = heading_toggle;
    navest_vehicle_id = id;
    emit headingToggleChanged(heading_toggle, id);
  }
  use_acomm_heading = heading_toggle;
  navest_vehicle_id = id;
}

void SentryAcomms::updateNavestHeading(int modem, const QVariantHash message)
{

  if (!use_acomm_heading) {
    return;
  }
  if (!m_view)
    return;

  auto type = static_cast<MessageType>(message["type"].toInt());
  auto queue = static_cast<QueueType>(message["queue"].toInt());
  if (!type == MessageType::DataQueue || !queue == QueueType::State) {
    return;
  }

  double heading = message["heading"].toDouble();

  auto scene = m_view->mapScene();

  if (!scene) {
    qCInfo(plugin_sentry_acomms) << "no scene";
    return;
  }

  if (!vehicle_layer) {
    // find the correct vehicle layer on the map
    using LayerHash = QHash<QUuid, dslmap::MapLayer*>;
    LayerHash nav_solution_layers =
      scene->layers(dslmap::MapItemType::NavestLayerType);
    if (nav_solution_layers.empty()) {
      qCInfo(plugin_sentry_acomms) << "layer list is empty";
      return;
    }
    QHash<QUuid, dslmap::MapLayer*>::iterator i;
    for (i = nav_solution_layers.begin(); i != nav_solution_layers.end(); ++i) {
      navg::plugins::navest::NavestLayer* nav_layer =
        dynamic_cast<navg::plugins::navest::NavestLayer*>(i.value());
      if (!nav_layer) {
        qCInfo(plugin_sentry_acomms) << "no navestlayer found";
        continue;
      }
      if (nav_layer->navestBeaconId() == navest_vehicle_id) {
        vehicle_layer = nav_layer;
        break;
      }
    }
  }

  if (vehicle_layer) {
    // if the layer is set to acomms heading mode...update the heading
    if (vehicle_layer->acommsHeading()){
      vehicle_layer->updateHeading(heading);
    }
  }

  // set heading
}
}
}
}
