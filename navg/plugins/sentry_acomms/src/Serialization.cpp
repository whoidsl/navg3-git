/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#include "Serialization.h"
#include "legacy_micromodem_serialization.h"

#include <QDebug>
#include <QRegularExpression>
#include <QVector>

#include <algorithm>
#include <stdio.h>

namespace navg {
namespace plugins {
namespace sentry_acomms {

MessageType
messageType(const QString& buf)
{
  if (buf.startsWith("SDQ")) {
    return MessageType::DataQueue;
  }

  if (buf.startsWith("SLR")) {
    return MessageType::LastReceived;
  }

  return MessageType::UserDefinedMessage;
}

static const auto SDQ_REGEX = QRegularExpression{ "SDQ (\\d+):(.*)" };
static const auto SLR_REGEX = QRegularExpression{ "SLR (\\d+),(\\d+):(.*)" };

QVariantHash
parseStateSms(const QString& buf)
{

  float x, y, depth, altitude, heading, h_vel, v_vel, battery, goal_x, goal_y,
    goal_z;
  unsigned char abort;
  unsigned short trackline;

  const auto std_str = buf.toStdString();
  const auto num_parsed =
    sscanf(std_str.data(), "%f,%f,%f,%f,%f,%f,%f,A%hhu,%*f,%f,%f,%f,%f,%*f,%*f,"
                           "%*f,%*f,%*f,%*f,%*f,%hu",
           &x, &y, &depth, &altitude, &heading, &h_vel, &v_vel, &abort,
           &battery, &goal_x, &goal_y, &goal_z, &trackline);

  if (num_parsed != 13) {
    return {};
  }

  return { { "x", x },
           { "y", y },
           { "depth", depth },
           { "alt", altitude },
           { "heading", heading },
           { "h_vel", h_vel },
           { "v_vel", v_vel },
           { "abort", abort },
           { "battery", battery },
           { "goal_x", goal_x },
           { "goal_y", goal_y },
           { "goal_z", goal_z },
           { "trackline", trackline } };
}

// QUESTION(LEL): Was it intentional that parseLastReceived and parseDataQueue
//  aren't declared in the header file?
QVariantHash
parseScalarScience(const QString& buf)
{
  float oxygen_concentration, obs_raw, orp_raw, ctd_temperature, ctd_salinity;
  float paro_depth;

  const auto std_str = buf.toStdString();
  const int num_parsed =
    sscanf(std_str.data(), "%f %f %f %f %f %f", &oxygen_concentration, &obs_raw,
           &orp_raw, &ctd_temperature, &ctd_salinity, &paro_depth);
  if (num_parsed != 6) {
    return {};
  }
  return { { "oxygen_concentration", oxygen_concentration },
           { "obs_raw", obs_raw },
           { "orp_raw", orp_raw },
           { "ctd_temperature", ctd_temperature },
           { "ctd_salinity", ctd_salinity },
           { "paro_depth", paro_depth } };
}

QVariantHash
parseDataQueue(const QString& buf)
{
  auto match = SDQ_REGEX.match(buf);
  if (!match.hasMatch()) {
    return {};
  }

  bool ok = true;

  const auto queue_num = match.captured(1).toInt(&ok);
  const auto data = match.captured(2);

  if (!ok) {
    return {};
  }

  auto result = QVariantHash{};
  auto queue = static_cast<QueueType>(queue_num);
  switch (queue) {
    case State:
      result = parseStateSms(data);
      break;
    case UserDefinedQueue:
      result["data"] = data;
      break;
    case ScalarScience:
      result = parseScalarScience(data);
    case Htp:
    case Shift:
    case Actuators:
    case BottomFollower:
    case GoalLeg:
    case Power:
    case Multibeam:
    case Blueview:
    case Camera:
      break;
    default:
      return {};
  }

  if (result.isEmpty()) {
    return result;
  }

  result["type"] = MessageType::DataQueue;
  result["queue"] = queue;

  return result;
}

QVariantHash
parseLastReceived(const QString& buf)
{
  Q_UNUSED(buf)
  return {};
}

QVariantHash
deserialize(const QString& buf)
{

  const auto buf_string = QString{ buf };

  switch (messageType(buf)) {
    case DataQueue:
      return parseDataQueue(buf_string);
    case LastReceived:
      return parseLastReceived(buf_string);
    case UserDefinedMessage:
    default:
      return { { "type", MessageType::UserDefinedMessage }, { "data", buf } };
  }
}

bool
hexstr_to_bytes(const char* hexstr, size_t hexstr_len, QVector<char>& bytes)
{
  if (hexstr_len % 2) {
    qWarning("Expected an even number of hex characters, received %lu",
             hexstr_len);
    return false;
  }

  bytes.resize(hexstr_len / 2);

  std::for_each(bytes.begin(), bytes.begin(), [&](char& value) {
    sscanf(hexstr, "%02hhX", &value);
    hexstr += 2;
  });
  return true;
}

QVariantHash
deserializeMicroModem(const QString& buf)
{
  auto bytes = QVector<char>{};
  bytes.resize(buf.size());
  auto cstring = buf.toStdString();
  std::copy(cstring.begin(), cstring.end(), bytes.begin());

  // if (!hexstr_to_bytes(cstring.data(), buf.size(), bytes)) {
  //   return {};
  // }

  const int message_type = buf.left(2).toUInt(nullptr, 16);

  auto msg = QVariantHash{};
  qDebug() << "Message type is " << message_type;
  switch (message_type) {
    case (legacy_micromodem::MessageType::UMODEMHROVSTATEXY): {
      legacy_micromodem::umodem_hrov_statexy_t data;
      if (legacy_micromodem::UMODEM_Decode_HROVStateXYData(&data, bytes.data(),
                                                           bytes.size())) {
        return {};
      }
      legacy_micromodem::hrov_statexy_to_sentry_vehicle_state(data, msg);
      msg["type"] = MessageType::DataQueue;
      msg["queue"] = QueueType::State;
      return msg;
    }
    case (legacy_micromodem::MessageType::UMODEMHROVJASONTALK): {
      legacy_micromodem::umodem_jasontalk_ascii_t data;
      if (legacy_micromodem::UMODEM_Decode_HROV_JasonTalk_ASCII_Msg(
            &data, bytes.data(), bytes.size())) {
        return {};
      }

      return deserialize(QString::fromLocal8Bit(
        reinterpret_cast<char*>(bytes.data()), bytes.size()));
    }
    default:
      return { { "type", MessageType::UserDefinedMessage }, { "data", buf } };
  }
}
}
}
}

// QUESION(LEL): Can this code be removed? It looks like a copy-and-paste from
//  sentry_acoomms's implementation of serialization.

#if 0
namespace sentry_acomms
{
// uint8_t queue_message_type(const std::vector<uint8_t>& buf)
// {
//   ROS_ERROR_STREAM("Queue commands from byte streams are not implemented");
//   return ds_acomms_queue_manager::QueueManagerMessageEnum::INVALID_MESSAGE_TYPE;
// }

uint8_t queue_message_type(const std::string& buf)
{
  // Skip white space
  const auto start = buf.find_first_not_of(' ');

#define ADD_TEST_CASE(STRING, VALUE)                                           \
  if (buf.compare(start, SentryQueueManagerCommandEnum::STRING.size(),         \
                  SentryQueueManagerCommandEnum::STRING) == 0) {               \
    return SentryQueueManagerCommandEnum::VALUE;                               \
  }

  // Mission controller (MCA) passthrough
  ADD_TEST_CASE(MISSION_CONTROLLER_CMD_STR, MISSION_CONTROLLER_COMMAND)

  // Reson daemon (RESONMSG) passthrough
  ADD_TEST_CASE(RESON_CMD_STR, RESON_COMMAND)

  // Blueview daemon (BLV) passthrough
  ADD_TEST_CASE(BLUEVIEW_CMD_STR, BLUEVIEW_COMMAND)

  // Bottom Follower Parameter (WBFP)
  ADD_TEST_CASE(BOTTOM_FOLLOWER_PARAM_CMD_STR, BOTTOM_FOLLOWER_PARAM_COMMAND)

  // Reson command (RESONMSG)
  ADD_TEST_CASE(RESON_CMD_STR, RESON_COMMAND)

  // Bottom Follower Override (BF_OVERRIDE)
  ADD_TEST_CASE(BOTTOM_FOLLOWER_OVERRIDE_CMD_STR, BOTTOM_FOLLOWER_OVERRIDE_COMMAND)

  // Acoustic Joystick Command (AJOY)
  ADD_TEST_CASE(ACOUSTIC_JOYSTICK_CMD_STR, ACOUSTIC_JOYSTICK_COMMAND)

  // Thrusters Enable Command (THRUSTERS_ENABLED)
  ADD_TEST_CASE(THRUSTERS_ENABLED_CMD_STR, THRUSTERS_ENABLED_COMMAND)

  // UDP Passthroughs
  ADD_TEST_CASE(UDP_PASSTHROUGH_00_CMD_STR, UDP_PASSTHROUGH_00_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_01_CMD_STR, UDP_PASSTHROUGH_01_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_02_CMD_STR, UDP_PASSTHROUGH_02_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_03_CMD_STR, UDP_PASSTHROUGH_03_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_04_CMD_STR, UDP_PASSTHROUGH_04_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_05_CMD_STR, UDP_PASSTHROUGH_05_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_06_CMD_STR, UDP_PASSTHROUGH_06_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_07_CMD_STR, UDP_PASSTHROUGH_07_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_08_CMD_STR, UDP_PASSTHROUGH_08_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_09_CMD_STR, UDP_PASSTHROUGH_09_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_10_CMD_STR, UDP_PASSTHROUGH_10_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_11_CMD_STR, UDP_PASSTHROUGH_11_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_12_CMD_STR, UDP_PASSTHROUGH_12_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_13_CMD_STR, UDP_PASSTHROUGH_13_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_14_CMD_STR, UDP_PASSTHROUGH_14_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_15_CMD_STR, UDP_PASSTHROUGH_15_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_16_CMD_STR, UDP_PASSTHROUGH_16_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_17_CMD_STR, UDP_PASSTHROUGH_17_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_18_CMD_STR, UDP_PASSTHROUGH_18_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_19_CMD_STR, UDP_PASSTHROUGH_19_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_20_CMD_STR, UDP_PASSTHROUGH_20_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_21_CMD_STR, UDP_PASSTHROUGH_21_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_22_CMD_STR, UDP_PASSTHROUGH_22_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_23_CMD_STR, UDP_PASSTHROUGH_23_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_24_CMD_STR, UDP_PASSTHROUGH_24_COMMAND)

#undef ADD_TEST_CASE

  return ds_acomms_queue_manager::QueueManagerMessageEnum::INVALID_MESSAGE_TYPE;
}
}  // namespace sentry_acomms

namespace ds_acomms_serialization
{
template <>
void serialize(const sentry_acomms::SentryVehicleState& msg, std::string& buf)
{
  auto ss = std::ostringstream{};
  // First up, position w/ no decimal places
  ss << std::fixed << std::setprecision(0);
  ss << msg.pos_ned.x << "," << msg.pos_ned.y << "," << msg.pos_ned.z << ",";
  // Next, altitude and heading w/ 1 decimal point.
  ss << std::setprecision(1);
  ss << msg.altitude << "," << msg.heading << ",";
  // Velocities get 2 decimals
  ss << std::setprecision(2);
  ss << msg.horz_velocity << "," << msg.vert_velocity << ",";
  // A single digit for the abort flag
  ss << "A" << static_cast<int>(msg.abort_status) << ",";
  // We don't have the 'cfg' parameter any more, this is only here for legacy purposes
  ss << "0,";
  // Battery percent
  ss << std::setprecision(1) << msg.battery_pct << ",";
  // Goals, no decimal places
  ss << std::setprecision(0);
  ss << msg.goal_ned.x << "," << msg.goal_ned.y << "," << msg.goal_ned.z << ",";
  // More unused fields now: battery_time, tx_count, rx_count, dvl_status
  ss << "0,0,0,0,";
  // Still more unused fields:  flourometer_volts, optode_volts, eh_volts
  ss << "0,0,0,";
  // Finally, trackline number
  ss << msg.trackline;

  buf.append(ss.str());
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryVehicleState& msg)
{
  auto fields = std::vector<std::string>{};

  const auto num_parsed =
      sscanf(buf.data(), "%f,%f,%f,%f,%f,%f,%f,A%hhu,%*f,%f,%f,%f,%f,%*f,%*f,%*f,%*f,%*f,%*f,%*f,%hu", &msg.pos_ned.x,
             &msg.pos_ned.y, &msg.pos_ned.z, &msg.altitude, &msg.heading, &msg.horz_velocity, &msg.vert_velocity,
             &msg.abort_status, &msg.battery_pct, &msg.goal_ned.x, &msg.goal_ned.y, &msg.goal_ned.z, &msg.trackline);

  return num_parsed == 13;
}

template <>
void serialize(const sentry_acomms::SentryHtp& msg, std::string& buf)
{
  auto ss = std::ostringstream{};
  ss << std::fixed;
  ss << "MAIN:";
  ss << std::setprecision(0) << "H" << msg.main_humidity << ",";
  ss << std::setprecision(1) << "T" << msg.main_temperature << ",P" << msg.main_pressure;

  ss << " BAT:";
  ss << std::setprecision(0) << "H" << msg.battery_humidity << ",";
  ss << std::setprecision(1) << "T" << msg.battery_temperature << ",P" << msg.battery_pressure;

  buf = ss.str();
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryHtp& msg)
{
  auto fields = std::vector<std::string>{};

  const auto num_parsed =
      sscanf(buf.data(), "MAIN:H%f,T%f,P%f BAT:H%f,T%f,P%f", &msg.main_humidity, &msg.main_temperature,
             &msg.main_pressure, &msg.battery_humidity, &msg.battery_temperature, &msg.battery_pressure);

  return num_parsed == 6;
}

template <>
void serialize(const sentry_acomms::MissionControllerCommand& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  const auto start = msg.command.find("MCA");
  if (start == !msg.command.npos)
  {
    os << msg.command.substr(start);
  }
  else
  {
    os << "MCA " << msg.command;
  }

  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::MissionControllerCommand& msg)
{
  // add that extra space at the end of MCA -- needs to be there as the actual command follows.
  const auto start = buf.find("MCA ");
  if (start == buf.npos)
  {
    return false;
  }

  msg.command = buf.substr(start);
  return true;
}

template <>
void serialize(const sentry_acomms::BlueviewCommand& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  const auto start = msg.command.find("BLV");
  if (start == !msg.command.npos)
  {
    os << msg.command.substr(start);
  }
  else
  {
    os << "BLV " << msg.command;
  }

  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::BlueviewCommand& msg)
{
  // add that extra space at the end of BLV -- needs to be there as the actual command follows.
  const auto start = buf.find("BLV ");
  if (start == buf.npos)
  {
    return false;
  }

  msg.command = buf.substr(start);
  return true;
}

template <>
void serialize(const sentry_acomms::ResonCommand& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  const auto start = msg.command.find("RESONMSG");
  if (start == !msg.command.npos)
  {
    os << msg.command.substr(start);
  }
  else
  {
    os << "RESONMSG " << msg.command;
  }

  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::ResonCommand& msg)
{
  // add that extra space at the end of BLV -- needs to be there as the actual command follows.
  const auto start = buf.find("RESONMSG ");
  if (start == buf.npos)
  {
    return false;
  }

  msg.command = buf.substr(start);
  return true;
}

template <>
void serialize(const sentry_acomms::AcousticJoystickCommand& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  os << "AJOY " << msg.timeout.toBoost() << " " << std::fixed << std::setprecision(1)
     << static_cast<int>(msg.allocation_enum) << " " << msg.surge_u << " " << msg.heave_w << " " << msg.torque_w;
  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::AcousticJoystickCommand& msg)
{
  // add that extra space at the end of AJOY -- needs to be there as the actual command follows.
  const auto start = buf.find("AJOY ");
  if (start == buf.npos)
  {
    return false;
  }

  auto is = std::istringstream{ buf.substr(start + 4) };

  boost::posix_time::time_duration duration;
  uint16_t allocation;
  int hold_heading;

  is >> duration >> allocation >> msg.surge_u >> msg.heave_w >> msg.torque_w;

  msg.timeout.fromSec(duration.total_seconds());
  msg.allocation_enum = static_cast<uint8_t>(allocation);

  if (!is)
  {
    return false;
  }

  return true;
}

template <>
void serialize(const sentry_acomms::SentryProsilicaCameraStatus& msg, std::string& buf)
{
  auto os = std::ostringstream{};

  os << std::fixed;
  os << "P" << static_cast<int>(msg.power) << ",A" << static_cast<int>(msg.active) << ",NF" << static_cast<int>(msg.nf)
     << ",FC" << msg.frames << ",E" << msg.exposure << ",G" << static_cast<int>(msg.gain) << ",FR"
     << std::setprecision(2) << msg.frame_rate;
  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryProsilicaCameraStatus& msg)
{
  const auto start = buf.find('P');
  if (start == buf.npos)
  {
    return false;
  }

  const auto num_parsed = std::sscanf(buf.substr(start).data(), "P%hhu,A%hhu,NF%hhu,FC%u,E%u,G%hhu,FR%f", &msg.power,
                                      &msg.active, &msg.nf, &msg.frames, &msg.exposure, &msg.gain, &msg.frame_rate);

  return num_parsed == 7;
}

template <>
void serialize(const sentry_acomms::SentryPowerSwitches& msg, std::string& buf)
{
  auto os = std::ostringstream{};

  os << std::fixed << std::hex << std::uppercase << std::setfill('0');
  os << std::setw(2) << static_cast<int>(msg.power[0]) << "," << std::setw(2) << static_cast<int>(msg.power[1]) << ","
     << std::setw(2) << static_cast<int>(msg.power[2]) << "," << std::setw(2) << static_cast<int>(msg.power[3]) << ","
     << std::setw(2) << static_cast<int>(msg.power[4]) << "," << std::setw(2) << static_cast<int>(msg.power[5]) << ","
     << std::setw(2) << static_cast<int>(msg.power[6]) << "," << std::setw(2) << static_cast<int>(msg.power[7]);
  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryPowerSwitches& msg)
{
  const auto num_parsed =
      std::sscanf(buf.data(), "%hhx,%hhx,%hhx,%hhx,%hhx,%hhx,%hhx,%hhx", &msg.power[0], &msg.power[1], &msg.power[2],
                  &msg.power[3], &msg.power[4], &msg.power[5], &msg.power[6], &msg.power[7]);

  return num_parsed == 8;
}

template <>
void serialize(const sentry_acomms::SentryCurrentShift& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  auto facet = new boost::posix_time::time_facet("%H:%M");
  os.imbue(std::locale(os.getloc(), facet));

  os << msg.stamp.toBoost() << " " << std::fixed << std::setprecision(0) << msg.x << " " << msg.y;

  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryCurrentShift& msg)
{
  auto is = std::istringstream{ buf };

  // Attempt to parse a time -- should be present on all responses.
  auto facet = new boost::posix_time::time_input_facet("%H:%M");
  is.imbue(std::locale(is.getloc(), facet));

  auto time = boost::posix_time::ptime{};
  is >> time;

  if (!is)
  {
    return false;
  }

  // Date parsing.. is complicated..  We only send HH:MM, so need to be mindful of
  // sending things with timestamps in the previous day.

  // This is the current time according to ROS
  auto boost_stamp = ros::Time::now().toBoost();
  // And the current date
  auto boost_date = boost_stamp.date();

  // If the parsed hours are GREATER than the current hours, then we have a situation where
  // we are sending a timestamp from the previous day.  Subtract one day from the current date
  if (time.time_of_day().hours() > boost_stamp.time_of_day().hours())
  {
    boost_date -= boost::gregorian::days(1);
  }

  // Create our timestamp.
  boost_stamp = boost::posix_time::ptime{ boost_date, time.time_of_day() };
  msg.stamp = msg.stamp.fromBoost(boost_stamp);

  is >> msg.x;
  is >> msg.y;

  if (!is)
  {
    return false;
  }

  return true;
}

template <>
void serialize(const ds_control_msgs::ExternalBottomFollowTimedOverride& msg, std::string& buf)
{
  auto os = std::ostringstream{};

  os << std::fixed << std::setprecision(2);
  os << "BF_OVERRIDE " << static_cast<int>(msg.timeout.toSec()) << " "
     << static_cast<int>(msg.override_depth_direction);
  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, ds_control_msgs::ExternalBottomFollowTimedOverride& msg)
{
  auto start_index = buf.find("BF_OVERRIDE ");
  if (start_index == buf.npos)
  {
    return false;
  }

  unsigned int timeout;

  const auto num_scanned =
      sscanf(buf.substr(start_index).data(), "BF_OVERRIDE %u %hhd", &timeout, &msg.override_depth_direction);

  if (num_scanned != 2)
  {
    return false;
  }

  switch (msg.override_depth_direction)
  {
    case ds_control_msgs::ExternalBottomFollowTimedOverride::DRIVE_UP:
    case ds_control_msgs::ExternalBottomFollowTimedOverride::DRIVE_DOWN:
      break;
    default:
      ROS_ERROR_STREAM(
          "Invalid bottom follower override direction: " << static_cast<int>(msg.override_depth_direction));
      return false;
  }

  msg.timeout.fromSec(timeout);
  msg.header.stamp = ros::Time::now();
  return true;
}

template <>
void serialize(const sentry_acomms::BottomFollowerParameterCommand& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  os << std::fixed << std::setprecision(2);
  os << "WBFP " << static_cast<int>(msg.index) << " " << msg.value;
  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::BottomFollowerParameterCommand& msg)
{
  auto start_index = buf.find("WBFP ");
  if (start_index == buf.npos)
  {
    msg.index = msg.INDEX_INVALID;
    return false;
  }

  const auto num_scanned = sscanf(buf.substr(start_index).data(), "WBFP %hhu %f", &msg.index, &msg.value);

  if (msg.index < sentry_acomms::BottomFollowerParameterCommand::INDEX_MIN ||
      msg.index > sentry_acomms::BottomFollowerParameterCommand::INDEX_MAX)
  {
    msg.index = msg.INDEX_INVALID;
    return false;
  }

  return num_scanned == 2;
}

template <>
void serialize(const sentry_acomms::SentryBottomFollowerParameters& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  os << std::fixed << std::setprecision(2);
  os << msg.altitude << " " << msg.envelope << " " << msg.speed << " " << msg.speed_gain << " " << msg.depth_rate << " "
     << msg.depth_acceleration << " " << msg.min_speed << " " << msg.alarm_timeout << " " << msg.depth_floor << " "
     << static_cast<int>(msg.user_override_active) << " " << static_cast<int>(msg.user_override_direction);

  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryBottomFollowerParameters& msg)
{
  const auto num_scanned =
      sscanf(buf.data(), "%f %f %f %f %f %f %f %f %f %hhu %hhd", &msg.altitude, &msg.envelope, &msg.speed,
             &msg.speed_gain, &msg.depth_rate, &msg.depth_acceleration, &msg.min_speed, &msg.alarm_timeout,
             &msg.depth_floor, &msg.user_override_active, &msg.user_override_direction);

  return num_scanned == 11;
}

template <>
void serialize(const sentry_acomms::SentryGoalLegState& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  os << std::fixed << std::setprecision(2);

  os << msg.line_start.x << " " << msg.line_start.y << " " << msg.line_end.x << " " << msg.line_end.y << " "
     << msg.angle_line_segment << " " << msg.off_line_vect << " " << msg.sign_of_vect << " " << msg.kappa << " "
     << msg.old_goal << " " << msg.new_goal << " " << msg.leg_number;

  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryGoalLegState& msg)
{
  const auto num_scanned = sscanf(buf.data(), "%f %f %f %f %f %f %f %f %f %f %lu", &msg.line_start.x, &msg.line_start.y,
                                  &msg.line_end.x, &msg.line_end.y, &msg.angle_line_segment, &msg.off_line_vect,
                                  &msg.sign_of_vect, &msg.kappa, &msg.old_goal, &msg.new_goal, &msg.leg_number);

  return num_scanned == 11;
}

template <>
void serialize(const sentry_acomms::SentryBlueviewStatus& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  os << std::fixed << std::setprecision(0);

  os << msg.time << " SONAR_";

  if (!msg.ok)
  {
    os << "BAD " << msg.ret;
    buf = os.str();
    return;
  }

  os << "OK " << msg.ret << " " << static_cast<int>(msg.enabled) << " " << static_cast<int>(msg.active);
  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryBlueviewStatus& msg)
{
  msg.ok = buf.find("SONAR_OK") != buf.npos;
  if (msg.ok)
  {
    const auto num_scanned =
        sscanf(buf.data(), "%f SONAR_OK %u %hhu %hhu", &msg.time, &msg.ret, &msg.enabled, &msg.active);
    return num_scanned == 4;
  }

  const auto num_scanned = sscanf(buf.data(), "%f SONAR_BAD %u", &msg.time, &msg.ret);
  return num_scanned == 2;
}

template <>
void serialize(const sentry_acomms::SentryControlAndActuatorState& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  os << std::fixed << std::setprecision(0);

  os << static_cast<int>(msg.active_controller) << " " << static_cast<int>(msg.active_allocation) << " " << msg.force_u
     << " " << msg.force_w << " " << msg.torque_q << " "

     << std::setprecision(1) << msg.fwd_servo_deg << " " << msg.aft_servo_deg << " "

     << std::hex << std::uppercase << static_cast<int>(msg.fwd_servo_switch) << " "
     << static_cast<int>(msg.aft_servo_switch) << " "

     << static_cast<int>(msg.fwd_servo_speed) << " " << static_cast<int>(msg.aft_servo_speed) << " "

     << std::dec << msg.fwd_port_thruster_desired_current << " " << msg.fwd_port_thruster_measured_current << " "

     << msg.fwd_stbd_thruster_desired_current << " " << msg.fwd_stbd_thruster_measured_current << " "

     << msg.aft_port_thruster_desired_current << " " << msg.aft_port_thruster_measured_current << " "

     << msg.aft_stbd_thruster_desired_current << " " << msg.aft_stbd_thruster_measured_current;

  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryControlAndActuatorState& msg)
{
  const auto num_scanned =
      sscanf(buf.data(), "%hu %hu %f %f %f %f %f %hhX %hhX %hhX %hhX %f %f %f %f %f %f %f %f", &msg.active_controller,
             &msg.active_allocation, &msg.force_u, &msg.force_w, &msg.torque_q, &msg.fwd_servo_deg, &msg.aft_servo_deg,
             &msg.fwd_servo_switch, &msg.aft_servo_switch, &msg.fwd_servo_speed, &msg.aft_servo_speed,
             &msg.fwd_port_thruster_desired_current, &msg.fwd_port_thruster_measured_current,
             &msg.fwd_stbd_thruster_desired_current, &msg.fwd_stbd_thruster_measured_current,
             &msg.aft_port_thruster_desired_current, &msg.aft_port_thruster_measured_current,
             &msg.aft_stbd_thruster_desired_current, &msg.aft_stbd_thruster_measured_current);

  return num_scanned == 19;
}

template <>
void serialize(const sentry_acomms::SentryResonStatus& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  os << std::fixed << std::setprecision(1) << std::noboolalpha;

  os << "RSTAT "
     << "REC" << static_cast<int>(msg.recording) << ","
     << "S=" << msg.status << ","
     << "CMD=" << msg.mode << ","
     << "0"
     << ","
     << "#" << msg.ping_number << ","
     << "AP" << msg.auto_pilot << ","
     << "GAIN" << msg.gain << ","
     << "RNG" << msg.range << ","
     << "PWR" << msg.power << ","
     << "CA" << msg.coverage_angle_deg;
  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryResonStatus& msg)
{
  const auto start_index = buf.find("RSTAT");
  if (start_index == buf.npos)
  {
    return false;
  }

  auto status = std::array<char, 64>{};
  status.fill('\0');

  const auto num_scanned =
      sscanf(buf.substr(start_index).data(), "RSTAT REC%hhu,S=%63[^,],CMD=%hd,%*d,#%u,AP%hd,GAIN%f,RNG%f,PWR%f,CA%f",
             &msg.recording, status.data(), &msg.mode, &msg.ping_number, &msg.auto_pilot, &msg.gain, &msg.range,
             &msg.power, &msg.coverage_angle_deg);

  msg.status = std::string{ status.data() };

  if (num_scanned != 9)
  {
    return false;
  }

  return true;
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::ThrustersEnabledCommand& msg)
{
  auto start_index = buf.find("THRUSTERS_ENABLED ");
  if (start_index == buf.npos)
  {
    return false;
  }

  int value;

  const auto num_scanned = sscanf(buf.substr(start_index).data(), "THRUSTERS_ENABLED %d", &value);
  msg.enabled = value > 0;
  return num_scanned == 1;
}

template <>
void serialize(const sentry_acomms::ThrustersEnabledCommand& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  os << "THRUSTERS_ENABLED " << static_cast<int>(msg.enabled);
  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::UdpQueueCommand& msg)
{
  auto start_idx = buf.find("UDP");
  if (start_idx == buf.npos)
  {
    std::cout << "Couldn't find UDP tag" << std::endl;
    return false;
  }

  auto split_idx = buf.find(" ", start_idx);
  if (split_idx == buf.npos)
  {
    std::cout << "Couldn't find space after UDP tag" << std::endl;
    return false;
  }

  // actually split the string into its bits
  msg.tag = buf.substr(0, split_idx);
  msg.command = buf.substr(split_idx + 1);

  std::cout << "Got TAG:\"" << msg.tag << "\", CMD:\"" << msg.command << "\"" << std::endl;

  return true;
}

template <>
void serialize(const sentry_acomms::UdpQueueCommand& msg, std::string& buf)
{
  buf = msg.tag + ' ' + msg.command;
}
}
#endif
