/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "SentryAcommsSettings.h"
#include "ui_SentryAcommsSettings.h"

#include <QRegExpValidator>

namespace navg {
namespace plugins {
namespace sentry_acomms {

SentryAcommsSettings::SentryAcommsSettings(QWidget* parent)
  : QDialog(parent)
  , ui(std::make_unique<Ui::SentryAcommsSettings>())
{
  ui->setupUi(this);

  // Add an ip address validator to the remote address line edit
  auto octet = QString{ "(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])" };
  auto ip_regex = QRegExp{ "^" + octet + "\\." + octet + "\\." + octet + "\\." +
                           octet + "$" };

  ui->sms_remote_address->setValidator(new QRegExpValidator(ip_regex));
  ui->umodem_remote_address->setValidator(new QRegExpValidator(ip_regex));
  ui->waveglider_remote_address->setValidator(new QRegExpValidator(ip_regex));
}

SentryAcommsSettings::~SentryAcommsSettings() = default;

void
SentryAcommsSettings::setListenPort(SentryAcomms::Modem modem, int port)
{
  switch (modem) {
    case SentryAcomms::Modem::AvTrak:
      ui->sms_local_port->setValue(port);
      return;
    case SentryAcomms::Modem::MicroModem:
      ui->umodem_local_port->setValue(port);
      return;
    case SentryAcomms::Modem::Waveglider:
      ui->waveglider_local_port->setValue(port);
      return;
    default:
      return;
  }
}

int
SentryAcommsSettings::listenPort(SentryAcomms::Modem modem) const
{
  switch (modem) {
    case SentryAcomms::Modem::AvTrak:
      return ui->sms_local_port->value();
    case SentryAcomms::Modem::MicroModem:
      return ui->umodem_local_port->value();
    case SentryAcomms::Modem::Waveglider:
      return ui->waveglider_local_port->value();
    default:
      return -1;
  }
}

void
SentryAcommsSettings::setRemotePort(SentryAcomms::Modem modem, int port)
{
  switch (modem) {
    case SentryAcomms::Modem::AvTrak:
      ui->sms_remote_port->setValue(port);
      return;
    case SentryAcomms::Modem::MicroModem:
      ui->umodem_remote_port->setValue(port);
      return;
    case SentryAcomms::Modem::Waveglider:
      ui->waveglider_remote_port->setValue(port);
      return;
    default:
      return;
  }
}
int
SentryAcommsSettings::remotePort(SentryAcomms::Modem modem) const
{
  switch (modem) {
    case SentryAcomms::Modem::AvTrak:
      return ui->sms_remote_port->value();
    case SentryAcomms::Modem::MicroModem:
      return ui->umodem_remote_port->value();
    case SentryAcomms::Modem::Waveglider:
      return ui->waveglider_remote_port->value();
    default:
      return -1;
  }
}
void
SentryAcommsSettings::setRemoteAddress(SentryAcomms::Modem modem,
                                       const QHostAddress& address)
{
  switch (modem) {
    case SentryAcomms::Modem::AvTrak:
      ui->sms_remote_address->setText(address.toString());
      return;
    case SentryAcomms::Modem::MicroModem:
      ui->umodem_remote_address->setText(address.toString());
      return;
    case SentryAcomms::Modem::Waveglider:
      ui->waveglider_remote_address->setText(address.toString());
      return;
    default:
      return;
  }
}
QHostAddress
SentryAcommsSettings::remoteAddress(SentryAcomms::Modem modem) const
{
  switch (modem) {
    case SentryAcomms::Modem::AvTrak:
      return QHostAddress{ ui->sms_remote_address->text() };
    case SentryAcomms::Modem::MicroModem:
      return QHostAddress{ ui->umodem_remote_address->text() };
    case SentryAcomms::Modem::Waveglider: {
      return QHostAddress{ ui->waveglider_remote_address->text() };
    }
    default:
      return {};
  }
}

void SentryAcommsSettings::setNavestHeadingToggle(bool use_heading){
  ui->acomms_heading_check->setChecked(use_heading);
}

void SentryAcommsSettings::setVehicleId(int id){
  ui->vehicle_id_spin->setValue(id);
}

bool SentryAcommsSettings::navestHeadingToggle(){
  return ui->acomms_heading_check->isChecked();
}

int SentryAcommsSettings::vehicleId(){
  return ui->vehicle_id_spin->value();
}

} // namespace navest
} // namespace plugins
} // namespace navg
