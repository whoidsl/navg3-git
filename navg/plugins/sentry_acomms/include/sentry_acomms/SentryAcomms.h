/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 7/23/18.
//

#ifndef NAVG_SENTRY_ACOMMS_H
#define NAVG_SENTRY_ACOMMS_H

#include "navg/CorePluginInterface.h"

#include <QHash>
#include <QHostAddress>
#include <QLoggingCategory>
#include <QWidget>
#include <QVariant>
#include "dslmap/MapView.h"
#include "dslmap/MapScene.h"
#include <../navest/Navest.h>
#include <../navest/NavestLayer.h>

#include <memory>

class QUdpSocket;
class QSignalMapper;

namespace navg {
namespace plugins {
namespace sentry_acomms {

namespace Ui {
class SentryAcomms;
}

class SentryAcommsLog;
class SentryLatestStatus;

class SentryAcomms : public QWidget, public CorePluginInterface
{
  Q_OBJECT
  Q_INTERFACES(navg::CorePluginInterface)
  Q_PLUGIN_METADATA(IID CorePluginInterface_iid FILE "SentryAcomms.json")

public:
  enum Modem
  {
    AvTrak = 0,
    MicroModem,
    Waveglider
  };
  Q_ENUM(Modem)

  explicit SentryAcomms(QWidget* parent = nullptr);
  ~SentryAcomms() override;

  //
  // CorePluginInterface Implementation
  //
  void loadSettings(QSettings* settings) override;
  void saveSettings(QSettings& settings) override;

  void connectPlugin(const QObject* plugin,
                     const QJsonObject& metadata) override;

  void setMapView(QPointer<dslmap::MapView> view) override;

  QList<QAction*> pluginMenuActions() override;
  QList<QAction*> pluginToolbarActions() override { return {}; };

  /// \brief Get the local listening port
  ///
  /// The plugin is listening for UDP packets on this port.
  /// \return
  int listenPort(Modem modem) const;

  /// \brief Get the remote navest port
  ///
  /// The plugin will send UDP packets to the address provided by
  /// PluginNavest::remoteAddress and this port
  ///
  /// \return
  int remotePort(Modem modem) const;

  /// \brief Get the remote navest host address
  ///
  /// The plugin will send UDP packets to this remote address on the port
  /// provided by PluginNavest::remotePort
  ///
  /// \return
  QHostAddress remoteAddress(Modem modem) const;

  /// \brief Returns the current trackline.
  ///
  /// \return
  unsigned int currentTrackline() const;

public slots:

  /// \breif Set the local listening port for Navest comms
  ///
  /// Attempts to bind to the provided port with the ShareAddress and
  /// ReuseAddress hints.
  ///
  /// \param port
  void setListenPort(Modem modem, int port);

  /// \brief Set the destination port for sent messages
  ///
  /// This port is used with the remote address
  /// (Navest::remoteAddress) to send messages to the navest daemon.
  ///
  /// \param port
  void setRemotePort(Modem modem, int port);

  /// \brief Set the destination address for sent messages
  ///
  /// This address is used with the remote port (Navest::remotePort)
  /// to send messages to the navest daemon.
  ///
  /// \param address
  void setRemoteAddress(Modem modem, const QHostAddress& address);

  /// \brief Set the destination address for sent messages
  ///
  /// This address is used with the remote port (Navest::remotePort)
  /// to send messages to the navest daemon.
  ///
  /// This is a overloaded method of Navest::setRemoteAddress
  /// (const QHostAddress&)
  ///
  /// \param address
  void setRemoteAddress(Modem modem, const QString& address);

  /// \brief Send a message to the Navest daemon
  ///
  /// The message will be sent to the remote host/port pair set by
  /// Navest::setRemotePort and Navest::setRemoteAddress
  ///
  /// \param message
  void sendMessage(Modem modem, const QByteArray& message);

  /// \brief Handle incoming data from the Sonardyne PAN port
  ///
  /// \param buf
  /// \param size
  QVariantHash handleAvTrakMessage(QString& buf);

  /// \brief Handle incoming data from the uModem driver.
  ///
  /// \param buf
  /// \param size
  QVariantHash handleMicroModemMessage(QString& buf);

  /// \brief Handle incoming data from the Waveglider port
  ///
  /// \param buf
  /// \param size
  QVariantHash handleWavegliderMessage(QString& buf);

  /// \brief Sets the current trackline
  ///
  /// Set automatically upon a successful SDQ 0 parse.
  ///
  /// \param trackline
  void setCurrentTrackline(unsigned int trackline);

signals:

  /// \brief Emitted when a message is received.
  void messageReceived(Modem source, const QByteArray& message);

  void messageReceived(Modem source, const QVariantHash message);

  /// \brief Emitted when a message is sent
  void messageSent(Modem destination, const QByteArray& message);

  /// \brief Emitted when the local port changes for a modem.
  void listenPortChanged(Modem source, int port);

  /// \brief Emitted when the remote port changes for a modem.
  void remotePortChanged(Modem destination, int port);

  /// \brief Emitted when the remote address changes for a modem.
  void remoteAddressChanged(Modem destination, QHostAddress address);

  /// \brief Emitted when the active trackline changes.
  void currentTracklineChanged(unsigned int trackline);

  /// \brief Emitted when any message with an abort status is received
  void abortStatusUpdated(bool is_aborting);

  /// \brief Emitted when any message with heading, depth, and altitude is
  /// received
  void hdgDepthAltUpdated(int hdg, int depth, int alt);

  /// \brief Emitted when any message with scalar science data is received
  void scalarScienceUpdated(float oxygen_concentration, float obs_raw,
                            float orp_raw, float ctd_temperature,
                            float ctd_salinity, float paro_depth);

  /// \brief Emitted when any message with vehicle-frame velocity is received
  void vehicleVelocityUpdated(float h_vel, float v_vel);

  // \brief Emitted when any message with vehicle state is received.
  void vehicleStateUpdated(float hdg, float depth, float alt, float h_vel,
                           float v_vel);

  /// \brief Emitted when use acomms heading is toggled or vehicle num changed                      
  void headingToggleChanged(bool heading_toggle, int id);

protected slots:
  void setAcommHeading(bool heading_toggle, int id, bool force = false);

private slots:
  void handleIncomingMessage(int modem);
  void showSettingsDialog();

  /// \brief If option enabled, update map vehicle marker heading
  void updateNavestHeading(int modem, const QVariantHash message);

private:
  QMap<Modem, QUdpSocket*> m_udp;
  QMap<Modem, QHostAddress> m_remote_address;
  QMap<Modem, int> m_remote_port;
  QSignalMapper* m_ready_read = nullptr;
  QAction* m_action_show_settings = nullptr;
  QAction* m_action_show_avtrak_log = nullptr;
  QAction* m_action_show_micromodem_log = nullptr;
  QAction* m_action_show_waveglider_log = nullptr;
  QAction* m_action_show_latest_status = nullptr;
  SentryAcommsLog* m_log_avtrak = nullptr;
  SentryAcommsLog* m_log_micromodem = nullptr;
  SentryAcommsLog* m_log_waveglider = nullptr;
  SentryLatestStatus* m_latest_status = nullptr;
  unsigned int m_current_trackline = std::numeric_limits<unsigned int>::max();
  navg::plugins::navest::NavestLayer* vehicle_layer = nullptr;

  QPointer<dslmap::MapView> m_view = nullptr;

  // Option to set sentry navest layer heading directly to value received by acomms
  bool use_acomm_heading = false;
  int navest_vehicle_id = 0;
};

Q_DECLARE_LOGGING_CATEGORY(plugin_sentry_acomms)
} // sentry_acomms
} // plugins
} // navg

#endif // NAVG_SENTRY_ACOMMS_H
