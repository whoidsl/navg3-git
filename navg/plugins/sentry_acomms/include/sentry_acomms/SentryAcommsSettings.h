/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef NAVG_PLUGINSENTRYACOMMSSETTINGS_H
#define NAVG_PLUGINSENTRYACOMMSSETTINGS_H

#include "SentryAcomms.h"

#include <QDialog>
#include <QHostAddress>

#include <memory>

namespace navg {
namespace plugins {
namespace sentry_acomms {

namespace Ui {
class SentryAcommsSettings;
}

class SentryAcommsSettings : public QDialog
{
  Q_OBJECT

public:
  explicit SentryAcommsSettings(QWidget* parent = 0);
  ~SentryAcommsSettings() override;

  /// \brief Set the listen port
  void setListenPort(SentryAcomms::Modem modem, int port);

  /// \breif get the listen port
  int listenPort(SentryAcomms::Modem modem) const;

  /// \breif Set the remote port
  void setRemotePort(SentryAcomms::Modem modem, int port);

  /// \breif Get the remote port
  int remotePort(SentryAcomms::Modem modem) const;

  /// \brief Set the remote host
  void setRemoteAddress(SentryAcomms::Modem modem, const QHostAddress& address);

  /// \brief Set the navest heading toggle
  void setNavestHeadingToggle(bool use_heading);

  /// \brief Set the navest vehicle id
  void setVehicleId(int id);

  /// \brief Get heading toggle state
  bool navestHeadingToggle();

  /// \brief Get the navest vehicle id
  int vehicleId();

  /// \breif Get the remote host
  QHostAddress remoteAddress(SentryAcomms::Modem modem) const;

private:
  std::unique_ptr<Ui::SentryAcommsSettings> ui;
};

} // namespace navest
} // namespace plugins
} // namespace navg
#endif // NAVG_PLUGINSENTRYACOMMSSETTINGS_H
