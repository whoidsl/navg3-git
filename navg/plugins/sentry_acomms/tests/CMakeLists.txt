add_executable(test_navg_sentry_acomms
  runner.cpp
  test_sms_serialization.cpp
  test_umodem_serialization.cpp
  test_umodem_match.cpp
  )

target_link_libraries(test_navg_sentry_acomms PRIVATE Qt5::Test Catch::Catch navg_sentry_acomms)

set_target_properties(test_navg_sentry_acomms
  PROPERTIES
  CXX_STANDARD 14
  CXX_STANDARD_REQUIRED ON
  )

add_test(test_navg_sentry_acomms test_navg_sentry_acomms)
