/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 1/15/19.
//
#include "SentryAcomms.h"
#include "catch.hpp"
#include <QRegularExpression>
//#include <QVector>
//#include <QLoggingCategory>

using namespace navg::plugins::sentry_acomms;

SCENARIO("uModem regex matching tests")
{
  auto regex = QRegularExpression{
    "\\$CARXD,(\\d+),(\\d+),(\\d+),(\\d+),([0-9a-fA-F]+)?\\*(\\d+)"
  };

  GIVEN("Valid uModem string and regex")
  {
    const QString message{ "$CARXD,0,15,0,1,"
                           "6405B49100360F0150137E001A04FE1C007036004C13002B000"
                           "000000095020000000007000000000000000000000000000000"
                           "00000000000000000000000000*20" };
    THEN("The uModem should match the regex")
    {
      const auto match = regex.match(message);
      REQUIRE(match.hasMatch());
      if (!match.hasMatch()) {
        qCWarning(plugin_sentry_acomms) << "Invalid frame reception message:"
                                        << message.trimmed();
      }
    }
  }

  GIVEN("Invalid uModem string and regex")
  {
    const QString fail_message{ "$CARXD,0,15,0,1,"
                                "64056E9100441001501381006D09FE1C007036004E1300"
                                "2900000000009602000000000700000000000000000000"
                                "0000000" };
    THEN("The uModem should match the regex")
    {
      const auto match = regex.match(fail_message);
      REQUIRE_FALSE(match.hasMatch());
      if (!match.hasMatch()) {
        qCWarning(plugin_sentry_acomms) << "Invalid frame reception message:"
                                        << fail_message.trimmed();
      }
    }
  }

  GIVEN("Valid short string")
  {
    const QString short_msg{ "$CARXD,0,15,0,2,*56" };
    THEN("The message should match")
    {
      const auto match = regex.match(short_msg);
      REQUIRE(match.hasMatch());
    }
  }
}
