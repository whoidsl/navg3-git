/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 1/14/19.
//

#include "Serialization.h"
#include "catch.hpp"
#include <QDebug>

using namespace navg::plugins::sentry_acomms;

SCENARIO("uModem Serialization tests")
{
  bool toggle_printlines = false;
  GIVEN("A valid uModem SDQ 0 string not aborting")
  {
    auto hex_str = QString{ "6405A89300988A0026138200B006A41D00081B002613002A00"
                            "00000000FF0100000000940000000000000000000000000000"
                            "0000000000000000000000000000*23" };

    WHEN("The string is turned into a set of decimal numbers")
    {
      for (int i = 0; i < hex_str.length() - 2; i += 2) {
        bool ok = false;
        int readme = hex_str.mid(i, 2).toInt(&ok, 16);
        if (ok && toggle_printlines) {
          qDebug() << i << ": " << readme;
        }
      }
    }

    WHEN("The string is deserialized")
    {
      const auto result = deserializeMicroModem(hex_str);

      THEN("The result is non-empty") { REQUIRE_FALSE(result.isEmpty()); }

      AND_THEN("The values are as expected")
      {
        CHECK(result["type"] == MessageType::DataQueue);
        CHECK(result["queue"] == QueueType::State);
        CHECK(result["x"].toDouble() == 3780);
        CHECK(result["y"].toDouble() == 3548);
        CHECK(result["depth"].toDouble() == 2451);
        CHECK(result["alt"].toDouble() == 65.0);
        CHECK(result["heading"].toDouble() == Approx(171.2));
        CHECK(result["h_vel"].toDouble() == Approx(1.05));
        CHECK(result["v_vel"].toDouble() == Approx(0.0));
        CHECK(result["abort"] == false);
        CHECK(result["battery"].toDouble() == Approx(51.1));
        CHECK(result["goal_x"].toDouble() == Approx(3794.0));
        CHECK(result["goal_y"].toDouble() == Approx(3460.0));
        CHECK(result["goal_z"].toDouble() == Approx(2451.0));
        CHECK(result["trackline"].toInt() == 148);
      }
    }
  }
}
