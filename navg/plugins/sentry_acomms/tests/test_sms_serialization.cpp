/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "catch.hpp"

#include "Serialization.h"

using namespace navg::plugins::sentry_acomms;

SCENARIO("Avtrak SMS Serialization tests")
{
  GIVEN("A valid SMS SDQ 0 string")
  {
    const auto SMS = QByteArray{ "SDQ "
                                 "0:2705,1487,963,65.0,178.5,1.00,0.04,A0,0,96."
                                 "3,2706,1442,963,0,0,0,0,0,0,0,13" };

    WHEN("The string is deserialized")
    {
      const auto result = deserialize(SMS);

      THEN("The result is non-empty") { REQUIRE_FALSE(result.isEmpty()); }

      AND_THEN("The values are as expected")
      {
        CHECK(result["type"] == MessageType::DataQueue);
        CHECK(result["queue"] == QueueType::State);
        CHECK(result["x"].toDouble() == 2705);
        CHECK(result["y"].toDouble() == 1487);
        CHECK(result["depth"].toDouble() == 963);
        CHECK(result["alt"].toDouble() == 65.0);
        CHECK(result["heading"].toDouble() == 178.5);
        CHECK(result["h_vel"].toDouble() == 1.0);
        CHECK(result["v_vel"].toDouble() == Approx(0.04));
        CHECK(result["abort"] == false);
        CHECK(result["battery"].toDouble() == Approx(96.3));
        CHECK(result["goal_x"].toDouble() == 2706);
        CHECK(result["goal_y"].toDouble() == 1442);
        CHECK(result["goal_z"].toDouble() == 963);
        CHECK(result["trackline"].toInt() == 13);
      }
    }
  }
}
