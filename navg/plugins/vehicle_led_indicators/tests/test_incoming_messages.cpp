/**
 * Copyright 2021 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUnamespace dslmap
 * {R OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
//
// Created by tmjoyce on 05/27/21.
//

#include "LedTester.h"
#include "catch.hpp"
#include <QLoggingCategory>
#include <QTimer>
#include <iostream>
#include <navest/Messages.h>

using namespace navg::plugins::vehicle_led_indicators;

SCENARIO("Receive incoming navest messages to change led statuses")
{

  auto plugin = std::make_unique<LedTester>(nullptr);

  GIVEN("Any incoming navest message")
  {

    navg::NavestMessageType type = navg::NavestMessageType::NDS;
    navg::NavestMessageNds nds_message = { 283853, 384858, 50.1 };
    QVariant navest_message = QVariant::fromValue(nds_message);

    plugin->giveTestNavestMessage(type, navest_message);
    plugin->forceLedUpdate();

    THEN("The Eng Comms Led should OK STATUS")
    {
      LED_STATUS correct_status = OK_STATUS;
      LED_STATUS actual_status = plugin->getEngCommsLightStatus();

      REQUIRE(correct_status == actual_status);
    }
    AND_THEN(
      "After waiting 3 seconds the eng comms led should be WARNING STATUS")
    {
      QDateTime current_dt = QDateTime::currentDateTime();
      plugin->setNavestLastReceived(current_dt.addSecs(-3));
      // now remember to update the lights
      plugin->forceLedUpdate();

      LED_STATUS correct_status = WARNING_STATUS;
      LED_STATUS actual_status = plugin->getEngCommsLightStatus();

      REQUIRE(correct_status == actual_status);
    }
    AND_THEN("Waiting a few more seconds for ERROR status")
    {
      QDateTime current_dt = QDateTime::currentDateTime();
      plugin->setNavestLastReceived(current_dt.addSecs(-20));
      // now remember to update the lights
      plugin->forceLedUpdate();

      LED_STATUS correct_status = ERROR_STATUS;
      LED_STATUS actual_status = plugin->getEngCommsLightStatus();

      REQUIRE(correct_status == actual_status);
    }
  }
  GIVEN("An Incoming NDS message with space remaining over 80")
  {
    navg::NavestMessageType type = navg::NavestMessageType::NDS;
    navg::NavestMessageNds nds_message = { 1283853, 3848538, 80.1 };
    QVariant navest_message = QVariant::fromValue(nds_message);
    plugin->giveTestNavestMessage(type, navest_message);

    THEN("The eng led status should be a Warning")
    {
      LED_STATUS correct_status = WARNING_STATUS;
      LED_STATUS actual_status = plugin->getEngLightStatus();

      REQUIRE(correct_status == actual_status);
    }
    AND_THEN("A new nds message but with the space remaining less than or "
             "equal 80% should be OK STATUS")
    {
      navg::NavestMessageType type = navg::NavestMessageType::NDS;
      navg::NavestMessageNds nds_message = { 1283853, 3848538, 20.1 };
      QVariant navest_message = QVariant::fromValue(nds_message);
      plugin->giveTestNavestMessage(type, navest_message);
      LED_STATUS correct_status = OK_STATUS;
      LED_STATUS actual_status = plugin->getEngLightStatus();

      REQUIRE(correct_status == actual_status);
    }
  }
  GIVEN("Incoming DVL Messages(DDD)")
  {
    QDateTime current_dt = QDateTime::currentDateTime();
    navg::NavestMessageType type = navg::NavestMessageType::DDD;
    double beam_ranges[4] = { 0 };

    const auto ddd_message = navg::NavestMessageDdd{
      .timestamp = current_dt,
      .roll = 0,
      .pitch = 0,
      .heading = 0,
      .beam_ranges = { 0, 0, 0, 0 },
      .beam_velocities = { 0, 0, 0, 0 },
      .alt = 0,
      .bt_beams = 4,
      .wt_beams = 0,
      .message = "",
    };

    QVariant navest_message = QVariant::fromValue(ddd_message);
    plugin->giveTestNavestMessage(type, navest_message);
    plugin->forceLedUpdate();

    THEN("The DVL comms light should be OK")
    {
      LED_STATUS correct_status = OK_STATUS;
      LED_STATUS actual_status = plugin->getDvlCommsLightStatus();
      REQUIRE(correct_status == actual_status);
    }
    AND_THEN("The DVL light should also be OK")
    {
      LED_STATUS correct_status = OK_STATUS;
      LED_STATUS actual_status = plugin->getDvlLightStatus();
      REQUIRE(correct_status == actual_status);
    }
  }
  AND_THEN("The Dvl Beams drop to 3")
  {
    QDateTime current_dt = QDateTime::currentDateTime();
    navg::NavestMessageType type = navg::NavestMessageType::DDD;
    const auto ddd_message = navg::NavestMessageDdd{
      .timestamp = current_dt,
      .roll = 0,
      .pitch = 0,
      .heading = 0,
      .beam_ranges = { 0, 0, 0, 0 },
      .beam_velocities = { 0, 0, 0, 0 },
      .alt = 0,
      .bt_beams = 3,
      .wt_beams = 0,
      .message = "",
    };
    QVariant navest_message = QVariant::fromValue(ddd_message);
    plugin->giveTestNavestMessage(type, navest_message);
    plugin->forceLedUpdate();

    LED_STATUS correct_status = WARNING_STATUS;
    LED_STATUS actual_status = plugin->getDvlLightStatus();
    REQUIRE(correct_status == actual_status);
  }
  AND_THEN("There are less than 3 beams")
  {
    QDateTime current_dt = QDateTime::currentDateTime();
    navg::NavestMessageType type = navg::NavestMessageType::DDD;
    const auto ddd_message = navg::NavestMessageDdd{
      .timestamp = current_dt,
      .roll = 0,
      .pitch = 0,
      .heading = 0,
      .beam_ranges = { 0, 0, 0, 0 },
      .beam_velocities = { 0, 0, 0, 0 },
      .alt = 0,
      .bt_beams = 1,
      .wt_beams = 0,
      .message = "",
    };

    QVariant navest_message = QVariant::fromValue(ddd_message);
    plugin->giveTestNavestMessage(type, navest_message);
    plugin->forceLedUpdate();

    LED_STATUS correct_status = ERROR_STATUS;
    LED_STATUS actual_status = plugin->getDvlLightStatus();
    REQUIRE(correct_status == actual_status);
  }
  GIVEN("An ahrs message over time")
  {
    navg::NavestMessageType type = navg::NavestMessageType::OCT;
    navg::NavestMessageOct oct_message = { "", 180, 3, 56 };
    QVariant navest_message = QVariant::fromValue(oct_message);

    plugin->giveTestNavestMessage(type, navest_message);
    plugin->forceLedUpdate();

    THEN("The ahrs Led should OK STATUS")
    {
      LED_STATUS correct_status = OK_STATUS;
      LED_STATUS actual_status = plugin->getAhrsLightStatus();

      REQUIRE(correct_status == actual_status);
    }
    AND_THEN(
      "After waiting 3 seconds the ahrs comms led should be WARNING STATUS")
    {
      QDateTime current_dt = QDateTime::currentDateTime();
      plugin->setAhrsLastReceived(current_dt.addSecs(-3));
      // now remember to update the lights
      plugin->forceLedUpdate();

      LED_STATUS correct_status = WARNING_STATUS;
      LED_STATUS actual_status = plugin->getAhrsLightStatus();

      REQUIRE(correct_status == actual_status);
    }
    AND_THEN("Waiting a few more seconds for ERROR status")
    {
      QDateTime current_dt = QDateTime::currentDateTime();
      plugin->setAhrsLastReceived(current_dt.addSecs(-100));
      // now remember to update the lights
      plugin->forceLedUpdate();

      LED_STATUS correct_status = ERROR_STATUS;
      LED_STATUS actual_status = plugin->getAhrsLightStatus();

      REQUIRE(correct_status == actual_status);
    }
  }
  GIVEN("An external fix message over time")
  {

    int vehicle_number_in = 0;
    QString fix_source = "MY_SOURCE";
    double lon = 54.345;
    double lat = -20.39483;
    double depth = 300;
    bool is_reset_source = true;

    plugin->giveTestVfrMessage(vehicle_number_in, fix_source, lon, lat, depth, is_reset_source);
    plugin->forceLedUpdate();

    THEN("The external fix led should OK STATUS")
    {
      LED_STATUS correct_status = OK_STATUS;
      LED_STATUS actual_status = plugin->getFixLightStatus();

      REQUIRE(correct_status == actual_status);
    }
    AND_THEN(
      "After waiting 18 seconds the ext fix led should be WARNING STATUS")
    {
      QDateTime current_dt = QDateTime::currentDateTime();
      plugin->setFixLastReceived(current_dt.addSecs(-18));
      // now remember to update the lights
      plugin->forceLedUpdate();

      LED_STATUS correct_status = WARNING_STATUS;
      LED_STATUS actual_status = plugin->getFixLightStatus();

      REQUIRE(correct_status == actual_status);
    }
    AND_THEN("Waiting a few more seconds for ERROR status")
    {
      QDateTime current_dt = QDateTime::currentDateTime();
      plugin->setFixLastReceived(current_dt.addSecs(-100));
      // now remember to update the lights
      plugin->forceLedUpdate();

      LED_STATUS correct_status = ERROR_STATUS;
      LED_STATUS actual_status = plugin->getFixLightStatus();

      REQUIRE(correct_status == actual_status);
    }
  }
}
