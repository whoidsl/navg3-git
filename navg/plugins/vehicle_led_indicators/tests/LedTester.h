/**
 * Copyright 2021 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUnamespace dslmap
 * {R OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
//
// Created by tmjoyce on 05/27/21.
//

#include "LedIndicators.h"
#include <navest/Messages.h>

using namespace navg::plugins::vehicle_led_indicators;

class LedTester : public LedIndicators
{

  Q_OBJECT

public:
  LedTester(QWidget* parent = nullptr)
    : LedIndicators(parent)
  {}

  void giveTestNavestMessage(navg::NavestMessageType type, QVariant message)
  {
    updateFromNavestMessage(type, message);
  }

  void giveTestVfrMessage(int vehicle_number_in,
                             QString fix_source,
                             double lon,
                             double lat,
                             double depth,
                             bool is_reset_source)
  {
    updateFromVfr(vehicle_number_in, fix_source, lon, lat, depth, is_reset_source);
  }

  void setAhrsLastReceived(QDateTime time) { last_ahrs_message = time; }
  void setNdsLastReceived(QDateTime time) { last_nds_message = time; }
  void setNavestLastReceived(QDateTime time) { last_navest_message = time; }
  void setDvlCommsLastReceived(QDateTime time)
  {
    last_dvl_comms_message = time;
  }
  void setFixLastReceived(QDateTime time) { last_external_fix_message = time; }

  void forceLedUpdate() { update(); }
};