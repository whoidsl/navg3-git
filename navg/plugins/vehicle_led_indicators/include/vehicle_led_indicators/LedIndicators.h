/**
 * Copyright 2021 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// Created by tmjoyce on 05/19/21.
//

#ifndef NAVG_LEDINDICATORS_H
#define NAVG_LEDINDICATORS_H

#include <navg/CorePluginInterface.h>

#include <QAction>
#include <QDateTime>
#include <QLoggingCategory>
#include <QObject>
#include <QTimer>
#include <QVariant>
#include <QWidget>
#include <memory>
#include <navest/Messages.h>
#include <navg/CorePluginInterface.h>

#include "LedLabel.h"
#include "LedSettings.h"

// time interval definitions in milliseconds
#define STATUS_ERROR_INTERVAL 5000
#define STATUS_WARNING_INTERVAL 2500
#define FIX_STATUS_ERROR_INTERVAL 30000
#define FIX_STATUS_WARNING_INTERVAL 15000

namespace navg {
namespace plugins {
namespace vehicle_led_indicators {

namespace Ui {
class LedIndicators;
}

class LedIndicators
  : public QWidget
  , public CorePluginInterface
{
  Q_OBJECT
  Q_INTERFACES(navg::CorePluginInterface)
  Q_PLUGIN_METADATA(IID CorePluginInterface_iid FILE "LedIndicators.json")

signals:
  void ledSizeModeChanged(int size);
  void sendSizeToLabel(int size);
public:
  explicit LedIndicators(QWidget* parent = nullptr);
  ~LedIndicators() override;

  //
  // CorePluginInterface Implementation
  //
  void loadSettings(QSettings* settings) override;
  void saveSettings(QSettings& settings) override;

  // This plugin connects to ....
  void connectPlugin(const QObject* plugin,
                     const QJsonObject& metadata) override;
  void setMapView(QPointer<dslmap::MapView> view) override;
  QList<QAction*> pluginMenuActions() override;
  QList<QAction*> pluginToolbarActions() override { return {}; }

  // some getters for the led statuses
  const LED_STATUS getDvlLightStatus();
  const LED_STATUS getDvlCommsLightStatus();
  const LED_STATUS getAhrsLightStatus();
  const LED_STATUS getFixLightStatus();
  const LED_STATUS getEngLightStatus();
  const LED_STATUS getEngCommsLightStatus();

public slots:

  /// \brief Show the settings dialog
  ///
  void showSettingsDialog();

protected:
  QDateTime last_navest_message;
  QDateTime last_dvl_comms_message;
  QDateTime last_ahrs_message;
  QDateTime last_external_fix_message;
  QDateTime last_nds_message;

protected slots:

  /// \brief Handle multiple incoming message types from navest
  void updateFromNavestMessage(NavestMessageType type, QVariant message);

  void updateFromVfr(int vehicle_number_in, QString fix_source, double lon,
                          double lat, double depth, bool is_reset_source = false);

  void setLedSize(int size);
  /// \brief Check for and update led statuses when appropriate
  void update();

private:
  std::unique_ptr<Ui::LedIndicators> ui;
  QPointer<dslmap::MapView> m_view;
  QScopedPointer<QTimer> timer;

  QAction* m_action_show_settings = nullptr;

  void updateEngLed(const double percent_full);
  void updateEngCommsLed();
  void updateExternalFixLed();
  void updateDvlLed(const int bt_beam_count);
  void updateDvlCommsLed();
  void updateAhrsLed();

  // used as the vehicle id of the fix vehicle
  int vehicle_number;

  int led_size = 1;
};

Q_DECLARE_LOGGING_CATEGORY(plugin_led)
} // LedIndicators
} // plugins
} // navg

#endif // LEDINDICATORS_H
