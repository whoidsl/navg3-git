#ifndef LEDLABEL_H
#define LEDLABEL_H

#include <QLabel>
#include <QLoggingCategory>

enum LED_STATUS
{
  OK_STATUS,
  WARNING_STATUS,
  ERROR_STATUS,
  UNKNOWN_STATUS
};

namespace navg {
namespace plugins {
namespace vehicle_led_indicators {

class LedLabel : public QLabel
{
  Q_OBJECT
public:
  explicit LedLabel(QWidget* parent = 0);
  void setLedStatus(LED_STATUS status);
  LED_STATUS getStatus();
  int getLedSize();
  int ledSize = 40;
protected:
  
private:
  // the stored status of the led
  LED_STATUS led_status;
  // led stylesheets

   const QString green =
   QString("border-radius: %1px; min-height: %2px; min-width: %2px; "
            "max-width: %2px;color: white;border-radius: %1;background-color: "
            "qlineargradient(spread:pad, x1:0.145, y1:0.16, x2:1, y2:1, stop:0 "
            "rgba(124,252,0, 255), stop:1 rgba(25, 134, 5, 255));");
    
   const QString red =
    QString(
      "border-radius: %1px; min-height: %2px; min-width: %2px; "
      "max-width: %2px;color: white;border-radius: %1;background-color: "
      "qlineargradient(spread:pad, x1:0.145, y1:0.16, x2:0.92, y2:0.988636, "
      "stop:0 rgba(255, 12, 12, 255), stop:0.869347 rgba(103, 0, 0, 255));");
      
   const QString yellow =
    QString(
      "border-radius: %1px; min-height: %2px; min-width: %2px; "
      "max-width: %2px;color: white;border-radius: %1;background-color: "
      "qlineargradient(spread:pad, x1:0.232, y1:0.272, x2:0.98, y2:0.959773, "
      "stop:0 rgba(220, 245, 100, 255), stop:1 rgba(91, 41, 7, 255))");
     
   const QString gray =
    QString("border-radius: %1px; min-height: %2px; min-width: %2px; "
            "max-width: %2px;color: white;border-radius: %1;background-color: "
            "qlineargradient(spread:pad, x1:0.04, y1:0.0565909, x2:0.799, "
            "y2:0.795, stop:0 rgba(128, 128, 128, 255), stop:0.41206 rgba(192, "
            "192, 192, 255), stop:1 rgba(105,105,105, 255));");
public slots:
  void changeLedSize(int size);

};
}
}
}
#endif
