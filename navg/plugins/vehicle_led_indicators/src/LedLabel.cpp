#include "LedLabel.h"

namespace navg {
namespace plugins {
namespace vehicle_led_indicators {

LedLabel::LedLabel(QWidget* parent)
  : QLabel(parent)
{
  ledSize = getLedSize();
  auto grayUpdatedSize = gray.arg(ledSize / 2).arg(ledSize);
  setStyleSheet(grayUpdatedSize);
  led_status = UNKNOWN_STATUS;
}

void
LedLabel::changeLedSize(int size) {
  if (size == 0) {
    ledSize = 20;
  }
  else {
    ledSize = 40;
  }
  auto grayUpdatedSize = gray.arg(ledSize / 2).arg(ledSize);
  auto greenUpdatedSize = green.arg(ledSize / 2).arg(ledSize);
  auto yellowUpdatedSize = yellow.arg(ledSize / 2).arg(ledSize);
  auto redUpdatedSize = red.arg(ledSize / 2).arg(ledSize);

  auto status = getStatus();
  switch (status) {
    case OK_STATUS:
      setStyleSheet(greenUpdatedSize);
      led_status = OK_STATUS;
      break;
    case WARNING_STATUS:
      setStyleSheet(yellowUpdatedSize);
      led_status = WARNING_STATUS;
      break;
    case ERROR_STATUS:
      setStyleSheet(redUpdatedSize);
      led_status = ERROR_STATUS;
      break;
    case UNKNOWN_STATUS:
      setStyleSheet(grayUpdatedSize);
      led_status = UNKNOWN_STATUS;
      break;
    default:
      setStyleSheet(grayUpdatedSize);
      led_status = UNKNOWN_STATUS;
  }
}

int 
LedLabel::getLedSize() {
  return ledSize;
}
void
LedLabel::setLedStatus(LED_STATUS status)
{

  int ledSize = getLedSize();
  auto grayUpdatedSize = gray.arg(ledSize / 2).arg(ledSize);
  auto greenUpdatedSize = green.arg(ledSize / 2).arg(ledSize);
  auto yellowUpdatedSize = yellow.arg(ledSize / 2).arg(ledSize);
  auto redUpdatedSize = red.arg(ledSize / 2).arg(ledSize);

  switch (status) {
    case OK_STATUS:
      setStyleSheet(greenUpdatedSize);
      led_status = OK_STATUS;
      break;
    case WARNING_STATUS:
      setStyleSheet(yellowUpdatedSize);
      led_status = WARNING_STATUS;
      break;
    case ERROR_STATUS:
      setStyleSheet(redUpdatedSize);
      led_status = ERROR_STATUS;
      break;
    case UNKNOWN_STATUS:
      setStyleSheet(grayUpdatedSize);
      led_status = UNKNOWN_STATUS;
      break;
    default:
      setStyleSheet(grayUpdatedSize);
      led_status = UNKNOWN_STATUS;
  }
}

LED_STATUS
LedLabel::getStatus()
{
  return led_status;
}
}
}
}