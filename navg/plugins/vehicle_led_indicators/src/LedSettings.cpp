/**
 * Copyright 2019 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "LedSettings.h"
#include "ui_LedSettings.h"
#include <QDebug>
namespace navg {
namespace plugins {
namespace vehicle_led_indicators {

LedSettings::LedSettings(QWidget* parent)
  : QDialog(parent)
  , ui(std::make_unique<Ui::LedSettings>())
{
  ui->setupUi(this);
}

LedSettings::~LedSettings() = default;

void
LedSettings::setFixVehicleNumberBox(int num)
{
  ui->fix_vehicle_spin->setValue(num);
}
int
LedSettings::getFixVehicleNumber()
{
  return ui->fix_vehicle_spin->value();
}

int
LedSettings::getLedSizeMode(){

  return ui->large_led_size->isChecked();
}

void
LedSettings::setLedSizeMode(int size){
  if (size == 1){
    ui->large_led_size->setChecked(true);
  }
  else {
    ui->small_led_size->setChecked(true);
  }
}



} // namespace vehicle_led_indicators
} // namespace plugins
} // namespace navg
