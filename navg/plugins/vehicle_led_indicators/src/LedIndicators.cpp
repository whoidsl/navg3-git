/**
 * Copyright 2021 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// Created by tmjoyce 05/19/21.
//

#include "LedIndicators.h"
#include "ui_LedIndicators.h"
#include <QSettings>
#include <dslmap/MapView.h>

namespace navg {
namespace plugins {
namespace vehicle_led_indicators {

Q_LOGGING_CATEGORY(plugin_led, "navg.plugins.vehicle_led_indicators")

LedIndicators::LedIndicators(QWidget* parent)
  : QWidget(parent)
  , ui(std::make_unique<Ui::LedIndicators>())
  , m_action_show_settings(new QAction("&Settings", this))
{
  ui->setupUi(this);
  vehicle_number = 0;
  timer.reset(new QTimer(this));

  connect(timer.data(), &QTimer::timeout, this, &LedIndicators::update);

  // update and check leds every half second
  timer->start(500);

  connect(m_action_show_settings,
          &QAction::triggered,
          this,
          &LedIndicators::showSettingsDialog);
 connect(this, &LedIndicators::ledSizeModeChanged, this, &LedIndicators::setLedSize);
        
}

LedIndicators::~LedIndicators() = default;

void
LedIndicators::setMapView(QPointer<dslmap::MapView> view)
{
  m_view = view;
}

void
LedIndicators::connectPlugin(const QObject* plugin, const QJsonObject& metadata)
{
  if (!plugin) {
    return;
  }

  const auto name =
    metadata.value("MetaData").toObject().value("name").toString();
  if (name.isEmpty()) {
    return;
  }

  if (name == "navest") {

    if (!connect(plugin,
                 SIGNAL(messageReceived(NavestMessageType, QVariant)),
                 this,
                 SLOT(updateFromNavestMessage(NavestMessageType, QVariant)))) {
      qCCritical(plugin_led,
                 "Unable to connect to signal: "
                 "Navest::messageReceived(NavestMessageType, "
                 "QVariant)");
    } else {
      qCInfo(plugin_led, "Connected to Navest::messageReceived");
    }
    if (!connect(
          plugin,
          SIGNAL(
            vfrMessageReceived(int, QString, double, double, double, bool)),
          this,
          SLOT(updateFromVfr(int, QString, double, double, double, bool)))) {
      qCCritical(plugin_led,
                 "Unable to connect to signal: "
                 "Navest::messageReceived(NavestMessageType, "
                 "QVariant)");
    } else {
      qCInfo(plugin_led, "Connected to Navest::messageReceived");
    }
  }
}


void
LedIndicators::setLedSize(int size)
{
  connect(this, &LedIndicators::sendSizeToLabel, ui->fix_led, &LedLabel::changeLedSize);
  connect(this, &LedIndicators::sendSizeToLabel, ui->dvl_led, &LedLabel::changeLedSize);
  connect(this, &LedIndicators::sendSizeToLabel, ui->dvl_comms_led, &LedLabel::changeLedSize);
  connect(this, &LedIndicators::sendSizeToLabel, ui->ahrs_led, &LedLabel::changeLedSize);
 // connect(this, &LedIndicators::sendSizeToLabel, ui->ins_led, &LedLabel::changeLedSize);
  connect(this, &LedIndicators::sendSizeToLabel, ui->eng_led, &LedLabel::changeLedSize);
  connect(this, &LedIndicators::sendSizeToLabel, ui->eng_comms_led, &LedLabel::changeLedSize);

  emit sendSizeToLabel(size);

}

void
LedIndicators::loadSettings(QSettings* settings)
{
  auto value = settings->value("vehicle_fix_number");
  auto ok = false;
  if (value.isValid()) {
    const auto valid_val = value.toInt(&ok);
    if (ok) {
      vehicle_number = valid_val;
    }
  }

  auto valueLed = settings->value("led_size");
   if (valueLed.isValid()) {
        led_size = valueLed.toInt();
    }
  emit(ledSizeModeChanged(led_size));
}

void
LedIndicators::saveSettings(QSettings& settings)
{
  settings.setValue("vehicle_led_indicators_val", 1);
  settings.setValue("vehicle_fix_number", vehicle_number);
  settings.setValue("vehicle_fix_number", vehicle_number);
  settings.setValue("led_size", led_size);
}

void
LedIndicators::showSettingsDialog()
{

  QScopedPointer<LedSettings> dialog(new LedSettings);
  dialog->setFixVehicleNumberBox(vehicle_number);
  dialog->setLedSizeMode(led_size);
  dialog->setModal(true);
  if (!dialog->exec()) {
    return;
  }
  vehicle_number = dialog->getFixVehicleNumber();
  int size = dialog->getLedSizeMode();
  if (size != led_size) {
    led_size = size;
    emit ledSizeModeChanged(led_size);
  }
}

QList<QAction*>
LedIndicators::pluginMenuActions()
{
  return { m_action_show_settings };
}

void
LedIndicators::update()
{
  updateEngCommsLed();
  updateExternalFixLed();
  updateDvlCommsLed();
  updateAhrsLed();
}

void
LedIndicators::updateFromVfr(int vehicle_number_in,
                             QString fix_source,
                             double lon,
                             double lat,
                             double depth,
                             bool is_reset_source)
{
    // check if it is a fix from the "correct" vehicle id number
    if (vehicle_number_in == vehicle_number && is_reset_source) {
      last_external_fix_message = QDateTime::currentDateTimeUtc();
    }
    //else qDebug(plugin_led) << "SOURCE " << fix_source << " is NOT AN external fix";
}

void
LedIndicators::updateFromNavestMessage(NavestMessageType type, QVariant message)
{

  // any message received from navest counts as last navest fix for eng comms
  // led

  last_navest_message = QDateTime::currentDateTime();

  //this was the old way of updating from vfr
  /*
  if (type == navg::NavestMessageType::VFR) {

    auto vfr_message = message.value<NavestMessageVfr>();
    // check if it is a fix from the "correct" vehicle id number
    if (vfr_message.vehicle_number == vehicle_number) {
      last_external_fix_message = QDateTime::currentDateTimeUtc();
    }

  }
  */

  if (type == navg::NavestMessageType::DDD) {

    last_dvl_comms_message = QDateTime::currentDateTime();
    // extract num of beams from ddd message
    auto ddd_message = message.value<NavestMessageDdd>();
    updateDvlLed(ddd_message.bt_beams);
  }

  else if (type == navg::NavestMessageType::OCT) {

    last_ahrs_message = QDateTime::currentDateTime();

  }

  else if (type == navg::NavestMessageType::NDS) {

    last_nds_message = QDateTime::currentDateTime();
    // eng update disk space
    auto nds_message = message.value<NavestMessageNds>();
    updateEngLed(nds_message.percent_full);
  }
}

void
LedIndicators::updateEngLed(const double percent_full)
{
  // eng status led.
  // related to how full the vehicle disk is (NDS)

  if (percent_full > 80.0) {
    ui->eng_led->setLedStatus(WARNING_STATUS);
  } else {
    ui->eng_led->setLedStatus(OK_STATUS);
  }
}

void
LedIndicators::updateEngCommsLed()
{
  // eng comms led.
  // related to the time elapsed since last navest msg
  // uses STATUS_X_INTERVAL
  if (last_navest_message.isNull()) {
    return;
  }
  // difference in milliseconds
  uint last_navest_time_diff =
    (QDateTime::currentDateTime().toTime_t() - last_navest_message.toTime_t()) *
    1000;
  if (last_navest_time_diff > STATUS_ERROR_INTERVAL) {
    ui->eng_comms_led->setLedStatus(ERROR_STATUS);
  } else if (last_navest_time_diff > STATUS_WARNING_INTERVAL) {
    ui->eng_comms_led->setLedStatus(WARNING_STATUS);
  } else {
    ui->eng_comms_led->setLedStatus(OK_STATUS);
  }
}

void
LedIndicators::updateExternalFixLed()
{

  // fix status led.
  // related to when last fix was received (VFR)
  // uses FIX_STATUS_X_INTERVAL

  if (last_external_fix_message.isNull()) {
    return;
  }
  // difference in milliseconds
  uint last_external_fix_time_diff = (QDateTime::currentDateTime().toTime_t() -
                                      last_external_fix_message.toTime_t()) *
                                     1000;
  if (last_external_fix_time_diff > FIX_STATUS_ERROR_INTERVAL) {
    ui->fix_led->setLedStatus(ERROR_STATUS);
  } else if (last_external_fix_time_diff > FIX_STATUS_WARNING_INTERVAL) {
    ui->fix_led->setLedStatus(WARNING_STATUS);
  } else {
    ui->fix_led->setLedStatus(OK_STATUS);
  }
}

void
LedIndicators::updateDvlLed(const int bt_beam_count)
{

  if (bt_beam_count == 4) {
    ui->dvl_led->setLedStatus(OK_STATUS);
  } else if (bt_beam_count == 3) {
    ui->dvl_led->setLedStatus(WARNING_STATUS);
  }

  else if (bt_beam_count < 0) {
    ui->dvl_led->setLedStatus(UNKNOWN_STATUS);
  }

  else {
    ui->dvl_led->setLedStatus(ERROR_STATUS);
  }
}

void
LedIndicators::updateDvlCommsLed()
{
  // dvl comms led.
  // related to the time elapsed since last dvl msg (DDD)
  // uses STATUS_X_INTERVAL
  if (last_dvl_comms_message.isNull()) {
    return;
  }
  // difference in milliseconds
  uint last_dvl_comms_time_diff = (QDateTime::currentDateTime().toTime_t() -
                                   last_dvl_comms_message.toTime_t()) *
                                  1000;
  if (last_dvl_comms_time_diff > STATUS_ERROR_INTERVAL) {
    ui->dvl_comms_led->setLedStatus(ERROR_STATUS);
  } else if (last_dvl_comms_time_diff > STATUS_WARNING_INTERVAL) {
    ui->dvl_comms_led->setLedStatus(WARNING_STATUS);
  } else {
    ui->dvl_comms_led->setLedStatus(OK_STATUS);
  }
}

void
LedIndicators::updateAhrsLed()
{
  // ahrs led.
  // related to the time elapsed since last ahrs msg (OCT)
  // uses STATUS_X_INTERVAL
  if (last_ahrs_message.isNull()) {
    return;
  }
  // difference in milliseconds
  uint last_ahrs_time_diff =
    (QDateTime::currentDateTime().toTime_t() - last_ahrs_message.toTime_t()) *
    1000;
  if (last_ahrs_time_diff > STATUS_ERROR_INTERVAL) {
    ui->ahrs_led->setLedStatus(ERROR_STATUS);
  } else if (last_ahrs_time_diff > STATUS_WARNING_INTERVAL) {
    ui->ahrs_led->setLedStatus(WARNING_STATUS);
  } else {
    ui->ahrs_led->setLedStatus(OK_STATUS);
  }
}

const LED_STATUS
LedIndicators::getDvlLightStatus()
{
  return ui->dvl_led->getStatus();
}
const LED_STATUS
LedIndicators::getDvlCommsLightStatus()
{
  return ui->dvl_comms_led->getStatus();
}
const LED_STATUS
LedIndicators::getAhrsLightStatus()
{
  return ui->ahrs_led->getStatus();
}
const LED_STATUS
LedIndicators::getFixLightStatus()
{
  return ui->fix_led->getStatus();
}
const LED_STATUS
LedIndicators::getEngLightStatus()
{
  return ui->eng_led->getStatus();
}
const LED_STATUS
LedIndicators::getEngCommsLightStatus()
{
  return ui->eng_comms_led->getStatus();
}

} // vehicle_led_indicators
} // plugins
} // navg
