/**
 * Copyright 2019 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "RemoteTargetDialog.h"
#include "SymbolLayer.h"
#include "ui_RemoteTargetDialog.h"

namespace navg {
namespace plugins {
namespace target_sender {

RemoteTargetDialog::RemoteTargetDialog(QWidget* parent)
  : QDialog(parent)
  , ui(new Ui::RemoteTargetDialog)
{
  ui->setupUi(this);
  ui->editLat->hide();
  ui->editLon->hide();

  connect(ui->latlonBtn, &QRadioButton::clicked, ui->editLat, &QLineEdit::show);
  connect(ui->latlonBtn, &QRadioButton::clicked, ui->editLon, &QLineEdit::show);
  connect(
    ui->okBox, &QDialogButtonBox::accepted, this, &RemoteTargetDialog::accept);
  connect(
    ui->okBox, &QDialogButtonBox::rejected, this, &RemoteTargetDialog::reject);
}

RemoteTargetDialog::RemoteTargetDialog(QMap<QString, QPointF> position_options,
                                       QWidget* parent)
  : RemoteTargetDialog(parent)
{
  setPositions(position_options);
}

RemoteTargetDialog::~RemoteTargetDialog()
{
  delete ui;
}

QString
RemoteTargetDialog::extractLabeltext()
{
  return ui->editLabel->text();
}

QPointF
RemoteTargetDialog::extractPos()
{
  if (ui->latlonBtn->isChecked()) {
    QPointF ret{ ui->editLon->value(),
                 ui->editLat->value()};
    return ret;
  }

  else if (ui->cursorBtn->isChecked()) {
    return cursor_position;
  }

  return QPointF{};
}

void
RemoteTargetDialog::setCursorPosition(QPointF pos)
{
  cursor_position = pos;
}
}
}
} // namespace
