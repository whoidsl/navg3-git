/**
 * Copyright 2021 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// Created by tmjoyce 08/20/21.
//

#include "TargetSender.h"
#include <NavGMapScene.h>
#include <QMessageBox>
#include <QSettings>

#include <dslmap/Proj.h>

namespace navg {
namespace plugins {
namespace target_sender {

Q_LOGGING_CATEGORY(plugin_target_sender, "navg.plugins.target_sender")

TargetSender::TargetSender(QWidget* parent)
  : QWidget(parent)
  , m_action_show_settings(new QAction("&Settings", this))
{
  socket = new QUdpSocket(this);

  connect(m_action_show_settings,
          &QAction::triggered,
          this,
          &TargetSender::showSettingsDialog);

  m_action_open_remote_dialog =
    new QAction(QIcon{ ":GIS-icons/24x24/move.png" },
                QString{ "&Open Remote Target Dialog..." },
                this);
  m_action_open_remote_dialog->setToolTip("Open Remote Target Dialog");
  connect(m_action_open_remote_dialog,
          &QAction::triggered,
          this,
          &TargetSender ::openTargetDialog);
}

TargetSender::~TargetSender()
{
  delete socket;
}

QList<QAction*>
TargetSender::pluginToolbarActions()
{
  return { m_action_open_remote_dialog };
}

void
TargetSender::setMapView(QPointer<dslmap::MapView> view)
{
  m_view = view;
  n_view = qobject_cast<NavGMapView*>(view);
}

void
TargetSender::connectPlugin(const QObject* plugin, const QJsonObject& metadata)
{
  if (!plugin) {
    return;
  }

  const auto name =
    metadata.value("MetaData").toObject().value("name").toString();
  if (name.isEmpty()) {
    return;
  }
}

void
TargetSender::loadSettings(QSettings* settings)
{
  auto value = settings->value("port");
  auto ok = false;
  if (value.isValid()) {
    const auto valid_val = value.toInt(&ok);
    if (ok) {
      port = valid_val;
    }
  }
  address = settings->value("address").toString();
}

void
TargetSender::saveSettings(QSettings& settings)
{
  settings.setValue("persist", 1);
  settings.setValue("address", address);

  settings.setValue("port", port);
}

QList<QAction*>
TargetSender::pluginMenuActions()
{
  return { m_action_show_settings };
}

void
TargetSender::openTargetDialog()
{
  QScopedPointer<RemoteTargetDialog> widget(new RemoteTargetDialog(this));

  if (n_view) {
    auto m_scene = m_view->mapScene();
    if (m_scene && m_scene->projection()) {
      auto cursor_position = n_view->getCurrentPosition();

      double x = cursor_position.x();
      double y = cursor_position.y();
      m_scene->projection()->transformToLonLat(1, &x, &y);
      cursor_position.setX(x);
      cursor_position.setY(y);
      qDebug(plugin_target_sender) << "Setting cursor position to " << cursor_position;
      widget->setCursorPosition(cursor_position);
    }
  }

  if (widget->exec() == QDialog::Rejected) {
    qCWarning(plugin_target_sender, "qdialog x'd out.");
    return;
  }

  QPointF position = widget->extractPos();
  QString label = widget->extractLabeltext();

  if (position.isNull()) {
    qDebug(plugin_target_sender) << "position is null";
    return;
  }
  qDebug(plugin_target_sender)
    << "Sending to target broadcast function " << position.x() << " "
    << position.y() << " " << label;

  sendTargetMessage(position.x(), position.y(), label);
}

void
TargetSender::sendTargetMessage(double lon, double lat, QString name)
{
  if (address.isEmpty()) {
    qDebug(plugin_target_sender) << "Address is empty";
    return;
  }

  // json good buffers bad >:(
  QJsonObject jsonObj;
  jsonObj["lon"] = lon;
  jsonObj["lat"] = lat;
  jsonObj["name"] = name;
  QString type = "target";
  jsonObj["type"] = type;

  // json formatting example
  // {"lat":9.34,"lon":-84.56333,"name":"mytarget","type":"target"}

  qDebug(plugin_target_sender)
    << "sending target value of "
    << QJsonDocument(jsonObj).toJson(QJsonDocument::Compact);
  const QByteArray data = QJsonDocument(jsonObj).toJson(QJsonDocument::Compact);

  host_address.setAddress(address);
  socket->writeDatagram(data, host_address, port);

  qDebug(plugin_target_sender)
    << "wrote datagram to " << address << ":" << port;
}

void
TargetSender::showSettingsDialog()
{

  QScopedPointer<SenderSettings> dialog(new SenderSettings);

  dialog->setAddress(address);
  dialog->setPort(port);

  dialog->setModal(true);
  if (!dialog->exec()) {
    return;
  }

  address = dialog->getAddress();
  port = dialog->getPort();
}

} // target_sender
} // plugins
} // navg
