/**
 * Copyright 2019 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "SenderSettings.h"
#include "ui_SenderSettings.h"
#include <QDebug>

namespace navg {
namespace plugins {
namespace target_sender {

SenderSettings::SenderSettings(QWidget* parent)
  : QDialog(parent)
  , ui(std::make_unique<Ui::SenderSettings>())
{
  ui->setupUi(this);
  // Setting the max here again
  // UI was returning a smaller value?
  ui->port_spin->setMaximum(10000000);
  connect(ui->port_spin, static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged), [this]() {
    // going to grab the origin from settings, then pass in
    qDebug() << ui->port_spin->value();
  });
}

SenderSettings::~SenderSettings() = default;

void
SenderSettings::setPort(int num)
{
  ui->port_spin->setValue(num);
}
int
SenderSettings::getPort()
{
  return ui->port_spin->value();
}

void 
SenderSettings::setAddress(QString address){
  ui->address_line->setText(address);
}

QString 
SenderSettings::getAddress(){
  return ui->address_line->text();
}

} // namespace target_sender
} // namespace plugins
} // namespace navg
