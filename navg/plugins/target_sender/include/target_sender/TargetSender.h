/**
 * Copyright 2021 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// Created by tmjoyce
//

#ifndef NAVG_TARGETSENDER_H
#define NAVG_TARGETSENDER_H

#include <navg/CorePluginInterface.h>

#include "dslmap/MapView.h"
#include "dslmap/SingleTarget.h"
#include "dslmap/SymbolLayer.h"
#include <QAction>
#include <QHostAddress>
#include <QJsonDocument>
#include <QJsonObject>
#include <QLoggingCategory>
#include <QObject>
#include <QUdpSocket>
#include <QWidget>
#include <memory>

#include "RemoteTargetDialog.h"
#include "SenderSettings.h"

#include <navg/NavGMapView.h>

namespace navg {
namespace plugins {
namespace target_sender {

class TargetSender
  : public QWidget
  , public CorePluginInterface
{
  Q_OBJECT
  Q_INTERFACES(navg::CorePluginInterface)
  Q_PLUGIN_METADATA(IID CorePluginInterface_iid FILE "TargetSender.json")

signals:

public:
  explicit TargetSender(QWidget* parent = nullptr);
  ~TargetSender() override;

  //
  // CorePluginInterface Implementation
  //
  void loadSettings(QSettings* settings) override;
  void saveSettings(QSettings& settings) override;

  // This plugin connects to ....
  void connectPlugin(const QObject* plugin,
                     const QJsonObject& metadata) override;
  void setMapView(QPointer<dslmap::MapView> view) override;
  QList<QAction*> pluginMenuActions() override;

  QList<QAction*> pluginToolbarActions() override;

public slots:
  /// \brief Show the settings dialog
  ///
  void showSettingsDialog();

protected:
protected slots:

  void openTargetDialog();
  void sendTargetMessage(double, double, QString);

private:
  QPointer<dslmap::MapView> m_view;
  QPointer<navg::NavGMapView> n_view;

  QAction* m_action_open_remote_dialog = nullptr;
  QAction* m_action_show_settings = nullptr;

  QHostAddress host_address;
  QString address;
  int port;

  QPointer<QUdpSocket> socket;
};

Q_DECLARE_LOGGING_CATEGORY(plugin_target_sender)
} // target_sender
} // plugins
} // navg

#endif // TARGETSENDER_H
