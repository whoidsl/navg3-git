/**
 * Copyright 2019 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUnamespace dslmap
 * {R OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
//
// Created by tmjoyce on 04/20/21.
//

#include "TestAbortAccessor.h"
#include "catch.hpp"
#include <QLoggingCategory>

using namespace navg::plugins::abort;

SCENARIO("Receive an incoming VFR or Abort Message")
{

  GIVEN("Valid VFR message with an iridium fix source")
  {
    int vfr_id = 3;
    QString vfr_fix_source = "SOLN_IRIDIUM";
    double vfr_lon = -40.212;
    double vfr_lat = 10.343;
    double vfr_depth = 32145;

    THEN("The GUI text should display SURFACE")
    {
      TestAbortAccessor* abort = new TestAbortAccessor();
      abort->handleVfrUpdateProtected(
        vfr_id, vfr_fix_source, vfr_lon, vfr_lat, vfr_depth);

      QString correct_str = "SURFACE"; // expected output
      QString status_text = abort->getStatusProtected();
      REQUIRE(status_text == correct_str);
    }
  }

  GIVEN("Received a True Abort message")
  {

    bool aborted = true;

    THEN("The GUI text should display Aborted")
    {
      TestAbortAccessor* abort = new TestAbortAccessor();
      abort->handleAbortStatusProtected(aborted);

      QString correct_str = "ABORTED"; // expected output
      QString status_text = abort->getStatusProtected();
      REQUIRE(status_text == correct_str);
    }
  }
}
SCENARIO(
  "A series of abort and vfr messages are sent at separate time intervals")
{
  GIVEN("Our Abort plugin exists")
  {
    TestAbortAccessor* abort = new TestAbortAccessor();
    QDateTime current_dt = QDateTime::currentDateTime();

    WHEN("An false abort message comes in")
    {
      abort->handleAbortStatusProtected(false);

      THEN("The display is set to 'OK'") {
        QString status_text = abort->getStatusProtected();
        QString correct_str = "OK"; // expected output
        REQUIRE(status_text == correct_str);
      }

      AND_WHEN("An iridium message is received")
      {
        abort->handleVfrUpdateProtected(
          2, "SOLN_IRIDIUM", 20.43434, -10.43435, 60);

	
	THEN("The display is set to 'SURFACE'")
	{
          QString correct_str = "SURFACE";
          QString status_text = abort->getStatusProtected();
          REQUIRE(status_text.toStdString() == correct_str.toStdString());
	}

	AND_WHEN("Ten seconds pass")
	{
          // simulate 10 seconds passing since irid
          abort->setIridLastReceived(current_dt.addSecs(10));

          //update the timer
          abort->handleTimerProtected();

	  THEN("The display is still says 'SURFACE'")
	  {
            QString correct_str = "SURFACE";
            QString status_text = abort->getStatusProtected();
            REQUIRE(status_text.toStdString() == correct_str.toStdString());
	  }
	}
	AND_WHEN("More than the 'STALE' time passes")
	{
          uint stale = abort->getStaleAge();
          abort->setIridLastReceived(current_dt.addSecs(stale + 2000));
          abort->handleTimerProtected();

	  THEN("The display is STILL says 'SURFACE'")
	  {
            QString correct_str = "SURFACE";
            QString status_text = abort->getStatusProtected();
            REQUIRE(status_text.toStdString() == correct_str.toStdString());
	  }
	}
      }
    }
  }
}
