/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 12/6/18.
//

#ifndef NAVG_ABORT_H
#define NAVG_ABORT_H

#include <navg/CorePluginInterface.h>

#include <QAction>
#include <QLoggingCategory>
#include <QObject>
#include <QWidget>
#include <QDateTime>
#include <QTimer>
#include <memory>

namespace navg {
namespace plugins {
namespace abort {

namespace Ui {
class Abort;
}

class Abort : public QWidget, public CorePluginInterface
{
  Q_OBJECT
  Q_INTERFACES(navg::CorePluginInterface)
  Q_PLUGIN_METADATA(IID CorePluginInterface_iid FILE "Abort.json")

signals:

public:
  explicit Abort(QWidget* parent = nullptr);
  ~Abort() override;
  

  //
  // CorePluginInterface Implementation
  //
  void loadSettings(QSettings* settings) override;
  void saveSettings(QSettings& settings) override;

  // This plugin connects to acomms and navest plugins.
  void connectPlugin(const QObject* plugin,
                     const QJsonObject& metadata) override;
  void setMapView(QPointer<dslmap::MapView> view) override;
  QList<QAction*> pluginMenuActions() override;
  QList<QAction*> pluginToolbarActions() override { return {}; }

protected:
  QString getStatus();

  QDateTime m_time_last_received;
  QDateTime m_time_last_irid;  

  //These const values determine when to mark previous received fixes as "old"
  const uint ACOMMS_OLD_AGE = 60;
  const uint IRID_OLD_AGE = 60;
  const uint ACOMMS_STALE_AGE = 400;
  const uint IRID_STALE_AGE = 400;

protected slots:

  /// \brief Set the latest abort status from the acomms plugin
  ///
  /// \param is_aborting
  void handleAbortStatus(bool is_aborting);
  
  /// \brief Receive vfr update from navest that can contain an iridium fix
  void handleVfrUpdate(int vehicleId, QString fix_source, double lon,
                       double lat, double depth);
  
  /// \brief directly reacts to the signals emitted by the counting timer
  void handleTimer();

  /// \brief updates timer countdown related to abort status
  void updateTimerStatus();
  
  /// \brief updates timer countdown related to whether surface messagers were received
  void updateTimerSurface();


private:

  std::unique_ptr<Ui::Abort> ui;
  QPointer<dslmap::MapView> m_view;
  QScopedPointer<QTimer> timer;
  const static QString format_age(double seconds, double max = 60*60*24*7);
};

Q_DECLARE_LOGGING_CATEGORY(plugin_abort)
} // Abort
} // plugins
} // navg

#endif // NAVG_Abort_H
