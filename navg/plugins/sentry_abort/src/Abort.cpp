/**
 * Copyright 2019 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// Created by jvaccaro 12/6/18.
//

#include "Abort.h"
#include "ui_Abort.h"
#include <dslmap/MapView.h>
#include <QAction>
#include <QSettings>
#include <QUdpSocket>

namespace navg {
namespace plugins {
namespace abort {

Q_LOGGING_CATEGORY(plugin_abort, "navg.plugins.sentry_abort")

Abort::Abort(QWidget* parent)
  : QWidget(parent)
  , ui(std::make_unique<Ui::Abort>())
{
  ui->setupUi(this);
  timer.reset(new QTimer(this));

  connect(timer.data(), &QTimer::timeout, this, &Abort::handleTimer);

  timer->start(1000);
}

Abort::~Abort() = default;

void
Abort::handleTimer()
{
  // Both iridium and acoustic comms have a valid timestamp, update
  // the surface indicator if the iridium timestamp is newer.
  if (!m_time_last_received.isNull() && !m_time_last_irid.isNull()) {
     if (m_time_last_irid >= m_time_last_received) {
       updateTimerSurface();
     }
     else {
       updateTimerStatus();
     }
  }

  // Only the iridium timestamp is valid
  else if (!m_time_last_irid.isNull()) {
    updateTimerSurface();
  }

  // Only the accoms timestamp is valid
  else if (!m_time_last_received.isNull()) {
    updateTimerStatus();
  }
}

const QString
Abort::format_age(double seconds, double max)
{

  if (seconds > max) {
    return QString("---");
  } else if (seconds > 3600) {
    const int hours = seconds / 3600;
    const int minutes = (seconds - (hours * 3600)) / 60;
    const int sec = (seconds - (hours * 3600 + minutes * 60));
    return QString("%1h %2m %3s").arg(hours).arg(minutes).arg(sec);
  } else if (seconds > 120) {
    const int minutes = seconds / 60;
    const int sec = (seconds - minutes * 60);
    return QString("%1m %2s").arg(minutes).arg(sec);
  } else {
    return QString("%1s").arg(seconds, 0, 'd', 0);
  }
}

QString
Abort::getStatus()
{
  return ui->statusLbl->text();
}

void
Abort::updateTimerStatus()
{

  if (m_time_last_received.isNull()) {
    return;
  }

  const auto time_diff =
    QDateTime::currentDateTime().toTime_t() - m_time_last_received.toTime_t();

  if (time_diff <= 0) {
    return;
  }

  ui->sinceLbl->setText(format_age(time_diff));

  // stale age const int is larger value than old age const int
  if (time_diff >= ACOMMS_STALE_AGE) {

    ui->groupBox->setStyleSheet("QGroupBox { border:1px solid grey} ");
    ui->statusLbl->setText("UNKNOWN");
  }

  else if (time_diff >= ACOMMS_OLD_AGE) {
    ui->groupBox->setStyleSheet("QGroupBox { border:1px solid yellow} ");
  }
}

void
Abort::updateTimerSurface()
{

  if (m_time_last_irid.isNull()) {
    return;
  }
  uint time_diff =
    QDateTime::currentDateTime().toTime_t() - m_time_last_irid.toTime_t();

  if (time_diff <= 0) {
    return;
  }

  ui->sinceLbl->setText(format_age(time_diff));

  if (time_diff >= IRID_OLD_AGE) {
    ui->groupBox->setStyleSheet("QGroupBox { border:1px solid yellow} ");
  }
}

void
Abort::setMapView(QPointer<dslmap::MapView> view)
{
  m_view = view;
}

void
Abort::connectPlugin(const QObject* plugin, const QJsonObject& metadata)
{
  if (!plugin) {
    return;
  }

  const auto name =
    metadata.value("MetaData").toObject().value("name").toString();
  if (name.isEmpty()) {
    return;
  }

  if (name == "sentry_acomms") {
    if (!connect(plugin,
                 SIGNAL(abortStatusUpdated(bool)),
                 this,
                 SLOT(handleAbortStatus(bool)))) {
      ui->statusLbl->setText("NOT CONNECTED");
      qCCritical(plugin_abort,
                 "Unable to connect to signal: "
                 "SentryAcomms::abortStatusUpdated("
                 "bool)");
    } else {
      qCInfo(plugin_abort, "Connected to SentryAcomms::abortStatusUpdated");
    }
  }

  if (name == "navest") {
    if (!connect(
          plugin,
          SIGNAL(vfrMessageReceived(int, QString, double, double, double)),
          this,
          SLOT(handleVfrUpdate(int, QString, double, double, double)))) {
      qCCritical(plugin_abort,
                 "Unable to connect to signal: "
                 "Navest::vfrMessageReceived("
                 "int,Qstring,double,double,double)");
    } else {
      qCDebug(plugin_abort, "connected to navest");
    }
  }
}

void
Abort::loadSettings(QSettings*)
{}

void
Abort::saveSettings(QSettings& settings)
{
  settings.setValue("abort_val", 1);
}

QList<QAction*>
Abort::pluginMenuActions()
{
  return {};
}

void
Abort::handleVfrUpdate(int vehicleId,
                       QString fix_source,
                       double lon,
                       double lat,
                       double depth)
{
  if (fix_source != "SOLN_IRIDIUM") {
    return;
  }

  // got iridium fix. so its on surface
  m_time_last_irid = QDateTime::currentDateTime();

  ui->statusLbl->setText("SURFACE");
  ui->groupBox->setStyleSheet(
    "QGroupBox { border:2px solid orange; background-color:darkOrange;} "
    "QGroupBox QLabel { background-color:darkOrange;}");
}

void
Abort::handleAbortStatus(bool is_aborting)
{
  qDebug(plugin_abort) << "Received abort status of " << is_aborting;

  m_time_last_received = QDateTime::currentDateTime();

  if (is_aborting) {
    ui->statusLbl->setText("ABORTED");
    ui->groupBox->setStyleSheet(
      "QGroupBox { border:2px solid red; background-color:darkRed;} "
      "QGroupBox QLabel { background-color:darkRed;}");
  } else {
    ui->statusLbl->setText("OK");
    ui->groupBox->setStyleSheet(
      "QGroupBox { border:1px solid green; background-color:darkGreen;} "
      "QGroupBox QLabel { background-color:darkGreen;}");
  }
}

} // abort
} // plugins
} // navg
