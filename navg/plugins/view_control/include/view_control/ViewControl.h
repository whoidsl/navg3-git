/**
 * Copyright 2021 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// Created by tmjoyce on 06/01/21.
//

#ifndef NAVG_VIEWCONTROL_H
#define NAVG_VIEWCONTROL_H

#include <navg/CorePluginInterface.h>

#include "dslmap/MapView.h"
#include "dslmap/MapLayer.h"
#include <dslmap/SymbolLayer.h>
#include <QLoggingCategory>
#include <QObject>
#include <QWidget>
#include <QHash>
#include <QAction>
#include <memory>

#include <navg/NavGMapView.h>
#include "../../navest/include/navest/NavestLayer.h"

#include "ViewSettings.h"

namespace navg {
namespace plugins {
namespace view_control {

namespace Ui {
class ViewControl;
}

class ViewControl
  : public QWidget
  , public CorePluginInterface
{
  Q_OBJECT
  Q_INTERFACES(navg::CorePluginInterface)
  Q_PLUGIN_METADATA(IID CorePluginInterface_iid FILE "ViewControl.json")

signals:

public:
  explicit ViewControl(QWidget* parent = nullptr);
  ~ViewControl() override;

  //
  // CorePluginInterface Implementation
  //
  void loadSettings(QSettings* settings) override;
  void saveSettings(QSettings& settings) override;

  // This plugin connects to ....
  void connectPlugin(const QObject* plugin,
                     const QJsonObject& metadata) override;
  void setMapView(QPointer<dslmap::MapView> view) override;
  QList<QAction*> pluginMenuActions() override;
  QList<QAction*> pluginToolbarActions() override { return {}; }

public slots:
  /// \brief Zooms in to mapview one step
  ///
  /// \return zoom_level
  int zoomInStep();

  /// \brief Zooms out from mapview one step
  ///
  /// \return zoom_level
  int zoomOutStep();

  void setMouseMode(MOUSE_MODE mode);

  void placeCursorCentered();

  /// \brief Drop cursor on top on vehicle
  void placeCursorVehicle();

  /// \brief Drop target on top of vehicle
  void placeTargetVehicle();

  /// \brief Quick drop target on top of cursor position
  void placeTargetOnCursor();

  void setAlvinXYtoCursor();

  /// \brief Show the settings dialog
  ///
  void showSettingsDialog();

protected:
protected slots:

  void iterateModeVectorIndex();
  void setModeButtonStyle(MOUSE_MODE mode);
  void openTargetDialog();
  void openTargetEditor();

private:
  std::unique_ptr<Ui::ViewControl> ui;
  QPointer<dslmap::MapView> m_view;
  QPointer<navg::NavGMapView> n_view;
  dslmap::SymbolLayer* m_layer;

  std::vector<MOUSE_MODE> modes = { PAN_MODE, CURSOR_MODE, RANGE_MODE };
  int mode_index = 0;
  int vehicle_number;

  QAction* m_action_show_settings = nullptr;
};

Q_DECLARE_LOGGING_CATEGORY(plugin_view_control)
} // view_control
} // plugins
} // navg

#endif // VIEWCONTROL_H
