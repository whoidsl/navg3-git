/**
 * Copyright 2021 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// Created by tmjoyce 06/01/21.
//

#include "ViewControl.h"
#include "ui_ViewControl.h"
#include <NavGMapScene.h>
#include <QMessageBox>
#include <QSettings>
#include <dslmap/Proj.h>

using namespace navg::plugins::navest;

namespace navg {
namespace plugins {
namespace view_control {

Q_LOGGING_CATEGORY(plugin_view_control, "navg.plugins.view_control")

ViewControl::ViewControl(QWidget* parent)
  : QWidget(parent)
  , ui(std::make_unique<Ui::ViewControl>())
  , m_action_show_settings(new QAction("&Settings", this))
{
  ui->setupUi(this);
  vehicle_number = 0;
  setModeButtonStyle(modes[mode_index]);

  connect(m_action_show_settings,
          &QAction::triggered,
          this,
          &ViewControl::showSettingsDialog);
  connect(
    ui->zoom_in_button, &QPushButton::clicked, this, &ViewControl::zoomInStep);
  connect(ui->zoom_out_button,
          &QPushButton::clicked,
          this,
          &ViewControl::zoomOutStep);

  connect(ui->mouse_mode_button, &QPushButton::clicked, [this]() {
    if (!n_view)
      return;
    iterateModeVectorIndex();
    setModeButtonStyle(modes[mode_index]);
    setMouseMode(modes[mode_index]);
  });

  connect(ui->centered_cursor_button,
          &QPushButton::clicked,
          this,
          &ViewControl::placeCursorCentered);

  connect(ui->target_cursor_button,
          &QPushButton::clicked,
          this,
          &ViewControl::placeTargetOnCursor);

  connect(ui->target_menu_button,
          &QPushButton::clicked,
          this,
          &ViewControl::openTargetDialog);

  connect(ui->alvinxy_cursor_button,
          &QPushButton::clicked,
          this,
          &ViewControl::setAlvinXYtoCursor);

  connect(ui->vehicle_cursor_button,
          &QPushButton::clicked,
          this,
          &ViewControl::placeCursorVehicle);
  connect(ui->target_vehicle_button,
          &QPushButton::clicked,
          this,
          &ViewControl::placeTargetVehicle);
  connect(ui->target_editor_button,
          &QPushButton::clicked,
          this,
          &ViewControl::openTargetEditor);
}

ViewControl::~ViewControl()
{
  if (n_view) {
    n_view->setMouseMode(PAN_MODE);
  }
}

void
ViewControl::setMapView(QPointer<dslmap::MapView> view)
{
  m_view = view;
  n_view = qobject_cast<NavGMapView*>(view);
}

void
ViewControl::connectPlugin(const QObject* plugin, const QJsonObject& metadata)
{
  if (!plugin) {
    return;
  }

  const auto name =
    metadata.value("MetaData").toObject().value("name").toString();
  if (name.isEmpty()) {
    return;
  }
}

void
ViewControl::loadSettings(QSettings* settings)
{
  auto value = settings->value("vehicle_fix_number");
  auto ok = false;
  if (value.isValid()) {
    const auto valid_val = value.toInt(&ok);
    if (ok) {
      vehicle_number = valid_val;
    }
  }
}

void
ViewControl::showSettingsDialog()
{

  QScopedPointer<ViewSettings> dialog(new ViewSettings);
  dialog->setFixVehicleNumberBox(vehicle_number);

  dialog->setModal(true);
  if (!dialog->exec()) {
    return;
  }
  vehicle_number = dialog->getFixVehicleNumber();
}

QList<QAction*>
ViewControl::pluginMenuActions()
{
  return { m_action_show_settings };
}

void
ViewControl::saveSettings(QSettings& settings)
{
  settings.setValue("vehicle_fix_number", vehicle_number);
}

int
ViewControl::zoomInStep()
{
  if (!m_view) {
    return 0;
  }
  m_view->zoom(1, true);
  return m_view->zoomLevel();
}

int
ViewControl::zoomOutStep()
{
  if (!m_view) {
    return 0;
  }
  m_view->zoom(-1, true);
  return m_view->zoomLevel();
}

void
ViewControl::setMouseMode(MOUSE_MODE mode)
{

  if (!n_view) {
    return;
  }

  switch (mode) {

    case PAN_MODE:
      n_view->setMouseMode(PAN_MODE);

      break;
    case CURSOR_MODE:
      n_view->setMouseMode(CURSOR_MODE);

      break;
    case RANGE_MODE:
      n_view->setMouseMode(RANGE_MODE);

      break;
    default:
      n_view->setMouseMode(PAN_MODE);

      break;
  }
}

void
ViewControl::placeCursorVehicle(){

  if (!m_view) return;

  if (!n_view) return;

  auto scene = m_view->mapScene();

  if (!scene) return;

  QHash<QUuid, dslmap::MapLayer*> layers = scene->layers(dslmap::MapItemType::NavestLayerType);

  if (layers.size() < 1) return;

  QPointF position;
  for (auto layer : layers.values()){
    NavestLayer* nav = static_cast<NavestLayer*>(layer);
    if (nav){
      if (nav->navestBeaconId() == vehicle_number){
        position = nav->currentPosition();
        break;
      }
    }
  }
  if (position.isNull()) return;

  n_view->setCurrentPosition(position);
  n_view->dropTarget();

}

void
ViewControl::placeTargetVehicle()
{
  if (!m_view) 
    return;

  if (!n_view)
    return;

  auto scene = m_view->mapScene();

  if (!scene)
    return;

  QHash<QUuid, dslmap::MapLayer*> layers =
    scene->layers(dslmap::MapItemType::NavestLayerType);

  if (layers.size() < 1)
    return;
  NavestLayer* nav;
  QPointF position;
  for (auto layer : layers.values()) {
    nav = static_cast<NavestLayer*>(layer);
    if (nav) {
      if (nav->navestBeaconId() == vehicle_number) {
        position = nav->currentPosition();
        break;
      }
    }
  }
  if (position.isNull())
    return;

  n_view->setCurrentPosition(position);
  n_view->dropTarget();

  auto target = n_view->showSingleTargetDialog();
  if (target == nullptr) {
    return;
  }
  // lets add some extra attributes to the target for vehicle info
  // have to comment this heading stuff out. the setAttribute call is bugged and crashes. -TJ
  //double heading = nav->currentHeading();
  //target->setAttribute("Heading", heading);
}

void
ViewControl::placeCursorCentered()
{

  if (!n_view) {
    return;
  }
  QPointF view_center = n_view->viewSceneCenter();
  if (view_center.isNull()){
    return;
  }
  n_view->setCurrentPosition(view_center);
  n_view->dropTarget();
}

void
ViewControl::iterateModeVectorIndex()
{
  if (modes.empty()) {
    return;
  }
  mode_index = (mode_index + 1) % modes.size();
}

void
ViewControl::setModeButtonStyle(MOUSE_MODE mode)
{
  switch (mode) {

    case PAN_MODE:
      ui->mouse_mode_button->setText("Mouse Mode: Pan");
      ui->mouse_mode_button->setStyleSheet(
        "QPushButton { border:1px solid white} ");
      ui->mouse_mode_button->setToolTip("Click and hold to pan the map view");  

      break;
    case CURSOR_MODE:
      ui->mouse_mode_button->setText("Mouse Mode: Cursor");
      ui->mouse_mode_button->setStyleSheet(
        "QPushButton { border:1px solid red} ");
      ui->mouse_mode_button->setToolTip("Drop the cursor where you click");  

      break;
    case RANGE_MODE:
      ui->mouse_mode_button->setText("Mouse Mode: Range");
      ui->mouse_mode_button->setStyleSheet(
        "QPushButton { border:1px solid blue} ");
      ui->mouse_mode_button->setToolTip("Click and hold on map to calculate range and bearing");  

      break;
    default:
      ui->mouse_mode_button->setText("Mouse Mode: Pan");
      ui->mouse_mode_button->setStyleSheet(
        "QPushButton { border:1px solid white} ");
      ui->mouse_mode_button->setToolTip("Click and hold to pan the map view");  

      break;
  }
}

void
ViewControl::placeTargetOnCursor()
{
  if (!n_view) {
    return;
  }

  auto cursor_pos = n_view->getCurrentPosition();

  if (cursor_pos.isNull()) {
    return;
  }

  n_view->placeTargetAtPosition(cursor_pos, "Cursor_Target_Layer");
}

void
ViewControl::openTargetDialog()
{
  if (!n_view) {
    return;
  }
  n_view->showSingleTargetDialog();
}

void
ViewControl::openTargetEditor()
{
  if(!n_view) {
    return;
  }
  std::vector<qreal> z_vec;
  int max_z=0;
  m_layer = new dslmap::SymbolLayer();
  auto scene = qobject_cast<dslmap::MapScene*>(n_view->mapScene());
  QHash<QUuid, dslmap::MapLayer*> layers = scene->layers();
  if (layers.count() >=1) {
    for (auto layer : layers) {
      //qDebug()<<"layer is "<<layer->name();
      z_vec.push_back(layer->toGraphicsObject()->zValue());
    }
    max_z = *max_element(z_vec.begin(), z_vec.end());
    for (auto layer : layers) {
      auto symbollayer = qobject_cast<dslmap::SymbolLayer*>(layer);
      if (symbollayer) {
        if (layer->toGraphicsObject()->zValue() == max_z) {
          symbollayer->editLatLonLabels();
        }
      }
    }
  }
}
void
ViewControl::setAlvinXYtoCursor()
{
  if (!n_view) {
    return;
  }

  auto scene = qobject_cast<NavGMapScene*>(n_view->mapScene());
  if (!scene) {
    return;
  }

  auto cursor_pos = n_view->getCurrentPosition();

  if (cursor_pos.isNull()) {
    return;
  }
  double cursor_x = cursor_pos.x();
  double cursor_y = cursor_pos.y();

  const auto scene_proj = scene->projectionPtr();
  if (!scene_proj) return;
  if (!scene_proj->isLatLon()) {
    scene_proj->transformToLonLat(1, &cursor_x, &cursor_y, true);
  }

  const int result = QMessageBox::warning(
    this,
    "Set Alvin XY Origin",
    QString(
      "You are about to set the AlvinXY Origin to to %1 lon %2 lat.  Proceed?")
      .arg(cursor_x)
      .arg(cursor_y),
    QMessageBox::Ok | QMessageBox::Cancel,
    QMessageBox::Cancel);
  if (result == QMessageBox::Ok) {
    qDebug(plugin_view_control)
      << "Setting AlvinXY origin to " << QString::number(cursor_x, 'f', 8)
      << " " << QString::number(cursor_y, 'f', 8);
    scene->setAlvinXYOrigin(QPointF(cursor_x, cursor_y));
  }
}

} // view_control
} // plugins
} // navg
