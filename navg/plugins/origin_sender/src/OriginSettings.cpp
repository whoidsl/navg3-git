/**
 * Copyright 2019 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "OriginSettings.h"
#include "ui_OriginSettings.h"

namespace navg {
namespace plugins {
namespace origin_sender {

OriginSettings::OriginSettings(QWidget* parent)
  : QDialog(parent)
 , ui(std::make_unique<Ui::OriginSettings>())
{
  ui->setupUi(this);
}

OriginSettings::~OriginSettings() = default;

void
OriginSettings::setFixVehicleNumberBox(int num)
{
  //ui->fix_vehicle_spin->setValue(num);
}
void
OriginSettings::setip1(QString ip)
{
  ui->address1->setText(ip);
}
void
OriginSettings::setip2(QString ip)
{
  ui->address2->setText(ip);
}
void
OriginSettings::setip3(QString ip)
{
  ui->address3->setText(ip);
}

void
OriginSettings::setport1(int port)
{
  ui->port1->setValue(port);
}
void
OriginSettings::setport2(int port)
{
  ui->port2->setValue(port);
}
void
OriginSettings::setport3(int port)
{
  ui->port3->setValue(port);
}

QString
OriginSettings::getIp1()
{
  return ui->address1->text();
}
int
OriginSettings::getPort1()
{
  return ui->port1->value();
}

QString
OriginSettings::getIp2()
{
  return ui->address2->text();
}
int
OriginSettings::getPort2()
{
  return ui->port2->value();
}

QString
OriginSettings::getIp3()
{
  return ui->address3->text();
}
int
OriginSettings::getPort3()
{
  return ui->port3->value();
}

} // namespace origin_settings
} // namespace plugins
} // namespace navg
