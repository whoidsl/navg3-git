/**
 * Copyright 2021 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// Created by tmjoyce 08/09/21.
//

#include "OriginSender.h"
#include "ui_OriginSender.h"
#include <QSettings>
#include <QFileInfo>
#include <errno.h>
#include <sys/stat.h>

namespace navg {
namespace plugins {
namespace origin_sender {

Q_LOGGING_CATEGORY(plugin_origin_sender, "navg.plugins.origin_sender")

OriginSender::OriginSender(QWidget* parent)
  : QWidget(parent)
  , ui(std::make_unique<Ui::OriginSender>())
  , m_action_show_settings(new QAction("&Settings", this))
{
  ui->setupUi(this);

  connect(m_action_show_settings,
          &QAction::triggered,
          this,
          &OriginSender::showSettingsDialog);

  connect(ui->origin_button, &QPushButton::clicked, [this]() {
    this->postOrigin(ip1, port1);
    this->postOrigin(ip2, port2);
    this->postOrigin(ip3, port3);
  });
  connect(ui->bathy_button, &QPushButton::clicked, [this]() {
    this->postBathy(ip1, port1);
    this->postBathy(ip2, port2);
    this->postBathy(ip3, port3);
  });
  connect(ui->contour_button, &QPushButton::clicked, [this]() {
    this->postContour(ip1, port1);
    this->postContour(ip2, port2);
    this->postContour(ip3, port3);
    this->postContourSpacing(ip1, port1);
    this->postContourSpacing(ip2, port2);
    this->postContourSpacing(ip3, port3);
  });
  connect(ui->image_button, &QPushButton::clicked, [this]() {
    this->postImage(ip1, port1);
    this->postImage(ip2, port2);
    this->postImage(ip3, port3);
    this->postImageDimensions(ip1, port1);
    this->postImageDimensions(ip2, port2);
    this->postImageDimensions(ip3, port3);
  });
  connect(ui->geoimg_button, &QPushButton::clicked, [this]() {
    this->postGeoImage(ip1, port1);
    this->postGeoImage(ip2, port2);
    this->postGeoImage(ip3, port3);
  });
  connect(ui->start_button, &QPushButton::clicked, [this]() {
    this->postStartPoint(ip1, port1);
    this->postStartPoint(ip2, port2);
    this->postStartPoint(ip3, port3);
  });
  connect(ui->vehicle_name_button, &QPushButton::clicked, [this]() {
    this->postVehicleName(ip1, port1);
    this->postVehicleName(ip2, port2);
    this->postVehicleName(ip3, port3);
  });
  connect(ui->dive_number_button, &QPushButton::clicked, [this]() {
    this->postDiveNumber(ip1, port1);
    this->postDiveNumber(ip2, port2);
    this->postDiveNumber(ip3, port3);
  });
  connect(ui->clear_button, &QPushButton::clicked, [this]() {
    this->postUnderlayClear(ip1, port1);
    this->postUnderlayClear(ip2, port2);
    this->postUnderlayClear(ip3, port3);
  });
  connect(ui->target_clear_button, &QPushButton::clicked, [this]() {
    this->postTargetUnderlayClear(ip1, port1);
    this->postTargetUnderlayClear(ip2, port2);
    this->postTargetUnderlayClear(ip3, port3);
  });

  connect(ui->targets_button, &QPushButton::clicked, [this]() {
    this->postTargets(ip1, port1);
    this->postTargets(ip2, port2);
    this->postTargets(ip3, port3);
  });
}

OriginSender::~OriginSender() = default;

void
OriginSender::setMapView(QPointer<dslmap::MapView> view)
{
  m_view = view;
  n_view = qobject_cast<NavGMapView*>(view);

  if (m_view) {
    m_scene = view->mapScene();
    n_scene = qobject_cast<NavGMapScene*>(m_scene);
  }
}

void
OriginSender::connectPlugin(const QObject* plugin, const QJsonObject& metadata)
{
  if (!plugin) {
    return;
  }

  const auto name =
    metadata.value("MetaData").toObject().value("name").toString();
  if (name.isEmpty()) {
    return;
  }
}

void
OriginSender::loadSettings(QSettings* settings)
{

  auto value = settings->value("port1");
  auto ok = false;
  if (value.isValid()) {
    const auto valid_val = value.toInt(&ok);
    if (ok) {
      port1 = valid_val;
    }
  }
  value = settings->value("port2");
  ok = false;
  if (value.isValid()) {
    const auto valid_val = value.toInt(&ok);
    if (ok) {
      port2 = valid_val;
    }
  }
  value = settings->value("port3");
  ok = false;
  if (value.isValid()) {
    const auto valid_val = value.toInt(&ok);
    if (ok) {
      port3 = valid_val;
    }
  }

  value = settings->value("timeout_seconds");
  ok = false;
  if (value.isValid()) {
    const auto valid_val = value.toInt(&ok);
    if (ok) {
      request_timeout_seconds = valid_val;
    }
  }

  ip1 = settings->value("ip1").toString();
  ip2 = settings->value("ip2").toString();
  ip3 = settings->value("ip3").toString();
}

void
OriginSender::saveSettings(QSettings& settings)
{
  settings.setValue("persist", 1);
  settings.setValue("ip1", ip1);
  settings.setValue("ip2", ip2);
  settings.setValue("ip3", ip3);

  settings.setValue("port1", port1);
  settings.setValue("port2", port2);
  settings.setValue("port3", port3);

  settings.setValue("timeout_seconds", request_timeout_seconds);
}

void
OriginSender::showSettingsDialog()
{

  QScopedPointer<OriginSettings> dialog(new OriginSettings);
  // dialog->setFixVehicleNumberBox(vehicle_number);
  dialog->setip1(ip1);
  dialog->setip2(ip2);
  dialog->setip3(ip3);

  dialog->setport1(port1);
  dialog->setport2(port2);
  dialog->setport3(port3);

  dialog->setModal(true);
  if (!dialog->exec()) {
    return;
  }
  // vehicle_number = dialog->getFixVehicleNumber();

  ip1 = dialog->getIp1();
  port1 = dialog->getPort1();

  ip2 = dialog->getIp2();
  port2 = dialog->getPort2();

  ip3 = dialog->getIp3();
  port3 = dialog->getPort3();
}

QList<QAction*>
OriginSender::pluginMenuActions()
{
  return { m_action_show_settings };
}

size_t
OriginSender::WriteCallback(void* contents,
                            size_t size,
                            size_t nmemb,
                            void* userp)
{
  ((std::string*)userp)->append((char*)contents, size * nmemb);
  return size * nmemb;
}

int
OriginSender::postOrigin(QString ip, int port)
{
  if (ip.isEmpty())
    return -1;
  auto address = QString("http://" + ip + "/api/v1/origin");
  // lets populate our origin and underlay settings
  setupData();
  if (origin_lon.isEmpty() || origin_lat.isEmpty()) {
    QMessageBox msgBox;
    msgBox.setText(address + ":" + QString::number(port) +
                   ":\nNo origin to send.");
    msgBox.exec();
    // add alert to show user the error
    return -1;
  }
  curl_global_init(CURL_GLOBAL_DEFAULT);
  auto c = curl_easy_init();

  if (!c) {
    // curls couldnt init?
  } else {

    struct curl_slist* headers = NULL;
    headers = curl_slist_append(headers, "Content-Type: application/json");
    headers = curl_slist_append(headers, "Accept: application/json");
    std::string response_string;

    QJsonObject jsonObj;
    jsonObj["lon"] = origin_lon;
    jsonObj["lat"] = origin_lat;

    qDebug() << jsonObj;

    QJsonDocument jsonDoc(jsonObj);
    QString jsonString = jsonDoc.toJson();

    curl_easy_setopt(c, CURLOPT_URL, address.toLatin1().data());
    curl_easy_setopt(c, CURLOPT_PORT, port);
    curl_easy_setopt(c, CURLOPT_HTTPHEADER, headers);
    curl_easy_setopt(c, CURLOPT_TIMEOUT, request_timeout_seconds);

    curl_easy_setopt(c, CURLOPT_POST, 1L);
    curl_easy_setopt(c, CURLOPT_COPYPOSTFIELDS, jsonString.toLatin1().data());
    curl_easy_setopt(c, CURLOPT_WRITEFUNCTION, WriteCallback);
    curl_easy_setopt(c, CURLOPT_WRITEDATA, &response_string);
    /// curl_easy_setopt(c, CURLOPT_VERBOSE, 1L);

    CURLcode res = curl_easy_perform(c);
    curl_slist_free_all(headers);

    if (res != CURLE_OK) {

      if (res == 28) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nConnection timeout.");
        msgBox.exec();
      } else if (res == 7) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nServer Unreachable.");
        msgBox.exec();
      } else {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nUnknown Server Error:" + QString::number(res));
        msgBox.exec();
      }
    }

    else {
      long responseCode;
      curl_easy_getinfo(c, CURLINFO_RESPONSE_CODE, &responseCode);
      qDebug() << "response is" << QString::number(responseCode);
      qDebug() << "response data is is"
               << QString::fromStdString(response_string);
      if (responseCode == 665) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nError writing files.");
        msgBox.exec();
      } else if (responseCode == 664) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nWrong request format.");
        msgBox.exec();
      } else if (responseCode == 663) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nCannot handle request type.");
        msgBox.exec();
      } else if (responseCode == 200) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nData sent successfully.");
        msgBox.exec();

      } else if (responseCode == 667) {
        QMessageBox msgBox;
        msgBox.setText(
          address + ":" + QString::number(port) +
          ":\nAlvin cc ini not set. Other origins written successfully.");
        msgBox.exec();
      } else {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nUnknown Server Error:" + QString::number(res));
        msgBox.exec();
      }
    }

    curl_easy_cleanup(c);
  }
  return 1;
}

int
OriginSender::postContourSpacing(QString ip, int port)
{
  if (ip.isEmpty())
    return -1;
  auto address = QString("http://" + ip + "/api/v1/contourspacing");
  // lets populate our origin and underlay settings
  setupData();
  qDebug() << "contour spacing is " << contour_spacing;
  if (contour_spacing <= 0) {
    QMessageBox msgBox;
    msgBox.setText(address + ":" + QString::number(port) +
                   ":\nNo contour spacing to send.");
    msgBox.exec();
    // add alert to show user the error
    return -1;
  }
  curl_global_init(CURL_GLOBAL_DEFAULT);
  auto c = curl_easy_init();

  if (!c) {
    // curls couldnt init?
  } else {

    struct curl_slist* headers = NULL;
    headers = curl_slist_append(headers, "Content-Type: application/json");
    headers = curl_slist_append(headers, "Accept: application/json");
    std::string response_string;

    QJsonObject jsonObj;
    jsonObj["spacing"] = contour_spacing;

    qDebug() << jsonObj;

    QJsonDocument jsonDoc(jsonObj);
    QString jsonString = jsonDoc.toJson();

    curl_easy_setopt(c, CURLOPT_URL, address.toLatin1().data());
    curl_easy_setopt(c, CURLOPT_PORT, port);
    curl_easy_setopt(c, CURLOPT_HTTPHEADER, headers);
    curl_easy_setopt(c, CURLOPT_TIMEOUT, request_timeout_seconds);

    curl_easy_setopt(c, CURLOPT_POST, 1L);
    curl_easy_setopt(c, CURLOPT_COPYPOSTFIELDS, jsonString.toLatin1().data());
    curl_easy_setopt(c, CURLOPT_WRITEFUNCTION, WriteCallback);
    curl_easy_setopt(c, CURLOPT_WRITEDATA, &response_string);
    /// curl_easy_setopt(c, CURLOPT_VERBOSE, 1L);

    CURLcode res = curl_easy_perform(c);
    curl_slist_free_all(headers);

    if (res != CURLE_OK) {

      if (res == 28) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nConnection timeout.");
        msgBox.exec();
      } else if (res == 7) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nServer Unreachable.");
        msgBox.exec();
      } else {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nUnknown Server Error.");
        msgBox.exec();
      }
    }

    else {
      long responseCode;
      curl_easy_getinfo(c, CURLINFO_RESPONSE_CODE, &responseCode);
      qDebug() << "response is" << QString::number(responseCode);
      qDebug() << "response data is is"
               << QString::fromStdString(response_string);
      if (responseCode == 665) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nError writing files.");
        msgBox.exec();
      } else if (responseCode == 664) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nWrong request format.");
        msgBox.exec();
      } else if (responseCode == 663) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nCannot handle request type.");
        msgBox.exec();
      } else if (responseCode == 200) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nData sent successfully.");
        msgBox.exec();
      } else {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nUnknown Server Error.");
        msgBox.exec();
      }
    }

    curl_easy_cleanup(c);
  }
  return 1;
}

int
OriginSender::postImageDimensions(QString ip, int port)
{
  if (ip.isEmpty())
    return -1;
  auto address = QString("http://" + ip + "/api/v1/imagecoords");
  // lets populate our origin and underlay settings
  setupData();
  if (image_coords == "0.0000000,0.0000000,0.0000000,0.0000000" || image_coords.isEmpty()) {
    QMessageBox msgBox;
    msgBox.setText(address + ":" + QString::number(port) +
                   ":\nNo image dimensions to send.");
    msgBox.exec();
    // add alert to show user the error
    return -1;
  }
  curl_global_init(CURL_GLOBAL_DEFAULT);
  auto c = curl_easy_init();

  if (!c) {
    // curls couldnt init?
  } else {
    qDebug() << "before sending....image coord string is " << image_coords;
    struct curl_slist* headers = NULL;
    headers = curl_slist_append(headers, "Content-Type: application/json");
    headers = curl_slist_append(headers, "Accept: application/json");
    std::string response_string;

    QJsonObject jsonObj;
    jsonObj["coords"] = image_coords;

    qDebug() << jsonObj;

    QJsonDocument jsonDoc(jsonObj);
    QString jsonString = jsonDoc.toJson();

    curl_easy_setopt(c, CURLOPT_URL, address.toLatin1().data());
    curl_easy_setopt(c, CURLOPT_PORT, port);
    curl_easy_setopt(c, CURLOPT_HTTPHEADER, headers);
    curl_easy_setopt(c, CURLOPT_TIMEOUT, request_timeout_seconds);

    curl_easy_setopt(c, CURLOPT_POST, 1L);
    curl_easy_setopt(c, CURLOPT_COPYPOSTFIELDS, jsonString.toLatin1().data());
    curl_easy_setopt(c, CURLOPT_WRITEFUNCTION, WriteCallback);
    curl_easy_setopt(c, CURLOPT_WRITEDATA, &response_string);
    /// curl_easy_setopt(c, CURLOPT_VERBOSE, 1L);

    CURLcode res = curl_easy_perform(c);
    curl_slist_free_all(headers);

    if (res != CURLE_OK) {

      if (res == 28) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nConnection timeout.");
        msgBox.exec();
      } else if (res == 7) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nServer Unreachable.");
        msgBox.exec();
      } else {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nUnknown Server Error.");
        msgBox.exec();
      }
    }

    else {
      long responseCode;
      curl_easy_getinfo(c, CURLINFO_RESPONSE_CODE, &responseCode);
      qDebug() << "response is" << QString::number(responseCode);
      qDebug() << "response data is is"
               << QString::fromStdString(response_string);
      if (responseCode == 665) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nError writing files.");
        msgBox.exec();
      } else if (responseCode == 664) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nWrong request format.");
        msgBox.exec();
      } else if (responseCode == 663) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nCannot handle request type.");
        msgBox.exec();
      } else if (responseCode == 200) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nData sent successfully.");
        msgBox.exec();
      } else {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nUnknown Server Error.");
        msgBox.exec();
      }
    }

    curl_easy_cleanup(c);
  }
  return 1;
}

int
OriginSender::postStartPoint(QString ip, int port)
{
  if (ip.isEmpty())
    return -1;
  auto address = QString("http://" + ip + "/api/v1/startcoords");
  // lets populate our origin and underlay settings
  setupData();
  if (start_lon.isEmpty() || start_lat.isEmpty()) {
    // add alert to show user the error
    QMessageBox msgBox;
    msgBox.setText(address + ":" + QString::number(port) +
                   ":\nNo start latlon to send.");
    msgBox.exec();
    return -1;
  }
  curl_global_init(CURL_GLOBAL_DEFAULT);
  auto c = curl_easy_init();

  if (!c) {
    // curls couldnt init?
  } else {

    struct curl_slist* headers = NULL;
    headers = curl_slist_append(headers, "Content-Type: application/json");
    headers = curl_slist_append(headers, "Accept: application/json");
    std::string response_string;
 
    QJsonObject jsonObj;

    jsonObj["lon"] = start_lon;
    jsonObj["lat"] = start_lat;

    qDebug() << jsonObj;

    QJsonDocument jsonDoc(jsonObj);
    QString jsonString = jsonDoc.toJson();

    curl_easy_setopt(c, CURLOPT_URL, address.toLatin1().data());
    curl_easy_setopt(c, CURLOPT_PORT, port);
    // Header http type request
    curl_easy_setopt(c, CURLOPT_HTTPHEADER, headers);
    curl_easy_setopt(c, CURLOPT_TIMEOUT, request_timeout_seconds);
  
    curl_easy_setopt(c, CURLOPT_POST, 1L);

    curl_easy_setopt(c, CURLOPT_COPYPOSTFIELDS, jsonString.toLatin1().data());
    curl_easy_setopt(c, CURLOPT_WRITEFUNCTION, WriteCallback);
    curl_easy_setopt(c, CURLOPT_WRITEDATA, &response_string);

    CURLcode res = curl_easy_perform(c);
    curl_slist_free_all(headers);

    if (res != CURLE_OK) {

      if (res == 28) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nConnection timeout.");
        msgBox.exec();
      } else if (res == 7) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nServer Unreachable.");
        msgBox.exec();
      } else {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nUnknown Server Error.");
        msgBox.exec();
      }
    }

    else {
      long responseCode;
      curl_easy_getinfo(c, CURLINFO_RESPONSE_CODE, &responseCode);
      qDebug() << "response is" << QString::number(responseCode);
      qDebug() << "response data is is"
               << QString::fromStdString(response_string);
      if (responseCode == 665) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nError writing files.");
        msgBox.exec();
      } else if (responseCode == 664) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nWrong request format.");
        msgBox.exec();
      } else if (responseCode == 663) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nCannot handle request type.");
        msgBox.exec();
      } else if (responseCode == 200) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nData sent successfully.");
        msgBox.exec();
      } else {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nUnknown Server Error.");
        msgBox.exec();
      }
    }

    curl_easy_cleanup(c);
  }
  return 1;
}
int
OriginSender::postBathy(QString ip, int port)
{
  if (ip.isEmpty())
    return -1;
  QString address = QString("http://" + ip + "/api/v1/bathyunderlay");
  // lets populate our origin and underlay settings
  errno = 0;
  setupData();
  if (bathy_path.isEmpty()) {
    // add alert to show user the error
    QMessageBox msgBox;
    msgBox.setText(address + ":" + QString::number(port) +
                   ":\nNo bathy to send.");
    msgBox.exec();
    qDebug() << "bathy path is empty";
    return -1;
  }

  qDebug() << "dive number is  " << dive_number;
  if (dive_number <= 0) {
    QMessageBox msgBox;
    msgBox.setText(address + ":" + QString::number(port) +
                   ":\nNo valid dive number to send.");
    msgBox.exec();
    // add alert to show user the error
    return -1;
  }
  qDebug() << "the bathy path is " << bathy_path;
  // const char* path = bathy_path.toStdString().c_str();
  QFileInfo fi(bathy_path);
  QString ext = "Extension: " + fi.suffix();

  QString dive = "Dive: " + QString::number(dive_number);

  FILE* fd;

  QByteArray ba = bathy_path.toLatin1();
  char* file = ba.data();

  fd = fopen(file, "rb");
  if (!fd) {
    fprintf(stderr, "Could not open file.\n");
    qDebug() << "could not open file";
    qDebug() << "error num is " << errno;
    return -1;
  }

  struct stat st;
  stat(file, &st);
  int size = st.st_size;

  curl_global_init(CURL_GLOBAL_DEFAULT);
  auto c = curl_easy_init();

  if (!c) {
    // curls couldnt init?
  } else {

    struct curl_slist* headers = NULL;
    headers =
      curl_slist_append(headers, "Content-Type: application/octet-stream");
    headers = curl_slist_append(headers, "Accept: application/octet-stream");
    headers = curl_slist_append(headers, ext.toLatin1().data());
    headers = curl_slist_append(headers, dive.toLatin1().data());
    std::string response_string;

    curl_easy_setopt(c, CURLOPT_URL, address.toLatin1().data());
    curl_easy_setopt(c, CURLOPT_PORT, port);
    curl_easy_setopt(c, CURLOPT_READDATA, fd);
    // Header http type request
    curl_easy_setopt(c, CURLOPT_HTTPHEADER, headers);
    curl_easy_setopt(c, CURLOPT_TIMEOUT, request_timeout_seconds);
    curl_easy_setopt(c, CURLOPT_POSTFIELDSIZE, size);
    /* Specify request type. Get in this example, post in others */
    curl_easy_setopt(c, CURLOPT_POST, 1L);

    curl_easy_setopt(c, CURLOPT_WRITEFUNCTION, WriteCallback);
    curl_easy_setopt(c, CURLOPT_WRITEDATA, &response_string);

    CURLcode res = curl_easy_perform(c);
    curl_slist_free_all(headers);

    if (res != CURLE_OK) {

      if (res == 28) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nConnection timeout.");
        msgBox.exec();
      } else if (res == 7) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nServer Unreachable.");
        msgBox.exec();
      } else {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nUnknown Server Error.");
        msgBox.exec();
      }
    }

    else {
      long responseCode;
      curl_easy_getinfo(c, CURLINFO_RESPONSE_CODE, &responseCode);
      qDebug() << "response is" << QString::number(responseCode);
      qDebug() << "response data is is"
               << QString::fromStdString(response_string);
      if (responseCode == 665) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nError writing files.");
        msgBox.exec();
      } else if (responseCode == 664) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nWrong request format.");
        msgBox.exec();
      } else if (responseCode == 663) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nCannot handle request type.");
        msgBox.exec();
      } else if (responseCode == 200) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nData sent successfully.");
        msgBox.exec();
      } else {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nUnknown Server Error.");
        msgBox.exec();
      }
    }

    curl_easy_cleanup(c);
  }
  return 1;
}
int
OriginSender::postContour(QString ip, int port)
{
  if (ip.isEmpty())
    return -1;
  auto address = QString("http://" + ip + "/api/v1/contourunderlay");
  // lets populate our origin and underlay settings
  errno = 0;
  setupData();
  if (contour_path.isEmpty()) {
    QMessageBox msgBox;
    msgBox.setText(address + ":" + QString::number(port) +
                   ":\nNo contour layer to send.");
    msgBox.exec();
    // add alert to show user the error
    qDebug() << "contour path is empty";
    return -1;
  }
  qDebug() << "dive number is  " << dive_number;
  if (dive_number <= 0) {
    QMessageBox msgBox;
    msgBox.setText(address + ":" + QString::number(port) +
                   ":\nNo valid dive number to send.");
    msgBox.exec();
    // add alert to show user the error
    return -1;
  }
  qDebug() << "the contour path is " << contour_path;
  // const char* path = bathy_path.toStdString().c_str();
  QFileInfo fi(contour_path);
  QString ext = "Extension: " + fi.suffix();

  QString dive = "Dive: " + QString::number(dive_number);
  FILE* fd;
  // struct stat file_info;

  QByteArray ba = contour_path.toLatin1();
  char* file = ba.data();

  qDebug() << "char star path is " << file;
  // asprintf(&file, "%s/", path);

  fd = fopen(file, "rb");
  if (!fd) {
    fprintf(stderr, "Could not open file.\n");
    qDebug() << "could not open file";
    qDebug() << "error num is " << errno;
    return -1;
  }

  struct stat st;
  stat(file, &st);
  int size = st.st_size;

  curl_global_init(CURL_GLOBAL_DEFAULT);
  auto c = curl_easy_init();
 
  if (!c) {
    // curls couldnt init?
  } else {

    struct curl_slist* headers = NULL;
    headers =
      curl_slist_append(headers, "Content-Type: application/octet-stream");
    headers = curl_slist_append(headers, "Accept: application/octet-stream");
    headers = curl_slist_append(headers, ext.toLatin1().data());
    headers = curl_slist_append(headers, dive.toLatin1().data());
    std::string response_string;

    curl_easy_setopt(c, CURLOPT_URL, address.toLatin1().data());
    curl_easy_setopt(c, CURLOPT_PORT, port);
    curl_easy_setopt(c, CURLOPT_READDATA, fd);
    // Header http type request
    curl_easy_setopt(c, CURLOPT_HTTPHEADER, headers);
    curl_easy_setopt(c, CURLOPT_TIMEOUT, request_timeout_seconds);
    curl_easy_setopt(c, CURLOPT_POSTFIELDSIZE, size);
    /* Specify request type. Get in this example, post in others */
    curl_easy_setopt(c, CURLOPT_POST, 1L);

    curl_easy_setopt(c, CURLOPT_WRITEFUNCTION, WriteCallback);
    curl_easy_setopt(c, CURLOPT_WRITEDATA, &response_string);

    CURLcode res = curl_easy_perform(c);
    curl_slist_free_all(headers);

    if (res != CURLE_OK) {

      if (res == 28) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nConnection timeout.");
        msgBox.exec();
      } else if (res == 7) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nServer Unreachable.");
        msgBox.exec();
      } else {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nUnknown Server Error.");
        msgBox.exec();
      }
    }

    else {
      long responseCode;
      curl_easy_getinfo(c, CURLINFO_RESPONSE_CODE, &responseCode);
      qDebug() << "response is" << QString::number(responseCode);
      qDebug() << "response data is is"
               << QString::fromStdString(response_string);
      if (responseCode == 665) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nError writing files.");
        msgBox.exec();
      } else if (responseCode == 664) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nWrong request format.");
        msgBox.exec();
      } else if (responseCode == 663) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nCannot handle request type.");
        msgBox.exec();
      } else if (responseCode == 200) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nData sent successfully.");
        msgBox.exec();
      } else {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nUnknown Server Error.");
        msgBox.exec();
      }
    }

    curl_easy_cleanup(c);
  }
  return 1;
}
int
OriginSender::postImage(QString ip, int port)
{
  if (ip.isEmpty())
    return -1;
  auto address = QString("http://" + ip + "/api/v1/imageunderlay");
  // lets populate our origin and underlay settings
  errno = 0;
  setupData();
  if (image_path.isEmpty()) {
    QMessageBox msgBox;
    msgBox.setText(address + ":" + QString::number(port) +
                   ":\nNo image to send.");
    msgBox.exec();
    // add alert to show user the error
    qDebug() << "image path is empty";
    return -1;
  }
  qDebug() << "dive number is  " << dive_number;
  if (dive_number <= 0) {
    QMessageBox msgBox;
    msgBox.setText(address + ":" + QString::number(port) +
                   ":\nNo valid dive number to send.");
    msgBox.exec();
    // add alert to show user the error
    return -1;
  }
  qDebug() << "the bathy path is " << bathy_path;
  // const char* path = bathy_path.toStdString().c_str();

  QFileInfo fi(image_path);
  QString ext = "Extension: " + fi.suffix();

  QString dive = "Dive: " + QString::number(dive_number);

  FILE* fd;
  // struct stat file_info;

  QByteArray ba = image_path.toLatin1();
  char* file = ba.data();

  qDebug() << "char star path is " << file;
  // asprintf(&file, "%s/", path);

  fd = fopen(file, "rb");
  if (!fd) {
    fprintf(stderr, "Could not open file.\n");
    qDebug() << "could not open file";
    qDebug() << "error num is " << errno;
    return -1;
  }

  struct stat st;
  stat(file, &st);
  int size = st.st_size;

  curl_global_init(CURL_GLOBAL_DEFAULT);
  auto c = curl_easy_init();

  if (!c) {
    // curls couldnt init?
  } else {

    struct curl_slist* headers = NULL;
    headers =
      curl_slist_append(headers, "Content-Type: application/octet-stream");
    headers = curl_slist_append(headers, "Accept: application/octet-stream");
    headers = curl_slist_append(headers, ext.toLatin1().data());
    headers = curl_slist_append(headers, dive.toLatin1().data());
    std::string response_string;

    curl_easy_setopt(c, CURLOPT_URL, address.toLatin1().data());
    curl_easy_setopt(c, CURLOPT_PORT, port);
    curl_easy_setopt(c, CURLOPT_READDATA, fd);
    // Header http type request
    curl_easy_setopt(c, CURLOPT_HTTPHEADER, headers);
    curl_easy_setopt(c, CURLOPT_TIMEOUT, request_timeout_seconds);
    curl_easy_setopt(c, CURLOPT_POSTFIELDSIZE, size);
    /* Specify request type. Get in this example, post in others */
    curl_easy_setopt(c, CURLOPT_POST, 1L);

    curl_easy_setopt(c, CURLOPT_WRITEFUNCTION, WriteCallback);
    curl_easy_setopt(c, CURLOPT_WRITEDATA, &response_string);

    CURLcode res = curl_easy_perform(c);
    curl_slist_free_all(headers);

    if (res != CURLE_OK) {

      if (res == 28) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nConnection timeout.");
        msgBox.exec();
      } else if (res == 7) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nServer Unreachable.");
        msgBox.exec();
      } else {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nUnknown Server Error.");
        msgBox.exec();
      }
    }

    else {
      long responseCode;
      curl_easy_getinfo(c, CURLINFO_RESPONSE_CODE, &responseCode);
      qDebug() << "response is" << QString::number(responseCode);
      qDebug() << "response data is is"
               << QString::fromStdString(response_string);
      if (responseCode == 665) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nError writing files.");
        msgBox.exec();
      } else if (responseCode == 664) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nWrong request format.");
        msgBox.exec();
      } else if (responseCode == 663) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nCannot handle request type.");
        msgBox.exec();
      } else if (responseCode == 200) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nData sent successfully.");
        msgBox.exec();
      } else {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nUnknown Server Error.");
        msgBox.exec();
      }
    }

    curl_easy_cleanup(c);
  }
  return 1;
}
int
OriginSender::postGeoImage(QString ip, int port)
{
  if (ip.isEmpty())
    return -1;
  auto address = QString("http://" + ip + "/api/v1/geoimageunderlay");
  // lets populate our origin and underlay settings
  errno = 0;
  setupData();
  if (geoimage_path.isEmpty()) {
    QMessageBox msgBox;
    msgBox.setText(address + ":" + QString::number(port) +
                   ":\nNo geoimage to send.");
    msgBox.exec();
    // add alert to show user the error
    qDebug() << "geoimg path is empty";
    return -1;
  }
  qDebug() << "dive number is  " << dive_number;
  if (dive_number <= 0) {
    QMessageBox msgBox;
    msgBox.setText(address + ":" + QString::number(port) +
                   ":\nNo valid dive number to send.");
    msgBox.exec();
    // add alert to show user the error
    return -1;
  }

  QFileInfo fi(geoimage_path);
  QString ext = "Extension: " + fi.suffix();

  QString dive = "Dive: " + QString::number(dive_number);

  FILE* fd;
  // struct stat file_info;

  QByteArray ba = geoimage_path.toLatin1();
  char* file = ba.data();

  qDebug() << "char star path is " << file;
  // asprintf(&file, "%s/", path);

  fd = fopen(file, "rb");
  if (!fd) {
    fprintf(stderr, "Could not open file.\n");
    qDebug() << "could not open file";
    qDebug() << "error num is " << errno;
    return -1;
  }

  struct stat st;
  stat(file, &st);
  int size = st.st_size;

  curl_global_init(CURL_GLOBAL_DEFAULT);
  auto c = curl_easy_init();

  if (!c) {
    // curls couldnt init?
  } else {

    struct curl_slist* headers = NULL;
    headers =
      curl_slist_append(headers, "Content-Type: application/octet-stream");
    headers = curl_slist_append(headers, "Accept: application/octet-stream");
    headers = curl_slist_append(headers, ext.toLatin1().data());
    headers = curl_slist_append(headers, dive.toLatin1().data());
    std::string response_string;

    curl_easy_setopt(c, CURLOPT_URL, address.toLatin1().data());
    curl_easy_setopt(c, CURLOPT_PORT, port);
    curl_easy_setopt(c, CURLOPT_READDATA, fd);
    // Header http type request
    curl_easy_setopt(c, CURLOPT_HTTPHEADER, headers);
    curl_easy_setopt(c, CURLOPT_TIMEOUT, request_timeout_seconds);
    curl_easy_setopt(c, CURLOPT_POSTFIELDSIZE, size);
    /* Specify request type. Get in this example, post in others */
    curl_easy_setopt(c, CURLOPT_POST, 1L);

    curl_easy_setopt(c, CURLOPT_WRITEFUNCTION, WriteCallback);
    curl_easy_setopt(c, CURLOPT_WRITEDATA, &response_string);

    CURLcode res = curl_easy_perform(c);
    curl_slist_free_all(headers);

    if (res != CURLE_OK) {

      if (res == 28) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nConnection timeout.");
        msgBox.exec();
      } else if (res == 7) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nServer Unreachable.");
        msgBox.exec();
      } else {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nUnknown Server Error.");
        msgBox.exec();
      }
    }

    else {
      long responseCode;
      curl_easy_getinfo(c, CURLINFO_RESPONSE_CODE, &responseCode);
      qDebug() << "response is" << QString::number(responseCode);
      qDebug() << "response data is is"
               << QString::fromStdString(response_string);
      if (responseCode == 665) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nError writing files.");
        msgBox.exec();
      } else if (responseCode == 664) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nWrong request format.");
        msgBox.exec();
      } else if (responseCode == 663) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nCannot handle request type.");
        msgBox.exec();
      } else if (responseCode == 200) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nData sent successfully.");
        msgBox.exec();
      } else {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nUnknown Server Error.");
        msgBox.exec();
      }
    }

    curl_easy_cleanup(c);
  }
  return 1;
}

int
OriginSender::postTargets(QString ip, int port)
{
  if (ip.isEmpty())
    return -1;
  QString address = QString("http://" + ip + "/api/v1/targetfile");
  // lets populate our origin and underlay settings
  errno = 0;
  setupData();
  if (targets_path.isEmpty()) {
    // add alert to show user the error
    QMessageBox msgBox;
    msgBox.setText(address + ":" + QString::number(port) +
                   ":\nNo target file to send.");
    msgBox.exec();
    qDebug() << "target path is empty";
    return -1;
  }

  qDebug() << "dive number is  " << dive_number;
  if (dive_number <= 0) {
    QMessageBox msgBox;
    msgBox.setText(address + ":" + QString::number(port) +
                   ":\nNo valid dive number to send.");
    msgBox.exec();
    // add alert to show user the error
    return -1;
  }
  qDebug() << "the target path is " << targets_path;
  // const char* path = targets_path.toStdString().c_str();
  QFileInfo fi(targets_path);
  QString ext = "Extension: " + fi.suffix();

  QString dive = "Dive: " + QString::number(dive_number);

  FILE* fd;

  QByteArray ba = targets_path.toLatin1();
  char* file = ba.data();

  fd = fopen(file, "rb");
  if (!fd) {
    fprintf(stderr, "Could not open file.\n");
    qDebug() << "could not open file";
    qDebug() << "error num is " << errno;
    return -1;
  }

  struct stat st;
  stat(file, &st);
  int size = st.st_size;

  curl_global_init(CURL_GLOBAL_DEFAULT);
  auto c = curl_easy_init();

  if (!c) {
    // curls couldnt init?
  } else {

    struct curl_slist* headers = NULL;
    headers =
      curl_slist_append(headers, "Content-Type: application/octet-stream");
    headers = curl_slist_append(headers, "Accept: application/octet-stream");
    headers = curl_slist_append(headers, ext.toLatin1().data());
    headers = curl_slist_append(headers, dive.toLatin1().data());
    std::string response_string;

    curl_easy_setopt(c, CURLOPT_URL, address.toLatin1().data());
    curl_easy_setopt(c, CURLOPT_PORT, port);
    curl_easy_setopt(c, CURLOPT_READDATA, fd);
    // Header http type request
    curl_easy_setopt(c, CURLOPT_HTTPHEADER, headers);
    curl_easy_setopt(c, CURLOPT_TIMEOUT, request_timeout_seconds);
    curl_easy_setopt(c, CURLOPT_POSTFIELDSIZE, size);
    /* Specify request type. Get in this example, post in others */
    curl_easy_setopt(c, CURLOPT_POST, 1L);

    curl_easy_setopt(c, CURLOPT_WRITEFUNCTION, WriteCallback);
    curl_easy_setopt(c, CURLOPT_WRITEDATA, &response_string);

    CURLcode res = curl_easy_perform(c);
    curl_slist_free_all(headers);

    if (res != CURLE_OK) {

      if (res == 28) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nConnection timeout.");
        msgBox.exec();
      } else if (res == 7) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nServer Unreachable.");
        msgBox.exec();
      } else {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nUnknown Server Error.");
        msgBox.exec();
      }
    }

    else {
      long responseCode;
      curl_easy_getinfo(c, CURLINFO_RESPONSE_CODE, &responseCode);
      qDebug() << "response is" << QString::number(responseCode);
      qDebug() << "response data is is"
               << QString::fromStdString(response_string);
      if (responseCode == 665) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nError writing files.");
        msgBox.exec();
      } else if (responseCode == 664) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nWrong request format.");
        msgBox.exec();
      } else if (responseCode == 663) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nCannot handle request type.");
        msgBox.exec();
      } else if (responseCode == 200) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nData sent successfully.");
        msgBox.exec();
      } else {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nUnknown Server Error.");
        msgBox.exec();
      }
    }

    curl_easy_cleanup(c);
  }
  return 1;
}

int
OriginSender::postVehicleName(QString ip, int port)
{
  if (ip.isEmpty())
    return -1;
  auto address = QString("http://" + ip + "/api/v1/vehiclename");
  // lets populate our origin and underlay settings
  setupData();
  if (vehicle_name.isEmpty()) {
    QMessageBox msgBox;
    msgBox.setText(address + ":" + QString::number(port) +
                   ":\nNo vehicle name to send.");
    msgBox.exec();
    // add alert to show user the error
    return -1;
  }
  curl_global_init(CURL_GLOBAL_DEFAULT);
  auto c = curl_easy_init();

  if (!c) {
    // curls couldnt init?
  } else {

    struct curl_slist* headers = NULL;
    headers = curl_slist_append(headers, "Content-Type: application/json");
    headers = curl_slist_append(headers, "Accept: application/json");
    std::string response_string;

    QJsonObject jsonObj;
    jsonObj["name"] = vehicle_name;

    qDebug() << jsonObj;

    QJsonDocument jsonDoc(jsonObj);
    QString jsonString = jsonDoc.toJson();

    curl_easy_setopt(c, CURLOPT_URL, address.toLatin1().data());
    curl_easy_setopt(c, CURLOPT_PORT, port);
    curl_easy_setopt(c, CURLOPT_HTTPHEADER, headers);
    curl_easy_setopt(c, CURLOPT_TIMEOUT, request_timeout_seconds);

    curl_easy_setopt(c, CURLOPT_POST, 1L);
    curl_easy_setopt(c, CURLOPT_COPYPOSTFIELDS, jsonString.toLatin1().data());
    curl_easy_setopt(c, CURLOPT_WRITEFUNCTION, WriteCallback);
    curl_easy_setopt(c, CURLOPT_WRITEDATA, &response_string);
    /// curl_easy_setopt(c, CURLOPT_VERBOSE, 1L);

    CURLcode res = curl_easy_perform(c);
    curl_slist_free_all(headers);

    if (res != CURLE_OK) {

      if (res == 28) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nConnection timeout.");
        msgBox.exec();
      } else if (res == 7) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nServer Unreachable.");
        msgBox.exec();
      } else {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nUnknown Server Error.");
        msgBox.exec();
      }
    }

    else {
      long responseCode;
      curl_easy_getinfo(c, CURLINFO_RESPONSE_CODE, &responseCode);
      qDebug() << "response is" << QString::number(responseCode);
      qDebug() << "response data is is"
               << QString::fromStdString(response_string);
      if (responseCode == 665) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nError writing files.");
        msgBox.exec();
      } else if (responseCode == 664) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nWrong request format.");
        msgBox.exec();
      } else if (responseCode == 663) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nCannot handle request type.");
        msgBox.exec();
      } else if (responseCode == 200) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nData sent successfully.");
        msgBox.exec();
      } else if (responseCode == 667) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nCC ini path not set. Not writing vehicle name.");
        msgBox.exec();
      } else {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nUnknown Server Error.");
        msgBox.exec();
      }
    }

    curl_easy_cleanup(c);
  }
  return 1;
}
int
OriginSender::postDiveNumber(QString ip, int port)
{
  if (ip.isEmpty())
    return -1;
  auto address = QString("http://" + ip + "/api/v1/divenumber");
  // lets populate our origin and underlay settings
  setupData();
  qDebug() << "dive number is  " << dive_number;
  if (dive_number <= 0) {
    QMessageBox msgBox;
    msgBox.setText(address + ":" + QString::number(port) +
                   ":\nNo valid dive number to send.");
    msgBox.exec();
    // add alert to show user the error
    return -1;
  }
  curl_global_init(CURL_GLOBAL_DEFAULT);
  auto c = curl_easy_init();

  if (!c) {
    // curls couldnt init?
  } else {

    struct curl_slist* headers = NULL;
    headers = curl_slist_append(headers, "Content-Type: application/json");
    headers = curl_slist_append(headers, "Accept: application/json");
    std::string response_string;

    QJsonObject jsonObj;
    jsonObj["dive"] = dive_number;

    qDebug() << jsonObj;

    QJsonDocument jsonDoc(jsonObj);
    QString jsonString = jsonDoc.toJson();

    curl_easy_setopt(c, CURLOPT_URL, address.toLatin1().data());
    curl_easy_setopt(c, CURLOPT_PORT, port);
    curl_easy_setopt(c, CURLOPT_HTTPHEADER, headers);
    curl_easy_setopt(c, CURLOPT_TIMEOUT, request_timeout_seconds);

    curl_easy_setopt(c, CURLOPT_POST, 1L);
    curl_easy_setopt(c, CURLOPT_COPYPOSTFIELDS, jsonString.toLatin1().data());
    curl_easy_setopt(c, CURLOPT_WRITEFUNCTION, WriteCallback);
    curl_easy_setopt(c, CURLOPT_WRITEDATA, &response_string);
    /// curl_easy_setopt(c, CURLOPT_VERBOSE, 1L);

    CURLcode res = curl_easy_perform(c);
    curl_slist_free_all(headers);

    if (res != CURLE_OK) {

      if (res == 28) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nConnection timeout.");
        msgBox.exec();
      } else if (res == 7) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nServer Unreachable.");
        msgBox.exec();
      } else {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nUnknown Server Error.");
        msgBox.exec();
      }
    }

    else {
      long responseCode;
      curl_easy_getinfo(c, CURLINFO_RESPONSE_CODE, &responseCode);
      qDebug() << "response is" << QString::number(responseCode);
      qDebug() << "response data is is"
               << QString::fromStdString(response_string);
      if (responseCode == 665) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nError writing files.");
        msgBox.exec();
      } else if (responseCode == 664) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nWrong request format.");
        msgBox.exec();
      } else if (responseCode == 663) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nCannot handle request type.");
        msgBox.exec();
      } else if (responseCode == 200) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nData sent successfully.");
        msgBox.exec();
      } else if (responseCode == 667) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nDive number path not set. Not writing.");
        msgBox.exec();
      } else {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nUnknown Server Error.");
        msgBox.exec();
      }
    }

    curl_easy_cleanup(c);
  }
  return 1;
}

int
OriginSender::postUnderlayClear(QString ip, int port)
{
  if (ip.isEmpty())
    return -1;
  auto address = QString("http://" + ip + "/api/v1/clearunderlay");

  curl_global_init(CURL_GLOBAL_DEFAULT);
  auto c = curl_easy_init();

  if (!c) {
    // curls couldnt init?
  } else {

    struct curl_slist* headers = NULL;
    headers = curl_slist_append(headers, "Content-Type: application/json");
    headers = curl_slist_append(headers, "Accept: application/json");
    std::string response_string;

    QJsonObject jsonObj;
    jsonObj["clear"] = "true";

    qDebug() << jsonObj;

    QJsonDocument jsonDoc(jsonObj);
    QString jsonString = jsonDoc.toJson();

    curl_easy_setopt(c, CURLOPT_URL, address.toLatin1().data());
    curl_easy_setopt(c, CURLOPT_PORT, port);
    curl_easy_setopt(c, CURLOPT_HTTPHEADER, headers);
    curl_easy_setopt(c, CURLOPT_TIMEOUT, request_timeout_seconds);

    curl_easy_setopt(c, CURLOPT_POST, 1L);
    curl_easy_setopt(c, CURLOPT_COPYPOSTFIELDS, jsonString.toLatin1().data());
    curl_easy_setopt(c, CURLOPT_WRITEFUNCTION, WriteCallback);
    curl_easy_setopt(c, CURLOPT_WRITEDATA, &response_string);
    /// curl_easy_setopt(c, CURLOPT_VERBOSE, 1L);

    CURLcode res = curl_easy_perform(c);
    curl_slist_free_all(headers);

    if (res != CURLE_OK) {

      if (res == 28) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nConnection timeout.");
        msgBox.exec();
      } else if (res == 7) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nServer Unreachable.");
        msgBox.exec();
      } else {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nUnknown Server Error.");
        msgBox.exec();
      }
    }

    else {
      long responseCode;
      curl_easy_getinfo(c, CURLINFO_RESPONSE_CODE, &responseCode);
      qDebug() << "response is" << QString::number(responseCode);
      qDebug() << "response data is is"
               << QString::fromStdString(response_string);
      if (responseCode == 665) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nError writing files.");
        msgBox.exec();
      } else if (responseCode == 664) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nWrong request format.");
        msgBox.exec();
      } else if (responseCode == 663) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nCannot handle request type.");
        msgBox.exec();
      } else if (responseCode == 200) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nData sent successfully.");
        msgBox.exec();
      } else {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nUnknown Server Error.");
        msgBox.exec();
      }
    }

    curl_easy_cleanup(c);
  }
  return 1;
}

int
OriginSender::postTargetUnderlayClear(QString ip, int port)
{
  if (ip.isEmpty())
    return -1;
  auto address = QString("http://" + ip + "/api/v1/cleartargetunderlay");

  curl_global_init(CURL_GLOBAL_DEFAULT);
  auto c = curl_easy_init();

  if (!c) {
    // curls couldnt init?
  } else {

    struct curl_slist* headers = NULL;
    headers = curl_slist_append(headers, "Content-Type: application/json");
    headers = curl_slist_append(headers, "Accept: application/json");
    std::string response_string;

    QJsonObject jsonObj;
    jsonObj["clear"] = "true";

    qDebug() << jsonObj;

    QJsonDocument jsonDoc(jsonObj);
    QString jsonString = jsonDoc.toJson();

    curl_easy_setopt(c, CURLOPT_URL, address.toLatin1().data());
    curl_easy_setopt(c, CURLOPT_PORT, port);
    curl_easy_setopt(c, CURLOPT_HTTPHEADER, headers);
    curl_easy_setopt(c, CURLOPT_TIMEOUT, request_timeout_seconds);

    curl_easy_setopt(c, CURLOPT_POST, 1L);
    curl_easy_setopt(c, CURLOPT_COPYPOSTFIELDS, jsonString.toLatin1().data());
    curl_easy_setopt(c, CURLOPT_WRITEFUNCTION, WriteCallback);
    curl_easy_setopt(c, CURLOPT_WRITEDATA, &response_string);
    /// curl_easy_setopt(c, CURLOPT_VERBOSE, 1L);

    CURLcode res = curl_easy_perform(c);
    curl_slist_free_all(headers);

    if (res != CURLE_OK) {

      if (res == 28) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nConnection timeout.");
        msgBox.exec();
      } else if (res == 7) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nServer Unreachable.");
        msgBox.exec();
      } else {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nUnknown Server Error.");
        msgBox.exec();
      }
    }

    else {
      long responseCode;
      curl_easy_getinfo(c, CURLINFO_RESPONSE_CODE, &responseCode);
      qDebug() << "response is" << QString::number(responseCode);
      qDebug() << "response data is is"
               << QString::fromStdString(response_string);
      if (responseCode == 665) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nError writing files.");
        msgBox.exec();
      } else if (responseCode == 664) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nWrong request format.");
        msgBox.exec();
      } else if (responseCode == 663) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nCannot handle request type.");
        msgBox.exec();
      } else if (responseCode == 200) {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nData sent successfully.");
        msgBox.exec();
      } else {
        QMessageBox msgBox;
        msgBox.setText(address + ":" + QString::number(port) +
                       ":\nUnknown Server Error.");
        msgBox.exec();
      }
    }

    curl_easy_cleanup(c);
  }
  return 1;
}

void
OriginSender::setupData()
{
  if (!n_scene) {
    return;
  }
  QPointF origin = n_scene->alvinXYOrigin();
  origin_lon = QString::number(origin.x(), 'f', 8);
  origin_lat = QString::number(origin.y(), 'f', 8);

  QPointF start = n_scene->XYStart();
  start_lon = QString::number(start.x(), 'f', 8);
  start_lat = QString::number(start.y(), 'f', 8);

  bathy_path = n_scene->bathyPath();
  contour_path = n_scene->contourPath();
  image_path = n_scene->imagePath();
  geoimage_path = n_scene->geoimagePath();
  targets_path = n_scene->targetPath();

  contour_spacing = n_scene->contourSpacing();
  image_coords = n_scene->imageCoords();

  vehicle_name = n_scene->vehicleName();
  dive_number = n_scene->diveNum();

  qDebug() << "After grabbing from navgmapscene origin lon is " << origin_lon
           << " while origin lat is " << origin_lat;
}

} // origin_sender
} // plugins
} // navg
