/**
 * Copyright 2021 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// Created by tmjoyce on 06/21/21.
//

#ifndef NAVG_ORIGINSENDER_H
#define NAVG_ORIGINSENDER_H

#include <navg/CorePluginInterface.h>

#include "dslmap/MapView.h"
#include "dslmap/MapScene.h"
#include <QLoggingCategory>
#include <QObject>
#include <QWidget>
#include <QDateTime>
#include <QJsonObject>
#include <QJsonDocument>
#include <QMessageBox>
#include <QAction>
#include <memory>
#include <curl/curl.h> 
#include "OriginSettings.h"

#include <navg/NavGMapView.h>
#include <navg/NavGMapScene.h>

namespace navg {
namespace plugins {
namespace origin_sender {

namespace Ui {
class OriginSender;
}

class OriginSender
  : public QWidget
  , public CorePluginInterface
{
  Q_OBJECT
  Q_INTERFACES(navg::CorePluginInterface)
  Q_PLUGIN_METADATA(IID CorePluginInterface_iid FILE "OriginSender.json")

signals:
  void requestCoordConversion(QPointF);

public:
  explicit OriginSender(QWidget* parent = nullptr);
  ~OriginSender() override;

  //
  // CorePluginInterface Implementation
  //
  void loadSettings(QSettings* settings) override;
  void saveSettings(QSettings& settings) override;

  // This plugin connects to ....
  void connectPlugin(const QObject* plugin,
                     const QJsonObject& metadata) override;
  void setMapView(QPointer<dslmap::MapView> view) override;
  QList<QAction*> pluginMenuActions() override;
  QList<QAction*> pluginToolbarActions() override { return {}; }

public slots:
  /// \brief Show the settings dialog
  ///
  void showSettingsDialog();

protected:

protected slots:
  int postOrigin(QString,int);
  int postStartPoint(QString,int);
  int postBathy(QString,int);
  int postContour(QString,int);
  int postImage(QString,int);
  int postGeoImage(QString,int);
  int postUnderlayClear(QString,int);

  int postTargetUnderlayClear(QString,int);

  int postContourSpacing(QString, int);
  int postImageDimensions(QString,int);

  int postVehicleName(QString,int);
  int postDiveNumber(QString,int);

  int postTargets(QString,int);

private:
  std::unique_ptr<Ui::OriginSender> ui;

  // grab the values we need from mapscene
  void setupData();

  static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp);
  
  QPointer<dslmap::MapView> m_view;
  QPointer<navg::NavGMapView> n_view;

  QPointer<dslmap::MapScene> m_scene;
  QPointer<navg::NavGMapScene> n_scene;

  QAction* m_action_show_settings = nullptr;


  // our defined origins and paths
  QString origin_lon;
  QString origin_lat;

  QString start_lon;
  QString start_lat;

  QString bathy_path;
  
  QString contour_path;
  int contour_spacing = 0;

  QString image_path;
  QString image_coords;

  QString geoimage_path;

  int request_timeout_seconds = 10;

  int dive_number = 0;
  QString vehicle_name;

  QString targets_path;


  // remote settings
  // hardcoded 3 of them

  QString ip1;
  int port1=0;

  QString ip2;
  int port2=0;

  QString ip3;
  int port3=0;


  //add underlays also

};

Q_DECLARE_LOGGING_CATEGORY(plugin_origin_sender)
} // origin_sender
} // plugins
} // navg

#endif // ORIGINSENDER_H
