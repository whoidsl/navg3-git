/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 8/9/18.
//

#define CATCH_CONFIG_SENTRYTRACKLINELAYER

#include "SentryTracklineLayer.h"
#include "catch.hpp"

#include <dslmap/MapScene.h>

#include <QApplication>
#include <QDebug>
#include <QSettings>
#include <QSignalSpy>
//#include "dslmap/MapItem.h"
//#include "dslmap/Proj.h"

using namespace navg::plugins::sentrytracks;

TEST_CASE("SentryTracklineLayer Defaults")
{
  auto layer = std::make_unique<SentryTracklineLayer>();

  INFO("Layer has the proper MapGraphicsItemType");
  REQUIRE(layer->type() == dslmap::MapItemType::SentryTracklineLayerType);

  // INFO("Layer has no items")
  // REQUIRE(layer->count() == 0);

  INFO("Layer has a valid uuid")
  REQUIRE_FALSE(layer->id().isNull());

  // INFO("Layer had a non-null symbol");
  // REQUIRE_FALSE(layer->symbol().expired());

  auto proj = layer->projectionPtr();
  INFO("Layer projection defaults to WGS84")
  REQUIRE(proj);

  CHECK(proj->isSame(dslmap::ProjWGS84::getInstance()));
}

SCENARIO("adding a segment to a SentryTrailLayer")
{
  GIVEN("two QPointFs")
  {
    auto scene = std::make_unique<dslmap::MapScene>();
    auto layer = new SentryTracklineLayer;

    scene->addItem(layer);

    WHEN("a point is added to the layer")
    {
      const auto point1 = QPointF{ 10, 10 };
      const auto point2 = QPointF{ 11, 11 };
      // const auto label = QString{"marker label"};

      auto segment = layer->addSegment(point1, point2, 0, 0);

      THEN("the new segment is returned to the caller")
      {
        REQUIRE(segment != nullptr);
        // REQUIRE(->labelText() == label);
        // REQUIRE(marker->scenePos() == point);
      }

      AND_THEN("the marker's parentItem has been set to the layer")
      {
        REQUIRE(segment->parentItem() == layer);
        REQUIRE(layer->childItems().contains(segment));
      }
    }
  }
}

// SCENARIO("The projection changes.") {
//
// GIVEN("A SymbolLayer with a projection and points") {
//// Make 100 lat/lon coordinates near the scene UTM zone.
// auto positions = QList<QPointF> {};
// auto mean_lon = 0.0;
// auto mean_lat = 0.0;
// const auto num_points = 100;
// for(auto i=0; i<num_points; i++) {
// auto lon = static_cast<qreal>(qrand()/RAND_MAX * 4 - 4);
// auto lat = static_cast<qreal>(qrand()/RAND_MAX * 85);
// mean_lon += lon / num_points;
// mean_lat += lat / num_points;
// positions << QPointF{lon, lat};
//}
//
// const auto scene_origin = QPointF{mean_lon, mean_lat};
//
// auto proj = Proj::fromDefinition(PROJ_WGS84_DEFINITION);
// REQUIRE(proj);
//
// auto layer = new SymbolLayer;
// layer->setProjection(proj);
//
// auto items = QList<QGraphicsItem*>{};
// items.reserve(num_points);
//
//// Create "markers" at the generated positions.
// for(auto it=std::begin(positions); it!=std::end(positions); it++) {
// items << layer->addPoint(*it);
//}
//
// WHEN("the layer projection is changed.") {
// auto utm_proj = Proj::utmFromLonLat(scene_origin.x(), scene_origin.y());
// REQUIRE(utm_proj);
// layer->setProjection(utm_proj);
//
// THEN("the layer items are reprojected to the scene projection") {
//
// const auto new_proj = layer->projection();
//
//// The layer projection string should now match the scene projection.
// REQUIRE(new_proj);
// REQUIRE(new_proj->isSame(*utm_proj));
//
// for(auto i=0; i<items.count(); i++) {
// const auto new_pos = items.at(i)->scenePos();
// auto lon = positions.at(i).x();
// auto lat = positions.at(i).y();
//
// proj->transformTo(*utm_proj, 1, &lon, &lat);
//
// REQUIRE(lat == new_pos.y());
// REQUIRE(lon == new_pos.x());
//}
//}
//}
//}
//}

SCENARIO("Creating a SentryTracklineLayer from settings")
{
  GIVEN("A valid group of settings")
  {
    QSettings* settings =
      new QSettings(QSettings::InvalidFormat, QSettings::UserScope, "Test");
    settings->setValue("name", "Sentry495");
    settings->setValue("pens/do/color", "#00ffff");
    settings->setValue("pens/do/width", "1");
    settings->setValue("pens/do/style", "NoPen");
    settings->setValue("pens/doing/color", "#ff0000");
    settings->setValue("pens/doing/width", "2");
    settings->setValue("pens/doing/style", "NoPen");
    settings->setValue("pens/done/color", "#ffff00");
    settings->setValue("pens/done/width", "5");
    settings->setValue("pens/done/style", "NoPen");

    WHEN("A SentryTracklineLayer is created with the settings")
    {
      auto layer = SentryTracklineLayer::fromSettings(settings);

      THEN("The layer creation is successful")
      {
        REQUIRE(layer);
        REQUIRE_FALSE(layer->id().isNull());
      }

      AND_THEN("All layer properties are set correctly")
      {
        // auto wptr = layer->symbol();
        // REQUIRE_FALSE(wptr.expired());
        //
        // auto symbol =
        // std::dynamic_pointer_cast<ShapeMarkerSymbol>(wptr.lock());
        // REQUIRE(symbol);
        // auto expected_symbol = ShapeMarkerSymbol::fromSettings(settings,
        // "symbol/");
        // REQUIRE(expected_symbol);

        // CHECK(symbol->shape() == expected_symbol->shape());
        // CHECK(symbol->size() == expected_symbol->size());
        // CHECK(symbol->pen().color().name() ==
        // expected_symbol->pen().color().name());
        // CHECK(symbol->pen().widthF() == expected_symbol->pen().widthF());
        // CHECK(symbol->brush().color().name() ==
        // expected_symbol->brush().color().name());
        // CHECK(symbol->labelPen().color().name() ==
        // expected_symbol->labelPen().color().name());
        // CHECK(symbol->isLabelVisible() == expected_symbol->isLabelVisible());
        // CHECK(symbol->labelAlignment() == expected_symbol->labelAlignment());

        CHECK(layer->name() == settings->value("name").toString());
        CHECK(layer->penDo().color().name() ==
              settings->value("pens/do/color").toString());
        CHECK(layer->penDo().width() ==
              settings->value("pens/do/width").toInt());
        CHECK(layer->penDoing().color().name() ==
              settings->value("pens/doing/color").toString());
        CHECK(layer->penDoing().width() ==
              settings->value("pens/doing/width").toInt());
        CHECK(layer->penDone().color().name() ==
              settings->value("pens/done/color").toString());
        CHECK(layer->penDone().width() ==
              settings->value("pens/done/width").toInt());
      }
    }
  }
}

// SCENARIO("Saving SymbolLayer properites")
//{
// GIVEN("A BeaconLayer with two ShapeMarkerSymbols")
//{
// auto layer = std::make_unique<SymbolLayer>();
// layer->setName("Symbollayer");
// auto symbol =
// std::make_unique<ShapeMarkerSymbol>(ShapeMarkerSymbol::Shape::Diamond, 25);
// auto pen = QPen{Qt::red};
// pen.setWidthF(3);
// symbol->setPen(pen);
// pen.setColor(Qt::black);
// symbol->setLabelPen(pen);
// auto brush = QBrush{Qt::yellow};
// symbol->setBrush(brush);
// symbol->setLabelVisible(false);
// symbol->setLabelAlignment(Qt::AlignHCenter);
//
// layer->setSymbol(std::move(symbol));
//
// WHEN("The settings are saved to a QSettings object")
//{
// QSettings settings(QSettings::InvalidFormat, QSettings::UserScope, "Test");
// layer->saveSettings(settings);
//
// THEN("All settings are saved correctly")
//{
// auto wptr = layer->symbol();
// REQUIRE_FALSE(wptr.expired());
//
// auto symbol = std::dynamic_pointer_cast<ShapeMarkerSymbol>(wptr.lock());
// REQUIRE(symbol);
// auto expected_symbol = ShapeMarkerSymbol::fromSettings(settings, "symbol/");
// REQUIRE(expected_symbol);
//
// CHECK(symbol->shape() == expected_symbol->shape());
// CHECK(symbol->size() == expected_symbol->size());
// CHECK(symbol->pen().color().name() == expected_symbol->pen().color().name());
// CHECK(symbol->pen().widthF() == expected_symbol->pen().widthF());
// CHECK(symbol->brush().color().name() ==
// expected_symbol->brush().color().name());
// CHECK(symbol->labelPen().color().name() ==
// expected_symbol->labelPen().color().name());
// CHECK(symbol->isLabelVisible() == expected_symbol->isLabelVisible());
// CHECK(symbol->labelAlignment() == expected_symbol->labelAlignment());
//
// CHECK(layer->name() == settings.value("name").toString());
//}
//}
//
//}
//}
