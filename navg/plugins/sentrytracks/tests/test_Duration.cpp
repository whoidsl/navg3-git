/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 8/10/18.
//

#include "Duration.h"
#include "catch.hpp"

using namespace navg::plugins::sentrytracks;

SCENARIO("Duration initializes values correctly")
{

  GIVEN("hours, minutes, seconds")
  {
    const int hours = 10;
    const int minutes = 50;
    const int seconds = 10;

    WHEN("Duration is created")
    {
      const auto duration = Duration(hours, minutes, seconds);

      THEN("values match up")
      {
        REQUIRE(hours == duration.hours());
        REQUIRE(minutes == duration.minutes());
        REQUIRE(seconds == duration.seconds());
      }
    }
  }
  GIVEN("negative hours")
  {
    const int hours = -10;
    const int minutes = 50;
    const int seconds = 10;

    WHEN("Duration is created")
    {
      const auto duration = Duration(hours, minutes, seconds);

      THEN("values match up")
      {
        REQUIRE(hours == duration.hours());
        REQUIRE(minutes == duration.minutes());
        REQUIRE(seconds == duration.seconds());
      }
    }
  }
  GIVEN("double seconds initializer")
  {
    const double seconds = 3665.5;
    WHEN("Duration is created")
    {
      const auto duration = Duration(seconds);
      THEN("values match up")
      {
        REQUIRE(duration.hours() == 1);
        REQUIRE(duration.minutes() == 1);
        REQUIRE(duration.seconds() == 5);
      }
    }
  }

  GIVEN("negative seconds")
  {
    const double seconds = 3665.5;
    WHEN("Duration is created")
    {
      const auto duration = Duration(seconds);
      THEN("values match up")
      {
        REQUIRE(duration.hours() == 1);
        REQUIRE(duration.minutes() == 1);
        REQUIRE(duration.seconds() == 5);
      }
    }
  }
}
SCENARIO("Durations convert to hours, minutes, and seconds")
{
  GIVEN("Durations")
  {
    Duration d1(-5, 10, 25);
    Duration d2(500, 0, 0);
    Duration d3(2, 4, 6);
    Duration d4(500.3);
    WHEN("Converted to hours")
    {
      const auto h1 = d1.toHours();
      const auto h2 = d2.toHours();
      const auto h3 = d3.toHours();
      const auto h4 = d4.toHours();
      THEN("Hours")
      {
        REQUIRE(h1 == Approx(-4.826388));
        REQUIRE(h2 == Approx(500.0));
        REQUIRE(h3 == Approx(2.068333));
        REQUIRE(h4 == Approx(0.13888888));
      }
    }
    WHEN("Converted to minutes")
    {
      const auto m1 = d1.toMinutes();
      const auto m2 = d2.toMinutes();
      const auto m3 = d3.toMinutes();
      const auto m4 = d4.toMinutes();
      THEN("Minutes")
      {
        REQUIRE(m1 == Approx(-289.58328));
        REQUIRE(m2 == Approx(30000));
        REQUIRE(m3 == Approx(124.09998));
        REQUIRE(m4 == Approx(8.33333));
      }
    }
    WHEN("Converted to seconds")
    {
      const auto s1 = d1.toSeconds();
      const auto s2 = d2.toSeconds();
      const auto s3 = d3.toSeconds();
      const auto s4 = d4.toSeconds();
      THEN("Seconds")
      {
        REQUIRE(s1 == Approx(-17374.9968));
        REQUIRE(s2 == Approx(1800000));
        REQUIRE(s3 == Approx(7445.9988));
        REQUIRE(s4 == Approx(500));
      }
    }
  }
}

SCENARIO("DurationXDuration operators return expected values")
{
  GIVEN("Durations")
  {
    Duration d1(1, 15, 55);
    Duration d2(2, 35, 45);
    Duration d3(3, 55, 35);
    Duration d4(-4, 00, 25);
    WHEN("Added")
    {
      Duration sum = d1 + d2 + d3;
      THEN("The sum is what we expect") { REQUIRE(sum == Duration(7, 47, 15)); }
    }
    WHEN("Plus equals")
    {
      d1 += d2;
      THEN("d1 increases value by d2") { REQUIRE(d1 == Duration(3, 51, 40)); }
    }
    WHEN("Subtracted")
    {
      Duration dif21 = d2 - d1;
      Duration dif13 = d1 - d3;
      THEN("The differences (including negatives) are correct")
      {
        REQUIRE(dif21 == Duration(1, 19, 50));
        REQUIRE(dif13 == Duration(-3, 20, 20));
      }
    }
    WHEN("Minus equals")
    {
      d1 -= d2;
      THEN("d1 decreases by d2") { REQUIRE(d1 == Duration(-2, 40, 10)); }
    }
    WHEN("Divided")
    {
      const auto d1_d2 = d1 / d2;
      const auto d4_d1 = d4 / d1;
      THEN("returns ratio between Durations")
      {
        REQUIRE(d1_d2 == Approx(0.4874264312));
        REQUIRE(d4_d1 == Approx(-3.155872));
      }
    }
    WHEN("Multiplied by a scale factor")
    {
      const auto prod = d1 * 2.5;
      THEN("returns a scaled Duration") { REQUIRE(prod == Duration(3, 9, 47)); }
    }
    WHEN("Comparisons between Durations")
    {
      THEN("results make sense")
      {
        REQUIRE(d1 < d2);
        REQUIRE(d2 > d1);
        REQUIRE(d1 == Duration(1, 15, 55));
        REQUIRE(d4 < d3);
        REQUIRE(d4 < d3 - d1);
        REQUIRE(d2 >= d1);
        REQUIRE(d1 <= d2);
      }
    }
  }
}