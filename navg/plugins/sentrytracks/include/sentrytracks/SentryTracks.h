/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 7/26/18.
//

#ifndef NAVG_SENTRYTRACKS_H
#define NAVG_SENTRYTRACKS_H

#include "SentryTracklineLayer.h"

#include <dslmap/MapScene.h>
#include <dslmap/MarkerItem.h>
#include <navg/CorePluginInterface.h>

#include <QHash>
#include <QHostAddress>
#include <QLoggingCategory>
#include <QSettings>
#include <QWidget>

#include <memory>
#include <navg/NavGMapView.h>

namespace navg {
namespace plugins {
namespace sentrytracks {

namespace Ui {
class SentryTracks;
}

class SentryTracks : public QWidget, public CorePluginInterface
{
  Q_OBJECT
  Q_INTERFACES(navg::CorePluginInterface)
  Q_PLUGIN_METADATA(IID CorePluginInterface_iid FILE "SentryTracks.json")

signals:
  /// \brief Emitted whe the origin point changes.
  ///
  /// "No origin point" is represented by QPointF(nan, nan) and can be
  /// checked using NavGMapScene::isValidAlvinXYOrigin
  ///
  /// \param origin
  void alvinXYOriginChanged(QPointF origin);

public:
  explicit SentryTracks(QWidget* parent = nullptr);
  ~SentryTracks() override;

  //
  // CorePluginInterface Implementation
  //
  void loadSettings(QSettings* settings) override;
  void saveSettings(QSettings& settings) override;
  QList<QAction*> pluginMenuActions() override;
  QList<QAction*> pluginToolbarActions() override;

public slots:
  // This plugin does not create connections.
  void connectPlugin(const QObject* plugin,
                     const QJsonObject& metadata) override;
  void setMapView(QPointer<dslmap::MapView> view) override;

  /// \brief Set the remote IP address for a running Navest instance
  ///
  /// The IP address is used to load trackline information if topside-navest is
  /// running
  /// \param address
  void setNavestRemoteAddress(QHostAddress address);

  /// \brief Set the remote TCP port for a running Navest instance
  ///
  /// \param address
  void setNavestRemoteTcpPort(int port);

  // Set the active trackline index
  void setActiveTrackline(unsigned int trackline);

  /// \brief Load tracklines and origin from a tracks file.
  ///
  /// \param filename
  /// \return
  bool readSentryTracksFromFile(const QString& filename);

  /// \brief Load tracklines and origin from navest.
  ///
  /// \return
  bool readSentryTracksFromNavest();

private:
  bool loadSentryTracksFile(QIODevice& buffer);
  void readSentryTracksFromFileDialog();
  void radioBtnCallback();
  void aSlider2Val(int val);
  void bSlider2Val(int val);
  void showSlider2Val(int val);
  void updateTime();

  std::unique_ptr<Ui::SentryTracks> ui;
  QPointer<dslmap::MapView> m_view;
  qreal m_length;
  Duration m_time;
  int m_segments;
  SentryTracklineLayer* m_layer;
  int m_A_seg, m_B_seg;
  qreal m_A_len, m_B_len;
  Duration m_A_time, m_B_time;
  QSettings m_settings;
  QAction* m_act_add_tracks_from_file = nullptr;
  QAction* m_act_add_tracks_from_navest = nullptr;
  QHostAddress m_navest_remote_address = {};
  int m_navest_remote_tcp_port = 0;
};
Q_DECLARE_LOGGING_CATEGORY(plugin_sentrytracks)
}
}
}
#endif // NAVG_SENTRYTRACKS_H
