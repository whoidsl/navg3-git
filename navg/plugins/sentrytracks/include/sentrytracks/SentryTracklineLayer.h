/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 7/26/18.
//

#ifndef NAVG_SENTRYTRACKLINELAYER_H
#define NAVG_SENTRYTRACKLINELAYER_H

#include "SentryTrackline.h"
#include <QMenu>
#include <QPaintEvent>
#include <QPen>
#include <QStyleOptionGraphicsItem>
#include <QtCore>
#include <dslmap/DslMap.h>
#include <dslmap/MapLayer.h>
#include <dslmap/MarkerItem.h>
#include <dslmap/Proj.h>

namespace navg {
namespace plugins {
namespace sentrytracks {

class SentryTracklineLayerPrivate;

Q_DECLARE_LOGGING_CATEGORY(plugin_sentrytracks_layer)
class SentryTracklineLayer : public dslmap::MapLayer
{
  Q_OBJECT
  Q_DECLARE_PRIVATE(SentryTracklineLayer)

public:
  explicit SentryTracklineLayer(QGraphicsItem* parent = Q_NULLPTR);
  ~SentryTracklineLayer() override;

  void showPropertiesDialog() override;
  int type() const override
  {
    return dslmap::MapItemType::SentryTracklineLayerType;
  }
  void setName(const QString& name_) noexcept override;
  QMenu* contextMenu() override;
  bool saveSettings(QSettings& settings) override;
  bool loadSettings(QSettings& settings) override;
  bool saveSettings(QSettings* settings);
  bool loadSettings(QSettings* settings);
  void loadPenSettings(QPen* pen_ptr, QSettings* settings, QString prefix = "");
  void savePenSettings(QPen* pen_ptr, QSettings* settings, QString prefix = "");

  static std::unique_ptr<SentryTracklineLayer> fromSettings(
    QSettings* settings);

  QRectF boundingRect() const override;
  void paint(QPainter* painter, const QStyleOptionGraphicsItem* option,
             QWidget* widget = 0) override;

  void segmentDone();
  SentryTrackline* addSegment(const QPointF& start, const QPointF& end,
                              qreal length, Duration time);
  void makeSentryMarker();
  void setSentryLength(qreal length);
  void setSentryTime(Duration time);
  void setSentrySegments(int seg);
  qreal getSentryLength() const;
  Duration getSentryTime() const;
  int getSentrySegments() const;
  std::shared_ptr<dslmap::ShapeMarkerSymbol> getSentrySymbol();

  void makeDeltaMarker();
  void setDeltaLength(qreal length);
  void setDeltaTime(Duration time);
  void setDeltaSegments(int seg);
  qreal getDeltaLength() const;
  Duration getDeltaTime() const;
  int getDeltaSegments() const;
  std::shared_ptr<dslmap::ShapeMarkerSymbol> getDeltaSymbol();

  bool removeSegment(SentryTrackline* seg);
  void setProjection(std::shared_ptr<const dslmap::Proj> proj) override;
  void setOrigin(qreal x, qreal y);
  QPointF getOrigin();

  qreal totalLength() const;
  Duration totalTime() const;
  int totalSegments() const;

  void setShowLength(int length);

  QPen penDo();
  QPen penDoing();
  QPen penDone();

public slots:
  void setPenDo(const QPen& pen);
  void setPenDoing(const QPen& pen);
  void setPenDone(const QPen& pen);
  void setActiveTrackline(unsigned int trackline);

private:
  SentryTracklineLayerPrivate* d_ptr;
  void setupMenu();

  QPointF pointAtTime(Duration time, QString marker = "");
  QPointF pointAtLength(qreal length, QString marker = "");
  QPointF pointAtSeg(int seg, QString marker = "");
  void updateBoundingRect();
};
}
}
}

#endif // NAVG_SENTRYTRACKLINELAYER_H
