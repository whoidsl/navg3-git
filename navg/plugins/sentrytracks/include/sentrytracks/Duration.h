/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 8/10/18.
//

#ifndef NAVG_DURATION_H
#define NAVG_DURATION_H

#include <QString>
#include <QTime>
#include <math.h>
#include <sstream>
#include <string>

namespace navg {
namespace plugins {
namespace sentrytracks {

class Duration
{
  /// \brief Duration describes a difference in time to 1-second resolution
  /// This class was created primarily for the operators
  /// m_hours: integers
  /// m_minutes: integers between 0 and 59
  /// m_seconds: integers between 0 and 59
  /// print() returns +/-hh:+mm:+ss
  /// Addition and subtraction rules as follows:
  ///     -05:00:00 + 00:05:15 = -05:05:15
  ///     -05:00:00 - 00:05:15 = -06:54:45
  ///     01:02:03 - 2*(01:02:03) = -02:57:57, not -01:02:03
  ///
public:
  /// \brief default constructor makes a zero duration
  Duration()
    : m_hours(0)
    , m_minutes(0)
    , m_seconds(0)
  {
  }

  /// \brief create a duration from hours, minutes, and seconds
  /// inputs for minutes and seconds will be capped to valid 0-59 range
  Duration(int hours, unsigned int minutes, unsigned int seconds)
    : m_hours(hours)
    , m_minutes(minutes % 60)
    , m_seconds(seconds % 60)
  {
  }

  /// \brief create a duration from seconds--double for easier math
  Duration(double seconds)
  {
    m_hours = (int)floor(seconds / 3600.0);
    m_minutes = (unsigned int)floor(seconds / 60.0 - m_hours * 60);
    m_seconds = (unsigned int)floor(seconds - m_hours * 3600 - m_minutes * 60);
  }

  /// \brief SIMPLE GETTERS: get the stored value.
  /// returns a component of the Duration
  int hours() const { return m_hours; }
  unsigned int minutes() const { return m_minutes; }
  unsigned int seconds() const { return m_seconds; }

  /// \brief FULL-DURATION GETTERS:
  /// return a representation of the full Duration
  double toHours() const
  {
    return (double)m_hours + (double)(m_minutes) / 60.0 +
           (double)(m_seconds) / 3600.0;
  }
  double toMinutes() const
  {
    return (double)m_hours * 60.0 + (double)m_minutes +
           (double)(m_seconds) / 60.0;
  }
  double toSeconds() const
  {
    return (double)m_hours * 3600.0 + (double)m_minutes * 60.0 +
           (double)m_seconds;
  }

private:
  /// \brief Stored values
  int m_hours;
  unsigned int m_minutes;
  unsigned int m_seconds;

public:
  /// \brief convenience print function
  QString print()
  {
    if (m_hours < 0) {
      return QString("-%1:%2:%3")
        .arg(abs(m_hours + 1), 2, 10, QChar('0'))
        .arg(59 - m_minutes, 2, 10, QChar('0'))
        .arg(60 - m_seconds, 2, 10, QChar('0'));
    }
    return QString("%1:%2:%3")
      .arg(m_hours, 2, 10, QChar('0'))
      .arg(m_minutes, 2, 10, QChar('0'))
      .arg(m_seconds, 2, 10, QChar('0'));
  }

  /// \brief Operators
  /// These are not the most efficient algorithms,
  /// but they were easy to write and debug thoroughly
  /// Remember that minutes and seconds are in range (+0, +59)
  Duration operator+(const Duration& other) const
  {
    return { toSeconds() + other.toSeconds() };
  }
  Duration operator-(const Duration& other) const
  {
    return { toSeconds() - other.toSeconds() };
  }
  QTime operator+(const QTime& other) const
  {
    return other.addSecs(toSeconds());
  }
  /// \brief division by a Duration yields a scalar float
  /// It's like a ratio between Durations
  double operator/(const Duration& other) const
  {
    return toSeconds() / other.toSeconds();
  }
  /// \brief Scaling operations
  /// Division and multiplication are very similar
  Duration operator/(const double& recip) const
  {
    return { toSeconds() / recip };
  }
  Duration operator*(const double& scale) const
  {
    return { toSeconds() * scale };
  }

  /// \brief ASSIGNMENT OPERATORS
  /// Modify the value to the left
  /// assignment addition
  void operator+=(const Duration& other)
  {
    Duration d = (*this) + other;
    (*this) = d;
  }
  /// \brief assignment subtraction
  void operator-=(const Duration& other)
  {
    Duration d = (*this) - other;
    (*this) = d;
  }
  /// \brief assignment scalar multiplication
  void operator*=(const double& scale)
  {
    Duration d = (*this) * scale;
    (*this) = d;
  }
  /// \brief assignment scalar division
  void operator/=(const double& recip)
  {
    Duration d = (*this) / recip;
    (*this) = d;
  }

  /// \brief COMPARATORS
  /// Remember: values are only compared to the resolution of 1 second
  bool operator<(const Duration& other) const
  {
    return toSeconds() < other.toSeconds();
  }
  bool operator>(const Duration& other) const
  {
    return (toSeconds() > other.toSeconds());
  }
  /// Rounds to the nearest second before comparing
  bool operator==(const Duration& other) const
  {
    return m_hours == other.hours() && m_minutes == other.minutes() &&
           m_seconds == other.seconds();
  }
  bool operator<=(const Duration& other) const
  {
    return (*this) < other || (*this) == other;
  }
  bool operator>=(const Duration& other) const
  {
    return (*this) > other || (*this) == other;
  }
};
}
}
}

#endif // NAVG_DURATION_H
