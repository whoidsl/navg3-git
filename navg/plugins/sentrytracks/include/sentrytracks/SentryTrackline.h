/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 8/8/18.
//

#ifndef NAVG_TRAILITEM_H
#define NAVG_TRAILITEM_H

#include "Duration.h"
#include <QGraphicsPathItem>
#include <dslmap/MapItem.h>

namespace navg {
namespace plugins {
namespace sentrytracks {

class SentryTrackline : public QGraphicsPathItem
{
public:
  SentryTrackline(QGraphicsItem* parent)
    : QGraphicsPathItem(parent)
  {
  }
  SentryTrackline(const QPainterPath path, const Duration _time,
                  const qreal _length, const QPointF _start, const QPointF _end,
                  QGraphicsItem* parent)
    : QGraphicsPathItem(path, parent)
    , m_time(_time)
    , m_length(_length)
    , m_start(_start)
    , m_end(_end)
  {
  }

  ~SentryTrackline() = default;
  int type() const override
  {
    return dslmap::MapItemType::SentryTracklineObjectType;
  }
  QPointF pointAtPercent(qreal t) const
  {
    if (t <= 0) {
      path().pointAtPercent(0);
    } else if (t >= 1) {
      path().pointAtPercent(1);
    }
    return path().pointAtPercent(t);
  }
  QPointF pointAtTime(Duration attime, qreal* atlength = nullptr) const
  {
    if (atlength) {
      *atlength += m_length * (attime / m_time);
    }
    return path().pointAtPercent(attime / m_time);
  }
  QPointF pointAtLength(qreal atlength, Duration* attime = nullptr) const
  {
    if (attime) {
      *attime += m_time * (atlength / m_length);
    }
    return path().pointAtPercent(atlength / m_length);
  }
  Duration time() { return m_time; }
  qreal length() { return m_length; }
  QPointF start() { return m_start; }
  QPointF end() { return m_end; }
private:
  Duration m_time;
  qreal m_length;
  QPointF m_start;
  QPointF m_end;
};
}
}
}
#endif // NAVG_TRAILITEM_H
