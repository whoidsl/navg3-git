/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 7/26/18.
//
// .sentry-Sentrytracks file parsing function adapted from
// navgV2.0/TrackLine.cpp
//

#include "SentryTracks.h"
#include "navg/NavGMapScene.h"
#include "ui_SentryTracks.h"
#include <dslmap/SymbolLayer.h>

#include <QFileDialog>
#include <QTcpSocket>

namespace navg {
namespace plugins {
namespace sentrytracks {

Q_LOGGING_CATEGORY(plugin_sentrytracks, "navg.plugins.sentrytracks")

SentryTracks::SentryTracks(QWidget* parent)
  : QWidget(parent)
  , ui(std::make_unique<Ui::SentryTracks>())
  , m_view(nullptr)
  , m_length(0)
  , m_time(0)
  , m_segments(0)
  , m_layer(nullptr)
  , m_A_seg(0)
  , m_B_seg(0)
  , m_A_len(0)
  , m_B_len(0)
  , m_A_time(0)
  , m_B_time(0)
  , m_settings(nullptr)
{
  ui->setupUi(this);
  ui->aSlider->setRange(0, 1000);
  ui->bSlider->setRange(0, 1000);
  ui->showSlider->setRange(0, 1000);
  connect(ui->aSlider, &QSlider::valueChanged, this,
          &SentryTracks::aSlider2Val);
  connect(ui->bSlider, &QSlider::valueChanged, this,
          &SentryTracks::bSlider2Val);
  connect(ui->showSlider, &QSlider::valueChanged, this,
          &SentryTracks::showSlider2Val);
  connect(ui->tracklineSelect, &QRadioButton::toggled, this,
          &SentryTracks::radioBtnCallback);
  connect(ui->lengthSelect, &QRadioButton::toggled, this,
          &SentryTracks::radioBtnCallback);
  connect(ui->timeSelect, &QRadioButton::toggled, this,
          &SentryTracks::radioBtnCallback);
  connect(ui->predictSelect, &QRadioButton::toggled, this,
          &SentryTracks::radioBtnCallback);
  ui->lengthSelect->toggle();

  m_act_add_tracks_from_file =
    new QAction(QIcon{ ":GIS-icons/24x24/layer-tracks-add.png" },
                QString{ "&Add Tracks from File..." }, this);
  m_act_add_tracks_from_file->setToolTip("Add a tracks layer from file");
  connect(m_act_add_tracks_from_file, &QAction::triggered, this,
          &SentryTracks::readSentryTracksFromFileDialog);

  m_act_add_tracks_from_navest =
    new QAction(QIcon{ ":GIS-icons/24x24/layer-tracks-add-from-db.png" },
                QString{ "&Read Tracks from Navest" }, this);
  m_act_add_tracks_from_navest->setToolTip(
    "Read trackline information from a running navest instance.");
  connect(m_act_add_tracks_from_navest, &QAction::triggered, this,
          &SentryTracks::readSentryTracksFromNavest);

  auto t = new QTimer(this);
  connect(t, &QTimer::timeout, this, &SentryTracks::updateTime);
  t->start(1000);
}

SentryTracks::~SentryTracks() = default;

QList<QAction*>
SentryTracks::pluginMenuActions()
{
  return { m_act_add_tracks_from_file, m_act_add_tracks_from_navest };
}

QList<QAction*>
SentryTracks::pluginToolbarActions()
{
  return { m_act_add_tracks_from_file, m_act_add_tracks_from_navest };
}

void
SentryTracks::setMapView(QPointer<dslmap::MapView> view)
{
  m_view = view;
  auto scene = qobject_cast<NavGMapScene*>(view->scene());
  if (scene) {
    connect(this, &SentryTracks::alvinXYOriginChanged, scene,
            &NavGMapScene::setAlvinXYOrigin);
  }
  // loadSentryTracksFile();
}

void
SentryTracks::loadSettings(QSettings* settings)
{
  if (settings == nullptr) {
    return;
  }
  if (!m_layer) {
    m_layer = SentryTracklineLayer::fromSettings(settings).release();
  }
  m_layer->loadSettings(settings);
}

void
SentryTracks::saveSettings(QSettings& settings)
{
  if (m_layer) {
    m_layer->saveSettings(&settings);
  }
}

void
SentryTracks::connectPlugin(const QObject* plugin, const QJsonObject& metadata)
{
  if (!plugin) {
    return;
  }

  const auto name =
    metadata.value("MetaData").toObject().value("name").toString();
  if (name.isEmpty()) {
    return;
  }

  if (name == "sentry_acomms") {
    if (!connect(plugin, SIGNAL(currentTracklineChanged(unsigned int)), this,
                 SLOT(setActiveTrackline(unsigned int)))) {
      qCCritical(plugin_sentrytracks, "Unable to connect to signal: "
                                      "SentryAcomms::currentTracklineChanged("
                                      "unsinged int)");
      Q_ASSERT(false);
    }
    qCInfo(plugin_sentrytracks,
           "Connected to SentryAcomms::currentTracklineChanged");
  }

  if (name == "navest") {
    if (!connect(plugin, SIGNAL(remoteAddressChanged(QHostAddress)), this,
                 SLOT(setNavestRemoteAddress(QHostAddress)))) {
      qCCritical(plugin_sentrytracks, "Unable to connect to signal: "
                                      "Navest::remoteAddressChanged("
                                      "QHostAddress)");
      Q_ASSERT(false);
    }
    qCInfo(plugin_sentrytracks, "Connected to Navest::remoteAddressChanged");

    if (!connect(plugin, SIGNAL(remoteTcpPortChanged(int)), this,
                 SLOT(setNavestRemoteTcpPort(int)))) {
      qCCritical(
        plugin_sentrytracks,
        "Unable to connect to signal: Navest::remoteTcpPortChanged(int)");
      Q_ASSERT(false);
    }
    qCInfo(plugin_sentrytracks, "Connected to Navest::remoteTcpPortChanged");

    int tcp_port = -1;
    if (!QMetaObject::invokeMethod(const_cast<QObject*>(plugin),
                                   "remoteTcpPort", Qt::DirectConnection,
                                   Q_RETURN_ARG(int, tcp_port))) {
      qCCritical(plugin_sentrytracks,
                 "Unable to get remote navest tcp port from navest plugin.");
      Q_ASSERT(false);
    }

    qCInfo(plugin_sentrytracks,
           "From navest plugin, read remote tcp port as: %d", tcp_port);
    setNavestRemoteTcpPort(tcp_port);

    QHostAddress tcp_address;
    if (!QMetaObject::invokeMethod(const_cast<QObject*>(plugin),
                                   "remoteAddress", Qt::DirectConnection,
                                   Q_RETURN_ARG(QHostAddress, tcp_address))) {
      qCCritical(plugin_sentrytracks,
                 "Unable to get remote navest address from navest plugin.");
      Q_ASSERT(false);
    }

    qCInfo(plugin_sentrytracks)
      << "From navest plugin, read remote tcp address as:" << tcp_address;
    setNavestRemoteAddress(tcp_address);
  }
}

void
SentryTracks::radioBtnCallback()
{
  if (ui->tracklineSelect->isChecked()) {
    if (m_layer) {
      m_layer->setSentrySegments(m_A_seg);
      m_layer->setDeltaSegments(m_B_seg);
    }
    ui->segLbl->show();
    ui->lengthLbl->hide();
    ui->timeLbl->hide();
    ui->aSeg->show();
    ui->aLength->hide();
    ui->aTime->hide();
    ui->bSeg->show();
    ui->bLength->hide();
    ui->bTime->hide();
    ui->showSeg->show();
    ui->showLength->hide();
    ui->showTime->hide();
    ui->aPredict->hide();
    ui->bPredict->hide();
    ui->spinPredict->hide();
  }
  if (ui->lengthSelect->isChecked()) {
    if (m_layer) {
      m_layer->setSentryLength(m_A_len);
      m_layer->setDeltaLength(m_B_len);
    }
    ui->segLbl->hide();
    ui->lengthLbl->show();
    ui->timeLbl->hide();
    ui->aSeg->hide();
    ui->aLength->show();
    ui->aTime->hide();
    ui->bSeg->hide();
    ui->bLength->show();
    ui->bTime->hide();
    ui->showSeg->hide();
    ui->showLength->show();
    ui->showTime->hide();
    ui->aPredict->hide();
    ui->bPredict->hide();
    ui->spinPredict->hide();
  }
  if (ui->timeSelect->isChecked()) {
    if (m_layer) {
      m_layer->setSentryTime(m_A_time);
      m_layer->setDeltaTime(m_B_time);
    }
    ui->segLbl->hide();
    ui->lengthLbl->hide();
    ui->timeLbl->show();
    ui->aSeg->hide();
    ui->aLength->hide();
    ui->aTime->show();
    ui->bSeg->hide();
    ui->bLength->hide();
    ui->bTime->show();
    ui->showSeg->hide();
    ui->showLength->hide();
    ui->showTime->show();
    ui->aPredict->hide();
    ui->bPredict->hide();
    ui->spinPredict->hide();
  }
  if (ui->predictSelect->isChecked()) {
    ui->segLbl->hide();
    ui->lengthLbl->hide();
    ui->timeLbl->show();
    ui->aSeg->hide();
    ui->aLength->hide();
    ui->aTime->hide();
    ui->bSeg->hide();
    ui->bLength->hide();
    ui->bTime->hide();
    ui->showSeg->hide();
    ui->showLength->hide();
    ui->showTime->hide();
    ui->aPredict->show();
    ui->bPredict->show();
    ui->spinPredict->show();
  }
}

void
SentryTracks::aSlider2Val(int val)
{
  float pcnt = (qreal)val / 1000.0;

  if (ui->tracklineSelect->isChecked()) {
    m_A_seg = pcnt * m_segments;
    m_layer->setSentrySegments(m_A_seg);
  }
  if (ui->lengthSelect->isChecked()) {
    m_A_len = pcnt * m_length;
    m_layer->setSentryLength(m_A_len);
  }
  if (ui->timeSelect->isChecked() || ui->predictSelect->isChecked()) {
    m_A_time = m_time * pcnt;
    m_layer->setSentryTime(m_A_time);
  }

  m_A_seg = m_layer->getSentrySegments();
  m_A_len = std::round(m_layer->getSentryLength());
  m_A_time = m_layer->getSentryTime();

  ui->aSeg->setText(QString::number(m_A_seg));
  ui->aLength->setText(QString::number(m_A_len));
  ui->aTime->setText(m_A_time.print());

  ui->bSeg->setText(QString::number(m_B_seg - m_A_seg));
  ui->bLength->setText(QString::number(m_B_len - m_A_len));
  ui->bTime->setText((m_B_time - m_A_time).print());

  updateTime();
}

void
SentryTracks::bSlider2Val(int val)
{
  float pcnt = (qreal)val / 1000.0;

  if (ui->tracklineSelect->isChecked()) {
    m_B_seg = pcnt * m_segments;
    m_layer->setDeltaSegments(m_B_seg);
  }
  if (ui->lengthSelect->isChecked()) {
    m_B_len = pcnt * m_length;
    m_layer->setDeltaLength(m_B_len);
  }
  if (ui->timeSelect->isChecked() || ui->predictSelect->isChecked()) {
    m_B_time = m_time * pcnt;
    m_layer->setDeltaTime(m_B_time);
  }

  m_B_seg = m_layer->getDeltaSegments();
  m_B_len = std::round(m_layer->getDeltaLength());
  m_B_time = m_layer->getDeltaTime();

  ui->bSeg->setText(QString::number(m_B_seg - m_A_seg));
  ui->bLength->setText(QString::number(m_B_len - m_A_len));
  ui->bTime->setText((m_B_time - m_A_time).print());

  updateTime();
}

void
SentryTracks::showSlider2Val(int val)
{
  float pcnt = (float)val / m_segments;
  ui->showSeg->setText(QString::number(val));
  ui->showLength->setText(QString::number(round(pcnt * m_length)));
  ui->showTime->setText((m_time * pcnt).print());
  if (m_layer == nullptr) {
    return;
  }
  m_layer->setShowLength(val);
}

bool
SentryTracks::readSentryTracksFromFile(const QString& filename)
{
  QFile trackline_file(filename);
  if (!trackline_file.open(QIODevice::ReadOnly | QIODevice::Text)) {
    qCWarning(plugin_sentrytracks) << "Unable to open tracks file:" << filename;
    return false;
  }

  return loadSentryTracksFile(trackline_file);
}

bool
SentryTracks::readSentryTracksFromNavest()
{

  if (m_navest_remote_address.isNull()) {
    qCWarning(plugin_sentrytracks,
              "Unable to connect to navest daemon:  no remote address set.");
    return false;
  }

  QTcpSocket socket;
  socket.connectToHost(m_navest_remote_address, m_navest_remote_tcp_port);
  if (!socket.waitForConnected(2000)) {
    qCWarning(plugin_sentrytracks)
      << "Unable to connect to remote navest instance at:"
      << m_navest_remote_address << m_navest_remote_tcp_port;
    return false;
  }

  socket.write("REQUEST_INI");
  if (!socket.waitForBytesWritten(2000)) {
    qCWarning(plugin_sentrytracks)
      << "Timed out sending trackline request to navest at:"
      << m_navest_remote_address << m_navest_remote_tcp_port;
    return false;
  }

  QByteArray buf;
  int length = -1;
  while (length < 0) {
    if (!socket.waitForReadyRead(2000)) {
      qCWarning(plugin_sentrytracks)
        << "Timed out waiting for full INI file from Navest";
      return false;
    }

    const int bytes_availble = socket.bytesAvailable();
    qCDebug(plugin_sentrytracks, "Reading %d bytes from navest....",
            bytes_availble);
    buf.append(socket.read(bytes_availble));
    length = buf.lastIndexOf("END_OF_TRACKS_FILE");
  }

  buf.truncate(length);

  const auto start_marker = QString{ "END_OF_DIVE_INI_FILE" };
  const int start = buf.lastIndexOf(start_marker);
  if (start < 0) {
    qCWarning(
      plugin_sentrytracks,
      "Unable to find 'END_OF_DIVE_INI_FILE' marker in data from navest");
    return false;
  }

  buf = buf.mid(start + start_marker.length());
  QBuffer track_buf(&buf);
  track_buf.open(QIODevice::ReadOnly);
  return loadSentryTracksFile(track_buf);
}

bool
SentryTracks::loadSentryTracksFile(QIODevice& buffer)
{
  // sentrytracks file parsing function adapted from navgV2.0/TrackLine.cpp
  Q_ASSERT(m_view != nullptr);
  auto my_scene = qobject_cast<dslmap::MapScene*>(m_view->mapScene());
  Q_ASSERT(my_scene != nullptr);

  QSettings settings;
  if (m_layer) {
    m_layer->saveSettings(settings);
  }
  auto layer = SentryTracklineLayer::fromSettings(&settings);

  bool gotOriginLat = false;
  bool gotOriginLon = false;
  bool gotStartLat = false;
  bool gotStartLon = false;
  bool gotStartXY = false;
  double missionStartX;
  double missionStartY;
  double originLon;
  double originLat;
  double startLon;
  double startLat;
  double spd(1.0);
  auto name = QString{};

  // first scan the entire file for the start coordinate
  while (!buffer.atEnd()) {
    QByteArray line = buffer.readLine().trimmed();
    if (line.startsWith("#START")) {
      missionStartX = line.split(' ')[1].toDouble();
      missionStartY = line.split(' ')[2].toDouble();
      gotStartXY = true;
    } else if (line.startsWith("#LATITUDE_ORIGIN")) {
      originLat = line.split('=')[1].toDouble();
      gotOriginLat = true;
    } else if (line.startsWith("#LONGITUDE_ORIGIN")) {
      originLon = line.split('=')[1].toDouble();
      gotOriginLon = true;
    } else if (line.startsWith("#LATITUDE_START")) {
      startLat = line.split('=')[1].toDouble();
      gotStartLat = true;
    } else if (line.startsWith("#LONGITUDE_START")) {
      startLon = line.split('=')[1].toDouble();
      gotStartLon = true;
    } else if (line.startsWith("#NAME")) {
      name = line.split(' ')[1];
    }

    if (gotStartXY && gotOriginLon && gotOriginLat && gotStartLat &&
        gotStartLon)
      break;
  }

  if (!gotStartXY || !gotOriginLat || !gotOriginLon || !gotStartLon ||
      !gotStartLat) {
    qWarning() << "SentryTracks file missing one of #START_XY, "
                  "#LATITUDE_ORIGIN, #LONGITUDE_ORIGIN"
                  "#LATITUDE_START, or #LONGITUDE_START";
    return false;
  } else {
    layer->setOrigin(originLon, originLat);
    emit alvinXYOriginChanged(QPointF{ originLon, originLat });
    qDebug() << "Mission X: " << missionStartX
             << " Mission Y: " << missionStartY;
    qDebug() << "Start X: " << startLon << " Start Y: " << startLat;
  }

  // Start at the top and add tracklines to the layer
  buffer.seek(0);
  while (!buffer.atEnd()) {
    QByteArray line = buffer.readLine().trimmed();

    if (line.startsWith("BF")) {
      QList<QByteArray> param = line.split(' ');
      if (10 == param.length()) {
        spd = param[3].toDouble(); // spd in m/s
      }
    }
    if (line.startsWith("TL")) {
      QList<QByteArray> coord = line.split(' ');
      if (6 == coord.length()) {

        double startX = missionStartX + coord[2].toDouble();
        double startY = missionStartY + coord[3].toDouble();
        double endX = missionStartX + coord[4].toDouble();
        double endY = missionStartY + coord[5].toDouble();

        double length =
          sqrt(pow(endX - startX, 2) + pow(endY - startY, 2)); // length in m
        Duration duration(length / spd);                       // duration in s

        QPointF start(startX, startY);
        QPointF end(endX, endY);
        layer->addSegment(start, end, length, duration);
      }
    }
  }
  // Remove the old layer, add the new layer and waypoints
  if (m_layer != nullptr) {
    my_scene->removeLayer(m_layer->id());
  }
  my_scene->addLayer(layer.get());
  m_layer = layer.release();
  m_layer->makeSentryMarker();
  m_layer->makeDeltaMarker();
  if (!name.isEmpty()) {
    m_layer->setName(name);
  } else {
    m_layer->setName(QStringLiteral("sentry_tracklines"));
  }

  // Updates to the UI
  m_length = m_layer->totalLength();
  m_segments = m_layer->totalSegments();
  m_time = m_layer->totalTime();
  ui->showSlider->setRange(0, m_segments);
  ui->aSlider->setValue(0);
  ui->bSlider->setValue(0);
  ui->showSlider->setValue(0);
  ui->lengthLbl->setText("Total Length: " + QString::number(m_length));
  ui->timeLbl->setText("Total Time: " + m_time.print());
  ui->segLbl->setText("Total Segments: " + QString::number(m_segments));

  return true;
}

void
SentryTracks::setActiveTrackline(unsigned int trackline)
{
  if (!m_layer) {
    return;
  }
  m_layer->setActiveTrackline(trackline);
}

void
SentryTracks::readSentryTracksFromFileDialog()
{
  auto filename = QFileDialog::getOpenFileName(
    this, tr("Open Sentry Tracks File"), {}, tr("Tracks Files (*.tracks)"));

  if (filename.isEmpty()) {
    return;
  }

  readSentryTracksFromFile(filename);
}

void
SentryTracks::updateTime()
{
  auto now = QTime::currentTime();
  auto predictA = now;
  ui->aPredict->setText(predictA.toString("hh:mm"));
  auto predictB = (m_B_time - m_A_time) * ui->spinPredict->value() + now;
  ui->bPredict->setText(predictB.toString("hh:mm"));
}

void
SentryTracks::setNavestRemoteAddress(QHostAddress address)
{
  m_navest_remote_address = address;
}

void
SentryTracks::setNavestRemoteTcpPort(int port)
{
  m_navest_remote_tcp_port = port;
}

} // namespace sentrytracks
} // namespace plugins
} // namespace navest
