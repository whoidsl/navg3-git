/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 7/26/18.
//

#include "SentryTracklineLayer.h"
#include "SentryTracklineLayerPropertiesWidget.h"

#include <QGraphicsScene>
#include <QGridLayout>

namespace navg {
namespace plugins {
namespace sentrytracks {
Q_LOGGING_CATEGORY(plugin_sentrytracks_layer,
                   "navg.plugin.sentrytracklinelayer")

class SentryTracklineLayerPrivate
{
public:
  explicit SentryTracklineLayerPrivate()
    : m_progress(0)
    , m_show_length(0)
    , m_pen_do(Qt::green, 3, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin)
    , m_pen_doing(Qt::yellow, 3, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin)
    , m_pen_done(Qt::red, 3, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin)
    , m_menu(new QMenu)
    , m_properties(new SentryTracklineLayerPropertiesWidget)
  {
    m_pen_do.setCosmetic(true);
    m_pen_doing.setCosmetic(true);
    m_pen_done.setCosmetic(true);
  }

  ~SentryTracklineLayerPrivate() = default;
  QList<SentryTrackline*> m_segments;
  int m_progress;
  int m_show_length;
  QRectF m_bounding_rect;
  QPen m_pen_do;
  QPen m_pen_doing;
  QPen m_pen_done;
  QScopedPointer<QMenu> m_menu;
  SentryTracklineLayerPropertiesWidget* m_properties;
  QPointF m_origin;
  std::shared_ptr<dslmap::Proj> m_proj;
  dslmap::MarkerItem* m_sentry_marker = nullptr;
  qreal m_sentry_length = 0;
  Duration m_sentry_time = 0;
  int m_sentry_segments = 0;
  std::shared_ptr<dslmap::ShapeMarkerSymbol> m_sentry_symbol;
  dslmap::MarkerItem* m_delta_marker = nullptr;
  qreal m_delta_length = 0;
  Duration m_delta_time = 0;
  int m_delta_segments = 0;
  std::shared_ptr<dslmap::ShapeMarkerSymbol> m_delta_symbol;
};

SentryTracklineLayer::SentryTracklineLayer(QGraphicsItem* parent)
  : dslmap::MapLayer(parent)
  , d_ptr(new SentryTracklineLayerPrivate)

{
  Q_D(SentryTracklineLayer);
  setFlag(ItemHasNoContents, true);
  setupMenu();
  d->m_properties->setLayer(this);
}

void
SentryTracklineLayer::setupMenu()
{
  Q_D(SentryTracklineLayer);
  d->m_menu->setTitle(name());
  auto action = d->m_menu->addAction(QStringLiteral("Visible"));

  action->setCheckable(true);
  action->setChecked(isVisible());
  connect(action, &QAction::toggled, this,
          [=](bool visible) { setVisible(visible); });
  d->m_menu->addAction(action);

  action = d->m_menu->addAction(QStringLiteral("Properties"));
  connect(action, &QAction::triggered, this, [=]() { showPropertiesDialog(); });
  d->m_menu->addAction(action);

  action = d->m_menu->addAction(QStringLiteral("Advance"));
  action->setToolTip("Advance trackline progress by one.");
  connect(action, &QAction::triggered, this,
          &SentryTracklineLayer::segmentDone);
}

SentryTracklineLayer::~SentryTracklineLayer()
{
  delete d_ptr;
}

void
SentryTracklineLayer::showPropertiesDialog()
{
  Q_D(SentryTracklineLayer);
  d->m_properties->setLayer(this);
  d->m_properties->show();
}

bool
SentryTracklineLayer::saveSettings(QSettings& settings)
{
  return saveSettings(&settings);
}
bool
SentryTracklineLayer::loadSettings(QSettings& settings)
{
  return loadSettings(&settings);
}

bool
SentryTracklineLayer::saveSettings(QSettings* settings)
{
  settings->setValue("name", name());
  Q_D(SentryTracklineLayer);
  savePenSettings(&d->m_pen_doing, settings, "pens/doing/");
  savePenSettings(&d->m_pen_done, settings, "pens/done/");
  savePenSettings(&d->m_pen_do, settings, "pens/do/");

  if (d->m_sentry_symbol) {
    d->m_sentry_symbol->saveSettings(*settings, "symbol/sentry/");
  }
  if (d->m_delta_symbol) {
    d->m_delta_symbol->saveSettings(*settings, "symbol/delta/");
  }

  return true;
}

bool
SentryTracklineLayer::loadSettings(QSettings* settings)
{
  setName(settings->value("name", "").toString());

  Q_D(SentryTracklineLayer);
  loadPenSettings(&d->m_pen_doing, settings, "pens/doing/");
  loadPenSettings(&d->m_pen_done, settings, "pens/done/");
  loadPenSettings(&d->m_pen_do, settings, "pens/do/");

  d->m_sentry_symbol =
    dslmap::ShapeMarkerSymbol::fromSettings(*settings, "symbol/delta/");
  d->m_delta_symbol =
    dslmap::ShapeMarkerSymbol::fromSettings(*settings, "symbol/sentry/");
  return true;
}

std::unique_ptr<SentryTracklineLayer>
SentryTracklineLayer::fromSettings(QSettings* settings)
{
  auto layer = std::make_unique<SentryTracklineLayer>();
  layer->loadSettings(settings);
  return layer;
}

void
SentryTracklineLayer::savePenSettings(QPen* pen_ptr, QSettings* settings,
                                      QString prefix)
{
  settings->setValue(prefix + "width", pen_ptr->width());
  settings->setValue(prefix + "color", pen_ptr->color().name());
  settings->setValue(
    prefix + "style",
    QMetaEnum::fromType<Qt::PenStyle>().valueToKey(pen_ptr->style()));
}
void
SentryTracklineLayer::loadPenSettings(QPen* pen_ptr, QSettings* settings,
                                      QString prefix)
{
  bool ok = false;
  pen_ptr->setWidth(settings->value(prefix + "width").toInt());

  auto value = settings->value(prefix + "color");
  if (value.isValid()) {
    const auto color = QColor{ value.toString() };
    if (color.isValid()) {
      pen_ptr->setColor(color);
    }
  }

  value = settings->value(prefix + "style");
  if (value.isValid()) {
    const auto key = value.toString().toStdString();
    const auto style =
      QMetaEnum::fromType<Qt::PenStyle>().keyToValue(key.data(), &ok);
    if (ok) {
      pen_ptr->setStyle(static_cast<Qt::PenStyle>(style));
    }
  }
}

void
SentryTracklineLayer::setOrigin(qreal x, qreal y)
{
  Q_D(SentryTracklineLayer);
  auto tracks_proj = dslmap::Proj::alvinXYFromOrigin(x, y, nullptr);
  if (tracks_proj) {
    d->m_origin = QPointF{ x, y };
    setProjection(tracks_proj);
  }
}

QPointF
SentryTracklineLayer::getOrigin()
{
  Q_D(SentryTracklineLayer);
  return d->m_origin;
}

void
SentryTracklineLayer::setProjection(std::shared_ptr<const dslmap::Proj> proj)
{
  Q_D(SentryTracklineLayer);
  if (!proj) {
    return;
  }

  auto currentProj = projectionPtr();
  if (currentProj->isSame(*proj)) {
    return;
  }
  // Keep a copy to make the data
  auto old_segs = d->m_segments;

  // Remove all of the old segments
  for (auto i : d->m_segments) {
    removeSegment(i);
  }

  // Remake the segments with the proper coordinates
  for (auto i : old_segs) {
    auto start = i->start();
    auto end = i->end();
    auto start_x = start.x();
    auto start_y = start.y();
    auto end_x = end.x();
    auto end_y = end.y();
    currentProj->transformTo(*proj, 1, &start_x, &start_y);
    currentProj->transformTo(*proj, 1, &end_x, &end_y);
    start.setX(start_x);
    start.setY(start_y);
    end.setX(end_x);
    end.setY(end_y);
    addSegment(start, end, i->length(), i->time());
  }

  // Remake the markers
  makeSentryMarker();
  makeDeltaMarker();

  MapLayer::setProjection(proj);
}

SentryTrackline*
SentryTracklineLayer::addSegment(const QPointF& start, const QPointF& end,
                                 const qreal length, const Duration duration)
{
  Q_D(SentryTracklineLayer);
  auto proj = projectionPtr();
  if (!proj) {
    return nullptr;
  }

  const auto start_map = mapFromScene(start.x(), start.y());
  const auto end_map = mapFromScene(end.x(), end.y());
  QPainterPath seg(start_map);
  seg.lineTo(end_map);

  auto* item = new SentryTrackline(seg, duration, length, start, end, this);
  item->setFlag(ItemIsSelectable, true);
  if (d->m_progress < d->m_segments.size())
    item->setPen(d->m_pen_do);
  else if (d->m_progress == d->m_segments.size())
    item->setPen(d->m_pen_doing);
  else
    item->setPen(d->m_pen_done);
  item->setVisible(isVisible());
  d->m_segments.append(item);

  updateBoundingRect();
  return item;
}

bool
SentryTracklineLayer::removeSegment(SentryTrackline* seg)
{
  Q_D(SentryTracklineLayer);
  if (seg == nullptr) {
    qDebug() << "Failed to remove seg";
    return false;
  }

  if (!d->m_segments.contains(seg)) {
    qDebug() << "m_segments doesn't contain seg";
    return false;
  }

  d->m_segments.removeOne(seg);

  auto scene = seg->scene();
  if (scene != nullptr) {
    scene->removeItem(seg);
  }

  seg->setParentItem(nullptr);

  return true;
}

void
SentryTracklineLayer::paint(QPainter*, const QStyleOptionGraphicsItem*,
                            QWidget*)
{
}

void
SentryTracklineLayer::segmentDone()
{
  Q_D(SentryTracklineLayer);
  setActiveTrackline(d->m_progress + 1);
}

void
SentryTracklineLayer::updateBoundingRect()
{
  Q_D(SentryTracklineLayer);
  QRectF bounding{};
  for (auto i : d->m_segments) {
    bounding |= i->boundingRect();
  }
  prepareGeometryChange();
  d->m_bounding_rect = bounding;
}

QRectF
SentryTracklineLayer::boundingRect() const
{
  const Q_D(SentryTracklineLayer);
  return d->m_bounding_rect;
}

void
SentryTracklineLayer::setPenDo(const QPen& pen)
{
  Q_D(SentryTracklineLayer);
  d->m_pen_do = pen;
  for (int i = d->m_progress + 1; i < d->m_segments.size(); i++) {
    d->m_segments[i]->setPen(pen);
  }
}

void
SentryTracklineLayer::setPenDoing(const QPen& pen)
{
  Q_D(SentryTracklineLayer);
  d->m_pen_doing = pen;
  d->m_segments[d->m_progress]->setPen(pen);
}

void
SentryTracklineLayer::setPenDone(const QPen& pen)
{
  Q_D(SentryTracklineLayer);
  d->m_pen_done = pen;
  for (int i = 0; i < d->m_progress; i++) {
    d->m_segments[i]->setPen(pen);
  }
}

QPen
SentryTracklineLayer::penDo()
{
  Q_D(SentryTracklineLayer);
  return d->m_pen_do;
}

QPen
SentryTracklineLayer::penDoing()
{
  Q_D(SentryTracklineLayer);
  return d->m_pen_doing;
}

QPen
SentryTracklineLayer::penDone()
{
  Q_D(SentryTracklineLayer);
  return d->m_pen_done;
}

void
SentryTracklineLayer::setName(const QString& name_) noexcept
{
  if (name_ == name()) {
    return;
  }
  setObjectName(name_);
  Q_D(SentryTracklineLayer);
  d->m_menu->setTitle(name_);
}

QMenu*
SentryTracklineLayer::contextMenu()
{
  Q_D(SentryTracklineLayer);
  return d->m_menu.data();
}

qreal
SentryTracklineLayer::totalLength() const
{
  const Q_D(SentryTracklineLayer);
  qreal len = 0;
  for (auto seg : d->m_segments) {
    len += seg->length();
  }
  return len;
}

Duration
SentryTracklineLayer::totalTime() const
{
  const Q_D(SentryTracklineLayer);
  Duration time = 0;
  for (auto seg : d->m_segments) {
    time += seg->time();
  }
  return time;
}

int
SentryTracklineLayer::totalSegments() const
{
  const Q_D(SentryTracklineLayer);
  return d->m_segments.size();
}

void
SentryTracklineLayer::setShowLength(int length)
{
  Q_D(SentryTracklineLayer);
  length = length ? length : totalSegments();
  // Case 0: length hasn't changed, so do nothing
  if (length == d->m_show_length) {
    return;
  }
  // Case 1: length got longer, so show more
  for (int i = d->m_progress + d->m_show_length;
       i < totalSegments() && i < d->m_progress + length; i++) {
    d->m_segments[i]->setOpacity(1.0);
  }
  // Case 2: length got shorter, so hide more
  for (int i = d->m_progress + length; i < totalSegments(); i++) {
    d->m_segments[i]->setOpacity(0.3);
  }
  d->m_show_length = length;
}

QPointF
SentryTracklineLayer::pointAtLength(qreal length, QString marker)
{
  Q_D(SentryTracklineLayer);
  QPointF pt{};
  if (length < 0) {
    return d->m_segments[0]->pointAtPercent(0);
  } else if (length > totalLength()) {
    return d->m_segments[totalSegments() - 1]->pointAtPercent(1);
  }
  Duration time{ 0 };
  qreal start{ 0 };
  qreal end{ 0 };
  for (int i = 0; i < d->m_segments.size(); i++) {
    auto seg = d->m_segments[i];
    end += seg->length();
    if (start <= length && end >= length) {
      pt = seg->pointAtLength(length - start, &time);
      if (marker == "sentry") {
        d->m_sentry_segments = i;
        d->m_sentry_length = length;
        d->m_sentry_time = time;
      }
      if (marker == "delta") {
        d->m_delta_segments = i;
        d->m_delta_length = length;
        d->m_delta_time = time;
      }
      break;
    }
    start = end;
    time += seg->time();
  }
  if (pt == QPointF{}) {
    return d->m_segments[totalSegments() - 1]->pointAtPercent(1);
  }
  return pt;
}

QPointF
SentryTracklineLayer::pointAtTime(Duration time, QString marker)
{
  Q_D(SentryTracklineLayer);
  QPointF pt{};
  if (time < Duration{ 0 }) {
    return d->m_segments[0]->pointAtPercent(0);
  } else if (time > totalTime()) {
    return d->m_segments[totalSegments() - 1]->pointAtPercent(1);
  }
  qreal length{ 0 };

  Duration start;
  Duration end;
  for (int i = 0; i < d->m_segments.size(); i++) {
    auto seg = d->m_segments[i];
    end += seg->time();
    if (start <= time && time <= end) {
      pt = seg->pointAtTime(time - start, &length);
      if (marker == "sentry") {
        d->m_sentry_segments = i;
        d->m_sentry_length = length;
        d->m_sentry_time = time;
      }
      if (marker == "delta") {
        d->m_delta_segments = i;
        d->m_delta_length = length;
        d->m_delta_time = time;
      }
      break;
    }
    start = end;
    length += seg->length();
  }
  if (pt == QPointF{}) {
    return d->m_segments[totalSegments() - 1]->pointAtPercent(1);
  }
  return pt;
}

QPointF
SentryTracklineLayer::pointAtSeg(int seg, QString marker)
{
  Q_D(SentryTracklineLayer);
  if (seg < 0 || seg > totalSegments()) {
    return QPointF{};
  }
  qreal length{ 0 };
  Duration time{ 0 };
  for (int i = 0; i < seg; i++) {
    length += d->m_segments[i]->length();
    time += d->m_segments[i]->time();
  }
  if (marker == "sentry") {
    d->m_sentry_segments = seg;
    d->m_sentry_length = length;
    d->m_sentry_time = time;
  }
  if (marker == "delta") {
    d->m_delta_segments = seg;
    d->m_delta_time = time;
    d->m_delta_length = length;
  }
  if (seg == totalSegments()) {
    return d->m_segments[seg - 1]->pointAtPercent(1);
  }
  return d->m_segments[seg]->pointAtPercent(0);
}

void
SentryTracklineLayer::makeSentryMarker()
{
  Q_D(SentryTracklineLayer);
  if (d->m_segments.empty()) {
    return;
  }
  if (d->m_sentry_marker != nullptr) {
    delete d->m_sentry_marker;
  } else {
    d->m_sentry_symbol = std::make_shared<dslmap::ShapeMarkerSymbol>(
      dslmap::ShapeMarkerSymbol::Shape::Cross, 10);
    d->m_sentry_symbol->setOutlineColor(Qt::red);
    d->m_sentry_symbol->setLabelColor(Qt::white);
  }

  d->m_sentry_marker = new dslmap::MarkerItem(this);
  d->m_sentry_marker->setParentItem(this);
  d->m_sentry_marker->setSymbol(d->m_sentry_symbol);
  setSentryLength(d->m_sentry_length);
}

void
SentryTracklineLayer::makeDeltaMarker()
{
  Q_D(SentryTracklineLayer);
  if (d->m_segments.empty()) {
    return;
  }

  if (d->m_delta_marker == nullptr) {
    d->m_delta_symbol = std::make_shared<dslmap::ShapeMarkerSymbol>(
      dslmap::ShapeMarkerSymbol::Shape::Square, 10);
    d->m_sentry_symbol->setOutlineColor(Qt::red);
    d->m_sentry_symbol->setLabelColor(Qt::white);
  }
  delete d->m_delta_marker;

  d->m_delta_marker = new dslmap::MarkerItem(this);
  d->m_delta_marker->setParentItem(this);
  d->m_delta_marker->setSymbol(d->m_delta_symbol);
  setDeltaLength(d->m_delta_length);
}

void
SentryTracklineLayer::setSentryLength(qreal length)
{
  Q_D(SentryTracklineLayer);
  if (d->m_sentry_marker == nullptr) {
    return;
  }
  d->m_sentry_marker->setPos(pointAtLength(length, "sentry"));
  d->m_sentry_marker->setLabelText("Length " + QString::number(length) + "m");

  if (d->m_delta_marker == nullptr) {
    return;
  }
  d->m_delta_marker->setLabelText(
    "Length " + QString::number(getDeltaLength() - length) + "m");
}

void
SentryTracklineLayer::setDeltaLength(qreal length)
{
  Q_D(SentryTracklineLayer);
  if (d->m_delta_marker == nullptr) {
    return;
  }
  d->m_delta_marker->setPos(pointAtLength(length, "delta"));
  d->m_delta_marker->setLabelText(
    "Length " + QString::number(length - getSentryLength()) + "m");

  if (d->m_sentry_marker == nullptr) {
    return;
  }
  d->m_sentry_marker->setLabelText("Length " +
                                   QString::number(getSentryLength()) + "m");
}

void
SentryTracklineLayer::setSentryTime(Duration time)
{
  Q_D(SentryTracklineLayer);
  if (d->m_sentry_marker == nullptr) {
    return;
  }
  d->m_sentry_marker->setPos(pointAtTime(time, "sentry"));
  d->m_sentry_marker->setLabelText("Time " + time.print());

  if (d->m_delta_marker == nullptr) {
    return;
  }
  d->m_delta_marker->setLabelText("Time " + (getDeltaTime() - time).print());
}

void
SentryTracklineLayer::setDeltaTime(Duration time)
{
  Q_D(SentryTracklineLayer);
  if (d->m_delta_marker == nullptr) {
    return;
  }
  d->m_delta_marker->setPos(pointAtTime(time, "delta"));
  d->m_delta_marker->setLabelText("Time " + (time - getSentryTime()).print());

  if (d->m_sentry_marker == nullptr) {
    return;
  }
  d->m_sentry_marker->setLabelText("Time " + getSentryTime().print());
}

void
SentryTracklineLayer::setSentrySegments(int seg)
{
  Q_D(SentryTracklineLayer);
  if (d->m_sentry_marker == nullptr) {
    return;
  }
  d->m_sentry_marker->setPos(pointAtSeg(seg, "sentry"));
  d->m_sentry_marker->setLabelText("Seg " + QString::number(seg));

  if (d->m_delta_marker == nullptr) {
    return;
  }
  d->m_delta_marker->setLabelText("Seg " +
                                  QString::number(getDeltaSegments() - seg));
}

void
SentryTracklineLayer::setDeltaSegments(int seg)
{
  Q_D(SentryTracklineLayer);
  if (d->m_delta_marker == nullptr) {
    return;
  }
  d->m_delta_marker->setPos(pointAtSeg(seg, "delta"));
  d->m_delta_marker->setLabelText("Seg " +
                                  QString::number(seg - getSentrySegments()));

  if (d->m_sentry_marker == nullptr) {
    return;
  }
  d->m_sentry_marker->setLabelText("Seg " +
                                   QString::number(getSentrySegments()));
}

qreal
SentryTracklineLayer::getSentryLength() const
{
  const Q_D(SentryTracklineLayer);
  return d->m_sentry_length;
}

qreal
SentryTracklineLayer::getDeltaLength() const
{
  const Q_D(SentryTracklineLayer);
  return d->m_delta_length;
}

Duration
SentryTracklineLayer::getSentryTime() const
{
  const Q_D(SentryTracklineLayer);
  return d->m_sentry_time;
}

Duration
SentryTracklineLayer::getDeltaTime() const
{
  const Q_D(SentryTracklineLayer);
  return d->m_delta_time;
}

int
SentryTracklineLayer::getSentrySegments() const
{
  const Q_D(SentryTracklineLayer);
  return d->m_sentry_segments;
}

int
SentryTracklineLayer::getDeltaSegments() const
{
  const Q_D(SentryTracklineLayer);
  return d->m_delta_segments;
}

std::shared_ptr<dslmap::ShapeMarkerSymbol>
SentryTracklineLayer::getSentrySymbol()
{
  Q_D(SentryTracklineLayer);
  return d->m_sentry_symbol;
}

std::shared_ptr<dslmap::ShapeMarkerSymbol>
SentryTracklineLayer::getDeltaSymbol()
{
  Q_D(SentryTracklineLayer);
  return d->m_delta_symbol;
}

void
SentryTracklineLayer::setActiveTrackline(unsigned int trackline)
{
  // Tracklines coming from Sentry are 1-based whereas this plugin
  // seems to be 0-based.  Indexing *should* be what's changed, but
  // there's too many functions with too many magic-number offsets.
  // We'll just adjust here.
  auto progress = trackline > 0 ? trackline - 1 : 0;

  Q_D(SentryTracklineLayer);
  if (static_cast<unsigned int>(d->m_progress) == progress) {
    return;
  }

  // trackline value > our parsed segments, mark all done.
  if (progress >= static_cast<unsigned int>(totalSegments())) {
    for (auto& seg : d->m_segments) {
      seg->setPen(d->m_pen_done);
    }
    return;
  }

  auto i = 0u;
  for (auto& seg : d->m_segments) {

    if (d->m_show_length == 0 ||
        static_cast<unsigned int>(progress + d->m_show_length) > i) {
      seg->setOpacity(1.0);
    } else {
      seg->setOpacity(0.3);
    }

    if (i < progress) {
      seg->setPen(d->m_pen_done);
    } else if (i == progress) {
      seg->setPen(d->m_pen_doing);
    } else {
      seg->setPen(d->m_pen_do);
    }
    ++i;
  }
  d->m_progress = progress;
}
} // namespace sentry-sentry-tracks
} // namespace plugins
} // namespace navest
