/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 9/17/18.
//

#include "SentryTracklineLayerPropertiesWidget.h"
#include "SentryTracklineLayer.h"
#include "ui_SentryTracklineLayerPropertiesWidget.h"

#include <dslmap/SymbolPropertiesWidget.h>
namespace navg {
namespace plugins {
namespace sentrytracks {

SentryTracklineLayerPropertiesWidget::SentryTracklineLayerPropertiesWidget(
  QWidget* parent)
  : QWidget(parent)
  , ui(new Ui::SentryTracklineLayerPropertiesWidget)
{
  ui->setupUi(this);
  setWindowTitle("SentryTracks Layer Properties");
  ui->penDo->setName("Future");
  ui->penDoing->setName("Current");
  ui->penDone->setName("Completed");
  ui->symbolSentry->setName("Sentry");
  ui->symbolDelta->setName("Delta");
}

SentryTracklineLayerPropertiesWidget::~SentryTracklineLayerPropertiesWidget()
{
  delete ui;
}

void
SentryTracklineLayerPropertiesWidget::setLayer(SentryTracklineLayer* layer)
{
  ui->general->setLayer(qobject_cast<dslmap::MapLayer*>(layer));
  ui->penDo->setPen(layer->penDo());
  ui->penDoing->setPen(layer->penDoing());
  ui->penDone->setPen(layer->penDone());

  std::shared_ptr<dslmap::ShapeMarkerSymbol> sentry_symbol =
    layer->getSentrySymbol();
  //  auto sentry_symbol_ptr = sentry_symbol.get();
  if (sentry_symbol != nullptr) {
    ui->symbolSentry->setOutlineVisible(sentry_symbol->outlineStyle() !=
                                        Qt::PenStyle::NoPen);
    ui->symbolSentry->setFillVisible(sentry_symbol->fillStyle() !=
                                     Qt::BrushStyle::NoBrush);
    ui->symbolSentry->setOutlineColor(sentry_symbol->outlineColor());
    ui->symbolSentry->setFillColor(sentry_symbol->fillColor());
    ui->symbolSentry->setLabelColor(sentry_symbol->labelColor());
    ui->symbolSentry->setLabelVisible(sentry_symbol->isLabelVisible());
    ui->symbolSentry->setShape(sentry_symbol->shape());
    ui->symbolSentry->setSymbolSize(sentry_symbol->size());

    connect(ui->symbolSentry,
            &dslmap::SymbolPropertiesWidget::labelVisibleChanged,
            [=](bool visible) { sentry_symbol->setLabelVisible(visible); });
    connect(ui->symbolSentry,
            &dslmap::SymbolPropertiesWidget::fillVisibleChanged,
            [=](bool visible) {
              if (visible)
                sentry_symbol->setFillStyle(Qt::BrushStyle::SolidPattern);
              else
                sentry_symbol->setFillStyle(Qt::BrushStyle::NoBrush);
            });
    connect(ui->symbolSentry,
            &dslmap::SymbolPropertiesWidget::outlineVisibleChanged,
            [=](bool visible) {
              if (visible)
                sentry_symbol->setOutlineStyle(Qt::PenStyle::SolidLine);
              else
                sentry_symbol->setOutlineStyle(Qt::PenStyle::NoPen);
            });
    connect(ui->symbolSentry,
            &dslmap::SymbolPropertiesWidget::outlineColorChanged,
            [=](QColor color) { sentry_symbol->setOutlineColor(color); });
    connect(ui->symbolSentry,
            &dslmap::SymbolPropertiesWidget::labelColorChanged,
            [=](QColor color) { sentry_symbol->setLabelColor(color); });
    connect(ui->symbolSentry, &dslmap::SymbolPropertiesWidget::fillColorChanged,
            [=](QColor color) { sentry_symbol->setFillColor(color); });
    connect(ui->symbolSentry, &dslmap::SymbolPropertiesWidget::shapeChanged,
            [=](dslmap::ShapeMarkerSymbol::Shape shape, qreal size) {
              sentry_symbol->setShape(shape, size);
            });
  }

  std::shared_ptr<dslmap::ShapeMarkerSymbol> delta_symbol =
    layer->getDeltaSymbol();
  if (delta_symbol != nullptr) {
    ui->symbolDelta->setOutlineColor(delta_symbol->outlineColor());
    ui->symbolDelta->setFillColor(delta_symbol->fillColor());
    ui->symbolDelta->setLabelColor(delta_symbol->labelColor());
    ui->symbolDelta->setLabelVisible(delta_symbol->isLabelVisible());
    ui->symbolDelta->setOutlineVisible(delta_symbol->outlineStyle() !=
                                       Qt::PenStyle::NoPen);
    ui->symbolDelta->setFillVisible(delta_symbol->fillStyle() !=
                                    Qt::BrushStyle::NoBrush);
    ui->symbolDelta->setShape(delta_symbol->shape());
    ui->symbolDelta->setSymbolSize(delta_symbol->size());

    connect(ui->symbolDelta,
            &dslmap::SymbolPropertiesWidget::labelVisibleChanged,
            [=](bool visible) { delta_symbol->setLabelVisible(visible); });
    connect(ui->symbolDelta,
            &dslmap::SymbolPropertiesWidget::fillVisibleChanged,
            [=](bool visible) {
              if (visible)
                delta_symbol->setFillStyle(Qt::BrushStyle::SolidPattern);
              else
                delta_symbol->setFillStyle(Qt::BrushStyle::NoBrush);
            });
    connect(ui->symbolDelta,
            &dslmap::SymbolPropertiesWidget::outlineVisibleChanged,
            [=](bool visible) {
              if (visible)
                delta_symbol->setOutlineStyle(Qt::PenStyle::SolidLine);
              else
                delta_symbol->setOutlineStyle(Qt::PenStyle::NoPen);
            });
    connect(ui->symbolDelta,
            &dslmap::SymbolPropertiesWidget::outlineColorChanged,
            [=](QColor color) { delta_symbol->setOutlineColor(color); });
    connect(ui->symbolDelta, &dslmap::SymbolPropertiesWidget::labelColorChanged,
            [=](QColor color) { delta_symbol->setLabelColor(color); });
    connect(ui->symbolDelta, &dslmap::SymbolPropertiesWidget::fillColorChanged,
            [=](QColor color) { delta_symbol->setFillColor(color); });
    connect(ui->symbolDelta, &dslmap::SymbolPropertiesWidget::shapeChanged,
            [=](dslmap::ShapeMarkerSymbol::Shape shape, qreal size) {
              delta_symbol->setShape(shape, size);
            });
  }

  connect(ui->penDo, &dslmap::PenPropertiesWidget::penChanged, layer,
          &SentryTracklineLayer::setPenDo);
  connect(ui->penDoing, &dslmap::PenPropertiesWidget::penChanged, layer,
          &SentryTracklineLayer::setPenDoing);
  connect(ui->penDone, &dslmap::PenPropertiesWidget::penChanged, layer,
          &SentryTracklineLayer::setPenDone);

  //  connect(ui->symbolSentry, &SymbolPropertiesWidget::)
}

} // namespaces
}
}
