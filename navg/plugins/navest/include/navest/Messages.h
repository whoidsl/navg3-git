/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 9/28/17.
//

#ifndef NAVG_NAVESTMESSAGES_H
#define NAVG_NAVESTMESSAGES_H

#include <QDateTime>
#include <QString>
#include <QtCore>

namespace navg {

enum class NavestMessageType
{
  SPS,
  VFR,
  VPR,
  OCT,
  DDD,
  NDS,
  DEP,
  RCM,
  TDR,
  Unknown,
  Invalid,
};

struct NavestMessageSps
{
  double lat;
  double lon;
  double z;
  int beacon_id;

  static QVariant parse(const QByteArrayList& fields);
};

struct NavestMessageVfr
{
  QDateTime timestamp;
  int mode;
  int vehicle_number;
  QString fix_source;
  double lon;
  double lat;
  double depth;
  double alt;
  int depth_or_altitude;
  double speed_over_ground;
  double course_over_ground;

  static QVariant parse(const QByteArrayList& fields);
};

struct NavestMessageVpr
{
  QString vehicle_name;
  int vehicle_number;
  double lon;
  double lat;
  double depth;
  double heading;

  static QVariant parse(const QByteArrayList& fields);
};

struct NavestMessageOct
{
  QString platform;
  double heading;
  double pitch;
  double roll;

  static QVariant parse(const QByteArrayList& fields);
};

struct NavestMessageNds
{
  unsigned long availble;
  unsigned long free;
  double percent_full;

  static QVariant parse(const QByteArrayList& fields);
};

struct NavestMessageDep
{
  QString vehicle_name;
  int paro_number;
  double depth;

  static QVariant parse(const QByteArrayList& fields);
};

struct NavestMessageRcm
{
  double lat;
  double lon;
  double range;
  int vehicle_number;
  double depth;

  static QVariant parse(const QByteArrayList& fields);
};

struct NavestMessageTdr
{
  double time_since_reset;

  static QVariant parse(const QByteArrayList& fields);
};

struct NavestMessageDdd
{
  QDateTime timestamp;
  double roll;
  double pitch;
  double heading;
  double beam_ranges[4];
  double beam_velocities[4];
  double alt;
  int bt_beams;
  int wt_beams;
  QString message;

  static QVariant parse(const QByteArrayList& fields);
};

QPair<NavestMessageType, QVariant> parseNavestMessage(
  const QByteArray& message);
}

Q_DECLARE_METATYPE(navg::NavestMessageType);
Q_DECLARE_METATYPE(navg::NavestMessageSps);
Q_DECLARE_METATYPE(navg::NavestMessageVfr);
Q_DECLARE_METATYPE(navg::NavestMessageVpr);
Q_DECLARE_METATYPE(navg::NavestMessageOct);
Q_DECLARE_METATYPE(navg::NavestMessageNds);
Q_DECLARE_METATYPE(navg::NavestMessageDep);
Q_DECLARE_METATYPE(navg::NavestMessageRcm);
Q_DECLARE_METATYPE(navg::NavestMessageTdr);
Q_DECLARE_METATYPE(navg::NavestMessageDdd);

#endif // NAVG_NAVESTMESSAGES_H
