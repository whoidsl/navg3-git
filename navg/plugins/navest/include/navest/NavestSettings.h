/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef NAVG_PLUGINNAVESTSETTINGS_H
#define NAVG_PLUGINNAVESTSETTINGS_H

#include <navg/NavG.h>

#include <QDialog>
#include <QHostAddress>

#include <memory>

namespace navg {
namespace plugins {
namespace navest {

namespace Ui {
class NavestSettings;
}

class NavestSettings : public QDialog
{
  Q_OBJECT

public:
  using VehicleNumbers = QSet<int>;

  explicit NavestSettings(QWidget* parent = 0);
  ~NavestSettings();

  /// \brief Set the listen UDP port
  void setListenUdpPort(int port);

  /// \breif get the listen UDP port
  int listenUdpPort() const;

  /// \breif Set the remote UDP port
  void setRemoteUdpPort(int port);

  /// \breif Get the remote UDP port
  int remoteUdpPort() const;

  /// \breif Set the remote TCP port
  void setRemoteTcpPort(int port);

  /// \breif Get the remote TCP port
  int remoteTcpPort() const;

  /// \brief Set the remote host
  void setRemoteAddress(QHostAddress& address);

  /// \breif Get the remote host
  QHostAddress remoteAddress() const;

  /// \brief A list of beacon ID's to ignore
  ///
  /// \return
  VehicleNumbers ignoredVehicleNumbers() const;

  /// \brief Set the beacon ids
  ///
  /// \param ids
  /// \return
  void setIgnoredVehicleNumbers(
    const NavestSettings::VehicleNumbers& ids) noexcept;

  /// \brief Return a set of beacon ids from a QString
  static VehicleNumbers beaconIdsFromString(const QString& list) noexcept;

  /// \brief Return a set of beacon ids from a QString
  static QString beaconIdsToString(const VehicleNumbers& list) noexcept;

private:
  std::unique_ptr<Ui::NavestSettings> ui;
};

} // namespace navest
} // namespace plugins
} // namespace navg
#endif // NAVG_PLUGINNAVESTSETTINGS_H
