/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 8/9/18.
//

#ifndef NAVG_LAYEROPTIONS_H
#define NAVG_LAYEROPTIONS_H

// #include "navg/plugins/navest/NavestLayer.h"

#include <QPointer>
#include <QWidget>

#include <memory>

class QStandardItemModel;

namespace navg {
namespace plugins {
namespace navest {

class NavestLayer;

namespace Ui {
class LayerOptions;
}

class LayerOptions : public QWidget
{
  Q_OBJECT

  
public:
  explicit LayerOptions(NavestLayer* layer, bool beacononly = false,
                        QWidget* parent = nullptr);
  ~LayerOptions() override;
public slots:
  void getSourceExternalFix(const QString& type);
  						
private slots:
  void addSolution(const QString& solution);
  void setSourceExternalFix(const bool fix);

private:
  std::unique_ptr<Ui::LayerOptions> ui;
  QPointer<NavestLayer> m_layer;
  QStandardItemModel* m_trail_source_model;
  bool m_beacononly;

};
}
}
}

#endif // NAVG_LAYEROPTIONS_H
