/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 8/2/18.
//

#ifndef NAVG_NAVESTLAYER_H
#define NAVG_NAVESTLAYER_H

#include <dslmap/MapLayer.h>
#include <dslmap/MarkerItem.h>

#include <QDateTime>

namespace dslmap {
class BeaconLayer;
class MarkerSymbol;
}

namespace navg {
namespace plugins {
namespace navest {

const auto SENTRY_MARKER_STRING =
  QString{ "LINESTRING(0 -7, 2.0 -4.0, 4.0 -4.0, 4.0 -3.0, 2.0 -2.0, 2.0 2.0, "
           "4.0 2.0, 4.0 3.0, 2.0 4.0,  1.0 5.5, 0.0 6.0, -1.0 5.5, -2.0 4.0, "
           "-4.0 3.0, -4.0 2.0, -2.0  2.0, -2.0 -2.0, -4.0 -3.0, -4.0 -4.0, "
           "-2.0 -4.0)" };
const auto SHIP_MARKER_STRING =
  QString{ "LINESTRING(0 43.8, 5.5 38.9, 7.9 23.0, 7.9 -38.6, -7.9 -38.6, -7.9 "
           "23.0, -5.5 39.9)" };

const auto ALVIN_MARKER_STRING = QString
{ "LINESTRING(-0.7 0.9, 0.7 0.9, 1.2 0.0, 1.3 -0.7, "
"1.3 -2.3, 1.0 -3.8, 0.2 -6.1, -0.2 -6.1, "
"-1.0 -3.8, -1.3 -2.3, -1.3 -0.7, -1.2 0.0,-0.6 1.8,"
"0.6 1.8,"
"0.6 0.9,"
"-0.6 0.9,-0.6 1.8,"
"-0.4 -0.4,"
"0.4 -0.4,"
"0.3 -1.0,"
"-0.3 -1.0)"};

const auto JASON_MARKER_STRING = QString {
"LINESTRING(0.9  0.0  0.5,0.9 -2.58 0.5,-0.9 -2.58 0.5,-0.9 0.3 0.5,-0.51 0.72 0.5,0.51 0.72 0.5,0.9 0.3 0.5)"};

const auto MEDEA_MARKER_STRING = QString {
"LINESTRING(-0.5 -1.0 0.5,-0.5 1.0 0.5,0.5 1.0 0.5,0.5 -1.0 0.5,-0.5 -1.0 0.5)"};


///\brief A Navest trail layer
///
/// A navest trail layer can be used to display the position and track of an
/// object from the "VFR" (Vehicle Fix Report) and "VPR" (Vehicle Position
/// Report) messages output form the Navest (Navigation Estimator) program.
/// Navest is used by the DSL vehicles Alvin, Jason, and Sentry to provide
/// installation-independent navigation estimates of subsea assets.
///
/// A navest trail layer consists of two major components:  A vehicle marker
/// updated with the current position and heading, and a trail of symbols
/// indicating past positions.  Typically the vehicle marker's shape represents
/// the object being tracked, such as a ship, and has real physical size while
/// the past fix markers are simple scale-invariant shapes.
class NavestLayer : public dslmap::MapLayer
{
  Q_OBJECT

public:
  /// \brief Create a new navest layer.
  ///
  /// \param beacon_number The navest beacon id
  /// \param parent parent layer or graphical object.
  explicit NavestLayer(int beacon_number = -1, QGraphicsItem* parent = nullptr);
  ~NavestLayer() override;
  int type() const override { return dslmap::MapItemType::NavestLayerType; }

  QRectF boundingRect() const override;
  void showPropertiesDialog() override;
  void paint(QPainter* painter, const QStyleOptionGraphicsItem* option,
             QWidget* widget) override;

  /// \brief The navest beacon id for this layer.
  ///
  /// \return
  int navestBeaconId() const noexcept;

  /// \brief Set the navest beacon id for this layer.
  ///
  /// \param id
  void setNavestBeaconId(int id);

  /// \brief The active vehicle solution
  ///
  /// For the NavestLayer object, this solution determines where to place the
  /// "current location" vehicle marker.
  ///
  /// Navest use the concept of "Solutions" to determine which source to use
  /// as the "correct" value.  For example, an AUV may have USBL fixes and its
  /// own dead-reckoning position estimate.  Navest will listen to all
  /// configured navigation sources, but will only update the VPR message with
  /// the one active vehicle solution.
  ///
  /// \return
  QString vehicleSolution() const noexcept;

  /// \brief Get a list of solutions available for this beacon.
  ///
  /// Return a list of navigation solutions for this beacon.
  ///
  /// Navest use the concept of "Solutions" to determine which source to use
  /// as the "correct" value.  For example, an AUV may have USBL fixes and its
  /// own dead-reckoning position estimate.  Navest will listen to all
  /// configured navigation sources, but will only update the VPR message with
  /// the one active vehicle solution.
  ///
  /// \return
  QStringList vehicleSolutions() const noexcept;

  /// \brief Set the active navigation solution.
  ///
  /// For the NavestLayer object, this solution determines where to place the
  /// "current location" vehicle marker.
  ///
  /// This solution should be one of the values returned by the
  /// NavestLayer::vehicleSolutions() method.
  ///
  /// Navest use the concept of "Solutions" to determine which source to use
  /// as the "correct" value.  For example, an AUV may have USBL fixes and its
  /// own dead-reckoning position estimate.  Navest will listen to all
  /// configured navigation sources, but will only update the VPR message with
  /// the one active vehicle solution.
  ///
  /// \param type
  void setVehicleSolution(const QString& type);

  bool getSourceExternalFix(const QString& type);

  /// \brief Set the vehicle marker visibility
  ///
  /// \param visible
  void setVehicleVisible(bool visible);

  /// \brief Return the vehicle marker symbol's visibility
  ///
  /// \return
  bool vehicleVisible() const noexcept;

  /// \brief Set the vehicle trail visibilty
  ///
  /// A vehicle can have multiple trails visible at once.
  ///
  /// \param type  Solution type (see vehicleSolutions())
  /// \param visible
  void setTrailVisible(const QString& type, bool visible);

  /// \brief Get the trail visibility for a solution type.
  ///
  /// A vehicle can have multiple trails visible at once.
  ///
  /// \param type  Solution type (see vehicleSolutions())
  bool trailVisible(const QString& type) const noexcept;

  /// \brief Set the vehicle marker symbol's outline color.
  ///
  /// \param color
  void setVehicleOutlineColor(const QColor& color);

  /// \breif Get the vehicle marker symbol's outline color.
  ///
  /// \return
  QColor vehicleOutlineColor() const noexcept;

  /// \brief Set the vehicle marker symbol's fill color
  ///
  /// \param color
  void setVehicleFillColor(const QColor& color);

  /// \brief Get the vehicle marker symbol's fill color.
  ///
  /// \return
  QColor vehicleFillColor() const noexcept;

  /// \brief Set the vehicle marker symbol's fill style.
  ///
  /// \param style
  void setVehicleFillStyle(Qt::BrushStyle style);

  /// \brief Get the vehicle marker symbol's fill style.
  ///
  /// \return
  Qt::BrushStyle vehicleFillStyle() const noexcept;

  /// \brief Set the vehicle marker's outline style.
  ///
  /// \param style
  void setVehicleOutlineStyle(Qt::PenStyle style);

  /// \brief Get the vehicle marker's outline style.
  ///
  /// \return
  Qt::PenStyle vehicleOutlineStyle() const noexcept;

  /// \brief Set the vehicle marker's label color.
  ///
  /// \param color
  void setVehicleLabelColor(const QColor& color);

  /// \brief Get the vehicle marker's label color.
  ///
  /// \return
  QColor vehicleLabelColor() const noexcept;

  /// \brief Set the trail symbol's outline color.
  ///
  /// \param type
  /// \param color
  void setTrailOutlineColor(const QString& type, const QColor& color);

  /// \brief Get the trail symbol's outline color.
  ///
  /// \param type
  /// \return
  QColor trailOutlineColor(const QString& type) const noexcept;

  /// \brief Set the trail symbol's fill color
  ///
  /// \param type
  /// \param color
  void setTrailFillColor(const QString& type, const QColor& color);

  /// \brief Get the trail symbol's fill color.
  ///
  /// \param type
  /// \return
  QColor trailFillColor(const QString& type) const noexcept;

  /// \brief Set the trail symbol's fill style
  ///
  /// \param type
  /// \param style
  void setTrailFillStyle(const QString& type, Qt::BrushStyle style);

  /// \brief Get the trail symbol's fill style
  ///
  /// \param type
  /// \return
  Qt::BrushStyle trailFillStyle(const QString& type) const noexcept;

  /// \brief Set the trail symbol outline style
  ///
  /// \param type
  /// \param style
  void setTrailOutlineStyle(const QString& type, Qt::PenStyle style);

  /// \brief Get the trail symbol outline style.
  ///
  /// \param type
  /// \return
  Qt::PenStyle trailOutlineStyle(const QString& type) const noexcept;

  /// \brief Set the trail symbol label color
  ///
  /// \param type
  /// \param color
  void setTrailLabelColor(const QString& type, const QColor& color);

  /// \brief Get the trail symbol label color
  ///
  /// \param type
  /// \return
  QColor trailLabelColor(const QString& type) const noexcept;

  /// \brief Set the displayed length of the vehicle trail.
  ///
  /// \param type
  /// \param length
  void setTrailLength(const QString& type, int length);

  /// \brief Get the displayed length of the vehicle trail.
  ///
  /// \param type
  /// \return
  int trailLength(const QString& type) const noexcept;

  /// \brief Set the maximum length of a vehicle trail.
  ///
  /// \param type
  /// \param capacity
  void setTrailCapacity(const QString& type, int capacity);

  /// \brief Get the maximum length of a vehicle trail
  ///
  /// \param type
  /// \return
  int trailCapacity(const QString& type) const noexcept;

  /// \brief Set the trail symbol
  ///
  /// Sets the trail symbol shape for solution `type`.  Can be used for the
  /// simple shapes defined by dslmap::ShapeMarkerSymbol::Shape, (e.g. circles,
  /// squares, etc.)
  ///
  /// \param type
  /// \param shape
  void setTrailSymbolShape(const QString& type,
                           dslmap::ShapeMarkerSymbol::Shape shape);

  /// \brief Get the trail symbol shape.
  ///
  /// \param type
  /// \return
  dslmap::ShapeMarkerSymbol::Shape trailSymbolShape(const QString& type);

  /// \brief Set the the trail symbol shape size
  ///
  /// \param type
  /// \param size
  void setTrailSymbolSize(const QString& type, qreal size);

  /// \brief Get the trail symbol shape size
  ///
  /// \param type
  /// \return
  qreal trailSymbolSize(const QString& type);

  /// \brief Set the vehicle marker symbol
  ///
  /// \param symbol
  void setVehicleSymbol(std::shared_ptr<dslmap::MarkerSymbol> symbol);

  /// \brief Get the vehicle marker symbol
  ///
  /// \return
  std::shared_ptr<dslmap::MarkerSymbol> vehicleSymbol() const noexcept;

  /// \brief Get a raw pointer to the vehicle marker symbol.
  ///
  /// \return
  dslmap::MarkerSymbol* vehicleSymbolPtr() const noexcept;

  /// \brief Set the vehicle trail symbol.
  ///
  /// \param solution
  /// \param symbol
  void setTrailSymbol(const QString& solution,
                      std::shared_ptr<dslmap::ShapeMarkerSymbol> symbol);

  /// \brief Get the vehicle trail symbol.
  ///
  /// \param solution
  /// \return
  std::shared_ptr<dslmap::ShapeMarkerSymbol> trailSymbol(
    const QString& solution) const noexcept;

  /// \brief Get a raw pointer to the vehicle trail symbol
  ///
  /// \param solution
  /// \return
  dslmap::ShapeMarkerSymbol* trailSymbolPtr(const QString& solution) const
    noexcept;

  dslmap::BeaconLayer* trailLayerPtr(const QString& solution) const noexcept;

  /// \brief Return whether the vehicle heading is updated from VFR messages.
  ///
  /// \return
  bool updateHeadingFromVfr() const noexcept;

  /// \brief Update vehicle marker heading from VFR messages.
  ///
  /// \param enabled
  void setUpdateHeadingFromVfr(bool enabled);

  /// \brief Get the current vehicle marker position
  ///
  /// \return
  QPointF currentPosition() const;

  /// \brief Get the current vehicle marker heading
  ///
  /// \return
  qreal currentHeading() const noexcept;

  /// \brief Save layer settings
  bool saveSettings(QSettings& settings) override;

  /// \brief load layer settings.
  bool loadSettings(QSettings& settings) override;

  /// \brief Create a new layer from settings
  ///
  /// \param settings
  /// \return
  static std::unique_ptr<NavestLayer> fromSettings(QSettings& settings);

  /// \brief Update the beacon location from a lon/lat coordinate
  ///
  /// The current location will be added to the beacon trail before the
  /// symbol is moved to the new location.
  ///
  /// This method will convert geographic coordinates (longitude/latitude) to
  /// whatever projection is being used by the layer.
  ///
  /// \param lon
  /// \param lat
  /// \param timestamp
  /// \param heading
  /// \param label
  /// \param attributes
  void updatePositionLonLat(
    qreal lon, qreal lat, const QString& solution,
    const QDateTime& timestamp = {}, const qreal* heading = nullptr,
    const QString& label = {},
    const dslmap::MarkerItem::Attributes& attributes = {});

  /// \brief Update the vehicle marker heading
  ///
  /// \param heading
  void updateHeading(qreal heading);

  void setHeadingMode(bool use_acomm, int vehicle_id);

  bool acommsHeading();

  /// \brief Set the layer projection.
  ///
  /// \param proj
  void setProjection(std::shared_ptr<const dslmap::Proj> proj) override;

protected slots:
  /// \brief Handle when position from beacon layers change
  void handleCurrentPositionChanged(QPointF pos);

private:
  dslmap::BeaconLayer* m_createNewLayer(const QString& solution);

  int m_beacon_number = -1;
  QString m_solution_type;
  QMap<QString, dslmap::BeaconLayer*> m_beacon_layer;
  QMap<QString, bool> m_trail_visible;
  QList<QMetaObject::Connection> m_beacon_symbol_connections;
  QMap<QString, QList<QMetaObject::Connection>> m_trail_symbol_connections;
  bool m_update_heading_from_vfr = true;
  std::shared_ptr<dslmap::MarkerSymbol> m_beacon_symbol;
  bool m_vehicle_visible = true;
  bool use_acomm_heading = false;

signals:
  /// \brief Emitted when the vehicle marker position changes.
  void currentPositionChanged(const QPointF& pos);

  /// \brief Emitted when the active vehicle solution changes
  void fixSolutionChanged(const QString& solution);

  /// \brief Emitted when the VFR heading flag changes.
  void updateHeadingFromVfrChanged(bool enabled);

  /// \brief Emitted when the vehicle marker visibilty changes
  void vehicleVisibilityChanged(bool visible);

  /// \brief Emitted when the vehicle marker outline color changes.
  void vehicleOutlineColorChanged(QColor color);

  /// \brief Emitted when the vehicle marker fill color chanes.
  void vehicleFillColorChanged(QColor color);

  /// \brief Emitted when the vehicle marker outline style changes.
  void vehicleOutlineStyleChanged(Qt::PenStyle style);

  /// \brief Emitted when the vehicle marker fill sytle changes.
  void vehicleFillStyleChanged(Qt::BrushStyle style);

  /// \brief Emitted when a trail is added for a new navigation solution.
  void trailAdded(const QString& solution);

  /// \brief Emitted when a trail visibility changes.
  void trailVisibilityChanged(const QString& solution, bool visible);

  /// \brief Emitted when a trail symbol outline color changes.
  void trailOutlineColorChanged(const QString& solution, QColor color);

  /// \brief Emitted when a trail symbol fill color changes.
  void trailFillColorChanged(const QString& solution, QColor color);

  /// \brief Emitted when a trail symbol shape changes
  void trailSymbolShapeChanged(const QString& solution,
                               dslmap::ShapeMarkerSymbol::Shape shape);

  /// \brief Emitted when a trail outline style changes
  void trailOutlineStyleChanged(const QString& solution, Qt::PenStyle style);

  /// \brief Emitted when a trail fill style changes
  void trailFillStyleChanged(const QString& solution, Qt::BrushStyle style);

  /// \brief Emitted when a symbol size changes
  void trailSymbolSizeChanged(const QString& solution, qreal size);

  /// \brief Emitted when the displayed length of a trail changes.
  void trailLengthChanged(const QString& solution, qreal size);

  /// \brief Emitted when the maximum displayed length of a trail changes.
  void trailCapacityChanged(const QString& solution, qreal size);

  /// \brief Emitted when the source fix combo is changed, to check a box or not
  void checkFixBox(bool fix);
};
}
}
}

#endif // NAVG_NAVESTLAYER_H
