/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 7/23/18.
//

#ifndef NAVG_NAVEST_H
#define NAVG_NAVEST_H

#include "Messages.h"
#include "NavestSettings.h"

#include <navg/CorePluginInterface.h>
#include <navg/NavGMapView.h>

#include <QHash>
#include <QHostAddress>
#include <QLoggingCategory>
#include <QWidget>
#include <dslmap/BeaconLayer.h>
#include <dslmap/Util.h>

#include <memory>
#include <cmath>

class QUdpSocket;
class QStandardItemModel;

namespace navg {
namespace plugins {
namespace navest {

class NavestSettings;
class NavestComms;
class NavestLayer;

namespace Ui {
class Navest;
}

class Navest : public QWidget, public CorePluginInterface
{
  Q_OBJECT
  Q_INTERFACES(navg::CorePluginInterface)
  Q_PLUGIN_METADATA(IID CorePluginInterface_iid FILE "Navest.json")

signals:
  void messageReceived(const QByteArray& message);
  void messageReceived(NavestMessageType type, QVariant message);
  void messageSent(const QByteArray& message);

  void listenUdpPortChanged(int port);
  void remoteUdpPortChanged(int port);
  void remoteTcpPortChanged(int port);
  void remoteAddressChanged(QHostAddress address);
  void ignoredVehiclesChanged(NavestSettings::VehicleNumbers vehicles);

  void layerAdded(NavestLayer* layer);
  void vfrMessageReceived(int vehicle_number, QString fix_source, double lon,
                          double lat, double depth);
  void vfrMessageReceived(int vehicle_number, QString fix_source, double lon,
                          double lat, double depth, bool is_reset_source);
  void navestSolutionChanged(QString solution);
  
public:
  enum class Setting
  {
    /// Listen for Navest message strings (VPR, VFR, etc.) on this port
    ListenUdpPort,
    /// Remote host IP address to send Navest commands (reset DVL, etc.)
    RemoteHostIp,
    /// Remote host UDP port to send Navest commands (reset DVL, etc.)
    RemoteHostUdpPort,
    /// Remote host TCP port to retrieve Sentry trackfiles from
    RemoteHostTcpPort,
    /// Vehicle IDs to ignore
    IgnoredVehicleIds,
  };

  explicit Navest(QWidget* parent = nullptr);
  ~Navest() override;

  //
  // CorePluginInterface Implementation
  //
  void loadSettings(QSettings* settings) override;
  void saveSettings(QSettings& settings) override;

  // This plugin does not create connections.
  void connectPlugin(const QObject* plugin,
                     const QJsonObject& metadata) override;
  void setMapView(QPointer<dslmap::MapView> view) override;
  QList<QAction*> pluginMenuActions() override;
  QList<QAction*> pluginToolbarActions() override { return {}; }

  /// \brief Get the set of ignored beacon ids
  ///
  /// \return
  Q_INVOKABLE QSet<int> ignoredBeaconIds() const;

  /// \brief Get the local listening port for UDP messages
  ///
  /// The plugin is listening for UDP packets on this port.
  /// \return
  Q_INVOKABLE int listenUdpPort() const;

  /// \brief Get the remote navest port for UDP commands
  ///
  /// The plugin will send UDP packets to the address provided by
  /// PluginNavest::remoteAddress and this port
  ///
  /// \return
  Q_INVOKABLE int remoteUdpPort() const;

  /// \brief Get the remote navest port for TCP connections
  ///
  /// This is only used to receive a copy of the Sentry Tracks file loaded by
  /// navest at startup.
  ///
  /// \return
  Q_INVOKABLE int remoteTcpPort() const;

  /// \brief Get the remote navest host address
  ///
  /// The plugin will send UDP packets to this remote address on the port
  /// provided by PluginNavest::remotePort
  ///
  /// \return
  Q_INVOKABLE QHostAddress remoteAddress() const;

  /// \brief Is the plugin bound to a local port
  ///
  /// Returns true if the plugin is currently bound to the port given by
  /// PluginNavest::listenPort
  ///
  /// \return
  bool isBound() const;

  /// \brief Get a raw pointer to the navest layer for a vehicle
  ///
  /// \param id
  /// \return
  NavestLayer* layer(int id);

  QMap<int, NavestLayer*> layers() const;

  static QString settingString(const Navest::Setting& setting) noexcept
  {
    switch (setting) {
      case Navest::Setting::ListenUdpPort:
        return QString{ "listen_udp_port" };
      case Navest::Setting::RemoteHostIp:
        return QString{ "remote_address" };
      case Navest::Setting::RemoteHostUdpPort:
        return QString{ "remote_udp_port" };
      case Navest::Setting::RemoteHostTcpPort:
        return QString{ "remote_tcp_port" };
      case Navest::Setting::IgnoredVehicleIds:
        return QString{ "ignored_vehicle_ids" };
    }
    /// Should never get here, using an enum class
    return {};
  }

public slots:

  /// \breif Set the local listening port for Navest comms
  ///
  /// Attempts to bind to the provided port with the ShareAddress and
  /// ReuseAddress hints.
  ///
  /// \param port
  void setListenUdpPort(int port);

  /// \brief Set the destination port for sent messages
  ///
  /// This port is used with the remote address
  /// (Navest::remoteAddress) to send messages to the navest daemon.
  ///
  /// \param port
  void setRemoteUdpPort(int port);

  /// \brief Set the port for Navest TCP connections
  ///
  /// Navest TCP connections are only used to receive the Sentry tracks file.
  ///
  /// \param port
  void setRemoteTcpPort(int port);

  /// \brief Set the destination address for sent messages
  ///
  /// This address is used with the remote port (Navest::remotePort)
  /// to send messages to the navest daemon.
  ///
  /// \param address
  void setRemoteAddress(const QHostAddress& address);

  /// \brief Set the destination address for sent messages
  ///
  /// This address is used with the remote port (Navest::remotePort)
  /// to send messages to the navest daemon.
  ///
  /// This is a overloaded method of Navest::setRemoteAddress
  /// (const QHostAddress&)
  ///
  /// \param address
  void setRemoteAddress(const QString& address);

  /// \brief Send a message to the Navest daemon
  ///
  /// The message will be sent to the remote host/port pair set by
  /// Navest::setRemotePort and Navest::setRemoteAddress
  ///
  /// \param message
  void sendMessage(const QByteArray& message);

  /// \brief Show the settings dialog
  ///
  void showSettingsDialog();

  /// \brief Set ignored navest vehicles
  ///
  /// \param vehicles
  void setIgnoredVehicleNumbers(const NavestSettings::VehicleNumbers& vehicles);

  QString vehicleName(int vehicle_id);

  void updatePositionLonLat(
    int id, qreal lon, qreal lat, const QString& solution,
    const QDateTime& timestamp = {}, const qreal* heading = nullptr,
    const QString& label = {},
    const dslmap::MarkerItem::Attributes& attributes = {});

  void updateHeading(int id, qreal heading);

  void setHeadingMode(bool,int);
  void addLayer(NavestLayer* layer);

protected:
  
  void zoomToLayer(dslmap::MapLayer* layer);  
protected slots:

  /// \brief Handle incoming UDP data
  void handleIncomingDatagram();

  void handleIncomingVfr(NavestMessageVfr vfr);
  void handleIncomingVpr(NavestMessageVpr vpr);

  
  NavestLayer* createNewLayer(const QString& name, qreal lon, qreal lat,
                              int beacon, const QString& fix_source);
  void updateSolutionSelected(int vehicle, QString solution);
  
private:
  std::unique_ptr<Ui::Navest> ui;
  NavestSettings* m_dialog_settings;
  NavestComms* m_dialog_comms;
  QList<NavestLayer*> m_layers;
  // NavestLayerModel* m_model;

  int m_listen_udp_port = 0;
  int m_remote_udp_port = 0;
  int m_remote_tcp_port = 0;
  QSet<int> m_ignored_beacon_ids = {};
  QHostAddress m_remote_address;
  QUdpSocket* m_udp;
  QPointer<dslmap::MapView> m_view;
  QAction* m_action_show_settings = nullptr;
  QAction* m_action_show_comms = nullptr;
  QStandardItemModel* m_vehicle_list_model;
};

Q_DECLARE_LOGGING_CATEGORY(plugin_navest)
} // navest
} // plugins
} // navg

#endif // NAVG_NAVEST_H
