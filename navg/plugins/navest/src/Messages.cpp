/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 9/28/17.
//

#include "Messages.h"

namespace navg {

QVariant
NavestMessageSps::parse(const QByteArrayList& fields)
{

  if (fields.at(0) != "SPS") {
    return {};
  }

  if (fields.length() < 8) {
    return {};
  }

  const auto parsed = NavestMessageSps{.lat = fields.at(2).toDouble(),
                                       .lon = fields.at(3).toDouble(),
                                       .z = fields.at(4).toDouble(),
                                       .beacon_id = fields.at(7).toInt() };

  return QVariant::fromValue(parsed);
}

QVariant
NavestMessageVfr::parse(const QByteArrayList& fields)
{

  if (fields.at(0) != "VFR") {
    return {};
  }

  if (fields.length() < 13) {
    return {};
  }

  auto date = QDate::fromString(QString::fromLatin1(fields.at(1)), "yyyy/M/d");
  auto time = QTime::fromString(QString::fromLatin1(fields.at(2)), "H:mm:ss.z");

  const auto parsed =
    NavestMessageVfr{.timestamp = QDateTime{ date, time },
                     .mode = fields.at(3).toInt(),
                     .vehicle_number = fields.at(4).toInt(),
                     .fix_source = QString::fromLatin1(fields.at(5)),
                     .lon = fields.at(6).toDouble(),
                     .lat = fields.at(7).toDouble(),
                     .depth = fields.at(8).toDouble(),
                     .alt = fields.at(9).toDouble(),
                     .depth_or_altitude = fields.at(10).toInt(),
                     .speed_over_ground = fields.at(11).toDouble(),
                     .course_over_ground = fields.at(12).toDouble() };

  return QVariant::fromValue(parsed);
}

QVariant
NavestMessageVpr::parse(const QByteArrayList& fields)
{

  if (fields.at(0) != "VPR") {
    return {};
  }

  if (fields.length() < 9) {
    return {};
  }

  const auto parsed = NavestMessageVpr{
    .vehicle_name = fields.at(3),
    .vehicle_number = fields.at(4).toInt(),
    .lon = fields.at(5).toDouble(),
    .lat = fields.at(6).toDouble(),
    .depth = fields.at(7).toDouble(),
    .heading = fields.at(8).toDouble(),
  };

  return QVariant::fromValue(parsed);
}

QVariant
NavestMessageOct::parse(const QByteArrayList& fields)
{

  if (fields.at(0) != "OCT") {
    return {};
  }

  if (fields.length() < 10) {
    return {};
  }

  const auto parsed = NavestMessageOct{
    .platform = QString::fromLatin1(fields.at(3)),
    .heading = fields.at(7).toDouble(),
    .pitch = fields.at(8).toDouble(),
    .roll = fields.at(9).toDouble(),
  };

  return QVariant::fromValue(parsed);
}

QVariant
NavestMessageNds::parse(const QByteArrayList& fields)
{

  if (fields.at(0) != "NDS") {
    return {};
  }

  if (fields.length() < 7) {
    return {};
  }

  const auto parsed =
    NavestMessageNds{.availble = fields.at(4).toULongLong(),
                     .free = fields.at(5).toULongLong(),
                     .percent_full = fields.at(6).toDouble() };

  return QVariant::fromValue(parsed);
}

QVariant
NavestMessageDep::parse(const QByteArrayList& fields)
{

  if (fields.at(0) != "DEP") {
    return {};
  }

  if (fields.length() < 6) {
    return {};
  }

  const auto parsed =
    NavestMessageDep{.vehicle_name = QString::fromLatin1(fields.at(3)),
                     .paro_number = fields.at(4).toInt(),
                     .depth = fields.at(5).toDouble() };

  return QVariant::fromValue(parsed);
}

QVariant
NavestMessageTdr::parse(const QByteArrayList& fields)
{

  if (fields.at(0) != "TDR") {
    return {};
  }

  if (fields.length() < 5) {
    return {};
  }

  const auto parsed =
    NavestMessageTdr{.time_since_reset = fields.at(4).toDouble() };

  return QVariant::fromValue(parsed);
}

QVariant
NavestMessageRcm::parse(const QByteArrayList& fields)
{

  if (fields.at(0) != "RCM") {
    return {};
  }

  if (fields.length() < 8) {
    return {};
  }

  const auto parsed = NavestMessageRcm{
    .lat = fields.at(3).toDouble(),
    .lon = fields.at(4).toDouble(),
    .range = fields.at(5).toDouble(),
    .vehicle_number = fields.at(6).toInt(),
    .depth = fields.at(7).toDouble(),
  };

  return QVariant::fromValue(parsed);
}

QVariant
NavestMessageDdd::parse(const QByteArrayList& fields)
{

  if (fields.at(0) != "Ddd" && fields.at(0) != "DDD") {
    return {};
  }

  if (fields.length() < 18) {
    return {};
  }

  auto date = QDate::fromString(QString::fromLatin1(fields.at(1)), "yyyy/M/d");
  auto time = QTime::fromString(QString::fromLatin1(fields.at(2)), "H:mm:ss.z");

  const auto parsed = NavestMessageDdd{
    .timestamp = QDateTime{ date, time },
    .roll = fields.at(6).toDouble(),
    .pitch = fields.at(5).toDouble(),
    .heading = fields.at(4).toDouble(),
    .beam_ranges = { fields.at(7).toDouble(), fields.at(8).toDouble(),
                     fields.at(9).toDouble(), fields.at(10).toDouble() },
    .beam_velocities = { fields.at(14).toDouble(), fields.at(15).toDouble(),
                         fields.at(16).toDouble(), fields.at(17).toDouble() },
    .alt = fields.at(11).toDouble(),
    .bt_beams = fields.at(12).toInt(),
    .wt_beams = fields.at(13).toInt(),
    .message = fields.join(' '),
  };

  return QVariant::fromValue(parsed);
}

QPair<NavestMessageType, QVariant>
parseNavestMessage(const QByteArray& message)
{
  const auto fields = message.split(' ');
  const auto& header = fields.at(0);

  auto parsed = QVariant{};
  auto type = NavestMessageType::Unknown;

  if (header == "SPS") {
    parsed = NavestMessageSps::parse(fields);
    type = NavestMessageType::SPS;
  } else if (header == "VFR") {
    parsed = NavestMessageVfr::parse(fields);
    type = NavestMessageType::VFR;
  } else if (header == "VPR") {
    parsed = NavestMessageVpr::parse(fields);
    type = NavestMessageType::VPR;
  } else if (header == "OCT") {
    parsed = NavestMessageOct::parse(fields);
    type = NavestMessageType::OCT;
  } else if (header == "DDD") {
    parsed = NavestMessageDdd::parse(fields);
    type = NavestMessageType::DDD;
  } else if (header == "NDS") {
    parsed = NavestMessageNds::parse(fields);
    type = NavestMessageType::NDS;
  } else if (header == "DEP") {
    parsed = NavestMessageDep::parse(fields);
    type = NavestMessageType::DEP;
  } else if (header == "RCM") {
    parsed = NavestMessageRcm::parse(fields);
    type = NavestMessageType::RCM;
  } else if (header == "TDR") {
    parsed = NavestMessageTdr::parse(fields);
    type = NavestMessageType::TDR;
  } else {
    type = NavestMessageType::Unknown;
    parsed.setValue(message);
  }

  if (!parsed.isValid()) {
    type = NavestMessageType::Invalid;
    parsed.setValue(message);
  }

  return { type, parsed };
}
}
