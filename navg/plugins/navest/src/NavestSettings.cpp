/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "NavestSettings.h"
#include "ui_NavestSettings.h"

#include <QRegExpValidator>

namespace navg {
namespace plugins {
namespace navest {

NavestSettings::NavestSettings(QWidget* parent)
  : QDialog(parent)
  , ui(std::make_unique<Ui::NavestSettings>())
{
  ui->setupUi(this);

  // Add an ip address validator to the remote address line edit
  auto octet = QString{ "(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])" };
  auto ip_regex = QRegExp{ "^" + octet + "\\." + octet + "\\." + octet + "\\." +
                           octet + "$" };

  ui->remoteAddress->setValidator(new QRegExpValidator(ip_regex));
}

NavestSettings::~NavestSettings() = default;

void
NavestSettings::setListenUdpPort(int port)
{
  ui->listenUdpPort->setValue(port);
}

int
NavestSettings::listenUdpPort() const
{
  return ui->listenUdpPort->value();
}

void
NavestSettings::setRemoteUdpPort(int port)
{
  ui->remoteUdpPort->setValue(port);
}
int
NavestSettings::remoteUdpPort() const
{
  return ui->remoteUdpPort->value();
}
void
NavestSettings::setRemoteAddress(QHostAddress& address)
{
  ui->remoteAddress->setText(address.toString());
}
QHostAddress
NavestSettings::remoteAddress() const
{
  return QHostAddress{ ui->remoteAddress->text() };
}
void
NavestSettings::setRemoteTcpPort(int port)
{
  ui->remoteTcpPort->setValue(port);
}
int
NavestSettings::remoteTcpPort() const
{
  return ui->remoteTcpPort->value();
}
QSet<int>
NavestSettings::ignoredVehicleNumbers() const
{
  return beaconIdsFromString(ui->ignoredVehicleNumbers->text());
}

void
NavestSettings::setIgnoredVehicleNumbers(
  const NavestSettings::VehicleNumbers& ids) noexcept
{
  ui->ignoredVehicleNumbers->setText(beaconIdsToString(ids));
}

NavestSettings::VehicleNumbers
NavestSettings::beaconIdsFromString(const QString& list) noexcept
{
  auto ids = VehicleNumbers{};
  auto parts =
    list.splitRef(QChar{ ',' }, QString::SplitBehavior::SkipEmptyParts,
                  Qt::CaseSensitivity::CaseInsensitive);
  std::for_each(std::begin(parts), std::end(parts), [&](auto it) {
    bool ok = false;
    auto id = it.trimmed().toInt(&ok);
    if (ok) {
      ids.insert(id);
    }
  });
  return ids;
}
QString
NavestSettings::beaconIdsToString(const VehicleNumbers& set) noexcept
{
  auto string_list = QStringList{};
  std::for_each(std::begin(set), std::end(set),
                [&](auto it) { string_list.push_back(QString::number(it)); });
  return string_list.join(QChar{ ',' });
}

} // namespace navest
} // namespace plugins
} // namespace navg
