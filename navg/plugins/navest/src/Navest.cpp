/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 7/23/18.
//

#include "Navest.h"
#include "LayerOptions.h"
#include "NavestComms.h"
#include "NavestLayer.h"
#include "NavestSettings.h"

#include "ui_Navest.h"

#include <dslmap/MapScene.h>
#include <dslmap/MapView.h>
#include <dslmap/Proj.h>
#include <dslmap/ShapeMarkerSymbol.h>
#include <dslmap/BeaconLayer.h>

#include <QAction>
#include <QSortFilterProxyModel>
#include <QStandardItemModel>
#include <QUdpSocket>

namespace navg {
namespace plugins {
namespace navest {

Q_LOGGING_CATEGORY(plugin_navest, "navg.plugins.navest")

Navest::Navest(QWidget* parent)
  : QWidget(parent)
  , ui(std::make_unique<Ui::Navest>())
  , m_dialog_settings(new NavestSettings(this))
  , m_dialog_comms(new NavestComms(this))
  , m_udp(new QUdpSocket(this))
  , m_action_show_settings(new QAction("&Settings", this))
  , m_action_show_comms(new QAction("&Log", this))
  , m_vehicle_list_model(new QStandardItemModel(this))
{
  ui->setupUi(this);
  ui->combo_vehicle->setModel(m_vehicle_list_model);

  // Connect the vehicle combo box to the layer options stack
  connect(ui->combo_vehicle, static_cast<void (QComboBox::*)(int)>(
                               &QComboBox::currentIndexChanged),
          [this](int) {
            const auto page =
              ui->combo_vehicle->currentData(Qt::UserRole + 1).toInt();
            ui->stack_layer_options->setCurrentIndex(page);
          });

  connect(m_udp, &QUdpSocket::readyRead, this, &Navest::handleIncomingDatagram);

  connect(this, static_cast<void (Navest::*)(const QByteArray&)>(
                  &Navest::messageReceived),
          m_dialog_comms, &NavestComms::logReceivedMessage);
  connect(this, static_cast<void (Navest::*)(const QByteArray&)>(
                  &Navest::messageSent),
          m_dialog_comms, &NavestComms::logTransmittedMessage);
  connect(m_dialog_comms, &NavestComms::sendMessage, this,
          &Navest::sendMessage);

  connect(m_action_show_settings, &QAction::triggered, this,
          &Navest::showSettingsDialog);
  connect(m_action_show_comms, &QAction::triggered, m_dialog_comms,
          &QWidget::show);
}

Navest::~Navest() = default;

void
Navest::setMapView(QPointer<dslmap::MapView> view)
{
  m_view = view;
}

void
Navest::connectPlugin(const QObject* plugin, const QJsonObject& metadata)
{

if (!plugin) {
    return;
  }

  const auto name =
    metadata.value("MetaData").toObject().value("name").toString();
  if (name.isEmpty()) {
    return;
  }

  /*  if (name == "Solution_Selector") {

    connect(plugin,
	    SIGNAL(solutionSelectorChanged(int, QString)),
	    this,
	    SLOT(updateSolutionSelected(int, QString)));
   
	    }*/

}

void
Navest::loadSettings(QSettings* settings)
{
  if (settings == nullptr) {
    return;
  }

  auto ok = bool{ false };
  auto port =
    settings->value(settingString(Navest::Setting::ListenUdpPort)).toInt(&ok);
  if (ok) {
    setListenUdpPort(port);
  } else {
    qCWarning(plugin_navest)
      << "Unable to load 'listen_udp_port' from settings";
  }

  port = settings->value(settingString(Navest::Setting::RemoteHostUdpPort))
           .toInt(&ok);
  if (ok) {
    setRemoteUdpPort(port);
  } else {
    qCWarning(plugin_navest)
      << "Unable to load 'remote_udp_port' from settings";
  }

  port = settings->value(settingString(Navest::Setting::RemoteHostTcpPort))
           .toInt(&ok);
  if (ok) {
    setRemoteTcpPort(port);
  } else {
    qCWarning(plugin_navest)
      << "Unable to load 'remote_tcp_port' from settings";
  }

  auto remote_addr =
    settings->value(settingString(Navest::Setting::RemoteHostIp)).toString();
  setRemoteAddress(remote_addr);

  auto beacon_ids = NavestSettings::beaconIdsFromString(
    settings->value(settingString(Navest::Setting::IgnoredVehicleIds))
      .toString());
  setIgnoredVehicleNumbers(beacon_ids);

  const auto num_layers = settings->beginReadArray("layers");
  for (auto i = 0; i < num_layers; ++i) {
    settings->setArrayIndex(i);
    auto layer = NavestLayer::fromSettings(*settings);
    if (!layer) {
      qCWarning(plugin_navest,
                "Error creating layer from parameters stored in array index %d",
                i);
      continue;
    }

    addLayer(layer.release());
  }
  settings->endArray();
}

void
Navest::saveSettings(QSettings& settings)
{
  settings.setValue(settingString(Navest::Setting::ListenUdpPort),
                    m_listen_udp_port);
  settings.setValue(settingString(Navest::Setting::RemoteHostUdpPort),
                    m_remote_udp_port);
  settings.setValue(settingString(Navest::Setting::RemoteHostIp),
                    m_remote_address.toString());
  settings.setValue(settingString(Navest::Setting::RemoteHostTcpPort),
                    m_remote_tcp_port);
  settings.setValue(settingString(Navest::Setting::IgnoredVehicleIds),
                    NavestSettings::beaconIdsToString(m_ignored_beacon_ids));

  settings.beginWriteArray("layers", m_layers.count());
  auto i = 0;
  for (auto it = std::cbegin(m_layers); it != std::cend(m_layers); ++it) {
    if (!(*it)) {
      qCWarning(plugin_navest, "Skipping null layer");
      continue;
    }
    settings.setArrayIndex(i);
    (*it)->saveSettings(settings);
    ++i;
  }
  settings.endArray();
}

int
Navest::listenUdpPort() const
{
  return m_listen_udp_port;
}

void
Navest::setListenUdpPort(int port)
{

  // Already listening on this port.
  if (m_udp->localPort() == port && m_udp->isOpen()) {
    return;
  }

  // Close the port before rebinding.
  m_udp->close();

  if (!m_udp->bind(port,
                   QUdpSocket::ReuseAddressHint | QUdpSocket::ShareAddress)) {
    qCWarning(plugin_navest) << "Unable to bind to local port:" << port;
  } else {
    qCInfo(plugin_navest) << "Listening for udp messages on local port: "
                          << port;
  }

  m_listen_udp_port = port;
  emit listenUdpPortChanged(port);
}
int
Navest::remoteUdpPort() const
{
  return m_remote_udp_port;
}
void
Navest::setRemoteUdpPort(int port)
{
  if (m_remote_udp_port == port) {
    return;
  }
  qCInfo(plugin_navest) << "Setting remote udp port to:" << port;
  m_remote_udp_port = port;
  emit remoteUdpPortChanged(port);
}

int
Navest::remoteTcpPort() const
{
  return m_remote_tcp_port;
}

void
Navest::setRemoteTcpPort(int port)
{
  if (m_remote_tcp_port == port) {
    return;
  }

  qCInfo(plugin_navest) << "Setting remote tcp port to:" << port;
  m_remote_tcp_port = port;
  emit remoteTcpPortChanged(port);
}

QHostAddress
Navest::remoteAddress() const
{
  return m_remote_address;
}

void
Navest::setRemoteAddress(const QString& address)
{
  auto host_addr = QHostAddress{ address };
  if (host_addr.isNull()) {
    qCWarning(plugin_navest) << "'" << address << "' is not a valid ip address";
    return;
  }

  setRemoteAddress(host_addr);
}

void
Navest::setRemoteAddress(const QHostAddress& address)
{

  if (address == m_remote_address) {
    return;
  }

  qCInfo(plugin_navest) << "Setting output address to:" << address.toString();
  m_remote_address = address;
  emit remoteAddressChanged(address);
}

void
Navest::setIgnoredVehicleNumbers(const NavestSettings::VehicleNumbers& vehicles)
{
  if (vehicles == m_ignored_beacon_ids) {
    return;
  }
  qCInfo(plugin_navest) << "Setting ignored vehicle numbers to:" << vehicles;
  m_ignored_beacon_ids = vehicles;
  emit ignoredVehiclesChanged(vehicles);
}

QSet<int>
Navest::ignoredBeaconIds() const
{
  return m_ignored_beacon_ids;
}

void
Navest::sendMessage(const QByteArray& message)
{
  m_udp->writeDatagram(message, m_remote_address, m_remote_udp_port);
  emit messageSent(message);
}

bool
Navest::isBound() const
{
  return m_udp->state() == QUdpSocket::BoundState;
}

void
Navest::showSettingsDialog()
{
  QScopedPointer<NavestSettings> dialog(new NavestSettings);
  dialog->setListenUdpPort(m_listen_udp_port);
  dialog->setRemoteUdpPort(m_remote_udp_port);
  dialog->setRemoteAddress(m_remote_address);
  dialog->setRemoteTcpPort(m_remote_tcp_port);
  dialog->setIgnoredVehicleNumbers(m_ignored_beacon_ids);

  dialog->setModal(true);
  if (!dialog->exec()) {
    return;
  }

  setRemoteAddress(dialog->remoteAddress());
  setRemoteUdpPort(dialog->remoteUdpPort());
  setListenUdpPort(dialog->listenUdpPort());
  setRemoteTcpPort(dialog->remoteTcpPort());
  auto beacon_ids = dialog->ignoredVehicleNumbers();
  setIgnoredVehicleNumbers(beacon_ids);
}
void
Navest::addLayer(NavestLayer* layer)
{
  if (!layer) {
    return;
  }

  m_layers.append(layer);
  auto options_widget = new LayerOptions(layer);
  const auto page = ui->stack_layer_options->addWidget(options_widget);
  auto item = new QStandardItem;
  item->setData(layer->name(), Qt::DisplayRole);
  item->setData(page, Qt::UserRole + 1);

  // Change the item text when the layer name changes.
  connect(layer, &NavestLayer::nameChanged, [item](const QString& name) {
    item->setData(name, Qt::DisplayRole);
  });

  m_vehicle_list_model->setItem(m_vehicle_list_model->rowCount(), item);
  if (ui->combo_vehicle->currentIndex() < 0) {
    ui->combo_vehicle->setCurrentIndex(0);
  }

  emit layerAdded(layer);
}

void
Navest::handleIncomingDatagram()
{

  static auto buf = QByteArray{};

  while (m_udp->hasPendingDatagrams()) {
    auto incoming_size = m_udp->pendingDatagramSize();
    if (incoming_size > buf.size()) {
      buf.resize(static_cast<int>(incoming_size));
    }

    auto bytes_read =
      m_udp->readDatagram(buf.data(), buf.size(), nullptr, nullptr);

    if (bytes_read != incoming_size) {
      qCWarning(plugin_navest) << "Short read: expected" << incoming_size
                               << "read" << bytes_read;
    }

    if (bytes_read == 0) {
      continue;
    }

    auto msg = QByteArray{ buf.data(), static_cast<int>(bytes_read) };
    emit messageReceived(msg);

    // Now try parsing it
    auto parsed = parseNavestMessage(msg);
    switch (parsed.first) {
      case NavestMessageType::VFR: {
        handleIncomingVfr(parsed.second.value<NavestMessageVfr>());
        break;
      }

      case NavestMessageType::VPR: {
        handleIncomingVpr(parsed.second.value<NavestMessageVpr>());
        break;
      }

      case NavestMessageType::DDD: {
        break;
      }

      case NavestMessageType::OCT: {
        break;
      }

      case NavestMessageType::NDS: {
        break;
      }

      case NavestMessageType::DEP: {
        break;
      }

      case NavestMessageType::TDR: {
        break;
      }

      case NavestMessageType::Invalid:
        qCWarning(plugin_navest) << "Failed to parse navest message:"
                                 << msg.data();
        break;

      default:
        qCDebug(plugin_navest) << "Ignoring message of type:"
                               << static_cast<int>(parsed.first);
        break;
    }

    emit messageReceived(parsed.first, parsed.second);
  }
}

NavestLayer*
Navest::createNewLayer(const QString& name, qreal lon, qreal lat, int beacon,
                       const QString& fix_source)
{
  
  auto scene = m_view->mapScene();

  if (!scene) {
    qCDebug(plugin_navest) << "No map scene associated with view.";
    return nullptr;
  }

  auto proj = scene->projection();
  if (!proj) {
    qCInfo(
      plugin_navest,
      "Map scene has no projection yet; setting to UTM for lon: %f, lat: %f",
      lon, lat);
    proj = dslmap::Proj::utmFromLonLat(lon, lat);
    scene->setProjection(proj);
  }

  qCInfo(plugin_navest) << "Creating new layer:" << name;
  auto layer = new NavestLayer;
  layer->setProjection(proj);
  layer->setVisible(true);
  layer->setName(name);
  layer->setNavestBeaconId(beacon);

  if (!fix_source.isEmpty()) {
    layer->setVehicleSolution(fix_source);
  }

  return layer;
}
void Navest::handleIncomingVfr(NavestMessageVfr vfr)
{
  //we wait for a name and for a fix source before populating the navest
  //source onto the map. We still update the position fix even if no 
  //name yet

  if (!m_view) {
    qCDebug(plugin_navest) << "No map view associated with plugin.";
    return;
  }

  if (m_ignored_beacon_ids.contains(vfr.vehicle_number)) {
    qCDebug(plugin_navest) << "Ignoring VFR update from beacon id:"
                           << vfr.vehicle_number;
    return;
  }
  
  //Let's not send updates to layers with bad coordinates
  if (vfr.lat > 90 || vfr.lat < -90){
    return;
  }

  if (vfr.lon > 180 || vfr.lon < -180){
    return;
  }

  // 2021-10-29 SS - added 0,0 as bad coordinates to discard
  if (vfr.lat == 0 || vfr.lon == 0){
    qCDebug(plugin_navest) << "Discarding null VFR coordinates";
    return;
  }

  auto _layer = layer(vfr.vehicle_number);
  

  if (_layer){
    //qDebug(plugin_navest) << "For vehicle number " << vfr.vehicle_number << " found layer with name " << _layer->name();
    //_layer->setVehicleSolution(vfr.fix_source);
    _layer->setName(_layer->name());
    if (_layer->vehicleSolution().isEmpty()){
      _layer->setVehicleSolution(vfr.fix_source);
    }
  }

  ///additional layer logic. Throw out fix if recent fix matching this fix source is over 1000 km away within a SMALL time frame
  //added by tj to mitigate bad fixes during ops 2024-04-14
  if (_layer) {
    //qstringlist of fix sources

    auto trail_ptr = _layer->trailLayerPtr(vfr.fix_source);
    if (!trail_ptr){
      return;
    }

    auto trail_size = trail_ptr->constTrailMarkers().size();
    qDebug(plugin_navest) << "Trail marker size is " << trail_size;

    if (trail_size > 0) {
      qDebug(plugin_navest)
        << "current position for fix " << vfr.fix_source << " is "
        << trail_ptr->currentLonLatPosition().y() << " : "
        << trail_ptr->currentLonLatPosition().x();

      auto distance =
        dslmap::calculateDistance(QPointF(vfr.lon, vfr.lat),
                          QPointF(trail_ptr->currentLonLatPosition().x(),
                                  trail_ptr->currentLonLatPosition().y()));
      qDebug(plugin_navest) << "Distance from last pos for fix "
                            << vfr.fix_source << " is: " << distance;

      // Check if there are other named fixes for the same vehicle ID
      bool hasOtherNamedFixes = false;
      bool isCloseToOtherFix = false;
      for (const auto& solution : _layer->vehicleSolutions()) {
        if (solution != vfr.fix_source) {
          auto otherTrailPtr = _layer->trailLayerPtr(solution);
          if (otherTrailPtr && !otherTrailPtr->constTrailMarkers().isEmpty()) {
            hasOtherNamedFixes = true;
            auto otherFixDistance = dslmap::calculateDistance(QPointF(vfr.lon, vfr.lat),
                                                      otherTrailPtr->currentLonLatPosition());
            if (otherFixDistance <= 500000) {
              isCloseToOtherFix = true;
              break;
            }
          }
        }
      }

      if (!hasOtherNamedFixes) {
        qDebug() << "No other named fixes found, accepting the fix.";
      } else if (distance > 500000 && !isCloseToOtherFix) {
        // not doing a velocity check. this isnt really intended to be an outlier filter,
        // but instead to prevent bad data from messing up navgs projection and causing issues.
        qDebug(plugin_navest) << "THROWING OUT INCOMING VFR " << vfr.fix_source
                              << " Lat: " << vfr.lat << " Lon: " << vfr.lon
                              << " as it is over 1000 km away from last "
                              << vfr.fix_source << " fix and not close to other named fixes";
        return;
      } else {
        qDebug() << "keeping incoming vfr";
      }
    }
  }

  if (!_layer) {
    _layer =
      createNewLayer("", vfr.lon, vfr.lat, vfr.vehicle_number, vfr.fix_source);

    if (!_layer) {
      return;
    }

    addLayer(_layer);
  }

  const auto attributes =
    QVariantMap{ { "navest id", vfr.vehicle_number },
                 { "source", vfr.fix_source },
                 { "name", _layer->name() },
                 { "lat", QString::number(vfr.lat, 'f', 6) },
                 { "lon", QString::number(vfr.lon, 'f', 6) },
                 { "depth", QString::number(vfr.depth, 'f', 2) } };
  _layer->updatePositionLonLat(vfr.lon, vfr.lat, vfr.fix_source, vfr.timestamp,
                               nullptr, {}, attributes);
  
  // Adding a bool to this, to indicate if fix source is external
  // Will need to pull this bool from navest settings/per vehicle/per fix source
  bool is_external_fix = _layer->getSourceExternalFix(vfr.fix_source);
  emit vfrMessageReceived(vfr.vehicle_number, vfr.fix_source, vfr.lon, vfr.lat,
                          vfr.depth, is_external_fix);
  // let's emit both types of vfr signals to ensure we dont lose existing functionality
  emit vfrMessageReceived(vfr.vehicle_number, vfr.fix_source, vfr.lon, vfr.lat,
                          vfr.depth);
                          

  auto scene = m_view->mapScene();
  if (!_layer->scene() && scene) {
    if (!_layer->vehicleSolution().isEmpty() && _layer->name()!=""){

      qDebug() << "in vfr update adding layer " << _layer->name() << " to scene";
      scene->addLayer(_layer);
    }
  }
}

void
Navest::handleIncomingVpr(NavestMessageVpr vpr)
{

  if (!m_view) {
    qCDebug(plugin_navest) << "No map view associated with plugin.";
    return;
  }

  if (m_ignored_beacon_ids.contains(vpr.vehicle_number)) {
    qCDebug(plugin_navest) << "Ignoring VPR update from beacon id:"
                           << vpr.vehicle_number;
    return;
  }
 
  //Let's not send updates to layers with bad coordinates
  if (vpr.lat > 90 || vpr.lat < -90){
    return;
  }

  if (vpr.lon > 180 || vpr.lon < -180){
    return;
  }

  auto _layer = layer(vpr.vehicle_number);

  //if (_layer) {
  if (_layer && _layer->name() == "") {

    _layer->setName(vpr.vehicle_name);
   
  }
  

  //if there is no _layer in our m_layers list lets make one and add it
  if (!_layer) {

    _layer = createNewLayer(
      vpr.vehicle_name, vpr.lon, vpr.lat, vpr.vehicle_number, "");

    if (!_layer) {
      return;
    }

    addLayer(_layer);
  }
  //update the layers heading. Either the new one or the m_layer member
  if (!_layer->acommsHeading()){
    _layer->updateHeading(vpr.heading);
  }
}

void 
Navest::setHeadingMode(bool use_acomm, int vehicle_id){
  auto _layer = layer(vehicle_id);
  if (!_layer) return;
  _layer->setHeadingMode(use_acomm,vehicle_id);
}

QString
Navest::vehicleName(int vehicle_id)
{
  Q_UNUSED(vehicle_id)
  return QString();
}

NavestLayer*
Navest::layer(int id)
{
  const auto it = std::find_if(
    std::begin(m_layers), std::end(m_layers),
    [id](NavestLayer* layer) { return layer->navestBeaconId() == id; });
  return it != std::end(m_layers) ? *it : nullptr;
}

QMap<int, NavestLayer*>
Navest::layers() const
{
  auto layermap = QMap<int, NavestLayer*>{};
  std::for_each(std::cbegin(m_layers), std::cend(m_layers),
                [&layermap](NavestLayer* layer) {
                  layermap.insert(layer->navestBeaconId(), layer);
                });

  return layermap;
}

QList<QAction*>
Navest::pluginMenuActions()
{
  return { m_action_show_settings, m_action_show_comms };
}

void
Navest::updatePositionLonLat(int id, qreal lon, qreal lat,
                             const QString& solution,
                             const QDateTime& timestamp, const qreal* heading,
                             const QString& label,
                             const dslmap::MarkerItem::Attributes& attributes)
{

  auto layer_ = layer(id);
  if (!layer_) {
    return;
  }

  layer_->updatePositionLonLat(lon, lat, solution, timestamp, heading, label,
                               attributes);
}

void
Navest::updateHeading(int id, qreal heading)
{

  auto layer_ = layer(id);
  if (!layer_) {
    return;
  }

  layer_->updateHeading(heading);
}

void
Navest::zoomToLayer(dslmap::MapLayer* layer)
{
  auto zoom_level = m_view->zoomLevel();
  // m_view->zoomToRect(layer->boundingRect());                                                          
  m_view->zoom(layer->boundingRect().center(), zoom_level, false);
}

void
Navest::updateSolutionSelected(int id, QString solution)
{

  // set solution for alvin
  const auto SOLN = solution;
  // first call Navest::layer(id)
  auto _layer = layer(id); 
  
  // then call NavestLayer::setVehicleSolution(type)
  _layer->setVehicleSolution(SOLN); 
  //emit signal upon completion to update color of solution selector button
  zoomToLayer(_layer);
  
  emit navestSolutionChanged(solution);
  
}  

} // navest
} // plugins
} // navg
