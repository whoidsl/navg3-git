/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 8/2/18.
//

#include "NavestLayer.h"
#include "NavestLayerPropertiesWidget.h"

#include <dslmap/BeaconLayer.h>
#include <dslmap/Proj.h>
#include <dslmap/ShapeMarkerSymbol.h>
#include <dslmap/WktMarkerSymbol.h>

#include <QDateTime>
#include <QGraphicsScene>
#include <QMetaEnum>
#include <QSettings>
#include <QStringListModel>

namespace navg {
namespace plugins {
namespace navest {

NavestLayer::NavestLayer(int beacon_number, QGraphicsItem* parent)
  : MapLayer(parent)
  , m_beacon_number(beacon_number)
  , m_beacon_symbol(std::make_shared<dslmap::ShapeMarkerSymbol>(
      dslmap::ShapeMarkerSymbol::Shape::Circle, 10))
{
  setFlag(ItemHasNoContents, true);
}

NavestLayer::~NavestLayer() = default;

void
NavestLayer::setNavestBeaconId(int id)
{
  m_beacon_number = id;
}

int
NavestLayer::navestBeaconId() const noexcept
{
  return m_beacon_number;
}

bool
NavestLayer::saveSettings(QSettings& settings)
{
  settings.setValue("navest_id", navestBeaconId());
  settings.setValue("active_solution",
                    QVariant::fromValue(vehicleSolution()).toString());
  settings.setValue("name", name());
  if (m_beacon_symbol) {
    m_beacon_symbol->saveSettings(settings, "vehicle_symbol/");
  }

  settings.setValue("vehicle_symbol/visible", vehicleVisible());

  settings.beginWriteArray("trail");
  auto i = 0;
  for (const auto& solution : m_beacon_layer.keys()) {
    const auto& layer = m_beacon_layer.value(solution);
    const auto trail_symbol = layer->trailSymbolPtr();
    if (!trail_symbol) {
      continue;
    }

    settings.setArrayIndex(i);
    settings.setValue("solution", solution);
    settings.setValue("is_external_fix", layer->isExternalFix());
    trail_symbol->saveSettings(settings, "symbol/");
    settings.setValue("visible", layer->trailVisible());
    settings.setValue("length", layer->displayedLength());
    settings.setValue("capacity", layer->capacity());
    ++i;
  }
  settings.endArray();
  return true;
}

std::unique_ptr<NavestLayer>
NavestLayer::fromSettings(QSettings& settings)
{

  auto layer = std::make_unique<NavestLayer>();
  bool success = layer->loadSettings(settings);

  if (!success){
    return nullptr;
  }

  return layer;
}
bool
NavestLayer::loadSettings(QSettings& settings)
{
  setName(settings.value("name", "").toString());

  if (settings.contains("vehicle_symbol/shape_wkt")) {
    m_beacon_symbol =
      dslmap::WktMarkerSymbol::fromSettings(settings, "vehicle_symbol/");
    m_beacon_symbol->setScaleInvariant(false);
  } else {
    m_beacon_symbol =
      dslmap::ShapeMarkerSymbol::fromSettings(settings, "vehicle_symbol/");
    m_beacon_symbol->setScaleInvariant(true);
  }

  setVehicleVisible(settings.value("vehicle_symbol/visible", true).toBool());

  const auto num_trails = settings.beginReadArray("trail");
  for (auto i = 0; i < num_trails; ++i) {
    settings.setArrayIndex(i);
    const auto solution = settings.value("solution", "").toString();
    if (solution.isEmpty()) {
      qWarning() << "Skipping trail #" << i << "for layer" << name()
                 << "Missing 'solution' parameter.";
      continue;
    }
    auto layer = m_createNewLayer(solution);

    if (settings.contains("symbol/shape_wkt")) {
      auto symbol = dslmap::WktMarkerSymbol::fromSettings(settings, "symbol/");
      layer->setTrailSymbol(std::move(symbol));
    } else {
      auto symbol =
        dslmap::ShapeMarkerSymbol::fromSettings(settings, "symbol/");
      layer->setTrailSymbol(std::move(symbol));
    }
    layer->setBeaconSymbol(m_beacon_symbol);
    const auto visible = settings.value("visible", true).toBool();
    setTrailVisible(solution, visible);
    
    bool fix = settings.value("is_external_fix").toBool();
    layer->setAsExternalFix(fix);
    qDebug() << "emitting check fix box";
    //emit checkFixBox(fix);

    layer->setCapacity(settings.value("capacity", 1000).toInt());
    layer->setDisplayedLength(settings.value("length", 1000).toInt());
  }
  settings.endArray();

  setNavestBeaconId(settings.value("navest_id", -1).toInt());
  const auto vehicle_solution =
    settings.value("active_solution", "").toString();
  if (vehicle_solution.isEmpty()) {
    const auto keys = m_beacon_layer.keys();
    if (!keys.isEmpty()) {
      setVehicleSolution(keys.first());
    }
  } else {
    setVehicleSolution(vehicle_solution);
  }
  return true;
}

QString
NavestLayer::vehicleSolution() const noexcept
{
  return m_solution_type;
}

dslmap::BeaconLayer*
NavestLayer::m_createNewLayer(const QString& solution)
{

  auto new_layer = new dslmap::BeaconLayer();
  new_layer->setBeaconSymbol(m_beacon_symbol);

  const auto symbol = std::make_shared<dslmap::ShapeMarkerSymbol>(
    dslmap::ShapeMarkerSymbol::Shape::Circle, 10);
  new_layer->setTrailSymbol(symbol);
  new_layer->setProjection(projection());
  if (m_trail_visible.contains(solution)) {
    new_layer->setTrailVisible(m_trail_visible.value(solution));
  } else {
    m_trail_visible[solution] = false;
    new_layer->setTrailVisible(false);
  }
  new_layer->setBeaconVisible(vehicleSolution() == solution);

  new_layer->setParentItem(this);

  QObject::connect(new_layer, &dslmap::BeaconLayer::displayedLengthChanged,
                   [this, solution](int length) {
                     emit trailLengthChanged(solution, length);
                   });

  QObject::connect(new_layer, &dslmap::BeaconLayer::capacityChanged,
                   [this, solution](int capacity) {
                     emit trailCapacityChanged(solution, capacity);
                   });

  m_beacon_layer.insert(solution, new_layer);
  emit trailAdded(solution);
  return new_layer;
}

void
NavestLayer::setVehicleSolution(const QString& type)
{
  if (type == m_solution_type) {
    return;
  }

  const auto old_solution_type = m_solution_type;
  auto layer = m_beacon_layer.value(old_solution_type, nullptr);
  // Disconnect old solution layer slots.

  if (layer) {
    disconnect(layer,
               &dslmap::BeaconLayer::currentPositionChanged,
               this,
               &NavestLayer::handleCurrentPositionChanged);
    layer->setBeaconVisible(false);
  }

  layer = m_beacon_layer.value(type, nullptr);

  // Create a new layer if needed.
  if (!layer) {
    layer = m_createNewLayer(type);
  }

  // Connect new layer signals
  /*
  connect(layer, SIGNAL(currentPositionChanged(const QPointF&)), this,
          SIGNAL(currentPositionChanged(const QPointF&)));
  */
  connect(layer,
          &dslmap::BeaconLayer::currentPositionChanged,
          this,
          &NavestLayer::handleCurrentPositionChanged);
	  
  layer->setBeaconVisible(true);

  m_solution_type = type;
  prepareGeometryChange();
  emit fixSolutionChanged(type);
}

void
NavestLayer::setTrailVisible(const QString& type, bool visible)
{
  auto _visible = m_trail_visible[type];
  if (_visible == visible) {
    return;
  }

  auto layer = m_beacon_layer.value(type, nullptr);
  if (!layer) {
    return;
  }

  layer->setTrailVisible(visible);
  m_trail_visible[type] = visible;
  emit trailVisibilityChanged(type, visible);
}

bool
NavestLayer::trailVisible(const QString& type) const noexcept
{
  return m_trail_visible.value(type, false);
}

QRectF
NavestLayer::boundingRect() const
{
  auto layer = m_beacon_layer.value(m_solution_type, nullptr);
  if (!layer) {
    return {};
  }

  const auto rect = layer->mapRectToParent(layer->boundingRect());
  return rect;
}

void
NavestLayer::updateHeading(qreal heading)
{
  for (auto& layer : m_beacon_layer) {
    if (layer) {
      layer->updateHeading(heading);
    }
  }
}

void 
NavestLayer::setHeadingMode(bool use_acomm, int vehicle_id){
  use_acomm_heading = use_acomm;
}

bool 
NavestLayer::acommsHeading(){
  return use_acomm_heading;
}

bool
NavestLayer::updateHeadingFromVfr() const noexcept
{
  return m_update_heading_from_vfr;
}
void
NavestLayer::setUpdateHeadingFromVfr(bool enabled)
{
  if (enabled == m_update_heading_from_vfr) {
    return;
  }

  m_update_heading_from_vfr = enabled;
  emit updateHeadingFromVfrChanged(enabled);
}

void
NavestLayer::updatePositionLonLat(
  qreal lon, qreal lat, const QString& solution, const QDateTime& timestamp,
  const qreal* heading, const QString& label,
  const dslmap::MarkerItem::Attributes& attributes)
{
  auto layer = m_beacon_layer.value(solution, nullptr);
  // Create a new layer if needed.
  if (!layer) {
    layer = m_createNewLayer(solution);
    if (solution == vehicleSolution()) {
      // Connect new layer signals

      connect(layer,
              &dslmap::BeaconLayer::currentPositionChanged,
              this,
              &NavestLayer::handleCurrentPositionChanged);
    }
  }

  layer->updatePositionLonLat(lon, lat, timestamp, heading, label, attributes);
  if (solution == vehicleSolution() || trailVisible(solution)) {
    prepareGeometryChange();
  }
}

void 
NavestLayer::handleCurrentPositionChanged(QPointF pos){
  emit currentPositionChanged(pos);
}

QPointF
NavestLayer::currentPosition() const
{
  const auto layer = m_beacon_layer.value(vehicleSolution(), nullptr);
  if (!layer) {
    return {};
  }
  return layer->currentPosition();
}

qreal
NavestLayer::currentHeading() const noexcept
{
  const auto layer = m_beacon_layer.value(vehicleSolution(), nullptr);
  if (!layer) {
    return {};
  }
  return layer->currentHeading();
}

void
NavestLayer::showPropertiesDialog()
{
  auto widget = new NavestLayerPropertiesWidget();
  widget->setNavestLayer(this);
  widget->show();
}

void
NavestLayer::paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*)
{
}

void
NavestLayer::setProjection(std::shared_ptr<const dslmap::Proj> proj)
{
  if (!proj) {
    return;
  }

  auto existing = projectionPtr();
  if (existing && proj->isSame(*existing)) {
    return;
  }

  for (auto& l : m_beacon_layer) {
    if (!l) {
      continue;
    }

    l->setProjection(proj);
  }

  MapLayer::setProjection(proj);
}

QStringList
NavestLayer::vehicleSolutions() const noexcept
{
  return m_beacon_layer.keys();
}

void
NavestLayer::setVehicleVisible(bool visible)
{
  if (visible == m_vehicle_visible) {
    return;
  }

  m_vehicle_visible = visible;
  auto layer = m_beacon_layer.value(m_solution_type, nullptr);
  if (!layer) {
    return;
  }

  layer->setBeaconVisible(visible);
  emit vehicleVisibilityChanged(visible);
}

bool
NavestLayer::vehicleVisible() const noexcept
{
  return m_vehicle_visible;
}

void
NavestLayer::setVehicleSymbol(std::shared_ptr<dslmap::MarkerSymbol> symbol)
{
  if (!symbol) {
    return;
  }

  for (auto& con : m_beacon_symbol_connections) {
    QObject::disconnect(con);
  }
  m_beacon_symbol_connections.clear();

  const auto ptr = symbol.get();
  m_beacon_symbol_connections.clear();
  m_beacon_symbol_connections
    << QObject::connect(ptr, &dslmap::MarkerSymbol::outlineColorChanged, this,
                        &NavestLayer::vehicleOutlineColorChanged);

  m_beacon_symbol_connections
    << QObject::connect(ptr, &dslmap::MarkerSymbol::fillColorChanged, this,
                        &NavestLayer::vehicleFillColorChanged);

  m_beacon_symbol_connections
    << QObject::connect(symbol.get(), &dslmap::MarkerSymbol::fillStyleChanged,
                        this, &NavestLayer::vehicleFillStyleChanged);

  m_beacon_symbol_connections << QObject::connect(
    symbol.get(), &dslmap::MarkerSymbol::outlineStyleChanged, this,
    &NavestLayer::vehicleOutlineStyleChanged);

  m_beacon_symbol = std::move(symbol);
  for (auto& layer : m_beacon_layer.values()) {
    if (!layer) {
      continue;
    }
    layer->setBeaconSymbol(m_beacon_symbol);
  }

  emit vehicleOutlineColorChanged(ptr->outlineColor());
  emit vehicleFillColorChanged(ptr->fillColor());
}
std::shared_ptr<dslmap::MarkerSymbol>
NavestLayer::vehicleSymbol() const noexcept
{
  return m_beacon_symbol;
}
dslmap::MarkerSymbol*
NavestLayer::vehicleSymbolPtr() const noexcept
{
  return m_beacon_symbol.get();
}
void
NavestLayer::setTrailSymbol(const QString& solution,
                            std::shared_ptr<dslmap::ShapeMarkerSymbol> symbol)
{
  if (!symbol) {
    return;
  }

  auto layer = m_beacon_layer.value(solution, nullptr);

  if (!layer) {
    return;
  }

  auto symbol_connections = m_trail_symbol_connections[solution];
  for (auto& con : symbol_connections) {
    QObject::disconnect(con);
  }

  symbol_connections.clear();
  const auto ptr = symbol.get();
  symbol_connections << QObject::connect(
    ptr, &dslmap::ShapeMarkerSymbol::outlineColorChanged,
    [this, solution](QColor color) {
      emit trailOutlineColorChanged(solution, color);
    });

  symbol_connections << QObject::connect(
    ptr, &dslmap::ShapeMarkerSymbol::outlineStyleChanged,
    [this, solution](Qt::PenStyle style) {
      emit trailOutlineStyleChanged(solution, style);
    });

  symbol_connections << QObject::connect(
    ptr, &dslmap::ShapeMarkerSymbol::fillColorChanged,
    [this, solution](QColor color) {
      emit trailFillColorChanged(solution, color);
    });

  symbol_connections << QObject::connect(
    ptr, &dslmap::ShapeMarkerSymbol::fillStyleChanged,
    [this, solution](Qt::BrushStyle style) {
      emit trailFillStyleChanged(solution, style);
    });

  symbol_connections << QObject::connect(
    ptr, &dslmap::ShapeMarkerSymbol::shapeChanged,
    [this, solution](dslmap::ShapeMarkerSymbol::Shape shape) {
      emit trailSymbolShapeChanged(solution, shape);
    });

  symbol_connections << QObject::connect(
    ptr, &dslmap::ShapeMarkerSymbol::sizeChanged, [this, solution](qreal size) {
      emit trailSymbolSizeChanged(solution, size);
    });

  layer->setTrailSymbol(std::move(symbol));
  emit trailOutlineColorChanged(solution, layer->trailOutlineColor());
  emit trailFillColorChanged(solution, layer->trailFillColor());
  emit trailSymbolShapeChanged(solution, ptr->shape());
  emit trailSymbolSizeChanged(solution, ptr->size());
}

std::shared_ptr<dslmap::ShapeMarkerSymbol>
NavestLayer::trailSymbol(const QString& solution) const noexcept
{
  auto layer = m_beacon_layer.value(solution, nullptr);

  if (!layer) {
    return {};
  }

  return std::dynamic_pointer_cast<dslmap::ShapeMarkerSymbol>(
    layer->trailSymbol());
}

dslmap::ShapeMarkerSymbol*
NavestLayer::trailSymbolPtr(const QString& solution) const noexcept
{
  auto layer = m_beacon_layer.value(solution, nullptr);

  if (!layer) {
    return {};
  }

  return dynamic_cast<dslmap::ShapeMarkerSymbol*>(layer->trailSymbolPtr());
}

dslmap::BeaconLayer*
NavestLayer::trailLayerPtr(const QString& solution) const noexcept
{
  auto layer = m_beacon_layer.value(solution, nullptr);
  if (!layer) {
    return {};
  }
  return layer;
}

void
NavestLayer::setVehicleOutlineColor(const QColor& color)
{
  if (m_beacon_symbol) {
    m_beacon_symbol->setOutlineColor(color);
  }
}
QColor
NavestLayer::vehicleOutlineColor() const noexcept
{
  if (m_beacon_symbol) {
    return m_beacon_symbol->outlineColor();
  }

  return {};
}

void
NavestLayer::setVehicleFillColor(const QColor& color)
{
  if (m_beacon_symbol) {
    m_beacon_symbol->setFillColor(color);
  }
}
QColor
NavestLayer::vehicleFillColor() const noexcept
{
  if (m_beacon_symbol) {
    return m_beacon_symbol->fillColor();
  }

  return {};
}
void
NavestLayer::setVehicleLabelColor(const QColor& color)
{
  if (m_beacon_symbol) {
    m_beacon_symbol->setLabelColor(color);
  }
}
QColor
NavestLayer::vehicleLabelColor() const noexcept
{
  if (m_beacon_symbol) {
    return m_beacon_symbol->labelColor();
  }

  return {};
}

void
NavestLayer::setTrailOutlineColor(const QString& type, const QColor& color)
{

  auto layer = m_beacon_layer.value(type, nullptr);
  if (layer) {
    layer->setTrailOutlineColor(color);
  }
}
QColor
NavestLayer::trailOutlineColor(const QString& type) const noexcept
{
  auto layer = m_beacon_layer.value(type, nullptr);
  if (layer) {
    return layer->trailLabelColor();
  }
  return {};
}
void
NavestLayer::setTrailFillColor(const QString& type, const QColor& color)
{
  auto layer = m_beacon_layer.value(type, nullptr);
  if (layer) {
    layer->setTrailFillColor(color);
  }
}
QColor
NavestLayer::trailFillColor(const QString& type) const noexcept
{
  auto layer = m_beacon_layer.value(type, nullptr);
  if (layer) {
    return layer->trailFillColor();
  }
  return {};
}
void
NavestLayer::setTrailLabelColor(const QString& type, const QColor& color)
{
  auto layer = m_beacon_layer.value(type, nullptr);
  if (layer) {
    layer->setTrailLabelColor(color);
  }
}
QColor
NavestLayer::trailLabelColor(const QString& type) const noexcept
{
  auto layer = m_beacon_layer.value(type, nullptr);
  if (layer) {
    return layer->trailLabelColor();
  }
  return {};
}

void
NavestLayer::setTrailSymbolShape(const QString& type,
                                 dslmap::ShapeMarkerSymbol::Shape shape)
{
  auto layer = m_beacon_layer.value(type, nullptr);

  if (!layer || shape == dslmap::ShapeMarkerSymbol::Shape::Invalid) {
    return;
  }

  auto symbol =
    qobject_cast<dslmap::ShapeMarkerSymbol*>(layer->trailSymbolPtr());
  if (!symbol) {
    return;
  }

  symbol->setShape(shape);
}

dslmap::ShapeMarkerSymbol::Shape
NavestLayer::trailSymbolShape(const QString& type)
{
  const auto layer = m_beacon_layer.value(type, nullptr);
  if (!layer) {
    return dslmap::ShapeMarkerSymbol::Shape::Invalid;
  }

  const auto symbol =
    qobject_cast<dslmap::ShapeMarkerSymbol*>(layer->trailSymbolPtr());
  if (!symbol) {
    return dslmap::ShapeMarkerSymbol::Shape::Invalid;
  }

  return symbol->shape();
}

void
NavestLayer::setTrailSymbolSize(const QString& type, qreal size)
{
  auto layer = m_beacon_layer.value(type, nullptr);

  if (!layer || size < 0) {
    return;
  }

  auto symbol =
    qobject_cast<dslmap::ShapeMarkerSymbol*>(layer->trailSymbolPtr());
  if (!symbol) {
    return;
  }

  symbol->setSize(size);
}

qreal
NavestLayer::trailSymbolSize(const QString& type)
{
  const auto layer = m_beacon_layer.value(type, nullptr);
  if (!layer) {
    return -1;
  }

  const auto symbol =
    qobject_cast<dslmap::ShapeMarkerSymbol*>(layer->trailSymbolPtr());

  if (!symbol) {
    return -1;
  }

  return symbol->size();
}

void
NavestLayer::setTrailLength(const QString& type, int length)
{

  auto layer = m_beacon_layer.value(type, nullptr);
  if (layer) {
    layer->setDisplayedLength(length);
  }
}
int
NavestLayer::trailLength(const QString& type) const noexcept
{
  const auto layer = m_beacon_layer.value(type, nullptr);
  if (layer) {
    return layer->displayedLength();
  }

  return -1;
}
void
NavestLayer::setTrailCapacity(const QString& type, int capacity)
{

  auto layer = m_beacon_layer.value(type, nullptr);
  if (layer) {
    layer->setCapacity(capacity);
  }
}

int
NavestLayer::trailCapacity(const QString& type) const noexcept
{

  const auto layer = m_beacon_layer.value(type, nullptr);
  if (layer) {
    return layer->capacity();
  }
  return -1;
}

void
NavestLayer::setVehicleFillStyle(Qt::BrushStyle style)
{
  if (m_beacon_symbol) {
    m_beacon_symbol->setFillStyle(style);
  }
}
Qt::BrushStyle
NavestLayer::vehicleFillStyle() const noexcept
{
  if (m_beacon_symbol) {
    return m_beacon_symbol->fillStyle();
  }
  return Qt::NoBrush;
}
void
NavestLayer::setVehicleOutlineStyle(Qt::PenStyle style)
{
  if (m_beacon_symbol) {
    m_beacon_symbol->setOutlineStyle(style);
  }
}
Qt::PenStyle
NavestLayer::vehicleOutlineStyle() const noexcept
{
  if (m_beacon_symbol) {
    return m_beacon_symbol->outlineStyle();
  }

  return Qt::NoPen;
}

void
NavestLayer::setTrailFillStyle(const QString& type, Qt::BrushStyle style)
{
  auto layer = m_beacon_layer.value(type, nullptr);
  if (layer) {
    layer->setTrailFillStyle(style);
  }
}
Qt::BrushStyle
NavestLayer::trailFillStyle(const QString& type) const noexcept
{
  const auto layer = m_beacon_layer.value(type, nullptr);
  if (layer) {
    return layer->trailFillStyle();
  }

  return Qt::NoBrush;
}
void
NavestLayer::setTrailOutlineStyle(const QString& type, Qt::PenStyle style)
{
  auto layer = m_beacon_layer.value(type, nullptr);
  if (layer) {
    layer->setTrailOutlineStyle(style);
  }
}
Qt::PenStyle
NavestLayer::trailOutlineStyle(const QString& type) const noexcept
{
  const auto layer = m_beacon_layer.value(type, nullptr);
  if (layer) {
    return layer->trailOutlineStyle();
  }

  return Qt::NoPen;
}

bool 
NavestLayer::getSourceExternalFix(const QString& type){
  const auto layer = m_beacon_layer.value(type, nullptr);
  if (layer) {
    if (layer->isExternalFix()){
      return true;
    }
  }
  return false;
}
}
}
}
