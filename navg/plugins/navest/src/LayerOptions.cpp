/**
 * Copyright 2019 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// Created by zac on 8/9/18.
//

#include "LayerOptions.h"
#include "BeaconTrailPropertiesWidget.h"
#include "NavestLayer.h"
#include "ui_LayerOptions.h"


#include <dslmap/BeaconLayer.h>
#include <dslmap/ColorPickButton.h>
#include <dslmap/WktMarkerSymbol.h>

#include <QBrush>
#include <QComboBox>
#include <QDebug>
#include <QMetaEnum>
#include <QPen>
#include <QStandardItemModel>

namespace navg {
namespace plugins {
namespace navest {

LayerOptions::LayerOptions(NavestLayer* layer, bool beacononly, QWidget* parent)
  : QWidget(parent)
  , ui(std::make_unique<Ui::LayerOptions>())
  , m_layer(layer)
  , m_trail_source_model(new QStandardItemModel(this))
  , m_beacononly(beacononly)
{
  ui->setupUi(this);
  if (!layer) {
    return;
  }

  ui->combo_trail_source->setModel(m_trail_source_model);

  auto meta_enum = QMetaEnum::fromType<dslmap::ShapeMarkerSymbol::Shape>();
  for (auto i = 0; i < meta_enum.keyCount(); ++i) {
    const auto key = meta_enum.key(i);
    const auto value =
      static_cast<dslmap::ShapeMarkerSymbol::Shape>(meta_enum.keyToValue(key));
    if (value == dslmap::ShapeMarkerSymbol::Shape::Invalid) {
      continue;
    }

    ui->combo_marker_symbol->addItem(key, QVariant::fromValue(value));
  }
  ui->combo_marker_symbol->insertSeparator(ui->combo_marker_symbol->count());
  ui->combo_marker_symbol->addItem("Ship Type", SHIP_MARKER_STRING);
  ui->combo_marker_symbol->addItem("Sentry Type", SENTRY_MARKER_STRING);
  ui->combo_marker_symbol->addItem("Alvin Type", ALVIN_MARKER_STRING);
  ui->combo_marker_symbol->addItem("Jason Type", JASON_MARKER_STRING);
  ui->combo_marker_symbol->addItem("Medea Type", MEDEA_MARKER_STRING);
  
  const auto symbol = layer->vehicleSymbolPtr();
  if (symbol) {
    ui->button_outline->setColor(symbol->outlineColor());
    ui->check_outline->setChecked(symbol->outlineStyle() != Qt::NoPen);
    ui->button_fill->setColor(symbol->fillColor());
    ui->check_fill->setChecked(symbol->fillStyle() != Qt::NoBrush);

    if (qobject_cast<dslmap::ShapeMarkerSymbol*>(symbol)) {
      const auto shape_symbol =
        qobject_cast<dslmap::ShapeMarkerSymbol*>(symbol);
      const auto shape_index = ui->combo_marker_symbol->findData(
        QVariant::fromValue(shape_symbol->shape()));
      if (shape_index >= 0) {
        ui->combo_marker_symbol->setCurrentIndex(shape_index);
      } else {
        ui->combo_marker_symbol->setCurrentIndex(0);
      }

      ui->spinbox_marker_size->setValue(shape_symbol->size());
    }

    else if (qobject_cast<dslmap::WktMarkerSymbol*>(symbol)) {
      const auto wkt_symbol = qobject_cast<dslmap::WktMarkerSymbol*>(symbol);
      const auto wkt_string = wkt_symbol->wktString();
      const auto shape_index =
        ui->combo_marker_symbol->findData(QVariant::fromValue(wkt_string));
      if (shape_index >= 0) {
        ui->combo_marker_symbol->setCurrentIndex(shape_index);
      } else {
        ui->combo_marker_symbol->addItem("Custom Type", wkt_string);
        ui->combo_marker_symbol->setCurrentIndex(
          ui->combo_marker_symbol->count() - 1);
      }

      ui->spinbox_marker_size->setValue(wkt_symbol->minimumSize());
    }
  }

  //
  //
  // Vehicle Marker Visibility control setup
  //
  //
  ui->check_visible->setChecked(layer->vehicleVisible());
  connect(layer, &NavestLayer::vehicleVisibilityChanged, [this](bool visible) {
    ui->check_visible->setChecked(visible);
  });

  connect(ui->check_visible, &QCheckBox::clicked, [layer](bool visible) {
    layer->setVehicleVisible(visible);
  });  

  connect(layer,
          &NavestLayer::vehicleOutlineColorChanged,
          ui->button_outline,
          &dslmap::ColorPickButton::setColor);

  connect(ui->button_outline,
          &dslmap::ColorPickButton::colorChanged,
          layer,
          &NavestLayer::setVehicleOutlineColor);

// Hook to change vehicle marker shape
connect(ui->combo_marker_symbol,
        static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
        [layer, this](int index) {
          auto new_symbol = std::unique_ptr<dslmap::MarkerSymbol>{};

          // Setting the shape to a standard marker shape?
          if (index <
              static_cast<int>(dslmap::ShapeMarkerSymbol::Shape::Invalid)) {
            auto symbol = dynamic_cast<dslmap::ShapeMarkerSymbol*>(
              layer->vehicleSymbolPtr());

            // If we're already a standard shape, just change the shape type.
            const auto desired_shape =
              static_cast<dslmap::ShapeMarkerSymbol::Shape>(index);
            if (symbol) {
              symbol->setShape(desired_shape);
              return;
            }
            // Otherwise build a whole new marker symbol.
            else {
              new_symbol = std::make_unique<dslmap::ShapeMarkerSymbol>(
                desired_shape, ui->spinbox_marker_size->value());
            }
          }

          // Custom WKT-based shape.
          else {
            const auto wkt_string =
              ui->combo_marker_symbol->itemData(index).toString();

            if (wkt_string.isEmpty()) {
              return;
            }

            auto symbol =
              dynamic_cast<dslmap::WktMarkerSymbol*>(layer->vehicleSymbolPtr());
            if (symbol) {
              symbol->setPathFromWkt(wkt_string);
              symbol->setScaleInvariant(false);
              return;
            } else {
              new_symbol =
                std::make_unique<dslmap::WktMarkerSymbol>(wkt_string);
              new_symbol->setScaleInvariant(false);
            }
          }

          if (!new_symbol) {
            return;
          }

          // Copy over drawing attributes
          const auto ptr = layer->vehicleSymbolPtr();
          new_symbol->setOutlineColor(ptr->outlineColor());
          new_symbol->setOutlineStyle(ptr->outlineStyle());
          new_symbol->setOutlineWidth(ptr->outlineWidth());
          new_symbol->setFillColor(ptr->fillColor());
          new_symbol->setFillStyle(ptr->fillStyle());

          // Set the new marker
          layer->setVehicleSymbol(std::move(new_symbol));
        });

connect(ui->spinbox_marker_size,
        static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged),
        [layer](int size) {
          if (auto ptr = qobject_cast<dslmap::ShapeMarkerSymbol*>(
                layer->vehicleSymbolPtr())) {
            ptr->setSize(size);
            return;
          }

          if (auto ptr = qobject_cast<dslmap::WktMarkerSymbol*>(
                layer->vehicleSymbolPtr())) {
            ptr->setMinimumSize(size);
            return;
          }
        });

connect(ui->check_outline,
        static_cast<void (QCheckBox::*)(bool)>(&QCheckBox::toggled),
        [this, layer](bool checked) {
          if (checked) {
            layer->setVehicleOutlineStyle(Qt::SolidLine);
          } else {
            layer->setVehicleOutlineStyle(Qt::NoPen);
          }
        });

connect(layer,
        &NavestLayer::vehicleOutlineColorChanged,
        ui->button_fill,
        &dslmap::ColorPickButton::setColor);
connect(ui->button_fill,
        &dslmap::ColorPickButton::colorChanged,
        layer,
        &NavestLayer::setVehicleFillColor);
connect(ui->check_fill,
        static_cast<void (QCheckBox::*)(bool)>(&QCheckBox::toggled),
        [this, layer](bool checked) {
          if (checked) {
            layer->setVehicleFillStyle(Qt::SolidPattern);
          } else {
            layer->setVehicleFillStyle(Qt::NoBrush);
          }
        });

for (const auto& solution : layer->vehicleSolutions()) {
  addSolution(solution);
}

connect(m_layer, &NavestLayer::trailAdded, this, &LayerOptions::addSolution);

connect(ui->combo_trail_source,
        static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
        [this]() {
          const auto page =
            ui->combo_trail_source->currentData(Qt::UserRole + 1).toInt();
          ui->stack_trail_options->setCurrentIndex(page);
        });

if (ui->combo_trail_source->count() > 0) {
  ui->combo_trail_source->setCurrentIndex(0);
}

ui->combo_marker_source->setModel(m_trail_source_model);
ui->combo_marker_source->setCurrentIndex(
ui->combo_marker_source->findText(layer->vehicleSolution()));
connect(ui->combo_marker_source,
        &QComboBox::currentTextChanged,
        m_layer,
        &NavestLayer::setVehicleSolution);
connect(ui->combo_marker_source,
        &QComboBox::currentTextChanged,
        this,
        &LayerOptions::getSourceExternalFix);
connect(ui->external_fix_box,
        &QCheckBox::stateChanged,
        this,
        &LayerOptions::setSourceExternalFix);
}

LayerOptions::~LayerOptions() = default;
  
void
LayerOptions::addSolution(const QString& solution)
{
  auto item = new QStandardItem{};
  item->setData(solution, Qt::DisplayRole);
  if (!m_beacononly) {
    auto trail = m_layer->trailLayerPtr(solution);
    auto w = new dslmap::BeaconTrailPropertiesWidget(trail);
    const auto page = ui->stack_trail_options->addWidget(w);
    item->setData(page, Qt::UserRole + 1);
  }
  m_trail_source_model->setItem(m_trail_source_model->rowCount(), item);
}

void
LayerOptions::getSourceExternalFix(const QString& type)
{

  auto layer = m_layer->trailLayerPtr(type);

  if (layer) {
    auto ext_fix = layer->isExternalFix();

    ui->external_fix_box->setChecked(ext_fix);
  }
}
void
LayerOptions::setSourceExternalFix(const bool fix)
{
  auto type = ui->combo_marker_source->currentText();
  if (type.isEmpty())
    return;
  auto layer = m_layer->trailLayerPtr(type);

  if (layer) {
    layer->setAsExternalFix(fix);

  }
}


}
}
}

