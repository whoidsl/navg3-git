/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "NavestLayerPropertiesWidget.h"
#include "LayerOptions.h"
#include "NavestLayer.h"
#include "dslmap/BeaconTrailPropertiesWidget.h"
#include "ui_NavestLayerPropertiesWidget.h"

#include <QLoggingCategory>

namespace navg {
namespace plugins {
namespace navest {

NavestLayerPropertiesWidget::NavestLayerPropertiesWidget(QWidget* parent)
  : QWidget(parent)
  , ui(new Ui::NavestLayerPropertiesWidget)
{
  ui->setupUi(this);
}

NavestLayerPropertiesWidget::~NavestLayerPropertiesWidget()
{
  delete ui;
}

void
NavestLayerPropertiesWidget::setNavestLayer(NavestLayer* layer)
{
  ui->general->setLayer(qobject_cast<dslmap::MapLayer*>(layer));
  auto layer_options = new LayerOptions(layer, true, this);
  ui->tabWidget->addTab(layer_options, "Beacon");

  for (auto trail_name : layer->vehicleSolutions()) {
    auto trail = layer->trailLayerPtr(trail_name);
    auto trail_widget = new dslmap::BeaconTrailPropertiesWidget(trail, this);
    auto str = toCamelCase(trail_name);
    ui->tabWidget->addTab(trail_widget, str);
  }
}

QString
NavestLayerPropertiesWidget::toCamelCase(QString s)
{
  s.replace("SOLN_", "", Qt::CaseSensitivity::CaseSensitive);
  s = s.toLower();
  QStringList parts = s.split('_', QString::SkipEmptyParts);
  for (int i = 0; i < parts.size(); ++i) {
    parts[i].replace(0, 1, parts[i][0].toUpper());
  }
  return parts.join(" ");
}

} // namespace
}
}
