/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "Navest.h"
#include "NavestLayer.h"
#include "catch.hpp"

#include <dslmap/MarkerSymbol.h>
#include <dslmap/WktMarkerSymbol.h>

#include <QSettings>

#include <algorithm>
#include <memory>
#include <random>

using namespace navg::plugins::navest;

SCENARIO("Changing the displayed rotation of a Navest beacon")
{
  // Set up the random headings
  std::random_device rd;
  std::mt19937 mt{ rd() };
  std::uniform_real_distribution<double> uni(0.0, 360.0);

  const auto headings = [&]() {
    std::vector<double> h(100);
    std::for_each(std::begin(h), std::end(h), [&](double& v) { v = uni(mt); });
    return h;
  }();

  GIVEN("A navest layer with a ShapeMarker symbol")
  {
    auto layer = std::make_unique<NavestLayer>(0, nullptr);
    auto marker_symbol = std::make_shared<dslmap::ShapeMarkerSymbol>(
      dslmap::ShapeMarkerSymbol::Shape::Square, 10);
    const auto SOLN = QString{ "TEST" };
    layer->setVehicleSymbol(marker_symbol);
    layer->setVehicleSolution(SOLN);

    layer->updatePositionLonLat(-72.0, 42.0, SOLN);
    REQUIRE(layer->currentHeading() == 0.0);

    WHEN("The symbol heading is updated")
    {
      std::for_each(std::begin(headings), std::end(headings), [&](double h) {
        layer->updateHeading(h);
        CHECK(layer->currentHeading() == Approx(h));
      });
    }
  }

  GIVEN("A navest layer with a WktMarkerSymbol")
  {
    auto layer = std::make_unique<NavestLayer>(0, nullptr);
    auto marker_symbol =
      std::make_shared<dslmap::WktMarkerSymbol>(SENTRY_MARKER_STRING, nullptr);
    const auto SOLN = QString{ "TEST" };
    layer->setVehicleSymbol(marker_symbol);
    layer->setVehicleSolution(SOLN);

    layer->updatePositionLonLat(-72.0, 42.0, SOLN);
    REQUIRE(layer->currentHeading() == 0.0);

    WHEN("The symbol heading is updated")
    {
      std::for_each(std::begin(headings), std::end(headings), [&](double h) {
        layer->updateHeading(h);
        CHECK(layer->currentHeading() == Approx(h));
      });
    }
  }
}