/**
 * Copyright 2019 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// Created by zac on 9/26/17.
//

#include "Navest.h"
#include "NavestSettings.h"
#include "catch.hpp"

#include <QSettings>

#include <memory>

using namespace navg;
using namespace navg::plugins::navest;

SCENARIO("loading and saving plugin settings using QSettings",
         "[plugin],[navest],[navg]")
{

  GIVEN("A plugin instance and the desired settings.")
  {

    const auto remote_address = QString{ "127.0.0.1" };
    const auto remote_udp_port = 34235;
    const auto listen_udp_port = 30128;
    const auto remote_tcp_port = 30281;
    const auto ignored_vehicles = QSet<int>{ 1, 5 };
    const auto ignored_vehicles_string =
      plugins::navest::NavestSettings::beaconIdsToString(ignored_vehicles);

    QSettings settings;

    settings.setValue(Navest::settingString(Navest::Setting::RemoteHostIp),
                      remote_address);
    settings.setValue(Navest::settingString(Navest::Setting::RemoteHostUdpPort),
                      remote_udp_port);
    settings.setValue(Navest::settingString(Navest::Setting::RemoteHostTcpPort),
                      remote_tcp_port);
    settings.setValue(Navest::settingString(Navest::Setting::ListenUdpPort),
                      listen_udp_port);
    settings.setValue(Navest::settingString(Navest::Setting::IgnoredVehicleIds),
                      ignored_vehicles_string);

    auto dialog = std::make_unique<plugins::navest::Navest>();

    WHEN("the plugin loads the settings")
    {

      dialog->loadSettings(&settings);

      THEN("the settings are applied to the plugin")
      {
        REQUIRE(dialog->remoteUdpPort() == remote_udp_port);
        REQUIRE(dialog->remoteAddress().toString() == remote_address);
        REQUIRE(dialog->listenUdpPort() == listen_udp_port);
        REQUIRE(dialog->remoteTcpPort() == remote_tcp_port);
        REQUIRE(dialog->ignoredBeaconIds() == ignored_vehicles);
      }
      AND_THEN("the UDP port should be open") { REQUIRE(dialog->isBound()); }
    }
  }

  GIVEN("A plugin instance with settings to save")
  {
    const auto remote_address = QString{ "127.0.0.1" };
    const auto remote_udp_port = 34235;
    const auto listen_udp_port = 30128;
    const auto remote_tcp_port = 30281;
    const auto ignored_vehicles = QSet<int>{ 1, 5 };
    const auto ignored_vehicles_string =
      plugins::navest::NavestSettings::beaconIdsToString(ignored_vehicles);
    auto dialog = std::make_unique<plugins::navest::Navest>();

    dialog->setListenUdpPort(listen_udp_port);
    dialog->setRemoteUdpPort(remote_udp_port);
    dialog->setRemoteAddress(remote_address);
    dialog->setRemoteTcpPort(remote_tcp_port);
    dialog->setIgnoredVehicleNumbers(ignored_vehicles);

    WHEN("the plugin saves the settings")
    {
      QSettings settings;
      dialog->saveSettings(settings);

      THEN("the settings are saved with the correct keys")
      {
        REQUIRE(
          settings.value(Navest::settingString(Navest::Setting::ListenUdpPort))
            .toInt() == listen_udp_port);
        REQUIRE(
          settings
            .value(Navest::settingString(Navest::Setting::RemoteHostUdpPort))
            .toInt() == remote_udp_port);
        REQUIRE(
          settings
            .value(Navest::settingString(Navest::Setting::RemoteHostTcpPort))
            .toInt() == remote_tcp_port);
        REQUIRE(
          settings.value(Navest::settingString(Navest::Setting::RemoteHostIp))
            .toString() == remote_address);
        REQUIRE(
          settings
            .value(Navest::settingString(Navest::Setting::IgnoredVehicleIds))
            .toString() == ignored_vehicles_string);
      }
    }
  }
}
