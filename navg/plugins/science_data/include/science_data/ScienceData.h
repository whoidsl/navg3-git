//
// Created by llindzey 2019 Jan 16
//

#ifndef NAVG_SCIENCE_DATA_H
#define NAVG_SCIENCE_DATA_H

#include <navg/CorePluginInterface.h>

// QUESTION(LEL): Is it better to put includes in .h or .cpp?
#include <dslmap/GradientWidget.h>
#include <dslmap/MapScene.h>
#include <dslmap/SymbolLayer.h>

#include <QAction>
#include <QHash>
#include <QHostAddress>
#include <QJsonObject>
#include <QLoggingCategory>
#include <QObject>
#include <QTime>
#include <QWidget>

#include <memory>

namespace navg {
namespace plugins {
namespace science_data {

namespace Ui {
class ScienceData;
}

class ScienceData : public QWidget, public CorePluginInterface
{
  Q_OBJECT
  Q_INTERFACES(navg::CorePluginInterface)
  Q_PLUGIN_METADATA(IID CorePluginInterface_iid FILE "ScienceData.json")

signals:

public:
  explicit ScienceData(QWidget* parent = nullptr);
  ~ScienceData() override;

  //
  // CorePluginInterface Implementation
  //
  void loadSettings(QSettings* settings) override;
  void saveSettings(QSettings& settings) override;

  // This plugin connects to both the sentry_acomms and the navest plugins.
  void connectPlugin(const QObject* plugin,
                     const QJsonObject& metadata) override;
  void setMapView(QPointer<dslmap::MapView> view) override;

  QList<QAction*> pluginMenuActions() override;
  QList<QAction*> pluginToolbarActions() override { return {}; }

protected slots:

  void handleVfrMessageReceived(int vehicle_number, QString fix_source,
                                double lon, double lat, double depth);
  void handleScalarScience(float oxygen_concentration, float obs_raw,
                           float orp_raw, float ctd_temperature,
                           float ctd_salinity, float paro_depth);
  void handleVehicleState(float heading, float depth, float altitude,
                          float h_vel, float v_vel);

private:
  std::unique_ptr<Ui::ScienceData> m_ui;
  QPointer<dslmap::MapView> m_view;

  // Zac says that it should be safe to keep a pointer to the layer, even
  // though the scene takes ownership. Alternative would be to keep the
  // UUID, and retrieve a layer pointer from the scene (which in turn is
  // retrieved from the view) every time you want to use it.
  dslmap::SymbolLayer* m_layer;

  // List of pointers to markers in the map. Required to track these if we
  // want to be able to delete them.
  QList<dslmap::MarkerItem*> m_markers;

  // Create the layer on which the points will be displayed
  void createLayer();

  // When is navest data considered stale and no longer used for plotting
  // science data
  int m_navest_timeout;
  // Sentry's ID in navest's VFR messages
  int m_sentry_id;
  // Most recent coordinate from VFR
  double m_navest_lon;
  double m_navest_lat;
  // When most recent navest fix was received, in terms of number of
  // MILLIseconds since epoch
  qint64 m_navest_timestamp;
};

Q_DECLARE_LOGGING_CATEGORY(plugin_science_data)
} // science_data
} // plugins
} // navg

#endif // NAVG_SCIENCE_DATA_H
