//
// Created by llindzey 2019 Jan 16
//

#include "ScienceData.h"
#include "ui_ScienceData.h"

#include <dslmap/MapView.h>
#include <dslmap/SymbolLayer.h>

#include <QAction>
#include <QSettings>
#include <QStandardItemModel>

namespace navg {
namespace plugins {
namespace science_data {

Q_LOGGING_CATEGORY(plugin_science_data, "navg.plugins.science_data")

ScienceData::ScienceData(QWidget* parent)
  : QWidget(parent)
  , m_ui(std::make_unique<Ui::ScienceData>())
  , m_view(nullptr)
  , m_layer(nullptr)
{
  m_ui->setupUi(this);
}

ScienceData::~ScienceData() = default;

void
ScienceData::createLayer()
{
  dslmap::MapScene* scene = qobject_cast<dslmap::MapScene*>(m_view->mapScene());
  Q_ASSERT(scene != nullptr);

  if (m_layer != nullptr) {
    qCWarning(plugin_science_data, "Already had old layer; removing");
    auto layer = scene->removeLayer(m_layer->id());
    delete layer;
  }

  m_layer = new dslmap::SymbolLayer();

  auto scene_projection = scene->projection();
  m_layer->setProjection(scene_projection);

  auto target = scene->addLayer(m_layer);
  if (target.isNull()) {
    qCCritical(plugin_science_data, "Unable to add layer to scene.");
    return;
  }
  qCDebug(plugin_science_data, "Added layer to scene.");

  m_layer->setName("Science Data");

  int marker_size = 20;
  m_layer->setShape(dslmap::ShapeMarkerSymbol::Shape::Circle, marker_size);
  m_layer->setScaleInvariant(true);

  auto symbol_ptr = m_layer->symbolPtr();

  symbol_ptr->setOutlineColor(Qt::red);
  symbol_ptr->setFillColor(Qt::red);
  symbol_ptr->setFillStyle(Qt::SolidPattern); // defaults to Qt::NoBrush
  symbol_ptr->setLabelColor(Qt::white);

  symbol_ptr->setUseColorGradient(true);
  symbol_ptr->setGradientPreset(
    dslmap::GdalColorGradient::GradientPreset::Warm);
  symbol_ptr->setGradientRange(0.0, 10.0);

  m_layer->setLabelVisible(true);
  m_layer->setVisible(true);

  // Attach gradient widget to the layer's symbol's GdalColorGradient
  auto gradient_ptr = symbol_ptr->gradient();
  m_ui->gradWidget->setGradient(gradient_ptr);

  // TODO(LEL): The ticks don't look very good on arbitrary values (originally
  //  implemented for depths), and I haven't had time to clean it up.
  //  So, for now, just effectively turning them off.
  // m_ui->gradWidget->setLegendSpacing(1000);
  // Autoscale legend increment to have fixed number of increments between
  // min/max bounds
  // QUESTION(LEL): Is this even close to right/OK for how to capture m_ui and
  // gradient_ptr?
  // NOPE. Compiles but segfaults in some cases. (but not others ... play with
  // that!)
  auto raw_ptr = gradient_ptr.get();
  connect(gradient_ptr.get(), &dslmap::GdalColorGradient::gdalColorTableChanged,
          this, [=]() {
            float increment = (raw_ptr->max() - raw_ptr->min()) / 4.0;
            m_ui->gradWidget->setLegendSpacing(increment);
          });

  connect(m_ui->rescaleButton, &QPushButton::clicked, m_layer,
          &dslmap::SymbolLayer::rescaleGradient);
  connect(symbol_ptr, &dslmap::MarkerSymbol::gradientAttributeChanged, [=]() {
    m_ui->attributeLabel->setText(m_layer->symbolPtr()->gradientAttribute());
  });
}

void
ScienceData::setMapView(QPointer<dslmap::MapView> view)
{
  m_view = view;
}

void
ScienceData::connectPlugin(const QObject* plugin, const QJsonObject& metadata)
{
  if (!plugin) {
    return;
  }

  const auto name =
    metadata.value("MetaData").toObject().value("name").toString();
  if (name.isEmpty()) {
    return;
  }

  if (name == "navest") {
    if (!connect(plugin, SIGNAL(vfrMessageReceived(int, QString, double, double,
                                                   double)),
                 this, SLOT(handleVfrMessageReceived(int, QString, double,
                                                     double, double)))) {
      qCCritical(plugin_science_data, "Unable to connect to signal: "
                                      "navest::vfrMessageReceived");
      Q_ASSERT(false);
    } else {
      qCInfo(plugin_science_data, "Connected to SentryAcomms::messageReceived");
    }
  }

  if (name == "sentry_acomms") {
    if (!connect(plugin, SIGNAL(scalarScienceUpdated(float, float, float, float,
                                                     float, float)),
                 this, SLOT(handleScalarScience(float, float, float, float,
                                                float, float)))) {
      qCCritical(plugin_science_data, "Unable to connect to signal: "
                                      "SentryAcomms::scalarScienceUpdated");

      Q_ASSERT(false);
    } else {
      qCInfo(plugin_science_data,
             "Connected to SentryAcomms::scalarScienceUpdated");
    }

    if (!connect(plugin,
                 SIGNAL(vehicleStateUpdated(float, float, float, float, float)),
                 this,
                 SLOT(handleVehicleState(float, float, float, float, float)))) {
      qCCritical(plugin_science_data, "Unable to connect to signal: "
                                      "SentryAcomms::vehicleStateUpdated");
      Q_ASSERT(false);
    } else {
      qCInfo(plugin_science_data,
             "Connected to SentryAcomms::vehicleStateUpdated");
    }
  }
}

void
ScienceData::loadSettings(QSettings* settings)
{
  if (settings == nullptr) {
    return;
  }
  m_navest_timeout = settings->value("navest_timeout", 120).toInt();
  m_sentry_id = settings->value("sentry_navest_id", 0).toInt();
}

void
ScienceData::saveSettings(QSettings& settings)
{
  settings.setValue("navest_timeout", m_navest_timeout);
  settings.setValue("sentry_navest_id", m_sentry_id);
}

QList<QAction*>
ScienceData::pluginMenuActions()
{
  // QUESTION(LEL): When activating this plugin, I see the following error
  //   message in the terminal:
  //   warning default unknown:0 uint DBusMenuExporterDBus::GetLayout(int, int,
  //   const QStringList&, DBusMenuLayoutItem&): Condition failed: menu
  // Does this matter at all?
  return {};
}

void
ScienceData::handleVfrMessageReceived(int vehicle_number, QString fix_source,
                                      double lon, double lat, double depth)
{
  if (m_sentry_id != vehicle_number || QString("SOLN_USBL") != fix_source) {
    // There will be frequent GPS updates from the ship, and potentially other
    // tracked items (e.g. CTD). Filter those out.
    return;
  }
  // qCInfo(plugin_science_data) <<  "Got vfr message! vehicle_number: " <<
  // vehicle_number << " fix source: " << fix_source;
  m_navest_lon = lon;
  m_navest_lat = lat;
  m_navest_timestamp = QDateTime::currentMSecsSinceEpoch();
}

void
ScienceData::handleScalarScience(float oxygen_concentration, float obs_raw,
                                 float orp_raw, float ctd_temperature,
                                 float ctd_salinity, float paro_depth)
{
  const auto current_msecs = QDateTime::currentMSecsSinceEpoch();
  if (current_msecs - m_navest_timestamp > 1000 * m_navest_timeout) {
    qCInfo(plugin_science_data, "Received science data, but discarding because "
                                "navest fix is uninitialized or old");
    return;
  }

  // qCInfo(plugin_science_data, "Got science data!!");

  // Initialize the layer, if required. Doing this here, rather than at setup
  // because trying to add the layer to the scene during setMapView() fails.
  if (m_layer == nullptr) {
    createLayer();
  }

  // Add point to layer at most recent navest coordinates with the appropriate
  // attributes
  QVariantMap attributes;
  attributes.insert("Oxygen Concentration", oxygen_concentration);
  attributes.insert("raw OBS", obs_raw);
  attributes.insert("raw ORP", orp_raw);
  attributes.insert("Temperature (SBE49)", ctd_temperature);
  attributes.insert("Salinity (SBE49)", ctd_salinity);
  attributes.insert("Depth (paro)", paro_depth);
  auto marker =
    m_layer->addLonLatPoint(m_navest_lon, m_navest_lat, "", attributes);
  m_markers.append(marker);
}

void
ScienceData::handleVehicleState(float heading, float depth, float altitude,
                                float h_vel, float v_vel)
{
  const auto current_msecs = QDateTime::currentMSecsSinceEpoch();
  if (current_msecs - m_navest_timestamp > 1000 * m_navest_timeout) {
    qCInfo(plugin_science_data,
           "Received velocity data, but discarding because navest fix is old");
    return;
  }

  // qCInfo(plugin_science_data, "Got Vehicle State!");

  // Initialize the layer, if required. Checking this here, rather than once at
  // setup, because trying to add the layer to the scene during setMapView()
  // fails.
  if (m_layer == nullptr) {
    createLayer();
  }

  // Add point to layer at most recent navest coordinates with the appropriate
  // attributes
  QVariantMap attributes;
  attributes.insert("Heading", heading);
  attributes.insert("Depth", depth);
  attributes.insert("Altitude", depth);
  attributes.insert("Horizontal Velocity", h_vel);
  attributes.insert("Vertical Velocity", v_vel);
  auto marker =
    m_layer->addLonLatPoint(m_navest_lon, m_navest_lat, "", attributes);
  m_markers.append(marker);
}

} // science_data
} // plugins
} // navg
