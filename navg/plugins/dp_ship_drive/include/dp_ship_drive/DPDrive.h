/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef NAVG_DP_DRIVE_H
#define NAVG_DP_DRIVE_H

#include <navg/CorePluginInterface.h>

#include <QAction>
#include <QHash>
#include <QHostAddress>
#include <QJsonObject>
#include <QLoggingCategory>
#include <QObject>
#include <QTime>
#include <QWidget>
#include <QUdpSocket>
#include <QDateTime>
//#include <memory>

#include <dslmap/ColorPickButton.h>
#include <dslmap/MapView.h>
#include <dslmap/MapScene.h>
#include <dslmap/SymbolLayer.h>
#include <dslmap/Proj.h>

#include <../../navest/include/navest/Messages.h>
#include <../../navest/include/navest/Navest.h>
#include <../../navest/include/navest/NavestLayer.h>
#include <navg/NavGMapView.h>
#include "DPDriveSettings.h"
#include <dslmap/Util.h>

//#include "nmea.h"
//#include "convert.h"

#include <sstream>

#define	MAX_REF_SPEED 1.3
#define TRAJECTORY_TIME_CONSTANT 90.0

enum DP_TYPE{
 NO_DP,
 FAUX_GPS,
 FOLLOW_ROV
};

enum MARKER_ID{
 MARKER_GOAL,
 MARKER_REF,
 MARKER_ANTIREF
};

namespace navg {
namespace plugins {
namespace dp_drive {

namespace Ui {
class DPDrive;
}

class DPDrive : public QWidget, public navg::CorePluginInterface {
  Q_OBJECT
  Q_INTERFACES(navg::CorePluginInterface)
  Q_PLUGIN_METADATA(IID CorePluginInterface_iid FILE "DPDrive.json")

signals:
  void goalColorChanged(QColor color);
  void refColorChanged(QColor color);
  void referenceChanged(QPointF);
  void goalChanged(QPointF);

public:
  explicit DPDrive(QWidget *parent = nullptr);
  ~DPDrive() override;

  //
  // CorePluginInterface Implementation
  //
  void loadSettings(QSettings *settings) override;
  void saveSettings(QSettings &settings) override;

  // This plugin connects to acomms plugin.
  void connectPlugin(const QObject *plugin,
                     const QJsonObject &metadata) override;
  void setMapView(QPointer<dslmap::MapView> view) override;
  QList<QAction *> pluginMenuActions() override;
  QList<QAction *> pluginToolbarActions() override { return {}; }
  bool hasLayer();


public slots:
  /// \brief Show the settings dialog
  void showSettingsDialog();
  
protected slots:
  void updateFromNavestMessage(navg::NavestMessageType type,
                               QVariant message);
  void handleDirectionButton();
  void handleSpeedDialSpin(double value);
  void handleRangeSpn(double value);
  void handleBearingSpn(double value);
  void handleRangeComboChoice();
  void dpIntervalTimeout();
  void dpDisableInitTimeout();

  void changeSpeed(double speed);
  void handleEnableDPCheck(bool checked);
  void handleStartDPButton();
  void handleGoalRefButton();
  void handleGoalCursorButton();
  void handleGoalRangeBearingBtn();
  void handleDPSelectionRadios(int state);
  void handleSceneMousePress(QPointF cursor);

  void setGoalColor(QColor color);
  void setRefColor(QColor color);

  void dpSocketDataPending(void);

private:
  std::unique_ptr<Ui::DPDrive> ui;
  QAction* m_action_show_settings = nullptr;
  QPointer<dslmap::MapView> m_view;
  navg::NavGMapView *nview;

  // Layer to display goal and reference etc
  dslmap::SymbolLayer* m_layer;
  // List of pointers to markers in the map. Required to track these if we
  // want to be able to delete them.
  QList<dslmap::MarkerItem*> m_markers;

  // Create the layer on which the points will be displayed
  void createLayer();


  QDateTime                  lastRxStatusDT;
  
  QPointF current_ship_local_pos;
  QPointF last_ship_local_pos;

  double                     cursor_lon;
  double                     cursor_lat;

  double                     m_range;
  double                     m_bearing;
  
  double                     reference_speed;
  DP_TYPE                    dp_type;
  QTimer*                    dp_timer;
  QTimer*                    disable_init_timer;
  double                     dp_update_interval;

  // These handle I/O to the ship'd DP host
  QHostAddress               dpHostAddress;
  unsigned short             dpHostInSocketNumber;
  unsigned short             dpHostOutSocketNumber;
  bool                       dpEnabled;
  QUdpSocket                 *dpSocket;

  void connectUI();
  void setDPType(DP_TYPE type);

  // NAV host control interface
  void incrementGoal(double x_amount, double y_amount);
  void gotoRangeBearing(double range, double bearing);
  void gotoLonLat(double lon, double lat);
  void gotoGoalRef();

  void initializeDp(void);

};

Q_DECLARE_LOGGING_CATEGORY(plugin_dp_drive)
} // dp_drive
} // plugins
} // navg

#endif // NAVG_DP_DRIVE_H
