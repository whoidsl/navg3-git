/**
 * Copyright 2021 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "DPDriveSettings.h"
#include "ui_DPDriveSettings.h"

namespace navg {
namespace plugins {
namespace dp_drive {

DPDriveSettings::DPDriveSettings(QWidget* parent)
  : QDialog(parent)
 , ui(std::make_unique<Ui::DPDriveSettings>())
{
  ui->setupUi(this);

  // Add an ip address validator to the remote address line edit
  auto octet = QString{ "(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])" };
  auto ip_regex = QRegExp{ "^" + octet + "\\." + octet + "\\." + octet + "\\." +
                           octet + "$" };

  ui->remoteAddress->setValidator(new QRegExpValidator(ip_regex));
}

DPDriveSettings::~DPDriveSettings() = default;

void DPDriveSettings::setDPHostAddress(QHostAddress& dpHostAddress)
{
  ui->remoteAddress->setText(dpHostAddress.toString());
}
QHostAddress DPDriveSettings::getDPHostAddress() const
{
  return QHostAddress(ui->remoteAddress->text());
}

void DPDriveSettings::setDPHostInSocketNumber(int dpHostInSocketNumber)
{
  //ui->dp_host_in_socket_number_edit->setText(QString::number(dpHostInSocketNumber));
  ui->listenUdpPort->setValue(dpHostInSocketNumber);
}

int DPDriveSettings::getDPHostInSocketNumber() const
{

  return ui->listenUdpPort->value();
}

void DPDriveSettings::setDPHostOutSocketNumber(int dpHostOutSocketNumber)
{
    //ui->dp_host_out_socket_number_edit->setText(QString::number(dpHostOutSocketNumber));
    ui->remoteUdpPort->setValue(dpHostOutSocketNumber);
}

int DPDriveSettings::getDPHostOutSocketNumber() const
{
    return ui->remoteUdpPort->value();
}

} // namespace dp_drive
} // namespace plugins
} // namespace navg
