/**
 * Copyright 2020 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "DPDrive.h"
#include "ui_DPDrive.h"
#include <QAction>
#include <QDoubleSpinBox>
#include <QSettings>
#include <QTimer>
#include <dslmap/MapView.h>


namespace navg {
namespace plugins {
namespace dp_drive {

Q_LOGGING_CATEGORY(plugin_dp_drive, "navg.plugins.dp_drive")

DPDrive::DPDrive(QWidget* parent)
  : QWidget(parent)
  , ui(std::make_unique<Ui::DPDrive>())
  , m_action_show_settings(new QAction("&Settings", this))
  , m_layer(nullptr)
{
  ui->setupUi(this);

  connect(m_action_show_settings,
          &QAction::triggered,
          this,
          &DPDrive::showSettingsDialog);
  // This plugin is significantly different from navg2 dp plugin.
  // In navg2 all the dp math and i/o feeds was done in the gui. This has
  // several disadvantages:
  // 1. Only one instance of the gui can use dp control
  // 2. It is essentially unusable over satellite high latency links, which
  //    would make impossible having remote navigator capabilities
  // 3. Philosophically, all the nav computations should reside in navest
  //    or a ros plugin of some sort and the gui is only a tool to interact with
  //    that
  // So, this new plugin will just be an interface to the underlying nav engine
  // to address all the shortcomings listed above
  reference_speed = 0;

  dp_update_interval = 1.0;
  dp_timer = new QTimer; // delete in destructor
  dp_timer->setInterval((int)(dp_update_interval * 1000.0));
  disable_init_timer = new QTimer; // delete in destructor
  disable_init_timer->setInterval((int)(20000.0));
  dp_type = NO_DP;
  last_ship_local_pos = QPointF(0, 0);
  current_ship_local_pos = QPointF(0, 0);
  ui->start_dp_button->setEnabled(false);
  ui->no_dp_radio->setChecked(true);
  m_markers.reserve(2);
  lastRxStatusDT = QDateTime::currentDateTimeUtc();

  m_range = ui->rangeSpn->value();
  m_bearing = ui->bearingSpn->value();
}

DPDrive::~DPDrive()
{
  dp_timer->stop();
  disable_init_timer->stop();
  delete dp_timer;
  delete disable_init_timer;
  delete dpSocket;
}

void
DPDrive::setMapView(QPointer<dslmap::MapView> view)
{
  // parent view
  m_view = view;

  // new cast view contained in this object
  nview = qobject_cast<navg::NavGMapView*>(view);
  if (!nview) {
    qCWarning(plugin_dp_drive) << "nview not valid";
    return;
  }
  if (nview) {
    connect(nview,
            &navg::NavGMapView::cursorDropped,
            this,
            &DPDrive::handleSceneMousePress);
  } else {
    qCWarning(plugin_dp_drive) << "unknown nview";
  }

  // connect(nview, &navg::NavGMapView::mouseDoubleClickedOnScene, this,
  // &DPDrive::handleSceneMousePress );
}

void
DPDrive::connectPlugin(const QObject* plugin, const QJsonObject& metadata)
{
  if (!plugin) {
    qCCritical(plugin_dp_drive, "plugin not created, returning");
    return;
  }

  const auto name =
    metadata.value("MetaData").toObject().value("name").toString();
  if (name.isEmpty()) {
    return;
  }
  if (name == "navest") {
    connect(plugin,
            SIGNAL(messageReceived(NavestMessageType, QVariant)),
            this,
            SLOT(updateFromNavestMessage(NavestMessageType, QVariant)));
    connectUI();
  }
}
void
DPDrive::connectUI()
{

  connect(dp_timer, &QTimer::timeout, this, &DPDrive::dpIntervalTimeout);
  connect(
    disable_init_timer, &QTimer::timeout, this, &DPDrive::dpDisableInitTimeout);
  connect(ui->northButton,
          &QPushButton::pressed,
          this,
          &DPDrive::handleDirectionButton);
  connect(ui->southButton,
          &QPushButton::pressed,
          this,
          &DPDrive::handleDirectionButton);
  connect(ui->eastButton,
          &QPushButton::pressed,
          this,
          &DPDrive::handleDirectionButton);
  connect(ui->westButton,
          &QPushButton::pressed,
          this,
          &DPDrive::handleDirectionButton);

  ui->range_change_combo->addItem("1.0", QVariant(1.0));
  ui->range_change_combo->addItem("10.0", QVariant(10.0));
  ui->range_change_combo->addItem("100.0", QVariant(100.0));
  ui->range_change_combo->addItem("1000.0", QVariant(1000.0));
  ui->speed_dial_spin->setButtonSymbols(QDoubleSpinBox::PlusMinus);
  ui->speed_dial_spin->setStyleSheet(
    "QDoubleSpinBox {height: 30px;}"
    "QDoubleSpinBox::up-button {width: 20px; height: 15px;}"
    "QDoubleSpinBox::down-button {width: 20px; height: 15px;}");
  connect(ui->rangeSpn,
          static_cast<void (QDoubleSpinBox::*)(double)>(
            &QDoubleSpinBox::valueChanged),
          this,
          &DPDrive::handleRangeSpn);
  connect(ui->bearingSpn,
          static_cast<void (QDoubleSpinBox::*)(double)>(
            &QDoubleSpinBox::valueChanged),
          this,
          &DPDrive::handleBearingSpn);

  connect(ui->speed_dial_spin,
          static_cast<void (QDoubleSpinBox::*)(double)>(
            &QDoubleSpinBox::valueChanged),
          this,
          &DPDrive::handleSpeedDialSpin);
  connect(ui->enable_dp_check,
          &QCheckBox::stateChanged,
          this,
          &DPDrive::handleEnableDPCheck);
  connect(ui->start_dp_button,
          &QPushButton::clicked,
          this,
          &DPDrive::handleStartDPButton);
  connect(ui->goalRangeBearingBtn,
          &QPushButton::clicked,
          this,
          &DPDrive::handleGoalRangeBearingBtn);
  connect(ui->goal_ref_button,
          &QPushButton::clicked,
          this,
          &DPDrive::handleGoalRefButton);
  connect(ui->goal_cursor_button,
          &QPushButton::clicked,
          this,
          &DPDrive::handleGoalCursorButton);
  connect(ui->no_dp_radio,
          &QRadioButton::toggled,
          this,
          &DPDrive::handleDPSelectionRadios);
  connect(ui->dgps_radio,
          &QRadioButton::toggled,
          this,
          &DPDrive::handleDPSelectionRadios);

  connect(ui->goal_color_button,
          &dslmap::ColorPickButton::colorChanged,
          [=](const QColor color) { emit goalColorChanged(color); });
  connect(ui->ref_color_button,
          &dslmap::ColorPickButton::colorChanged,
          [=](const QColor color) { emit refColorChanged(color); });

  connect(this, &DPDrive::goalColorChanged, this, &DPDrive::setGoalColor);
  connect(this, &DPDrive::refColorChanged, this, &DPDrive::setRefColor);

  // ui->no_dp_radio->setStyleSheet("QRadioButton { background-color:
  // transparent; color: black; }"); ui->dgps_radio->setStyleSheet("QRadioButton
  // { background-color: lightGreen; color: black; }");
}

void
DPDrive::loadSettings(QSettings* settings)
{

  if (settings == nullptr) {
    qCWarning(plugin_dp_drive) << "Settings are nullptr";
    return;
  }

  QString address;
  address = settings->value("DP_host_address", "127.0.0.1").toString();
  dpHostAddress.setAddress(address);

  auto dpHostInSocketNumber_val = settings->value("port_from_DP");
  auto in_socket_ok = false;
  if (dpHostInSocketNumber_val.isValid()) {
    const auto valid_dpHostInSocketNumber =
      dpHostInSocketNumber_val.toInt(&in_socket_ok);
    if (in_socket_ok) {
      dpHostInSocketNumber = valid_dpHostInSocketNumber;
    }
  }

  auto dpHostOutSocketNumber_val = settings->value("port_to_DP");
  auto out_socket_ok = false;
  if (dpHostOutSocketNumber_val.isValid()) {
    const auto valid_dpHostOutSocketNumber =
      dpHostOutSocketNumber_val.toInt(&out_socket_ok);
    if (out_socket_ok) {
      dpHostOutSocketNumber = valid_dpHostOutSocketNumber;
    }
  }

  dpEnabled = settings->value("DP_enabled", false).toBool();

  dpSocket = new QUdpSocket(this); // Delete in destructor
  if (!dpSocket->bind(dpHostInSocketNumber,
                      QUdpSocket::ShareAddress |
                        QUdpSocket::ReuseAddressHint)) {
    qDebug(plugin_dp_drive) << "could not bind to socket";
  } else {
    QObject::connect(dpSocket,
                     SIGNAL(readyRead()),
                     this,
                     SLOT(dpSocketDataPending()),
                     Qt::UniqueConnection);
  }
  // Start the DP timer to send user requests to the nav engine
  dp_timer->start();

  return;
}

void
DPDrive::saveSettings(QSettings& settings)
{
  settings.setValue("DP_host_address", dpHostAddress.toString());
  settings.setValue("port_from_DP", dpHostInSocketNumber);
  settings.setValue("port_to_DP", dpHostOutSocketNumber);
  settings.setValue("DP_enabled", dpEnabled);
  return;
}
void
DPDrive::showSettingsDialog()
{

  QScopedPointer<DPDriveSettings> dialog(new DPDriveSettings);
  auto host_addr = QHostAddress{ dpHostAddress };
  if (dpHostAddress.isNull()) {
    qWarning(plugin_dp_drive)
      << "DP host address "
      << "'" << dpHostAddress << "' is not a valid ip address";
    return;
  }

  dialog->setDPHostAddress(host_addr);
  dialog->setDPHostInSocketNumber(dpHostInSocketNumber);
  dialog->setDPHostOutSocketNumber(dpHostOutSocketNumber);

  dialog->setModal(true);
  if (!dialog->exec()) {
    return;
  }

  host_addr = dialog->getDPHostAddress();
  dpHostInSocketNumber = dialog->getDPHostInSocketNumber();
  dpHostOutSocketNumber = dialog->getDPHostOutSocketNumber();
}

QList<QAction*>
DPDrive::pluginMenuActions()
{
  return { m_action_show_settings };
}

void
DPDrive::updateFromNavestMessage(navg::NavestMessageType type, QVariant message)
{

  if (type == navg::NavestMessageType::VFR) {

    int vehicle_id = message.value<navg::NavestMessageVfr>().vehicle_number;

    // vehicle 2 is ship
    if (vehicle_id == 2) {

      last_ship_local_pos.setX(current_ship_local_pos.x());
      last_ship_local_pos.setY(current_ship_local_pos.y());
    } else {
      return;
    }
  }
}

void
DPDrive::handleDirectionButton()
{

  // can handle request based on sender. all do similar things
  // or use additional info passed from slot
  //qCDebug(plugin_dp_drive) << sender();

  //double step_index = ui->range_change_combo->currentIndex();
  double step_size = ui->range_change_combo->currentData().toDouble();

  //qCDebug(plugin_dp_drive) << "step index is " << step_index;
  //qCDebug(plugin_dp_drive) << "step size is " << step_size;

  // Send message to nav engine telling it to increment goal position
  // The message is in xy step and the nav engine will compute
  // a new lat lon goal based on that

  QPushButton* selected_button = (QPushButton*)sender();
  if (selected_button == ui->northButton) {
    incrementGoal(0, step_size);
  } else if (selected_button == ui->southButton) {
    incrementGoal(0, -step_size);
  } else if (selected_button == ui->eastButton) {
    incrementGoal(step_size, 0);
  } else if (selected_button == ui->westButton) {
    incrementGoal(-step_size, 0);
  }
}

void
DPDrive::handleSpeedDialSpin(double value)
{
  changeSpeed(value);
}

void
DPDrive::handleBearingSpn(double value)
{
  m_bearing = value;
}

void
DPDrive::handleRangeSpn(double value)
{
  m_range = value;
}

void
DPDrive::handleRangeComboChoice()
{}

void
DPDrive::handleSceneMousePress(QPointF cursor)
{

  auto map_scene = m_view->mapScene();

  if (!map_scene) {

    return;
  }
  double lon = cursor.x();
  double lat = cursor.y();
  auto proj = map_scene->projection();
  if (!proj){
    return;
   
  }
  // get proj from scene first
  proj->transformToLonLat(1, &lon, &lat);

  qDebug(plugin_dp_drive) << "Target lat: " << lat << " lon: " << lon;

  cursor_lon = lon;
  cursor_lat = lat;
}

void
DPDrive::changeSpeed(double speed)
{

  if (speed > MAX_REF_SPEED) {
    reference_speed = MAX_REF_SPEED;
  } else if (speed < 0.1) {
    reference_speed = 0.0;
  } else {
    reference_speed = speed;
  }
}

void
DPDrive::handleEnableDPCheck(bool checked)
{

  (checked == true) ? ui->start_dp_button->setEnabled(true)
                    : ui->start_dp_button->setEnabled(false);
  if (checked == true) {
    disable_init_timer->start(20000);
  }
}

void
DPDrive::handleStartDPButton()
{
  if (ui->enable_dp_check->isChecked()) {
    // send a message to init dp
    initializeDp();

    // Create layer must be called only after a projection already exists. here
    // it's very likely that the projection already exists. Maybe we should add
    // a signal emitted by the core parts that is emitted when a projection is
    // made available?
    createLayer();

    // This is just a test marker
    // auto marker = m_layer->addLonLatPoint(-130.1, 45.9, "GOAL");
    // m_markers[0] = marker;
    // auto marker2 = m_layer->addLonLatPoint(-130.1, 46.0, "REF");
    // m_markers[1] = marker2;
    // m_layer->removePoint(m_markers[1]);
    // auto marker3 = m_layer->addLonLatPoint(-130.1, 46.1, "REF");
    // m_markers[1] = marker3;

  } else
    return;
}

void
DPDrive::handleGoalRefButton()
{
  gotoGoalRef();
}

void
DPDrive::handleGoalCursorButton()
{
  // Verify that cursor pos is not 0
  gotoLonLat(cursor_lon, cursor_lat);
}

void
DPDrive::handleGoalRangeBearingBtn()
{
  gotoRangeBearing(ui->rangeSpn->value(), ui->bearingSpn->value());
  ui->rangeSpn->setValue(0.0);
  ui->bearingSpn->setValue(0.0);

}

void
DPDrive::handleDPSelectionRadios(int state)
{
  QRadioButton* selection = (QRadioButton*)sender();

  if (selection == ui->dgps_radio) {
    setDPType(FAUX_GPS);
  }

  else if (selection == ui->no_dp_radio) {
    setDPType(NO_DP);
  }
}

void
DPDrive::setGoalColor(QColor color)
{
  /*   if (m_layer != nullptr) {
      qDebug(plugin_dp_drive)<<"Found mlayer, changing color";
      m_markers[MARKER_GOAL] = m_layer->setOutlineColor(color);
      m_markers[MARKER_GOAL] = m_layer->setFillColor(color);

    } */
}

void
DPDrive::setRefColor(QColor color)
{}

void
DPDrive::setDPType(DP_TYPE type)
{
  dp_type = type;
}

void
DPDrive::dpDisableInitTimeout()
{
  if (ui->enable_dp_check->isChecked()) {
    ui->enable_dp_check->setChecked(false);
    disable_init_timer->stop();
  }
}

void
DPDrive::createLayer()
{
  dslmap::MapScene* scene = qobject_cast<dslmap::MapScene*>(m_view->mapScene());
  Q_ASSERT(scene != nullptr);

  if (m_layer != nullptr) {
    return; // Added to bypass temorarily layer management
    qCWarning(plugin_dp_drive, "Already had old layer; removing");
    auto layer = scene->removeLayer(m_layer->id());
    delete layer;
  }

  m_layer = new dslmap::SymbolLayer();

  // auto scene_projection = scene->projection();
  // m_layer->setProjection(scene_projection);

  auto target = scene->addLayer(m_layer);
  if (target.isNull()) {
    qCCritical(plugin_dp_drive, "Unable to add layer to scene.");
    return;
  }
  qCDebug(plugin_dp_drive, "Added layer to scene.");

  m_layer->setName("Ship DP Drive Layer");

  int marker_size = 15;
  m_layer->setShape(dslmap::ShapeMarkerSymbol::Shape::Circle, marker_size);
  m_layer->setScaleInvariant(true);

  auto symbol_ptr = m_layer->symbolPtr();

  symbol_ptr->setOutlineColor(Qt::red);
  symbol_ptr->setFillColor(Qt::red);
  symbol_ptr->setFillStyle(Qt::SolidPattern); // defaults to Qt::NoBrush
  symbol_ptr->setLabelColor(Qt::white);

  m_layer->setLabelVisible(true);
  m_layer->setVisible(true);
}

bool
DPDrive::hasLayer()
{
  return !(m_layer == nullptr);
}

////////// THE FOLLOWING ARE INTERFACE IMPLEMENTATIONS

void
DPDrive::dpSocketDataPending()
{
  QUdpSocket* theSocket = (QUdpSocket*)sender();
  while (theSocket->hasPendingDatagrams()) {

    QByteArray buffer(theSocket->pendingDatagramSize(), 0);
    theSocket->readDatagram(buffer.data(), buffer.size());
    //qDebug(plugin_dp_drive) << "Bytes read: " << bytesRead;

    // Here receive the status updates from the nav engine and update user
    // display status:
    // 1. whether DP is actually enabled or disabled
    // 2. error remaining to position goal
    // 3. Active mode: 0 - move at range and bearing, 1 - move to position
    // 4. actual reference speed
    // 5. actual lat lon goal
    // 6. actual lat lon reference (this moves smoothly towards the goal at the
    // reference speed) Update plugin controls color based on status match with
    // request
    // 7. Ship speed from VTG message
    // 8. Ship COG from VTG message

    int l_enabled;
    double l_x_err;
    double l_y_err;
    int l_active_mode;
    double l_ref_speed;
    double l_goal_lon;
    double l_goal_lat;
    double l_ref_lon;
    double l_ref_lat;
    double l_curr_ship_speed;
    double l_curr_ship_cog;
    //double l_ship_speed;
    //double l_ship_cog;
    double l_actual_time_goal_min;
    double l_actual_time_goal_sec;
    double l_actual_dist_goal_meters;
    double l_anti_ref_lat;
    double l_anti_ref_lon;
    
    int args =
      sscanf(buffer.data(),
             "DPS %d %lf %lf %d %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf",
             &l_enabled,
             &l_x_err,
             &l_y_err,
             &l_active_mode,
             &l_ref_speed,
             &l_goal_lon,
             &l_goal_lat,
             &l_ref_lat,
             &l_ref_lon,
             &l_curr_ship_speed,
             &l_curr_ship_cog,
             &l_actual_time_goal_min,
             &l_actual_time_goal_sec,
             &l_actual_dist_goal_meters,
             &l_anti_ref_lat,
             &l_anti_ref_lon);
    //qDebug(plugin_dp_drive) << "UI update: " << buffer.data();
    if (args < 13) {
      qDebug(plugin_dp_drive) << "DPS parsing error";
      return;
    } else {
      // qDebug(plugin_dp_drive) << "args >= 9";
      //  Update the last rx message
      lastRxStatusDT = QDateTime::currentDateTimeUtc();
      if (l_enabled) {
        // qDebug(plugin_dp_drive) << "l_enabled";
        ui->no_dp_radio->setStyleSheet(
          "QRadioButton { background-color: transparent; color: black; }");
        ui->dgps_radio->setStyleSheet(
          "QRadioButton { background-color: lightGreen; color: black; }");
      } else {
        // qDebug(plugin_dp_drive) << "l_enabled == false";
        ui->no_dp_radio->setStyleSheet(
          "QRadioButton { background-color: lightGreen; color: black; }");
        ui->dgps_radio->setStyleSheet(
          "QRadioButton { background-color: transparent; color: black; }");
      }
      
      ui->exLbl->setStyleSheet("QLabel { color: white} ");
      ui->eyLbl->setStyleSheet("QLabel { color: white} ");
      ui->exLbl->setText("ex: " + QString::number(l_x_err));
      ui->eyLbl->setText("ey: " + QString::number(l_y_err));
      
      if (l_x_err >= 20 || l_x_err <= -20) {
	      //qDebug(plugin_dp_drive) << "HIGH EX ERROR, REINIT DP";
      	      ui->exLbl->setStyleSheet("QLabel { color: red} ");
      }
      if (l_y_err >= 20 || l_y_err <= -20) {
	      //qDebug(plugin_dp_drive) << "HIGH EY ERROR, REINIT DP";
	      ui->eyLbl->setStyleSheet("QLabel { color: red} ");
      }
      
      ui->cogLbl->setText("cog: " + QString::number(l_curr_ship_cog) + " deg true");
      ui->sogLbl->setText("sog: " + QString::number(l_curr_ship_speed) + " knots");
      if (!std::isnan(l_actual_time_goal_min) ||
          !std::isnan(l_actual_time_goal_sec)) {
        ui->ttgLbl->setText(
          "TTG: " + QString::number(l_actual_time_goal_min) + " min " +
          QString::number(l_actual_time_goal_sec) + " sec");
        ui->dtgLbl->setText("DTG: " + QString::number(l_actual_dist_goal_meters) + " meters ");
      } else {
        ui->ttgLbl->setText("TTG: none");
      }

      // Set things to zero when distance to goal is less than hypot of x and y error
      // 2024-07-01 IV: Adding 3m to account for accuracy of modern GPS systems
      if (l_actual_dist_goal_meters < (std::hypot(l_x_err, l_y_err)+3)) {
        ui->speed_dial_spin->setValue(0.0);
      }

      if (m_layer != nullptr) {
        // qDebug(plugin_dp_drive)<<"m_layer != nullptr";
        if (m_markers[MARKER_GOAL] == nullptr)
          m_markers[MARKER_GOAL] =
            m_layer->addLonLatPoint(l_goal_lon, l_goal_lat, "DP GOAL_INIT");
        if (m_markers[MARKER_REF] == nullptr)
          m_markers[MARKER_REF] =
            m_layer->addLonLatPoint(l_goal_lon, l_goal_lat, "DP REF_INIT");
        /*
	if (m_markers[MARKER_ANTIREF] == nullptr)
          m_markers[MARKER_ANTIREF] = m_layer->addLonLatPoint(
            l_anti_ref_lon, l_anti_ref_lat, "ANTI_REF_INIT");
	*/
        if (m_markers[MARKER_GOAL] != nullptr) {
          m_layer->removePoint(m_markers[MARKER_GOAL]);
          auto marker = m_layer->addLonLatPoint(l_goal_lon, l_goal_lat, "DP GOAL");
          m_markers[MARKER_GOAL] = marker;
          // qDebug(plugin_dp_drive) << "Added GOAL Marker to m_markers";
        }
        if (m_markers[MARKER_REF] != nullptr) {
          m_layer->removePoint(m_markers[MARKER_REF]);
          auto marker = m_layer->addLonLatPoint(l_ref_lon, l_ref_lat, "DP REF");
          m_markers[MARKER_REF] = marker;
          // qDebug(plugin_dp_drive) << "Added REF Marker to m_markers";
        }

        // if (a1 = b1 and a2 = b2) throw an error
        // get dist between ret and goal:
        double goal_bearing = dslmap::calculateBearing(
          QPointF(l_ref_lon, l_ref_lat), QPointF(l_goal_lon, l_goal_lat));
        qDebug () << "ref lon is " << l_ref_lon << "ref lat is " << l_ref_lat 
        << "goal lon is " << l_goal_lon << "goal lat is " << l_goal_lat;
        if (goal_bearing > 360 || goal_bearing < 0) {
          // do nothing for now
        } else
          ui->btgLbl->setText(
            "BTG: " + QString::number(goal_bearing, 'f', 2) + "º");

        /*
        if (m_markers[MARKER_ANTIREF] != nullptr) {
          m_layer->removePoint(m_markers[MARKER_ANTIREF]);
          auto marker =
            m_layer->addLonLatPoint(l_anti_ref_lon, l_anti_ref_lat, "DP ANTI REF");
          m_markers[MARKER_ANTIREF] = marker;
        }
	*/
      } else {
        //qDebug(plugin_dp_drive) << "m_layer == nullptr";
      }
    }
  }
}

void
DPDrive::dpIntervalTimeout()
{

  // make the synchronous user request messages for the nav engine and
  // send it to it. Things to put in it:
  // 1. DP enabled/disabled flag
  // 2. Requested ref speed
  // 3. Requested Mode: 0 - move at range and bearing, 1 - move to position
  // 4. Target range and bearing
  // 5. Target position (lon, lat)
  // The nav engine should keep track of the last request message received and
  // automatically disable DP output if it stops receiving messages for more
  // than a certain amount of time, as that would mean that the user ability
  // to control the dp system is inhibited
  QString rdp_os;

  // Update staleness display + only write to socket if actively receiving DGPS updates
  QDateTime lastTimer = QDateTime::currentDateTimeUtc();
  if (lastRxStatusDT.secsTo(lastTimer) > 10) {
    qDebug(plugin_dp_drive) << "STALE DP INPUT, NOT PUBLISHING DP UPDATES";
    ui->no_dp_radio->setStyleSheet(
      "QRadioButton { background-color: yellow; color: black; }");
    ui->dgps_radio->setStyleSheet(
      "QRadioButton { background-color: yellow; color: black; }");
  }
  else {
  //qDebug(plugin_dp_drive) << "Last status rxd: "<< lastRxStatusDT.secsTo(lastTimer);
  rdp_os = QString("DP RDP ") + QString::number((dp_type == NO_DP) ? 0 : 1) +
           QString("\r\n");

  //qDebug(plugin_dp_drive) << "DP request: " << rdp_os;

  dpSocket->writeDatagram(
    rdp_os.toLocal8Bit(), dpHostAddress, dpHostOutSocketNumber);

  QString rdp_refspeed;
  rdp_refspeed = QString("DP REFSPEED ") + QString::number(reference_speed) +
                 QString("\r\n");
  //qDebug(plugin_dp_drive) << "DP request: " << rdp_refspeed;
  dpSocket->writeDatagram(
    rdp_refspeed.toLocal8Bit(), dpHostAddress, dpHostOutSocketNumber);
  }
}

void
DPDrive::incrementGoal(double x_amount, double y_amount)
{
  QString xyGoalChange =
    "DP XYS " + QString::number(x_amount) + " " + QString::number(y_amount);
  dpSocket->writeDatagram(
    xyGoalChange.toLocal8Bit(), dpHostAddress, dpHostOutSocketNumber);
}

void
DPDrive::gotoGoalRef()
{
  QString goaltoRef = "DP GTR ";
  dpSocket->writeDatagram(
    goaltoRef.toLocal8Bit(), dpHostAddress, dpHostOutSocketNumber);
}

void
DPDrive::gotoRangeBearing(double range, double bearing)
{
  QString rangeBearing =
    "DP RBG " + QString::number(m_range) + " " + QString::number(m_bearing);
  dpSocket->writeDatagram(
    rangeBearing.toLocal8Bit(), dpHostAddress, dpHostOutSocketNumber);
}

void
DPDrive::gotoLonLat(double lon, double lat)
{
  QString llGoal = "DP GLL " + QString::number(lon, 'f', 5) + " " +
                   QString::number(lat, 'f', 5);
  dpSocket->writeDatagram(
    llGoal.toLocal8Bit(), dpHostAddress, dpHostOutSocketNumber);
}

void
DPDrive::initializeDp(void)
{

  // Send message to nav engine to initialize DP. Actions to take there:
  // 1. Set xy projection origin to current ship position
  // 2. Set reference to current ship position
  // 3. Set goal to current reference

  QString dpInit = "DP INI";
  dpSocket->writeDatagram(
    dpInit.toLocal8Bit(), dpHostAddress, dpHostOutSocketNumber);
}

} // dp_drive
} // plugins
} // navg
