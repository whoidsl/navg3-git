/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 12/7/18.
//

#ifndef NAVG_GRADIENTWIDGET_H
#define NAVG_GRADIENTWIDGET_H

#include <QWidget>
#include <memory>

namespace dslmap {
class GdalColorGradient;
}

namespace navg {
namespace plugins {
namespace gradient {

class GradientWidget : public QWidget
{
  Q_OBJECT

public:
  enum TextColor
  {
    Auto = 0,
    Black,
    White
  };
  Q_ENUM(TextColor)

  GradientWidget(QWidget* parent = nullptr);
  ~GradientWidget();

  void setGradient(std::shared_ptr<dslmap::GdalColorGradient> grad);

  void setDepthSpacing(int meters);
  int depthSpacing();

  void setDepthLabelRatio(int ratio);
  int depthLabelRatio();

  void setTextColor(TextColor color);
  TextColor textColor();

protected:
  void paintEvent(QPaintEvent* e) override;
  void mouseMoveEvent(QMouseEvent* event) override;
  void mouseDoubleClickEvent(QMouseEvent* event) override;

private:
  void setPenColor(QPainter* p);
  std::shared_ptr<dslmap::GdalColorGradient> m_gradient;
  int m_cursor_y;
  int m_depth_spacing;
  int m_depth_label_ratio;
  TextColor m_textcolor;
};

} // namespace gradient
} // namespace plugins
} // namespace navg

#endif // NAVG_GRADIENTWIDGET_H
