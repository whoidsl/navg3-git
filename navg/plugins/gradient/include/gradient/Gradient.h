/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 12/6/18.
//

#ifndef NAVG_GRADIENT_H
#define NAVG_GRADIENT_H

#include <dslmap/GdalColorGradient.h>
#include <navg/CorePluginInterface.h>

#include <QLoggingCategory>
#include <QMenu>
#include <QObject>
#include <QWidget>

#include <memory>

namespace dslmap {
class GdalColorGradientPropertiesWidget;
}

namespace navg {
namespace plugins {
namespace gradient {

class GradientPropertiesWidget;

namespace Ui {
class Gradient;
}

class Gradient : public QWidget, public CorePluginInterface
{
  Q_OBJECT
  Q_INTERFACES(navg::CorePluginInterface)
  Q_PLUGIN_METADATA(IID CorePluginInterface_iid FILE "Gradient.json")

signals:
  void gradientUpdated();

public:
  explicit Gradient(QWidget* parent = nullptr);
  ~Gradient() override;

  //
  // CorePluginInterface Implementation
  //
  void loadSettings(QSettings* settings) override;
  void saveSettings(QSettings& settings) override;

  // This plugin connects to acomms plugin.
  void connectPlugin(const QObject* plugin,
                     const QJsonObject& metadata) override;
  void setMapView(QPointer<dslmap::MapView> view) override;
  QList<QAction*> pluginMenuActions() override;
  QList<QAction*> pluginToolbarActions() override { return {}; }

  void setGradient(std::shared_ptr<dslmap::GdalColorGradient> grad);

protected slots:

  /// \brief Update the gradient, maybe due to new bounds, etc
  ///
  /// prompts a redraw of the widget
  void updateGradient();
  void customContextMenuEvent(QPoint pos);

private:
  void setupMenu();
  std::unique_ptr<Ui::Gradient> ui;
  std::shared_ptr<dslmap::GdalColorGradient> m_gradient;
  QPointer<dslmap::MapView> m_view;
  QMenu* m_menu;
  GradientPropertiesWidget* w_display;
  dslmap::GdalColorGradientPropertiesWidget* w_gradient;
};

Q_DECLARE_LOGGING_CATEGORY(plugin_gradient)
} // gradient
} // plugins
} // navg

#endif // NAVG_GRADIENT_H
