/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 12/7/18.
//

#include "GradientWidget.h"
#include <QMouseEvent>
#include <QPainter>
#include <cmath>
#include <dslmap/GdalColorGradient.h>

namespace navg {
namespace plugins {
namespace gradient {

GradientWidget::GradientWidget(QWidget* parent)
  : QWidget(parent)
  , m_cursor_y(0)
  , m_depth_spacing(25)
  , m_depth_label_ratio(1)
  , m_textcolor(GradientWidget::TextColor::Auto)
{
}

GradientWidget::~GradientWidget()
{
}

void
GradientWidget::setGradient(std::shared_ptr<dslmap::GdalColorGradient> grad)
{
  if (!grad) {
    return;
  }
  m_gradient = grad;
  update();
  connect(m_gradient.get(), &dslmap::GdalColorGradient::gdalColorTableChanged,
          [=]() { update(); });
}

void
GradientWidget::paintEvent(QPaintEvent*)
{
  if (!m_gradient) {
    return;
  }
  QPainter painter(this);
  auto w = width();
  auto h = height();
  QLinearGradient linearGrad(QPointF(2 * w / 5, h), QPointF(2 * w / 5, 0));
  linearGrad.setStops(m_gradient->stops());

  QRect rect_linear(2 * w / 5, 0, w / 5, h);
  painter.fillRect(rect_linear, linearGrad);

  auto min = m_gradient->min();
  auto max = m_gradient->max();
  auto count = 0;
  for (qreal d = 0; d > min; d -= m_depth_spacing) {
    if (d < max) {
      auto hd = static_cast<int>(h - h * (d - min) / (max - min));
      auto color = m_gradient->colorAt(d);
      painter.setPen(color);
      painter.drawLine(QPoint{ 0, hd }, QPoint{ w / 2, hd });
      if (count % m_depth_label_ratio == 0) {
        setPenColor(&painter);
        painter.drawText(QPoint{ w / 5, hd }, QString::number(d));
      }
    }
    count += 1;
  }

  qreal depth = min + (max - min) * (h - m_cursor_y) / h;
  auto color = m_gradient->colorAt(depth);
  QPen depth_pen{ color };
  depth_pen.setWidth(3);
  painter.setPen(depth_pen);
  painter.drawLine(QPoint{ 0, m_cursor_y }, QPoint{ w, m_cursor_y });
  QPoint depth_pos{ 3 * w / 5, m_cursor_y };
  setPenColor(&painter);
  painter.drawText(depth_pos, QString::number(round(depth)));
}

void
GradientWidget::mouseMoveEvent(QMouseEvent* e)
{
  QPainter painter(this);
  auto w = width();
  auto h = height();
  QRect rect_linear(2 * w / 5, 0, w / 5, h);
  auto pos = e->pos();
  if (!rect_linear.contains(pos) || !m_gradient) {
    return;
  }
  m_cursor_y = pos.y();
  update();
}

void
GradientWidget::mouseDoubleClickEvent(QMouseEvent* e)
{
  mouseMoveEvent(e);
}

void
GradientWidget::setDepthSpacing(int meters)
{
  if (meters < 5) {
    return;
  }
  m_depth_spacing = meters;
  update();
}

int
GradientWidget::depthSpacing()
{
  return m_depth_spacing;
}

void
GradientWidget::setDepthLabelRatio(int ratio)
{
  if (ratio < 1) {
    return;
  }
  m_depth_label_ratio = ratio;
  update();
}

int
GradientWidget::depthLabelRatio()
{
  return m_depth_label_ratio;
}

void
GradientWidget::setTextColor(GradientWidget::TextColor color)
{
  m_textcolor = color;
  update();
}

GradientWidget::TextColor
GradientWidget::textColor()
{
  return m_textcolor;
}

void
GradientWidget::setPenColor(QPainter* p)
{
  switch (m_textcolor) {
    case TextColor::Black:
      p->setPen(Qt::black);
      break;
    case TextColor::White:
      p->setPen(Qt::white);
      break;
    default:
      break;
  }
}

} // namespace
}
}
