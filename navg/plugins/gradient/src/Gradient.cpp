/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro 12/6/18.
//

#include "Gradient.h"
#include "GradientPropertiesWidget.h"
#include "ui_Gradient.h"

#include <dslmap/GdalColorGradientPropertiesWidget.h>
#include <dslmap/MapScene.h>
#include <dslmap/MapView.h>

#include <QAction>
#include <QMenu>
#include <QMessageBox>
#include <QSettings>

namespace navg {
namespace plugins {
namespace gradient {

Q_LOGGING_CATEGORY(plugin_gradient, "navg.plugins.gradient")

Gradient::Gradient(QWidget* parent)
  : QWidget(parent)
  , ui(std::make_unique<Ui::Gradient>())
  , m_gradient(nullptr)
{
  ui->setupUi(this);
  w_display = new GradientPropertiesWidget();
  w_display->setWindowTitle("Edit Gradient Plugin Display");
  w_display->hide();
  w_gradient = new dslmap::GdalColorGradientPropertiesWidget();
  w_gradient->setWindowTitle("Edit Scene Gradient");
  w_gradient->hide();

  setupMenu();
  this->setContextMenuPolicy(Qt::CustomContextMenu);
  //  ui->gradWidget->setContextMenuPolicy(Qt::CustomContextMenu);
  connect(this, SIGNAL(customContextMenuRequested(QPoint)), this,
          SLOT(customContextMenuEvent(QPoint)));
}

Gradient::~Gradient() = default;

void
Gradient::setMapView(QPointer<dslmap::MapView> view)
{
  if (!view) {
    return;
  }
  m_view = view;
  auto scene = qobject_cast<dslmap::MapScene*>(m_view->scene());
  if (scene) {
    auto grad = scene->gradient();
    if (grad) {
      setGradient(grad);
    }
    connect(scene, &dslmap::MapScene::gradientChanged, [=]() {
      auto scene = qobject_cast<dslmap::MapScene*>(m_view->scene());
      this->setGradient(scene->gradient());
    });
  }
}

void
Gradient::connectPlugin(const QObject*, const QJsonObject&)
{
}

void
Gradient::loadSettings(QSettings* settings)
{
  if (settings == nullptr) {
    return;
  }
  ui->gradWidget->setDepthSpacing(settings->value("depth_spacing").toInt());
  ui->gradWidget->setDepthLabelRatio(
    settings->value("depth_label_ratio").toInt());
  ui->gradWidget->setTextColor(static_cast<GradientWidget::TextColor>(
    settings->value("text_color").toInt()));
}

void
Gradient::saveSettings(QSettings& settings)
{
  settings.setValue("depth_spacing", ui->gradWidget->depthSpacing());
  settings.setValue("depth_label_ratio", ui->gradWidget->depthLabelRatio());
  settings.setValue("text_color",
                    static_cast<int>(ui->gradWidget->textColor()));
}

QList<QAction*>
Gradient::pluginMenuActions()
{
  return {};
}

void
Gradient::setGradient(std::shared_ptr<dslmap::GdalColorGradient> grad)
{
  if (!grad) {
    return;
  }
  m_gradient = grad;
  ui->gradWidget->setGradient(m_gradient);
  updateGradient();
}

void
Gradient::updateGradient()
{
}

void
Gradient::customContextMenuEvent(QPoint pos)
{
  m_menu->popup(ui->gradWidget->mapToGlobal(pos));
}

void
Gradient::setupMenu()
{
  qDebug() << "Created a menu";
  QAction* displayAct = new QAction(QString{ "Edit Display" }, this);
  displayAct->setToolTip("Edit this plugin's display");
  QAction* gradAct = new QAction(QString{ "Edit Gradient" }, this);
  gradAct->setToolTip("Edit the scene gradient");
  connect(displayAct, &QAction::triggered, [=]() {
    w_display->setGradientWidget(ui->gradWidget);
    w_display->show();
    w_display->raise();
  });
  connect(gradAct, &QAction::triggered, [=]() {
    if (!m_gradient) {
      QMessageBox::information(this, "No Scene Gradient",
                               "Create a Contour/Bathy Layer first.");
      return;
    }
    w_gradient->setGradient(m_gradient);
    w_gradient->show();
    w_gradient->raise();
  });
  m_menu = new QMenu(this);
  m_menu->addAction(displayAct);
  m_menu->addAction(gradAct);
  //  connect(gradAct, &QAction::triggered, this,
  //  &MapLayerEditWidget::do_upAct);
}

} // Gradient
} // plugins
} // navg
