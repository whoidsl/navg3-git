/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 12/11/18.
//
#include "GradientPropertiesWidget.h"
#include "ui_GradientPropertiesWidget.h"

#include "GradientWidget.h"
#include <QMetaEnum>

namespace navg {
namespace plugins {
namespace gradient {

GradientPropertiesWidget::GradientPropertiesWidget(QWidget* parent)
  : QWidget(parent, Qt::Window)
  , ui(new Ui::GradientPropertiesWidget)
{
  ui->setupUi(this);
  auto metaenum = QMetaEnum::fromType<GradientWidget::TextColor>();
  for (auto i = 0; i < metaenum.keyCount(); ++i) {
    ui->colorBox->addItem(metaenum.key(i), i);
  }
}

GradientPropertiesWidget::~GradientPropertiesWidget()
{
  delete ui;
}

void
GradientPropertiesWidget::setGradientWidget(GradientWidget* w)
{
  ui->d_spacing->setValue(w->depthSpacing());
  connect(ui->d_spacing,
          static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged),
          [w](int i) { w->setDepthSpacing(i); });
  ui->d_label->setValue(w->depthLabelRatio());
  connect(ui->d_label,
          static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged),
          [w](int i) { w->setDepthLabelRatio(i); });
  ui->colorBox->setCurrentIndex(static_cast<int>(w->textColor()));
  connect(
    ui->colorBox,
    static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
    [w](int i) { w->setTextColor(static_cast<GradientWidget::TextColor>(i)); });
}

} // namespace
}
}