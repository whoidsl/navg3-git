/**
 * Copyright 2020 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// created by ajdalpe
//

#include "SolutionSelector.h"
#include "ui_SolutionSelector.h"

#include <dslmap/MapView.h>
#include <QTimer>
#include <QAction>
#include <QSettings>
#include <QSortFilterProxyModel>
#include <QStandardItemModel>
#include <QTimer>
#include <QUdpSocket>

namespace navg {
namespace plugins {
namespace solution_selector {

Q_LOGGING_CATEGORY(plugin_solutionselector, "navg.plugins.solutionselector")

SolutionSelector::SolutionSelector(QWidget* parent)
  : QWidget(parent)
  , ui(std::make_unique<Ui::SolutionSelector>())

{
  ui->setupUi(this);
  ui->DR_button->setChecked(false);
  ui->INS_button->setChecked(true);

  socket = new QUdpSocket(this);

  //default state is deadreck
  ui->INS_button->setStyleSheet("QPushButton { border:1px solid green} ");
  ui->INS_button->setText("INS: Enabled");
  ui->DR_button->setStyleSheet("QPushButton { border:1px solid red} ");
  ui->DR_button->setText("DR: Disabled");
  m_selection = 1;

  connect(ui->DR_button,
	  &QPushButton::clicked,
	  this,
	  &SolutionSelector::drButtonClicked);

  connect(ui->INS_button,
	  &QPushButton::clicked,
	  this,
	  &SolutionSelector::insButtonClicked);

  
  timer = new QTimer(this);
  connect(timer, &QTimer::timeout, this, &SolutionSelector::handleTimer);
  timer->start(500);
  
  
}

//   delete socket;
SolutionSelector::~SolutionSelector() = default;

void
SolutionSelector::handleTimer()
{

  // Send the currently selected solution to UDP
  // 0 = DR
  // 1 = INS

  QString selection_os;

  selection_os = QString("SELECT NAV ") + QString::number(m_selection) + QString("\n");

  
  host_address.setAddress(address);
  socket->writeDatagram(selection_os.toLocal8Bit(), host_address, port);

  qDebug(plugin_solutionselector)
    << "wrote datagram " << selection_os << " to " << address << ":" << port;

}
  
void
SolutionSelector::setMapView(QPointer<dslmap::MapView> view)
{
  m_view = view;
}

  
void
SolutionSelector::connectPlugin(const QObject* plugin, const QJsonObject& metadata)
{
  if (!plugin) {
    return;
  }

  const auto name =
    metadata.value("MetaData").toObject().value("name").toString();
  if (name.isEmpty()) {
    return;
  }

  if (name == "navest") {

    // confirm navest received change. outline button in green for visual confirmation
    connect(plugin, SIGNAL(navestSolutionChanged(QString)), this, SLOT(navestUpdated(QString)));
    connect(this,
	    SIGNAL(solutionSelectorChanged(int, QString)),
	    plugin,
	    SLOT(updateSolutionSelected(int, QString)));
  }
  // called to initialize default state
  insButtonClicked();
}

void
SolutionSelector::loadSettings(QSettings* settings)
{
  auto value = settings->value("port");
  auto ok = false;
  if (value.isValid()) {
    const auto valid_val = value.toInt(&ok);
    if (ok) {
      port = valid_val;
    }
  }
  address = settings->value("address").toString();
}

void
SolutionSelector::saveSettings(QSettings& settings)
{
  settings.setValue("persist", 1);
  settings.setValue("address", address);
  settings.setValue("port", port);

}

QList<QAction*>
SolutionSelector::pluginMenuActions()
{
  return {};
}

void
SolutionSelector::drButtonClicked()
{

  m_selection = 0;
  
  //emit signal for navest with vehicle (hardcoded) and solution type
  this->solution = "SOLN_DEADRECK";
  emit solutionSelectorChanged(this->vehicleID, this->solution);
  
}

void
SolutionSelector::insButtonClicked()
{

  m_selection = 1;
  
  //emit signal for navest with vehicle (hardcoded) and solution type
  this->solution = "SOLN_PHINS";
  emit solutionSelectorChanged(this->vehicleID, this->solution);
  
}  

void
SolutionSelector::navestUpdated(QString solution)
{

  QString dr = "SOLN_DEADRECK";
  QString ins = "SOLN_PHINS";
  
// confirms navest has received solution update. turn button green
  if (QString::compare(dr, solution) == 0){
		   
      ui->DR_button->setStyleSheet("QPushButton { border:1px solid green} ");                             ui->DR_button->setText("DR: Enabled");
      
      ui->INS_button->setStyleSheet("QPushButton { border:1px solid red} ");
      ui->INS_button->setText("INS: Disabled");

  }

  else if (QString::compare(ins, solution) == 0){
      
      ui->INS_button->setStyleSheet("QPushButton { border:1px solid green} ");                            ui->INS_button->setText("INS: Enabled");

      ui->DR_button->setStyleSheet("QPushButton { border:1px solid red} ");                               ui->DR_button->setText("DR: Disabled");                                                       
    
  }    

}  

  
} // solution_selector 
} // plugins
} // navg
