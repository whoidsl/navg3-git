/**
 * Copyright 2020 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// created by tmjoyce
//

#ifndef NAVG_GRIDMAPOBJECT_H
#define NAVG_GRIDMAPOBJECT_H

#include <QLoggingCategory>
#include <QGraphicsTextItem>
#include <dslmap/MapView.h>
#include <QGraphicsView>
#include <dslmap/MapScene.h>
#include <QPushButton>
#include <QLabel>
#include <QVBoxLayout>
#include <QWidget>

namespace navg {
namespace plugins {
namespace grid_overlay {

//Q_LOGGING_CATEGORY(plugin_grid_object, "navg.plugins.object")

class GridMapObject : public QGraphicsObject {
  Q_OBJECT

  signals:
    void gridValuesChanged();

  public:
    explicit GridMapObject(dslmap::MapView *m_view, QGraphicsItem* parent = nullptr);
    ~GridMapObject();

    void paint(QPainter* painter, const QStyleOptionGraphicsItem* option,
             QWidget* widget) override;
    QRectF boundingRect() const override;

  public slots:
    void setGridWidth(double width);
    void setGridHeight(double height);
    void drawGridItems();
    int roundVals(int num);

  private:
    double grid_width_ = 0;
    double grid_height_ = 0;
    double grid_spacing_ = 0;

    dslmap::MapView *map_view;
    
    QWidget *layout_widget;
    QVBoxLayout *layout;
    QLabel *grid_width_label;
    QLabel *grid_height_label;
    QLabel *grid_spacing_label;

};


} // grid_overlay
} // plugins
} // navg

#endif // NAVG_GRIDMAPOBJECT_H