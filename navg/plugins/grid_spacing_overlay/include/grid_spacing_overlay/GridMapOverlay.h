/**
 * Copyright 2020 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// created by tmjoyce
//

#ifndef NAVG_GRIDMAPOVERLAY_H
#define NAVG_GRIDMAPOVERLAY_H

#include <QAction>
#include <QHash>
#include <QHostAddress>
#include <QJsonObject>
#include <QLineF>
#include <QLoggingCategory>
#include <QObject>
#include <QTableView>
#include <QTime>
#include <QUuid>
#include <QWidget>
#include <dslmap/MapItem.h>
#include <dslmap/MapLayer.h>
#include <dslmap/MapScene.h>
#include <dslmap/Proj.h>
#include <math.h>
#include <memory>
#include <navg/CorePluginInterface.h>
#include <../navest/Navest.h>
#include <../navest/NavestLayer.h>
#include <dslmap/Util.h>
#include <GridMapObject.h>

namespace navg {
namespace plugins {
namespace grid_overlay {

class GridMapOverlay
  : public QWidget
  , public CorePluginInterface
{
  Q_OBJECT
  Q_INTERFACES(navg::CorePluginInterface)
  Q_PLUGIN_METADATA(IID CorePluginInterface_iid FILE "GridMapOverlay.json")

signals:
  void layerQuantityChanged();

public:
  explicit GridMapOverlay(QWidget* parent = nullptr);
  ~GridMapOverlay() override;

  

  //
  // CorePluginInterface Implementation
  //
  void loadSettings(QSettings* settings) override;
  void saveSettings(QSettings& settings) override;

  void connectPlugin(const QObject* plugin,
                     const QJsonObject& metadata) override;
  void setMapView(QPointer<dslmap::MapView> view) override;
  QList<QAction*> pluginMenuActions() override;
  QList<QAction*> pluginToolbarActions() override { return {}; }
  

protected slots:

  void handleGridSpacingChange(qreal width, qreal height) noexcept;  

private:
  
  QPointer<dslmap::MapView> m_view;
  GridMapObject *grid_object;
  
};

Q_DECLARE_LOGGING_CATEGORY(plugin_grid_overlay)

} // grid_overlay
} // plugins
} // navg

#endif // NAVG_GRIDMAPOVERLAY_H
