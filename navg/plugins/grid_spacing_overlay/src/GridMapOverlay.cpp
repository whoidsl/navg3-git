/**
 * Copyright 2020 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// created by tmjoyce
//

#include "GridMapOverlay.h"

#include <dslmap/MapView.h>

#include <QAction>
#include <QSettings>
#include <QSortFilterProxyModel>
#include <QStandardItemModel>
#include <QTimer>
#include <QUdpSocket>

namespace navg {
namespace plugins {
namespace grid_overlay {

Q_LOGGING_CATEGORY(plugin_grid_overlay, "navg.plugins.grid_overlay")

GridMapOverlay::GridMapOverlay(QWidget* parent)
  : QWidget(parent)

{
  
}
GridMapOverlay::~GridMapOverlay() {
  delete grid_object;
};

void
GridMapOverlay::setMapView(QPointer<dslmap::MapView> view)
{
  m_view = view;
  grid_object = new GridMapObject(m_view);
  // connect layer add and remove hooks to combo box
  //auto scene = m_view->mapScene();
  
  //connect(m_view, &dslmap::MapView::graticuleWidthChanged, this, &GridMapOverlay::handleGridSpacingChange);
  connect(m_view, &dslmap::MapView::gridSpacingChanged, this, &GridMapOverlay::handleGridSpacingChange);
  //connect(m_view, &dslmap::MapView::graticuleWidthSpacingChanged, this, &GridMapOverlay::handleGridSpacingChange);
  
}

void
GridMapOverlay::connectPlugin(const QObject* plugin,
                                  const QJsonObject& metadata)
{
  if (!plugin) {
    return;
  }

  const auto name =
    metadata.value("MetaData").toObject().value("name").toString();
  if (name.isEmpty()) {
    return;
  }
}

void
GridMapOverlay::loadSettings(QSettings*)
{}

void
GridMapOverlay::saveSettings(QSettings& settings)
{
  settings.setValue("persist", 1);
}

QList<QAction*>
GridMapOverlay::pluginMenuActions()
{
  return {};
}

//slots

void GridMapOverlay::handleGridSpacingChange(qreal width, qreal height) noexcept {
 //qCDebug(plugin_grid_overlay) << "spacing is " << zoom_level;

 //qCDebug(plugin_grid_overlay) << "graticule width from m_view is  " << m_view->graticuleWidth();

 //qCDebug(plugin_grid_overlay) << "Actual graticule spacing width is " << QString::number(m_view->graticuleSpacingWidth(), 'f' , 6);

 //qCDebug(plugin_grid_overlay) << "Actual graticule spacing height is " << QString::number(m_view->graticuleSpacingHeight(), 'f', 6);

 //may result in NaN on double allocation
 //double width = m_view->graticuleSpacingWidth();

 if (!grid_object){
   qDebug() << "no grid object constructed. returning";
   return;
 }

 grid_object->setGridWidth(width);
 grid_object->setGridHeight(height);
}

} // grid_overlay
} // plugins
} // navg
