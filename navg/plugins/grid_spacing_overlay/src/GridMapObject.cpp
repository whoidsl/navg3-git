/**
 * Copyright 2020 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// created by tmjoyce
//

#include "GridMapObject.h"

namespace navg {
namespace plugins {
namespace grid_overlay {

GridMapObject::GridMapObject(dslmap::MapView* m_view, QGraphicsItem* parent)
  : QGraphicsObject(parent)

{

  map_view = m_view;

  layout_widget = new QWidget(map_view);

  layout_widget->setFixedWidth(300);
  layout_widget->setFixedHeight(40);

  layout = new QVBoxLayout(map_view);
  layout_widget->setLayout(layout);
  // layout->SetMaximumSize(50,30);
  grid_width_label = new QLabel("Grid Width: ", map_view);
  grid_height_label = new QLabel("Grid Height: ", map_view);
  grid_spacing_label = new QLabel("Grid Spacing: ", map_view);

  layout->addWidget(grid_width_label);
  layout->addWidget(grid_height_label);
  layout->addWidget(grid_spacing_label);

  layout_widget->show();
  grid_width_label->hide();
  grid_height_label->hide();
  grid_spacing_label->show();

  connect(this,
          &GridMapObject::gridValuesChanged,
          this,
          &GridMapObject::drawGridItems);
}

GridMapObject::~GridMapObject() {
  delete grid_width_label;
  delete grid_height_label;
  delete grid_spacing_label;
  delete layout_widget;
}

void
GridMapObject::paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget*)
{}

QRectF
GridMapObject::boundingRect() const
{
  return QRectF();
}

void
GridMapObject::drawGridItems()
{
  // grid_width_item->setPlainText("WIDTH:");
  // grid_height_item->setPlainText("HEIGHT:");

  if (!map_view) {
    return;
  }

  QFont f("sans", 12, QFont::Bold);
  grid_width_label->setText("Grid Width: " + QString::number(int(grid_width_)) +
                            "m");
  grid_width_label->setFont(f);
  grid_height_label->setText("Grid Height: " + QString::number(int(grid_height_)) +
                             "m");
  grid_height_label->setFont(f);
  
  grid_spacing_ = int((roundVals(grid_width_) + roundVals(grid_height_))/2);
  grid_spacing_label->setText("Grid Spacing: " + QString::number(grid_spacing_) + "m");
  grid_spacing_label->setFont(f);
}

void
GridMapObject::setGridWidth(double width)
{
  grid_width_ = width;
  //qDebug() << "grid width is " << grid_width_;
  emit gridValuesChanged();
}
void
GridMapObject::setGridHeight(double height)
{
  grid_height_ = height;
  emit gridValuesChanged();
}

int GridMapObject::roundVals(int num) {
  int lower_bound = (num / 5) * 5;
  int upper_bound = lower_bound + 5;
  return (num - lower_bound > upper_bound - num) ? upper_bound : lower_bound;
}
} // grid_overlay
} // plugins
} // navg
