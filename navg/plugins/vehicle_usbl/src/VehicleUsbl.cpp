/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro 12/6/18.
//

#include "VehicleUsbl.h"
#include "ui_VehicleUsbl.h"

#include <dslmap/MapView.h>
#include <dslmap/Util.h>

namespace navg {
namespace plugins {
namespace vehicle_usbl {

Q_LOGGING_CATEGORY(plugin_vehicle_usbl, "navg.plugins.vehicle_usbl")

VehicleUsbl::VehicleUsbl(QWidget* parent)
  : QWidget(parent)
  , ui(std::make_unique<Ui::VehicleUsbl>())
  , m_seconds_since(-1)
{
  ui->setupUi(this);

  // This timer helps show the age of USBL data
  // If the data is old, it turns an outline yellow
  auto timer = new QTimer(this);
  connect(timer, &QTimer::timeout, [=]() {
    if (m_seconds_since < 0) {
      return;
    }
    ui->ageLbl->setText("Age: " + QString::number(m_seconds_since));
    if (m_seconds_since == m_timeout) {
      ui->groupBox->setStyleSheet("QGroupBox { border:1px solid yellow} ");
    }
    m_seconds_since++;
  });
  timer->start(1000);

  // SS - added default lat lon format, otherwise no data displayed until format changed
  m_latlon_format = dslmap::LatLonFormat::DD_NE;
}

VehicleUsbl::~VehicleUsbl() = default;

void
VehicleUsbl::connectPlugin(const QObject* plugin, const QJsonObject& metadata)
{
  if (!plugin) {
    return;
  }

  const auto name =
    metadata.value("MetaData").toObject().value("name").toString();
  if (name.isEmpty()) {
    return;
  }

  // At startup time, crash the program if the connection to navest fails
  // We don't want quiet failures if something is broken
  if (name == "navest") {
    if (!connect(plugin, SIGNAL(messageReceived(NavestMessageType, QVariant)),
                 this,
                 SLOT(handleNavestMessage(NavestMessageType, QVariant)))) {
      qCCritical(plugin_vehicle_usbl,
                 "Unable to connect to signal: "
                 "Navest::messageReceived(NavestMessageType, "
                 "QVariant)");
    } else {
      qCInfo(plugin_vehicle_usbl, "Connected to Navest::messageReceived");
    }
  }

  // vehicle_usbl also connects to sentry_plot and sentry_hdg_depth_alt,
  // but these connections are made in the plugin with the callbacks.
  // Then signals can be public and slots can be private/protected
}

void
VehicleUsbl::loadSettings(QSettings* settings)
{
  // Default values are set here as well, in case there's
  // no settings to load
  m_timeout = settings->value("timeout", 30).toInt();
  m_vehicle_id = settings->value("vehicle_navest_id", 0).toInt();
}

void
VehicleUsbl::saveSettings(QSettings& settings)
{
  // Users can set the timeout when it turns data stale and yellow
  settings.setValue("timeout", m_timeout);

  // Users can set the appropriate navest beacon
  settings.setValue("vehicle_navest_id", m_vehicle_id);
}

void
VehicleUsbl::handleNavestMessage(NavestMessageType type, QVariant message)
{
  // First, check that incoming data has the correct type and is coming
  // in from the correct beacon. We want the vehicle USBL solution
  if (type != NavestMessageType::VFR) {
    return;
  }
  NavestMessageVfr msg = message.value<NavestMessageVfr>();
  if (msg.vehicle_number == m_vehicle_id && msg.fix_source == "SOLN_USBL") {

    // If the data was stale, then make it look fresh with a green border!
    if (m_seconds_since != -1 && m_seconds_since < m_timeout) {
    } else {
      ui->groupBox->setStyleSheet("QGroupBox { border:1px solid green;} ");
    }
    m_seconds_since = 0;

    // Update the latest information values
    ui->latLbl->setText(dslmap::formatLatitude(msg.lat, m_latlon_format));
    ui->lonLbl->setText(dslmap::formatLongitude(msg.lon, m_latlon_format));
    ui->depthLbl->setText(QString::number(msg.depth));

    // Emit the depth for plugins sentry_plot and sentry_hdg_depth_alt
    emit depthUpdated(-msg.depth);
  }
}

void
VehicleUsbl::setMapView(QPointer<dslmap::MapView> view)
{

  connect(view.data(), &dslmap::MapView::latLonFormatChanged, this,
          &VehicleUsbl::setLatLonFormat);
}

} // vehicle_usbl
} // plugins
} // navg
