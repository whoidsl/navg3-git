/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 12/6/18.
//

#ifndef NAVG_VEHICLEUSBL_H
#define NAVG_VEHICLEUSBL_H

// This plugin uses a message type defined in the navest plugin
#include <navest/Messages.h>
#include <navg/CorePluginInterface.h>

#include <dslmap/DslMap.h>

// There are a series of QObjects used in the plugin
#include <QAction>
#include <QHash>
#include <QHostAddress>
#include <QJsonObject>
#include <QLoggingCategory>
#include <QObject>
#include <QTime>
#include <QWidget>

#include <memory>

// Namespace as a navg plugin for consistency
namespace navg {
namespace plugins {
namespace vehicle_usbl {

namespace Ui {
class VehicleUsbl;
}

// Subclass CorePluginInterface
// an abstract class which defines several pure virtual methods

class VehicleUsbl : public QWidget, public CorePluginInterface
{

  // Declare some macros for better Qt ease
  Q_OBJECT
  Q_INTERFACES(navg::CorePluginInterface)
  Q_PLUGIN_METADATA(IID CorePluginInterface_iid FILE "VehicleUsbl.json")

signals:
  // Plugins should only communicate with one another via signals and slots
  void depthUpdated(double depth);

public:
  explicit VehicleUsbl(QWidget* parent = nullptr);
  ~VehicleUsbl() override;

  //
  // CorePluginInterface Implementation
  //
  void loadSettings(QSettings* settings) override;
  void saveSettings(QSettings& settings) override;

  // This plugin connects to the navest plugin.
  void connectPlugin(const QObject* plugin,
                     const QJsonObject& metadata) override;

  // This plugin does not use the MapView
  void setMapView(QPointer<dslmap::MapView> view) override;

  // If there are no actions, just return an empty list
  QList<QAction*> pluginMenuActions() override { return {}; }
  QList<QAction*> pluginToolbarActions() override { return {}; }

public slots:
  void setLatLonFormat(dslmap::LatLonFormat format) noexcept
  {
    m_latlon_format = format;
  };
protected slots:
  /// \brief Receive and filter out the latest VehicleUsbl
  /// status from the navest plugin
  void handleNavestMessage(NavestMessageType type, QVariant message);

private:
  // variables should not be directly accessible from the outside
  std::unique_ptr<Ui::VehicleUsbl> ui;
  int m_seconds_since;
  int m_timeout;
  int m_vehicle_id;
  dslmap::LatLonFormat m_latlon_format;
};

Q_DECLARE_LOGGING_CATEGORY(plugin_vehicle_usbl)
} // vehicle_usbl
} // plugins
} // navg

#endif // NAVG_VEHICLEUSBL_H
