/**
 * Copyright 2021 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// Created by ssuman
//

#include "SmsControl.h"
#include "ui_SmsControl.h"
#include <NavGMapScene.h>
#include <QMessageBox>
#include <QSettings>
#include <dslmap/Proj.h>

namespace navg {
namespace plugins {
namespace sms_control {

Q_LOGGING_CATEGORY(plugin_sms_control, "navg.plugins.sms_control")

SmsControl::SmsControl(QWidget* parent)
  : QWidget(parent)
  , ui(std::make_unique<Ui::SmsControl>())
  , m_action_show_settings(new QAction("&Settings", this))
  , address("127.0.0.1")
{
  ui->setupUi(this);

  socket = new QUdpSocket(this);

  in_socket_port = 0;

  connect(m_action_show_settings,
          &QAction::triggered,
          this,
          &SmsControl::showSettingsDialog);

  QObject::connect(socket,
                   SIGNAL(readyRead()),
                   this,
                   SLOT(dataPending()),
                   Qt::UniqueConnection);
  connect(
     ui->onBtn,
     &QPushButton::clicked,
     this,
     &SmsControl::handleOnBtnClicked);

  connect(
     ui->offBtn,
     &QPushButton::clicked,
     this,
     &SmsControl::handleOffBtnClicked);

}

SmsControl::~SmsControl()
{
  delete socket;
}

void
SmsControl::handleOnBtnClicked()
{
  qDebug(plugin_sms_control) << "On Button Clicked";

  // json good buffers bad >:(
  QJsonObject jsonObj;
  jsonObj["maxSec"] = ui->maxSecSpn->value();
  jsonObj["minSec"] = ui->minSecSpn->value();
  jsonObj["triggerId"] = ui->triggerSpn->value();
  jsonObj["extraId"] = ui->extraSpn->value();
  jsonObj["active"] = 1;
  QString type = "sms_control";
  jsonObj["type"] = type;

  // Example:  "{\"active\":1,\"extraId\":3,\"maxSec\":60,\"minSec\":15,\"triggerId\":0,\"type\":\"sms_control\"}"
  
  qDebug(plugin_sms_control)
    << "sending target value of "
    << QJsonDocument(jsonObj).toJson(QJsonDocument::Compact);
  const QByteArray data = QJsonDocument(jsonObj).toJson(QJsonDocument::Compact);

  host_address.setAddress(address);
  socket->writeDatagram(data, host_address, to_socket_port);

  qDebug(plugin_sms_control)
    << "wrote datagram to " << address << ":" << to_socket_port;

}

void
SmsControl::handleOffBtnClicked()
{
  qDebug(plugin_sms_control) << "Off Button Clicked";

  qDebug(plugin_sms_control) << "On Button Clicked";

  // json good buffers bad >:(
  QJsonObject jsonObj;
  jsonObj["maxSec"] = ui->maxSecSpn->value();
  jsonObj["minSec"] = ui->minSecSpn->value();
  jsonObj["triggerId"] = ui->triggerSpn->value();
  jsonObj["extraId"] = ui->extraSpn->value();
  jsonObj["active"] = 0;
  QString type = "sms_control";
  jsonObj["type"] = type;

  // Example:  "{\"active\":0,\"extraId\":3,\"maxSec\":60,\"minSec\":15,\"triggerId\":0,\"type\":\"sms_control\"}"
  
  qDebug(plugin_sms_control)
    << "sending target value of "
    << QJsonDocument(jsonObj).toJson(QJsonDocument::Compact);
  const QByteArray data = QJsonDocument(jsonObj).toJson(QJsonDocument::Compact);

  host_address.setAddress(address);
  socket->writeDatagram(data, host_address, to_socket_port);

  qDebug(plugin_sms_control)
    << "wrote datagram to " << address << ":" << to_socket_port;
}

void
SmsControl::setMapView(QPointer<dslmap::MapView> view)
{
  m_view = view;
  n_view = qobject_cast<NavGMapView*>(view);
}

void
SmsControl::connectPlugin(const QObject* plugin,
			  const QJsonObject& metadata)
{
  if (!plugin) {
    return;
  }

  const auto name =
    metadata.value("MetaData").toObject().value("name").toString();
  if (name.isEmpty()) {
    return;
  }
}

void
SmsControl::loadSettings(QSettings* settings)
{

  auto value = settings->value("in_port");
  auto ok = false;
  if (value.isValid()) {
    const auto valid_val = value.toInt(&ok);
    if (ok) {
      in_socket_port = valid_val;
    }
  }

  if (!socket->bind(in_socket_port,
                    QUdpSocket::ShareAddress | QUdpSocket::ReuseAddressHint)) {
    qDebug(plugin_sms_control) << "could not bind to socket";
  } else {
    QObject::connect(socket,
                     SIGNAL(readyRead()),
                     this,
                     SLOT(dataPending()),
                     Qt::UniqueConnection);
  }

  auto value2 = settings->value("to_port");
  ok = false;
  if (value2.isValid()) {
    const auto valid_val = value2.toInt(&ok);
    if (ok) {
      to_socket_port = valid_val;
    }
  }

}

void
SmsControl::showSettingsDialog()
{

  QScopedPointer<SmsControlSettings> dialog(new SmsControlSettings);
  qDebug(plugin_sms_control) << "port value we are setting to dialog is " << in_socket_port;
  dialog->setPort(in_socket_port);
  qDebug(plugin_sms_control) << "port to value we are setting to dialog is " << to_socket_port;
  dialog->setToPort(to_socket_port);

  dialog->setModal(true);
  if (!dialog->exec()) {
    return;
  }

  int temp = dialog->getPort();
  if (in_socket_port != temp) {
    in_socket_port = temp;
    socket->close();
    if (!socket->bind(in_socket_port,
                      QUdpSocket::ShareAddress |
                        QUdpSocket::ReuseAddressHint)) {
      qDebug(plugin_sms_control) << "could not bind to socket";
    } else {
      QObject::connect(socket,
                       SIGNAL(readyRead()),
                       this,
                       SLOT(dataPending()),
                       Qt::UniqueConnection);
    }
  }
  qDebug(plugin_sms_control) << "port value received from dialog is " << in_socket_port;

  int temp2 = dialog->getToPort();
  if (to_socket_port != temp2) {
    to_socket_port = temp2;
    }
  qDebug(plugin_sms_control) << "to port value received from dialog is " << to_socket_port;

}

void
SmsControl::saveSettings(QSettings& settings)
{
  settings.setValue("persist", 1);
  settings.setValue("in_port", in_socket_port);
  settings.setValue("to_port", to_socket_port);
}

QList<QAction*>
SmsControl::pluginMenuActions()
{
  return { m_action_show_settings };
}

void
SmsControl::dataPending()
{
  QUdpSocket* theSocket = (QUdpSocket*)sender();

  while (theSocket->hasPendingDatagrams()) {

    QByteArray buffer(theSocket->pendingDatagramSize(), 0);
    theSocket->readDatagram(buffer.data(), buffer.size());
    //qint64 bytesRead = theSocket->readDatagram(buffer.data(), buffer.size());
    //qDebug(plugin_sms_control) << "Bytes read: " << bytesRead;

    QJsonDocument itemDoc = QJsonDocument::fromJson(buffer);
    QJsonObject obj = itemDoc.object();
    QString type = obj.value("type").toVariant().toString();
    if (type != "sms_params"){
      return;
    }
    auto extraId = obj.value("extra_veh_index").toVariant().toInt();
    auto triggerId = obj.value("trigger_index").toVariant().toInt();
    auto maxTime = obj.value("max_time_between_downlinks").toVariant().toInt();
    auto minTime = obj.value("min_time_between_downlinks").toVariant().toInt();
    
    /*
    qDebug(plugin_sms_control)
      << "Received message\nextraId:" << QString::number(extraId)
      << "\ntriggerId:" << QString::number(triggerId) << "\nmaxTime:" << QString::number(maxTime) << "\nminTime: " << QString::number(minTime);
*/
    ui->extraLcd->display(extraId);
    ui->triggerLcd->display(triggerId);
    ui->maxSecLcd->display(maxTime);
    ui->minSecLcd->display(minTime);

    if (obj.value("active").toVariant().toInt() == 1)
      {
	ui->onBtn->setStyleSheet("QPushButton { border:1px solid green} ");
	ui->offBtn->setStyleSheet("QPushButton { border:1px solid red} ");
      }
    else
      {
	ui->onBtn->setStyleSheet("QPushButton { border:1px solid red} ");
	ui->offBtn->setStyleSheet("QPushButton { border:1px solid green} ");
      }
  }
}


} // sms_control
} // plugins
} // navg
