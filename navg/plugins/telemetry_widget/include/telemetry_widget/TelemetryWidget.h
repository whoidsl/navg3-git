/**
 * Copyright 2021 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// Created by ssuman
//

#ifndef NAVG_TELEMETRYWIDGET_H
#define NAVG_TELEMETRYWIDGET_H

#include <navg/CorePluginInterface.h>

#include "dslmap/MapView.h"
#include "dslmap/SymbolLayer.h"
#include "dslmap/MapLayer.h"
#include "dslmap/Proj.h"
#include <../navest/Navest.h>
#include <../navest/NavestLayer.h>
#include <QLoggingCategory>
#include <QObject>
#include <QWidget>
#include <QAction>
#include <QJsonDocument>
#include <QJsonObject>
#include <QHostAddress>
#include <QUdpSocket>
#include <QDateTime>
#include <QTimer>
#include <memory>
#include "TelemetryWidgetSettings.h"

#include <navg/NavGMapView.h>

namespace navg {
namespace plugins {
namespace telemetry_widget {

namespace Ui {
  class TelemetryWidget;
}

class TelemetryWidget
  : public QWidget
  , public CorePluginInterface
{
  Q_OBJECT
  Q_INTERFACES(navg::CorePluginInterface)
  Q_PLUGIN_METADATA(IID CorePluginInterface_iid FILE "TelemetryWidget.json")

signals:
  /// \brief Emitted when a message is received.
  void headingReceived(const double heading);
  /// \brief Emitted when use acomms heading is toggled or vehicle num changed                      
  void headingToggleChanged(bool heading_toggle, int id);

public:
  explicit TelemetryWidget(QWidget* parent = nullptr);
  ~TelemetryWidget() override;

  //
  // CorePluginInterface Implementation
  //
  void loadSettings(QSettings* settings) override;
  void saveSettings(QSettings& settings) override;

  // This plugin connects to ....
  void connectPlugin(const QObject* plugin,
                     const QJsonObject& metadata) override;
  void setMapView(QPointer<dslmap::MapView> view) override;
  QList<QAction*> pluginMenuActions() override;
  QList<QAction*> pluginToolbarActions() override { return {}; }

public slots:
  /// \brief Show the settings dialog
  ///
  void showSettingsDialog();

protected:

  void ageCheck();

protected slots:

  void dataPending();
  void setAcommHeading(bool heading_toggle, int id, bool force = false);

private slots:
  void updateNavestHeading(const double heading);

private:
  QPointer<dslmap::MapView> m_view;
  QPointer<navg::NavGMapView> n_view;

  QHostAddress host_address;
  int in_socket_port;

  std::unique_ptr<Ui::TelemetryWidget> ui;

  QPointer<QUdpSocket> socket;
  QAction* m_action_show_settings = nullptr;

  QPointer<dslmap::SymbolLayer> layer = nullptr;
  dslmap::MarkerItem* item = nullptr;

  QDateTime lastRxd; // time of the last received message

  QTimer *sacTimer; // age check timer

  // Option to set sentry navest layer heading directly to value received by acomms
  bool use_acomm_heading = false;
  int navest_vehicle_id = 0;
  navg::plugins::navest::NavestLayer* vehicle_layer = nullptr;
};

Q_DECLARE_LOGGING_CATEGORY(plugin_telemetry_widget)
} // telemetry_widget
} // plugins
} // navg

#endif // TELEMETRYWIDGET_H
