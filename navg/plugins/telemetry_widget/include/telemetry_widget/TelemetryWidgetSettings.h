/**
* Copyright 2021 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef NAVG_TELEMETRYWIDGETSETTINGS_H
#define NAVG_TELEMETRYWIDGETSETTINGS_H

#include <QDialog>
#include <memory>

namespace navg {
namespace plugins {
namespace telemetry_widget {

namespace Ui {
class TelemetryWidgetSettings;
}

class TelemetryWidgetSettings : public QDialog
{
  Q_OBJECT

public:

  explicit TelemetryWidgetSettings(QWidget* parent = 0);
  ~TelemetryWidgetSettings();

  void setPort(int num);
  int getPort();

  /// \brief Set the navest heading toggle
  void setNavestHeadingToggle(bool use_heading);

  /// \brief Set the navest vehicle id
  void setVehicleId(int id);

  /// \brief Get heading toggle state
  bool navestHeadingToggle();

  /// \brief Get the navest vehicle id
  int vehicleId();

private:
  std::unique_ptr<Ui::TelemetryWidgetSettings> ui;
};

} // namespace telemetry_widget
} // namespace plugins
} // namespace navg
#endif // NAVG_TELEMETRYWIDGETSETTINGS_H
