/**
 * Copyright 2021 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// Created by ssuman
//

#include "TelemetryWidget.h"
#include "ui_TelemetryWidget.h"
#include <NavGMapScene.h>
#include <QMessageBox>
#include <QSettings>
#include <dslmap/Proj.h>

namespace navg {
namespace plugins {
namespace telemetry_widget {

Q_LOGGING_CATEGORY(plugin_telemetry_widget, "navg.plugins.telemetry_widget")

TelemetryWidget::TelemetryWidget(QWidget* parent)
  : QWidget(parent)
  , ui(std::make_unique<Ui::TelemetryWidget>())
  , m_action_show_settings(new QAction("&Settings", this))
  , lastRxd(QDateTime::currentDateTimeUtc().addSecs(-180))
{
  ui->setupUi(this);

  socket = new QUdpSocket(this);

  in_socket_port = 0;

  connect(m_action_show_settings,
          &QAction::triggered,
          this,
          &TelemetryWidget::showSettingsDialog);

  QObject::connect(socket,
                   SIGNAL(readyRead()),
                   this,
                   SLOT(dataPending()),
                   Qt::UniqueConnection);

  connect(this, &TelemetryWidget::headingReceived, this, &TelemetryWidget::updateNavestHeading);

  sacTimer = new QTimer(this);

  connect(sacTimer, &QTimer::timeout, this, &TelemetryWidget::ageCheck);

  sacTimer->start(1000); // Start the timer with a 1 sec period age check
}

TelemetryWidget::~TelemetryWidget()
{
  delete socket;
}

void
TelemetryWidget::setMapView(QPointer<dslmap::MapView> view)
{
  m_view = view;
  n_view = qobject_cast<NavGMapView*>(view);
}

void
TelemetryWidget::connectPlugin(const QObject* plugin,
			  const QJsonObject& metadata)
{
  if (!plugin) {
    return;
  }

  const auto name =
    metadata.value("MetaData").toObject().value("name").toString();
  if (name.isEmpty()) {
    return;
  }
  
  if (name == "navest") {
    if (!connect(this,
                 SIGNAL(headingToggleChanged(bool, int)),
                 plugin,
                 SLOT(setHeadingMode(bool, int)))) {
      qCCritical(plugin_telemetry_widget, "failed to connect to navest plugin navest");
    }
    else {
      setAcommHeading(use_acomm_heading, navest_vehicle_id, true);
    }
  }
  
}

void
TelemetryWidget::loadSettings(QSettings* settings)
{

  auto value = settings->value("in_port");
  auto ok = false;
  if (value.isValid()) {
    const auto valid_val = value.toInt(&ok);
    if (ok) {
      in_socket_port = valid_val;
    }
  }

  if (!socket->bind(in_socket_port,
                    QUdpSocket::ShareAddress | QUdpSocket::ReuseAddressHint)) {
    qDebug(plugin_telemetry_widget) << "could not bind to socket";
  } else {
    QObject::connect(socket,
                     SIGNAL(readyRead()),
                     this,
                     SLOT(dataPending()),
                     Qt::UniqueConnection);
  }

  bool temp = settings->value("use_acomm_heading").toBool();
  int id = settings->value("navest_vehicle_id").toInt(&ok);
  if (ok) {
    setAcommHeading(temp,id);
  } else {
    qCWarning(plugin_telemetry_widget)
      << "Unable to load 'navest_vehicle_id' from settings";
  }

}

void
TelemetryWidget::showSettingsDialog()
{

  QScopedPointer<TelemetryWidgetSettings> dialog(new TelemetryWidgetSettings);
  qDebug(plugin_telemetry_widget) << "port value we are setting to dialog is " << in_socket_port;
  dialog->setPort(in_socket_port);

  // navest settings
  dialog->setNavestHeadingToggle(use_acomm_heading);
  dialog->setVehicleId(navest_vehicle_id);

  dialog->setModal(true);
  if (!dialog->exec()) {
    return;
  }

  navest_vehicle_id = dialog->vehicleId();
  use_acomm_heading = dialog->navestHeadingToggle();

  int temp = dialog->getPort();
  if (in_socket_port != temp) {
    in_socket_port = temp;
    socket->close();
    if (!socket->bind(in_socket_port,
                      QUdpSocket::ShareAddress |
                        QUdpSocket::ReuseAddressHint)) {
      qDebug(plugin_telemetry_widget) << "could not bind to socket";
    } else {
      QObject::connect(socket,
                       SIGNAL(readyRead()),
                       this,
                       SLOT(dataPending()),
                       Qt::UniqueConnection);
    }
  }
  qDebug(plugin_telemetry_widget) << "port value received from dialog is " << in_socket_port;

}

void
TelemetryWidget::saveSettings(QSettings& settings)
{
  settings.setValue("persist", 1);
  settings.setValue("in_port", in_socket_port);
  settings.setValue("use_acomm_heading", use_acomm_heading);
  settings.setValue("navest_vehicle_id", navest_vehicle_id);
}

QList<QAction*>
TelemetryWidget::pluginMenuActions()
{
  return { m_action_show_settings };
}

void
TelemetryWidget::dataPending()
{
  QUdpSocket* theSocket = (QUdpSocket*)sender();

  while (theSocket->hasPendingDatagrams()) {

    try
      {
	QByteArray buffer(theSocket->pendingDatagramSize(), 0);
	qint64 bytesRead = theSocket->readDatagram(buffer.data(), buffer.size());
	qDebug(plugin_telemetry_widget) << "Bytes read: " << bytesRead;

	QJsonDocument itemDoc = QJsonDocument::fromJson(buffer);
	QJsonObject obj = itemDoc.object();
	QString type = obj.value("type").toVariant().toString();
	if (type != "uplink"){
	  return;
	}
	auto dep = obj.value("depth").toVariant().toDouble();
	auto hdg = obj.value("heading").toVariant().toDouble();
	auto alt = obj.value("alt").toVariant().toDouble();

	qDebug(plugin_telemetry_widget)
	  << "Received message\ndepth:" << QString::number(dep)
	  << "\nhdg:" << QString::number(hdg) << "\nalt:" << QString::number(alt);

	ui->smsDepVleLbl->setText(QString::number(dep, 'f', 2));
	ui->smsHdgVleLbl->setText(QString::number(hdg, 'f', 2));
	ui->smsAltVleLbl->setText(QString::number(alt, 'f', 2));

	ui->smsDepVleLbl->setStyleSheet("QLabel { color: green} ");
	ui->smsHdgVleLbl->setStyleSheet("QLabel { color: green} ");
	ui->smsAltVleLbl->setStyleSheet("QLabel { color: green} ");

  emit headingReceived(hdg);

	lastRxd = QDateTime::currentDateTimeUtc();
      }
    catch (...)
      {
	qDebug(plugin_telemetry_widget) << "Exception while handling uplink";
      }
  }
}

void TelemetryWidget::ageCheck()
{
  QDateTime now = QDateTime::currentDateTimeUtc();

  //qDebug(plugin_telemetry_widget) << "Age check";

  if (lastRxd.secsTo(now) <= 60)
    {
      // Green
      ui->smsDepVleLbl->setStyleSheet("QLabel { color: green} ");
      ui->smsHdgVleLbl->setStyleSheet("QLabel { color: green} ");
      ui->smsAltVleLbl->setStyleSheet("QLabel { color: green} ");
    }
  else if (lastRxd.secsTo(now) > 60 && lastRxd.secsTo(now) <= 120)
    {
      // Yellow
      ui->smsDepVleLbl->setStyleSheet("QLabel { color: yellow} ");
      ui->smsHdgVleLbl->setStyleSheet("QLabel { color: yellow} ");
      ui->smsAltVleLbl->setStyleSheet("QLabel { color: yellow} ");
    }
  else if (lastRxd.secsTo(now) > 120)
    {
      // Red
      ui->smsDepVleLbl->setStyleSheet("QLabel { color: red} ");
      ui->smsHdgVleLbl->setStyleSheet("QLabel { color: red} ");
      ui->smsAltVleLbl->setStyleSheet("QLabel { color: red} ");
    }
}

void TelemetryWidget::updateNavestHeading(const double heading)
{

  if (!use_acomm_heading) {
    return;
  }
  if (!m_view)
    return;

  auto scene = m_view->mapScene();

  if (!scene) {
    qCInfo(plugin_telemetry_widget) << "no scene";
    return;
  }

  if (!vehicle_layer) {
    // find the correct vehicle layer on the map
    using LayerHash = QHash<QUuid, dslmap::MapLayer*>;
    LayerHash nav_solution_layers =
      scene->layers(dslmap::MapItemType::NavestLayerType);
    if (nav_solution_layers.empty()) {
      qCInfo(plugin_telemetry_widget) << "layer list is empty";
      return;
    }
  
    QHash<QUuid, dslmap::MapLayer*>::iterator i;
    for (i = nav_solution_layers.begin(); i != nav_solution_layers.end(); ++i) {
      navg::plugins::navest::NavestLayer* nav_layer =
        dynamic_cast<navg::plugins::navest::NavestLayer*>(i.value()); //was supposed to be dynamic cast, but run time checking is messing something up
        //hope it still works -TJ 2023-12-06
  
      if (!nav_layer) {
        qCInfo(plugin_telemetry_widget) << "no navestlayer found";
        continue;
      }
      if (nav_layer->navestBeaconId() == navest_vehicle_id) {
        vehicle_layer = nav_layer;
        break;
      }
    }
  }

  else {
    // if the layer is set to acomms heading mode...update the heading
    if (vehicle_layer->acommsHeading()){
      vehicle_layer->updateHeading(heading);
    }
  }
}

void
TelemetryWidget::setAcommHeading(bool heading_toggle, int id, bool force){

// if input is different from internal heading OR new vehicle id OR Force set(force navest signal to be emitted no matter state)
  if (use_acomm_heading != heading_toggle || navest_vehicle_id != id || force){
    use_acomm_heading = heading_toggle;
    navest_vehicle_id = id;
    emit headingToggleChanged(heading_toggle, id);
  }
  use_acomm_heading = heading_toggle;
  navest_vehicle_id = id;
}
  
} // telemetry_widget
} // plugins
} // navg
