
### Helper script for simulating data input into the navg Telemetry widget.
### Run with python3 no args
### Change IP and PORT as needed. -TJ

import socket
import json
from random import randint
from time import time, sleep
from typing import Dict, Any

IP = "127.0.0.1"
PORT = 10912


def generate_json_message() -> Dict[str, Any]:
    return {
        "depth": randint(1, 100),
        "heading": randint(100, 200),
        "alt": randint(144, 1005),
        "type": "uplink"
    }


def send_json_message(sock, gen_message):
    message = (json.dumps(gen_message)).encode()
    sock.sendto(message, (IP, PORT))
    print(f'{len(message)} bytes sent')


def main() -> None:
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) 

    while True:
        json_message = generate_json_message()
        send_json_message(sock, json_message)
        sleep(1)


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        pass
