/**
 * Copyright 2021 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "VehicleDvlSettings.h"
#include "ui_VehicleDvlSettings.h"

namespace navg {
namespace plugins {
namespace vehicle_dvl {

VehicleDvlSettings::VehicleDvlSettings(QWidget* parent)
  : QDialog(parent)
 , ui(std::make_unique<Ui::VehicleDvlSettings>())
{
  ui->setupUi(this);
}

VehicleDvlSettings::~VehicleDvlSettings() = default;

void
VehicleDvlSettings::setFixVehicleNumberBox(int num)
{
  ui->fix_vehicle_spin->setValue(num);
}

void
VehicleDvlSettings::setFixVehicleMode(int num){
  if (num == 1){
    ui->reduced_radio->setChecked(true);
  }
  else {
    ui->full_radio->setChecked(true);
  }
}

int
VehicleDvlSettings::getFixVehicleNumber()
{
  return ui->fix_vehicle_spin->value();
}

int
VehicleDvlSettings::getDvlMode(){

  return ui->reduced_radio->isChecked();
}

} // namespace vehicle_dvl
} // namespace plugins
} // namespace navg
