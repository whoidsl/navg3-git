/**
 * Copyright 2020 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "VehicleDvl.h"
#include "ui_VehicleDvl.h"
#include <QAction>
#include <QSettings>
#include <QTimer>
#include <dslmap/MapScene.h>
#include <dslmap/MapView.h>
#include <dslmap/Proj.h>
#include <navg/NavGMapView.h>

namespace navg {
namespace plugins {
namespace vehicle_dvl {

Q_LOGGING_CATEGORY(plugin_vehicle_dvl, "navg.plugins.dvl_switch")

VehicleDvl::VehicleDvl(QWidget* parent)
  : QWidget(parent)
  , ui(std::make_unique<Ui::VehicleDvl>())
  , m_action_show_settings(new QAction("&Settings", this))
{
  ui->setupUi(this);
  vehicle_number = 0;

  connect(m_action_show_settings,
          &QAction::triggered,
          this,
          &VehicleDvl::showSettingsDialog);
  offset_validator = new QDoubleValidator(-359.99, 359.99, 2, ui->offset_entry);
  offset_validator->setNotation(QDoubleValidator::StandardNotation);
  ui->offset_entry->setValidator(offset_validator);
  ui->rdi_radio->setChecked(true); // default as selected doppler
  this->doppler_choice = 0;        // default to rdi
  setMouseTracking(true);

  ui->reset_combo->addItem("Cursor", QVariant(QPointF{}));

  connect(
    ui->reset_combo,
    static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
    this,
    &VehicleDvl::handleResetComboChanged);

  connect(this, &VehicleDvl::dvlModeChanged, this, &VehicleDvl::setVehicleDvlMode);
}

VehicleDvl::~VehicleDvl() = default;

void
VehicleDvl::setMapView(QPointer<dslmap::MapView> view)
{

  // parent view
  m_view = view;

  // new cast view contained in this object
  nview = qobject_cast<NavGMapView*>(view);

  if (nview) {
    connect(nview,
            &navg::NavGMapView::cursorDropped,
            this,
            &VehicleDvl::handleSceneMousePress);
  }
}

void
VehicleDvl::connectPlugin(const QObject* plugin, const QJsonObject& metadata)
{
  if (!plugin) {
    qCCritical(plugin_vehicle_dvl, "plugin not created, returning");
    return;
  }

  const auto name =
    metadata.value("MetaData").toObject().value("name").toString();
  if (name.isEmpty()) {
    return;
  }

  if (name == "navest") {

    connect(plugin,
            SIGNAL(messageReceived(NavestMessageType, QVariant)),
            this,
            SLOT(updateFromNavestMessage(NavestMessageType, QVariant)));

    connect(
      plugin,
      SIGNAL(vfrMessageReceived(int, QString, double, double, double, bool)),
      this,
      SLOT(updateFromVfr(int, QString, double, double, double, bool)));

    connect(this,
            SIGNAL(sendNavestMessage(const QByteArray)),
            plugin,
            SLOT(sendMessage(const QByteArray)));

    connect(ui->send_doppler_button,
            &QPushButton::clicked,
            this,
            &VehicleDvl::sendDopplerToUse);
    connect(ui->send_offset_button,
            &QPushButton::clicked,
            this,
            &VehicleDvl::sendDopplerOffset);
    connect(ui->rdi_radio,
            &QRadioButton::clicked,
            this,
            &VehicleDvl::handleDopplerChoice);
    connect(ui->syrinx_radio,
            &QRadioButton::clicked,
            this,
            &VehicleDvl::handleDopplerChoice);

    connect(ui->reset_button,
            &QPushButton::clicked,
            this,
            &VehicleDvl::handleResetButton);
  }
}

void
VehicleDvl::setVehicleDvlMode(int mode){

  // limited mode
  if (mode == 1){

    // hiding dvl buttons
    ui->label->hide();
    ui->offset_entry->hide();
    ui->send_offset_button->hide();
    ui->rdi_radio->hide();
    ui->syrinx_radio->hide();
    ui->send_doppler_button->hide();

    // hiding dvl status labels 
    ui->groupBox_2->hide();
    ui->groupBox_3->hide();
    ui->BeamsLbl->hide();
    ui->BeamsValueLbl->hide();
    ui->TdrLbl->hide();
    ui->TdrValueLbl->hide();
  }

  // full mode
  else if (mode == 0){
    ui->label->show();
    ui->offset_entry->show();
    ui->send_offset_button->show();
    ui->rdi_radio->show();
    ui->syrinx_radio->show();
    ui->send_doppler_button->show();
 
    ui->groupBox_2->show();
    ui->groupBox_3->show();
    ui->BeamsLbl->show();
    ui->BeamsValueLbl->show();
    ui->TdrLbl->show();
    ui->TdrValueLbl->show(); 
  }
}

void
VehicleDvl::loadSettings(QSettings* settings)
{
  auto value = settings->value("vehicle_fix_number");
  auto ok = false;
  if (value.isValid()) {
    const auto valid_val = value.toInt(&ok);
    if (ok) {
      vehicle_number = valid_val;
    }
  }

  value = settings->value("vehicle_dvl_mode");

  ok = false;
  if (value.isValid()) {
    const auto valid_val = value.toInt(&ok);
    if (ok) {
     
      if (valid_val != dvl_mode){
          dvl_mode = valid_val;
          emit dvlModeChanged(dvl_mode);
      }
    }
  }
}

void
VehicleDvl::saveSettings(QSettings& settings)
{
  settings.setValue("persist", 1);
  settings.setValue("vehicle_fix_number", vehicle_number);
  settings.setValue("vehicle_dvl_mode", dvl_mode);
}

void
VehicleDvl::showSettingsDialog()
{

  QScopedPointer<VehicleDvlSettings> dialog(new VehicleDvlSettings);
  dialog->setFixVehicleNumberBox(vehicle_number);
  dialog->setFixVehicleMode(dvl_mode);

  dialog->setModal(true);
  if (!dialog->exec()) {
    return;
  }
  int mode = dialog->getDvlMode();
  if (mode != dvl_mode){
    dvl_mode = mode;
    emit dvlModeChanged(dvl_mode);
  }
  vehicle_number = dialog->getFixVehicleNumber();
}

QList<QAction*>
VehicleDvl::pluginMenuActions()
{
  return { m_action_show_settings };
}

void
VehicleDvl::sendDopplerToUse()
{
  // pass in doppler choice 0 or 1.
  // 0 for RDI, 1 for Syrinx

  char buffer[256];
  snprintf(buffer, 256, "NAVCMD CHOOSEDVL %d", this->doppler_choice);
 
  const int result = QMessageBox::warning(
    this,
    "Reset DVL",
    QString("You are about to send a Doppler choice of %1.  Proceed?").arg(this->doppler_choice),
    QMessageBox::Ok | QMessageBox::Cancel,
    QMessageBox::Cancel);
  if (result == QMessageBox::Ok) {
    emit sendNavestMessage(buffer);
    qCDebug(plugin_vehicle_dvl) << "Sending new doppler choice of:  " << buffer;
  }
}

void
VehicleDvl::sendDvlReset(QPointF fix)
{
  char buffer[256];
  sprintf(buffer, "NAVCMD RSTDVL GEOMAN %.8f %.8f", fix.x(), fix.y());

  qCDebug(plugin_vehicle_dvl) << "Sending DVL reset command:  " << buffer;

  emit sendNavestMessage(buffer);
}

void
VehicleDvl::sendDopplerOffset()
{
  double lower_range = -359.99;
  double upper_range = 359.99;
  char buffer[256];
  int doppler = this->doppler_choice;

  double offset = ui->offset_entry->text().toDouble();

  // some extra bound checking since QDoubleValidator has it's flaws...
  if (!(offset >= lower_range && offset <= upper_range))
    return;

  if (doppler == 0) {
    snprintf(buffer, 256, "NAVCMD RDVLOFF %.2f", offset);
  } else if (doppler == 1) {
    snprintf(buffer, 256, "NAVCMD SDVLOFF %.2f", offset);
  } else
    return;

  const int result = QMessageBox::warning(
    this,
    "Reset DVL",
    QString("You are about to send a DVL offset of %1.  Proceed?").arg(offset),
    QMessageBox::Ok | QMessageBox::Cancel,
    QMessageBox::Cancel);
  if (result == QMessageBox::Ok) {
    emit sendNavestMessage(buffer);
    qCDebug(plugin_vehicle_dvl) << "Sending doppler offset: " << buffer;
  }
}

void
VehicleDvl::handleDopplerChoice(bool is_checked)
{
  if (ui->rdi_radio == (QRadioButton*)sender()) {
    if (is_checked) {
      this->doppler_choice = 0;
    }
  } else if (ui->syrinx_radio == (QRadioButton*)sender()) {
    if (is_checked) {
      this->doppler_choice = 1;
    }
  }
}

void
VehicleDvl::handleResetButton()
{
  QString fixLabel = ui->reset_latlon_label->text();

  QPointF fix = ui->reset_combo->currentData().toPointF();

  if (fix.isNull()) {
    QMessageBox msgBox;
    msgBox.setText("Fix is null. Not sending");
    msgBox.exec();  
    return;
  }

   //Let's not send updates to layers with bad coordinates
   //x lon. y lat
  if (fix.y() > 90 || fix.y() < -90){
    QMessageBox msgBox;
    msgBox.setText("Fix not in lat lon format. Not sending.");
    msgBox.exec();
    return;
  }

  if (fix.x() > 180 || fix.x() < -180){
    QMessageBox msgBox;
    msgBox.setText("Fix not in lat lon format. Not sending.");
    msgBox.exec();
    return;
  }

  if (dvl_mode == 0) {
    const int result = QMessageBox::warning(
      this,
      "Reset DVL",
      QString("You are about to reset the DVL to %1.  Proceed?").arg(fixLabel),
      QMessageBox::Ok | QMessageBox::Cancel,
      QMessageBox::Cancel);
    if (result == QMessageBox::Ok) {
      this->sendDvlReset(fix);
    }
  } else {
    // just send it
    this->sendDvlReset(fix);
  }
}

void
VehicleDvl::updateFromNavestMessage(NavestMessageType type, QVariant message)
{

  if (type == NavestMessageType::TDR) {
    auto timeElapsed = message.value<NavestMessageTdr>().time_since_reset;
    ui->TdrValueLbl->setText(QString::number(timeElapsed, 'f', 1));
  }
  if (type == NavestMessageType::DDD) {
    auto dddMsg = message.value<NavestMessageDdd>();
    ui->r1Lbl->setText(QString::number(dddMsg.beam_ranges[0], 'f', 2));
    ui->r2Lbl->setText(QString::number(dddMsg.beam_ranges[1], 'f', 2));
    ui->r3Lbl->setText(QString::number(dddMsg.beam_ranges[2], 'f', 2));
    ui->r4Lbl->setText(QString::number(dddMsg.beam_ranges[3], 'f', 2));

    ui->v1Lbl->setText(QString::number(dddMsg.beam_velocities[0], 'f', 2));
    ui->v2Lbl->setText(QString::number(dddMsg.beam_velocities[1], 'f', 2));
    ui->v3Lbl->setText(QString::number(dddMsg.beam_velocities[2], 'f', 2));
    ui->v4Lbl->setText(QString::number(dddMsg.beam_velocities[3], 'f', 2));

    ui->BeamsValueLbl->setText(QString::number(dddMsg.bt_beams, 'f', 2));
    ui->BeamsAltValueLbl->setText(QString::number(dddMsg.alt, 'f', 2));
    ui->BeamsHdgValueLbl->setText(QString::number(dddMsg.heading, 'f', 2));
  }
}

void 
VehicleDvl::updateFromVfr(int vehicle_number, QString fix_source_in, double lon_in,
                          double lat_in, double depth, bool is_reset_source){
    
    if (!is_reset_source) {
      //qDebug(plugin_vehicle_dvl) << "VFR fix for " << fix_source_in << " is not an external source";
      return;
    }
    //qDebug(plugin_vehicle_dvl) << "VFR fix for " << fix_source_in << " is na external source";
    
    QString fix_source = fix_source_in;

    int vehicle_id = vehicle_number;
    if (vehicle_id != vehicle_number) {
      return;
    }

    // See if combo item already exists
    int current_index = 0;
    bool found = false;
    int reset_size = ui->reset_combo->count();
    for (int i = 0; i < reset_size; i++) {
      if (ui->reset_combo->itemText(i) == fix_source) {
        current_index = i;
        found = true;
        break;
      }
    }

    double lat = lat_in;
    double lon = lon_in;

    if (!found) {
      // lets just add a new combo item then
      ui->reset_combo->addItem(fix_source, QPointF(lon, lat));
      current_index = ui->reset_combo->count();
    }

    else {

      ui->reset_combo->setItemData(current_index, QPointF(lon, lat));
    }

    if (ui->reset_combo->currentIndex() == current_index) {

      // we are setting last fix in the qcombo item itself. Since fix
      // created here, we dont directly need to pull that data back and can just
      // use our lat lon vars
      ui->reset_latlon_label->setText(QString::number(lon) + " " +
                                      QString::number(lat));
    }
  
}

void
VehicleDvl::handleResetComboChanged(int index)
{
  QPointF data = ui->reset_combo->itemData(index).value<QPointF>();
  auto x = data.x();
  auto y = data.y();
  ui->reset_latlon_label->setText(QString::number(x) + " " +
                                  QString::number(y));
}

// slot for receiving mapview key press

void
VehicleDvl::handleSceneMousePress(QPointF cursor)
{
  if (nview) {

    auto map_scene = m_view->mapScene();

    if (!map_scene) {

      return;
    }
    auto cursor = nview->getCurrentPosition();
    double lon = cursor.x();
    double lat = cursor.y();
    auto proj = map_scene->projection();
    if (!proj) {
    return;
    }
    proj->transformToLonLat(1, &lon, &lat);

    auto fix = QPointF(lon, lat);

    //cursor always position 0 in combobox
    ui->reset_combo->setItemData(0, fix);
    if (ui->reset_combo->currentIndex() == 0) {
      ui->reset_latlon_label->setText(QString::number(fix.x()) + " " +
                                      QString::number(fix.y()));
    }
  }
}

} // vehicle_dvl
} // plugins
} // navg
