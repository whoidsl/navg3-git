/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef NAVG_VEHICLE_DVL_H
#define NAVG_VEHICLE_DVL_H

#include <navg/CorePluginInterface.h>

#include <QAction>
#include <QDoubleValidator>
#include <QHash>
#include <QHostAddress>
#include <QJsonObject>
#include <QLoggingCategory>
#include <QObject>
#include <QTime>
#include <QWidget>
#include <QtGlobal>
#include <QMessageBox>
#include <memory>
#include <QMouseEvent>
#include <../../navest/include/navest/Messages.h>
#include <../../navest/include/navest/Navest.h>
#include "VehicleDvlSettings.h"

namespace navg {
namespace plugins {
namespace vehicle_dvl {

namespace Ui {
class VehicleDvl;
}

class VehicleDvl : public QWidget, public CorePluginInterface {
  Q_OBJECT
  Q_INTERFACES(navg::CorePluginInterface)
  Q_PLUGIN_METADATA(IID CorePluginInterface_iid FILE "VehicleDvl.json")

signals:

public:
  explicit VehicleDvl(QWidget *parent = nullptr);
  ~VehicleDvl() override;

  //
  // CorePluginInterface Implementation
  //
  void loadSettings(QSettings *settings) override;
  void saveSettings(QSettings &settings) override;
  
  //connect to navest plugin
  void connectPlugin(const QObject *plugin,
                     const QJsonObject &metadata) override;

  //receive mapview reference. Actually a navg::NavGMapView object.
  void setMapView(QPointer<dslmap::MapView> view) override;
  QList<QAction *> pluginMenuActions() override;
  QList<QAction *> pluginToolbarActions() override { return {}; }


public slots:
  /// \brief Show the settings dialog
  ///
  void showSettingsDialog();
  void handleSceneMousePress(QPointF cursor);
 
protected slots:
  void updateFromNavestMessage(NavestMessageType type, QVariant message);
  void updateFromVfr(int vehicle_number, QString fix_source_in, double lon_in,
                          double lat_in, double depth, bool is_reset_source);
  void sendDopplerToUse();
  void sendDopplerOffset();
  void sendDvlReset(QPointF fix);
  void setNavestRemoteAddress(QHostAddress address);
  void handleDopplerChoice(bool is_checked);
  void handleResetButton();
  void handleLastFixButton();
  void handleResetComboChanged(int index);
  void setVehicleDvlMode(int mode);

signals:
  void sendNavestMessage(const QByteArray &message);
  void dvlModeChanged(int);

private:
  NavGMapView *nview;
  int doppler_choice; // 0 for rdi, 1 for syrinx. defaults at 0 on creation.
                      // manipulated by radio buttons
  int vehicle_number = 0;

  // 1 is alvin. 0 is jason
  int dvl_mode = 0;
  QDoubleValidator *offset_validator;
  std::unique_ptr<Ui::VehicleDvl> ui;
  QAction* m_action_show_settings = nullptr;
  QPointer<dslmap::MapView> m_view;
  QHostAddress m_navest_remote_address;

};

Q_DECLARE_LOGGING_CATEGORY(plugin_vehicle_dvl)
} // jason
} // plugins
} // navg

#endif // NAVG_VEHICLE_DVL_H
