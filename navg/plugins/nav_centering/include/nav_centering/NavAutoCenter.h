/**
 * Copyright 2020 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// created by tmjoyce
//

#ifndef NAVG_NAVAUTOCENTER_H
#define NAVG_NAVAUTOCENTER_H

#include <../navest/Navest.h>
#include <../navest/NavestLayer.h>
#include <QAction>
#include <QHash>
#include <QHostAddress>
#include <QJsonObject>
#include <QLineF>
#include <QLoggingCategory>
#include <QObject>
#include <QTableView>
#include <QTime>
#include <QUuid>
#include <QWidget>
#include <dslmap/MapItem.h>
#include <dslmap/MapLayer.h>
#include <dslmap/MapScene.h>
#include <dslmap/Proj.h>
#include <math.h>
#include <memory>
#include <navg/CorePluginInterface.h>

namespace navg {
namespace plugins {
namespace nav_autocenter {

namespace Ui {
class NavAutoCenter;
}

class NavAutoCenter
  : public QWidget
  , public CorePluginInterface
{
  Q_OBJECT
  Q_INTERFACES(navg::CorePluginInterface)
  Q_PLUGIN_METADATA(IID CorePluginInterface_iid FILE "NavAutoCenter.json")

signals:
  void layerQuantityChanged();
  void layerMoved(dslmap::MapLayer*);

public:
  explicit NavAutoCenter(QWidget* parent = nullptr);
  ~NavAutoCenter() override;

  using LayerHash = QHash<QUuid, dslmap::MapLayer*>;

  //
  // CorePluginInterface Implementation
  //
  void loadSettings(QSettings* settings) override;
  void saveSettings(QSettings& settings) override;

  void connectPlugin(const QObject* plugin,
                     const QJsonObject& metadata) override;
  void setMapView(QPointer<dslmap::MapView> view) override;
  QList<QAction*> pluginMenuActions() override;
  QList<QAction*> pluginToolbarActions() override { return {}; }
  dslmap::MapLayer* getCurrentSelectedLayer();

protected slots:

  void sceneLayerAdded(dslmap::MapLayer* layer);
  void sceneLayerRemoved(dslmap::MapLayer* layer);

  void receivePositionFix(const QPointF fix);
  void comboIndexChanged(int index);
  void centerCheckChanged(int state);
  void handleToggle(bool state);
  void zoomToLayer(dslmap::MapLayer*);
  void zoomTo();

  // this is what binds a layer to  signal so we can center on it periodically
  void bindLayer(dslmap::MapLayer* layer);
  void unbindLayer(dslmap::MapLayer* layer);

private:
  std::unique_ptr<Ui::NavAutoCenter> ui;
  int m_seconds_since;
  QPointer<dslmap::MapView> m_view;
  void setupLayerBox();
  bool persist;
};

Q_DECLARE_LOGGING_CATEGORY(plugin_nav_manager)

} // nav_autocenter
} // plugins
} // navg

#endif // NAVG_NAVAUTOCENTER_H
