/**
 * Copyright 2020 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// created by tmjoyce
//

#include "NavAutoCenter.h"
#include "ui_NavAutoCenter.h"

#include <dslmap/MapView.h>

#include <QAction>
#include <QSettings>
#include <QSortFilterProxyModel>
#include <QStandardItemModel>
#include <QTimer>
#include <QUdpSocket>

namespace navg {
namespace plugins {
namespace nav_autocenter {

Q_LOGGING_CATEGORY(plugin_autocenter, "navg.plugins.autocenter")

NavAutoCenter::NavAutoCenter(QWidget* parent)
  : QWidget(parent)
  , ui(std::make_unique<Ui::NavAutoCenter>())

{
  ui->setupUi(this);
  ui->zoom_button->setChecked(false);
  handleToggle(false);
}
NavAutoCenter::~NavAutoCenter() = default;

void
NavAutoCenter::setMapView(QPointer<dslmap::MapView> view)
{
  m_view = view;
  // connect layer add and remove hooks to combo box

  auto scene = m_view->mapScene();

  setupLayerBox();
  connect(scene,
          static_cast<void (dslmap::MapScene::*)(dslmap::MapLayer * layer)>(
            &dslmap::MapScene::layerAdded),
          this,
          &NavAutoCenter::sceneLayerAdded);

  connect(scene,
          static_cast<void (dslmap::MapScene::*)(dslmap::MapLayer * layer)>(
            &dslmap::MapScene::layerRemoved),
          this,
          &NavAutoCenter::sceneLayerRemoved);

  connect(ui->zoom_button,
          &QPushButton::clicked,
          this,
          &NavAutoCenter::centerCheckChanged);

  connect(ui->zoom_button,
          &QPushButton::toggled,
          this,
          &NavAutoCenter::handleToggle);


  connect(
    ui->layer_combo,
    static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
    this,
    &NavAutoCenter::comboIndexChanged);

}

void
NavAutoCenter::connectPlugin(const QObject* plugin, const QJsonObject& metadata)
{
  if (!plugin) {
    return;
  }

  const auto name =
    metadata.value("MetaData").toObject().value("name").toString();
  if (name.isEmpty()) {
    return;
  }
}

void
NavAutoCenter::loadSettings(QSettings* settings)
{}

void
NavAutoCenter::saveSettings(QSettings& settings)
{
  settings.setValue("persist", 1);
}

QList<QAction*>
NavAutoCenter::pluginMenuActions()
{
  return {};
}

void
NavAutoCenter::setupLayerBox()
{

  auto scene = m_view->mapScene();
  if (!scene) {
    qCInfo(plugin_autocenter) << "no scene";
    return;
  }
  LayerHash nav_solution_layers =
    scene->layers(dslmap::MapItemType::NavestLayerType);
  if (nav_solution_layers.empty()) {
    qCInfo(plugin_autocenter) << "layer list is empty";
    return;
  }
  QHash<QUuid, dslmap::MapLayer*>::iterator i;
  for (i = nav_solution_layers.begin(); i != nav_solution_layers.end(); ++i) {
    // Sets text to layer name. And stores value of unique Layer id
    ui->layer_combo->addItem(i.value()->name(), i.key());
  }
}

dslmap::MapLayer*
NavAutoCenter::getCurrentSelectedLayer()
{

  if (ui->layer_combo->count() <= 0) {
    return nullptr;
  }
  auto scene = m_view->mapScene();
  if (!scene) {
    return nullptr;
  }
  LayerHash nav_solution_layers =
    scene->layers(dslmap::MapItemType::NavestLayerType);

  QHash<QUuid, dslmap::MapLayer*>::iterator i;
  for (i = nav_solution_layers.begin(); i != nav_solution_layers.end(); ++i) {
    if (i.key() == ui->layer_combo->currentData(Qt::UserRole).toUuid()) {
      // returning the maplayer
      return i.value();
    }
  }
  return nullptr;
}

void
NavAutoCenter::centerCheckChanged(int state)
{
  auto layer = getCurrentSelectedLayer();
  if (!layer) {
    return;
  }
  if (state > 0) {
    // zoom to the layer directly. Fixes may not come in yet
    zoomToLayer(layer);
    bindLayer(layer);

  } else {
    unbindLayer(layer);
  }
}

void 
NavAutoCenter::handleToggle(bool state){
  if (state > 0){
    ui->zoom_button->setStyleSheet(
        "QPushButton { border:1px solid green} ");
    ui->zoom_button->setText("AutoCenter: On");
  }

  else {
    ui->zoom_button->setStyleSheet(
        "QPushButton { border:1px solid red} ");
        ui->zoom_button->setText("AutoCenter: Off");
  }
}

void
NavAutoCenter::bindLayer(dslmap::MapLayer* layer)
{
  if (!layer) {
    qCInfo(plugin_autocenter) << "no layer found";
  }

  if (layer->type() == dslmap::MapItemType::NavestLayerType) {

    navg::plugins::navest::NavestLayer* nav_layer =
      dynamic_cast<navg::plugins::navest::NavestLayer*>(layer);
    if (!nav_layer) {
      qCInfo(plugin_autocenter) << "no navestlayer found";
      return;
    }
    connect(nav_layer,
            &navg::plugins::navest::NavestLayer::currentPositionChanged,
            this,
            &NavAutoCenter::receivePositionFix);
  }
  // Add other layer types to support here
}

void
NavAutoCenter::unbindLayer(dslmap::MapLayer* layer)
{
  if (!layer) {
    return;
  }
  if (layer->type() == dslmap::MapItemType::NavestLayerType) {

    navg::plugins::navest::NavestLayer* nav_layer =
      dynamic_cast<navg::plugins::navest::NavestLayer*>(layer);
    if (!nav_layer) {
      qCInfo(plugin_autocenter) << "no navestlayer found";
      return;
    }
    disconnect(nav_layer,
               &navg::plugins::navest::NavestLayer::currentPositionChanged,
               this,
               &NavAutoCenter::receivePositionFix);
  }
}

void
NavAutoCenter::zoomTo()
{
  auto layer = getCurrentSelectedLayer();
  if (!layer) {
    return;
  }
  zoomToLayer(layer);
}

void
NavAutoCenter::zoomToLayer(dslmap::MapLayer* layer)
{
  auto zoom_level = m_view->zoomLevel();
  // m_view->zoomToRect(layer->boundingRect());
  m_view->zoom(layer->boundingRect().center(), zoom_level, false);
}

void
NavAutoCenter::receivePositionFix(const QPointF fix)
{
  // get sender of fix so we know which layer sent it.
  dslmap::MapLayer* layer = qobject_cast<dslmap::MapLayer*>(sender());
  if (layer == nullptr) {
    qCInfo(plugin_autocenter) << "error sender is null";
    return;
  }
  zoomToLayer(layer);
}

void
NavAutoCenter::comboIndexChanged(int index)
{
  ui->zoom_button->setChecked(false);
  auto scene = m_view->mapScene();

  // remove all connections we may have added
  int combo_size = ui->layer_combo->count();
  for (int i = 0; i < combo_size; i++) {
    auto id = ui->layer_combo->itemData(i).toUuid();

    if (scene->layer(id)) {

      unbindLayer(scene->layer(id));
    }
  }
}

void
NavAutoCenter::sceneLayerAdded(dslmap::MapLayer* layer)
{
  // will need to make this more dynamic as well to allow for other types
  // Ex: if layer->type is in #VALIDTYPES

  if (layer->type() != dslmap::MapItemType::NavestLayerType) {
    return;
  }

  navg::plugins::navest::NavestLayer* curr_nav_layer =
    dynamic_cast<navg::plugins::navest::NavestLayer*>(layer);
  if (!curr_nav_layer) {
    return;
  }

  if (layer) {
    qDebug() << "Adding item to layer combo with name " << layer->name();
    ui->layer_combo->addItem(layer->name(), layer->id());
  }
}
void
NavAutoCenter::sceneLayerRemoved(dslmap::MapLayer* layer)
{
  layer->disconnect();
  if (layer->type() != dslmap::MapItemType::NavestLayerType) {
    return;
  }
  navg::plugins::navest::NavestLayer* curr_nav_layer =
    dynamic_cast<navg::plugins::navest::NavestLayer*>(layer);
  if (!curr_nav_layer) {
    return;
  }
  QUuid layer_id = layer->id();
  int combo_size = ui->layer_combo->count();
  for (int i = 0; i < combo_size; i++) {
    if (ui->layer_combo->itemData(i).toUuid() == layer_id) {
      ui->layer_combo->removeItem(i);
    }
  }
}

} // navauto
} // plugins
} // navg
