/**
 * Copyright 2021 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// Created by tmjoyce 08/25/21.
//

#include "TargetReceiver.h"
#include <NavGMapScene.h>
#include <QMessageBox>
#include <QSettings>
#include <dslmap/Proj.h>

namespace navg {
namespace plugins {
namespace target_receiver {

Q_LOGGING_CATEGORY(plugin_target_receiver, "navg.plugins.target_receiver")

TargetReceiver::TargetReceiver(QWidget* parent)
  : QWidget(parent)
  , m_action_show_settings(new QAction("&Settings", this))
{

  socket = new QUdpSocket(this);

  in_socket_port = 0;

  connect(m_action_show_settings,
          &QAction::triggered,
          this,
          &TargetReceiver::showSettingsDialog);

  QObject::connect(socket,
                   SIGNAL(readyRead()),
                   this,
                   SLOT(dataPending()),
                   Qt::UniqueConnection);
}

TargetReceiver::~TargetReceiver()
{
  delete socket;
}

void
TargetReceiver::setMapView(QPointer<dslmap::MapView> view)
{
  m_view = view;
  n_view = qobject_cast<NavGMapView*>(view);
}

void
TargetReceiver::connectPlugin(const QObject* plugin,
                              const QJsonObject& metadata)
{
  if (!plugin) {
    return;
  }

  const auto name =
    metadata.value("MetaData").toObject().value("name").toString();
  if (name.isEmpty()) {
    return;
  }
}

void
TargetReceiver::loadSettings(QSettings* settings)
{

  auto value = settings->value("in_port");
  auto ok = false;
  if (value.isValid()) {
    const auto valid_val = value.toInt(&ok);
    if (ok) {
      in_socket_port = valid_val;
    }
  }

  if (!socket->bind(in_socket_port,
                    QUdpSocket::ShareAddress | QUdpSocket::ReuseAddressHint)) {
    qDebug(plugin_target_receiver) << "could not bind to socket";
  } else {
    QObject::connect(socket,
                     SIGNAL(readyRead()),
                     this,
                     SLOT(dataPending()),
                     Qt::UniqueConnection);
  }
}

void
TargetReceiver::showSettingsDialog()
{

  QScopedPointer<ReceiverSettings> dialog(new ReceiverSettings);
  qDebug(plugin_target_receiver) << "port value we are setting to dialog is " << in_socket_port;
  dialog->setPort(in_socket_port);

  dialog->setModal(true);
  if (!dialog->exec()) {
    return;
  }

  int temp = dialog->getPort();
  if (in_socket_port != temp) {
    in_socket_port = temp;
    socket->close();
    if (!socket->bind(in_socket_port,
                      QUdpSocket::ShareAddress |
                        QUdpSocket::ReuseAddressHint)) {
      qDebug(plugin_target_receiver) << "could not bind to socket";
    } else {
      QObject::connect(socket,
                       SIGNAL(readyRead()),
                       this,
                       SLOT(dataPending()),
                       Qt::UniqueConnection);
    }
  }
  qDebug(plugin_target_receiver) << "port value received from dialog is " << in_socket_port;
}

void
TargetReceiver::saveSettings(QSettings& settings)
{
  settings.setValue("persist", 1);
  settings.setValue("in_port", in_socket_port);
}

QList<QAction*>
TargetReceiver::pluginMenuActions()
{
  return { m_action_show_settings };
}

void
TargetReceiver::dataPending()
{
  QUdpSocket* theSocket = (QUdpSocket*)sender();

  while (theSocket->hasPendingDatagrams()) {

    QByteArray buffer(theSocket->pendingDatagramSize(), 0);
    qint64 bytesRead = theSocket->readDatagram(buffer.data(), buffer.size());
    qDebug(plugin_target_receiver) << "Bytes read: " << bytesRead;

    QJsonDocument itemDoc = QJsonDocument::fromJson(buffer);
    QJsonObject obj = itemDoc.object();
    QString type = obj.value("type").toVariant().toString();
    if (type != "target"){
      return;
    }
    double lon = obj.value("lon").toVariant().toDouble();
    double lat = obj.value("lat").toVariant().toDouble();
    QString name = obj.value("name").toVariant().toString();

    qDebug(plugin_target_receiver)
      << "Received message\nlon:" << QString::number(lon, 'f', 8)
      << "\nlat:" << QString::number(lat, 'f', 8) << "\nName:" << name << "\nType: " << type;

    drawTarget(lon, lat, name);
  }
}

void
TargetReceiver::drawTarget(double lon, double lat, QString name)
{

  auto my_scene = m_view->mapScene();
  if (!my_scene)
    return;

  auto pos = QPointF(lon, lat);

  if (my_scene->projection()) {
    double x_val(pos.x());
    double y_val(pos.y());
    my_scene->projection()->transformFromLonLat(1, &x_val, &y_val);
    pos.setX(x_val);
    pos.setY(y_val);
  }

  auto label = name;

  if (!layer) {
    qDebug(plugin_target_receiver) << "layer doesnt exist yet...create";

    // CREATE NEW SYMBOL LAYER
    auto layer_name = "Remote Target";
    layer = new dslmap::SymbolLayer();
    layer->setName(layer_name);
    // Create a red rectangle symbol to draw the points
    QPainterPath path;
    layer->setShape(dslmap::ShapeMarkerSymbol::Shape::X, 15);
    auto symbol_ptr = layer->symbolPtr();
    symbol_ptr->setOutlineColor(Qt::red);
    symbol_ptr->setFillColor(Qt::red);
    symbol_ptr->setLabelColor(Qt::red);

    // Draw the markers scale invariant.
    layer->setScaleInvariant(true);
    if (my_scene->projection()) {
      layer->setProjection(my_scene->projection());
    }
    item = layer->addPoint(pos, label, {});
    item->setFlag(QGraphicsItem::ItemIsMovable, false);
    layer->setLabelVisible(true);
    layer->setVisible(true);
    my_scene->addLayer(layer);

  } else {
    qDebug(plugin_target_receiver)
      << "layer does exist. removing old point and adding new";
    // clear the old point
    if (item) {
      layer->removePoint(item);
    }
    // special case if the user deleted the layer...
    if (my_scene->layer(layer->id()) == nullptr) {
      my_scene->addLayer(layer);
    }
    item = layer->addPoint(pos, label, {});
    item->setFlag(QGraphicsItem::ItemIsMovable, true);
  }
}

} // target_receiver
} // plugins
} // navg
