cmake_minimum_required(VERSION 3.5)
project(navg_target_receiver LANGUAGES CXX VERSION 1.0.0)

add_subdirectory(src)

# if (BUILD_TESTS)
#   add_subdirectory(tests)
# endif (BUILD_TESTS)
