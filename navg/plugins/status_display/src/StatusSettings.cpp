/**
 * Copyright 2019 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "StatusSettings.h"
#include "ui_StatusSettings.h"

namespace navg {
namespace plugins {
namespace status_display {

StatusSettings::StatusSettings(QWidget* parent)
  : QDialog(parent)
 , ui(std::make_unique<Ui::StatusSettings>())
{
  ui->setupUi(this);
}

StatusSettings::~StatusSettings() = default;

void
StatusSettings::setFixVehicleNumberBox(int num)
{
  ui->fix_vehicle_spin->setValue(num);
}

int
StatusSettings::getFixVehicleNumber()
{
  return ui->fix_vehicle_spin->value();
}

void
StatusSettings::setCurrentSolution(QString soln_name)
{
  ui->fix_vehicle_soln->setText(soln_name);
}

QString
StatusSettings::getCurrentSolution()
{
  return ui->fix_vehicle_soln->text();
}

void
StatusSettings::setAuxStatusPortBox(int num)
{
  ui->aux_port_spin->setValue(num);
}

int
StatusSettings::getAuxStatusPort()
{
  return ui->aux_port_spin->value();
}

void
StatusSettings::setStaleHeadingWarningIsEnabled(bool is_enabled)
{
  ui->enable_warn->setChecked(is_enabled);
}

bool
StatusSettings::getStaleHeadingWarningIsEnabled()
{
  return ui->enable_warn->isChecked();
}

void
StatusSettings::setStaleHeadingTimeoutSecs(int secs)
{
  ui->warn_spin->setValue(secs);
}

int
StatusSettings::getStaleHeadingTimeoutSecs()
{
  return ui->warn_spin->value();
}
} // namespace status_display
} // namespace plugins
} // namespace navg
