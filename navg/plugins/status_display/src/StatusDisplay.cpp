/**
 * Copyright 2021 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// Created by tmjoyce 06/21/21.
//

#include "StatusDisplay.h"
#include "ui_StatusDisplay.h"
#include <QDebug>
#include <QSettings>
#include <QTimer>
#include <iostream>

#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>

using namespace navg::plugins::navsol_manager;

namespace navg {
namespace plugins {
namespace status_display {

Q_LOGGING_CATEGORY(plugin_status_display, "navg.plugins.status_display")

StatusDisplay::StatusDisplay(QWidget* parent)
  : QWidget(parent)
  , ui(std::make_unique<Ui::StatusDisplay>())
  , m_action_show_settings(new QAction("&Settings", this))
{
  ui->setupUi(this);

  socket = new QUdpSocket(this);

  vehicle_number = 0;
  aux_status_port = 0;
  current_soln = "SOLN_DEADRECK";
  isStaleHeadingWarningEnabled = true;
  staleHeadingTimeoutSecs = STALE_HEADING_DEFAULT_SEC;

  connect(m_action_show_settings,
          &QAction::triggered,
          this,
          &StatusDisplay::showSettingsDialog);

  QObject::connect(socket,
                   SIGNAL(readyRead()),
                   this,
                   SLOT(dataPending()),
                   Qt::UniqueConnection);

  headingUpdateTimer = new QTimer(this);
  headingUpdateTimer->setInterval(staleHeadingTimeoutSecs * 1000);
  headingUpdateTimer->setSingleShot(true);
  connect(headingUpdateTimer, SIGNAL(timeout()), this, SLOT(headingUpdateTimedOut()));

  isHeadingRepeating = false;
  isReceivingVPRs = true;
  prevHeadingCentiDegrees = 0;
  timeHeadingLastChanged = QTime::currentTime();
}

StatusDisplay::~StatusDisplay()
{
  delete socket;
}

void
StatusDisplay::setMapView(QPointer<dslmap::MapView> view)
{
  m_view = view;
  n_view = qobject_cast<NavGMapView*>(view);
}

void
StatusDisplay::connectPlugin(const QObject* plugin, const QJsonObject& metadata)
{
  if (!plugin) {
    return;
  }

  const auto name =
    metadata.value("MetaData").toObject().value("name").toString();
  if (name.isEmpty()) {
    return;
  }

  if (name == "navest") {

    if (!connect(plugin,
                 SIGNAL(messageReceived(NavestMessageType, QVariant)),
                 this,
                 SLOT(updateFromNavestMessage(NavestMessageType, QVariant)))) {
      qCCritical(plugin_status_display,
                 "Unable to connect to signal: "
                 "Navest::messageReceived(NavestMessageType, "
                 "QVariant)");
    } else {
      qCInfo(plugin_status_display, "Connected to Navest::messageReceived");
    }

    connect(plugin,
            SIGNAL(navestSolutionChanged(QString)),
            this,
            SLOT(selectedSolutionChanged(QString)));
  }

  if (name == "Nav_Solution_Manager") {

    // this is messy, but it's the simplest way to share the coord converter
    // from navsol manager with this plugin
    if (connect(this,
                SIGNAL(requestCoordConversion(QPointF)),
                plugin,
                SLOT(receiveOutsideConversion(QPointF))) &&
        connect(plugin,
                SIGNAL(sendFix(QPair<QString, QString>)),
                this,
                SLOT(updateXYVPR(QPair<QString, QString>)))) {

      nav_connected = true;
    } else {
      nav_connected = false;
      qCCritical(plugin_status_display) << "Unable to connect to Nav Sol";
    }
    connect(
      plugin, SIGNAL(coordDisplayChanged()), this, SLOT(handleConversion()));
  }
}

void
StatusDisplay::handleConversion()
{
  emit requestCoordConversion(QPointF(current_vpr_lon, current_vpr_lat));
}

void
StatusDisplay::loadSettings(QSettings* settings)
{
  auto value = settings->value("vehicle_fix_number");
  auto ok = false;
  if (value.isValid()) {
    const auto valid_val = value.toInt(&ok);
    if (ok) {
      vehicle_number = valid_val;
    }
  }

  auto soln_value = settings->value("vehicle_soln");
  if (soln_value.isValid()) {
    const auto valid_soln_value = soln_value.toString();
    if (!valid_soln_value.isEmpty()) {
      current_soln = valid_soln_value;
    }
  }

  auto port_value = settings->value("aux_status_port");
  auto port_ok = false;
  if (port_value.isValid()) {
    const auto valid_val = port_value.toInt(&port_ok);
    if (port_ok) {
      aux_status_port = valid_val;
    }
  }

  auto enabled_value = settings->value("is_stale_heading_warning_enabled");
  if (enabled_value.isValid()) {
    isStaleHeadingWarningEnabled = enabled_value.toBool();
  }

  auto timeout_value = settings->value("stale_heading_timeout_secs");
  auto timeout_ok = false;
  if (timeout_value.isValid()) {
    const auto valid_val = timeout_value.toInt(&timeout_ok);
    if (timeout_ok) {
      staleHeadingTimeoutSecs = valid_val;
      headingUpdateTimer->setInterval(staleHeadingTimeoutSecs * 1000);
    }
  }

  if (!socket->bind(aux_status_port,
                    QUdpSocket::ShareAddress | QUdpSocket::ReuseAddressHint)) {
    qDebug(plugin_status_display) << "could not bind to socket";
  } else {
    QObject::connect(socket,
                     SIGNAL(readyRead()),
                     this,
                     SLOT(dataPending()),
                     Qt::UniqueConnection);
  }
}

void
StatusDisplay::saveSettings(QSettings& settings)
{
  settings.setValue("persist", 1);
  settings.setValue("vehicle_fix_number", vehicle_number);
  settings.setValue("vehicle_soln", current_soln);
  settings.setValue("aux_status_port", aux_status_port);
  settings.setValue("is_stale_heading_warning_enabled", isStaleHeadingWarningEnabled);
  settings.setValue("stale_heading_timeout_secs", staleHeadingTimeoutSecs);
}

void
StatusDisplay::showSettingsDialog()
{

  QScopedPointer<StatusSettings> dialog(new StatusSettings);
  dialog->setFixVehicleNumberBox(vehicle_number);
  dialog->setAuxStatusPortBox(aux_status_port);
  dialog->setCurrentSolution(current_soln);
  dialog->setStaleHeadingWarningIsEnabled(isStaleHeadingWarningEnabled);
  dialog->setStaleHeadingTimeoutSecs(staleHeadingTimeoutSecs);

  dialog->setModal(true);
  if (!dialog->exec()) {
    return;
  }
  vehicle_number = dialog->getFixVehicleNumber();
  current_soln = dialog->getCurrentSolution();
  int aux_status_port_temp = dialog->getAuxStatusPort();

  isStaleHeadingWarningEnabled = dialog->getStaleHeadingWarningIsEnabled();
  updateHeadingDisplayStyle();
  staleHeadingTimeoutSecs = dialog->getStaleHeadingTimeoutSecs();
  headingUpdateTimer->setInterval(staleHeadingTimeoutSecs * 1000);

  if (aux_status_port != aux_status_port_temp) {
    aux_status_port = aux_status_port_temp;
    socket->close();
    if (!socket->bind(aux_status_port,
                      QUdpSocket::ShareAddress |
                        QUdpSocket::ReuseAddressHint)) {
      qDebug(plugin_status_display) << "could not bind to socket";
    } else {
      QObject::connect(socket,
                       SIGNAL(readyRead()),
                       this,
                       SLOT(dataPending()),
                       Qt::UniqueConnection);
    }
  }
  qDebug(plugin_status_display)
    << "port value received from dialog is " << aux_status_port;
}

QList<QAction*>
StatusDisplay::pluginMenuActions()
{
  return { m_action_show_settings };
}

void
StatusDisplay::dataPending()
{
  // NOTE: This only works if ship GPS and HDG (GPVTG/HEHDT) are sent to the aux_status_port specified in settings
  QUdpSocket* theSocket = (QUdpSocket*)sender();
  while (theSocket->hasPendingDatagrams()) {

    QByteArray buffer(theSocket->pendingDatagramSize(), 0);
    theSocket->readDatagram(buffer.data(), buffer.size());
    //qDebug(plugin_status_display) << "Bytes read: " << bytesRead;
    std::string bufstr(buffer.data());
    //qDebug(plugin_status_display) << bufstr.c_str();
    std::size_t vtg_found = bufstr.find("VTG");
    std::size_t hdt_found = bufstr.find("HDT");
    if (vtg_found != std::string::npos) {
      auto fields = std::vector<std::string>{};
      double ship_sog_val;
      boost::split(
        fields, bufstr, boost::is_any_of(",*"), boost::token_compress_off);

      // Expect at LEAST 10 fields.
      if (fields.size() <= 10) {
        qDebug(plugin_status_display) << "Wrong number of fields";
      }

      if (!fields.at(6).compare("N")) {
        sscanf(fields.at(5).c_str(), "%lf", &ship_sog_val);
        //qDebug(plugin_status_display) << "ship sog is " << ship_sog_val;
        ui->sog_label->setText(QString::number(ship_sog_val) + " knots");
      } else {
        ui->sog_label->setText("unknown");
      }
    }
    if (hdt_found != std::string::npos) {
      auto fields = std::vector<std::string>{};
      double ship_hdg_val;
      boost::split(
        fields, bufstr, boost::is_any_of(",*"), boost::token_compress_off);

      // Expect at LEAST 2 fields.
      if (fields.size() <= 2) {
        qDebug(plugin_status_display) << "Wrong number of fields";
      }

      if (!fields.at(2).compare("T")) {
        sscanf(fields.at(1).c_str(), "%lf", &ship_hdg_val);
        //qDebug(plugin_status_display) << "ship hdg is " << ship_hdg_val;
        ui->ship_hdg_label->setText(QString::number(ship_hdg_val) + " T");
      } else {
        ui->ship_hdg_label->setText("unknown");
      }
    }
  }
}

void
StatusDisplay::updateFromNavestMessage(NavestMessageType type, QVariant message)
{

  if (type == navg::NavestMessageType::VFR) {

    auto vfr_message = message.value<NavestMessageVfr>();

    // only want altitude from deadreck solution
    if (QString::compare(vfr_message.fix_source, alt_solution) == 0) {
      if (vfr_message.alt > 0) {
        ui->alt_label->setText(QString::number(vfr_message.alt) + "m");
      } else {
        ui->alt_label->setText("unknown");
      }
    }

    // only get fixes from selected solution type
    //if (QString::compare(vfr_message.fix_source, getCurrentSolution()) == 0){

      // check if it is a fix from the "correct" vehicle id number
      if (vfr_message.vehicle_number == vehicle_number) {

        current_vfr_lon = vfr_message.lon;
        current_vfr_lat = vfr_message.lat;
        if (nav_connected) {
          // this is basically pinging navsol to give us a converted fix back
          emit requestCoordConversion(
            QPointF(vfr_message.lon, vfr_message.lat));
        }

        else {

          updateXYVPR(
            QPair<QString, QString>(QString::number(current_vfr_lon, 'f', 6),
                                    QString::number(current_vfr_lat, 'f', 6)));
        }
      }
    //}    
    }

  else if (type == navg::NavestMessageType::VPR) {
    auto vpr_message = message.value<NavestMessageVpr>();
    if (vpr_message.vehicle_number == vehicle_number) {

      updateEphemeris(-vpr_message.depth, vpr_message.heading);
      received_vpr = true;

      int headingCentiDegrees = (int) (vpr_message.heading * 100);

      if (headingCentiDegrees != prevHeadingCentiDegrees)
      {
        timeHeadingLastChanged = QTime::currentTime();

        if (isHeadingRepeating)
        {
          isHeadingRepeating = false;
          qDebug() << "Heading resumed changing";
          updateHeadingDisplayStyle();
        }
      }

      if (timeHeadingLastChanged.msecsTo(QTime::currentTime()) > staleHeadingTimeoutSecs * 1000)
      {
        if (!isHeadingRepeating)
        {
          isHeadingRepeating = true;
          qDebug() << "Heading has been repeating";
          updateHeadingDisplayStyle();
        }
      }

      prevHeadingCentiDegrees = headingCentiDegrees;

      if (!isReceivingVPRs)
      {
        isReceivingVPRs = true;
        qDebug() << "Again receiving VPRs";
        updateHeadingDisplayStyle();
      }
      headingUpdateTimer->stop();
      if (isStaleHeadingWarningEnabled)
      {
        headingUpdateTimer->start(); // restart the timer
      }
      // moved section below to VFR section above so can filter by solution (VPR
      // does not give solution)
      /*current_vpr_lon = vpr_message.lon;
      current_vpr_lat = vpr_message.lat;
      if (nav_connected) {
        // this is basically pinging navsol to give us a converted fix back
        emit requestCoordConversion(QPointF(vpr_message.lon, vpr_message.lat));
      }

      else {

        updateXYVPR(
          QPair<QString, QString>(QString::number(current_vpr_lon, 'f', 6),
                                  QString::number(current_vpr_lat, 'f', 6)));
                                  }*/
    }
  }

  else if (type == navg::NavestMessageType::OCT) {

    // do nothing.
    // could potentially handle ahrs incoming navest logs here
  }
}

void
StatusDisplay::updateEphemeris(double inDepth, double inHeading)
{
  ui->heading_label->setText(QString::number(inHeading, 'f', 2));
  // ui->depth_label->setText(QString::number(inDepth, 'f', 2));
}

void
StatusDisplay::updateXYVPR(QPair<QString, QString> pair)
{
  if (received_vpr) {
    ui->x_label->setText(pair.first);
    ui->y_label->setText(pair.second);
  }
}

void
StatusDisplay::selectedSolutionChanged(const QString& solution)
{

  // takes a signal when the navest solution changes
  this->current_soln = solution;
  //this->currentSolution = solution;
  
  // use solution to filter VFR messages to only selected solution
  // qDebug(plugin_status_display) << this->currentSolution;
}

QString
StatusDisplay::getCurrentSolution()
{

  //  currentSolution = "SOLN_DEADRECK";
  //return this->currentSolution;
  return this->current_soln;
}

void
StatusDisplay::headingUpdateTimedOut()
{
  if (isReceivingVPRs)
  {
    isReceivingVPRs = false;
    qDebug() << "Not receiving VPRs";
    updateHeadingDisplayStyle();
  }
}

void
StatusDisplay::updateHeadingDisplayStyle()
{
  if (isStaleHeadingWarningEnabled && (!isReceivingVPRs || isHeadingRepeating))
  {
    if (!ui->heading_label->text().endsWith("?"))
    {
      ui->heading_label->setText(ui->heading_label->text() + " ?");
    }
    ui->heading_label->setStyleSheet("QLabel { background-color : red ; color : black }");
    ui->label_5->setStyleSheet("QLabel { background-color : red ; color : black }");
  }
  else
  {
    ui->heading_label->setStyleSheet("");
    ui->label_5->setStyleSheet("");
  }
}

} // status_display
} // plugins
} // navg
