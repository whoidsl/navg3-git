/**
 * Copyright 2021 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// Created by tmjoyce on 06/21/21.
//

#ifndef NAVG_STATUSDISPLAY_H
#define NAVG_STATUSDISPLAY_H

#include <navg/CorePluginInterface.h>

#include "dslmap/MapView.h"
#include <QLoggingCategory>
#include <QObject>
#include <QWidget>
#include <QDateTime>
#include <QUdpSocket>
#include <memory>
#include "StatusSettings.h"

#include <navg/NavGMapView.h>
#include <../../navest/include/navest/Messages.h>
#include <../../nav_management/include/nav_management/NavSolutionManager.h>

namespace navg {
namespace plugins {
namespace status_display {

namespace Ui {
class StatusDisplay;
}

class StatusDisplay
  : public QWidget
  , public CorePluginInterface
{
  Q_OBJECT
  Q_INTERFACES(navg::CorePluginInterface)
  Q_PLUGIN_METADATA(IID CorePluginInterface_iid FILE "StatusDisplay.json")

signals:
  void requestCoordConversion(QPointF);

public:
  explicit StatusDisplay(QWidget* parent = nullptr);
  ~StatusDisplay() override;

  //
  // CorePluginInterface Implementation
  //
  void loadSettings(QSettings* settings) override;
  void saveSettings(QSettings& settings) override;

  // This plugin connects to ....
  void connectPlugin(const QObject* plugin,
                     const QJsonObject& metadata) override;
  void setMapView(QPointer<dslmap::MapView> view) override;
  QList<QAction*> pluginMenuActions() override;
  QList<QAction*> pluginToolbarActions() override { return {}; }
  QString getCurrentSolution();

public slots:
  /// \brief Show the settings dialog
  ///
  void showSettingsDialog();
  void selectedSolutionChanged(const QString& solution);
  
protected:
protected slots:
  /// \brief Handle multiple incoming message types from navest
  void updateFromNavestMessage(NavestMessageType type, QVariant message);

  void updateEphemeris(double inDepth, double inHeading);
  void updateXYVPR(QPair<QString,QString>);

  /// \brief Helper slot that really just handles updating the XY conversion from navsol
  void handleConversion();

  void dataPending();

  void headingUpdateTimedOut();

private:
  std::unique_ptr<Ui::StatusDisplay> ui;
  QPointer<dslmap::MapView> m_view;
  QPointer<navg::NavGMapView> n_view;

  double current_vpr_lon = 0;
  double current_vpr_lat = 0;
  double current_vfr_lon = 0;
  double current_vfr_lat = 0;
  bool received_vpr = false;
  bool nav_connected = false;

  QAction* m_action_show_settings = nullptr;

  // used as the vehicle id of the fix vehicle
  int vehicle_number;
  int aux_status_port;
  QString current_soln;
  bool isStaleHeadingWarningEnabled;
  int staleHeadingTimeoutSecs;

  QPointer<QUdpSocket> socket;

  //QString currentSolution = "SOLN_DEADRECK"; // default
  QString alt_solution = "SOLN_DEADRECK"; // only want deadreck sol for altitude
  QString dr = "SOLN_DEADRECK";
  QString ins = "SOLN_PHINS";
  QString usbl = "SOLN_USBL";
  
  static constexpr int STALE_HEADING_DEFAULT_SEC = 3;
  void updateHeadingDisplayStyle();
  QTimer *headingUpdateTimer;
  bool isHeadingRepeating;
  bool isReceivingVPRs;
  QTime timeHeadingLastChanged;
  int prevHeadingCentiDegrees;
};

Q_DECLARE_LOGGING_CATEGORY(plugin_status_display)
} // status_display
} // plugins
} // navg

#endif // STATUSDISPLAY_H
