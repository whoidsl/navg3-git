/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 12/6/18.
//

#ifndef NAVG_HDG_DEPTH_ALT_H
#define NAVG_HDG_DEPTH_ALT_H

#include <navg/CorePluginInterface.h>

#include <QAction>
#include <QHash>
#include <QHostAddress>
#include <QJsonObject>
#include <QLoggingCategory>
#include <QObject>
#include <QTime>
#include <QWidget>

#include <memory>

namespace navg {
namespace plugins {

// namespace sentry_acomms {
// namespace SentryAcomms {
// class Modem;
//}
//}

namespace hdg_depth_alt {

namespace Ui {
class HdgDepthAlt;
}

class HdgDepthAlt : public QWidget, public CorePluginInterface
{
  Q_OBJECT
  Q_INTERFACES(navg::CorePluginInterface)
  Q_PLUGIN_METADATA(IID CorePluginInterface_iid FILE "HdgDepthAlt.json")

signals:

public:
  explicit HdgDepthAlt(QWidget* parent = nullptr);
  ~HdgDepthAlt() override;

  //
  // CorePluginInterface Implementation
  //
  void loadSettings(QSettings* settings) override;
  void saveSettings(QSettings& settings) override;

  // This plugin connects to acomms plugin.
  void connectPlugin(const QObject* plugin,
                     const QJsonObject& metadata) override;
  void setMapView(QPointer<dslmap::MapView> view) override;
  QList<QAction*> pluginMenuActions() override;
  QList<QAction*> pluginToolbarActions() override;

protected:
  void wheelEvent(QWheelEvent* event) override;

  int fontsize() { return m_fontsize; }

protected slots:

  /// \brief Set the latest abort status from the acomms plugin
  ///
  /// \param is_aborting
  void handleMessage(int, int, int);

  void updateDepth(double);

  void setFontsize(int);

private:
  std::unique_ptr<Ui::HdgDepthAlt> ui;
  int m_seconds_since;
  int m_seconds_since_depth;
  int m_hdg;
  int m_depth;
  int m_alt;
  int m_fontsize;
  QFont m_font;
};

Q_DECLARE_LOGGING_CATEGORY(plugin_hdg_depth_alt)
} // hdg_depth_alt
} // plugins
} // navg

#endif // NAVG_HDG_DEPTH_ALT_H
