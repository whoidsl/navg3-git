/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro 1/8/19.
//

#include "HdgDepthAlt.h"
#include "ui_HdgDepthAlt.h"

#include <dslmap/MapView.h>

#include <QSettings>
#include <QStandardItemModel>
#include <QTimer>
#include <QWheelEvent>

namespace navg {
namespace plugins {
namespace hdg_depth_alt {

Q_LOGGING_CATEGORY(plugin_hdg_depth_alt, "navg.plugins.hdg_depth_alt")

HdgDepthAlt::HdgDepthAlt(QWidget* parent)
  : QWidget(parent)
  , ui(std::make_unique<Ui::HdgDepthAlt>())
  , m_seconds_since(-1)
  , m_seconds_since_depth(-1)
  , m_fontsize(10)
{
  ui->setupUi(this);
  m_font = ui->ageLbl->font();
  auto t = new QTimer(this);
  connect(t, &QTimer::timeout, [this]() {
    if (m_seconds_since >= 0) {
      ui->ageLbl->setText("Age: " + QString::number(m_seconds_since));
      if (m_seconds_since >= 120) {
        ui->groupBox->setStyleSheet("QGroupBox { border:1px solid yellow} ");
      }
      m_seconds_since++;
    }
    if (m_seconds_since_depth >= 0) {
      ui->ageLbl_depth->setText("Age: " +
                                QString::number(m_seconds_since_depth));
      if (m_seconds_since_depth >= 120) {
        ui->groupBox->setStyleSheet("QGroupBox { border:1px solid yellow} ");
      }
      m_seconds_since_depth++;
    }

  });
  t->start(1000);
}

HdgDepthAlt::~HdgDepthAlt() = default;

void HdgDepthAlt::setMapView(QPointer<dslmap::MapView>)
{
}

void
HdgDepthAlt::connectPlugin(const QObject* plugin, const QJsonObject& metadata)
{
  if (!plugin) {
    return;
  }

  const auto name =
    metadata.value("MetaData").toObject().value("name").toString();
  if (name.isEmpty()) {
    return;
  }

  if (name == "sentry_acomms") {
    if (!connect(plugin, SIGNAL(hdgDepthAltUpdated(int, int, int)), this,
                 SLOT(handleMessage(int, int, int)))) {
      qCCritical(plugin_hdg_depth_alt,
                 "Unable to connect to signal: "
                 "SentryAcomms::hdgDepthAltUpdated(int, int, int)");
      Q_ASSERT(false);
    } else {
      qCInfo(plugin_hdg_depth_alt,
             "Connected to SentryAcomms::hdgDepthAltUpdated");
    }
  }

  if (name == "vehicle_usbl") {
    if (!connect(plugin, SIGNAL(depthUpdated(double)), this,
                 SLOT(updateDepth(double)))) {
      qCCritical(plugin_hdg_depth_alt, "Unable to connect to signal: "
                                       "SentryUsbl::depthUpdated(double)");
    } else {
      qCInfo(plugin_hdg_depth_alt, "Connected to SentryUsbl::depthUpdated");
    }
  }
}

void
HdgDepthAlt::loadSettings(QSettings* settings)
{
  if (settings == nullptr) {
    return;
  }
  bool ok = false;
  auto fontsize = settings->value("fontsize").toInt(&ok);
  if (ok) {
    setFontsize(fontsize);
  }
}

void
HdgDepthAlt::saveSettings(QSettings& settings)
{
  settings.setValue("fontsize", m_fontsize);
}

QList<QAction*>
HdgDepthAlt::pluginToolbarActions()
{
  return {};
}

QList<QAction*>
HdgDepthAlt::pluginMenuActions()
{
  return {};
}

void
HdgDepthAlt::handleMessage(int hdg, int depth, int alt)
{
  if (m_seconds_since < 0 || m_seconds_since >= 120) {
    // make young again
    ui->groupBox->setStyleSheet("QGroupBox { border:1px solid green;} ");
  }
  m_seconds_since = 0;
  m_seconds_since_depth = 0;
  m_hdg = hdg;
  m_depth = depth;
  m_alt = alt;
  ui->ageLbl_depth->hide();
  ui->ageLbl->setText("Age: " + QString::number(m_seconds_since));
  ui->depthLbl->setText(QString::number(m_depth) + " m");
  ui->altLbl->setText(QString::number(m_alt) + " m");
  ui->hdgLbl->setText(QString::number(m_hdg) + "°");

  ui->label->setText("Heading (Acomms)");
  ui->label2->setText("Depth (Acomms)");
  ui->label3->setText("Altitude (Acomms)");
}

void
HdgDepthAlt::updateDepth(double depth)
{
  if (m_seconds_since < 0 || m_seconds_since >= 120) {
    // make young again
    ui->groupBox->setStyleSheet("QGroupBox { border:1px solid green;} ");
  }
  m_seconds_since_depth = 0;
  m_depth = depth;
  ui->ageLbl_depth->show();
  ui->ageLbl_depth->setText("Age: " + QString::number(m_seconds_since_depth));
  ui->depthLbl->setText(QString::number(m_depth) + " m");

  ui->label2->setText("Depth (USBL)");
}

void
HdgDepthAlt::setFontsize(int fontsize)
{
  if (fontsize == m_fontsize) {
    return;
  } else if (fontsize < 5) {
    m_fontsize = 5;
  } else if (fontsize > 100) {
    m_fontsize = 100;
  } else {
    m_fontsize = fontsize;
  }

  QFont font(m_font.family(), m_fontsize, m_font.style());
  ui->hdgLbl->setFont(font);
  ui->depthLbl->setFont(font);
  ui->altLbl->setFont(font);
}

void
HdgDepthAlt::wheelEvent(QWheelEvent* e)
{
  QPoint numDegrees = e->angleDelta() / 24;
  if (!numDegrees.isNull()) {
    setFontsize(fontsize() + numDegrees.y());
    e->accept();
  } else {
    e->ignore();
  }
}

} // hdg_depth_alt
} // plugins
} // navg
