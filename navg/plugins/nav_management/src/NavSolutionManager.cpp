/**
 * Copyright 2020 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// created by tmjoyce
//

#include "NavSolutionManager.h"
#include "ui_NavSolutionManager.h"

#include <dslmap/MapView.h>

#include <QAction>
#include <QSettings>
#include <QSortFilterProxyModel>
#include <QStandardItemModel>
#include <QUdpSocket>
#include <QVariant>

namespace navg {
namespace plugins {
namespace navsol_manager {

Q_LOGGING_CATEGORY(plugin_nav_manager, "navg.plugins.nav_manager")

NavSolutionManager::NavSolutionManager(QWidget* parent)
  : QWidget(parent)
  , ui(std::make_unique<Ui::NavSolutionManager>())
{
  hori_labels << "Name"
              << "Latitude"
              << "Longitude"
              << "Range(m)"
              << "Bearing";

  hori_labels_xy << "Name"
                 << "X"
                 << "Y"
                 << "Range(m)"
                 << "Bearing";

  ui->setupUi(this);
  ui->dd_radio->setChecked(true);
  mouse_id = QUuid::createUuid();
  cursor_id = QUuid::createUuid();

  mouse_row = 0;
  cursor_row = 1;
  ui->nav_table->setEditTriggers(QAbstractItemView::NoEditTriggers);

  connect(ui->dd_radio,
          &QRadioButton::toggled,
          this,
          &NavSolutionManager::changeCoordDisplay);
  connect(ui->dm_radio,
          &QRadioButton::toggled,
          this,
          &NavSolutionManager::changeCoordDisplay);
  connect(ui->alvin_radio,
          &QRadioButton::toggled,
          this,
          &NavSolutionManager::changeCoordDisplay);
  connect(ui->dms_radio,
          &QRadioButton::toggled,
          this,
          &NavSolutionManager::changeCoordDisplay);

  connect(
    ui->layer_combo,
    static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
    this,
    &NavSolutionManager::comboIndexChanged);
}
NavSolutionManager::~NavSolutionManager() = default;

void
NavSolutionManager::setMapView(QPointer<dslmap::MapView> view)
{
  m_view = view;
  // connect layer add and remove hooks to combo box
  auto scene = m_view->mapScene();

  n_scene = qobject_cast<NavGMapScene*>(scene);

  setupNavTable();
  connect(scene,
          static_cast<void (dslmap::MapScene::*)(dslmap::MapLayer * layer)>(
            &dslmap::MapScene::layerAdded),
          this,
          &NavSolutionManager::sceneLayerAdded,
          Qt::QueuedConnection);
  connect(scene,
          static_cast<void (dslmap::MapScene::*)(dslmap::MapLayer * layer)>(
            &dslmap::MapScene::layerRemoved),
          this,
          &NavSolutionManager::sceneLayerRemoved,
          Qt::QueuedConnection);
  connect(m_view,
          &dslmap::MapView::scenePositionUnderMouseChanged,
          this,
          &NavSolutionManager::receiveMousePosition);

  auto nview = qobject_cast<NavGMapView*>(view);
  connect(nview,
          &navg::NavGMapView::cursorDropped,
          this,
          &NavSolutionManager::receiveCursorPosition);
  // trigger the display manager to update alvinxy coords when a new origin is
  // added
  connect(n_scene, &NavGMapScene::alvinXYOriginChanged, [this]() {
    changeCoordDisplay(1);
  });

  setupLayerBox();
}

void
NavSolutionManager::connectPlugin(const QObject* plugin,
                                  const QJsonObject& metadata)
{
  if (!plugin) {
    return;
  }

  const auto name =
    metadata.value("MetaData").toObject().value("name").toString();
  if (name.isEmpty()) {
    return;
  }
}

void
NavSolutionManager::loadSettings(QSettings* settings)
{
  auto value = settings->value("dd_radio");

  if (value.isValid()) {
    const auto valid_val = value.toBool();

    if (valid_val) {
      ui->dd_radio->setChecked(true);
    }
  }

  value = settings->value("dm_radio");

  if (value.isValid()) {
    const auto valid_val = value.toBool();

    if (valid_val) {
      ui->dm_radio->setChecked(true);
    }
  }
  value = settings->value("dms_radio");

  if (value.isValid()) {
    const auto valid_val = value.toBool();

    if (valid_val) {
      ui->dms_radio->setChecked(true);
    }
  }
  value = settings->value("alvin_radio");

  if (value.isValid()) {
    const auto valid_val = value.toBool();

    if (valid_val) {
      ui->alvin_radio->setChecked(true);
    }
  }
}

void
NavSolutionManager::saveSettings(QSettings& settings)
{
  settings.setValue("persist", 1);

  if (ui->dd_radio->isChecked()) {
    settings.setValue("dd_radio", true);
    settings.setValue("dm_radio", false);
    settings.setValue("dms_radio", false);
    settings.setValue("alvin_radio", false);
  } else if (ui->dm_radio->isChecked()) {
    settings.setValue("dm_radio", true);
    settings.setValue("dd_radio", false);
    settings.setValue("dms_radio", false);
    settings.setValue("alvin_radio", false);
  } else if (ui->dms_radio->isChecked()) {
    settings.setValue("dms_radio", true);
    settings.setValue("dd_radio", false);
    settings.setValue("dm_radio", false);
    settings.setValue("alvin_radio", false);
  } else if (ui->alvin_radio->isChecked()) {
    settings.setValue("alvin_radio", true);
    settings.setValue("dd_radio", false);
    settings.setValue("dm_radio", false);
    settings.setValue("dms_radio", false);
  }
}

QList<QAction*>
NavSolutionManager::pluginMenuActions()
{
  return {};
}

void
NavSolutionManager::setupLayerBox()
{

  auto scene = m_view->mapScene();
  LayerHash nav_solution_layers =
    scene->layers(dslmap::MapItemType::NavestLayerType);

  ui->layer_combo->addItem("Mouse", mouse_id);
  ui->layer_combo->addItem("Cursor", cursor_id);

  QHash<QUuid, dslmap::MapLayer*>::iterator i;
  for (i = nav_solution_layers.begin(); i != nav_solution_layers.end(); ++i) {

    // Sets text to layer name. And stores value of unique Layer id
    ui->layer_combo->addItem(i.value()->name(), i.key());
  }
}

void
NavSolutionManager::setInitialPosition(dslmap::MapLayer* layer)
{
  // sets the table latlon of the input layers current position

  navg::plugins::navest::NavestLayer* curr_nav_layer =
    dynamic_cast<navg::plugins::navest::NavestLayer*>(layer);

  if (curr_nav_layer == nullptr) {
    qCInfo(plugin_nav_manager) << "error maplayer is null";
    return;
  }

  QPointF curr_pos = curr_nav_layer->currentPosition();

  if (curr_pos.isNull()) {
    return;
  }

  updateNavTableLatLon(curr_pos, layer->id());
  updateNavTableRangeBear();
}

void
NavSolutionManager::receivePositionFix(const QPointF fix)
{

  // get sender of fix so we know which layer sent it.
  dslmap::MapLayer* layer = qobject_cast<dslmap::MapLayer*>(sender());
  if (layer == nullptr) {
    qCInfo(plugin_nav_manager) << "error sender is null";
    return;
  }

  updateNavTableLatLon(fix, layer->id());
  updateNavTableRangeBear();
}

void
NavSolutionManager::receiveMousePosition(QPointF pos)
{
  updateNavTablePosition(pos, mouse_row);
}

void
NavSolutionManager::receiveCursorPosition(QPointF pos)
{

  updateNavTablePosition(pos, cursor_row);
}

void
NavSolutionManager::updateNavTablePosition(QPointF pos, int row)
{

  auto converted_fix = convertPositionFix(pos);
  QTableWidgetItem* pos_fix_item = new QTableWidgetItem(converted_fix.first);
  pos_fix_item->setData(Qt::UserRole,
                        pos); // storing the unconverted position fix as data
  ui->nav_table->setItem(row, 1, pos_fix_item);
  ui->nav_table->setItem(row, 2, new QTableWidgetItem(converted_fix.second));

  updateNavTableRangeBear();
}

void
NavSolutionManager::updateNavTableLatLon(const QPointF fix, QUuid layer_id)
{

  auto converted_fix = convertPositionFix(fix);

  // loop through table to find matching quuid. if found populate table cells
  int rows = ui->nav_table->rowCount();

  for (int i = 1; i < rows; i++) {
    QTableWidgetItem* item = ui->nav_table->item(i, 0);
    if (item->data(Qt::UserRole).toUuid() == layer_id) {
      QTableWidgetItem* lat_item = new QTableWidgetItem(converted_fix.first);
      lat_item->setData(Qt::UserRole, fix);
      ;
      ui->nav_table->setItem(i, 1, lat_item);
      ui->nav_table->setItem(i, 2, new QTableWidgetItem(converted_fix.second));
    }
  }
}

void
NavSolutionManager::changeCoordDisplay(int state)
{
  emit coordDisplayChanged();
  QObject* obj = sender();
  if (obj == ui->alvin_radio) {

    ui->nav_table->setColumnCount(hori_labels_xy.size());

    ui->nav_table->setHorizontalHeaderLabels(hori_labels_xy);
  } else {

    ui->nav_table->setColumnCount(hori_labels.size());

    ui->nav_table->setHorizontalHeaderLabels(hori_labels);
  }

  int rows = ui->nav_table->rowCount();
  for (int i = 0; i < rows; i++) {
    QTableWidgetItem* x_item = ui->nav_table->item(i, 1);
    QTableWidgetItem* y_item = ui->nav_table->item(i, 2);

    if (x_item && y_item) {
      // contains latlon fix in x_item data
      auto converted_fix =
        convertPositionFix(x_item->data(Qt::UserRole).toPointF());

      if (obj == ui->alvin_radio) {
	      x_item->setText(converted_fix.second);
	      y_item->setText(converted_fix.first);
      }
      else {
      x_item->setText(converted_fix.first);
      y_item->setText(converted_fix.second);
      }
    }
  }
}

void
NavSolutionManager::comboIndexChanged(int index)
{
  updateNavTableRangeBear();
}

void
NavSolutionManager::updateNavTableRangeBear()
{

  int rows = ui->nav_table->rowCount();

  auto current_layer_id = ui->layer_combo->currentData(Qt::UserRole).toUuid();

  // the row in qtable that contains the same quuid as our current combobox

  int current_layer_row = -1;

  if (current_layer_id == mouse_id) {

    current_layer_row = mouse_row;
  } else if (current_layer_id == cursor_id) {

    current_layer_row = cursor_row;
  } else {

    for (int i = 1; i < rows; i++) {
      // this is a loop just to grab the corresponding combobox data and sync it
      // with the nav table
      QTableWidgetItem* row_lat_item = ui->nav_table->item(i, 0);
      if (!row_lat_item) {
        continue;
      }
      if (current_layer_id == row_lat_item->data(Qt::UserRole).toUuid()) {

        current_layer_row = i;
        break;
      }
    }
  }
  if (current_layer_row < 0) {

    return;
  }

  QTableWidgetItem* source_lat_item = ui->nav_table->item(current_layer_row, 1);
  if (!source_lat_item) {

    return;
  }
  // the loop to actually populate the correct values in table
  for (int i = 0; i < rows; i++) {

    QTableWidgetItem* item = ui->nav_table->item(i, 0);
    if (!item) {
      continue;
    }
    QTableWidgetItem* curr_lat_item = ui->nav_table->item(i, 1);
    if (!curr_lat_item) {
      continue;
    }
    double range =
      calculateRange(source_lat_item->data(Qt::UserRole).toPointF(),
                     curr_lat_item->data(Qt::UserRole).toPointF());
    // plot in table
    ui->nav_table->setItem(
      i, 3, new QTableWidgetItem(QString::number(range, 'f', 2)));

    double bearing =
      calculateBearing(source_lat_item->data(Qt::UserRole).toPointF(),
                       curr_lat_item->data(Qt::UserRole).toPointF());

    ui->nav_table->setItem(
      i, 4, new QTableWidgetItem(QString::number(bearing, 'f', 2)));
  }
}

QPair<QString, QString>
NavSolutionManager::convertPositionFix(QPointF fix)
{
  auto scene = m_view->mapScene();
  auto proj = scene->projection();

  LatLonType type;
  if (ui->dd_radio->isChecked()) {
    type = DD;
  } else if (ui->dm_radio->isChecked()) {
    type = DM;
  } else if (ui->dms_radio->isChecked()) {
    type = DMS;
  } else if (ui->alvin_radio->isChecked()) {
    type = ALVIN;
  } else {
    return QPair<QString, QString>();
  }
  QPointF converted_fix;

  if (!proj) {
    // return converted_fix; // will be invalid
    return QPair<QString, QString>();
  }

  double x_pos = fix.x();
  double y_pos = fix.y();

  if (type == DD) {
    proj->transformToLonLat(1, &x_pos, &y_pos);
    return QPair<QString, QString>(QString::number(y_pos, 'f', 6),
                                   QString::number(x_pos, 'f', 6));

  } else if (type == DMS) {

    proj->transformToLonLat(1, &x_pos, &y_pos);
    int x_d = int(x_pos);
    int x_minutes = (int)((x_pos - (float)x_d) * 60.f);
    int x_seconds =
      (int)((x_pos - (float)x_d - (float)x_minutes / 60.f) * 60.f * 60.f);

    QString x_string = QString::number(x_d) + "° " +
                       QString::number(abs(x_minutes)) + "' " +
                       QString::number(abs(x_seconds)) + "\"";

    int y_d = int(y_pos);
    int y_minutes = (int)((y_pos - (float)y_d) * 60.f);
    int y_seconds =
      (int)((y_pos - (float)y_d - (float)y_minutes / 60.f) * 60.f * 60.f);

    QString y_string = QString::number(y_d) + "° " +
                       QString::number(abs(y_minutes)) + "' " +
                       QString::number(abs(y_seconds)) + "\"";

    return QPair<QString, QString>(y_string, x_string);
  } else if (type == DM) {
    proj->transformToLonLat(1, &x_pos, &y_pos);
    int x_d = int(x_pos);
    double x_minutes = ((x_pos - (float)x_d) * 60.0f);

    QString x_string = QString::number(x_d) + "° " +
                       QString::number(abs(x_minutes), 'f', 4) + "' ";

    int y_d = int(y_pos);
    double y_minutes = ((y_pos - (float)y_d) * 60.0f);

    QString y_string = QString::number(y_d) + "° " +
                       QString::number(abs(y_minutes), 'f', 4) + "' ";

    return QPair<QString, QString>(y_string, x_string);

  } else if (type == ALVIN) {

    if (!n_scene)
      return QPair<QString, QString>();

    if (!n_scene->hasAlvinXYOrigin()) {
      qCWarning(plugin_nav_manager) << "No AlvinXY origin specified for scene.";
      return QPair<QString, QString>();
    }
    const auto origin = n_scene->alvinXYOrigin();
    const auto alv_proj =
      dslmap::Proj::alvinXYFromOrigin(origin.x(), origin.y());

    proj->transformTo(*alv_proj, 1, &x_pos, &y_pos);

    return QPair<QString, QString>(QString::number(x_pos, 'f', 2) + "m",
                                   QString::number(y_pos, 'f', 2) + "m");
  }
  return QPair<QString, QString>();
}

QPair<QString, QString>
NavSolutionManager::convertPositionFixLonLat(QPointF fix)
{
  auto scene = m_view->mapScene();
  auto proj = scene->projection();

  LatLonType type;
  if (ui->dd_radio->isChecked()) {
    type = DD;
  } else if (ui->dm_radio->isChecked()) {
    type = DM;
  } else if (ui->dms_radio->isChecked()) {
    type = DMS;
  } else if (ui->alvin_radio->isChecked()) {
    type = ALVIN;
  } else {
    return QPair<QString, QString>();
  }
  QPointF converted_fix;

  double x_pos = fix.x();
  double y_pos = fix.y();

  if (type == DD) {
    return QPair<QString, QString>(QString::number(y_pos, 'f', 6),
                                   QString::number(x_pos, 'f', 6));

  } else if (type == DMS) {

    int x_d = int(x_pos);
    int x_minutes = (int)((x_pos - (float)x_d) * 60.f);
    int x_seconds =
      (int)((x_pos - (float)x_d - (float)x_minutes / 60.f) * 60.f * 60.f);

    QString x_string = QString::number(x_d) + "° " +
                       QString::number(abs(x_minutes)) + "' " +
                       QString::number(abs(x_seconds)) + "\"";

    int y_d = int(y_pos);
    int y_minutes = (int)((y_pos - (float)y_d) * 60.f);
    int y_seconds =
      (int)((y_pos - (float)y_d - (float)y_minutes / 60.f) * 60.f * 60.f);

    QString y_string = QString::number(y_d) + "° " +
                       QString::number(abs(y_minutes)) + "' " +
                       QString::number(abs(y_seconds)) + "\"";

    return QPair<QString, QString>(y_string, x_string);
  } else if (type == DM) {
    int x_d = int(x_pos);
    double x_minutes = ((x_pos - (float)x_d) * 60.0f);

    QString x_string = QString::number(x_d) + "° " +
                       QString::number(abs(x_minutes), 'f', 4) + "' ";

    int y_d = int(y_pos);
    double y_minutes = ((y_pos - (float)y_d) * 60.0f);

    QString y_string = QString::number(y_d) + "° " +
                       QString::number(abs(y_minutes), 'f', 4) + "' ";

    return QPair<QString, QString>(y_string, x_string);

  } else if (type == ALVIN) {

    if (!n_scene)
      return QPair<QString, QString>();

    if (!n_scene->hasAlvinXYOrigin()) {
      qCWarning(plugin_nav_manager) << "No AlvinXY origin specified for scene.";
      return QPair<QString, QString>();
    }
    const auto origin = n_scene->alvinXYOrigin();
    const auto alv_proj =
      dslmap::Proj::alvinXYFromOrigin(origin.x(), origin.y());

    alv_proj->transformFromLonLat(1, &x_pos, &y_pos);

    return QPair<QString, QString>(QString::number(y_pos, 'f', 2) + "m",
                                   QString::number(x_pos, 'f', 2) + "m");
  }
  return QPair<QString, QString>();
}

void
NavSolutionManager::setupNavTable()
{
  // called once on construction. Calling again could result in duplicate values
  // in tables

  ui->nav_table->setColumnCount(hori_labels.size());

  ui->nav_table->setHorizontalHeaderLabels(hori_labels);
  QTableWidgetItem* table_item = new QTableWidgetItem("Mouse");
  table_item->setData(Qt::UserRole, mouse_id);

  ui->nav_table->insertRow(ui->nav_table->rowCount());
  ui->nav_table->setItem(ui->nav_table->rowCount() - 1, 0, table_item);

  table_item = new QTableWidgetItem("Cursor");
  table_item->setData(Qt::UserRole, cursor_id);

  ui->nav_table->insertRow(ui->nav_table->rowCount());
  ui->nav_table->setItem(ui->nav_table->rowCount() - 1, 0, table_item);

  auto scene = m_view->mapScene();

  // returns MapLayer* hash that has navestlayertype property
  LayerHash nav_solution_layers =
    scene->layers(dslmap::MapItemType::NavestLayerType);

  // LayerHash converted;
  QHash<QUuid, dslmap::MapLayer*>::iterator i;
  for (i = nav_solution_layers.begin(); i != nav_solution_layers.end(); ++i) {
    // for now just add the name of editor to prove concept
    QTableWidgetItem* table_item = new QTableWidgetItem(i.value()->name());
    table_item->setData(Qt::UserRole, i.value()->id());

    ui->nav_table->insertRow(ui->nav_table->rowCount());
    ui->nav_table->setItem(ui->nav_table->rowCount() - 1, 0, table_item);

    dslmap::MapLayer* m = i.value();
    setInitialPosition(m);
    navg::plugins::navest::NavestLayer* curr_nav_layer =
      dynamic_cast<navg::plugins::navest::NavestLayer*>(m);

    connect(curr_nav_layer,
            &navg::plugins::navest::NavestLayer::currentPositionChanged,
            this,
            &NavSolutionManager::receivePositionFix,
            Qt::QueuedConnection);
  }
}

void
NavSolutionManager::sceneLayerAdded(dslmap::MapLayer* layer)
{
  if (layer->type() != dslmap::MapItemType::NavestLayerType) {
    return;
  }
  navg::plugins::navest::NavestLayer* curr_nav_layer =
    dynamic_cast<navg::plugins::navest::NavestLayer*>(layer);
  if (!curr_nav_layer) {
    return;
  }
  ui->layer_combo->addItem(layer->name(), layer->id());

  QTableWidgetItem* table_item = new QTableWidgetItem(layer->name());
  table_item->setData(Qt::UserRole, layer->id());

  ui->nav_table->insertRow(ui->nav_table->rowCount());
  ui->nav_table->setItem(ui->nav_table->rowCount() - 1, 0, table_item);

  setInitialPosition(layer);

  connect(curr_nav_layer,
          &navg::plugins::navest::NavestLayer::currentPositionChanged,
          this,
          &NavSolutionManager::receivePositionFix,
          Qt::QueuedConnection);
}
void
NavSolutionManager::sceneLayerRemoved(dslmap::MapLayer* layer)
{
  if (layer->type() != dslmap::MapItemType::NavestLayerType) {
    return;
  }
  navg::plugins::navest::NavestLayer* curr_nav_layer =
    dynamic_cast<navg::plugins::navest::NavestLayer*>(layer);
  if (!curr_nav_layer) {
    return;
  }
  QUuid layer_id = layer->id();
  int combo_size = ui->layer_combo->count();
  for (int i = 0; i < combo_size; i++) {
    if (ui->layer_combo->itemData(i).toUuid() == layer_id) {
      ui->layer_combo->removeItem(i);
    }
  }
  int table_row_size = ui->nav_table->rowCount();
  for (int i = 0; i < table_row_size; i++) {
    QTableWidgetItem* item = ui->nav_table->item(i, 0);
    if (item->data(Qt::UserRole).toUuid() == layer_id) {
      // remove the row
      ui->nav_table->removeRow(i);
      // need the break or segfault
      break;
    }
  }
}

double
NavSolutionManager::calculateRange(const QPointF source_fix,
                                   const QPointF dest_fix)
{
  auto scene = m_view->mapScene();
  auto proj = scene->projection();

  if (!proj) {
    qCDebug(plugin_nav_manager) << "no proj returning 0 ";
    return 0;
  }
  auto range = proj->geodeticDistance(source_fix, dest_fix);

  return range;
}

double
NavSolutionManager::calculateBearing(const QPointF source_fix,
                                     const QPointF dest_fix)
{
  auto scene = m_view->mapScene();
  auto proj = scene->projection();
  if (!scene) {
    qCDebug(plugin_nav_manager) << "bearing needs a scene to calculate ";
    return 0;
  }
  if (!proj) {
    qCDebug(plugin_nav_manager) << "no proj returning 0 ";
    return 0;
  }
  const auto range_line = QLineF{ m_view->mapToScene((source_fix.toPoint())),
                                  m_view->mapToScene((dest_fix.toPoint())) };
  if (range_line.length() == 0) {

    return 0;
  }
  float angle = atan2(range_line.p1().y() - range_line.p2().y(),
                      range_line.p1().x() - range_line.p2().x());
  angle = std::fmod(((angle * 180 / M_PI) + 90), 360);

  // some negative correction...
  if (angle < 0) {
    angle += 360;
  }

  return angle;
}

void
NavSolutionManager::receiveOutsideConversion(QPointF coords)
{
  emit sendFix(convertPositionFixLonLat(coords));
}

} // navsol
} // plugins
} // navg
