/**
 * Copyright 2020 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
//
// created by tmjoyce
//

#ifndef NAVG_NAVSOLUTIONMANAGER_H
#define NAVG_NAVSOLUTIONMANAGER_H

#include <QAction>
#include <QHash>
#include <QHostAddress>
#include <QJsonObject>
#include <QLineF>
#include <QLoggingCategory>
#include <QObject>
#include <QTableView>
#include <QTime>
#include <QUuid>
#include <QWidget>
#include <dslmap/MapItem.h>
#include <dslmap/MapLayer.h>
#include <dslmap/MapScene.h>
#include <dslmap/Proj.h>
#include <math.h>
#include <memory>
#include <navg/CorePluginInterface.h>
#include <../navest/Navest.h>
#include <../navest/NavestLayer.h>
#include <navg/NavGMapView.h>
#include <navg/NavGMapScene.h>

namespace navg {
namespace plugins {
namespace navsol_manager {

namespace Ui {
class NavSolutionManager;
}

class NavSolutionManager
  : public QWidget
  , public CorePluginInterface
{
  Q_OBJECT
  Q_INTERFACES(navg::CorePluginInterface)
  Q_PLUGIN_METADATA(IID CorePluginInterface_iid FILE "NavSolutionManager.json")

signals:
  void layerQuantityChanged();
  void sendFix(QPair<QString,QString>);
  void coordDisplayChanged();

public:
  explicit NavSolutionManager(QWidget* parent = nullptr);
  ~NavSolutionManager() override;

  using LayerHash = QHash<QUuid, dslmap::MapLayer*>;

  //
  // CorePluginInterface Implementation
  //
  void loadSettings(QSettings* settings) override;
  void saveSettings(QSettings& settings) override;

  void connectPlugin(const QObject* plugin,
                     const QJsonObject& metadata) override;
  void setMapView(QPointer<dslmap::MapView> view) override;
  QList<QAction*> pluginMenuActions() override;
  QList<QAction*> pluginToolbarActions() override { return {}; }
  enum LatLonType
  {
    DD,
    DM,
    DMS,
    ALVIN
  };

public slots:
  void receiveOutsideConversion(QPointF coords);

protected slots:

  void sceneLayerAdded(dslmap::MapLayer* layer);
  void sceneLayerRemoved(dslmap::MapLayer* layer);
  
  void setInitialPosition(dslmap::MapLayer* layer);
  void receivePositionFix(const QPointF fix);
  void updateNavTableLatLon(const QPointF fix, QUuid layer_id);
  void updateNavTableRangeBear();
  void receiveMousePosition(QPointF pos);
  void receiveCursorPosition(QPointF pos);
  void updateNavTablePosition(QPointF pos, int row);
  void changeCoordDisplay(int state);
  void comboIndexChanged(int index);

private:
  std::unique_ptr<Ui::NavSolutionManager> ui;
  QPointer<dslmap::MapView> m_view;
  QPointer<navg::NavGMapScene> n_scene;
  void setupNavTable();
  void setupLayerBox();
  double calculateRange(const QPointF source_fix, const QPointF dest_fix);
  double calculateBearing(const QPointF source_fix, const QPointF dest_fix);

  QStringList hori_labels;
  QStringList hori_labels_xy;
 
  QPair<QString, QString> convertPositionFix(QPointF fix);
  QPair<QString, QString> convertPositionFixLonLat(QPointF fix);

  int mouse_row;
  int cursor_row;

  QUuid mouse_id;
  QUuid cursor_id;
};

Q_DECLARE_LOGGING_CATEGORY(plugin_nav_manager)

} // navsol_manager
} // plugins
} // navg

#endif // NAVG_NAVSOLUTIONMANAGER_H
