/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef NAVG_NAVGMAPSCENE_H
#define NAVG_NAVGMAPSCENE_H

#include <dslmap/MapScene.h>
#include <QSettings>

namespace navg {

Q_DECLARE_LOGGING_CATEGORY(navgmapscene);

/// \brief NavG MapScene
///
/// Build upon the dslmap::MapScene class and adds the following:
///
///  - An Alvin XY Origin location
///  - Defined projection switching
///
/// ## Alvin XY Origin
/// DSL vehicles still use the notion of an Origin point, from which a local
/// XY coordinate system is projected.  The NavGMapScene provides a place to
/// store and retrieve this origin point through the following methods:
///
///   - void NavGMapScene::setAlvinXYOrigin(QPointF origin)
///   - void NavGMapScene::clearAlvinXYOrigin()
///   - QPointF NavGMapScene::alvinXYOrigin()
///   - (signal) NavGMapScene::alvinXYOriginChanged(QPointF origin)
///
/// "No origin point" is represented by QPointF(nan, nan)
///
/// ## Defined projection switching
/// Easy switching between three defined projections (Mercator, UTM, and
/// AlvinXY)
/// using the methods:
///
///   - void setProjectionType(ProjectionType type)
///   - ProjectionType projectionType()
///
/// Some projections have requriements:
///
///   - UTM projections require a non-empty scene.  The UTM zone is chosen by
///   the center of the
///     scene's bounding rectangle.
///   - AlvinXY projections need an AlvinXY origin defined.
///
/// By default, the NavGMapScene is initalized to a Mercator projection as it
/// has no preconditions for
/// use.
///
class NavGMapScene final : public dslmap::MapScene
{

  Q_OBJECT

public:
  /// \brief NavGMapScene projection type
  ///
  /// NavG supported map projections.
  ///  - Mercator,  Mercator projection with origin at scene center
  ///  - UTM,       projection using zone containing scene center
  ///  - AlvinXY,   AlvinXY projection with user-defined origin
  ///
  /// The only difference between Mercator and AlvinXY is that the later allows
  /// the user to define the origin of the transformation.
  enum class ProjectionType
  {
    Mercator, //!< Mercator projection with origin at scene center
    UTM,      //!< UTM projection using zone containing scene center
    AlvinXY,  //!< AlvinXY projection with user-defined origin
  };
  Q_ENUM(ProjectionType)

  explicit NavGMapScene(QObject* parent = Q_NULLPTR);
  ~NavGMapScene() override;

  void loadSettings(QSettings* settings);
  void saveSettings(QSettings& settings);

  /// \brief Get the stored AlvinXY Origin point
  ///
  /// "No origin point" is represented by QPointF(nan, nan) and can be
  /// checked using NavGMapScene::isValidAlvinXYOrigin
  ///
  /// \return
  QPointF alvinXYOrigin() const noexcept;

  /// \brief Check if the scene has a valid Alvin XY Origin
  ///
  /// \return
  bool hasAlvinXYOrigin() const noexcept;

  bool hasStartCoords() const noexcept;

  /// \brief Check if the supplied point is a valid Alvin XY Origin
  ///
  /// "No origin point" is represented by QPointF(nan, nan) and can be
  /// checked using NavGMapScene::isValidAlvinXYOrigin
  ///
  /// \return
  static bool isValidAlvinXYOrigin(const QPointF& origin) noexcept;

  /// \brief Get the scene projection type
  ///
  /// \return
  ProjectionType projectionType() const noexcept;

  /// Some getters defined for underlays/origins

  /// \return Mission start point
  QPointF XYStart() const noexcept;

  /// \return Saved bathymetric underlay file path
  QString bathyPath();

  QString contourPath();

  QString imagePath();

  QString geoimagePath();

  int contourSpacing();
  
  QString imageCoords();

  int diveNum();
  
  QString vehicleName();

  //target layer saved underlay path
  QString targetPath();

public slots:

  /// \brief Set the stored AlvinXY Origin point
  ///
  /// "No origin point" is represented by QPointF(nan, nan) and can be
  /// checked using NavGMapScene::isValidAlvinXYOrigin
  ///
  /// \param origin
  void setAlvinXYOrigin(QPointF origin) noexcept;

  void setStartXY(QPointF coords) noexcept;

  /// \brief Clear the Alvin XY Origin
  ///
  /// "No origin point" is represented by QPointF(nan, nan) and can be
  /// checked using NavGMapScene::isValidAlvinXYOrigin
  void clearAlvinXYOrigin() noexcept;

  /// \brief Set the projection type.
  ///
  /// \param type
  /// \return
  bool setProjectionType(ProjectionType type) noexcept;

  void setBathyPath(QString path);
  void setContourPath(QString path, int spacing);
  void setGeoImagePath(QString path);
  void setTargetPath(QString path);
  void setImagePath(QString, QRectF);

  void setDiveNumber(int num);
  void setVehicleName(QString name);

  void clearUnderlayPreloads();
  void clearTargetPreloads();

private:
  /// Alvin XY Origin point.
  QPointF m_alvinxy_origin;
  
  /// Start XY coords. 
  QPointF m_startxy;

  /// Projection Type
  ProjectionType m_projection_type;

  /// Values for auto loading layers as required by operations
  QString bath_preload_path;
  QString image_preload_path;
  QString geoimage_preload_path;
  QString contour_preload_path;
  QString image_rect_string;
  QString target_preload_path;
  int contour_preload_spacing = 0;

  QRectF image_preload_rect;

  //Dive number and vehicle name, optional settings for remote sending
  int dive_number = 0;
  QString vehicle_name;


signals:
  /// \brief Emitted whe the origin point changes.
  ///
  /// "No origin point" is represented by QPointF(nan, nan) and can be
  /// checked using NavGMapScene::isValidAlvinXYOrigin
  ///
  /// \param origin
  void alvinXYOriginChanged(QPointF origin);

  /// \brief Emitted when the projection type changes.
  ///
  /// \param type
  void projectionTypeChanged(ProjectionType type);

  void imagePreloadReady(QString,QRectF);
  void geoimagePreloadReady(QString);
  void bathyPreloadReady(QString);
  void contourPreloadReady(QString,int);
  void targetPreloadReady(QString);
};

} // namespace navg

#endif // NAVG_NAVGMAPSCENE_H
