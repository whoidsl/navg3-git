/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
// Created by J. Vaccaro 6/26/18

#ifndef MAPLAYERLISTMODEL_H
#define MAPLAYERLISTMODEL_H

#include "dslmap/BathyLayer.h"
#include "dslmap/BeaconLayer.h"
#include "dslmap/MapLayer.h"
#include "dslmap/MapScene.h"
#include "dslmap/SymbolLayer.h"
#include <QObject>
#include <QtCore>

/// MapLayerListModel
///
/// Maintains a representation of the layers in a given dslmap MapScene
/// Signals changes in the underlying MapScene, like deleting and adding layers
/// Signals changes in underlying AbstractMapLayers, like changing the Z-value
/// Created for use in a layer-editing NavG plugin

// using LayerHash = QHash< QUuid, dslmap::MapLayer * >;

namespace navg {

QString typeName(int type);

class MapLayerData
{
  /// Each layer is stored in a MapLayerData object
public:
  MapLayerData();
  MapLayerData(dslmap::MapLayer* layer_ptr);
  MapLayerData(QString _name, QUuid _id, qreal _Z, bool _isValid = false,
               bool _isVisible = false);
  ~MapLayerData();
  //    dslmap::MapLayer::AbstractMapLayerType type;
  QString name;
  QUuid id;
  qreal Z;
  bool isValid;
  bool isVisible;
  /// This operator allows for easy sorting by Z value when a new layer is added
  bool operator<(const MapLayerData& other) const { return Z > other.Z; }
  bool operator>(const MapLayerData& other) const { return Z < other.Z; }
  void print();
  bool reload(dslmap::MapLayer* layer_ptr);
  QVariant dataDisplay() const;
  QVariant dataEdit() const;
  QVariant dataForeground() const;
};

class MapLayerListModel : public QAbstractListModel
{
  Q_OBJECT
public:
  MapLayerListModel(dslmap::MapScene* scene, QObject* parent = 0);
  ~MapLayerListModel();

  /// Necessary methods to make a non-abstract ListModel
  int rowCount(const QModelIndex& parent) const override;
  int rows();

  /// Only return a valid QVariant if DisplayRole of EditRole
  QVariant data(const int row, int role) const;
  QVariant data(const QModelIndex& index, int role) const override;
  Qt::DropActions supportedDropActions() const override;

  /// Edit the correct field (layer name) when double-clicking the display value
  bool setData(const QModelIndex& index, const QVariant& value,
               int role) override;

  Qt::ItemFlags flags(const QModelIndex& index) const override;

private:
  bool m_zbysort;
  dslmap::MapScene* m_scene;
  QList<MapLayerData> m_layers;

public slots:
  /// Callbacks for connections made in dialog

  // swap the locations of two layers in the modellist, corresponding with their
  // Z-order
  QMenu* getMenu(int row);
  QMenu* getMenu(QModelIndex index);
  void zoomTo(int row);
  void zoomTo(QModelIndex index);
  void orderSwap(int row1, int row2);
  void orderSwap(QModelIndex index1, QModelIndex index2);
  void top(int row);
  void bottom(int row);
  bool removeRows(int row, int count, const QModelIndex& parent) override;
  void sortByZ();
  void zBySort();
  void showProperties(QModelIndex index);
  void changeVisible(int row);

private slots:
  /// Callbacks for MapScene signals (connections made in model)
  void scene_layerAdded_layer(dslmap::MapLayer* layer);
  void scene_layerAdded_uuid(const QUuid& uuid);
  void scene_layerRemoved_layer(dslmap::MapLayer* layer);
  void scene_layerRemoved_uuid(const QUuid& uuid);

  /// Callbacks for MapLayer signals
  void zChanged();
  void nameChanged(QString new_name);
  void visibleChanged();

signals:
  /// Call MapScene actions using these signals
  /// Don't change model until MapScene data changes
  void layerRemoved(dslmap::MapLayer* layer);
  void layerRemoved(const QUuid& uuid);
};
}

#endif // MAPLAYERLISTMODEL_H
