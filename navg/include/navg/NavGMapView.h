/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef NAVG_NAVGMAPVIEW_H
#define NAVG_NAVGMAPVIEW_H

#include <dslmap/MapView.h>
#include <dslmap/MarkerItem.h>
#include <dslmap/MapLayer.h>

#include <QLoggingCategory>
#include <QDir>


#include <memory>

enum MOUSE_MODE
{
  PAN_MODE,
  CURSOR_MODE,
  RANGE_MODE,
  PLUGIN_CONTROL_MODE,
};

namespace navg {

class ToolRangeBearing;

Q_DECLARE_LOGGING_CATEGORY(navgmapview);

class NavGMapView final : public dslmap::MapView
{

  Q_OBJECT

public:
  /// \brief  Graticule Projection type.
  enum class GraticuleType
  {
    LatLon, //!< WGS84 Lat/Lon (default)
    UTM,    //!< UTM
    AlvinXY //!< Alvin XY (requires AlvinXY origin on attached NavGMapScene
  };
  Q_ENUM(GraticuleType);

  explicit NavGMapView(QWidget* parent = Q_NULLPTR);
  ~NavGMapView() override;

  void loadSettings(QSettings* settings);
  void saveSettings(QSettings& settings);

  bool rangeBearingToolEnabled() const noexcept;
  void setRangeBearingToolEnabled(bool enabled);
  // TODO:  Move this method into something more reasonable before calling it
  // good.
  /// \brief Widget for target select, then adds new target layer.
  ///
  /// \param target
  void showTargetImportDialog();
  dslmap::MarkerItem* showSingleTargetDialog();

  /// \ This drops a cursor, not a 'target'
  void dropTarget();

  /// \brief A setter for m_currentpos for use with cursor placement
  void setCurrentPosition(const QPointF pos);

  /// \brief returnns a copy of the position of m_currentpos
  QPointF getCurrentPosition();

  /// \brief returns a const pointer to m_targetcursor
  const dslmap::MarkerItem* getTargetCursor();

  /// \brief Places a target at specified position
  void placeTargetAtPosition(QPointF position, QString layer_name = "New Target Layer Name");

  /// \brief Return the projection type used for the map graticule marks.
  GraticuleType graticuleProjectionType() const noexcept;

public slots:
  /// \brief Set the projection type used for the map graticule marks.
  ///
  /// \param type
  /// \return
  bool setGraticuleProjectionType(GraticuleType type) noexcept;
  
  /// \brief public slot to zoom into the first layer added onto a navg mapscene
  void zoomFirstLayer(dslmap::MapLayer *layer);

  /// \brief set the left click mouse mode
  void setMouseMode(MOUSE_MODE mode);

  /// \brief Gets the current left click mouse mode
  MOUSE_MODE getMouseMode();

  /// \brief set the color of the target cursor
  void setCursorColor(QColor color);

  /// \brief set the color of the range and bearing tool
  void setRangeBearingColor(QColor color);

  /// \brief Returns the color of the target cursor
  QColor cursorColor();

  /// \brief Returns the color of the range and bearing tool
  QColor rangeBearingColor();

  /// Sets an internal scene settings value to be able to load target layers
  /// automatically.
  /// This was put in here because I needed access to targets layers that were
  /// created via the single target dialog
  void setSceneTarget(QString filename);

  void hookUnderlayLayers(dslmap::MapLayer *layer);

  void setTargetFileExportEnabled(bool enabled);

  void setTargetFileDirectoryChanged(const QString& filepath);

protected:
  void drawForeground(QPainter* painter, const QRectF& rect) override;

  void wheelEvent(QWheelEvent* event) override;
  void mousePressEvent(QMouseEvent* event) override;
  void mouseDoubleClickEvent(QMouseEvent* event) override;
  void mouseMoveEvent(QMouseEvent* event) override;
  void mouseReleaseEvent(QMouseEvent* event) override;
  void contextMenuLaunch(QMouseEvent* event);

  //void autosaveTargetExport(dslmap::SymbolLayer *layer);
  void autosaveTargetExport(QVector<QVector<QString>> tableData, QString layername);

private:
  // Target Cursor
  QPointF m_currentpos;
  dslmap::MarkerItem* m_targetcursor = nullptr;
  QColor m_cursor_color = Qt::white;
  QColor m_rangebearing_color = Qt::red;

  // General mouse-interaction variables
  QPoint m_mouse_p1;
  QPoint m_mouse_p2;

  // Target export variables
  QString targetfile_path;
  QDir default_path = QString::fromStdString("/data/targetfiles/");
  bool targetexport_enabled = false;
  QString filepath;

  // Range/Bearing tool variables
  ToolRangeBearing* m_tool_range_bearing;
  bool m_tool_range_bearing_enabled = false;
  bool m_tool_range_bearing_visible = false;

  // Logic to launch contextMenu after right click and drag
  bool m_launch_context_menu = false;

  // Graticule Projection Type
  GraticuleType m_graticule_projection_type = GraticuleType::LatLon;

  // Mouse mode for left click behavior
  MOUSE_MODE m_mode = PAN_MODE;

  // plugin mode mouse is depressed for event
  bool plugin_pressed = false;

signals:
  /// \brief Emitted when the graticule type changes.
  void graticuleTypeChanged(GraticuleType type);
  /// \brief Emitted when mouse registers double click on map scene.
  void mouseDoubleClickedOnScene(QPointF cursor);
  
  /// \brief Emitted when target cursor is dropped onto scene or moved
  void cursorDropped(QPointF cursor);

  void plugin_held_event();
  void mouseStartPos(QPoint pos);
  void mouseMovePos(QPoint pos);
};

} // namespace navg

#endif // NAVG_NAVGMAPVIEW_H
