/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef NAVG_TOOLRANGEBEARING_H
#define NAVG_TOOLRANGEBEARING_H

#include <QBrush>
#include <QObject>
#include <QPen>

namespace dslmap {
class MapView;
}
namespace navg {

///\brief Draw a range/bearing indicator on the map.
class ToolRangeBearing : public QObject
{
  Q_OBJECT

signals:

  ///\brief Emitted when the pen color changes
  void colorChanged(QColor color);

  ///\brief Emitted when drawing XY lines is toggled.
  void drawXYChanged(bool);

  ///\brief Emitted when drawing watch circles is toggled.
  void drawWatchCircleChanged(bool);

public:
  explicit ToolRangeBearing(QObject* parent = nullptr);
  ~ToolRangeBearing() override;

  ///\brief Set the line and label color for the tool.
  ///
  /// \param color
  void setColor(QColor color);

  ///\brief Get the color used for drawing lines and labels.
  ///
  /// \return
  const QColor color() const;

  ///\brief True if drawing XY lines is enabled
  ///
  /// \return
  bool xyEnabled() const;

  ///\brief True if drwaing watch circles is enabled.
  ///
  /// \return
  bool watchCircleEnabled() const;

  ///\brief Draw the range bearing indicator.
  ///
  /// Should be called from within dslmap::MapView::drawForeground
  ///
  /// \param view
  /// \param painter
  /// \param rect
  void draw(const dslmap::MapView* view, QPainter* painter,
            const QRectF& rect) const;

  /// The start point of the range/bearing line.
  QPoint p1() const;

  /// The end point of the range/bearing line.
  QPoint p2() const;

public slots:
  ///\brief Enable drawing XY lines
  ///
  /// Will draw X and Y components of the range/bearing line
  ///
  /// \param enabled
  void setXYEnabled(bool enabled);

  /// \brief Enable drawing watch circles
  ///
  /// Will draw a watch circle using the current range as the radius.
  ///
  /// \param enabled
  void setWatchCircleEnabled(bool enabled);

  /// \brief Set the start point of the range/bearing line.
  ///
  /// \param p1
  void setP1(const QPoint& p1);

  /// \brief Set the end point of the range/bearing line
  void setP2(const QPoint& p2);

  /// \brief Set the position shift offset of text
  void setTextOffset(const int x, const int y);

private:
  QPen m_pen = QPen{ Qt::red }; //!< Pen used for range/bearing indicator
  QBrush m_label_fill_brush =
    QBrush{ QColor{ 0, 0, 0, 150 },
            Qt::SolidPattern };       //!< label background fill brush
  bool m_xy_enabled = true;           //!< Draw XY line option
  bool m_watch_circle_enabled = true; //!< Draw watch circle option
  QPoint m_p1;                        //!< range/bearing start position.
  QPoint m_p2;                        //!< range/bearing end position

  int m_shift_x = 0;  //Shift position offsets for text labels
  int m_shift_y = 0;
};
}
#endif // NAVG_TOOLRANGEBEARING_H
