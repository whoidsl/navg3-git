/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 9/26/17.
//

#ifndef NAVG_COREPLUGININTERFACE_H
#define NAVG_COREPLUGININTERFACE_H

#include <QtPlugin>

class QSettings;
class QAction;

#define CorePluginInterface_iid "edu.whoi.dsl.navg.coreplugininterface/1.0"

namespace dslmap {
class MapView;
}

namespace navg {

/// \brief Core interface for NavG Plugins
///
/// Plugins make up the majority of the NavG application. They represent
/// modular, self-contained, limited-scope functions that can share
/// information amongst other plugin instances.  That's a mouthful, but what
/// it really means is:
///
/// - Plugins should do one thing and do it well
/// - Plugins can communicate with each other ONLY through signals and slots
///
class CorePluginInterface
{

public:
  virtual ~CorePluginInterface() = default;

  /// \brief Load settings
  ///
  /// Loads plugin settings from a QSettings object.  This method may be
  /// called when NavG starts up to reinitialize a plugin from a previously
  /// saved state.
  ///
  /// \param settings
  virtual void loadSettings(QSettings* settings) = 0;

  /// \brief Save settings
  ///
  /// Saves plugin settings to a QSettings object.  Settings saved this way
  /// can be used later by PluginInterface::loadPluginSettings to restore or
  /// reinitialize the plugin.
  ///
  /// \param settings
  virtual void saveSettings(QSettings& settings) = 0;

  /// \brief List of plugin actions that can be added to a menu bar
  ///
  /// \return
  virtual QList<QAction*> pluginMenuActions() = 0;

  /// \brief List of plugin actions that can be added to a tool bar.
  ///
  /// \return
  virtual QList<QAction*> pluginToolbarActions() = 0;
public slots:

  /// \brief Form connections with another plugin
  ///
  /// This slot is called by NavG whenever it loads a new plugin.  This
  /// method should examine the provided plugin and make any desired
  /// connections to the plugin's signals.
  ///
  /// You should **NOT** keep references to the provided plugin pointer as it
  /// may go away.  If you must, wrap the provided QObject pointer in a QPointer
  /// and check that is still valid before use.
  ///
  /// \param plugin
  virtual void connectPlugin(const QObject* plugin,
                             const QJsonObject& metadata) = 0;

  /// \brief Set the dslmap::MapView for the plugin
  ///
  /// This slot is called by NavG when it loads the plugin.  The provided
  /// `QPointer` `dslmap::MapView` object provides access to the main map view
  /// of the
  /// NavG application and can be safely stored by the plugin.
  virtual void setMapView(QPointer<dslmap::MapView> view) = 0;
};
}
Q_DECLARE_INTERFACE(navg::CorePluginInterface, CorePluginInterface_iid)
#endif // NAVG_COREPLUGININTERFACE_H
