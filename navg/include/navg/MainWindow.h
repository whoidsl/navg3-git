/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef NAVG_MAINWINDOW_H
#define NAVG_MAINWINDOW_H

#define NORMAL_ICON_SIZE 24
#define LARGE_ICON_SIZE 48

#include <QJsonObject>
#include <QLoggingCategory>
#include <QMainWindow>
#include <QSettings>
#include <QWidgetAction>

#include "MapLayerListModel.h"
#include "MapLayerEditWidget.h"
#include "NavGMapView.h"
#include "ToolRangeBearing.h"
#include <dslmap/ColorPickButton.h>
#include <memory>

// Declared loggers
class QDockWidget;
class QPluginLoader;

namespace navg {
class NavGDockWidget;

Q_DECLARE_LOGGING_CATEGORY(navg)

class MainWindow : public QMainWindow
{
  Q_OBJECT
signals:
  /// @brief Map only mode is enabled or disabled.
  void mapOnlyModeToggled(bool enabled);

  void iconSizeChanged(int size);

  void settingsFileChanged(const QString &filename);

  void targetfileDirectoryChanged(const QString &filepath);

  void targetfileExportEnabled(bool enabled);

public:
  static const int REBOOT_CODE =
    555555; //!< Returned when a application restart is requested.

  explicit MainWindow(bool UseOpenGL = true, QWidget* parent = 0, QString arg_settings_file="");
  ~MainWindow() override;

  /// Override close window event
  void closeEvent(QCloseEvent *event);

  /// @brief Save NavG settings to a file
  ///
  /// This method will save NavG settings to a file.
  /// values).
  ///
  /// \param filename
  /// \return
  bool saveSettings(const QString& filename);

  bool saveSettings();

  /// @brief Load NavG settings to a file
  ///
  /// This method will load NavG settings from a file.  This does *not* restore
  /// the plugin window locations,
  /// only the various settings used by the currently loaded plugins (e.g. port
  /// values).
  ///
  /// \param filename
  /// \return
  QSettings::Status loadSettings(const QString& filename);

  /// @brief Return whether map-only mode is enabled or disabled.
  ///
  /// \return
  bool mapOnlyModeEnabled() const noexcept;
  
  // true inhibits ability to save as and import ini. false undoes this
  void inhibitIniSetting(bool state);

protected slots:

  /// @brief Restart the application
  ///
  /// Request an application restart by exiting with a return code of
  /// MainWindow::REBOOT_CODE
  void restart();

  /// @brief Enable or disable map-only mode
  ///
  /// In map-only mode only the main central map is displayed.  All toolbars,
  /// menus, status bars, and docked widgets are hidden.
  /// \param enabled
  void setMapOnlyModeEnabled(bool enabled);

  void saveSettingsFile(const QString &filename);

protected:
  bool saveSettings(QSettings& settings);

  void createActions();

  /// \brief Create main window menus
  void createMenus();
  
  /// \brief Create action widgets for use in window toolbars
  void createWidgets();

protected slots:

  /// Set mapview variables to be allow to preload underlays on startup
  void setSceneImage(QString filename, QPointF topleft, QPointF bottomright);
  void setSceneGeoimage(QString filename);
  void setSceneBathy(QString filename);
  void setSceneTarget(QString filename);
  void setSceneContour(QString filename, int spacing);

private:
  void loadBathyLayerDialog();
  void loadContourLayerDialog();
  void loadImageLayerDialog();
  void loadGeoreferenceImageLayerDialog();

  void loadBathyLayer(QString filename);
  void loadContourLayer(QString filename, int spacing=10);
  void loadImageLayer(QString filename, QRectF rect, QString layername="");
  void loadGeoreferenceImageLayer(QString filename, QString layerName="", QColor color=QColor());
  void loadTargetLayer(QString filename);

  void deleteImageSettings();
  void saveImageSettings(QSettings& settings);
  void loadVectorLayerDialog();
  void loadTargetLayerDialog();
  
  void takeScreenshot();

  void scanPlugins();
  void addPluginMenuActions(const QString& name, QPluginLoader* loader);

  void loadPlugin(const QString& name, QPluginLoader* loader);
  void unloadPlugin(const QString& name, QPluginLoader* loader);

  void scaleActionIcons(QToolBar* toolbar, int size);

  /// \brief Create the main window file menu.

  navg::NavGMapView* m_map_view = nullptr;

  std::unique_ptr<QSettings> m_settings;
  QMenu* m_menu_layers = nullptr;
  QMenu* m_menu_map = nullptr;
  QMenu* m_menu_settings = nullptr;
  QMenu* m_menu_plugin = nullptr;
  QMenu* m_menu_map_graticule = nullptr;
  QMenu* m_menu_map_projection = nullptr;
  QMenu* m_menu_window = nullptr;
  QMenu* m_menu_view = nullptr;
  QMenu* m_menu_icon_size = nullptr;

  QToolBar* m_toolbar_map_interaction = nullptr;
  QToolBar* m_toolbar_layers = nullptr;

  QAction* m_action_add_bathymetry_layer = nullptr;
  QAction* m_action_add_contour_layer = nullptr;
  QAction* m_action_add_vector_layer = nullptr;
  QAction* m_action_add_image_layer = nullptr;
  QAction* m_action_add_target_layer = nullptr;
  QAction* m_action_add_georeference_image_layer = nullptr;
  QAction* m_action_toggle_layer_widget = nullptr;
  QAction* m_action_toggle_range_bearing_tool = nullptr;
  QAction* m_action_settings_import = nullptr;
  QAction* m_action_settings_save = nullptr;
  QAction* m_action_settings_save_as = nullptr;
  QAction* m_action_settings_set_screenshot_path = nullptr;
  QAction* m_action_settings_set_targetfile_enabled = nullptr;
  QAction* m_action_settings_set_targetfile_path = nullptr;
  QAction* m_action_show_plugin_about = nullptr;
  QAction* m_action_toggle_plugin_lock = nullptr;
  QAction* m_action_map_graticule_latlon = nullptr;
  QAction* m_action_map_graticule_utm = nullptr;
  QAction* m_action_map_graticule_alvinxy = nullptr;
  QAction* m_action_edit_alvinxy_origin = nullptr;
  QAction* m_action_map_scene_mercator = nullptr;
  QAction* m_action_map_scene_utm = nullptr;
  QAction* m_action_map_scene_alvinxy = nullptr;
  QAction* m_action_window_full_screen = nullptr;
  QAction* m_action_window_map_only = nullptr;
  QAction* m_action_set_max_zoom_limit = nullptr;
  QAction* m_action_view_normal_icons = nullptr;
  QAction* m_action_view_large_icons = nullptr;
  QAction* m_action_take_screenshot = nullptr;
  QAction* m_action_clear_mapscene_underlays = nullptr;
  QAction* m_action_clear_target_underlays = nullptr;

  QWidgetAction* m_action_cursor_color = nullptr;
  QWidgetAction* m_action_rangebearing_color = nullptr;

  dslmap::ColorPickButton* cursor_color_widget = nullptr;
  dslmap::ColorPickButton* rangebearing_color_widget = nullptr;

  NavGDockWidget* m_layer_widget = nullptr;
  MapLayerEditWidget* m_layer_edit_widget = nullptr;
  navg::MapLayerListModel* m_layer_model = nullptr;

  QString image_path;
  QString image_layer_name;
  QRectF image_rect;
  
  QString screenshot_directory;
  QString targetfile_directory;
  bool targetfile_enabled = false;

  struct Plugin;
  QMap<QString, QPluginLoader*> m_plugins;
  QStringList m_plugin_paths;

  bool m_map_only_mode_enabled = false;
};

} // namespace navg
#endif // NAVG_MAINWINDOW_H
