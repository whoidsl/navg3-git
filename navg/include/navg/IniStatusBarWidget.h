/**
* Copyright 2022 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef INISTATUSBARWIDGET_H
#define INISTATUSBARWIDGET_H

#include <QWidget>

#include "NavGMapScene.h"
#include "NavGMapView.h"

class QToolButton;
class QLabel;

namespace navg {

///\brief Simple widget to display a the loaded Config INI for navg
///
/// Used in the NavG status bar.
class IniStatusBarWidget : public QWidget
{
  Q_OBJECT

public:
  explicit IniStatusBarWidget(QWidget* parent = 0);
  ~IniStatusBarWidget() override;

public slots:

  void setFileName(const QString &name) noexcept;
  void setDirPath(const QString &path) noexcept;
  void showFullPath(bool show);


  /// \brief Set Path labels for loaded ini
  ///
  /// \param value
  void setConfigFile(const QString &full_path) noexcept;

private:
  QToolButton* path_button;
  QLabel* filename_label;

  QString stored_path;
};
}

#endif // IniSTATUSBARWIDGET_H
