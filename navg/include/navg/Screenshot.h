// Created by tmjoyce on 06/08/21.

#ifndef NAVG_SCREENSHOT_H
#define NAVG_SCREENSHOT_H

#include <QLoggingCategory>
#include <QPixmap>
#include <QWidget>
#include <QScreen>
#include <QWindow>
#include <QDir>
#include <QFileDialog>
#include <QStandardPaths>
#include <QImageWriter>
#include <QMessageBox>
#include <QGuiApplication>
#include <QSettings>

// Created with the help of 
// qt doc examples

namespace navg {

Q_DECLARE_LOGGING_CATEGORY(navg_screenshot)

class Screenshot : public QWidget
{
    Q_OBJECT

public:
    Screenshot();

    /// \brief Open a file dialog and save screenshot to location
    void saveScreenshot();
    /// \brief Save a screenshot with a given name to a given directory
    void saveScreenshot(QString name, QString directory = "");

    /// \brief Capture the screen and save it in member pixmap
    void shootScreen();

    /// \brief Return a copy of the screenshot pixmap
    QPixmap getScreenshotPixmap();

private:
    QPixmap originalPixmap;
    QDir default_path = QDir::currentPath() + "/Screenshots/";
};

}

#endif //NAVG_SCREENSHOT