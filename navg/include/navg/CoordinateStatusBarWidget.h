/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef CoordinateStatusBarWidget_H
#define CoordinateStatusBarWidget_H

#include "NavGMapScene.h"
#include "NavGMapView.h"

#include <dslmap/DslMap.h>

#include <QWidget>
class QLabel;

namespace navg {

///\brief Simple widget to display a projection name and position.
///
/// Used in the NavG status bar.
class CoordinateStatusBarWidget : public QWidget
{
  Q_OBJECT

public:
  /// \brief Unit type for coordinate system.
  enum class CoordinateType
  {
    Meters,
    LatLon,
  };

  explicit CoordinateStatusBarWidget(QWidget* parent = 0);
  ~CoordinateStatusBarWidget() override;

public slots:
  /// \brief Set the projection name from a graticule type
  ///
  /// \param type
  void setGraticuleProjection(NavGMapView::GraticuleType type) noexcept;

  /// \brief Set the projection name from a scene projection type
  ///
  /// \param type
  void setSceneProjection(NavGMapScene::ProjectionType type) noexcept;

  /// \brief Set the coordinate value
  ///
  /// \param value
  void setCoordinateValue(QPointF value) noexcept;

  /// \brief Set the format for showing degree values
  ///
  /// \param display_type
  void setLatLonFormat(dslmap::LatLonFormat format) noexcept;

  /// \brief Get the format for showing degrees values
  ///
  /// \return
  dslmap::LatLonFormat latLonFormat() const noexcept;

private:
  CoordinateType m_coordinate_type;
  dslmap::LatLonFormat m_latlon_format = dslmap::LatLonFormat::DD_NE;
  QLabel* m_name_label;
  QLabel* m_value_label;
};
}

#endif // CoordinateStatusBarWidget_H
