/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
// Created by J. Vaccaro 6/26/18
// Based on tutorials:
// Model/View https://www.youtube.com/watch?v=uDC9L4T59bM
// Action https://www.youtube.com/watch?v=uLF9KWUR9ro
// Context menu
// https://forum.qt.io/topic/31233/how-to-create-a-custom-context-menu-for-qtableview/3

#ifndef MapLayerEditWidget_H
#define MapLayerEditWidget_H

#include "MapLayerListModel.h"
#include <QDialog>
#include <QMenu>
#include <QToolBar>
#include <QtCore>
#include <QtGui>

namespace navg {

namespace Ui {
class MapLayerEditWidget;
}

class MapLayerEditWidget : public QDialog
{
  Q_OBJECT

public:
  explicit MapLayerEditWidget(MapLayerListModel* _layer_model,
                              QWidget* parent = 0);
  ~MapLayerEditWidget();

public slots:
  void setIconSize(int size);

private slots:
  void customMenuRequested(QPoint pos);
  void do_rectAct();
  void do_deleteAct();
  // void do_copyAct();
  // void do_newAct();
  void do_editAct();
  void do_upAct();
  void do_downAct();
  void do_topAct();
  void do_bottomAct();
  //  void do_showAct(bool checked);
  //  void do_sortAct();
  void do_propAct();
  void do_visibleAct();
  void scaleActionIcons(QToolBar* toolbar, int size);

private:
  void setupMenus();
  Ui::MapLayerEditWidget* ui;
  MapLayerListModel* layer_model;
  QMenu* menu;
  QToolBar* toolBar;
};
}

#endif // MapLayerEditWidget_H
