# NavG - a Navigation GUI for WHOI/DSL vehicles.

## Required Dependencies

NavG targets the Ubuntu 18.04 LTS release and the Ubuntu 20.04 LTS release.  All dependencies (both required and optional) can be installed using 
the standard ubuntu repositories.  The mapping library requires:

- C++14 support (clang 3.8, gcc 5.4 and above)
- Qt 5.5 (5.5 is the *target* release.  It may work with earlier versions of Qt5)
- libdslmap (associated mapping library)
- cmake (3.5 and above)

### Optional Development Dependencies

NavG uses a few optional dependencies for development:

- doxygen (optional, for documentation)
- clang-format (optional, for source formatting during development)
  
### Installing CMake 3.5 or newer
CMake 3.5 is the CMake version associated with Ubuntu 16.04. Ubuntu 18.04 and Ubuntu 20.04 provide newer CMake versions and are supported.

If you need to install CMake manually, see 
[cmake.org](https://cmake.org/download/) to download a compatible version.

### Ubuntu 18.04 and 20.04 apt one-liner
There exist scripts to install required packages for your machine 
in navg3-git/ci/ named ubuntu1804-requirements.sh and ubuntu2004-requirements.sh. 
        
## Building NavG

Use the standard cmake build sequence to build libdslmap:

    mkdir build && cd $_
    cmake ..
    make
    
If you want to compile a debug version, replace `cmake` command with `cmake .. -DCMAKE_BUILD_TYPE=Debug`

If Qt is installed in a non-standard location you may need to define `Qt5_DIR`.  For example,
if Qt is installed into `/opt/Qt5.5.0`:

    mkdir build && cd $_
    cmake .. -DQt5_DIR=/opt/Qt5.5.0/5.5/gcc_64/lib/cmake/Qt5
    make     
    
Upon a successful build you should be able to run NavG from within the build
directory using:

    ./src/navg

NavG3 may also be installed system wide so that you can run the program from any terminal location. After running make, do....

    sudo make install
    navg

If you were to receive an error complaining about not being able to find libnavg.so

    sudo ldconfig
    navg
    
### Build Options

Testing is enabled by default.  To disable building tests, pass `-DBUILD_TESTS=OFF` when running `cmake`

Packaged plugins are built by default.  To disable building plugins, pass `-DBUILD_PLUGINS=OFF` when running `cmake`

    
## Using QtCreator

To use QtCreator with this project you will need to do the following:

- Download Qt 5.9 from [Qt5.9 LTS](https://www1.qt.io/qt5-9/).  Yes, you need 
  yet another version of Qt because 5.5 just does not have good enough cmake
  support.

The best way to do this is to clone the default kit and change what's needed.
If you need to add a non-system version of cmake, click the `CMake` tab and click the `Add` button.  
Then fill in details for where cmake is located on your machine.

Then you should be able to open the project in QtCreator using `File->Open 
File or Project` and select the `CMakeLists.txt` file in this directory.  
Use the new kit you just created when prompted to configure the project.
   
File browsing is a bit messy in Qt.  To get to the sources, look in the 
`<Source Directory>` folder for a target.  For example, `dslmap-><Source 
Directory>` will be the root directory of the `dslmap` sources.


## Additional Documentation

- [CHANGELOG.markdown](CHANGELOG.markdown)
- [CONTRIBUTING.markdown](CONTRIBUTING.markdown) Guidelines for contributing patches and updates to this library.
- [PLUGINS.markdown](PLUGINS.markdown) Guidelines for developing new plugins for NavG
- [USAGE.markdown](USAGE.markdown) Help using NavG.
