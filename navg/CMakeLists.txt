cmake_minimum_required(VERSION 3.5)
project(navg LANGUAGES CXX VERSION 3.0.0)

option(BUILD_TESTS "Build Tests" ON)
option(BUILD_PLUGINS "Build plugins" ON)
option(BUILD_SHARED_LIBS "Build shared libraries" ON)

list(INSERT CMAKE_MODULE_PATH 0 ${PROJECT_SOURCE_DIR}/cmake)

find_package(Qt5 5.5 REQUIRED COMPONENTS Widgets)
find_package(dslmap REQUIRED)
find_package(ClangFormat)
find_package(Doxygen)

if(CLANG_FORMAT_FOUND)
  add_custom_target(navg-clang-format
    COMMENT "Run clang-format on all project C++ sources"
    WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
    COMMAND
      find .
      -iname '*.h' -o -iname '*.h.in' -o -iname '*.cpp'
      | xargs ${CLANG_FORMAT_EXECUTABLE} -i
    )
endif(CLANG_FORMAT_FOUND)

if(DOXYGEN_FOUND)
  find_file(QT_DOCUMENT_ROOT qtcore.tags
          PATHS /usr/share /usr/doc
          PATH_SUFFIXES qt5/doc/qtcore
          NO_DEFAULT_PATH
          )
  if(QT_DOCUMENT_ROOT)
    get_filename_component(QT_DOCUMENT_ROOT ${QT_DOCUMENT_ROOT} DIRECTORY)
    get_filename_component(QT_DOCUMENT_ROOT ${QT_DOCUMENT_ROOT} DIRECTORY)
  else(QT_DOCUMENT_ROOT)
    message("Unable to find Qt document tags, doxygen will not have links to Qt structures")
  endif(QT_DOCUMENT_ROOT)

  configure_file(Doxyfile.in ${PROJECT_BINARY_DIR}/Doxyfile @ONLY)
  add_custom_target(navg-doxygen
          COMMENT "Generate documentation with Doxygen"
          WORKING_DIRECTORY ${PROJECT_BINARY_DIR}
          COMMAND ${DOXYGEN_EXECUTABLE} ${PROJECT_BINARY_DIR}/Doxyfile
          COMMENT "Generating navg API with doxygen"
          VERBATIM
          )

else(DOXYGEN_FOUND)
  message("Doxygen not found, skipping documents")
  set(BUILD_DOCS OFF)
endif(DOXYGEN_FOUND)

add_subdirectory(src)

if (BUILD_PLUGINS)
  add_subdirectory(plugins)
endif (BUILD_PLUGINS)
