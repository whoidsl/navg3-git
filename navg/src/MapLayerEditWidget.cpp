/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "MapLayerEditWidget.h"
#include "ui_MapLayerEditWidget.h"
#include <QMessageBox>

namespace navg {

MapLayerEditWidget::MapLayerEditWidget(MapLayerListModel* _layer_model,
                                       QWidget* parent)
  : QDialog(parent)
  , ui(new Ui::MapLayerEditWidget)
  , layer_model(_layer_model)
{
  ui->setupUi(this);

  ui->listView->setModel(layer_model);
  ui->listView->setCurrentIndex(layer_model->index(0, 0));
  ui->listView->setDragDropMode(QAbstractItemView::InternalMove);
  ui->listView->setAcceptDrops(true);
  ui->listView->setDropIndicatorShown(true);
  ui->listView->setDefaultDropAction(Qt::MoveAction);

  setupMenus();

  ui->listView->setContextMenuPolicy(Qt::CustomContextMenu);
  connect(ui->listView, SIGNAL(customContextMenuRequested(QPoint)), this,
          SLOT(customMenuRequested(QPoint)));
}

void
MapLayerEditWidget::setupMenus()
{
  QAction* rectAct = new QAction(QIcon{ ":GIS-icons/24x24/zoom-layer.png" },
                                 QString{ "Zoom" }, this);
  rectAct->setToolTip("Zoom to Layer Rect");
  QAction* deleteAct = new QAction(QIcon{ ":GIS-icons/24x24/layer-remove.png" },
                                   QString{ "Delete" }, this);
  deleteAct->setToolTip("Delete Layer");
  QAction* upAct = new QAction(QIcon{ ":GIS-icons/24x24/layer-up.png" },
                               QString{ "Up" }, this);
  upAct->setToolTip("Bring Layer Forward");
  QAction* downAct = new QAction(QIcon{ ":GIS-icons/24x24/layer-down.png" },
                                 QString{ "Down" }, this);
  downAct->setToolTip("Send Layer Backward");
  QAction* topAct = new QAction(QIcon{ ":GIS-icons/24x24/layer-top.png" },
                                QString{ "Top" }, this);
  topAct->setToolTip("Bring Layer to Front");
  QAction* bottomAct = new QAction(QIcon{ ":GIS-icons/24x24/layer-bottom.png" },
                                   QString{ "Bottom" }, this);
  bottomAct->setToolTip("Send Layer to Back");
  QAction* propAct = new QAction(QIcon{ ":GIS-icons/24x24/layer-edit.png" },
                                 QString{ "Properties" }, this);
  propAct->setToolTip("Display Layer Properties");
  QAction* visibleAct = new QAction(QIcon{ ":GIS-icons/24x24/layer-show.png" },
                                    QString{ "Visible" }, this);
  visibleAct->setToolTip("Toggle the visibility of the layer");

  connect(rectAct, &QAction::triggered, this, &MapLayerEditWidget::do_rectAct);
  connect(upAct, &QAction::triggered, this, &MapLayerEditWidget::do_upAct);
  connect(downAct, &QAction::triggered, this, &MapLayerEditWidget::do_downAct);
  connect(topAct, &QAction::triggered, this, &MapLayerEditWidget::do_topAct);
  connect(bottomAct, &QAction::triggered, this,
          &MapLayerEditWidget::do_bottomAct);
  connect(deleteAct, &QAction::triggered, this,
          &MapLayerEditWidget::do_deleteAct);
  connect(propAct, &QAction::triggered, this, &MapLayerEditWidget::do_propAct);
  connect(visibleAct, &QAction::triggered, this,
          &MapLayerEditWidget::do_visibleAct);

  menu = new QMenu(this);

  menu->addAction(upAct);
  menu->addAction(downAct);
  menu->addAction(topAct);
  menu->addAction(bottomAct);
  menu->addAction(deleteAct);
  menu->addAction(rectAct);
  //  menu->addAction(propAct);
  //  menu->addAction(visibleAct);

  toolBar = new QToolBar(this);
  toolBar->addAction(deleteAct);
  toolBar->addAction(propAct);
  toolBar->addAction(upAct);
  toolBar->addAction(downAct);
  toolBar->addAction(topAct);
  toolBar->addAction(bottomAct);
  toolBar->addAction(visibleAct);
  toolBar->addAction(rectAct);

  ui->gridLayout->addWidget(toolBar);
}

MapLayerEditWidget::~MapLayerEditWidget()
{
  delete ui;
}

void
MapLayerEditWidget::customMenuRequested(QPoint pos)
{
  QModelIndex index = ui->listView->currentIndex();
  if (!index.isValid()) {
    QMessageBox::warning(this, "Oops!", "Please select a layer");
    return;
  }
  auto this_menu = new QMenu(this);
  for (auto action : menu->actions()) {
    this_menu->addAction(action);
  }
  this_menu->addSeparator();
  auto action_menu = layer_model->getMenu(index);
  if (action_menu) {
    for (auto action : action_menu->actions()) {
      this_menu->addAction(action);
    }
  }
  this_menu->popup(ui->listView->viewport()->mapToGlobal(pos));
}

void
MapLayerEditWidget::do_rectAct()
{
  QModelIndex index = ui->listView->currentIndex();
  if (!index.isValid()) {
    QMessageBox::warning(this, "Oops!", "Please select a layer");
    return;
  }
  layer_model->zoomTo(index);
}

void
MapLayerEditWidget::do_deleteAct()
{
  QModelIndex index = ui->listView->currentIndex();
  if (!index.isValid()) {
    QMessageBox::warning(this, "Oops!", "Please select a layer");
    return;
  }
  auto confirm = QMessageBox::warning(
    this, "Delete Layer?", "Confirming will permanently delete the layer",
    QMessageBox::Ok | QMessageBox::Cancel, QMessageBox::Cancel);
  if (confirm == QMessageBox::Ok) {
    layer_model->removeRow(index.row(), index);
  }
}

void
MapLayerEditWidget::do_editAct()
{
  QModelIndex index = ui->listView->currentIndex();
  if (!index.isValid()) {
    QMessageBox::warning(this, "Oops!", "Please select a layer");
    return;
  }
  ui->listView->edit(index);
}

void
MapLayerEditWidget::do_upAct()
{
  QModelIndex index = ui->listView->currentIndex();
  if (!index.isValid()) {
    QMessageBox::warning(this, "Oops!", "Please select a layer");
    return;
  }
  layer_model->orderSwap(index.row() - 1, index.row());
  ui->listView->setCurrentIndex(index.sibling(index.row() - 1, 0));
}

void
MapLayerEditWidget::do_downAct()
{
  QModelIndex index = ui->listView->currentIndex();
  if (!index.isValid()) {
    QMessageBox::warning(this, "Oops!", "Please select a layer");
    return;
  }
  layer_model->orderSwap(index.row(), index.row() + 1);
  ui->listView->setCurrentIndex(index.sibling(index.row() + 1, 0));
}

// void
// MapLayerEditWidget::do_sortAct()
//{
//  QModelIndex index = ui->listView->currentIndex();
//  if (!index.isValid()) {
//    QMessageBox::warning(this, "Oops!", "Please select a layer");
//    return;
//  }
//  layer_model->sortByZ();
//}

void
MapLayerEditWidget::do_propAct()
{
  QModelIndex index = ui->listView->currentIndex();
  if (!index.isValid()) {
    QMessageBox::warning(this, "Oops!", "Please select a layer");
    return;
  }
  layer_model->showProperties(index);
}

void
MapLayerEditWidget::do_topAct()
{
  QModelIndex index = ui->listView->currentIndex();
  if (!index.isValid()) {
    QMessageBox::warning(this, "Oops!", "Please select a layer");
    return;
  }
  layer_model->top(index.row());
  ui->listView->setCurrentIndex(layer_model->index(0, 0));
}

void
MapLayerEditWidget::do_bottomAct()
{
  QModelIndex index = ui->listView->currentIndex();
  if (!index.isValid()) {
    QMessageBox::warning(this, "Oops!", "Please select a layer");
    return;
  }
  layer_model->bottom(index.row());
  ui->listView->setCurrentIndex(layer_model->index(layer_model->rows() - 1, 0));
}

void
MapLayerEditWidget::do_visibleAct()
{
  QModelIndex index = ui->listView->currentIndex();
  if (!index.isValid()) {
    QMessageBox::warning(this, "Oops!", "Please select a layer");
    return;
  }
  layer_model->changeVisible(index.row());
}

void
MapLayerEditWidget::setIconSize(int size){
  scaleActionIcons(toolBar, size);
}

void
MapLayerEditWidget::scaleActionIcons(QToolBar* toolbar, int size){

  if (!toolbar) {
    return;
  }

  toolbar->setIconSize(QSize(size,size));
  
  auto actions = toolbar->actions();
  for (auto action : actions){
    if (action){
    QPixmap scaled_pix = action->icon().pixmap(size,size);
    scaled_pix = scaled_pix.scaled(size,size,Qt::KeepAspectRatio);

    QIcon icon = QIcon(scaled_pix);
    action->setIcon(icon);
    }
  }
}

} // NAMESPACE