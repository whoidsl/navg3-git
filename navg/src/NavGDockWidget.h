/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef NAVG_NAVGDOCKWIDGET_H
#define NAVG_NAVGDOCKWIDGET_H

#include <QDockWidget>

namespace navg {

/// @brief Simple wrapper around QDockWidget to handle map-only mode toggling.
///
/// A simple extension to QDockWidget that adds a single public slot:
///   - handleMapOnlyModeToggled(bool enabled)
///
/// Only the map is visible when Map Only mode is true.  We need to
/// hide all visible dock widgets when entering this mode.
///
/// When exiting the mode we wish to restore *only* the dock widgets
/// that were visible before, so we can't just call  setVisible() on all
/// dock widgets.
class NavGDockWidget : public QDockWidget
{
  Q_OBJECT

public:
  explicit NavGDockWidget(const QString& title, QWidget* parent = nullptr,
                          Qt::WindowFlags flags = 0);
  explicit NavGDockWidget(QWidget* parent = nullptr, Qt::WindowFlags flags = 0);

  ~NavGDockWidget() override;

public slots:
  /// @brief Handle Map Only mode state changes
  ///
  /// When entering map only mode, sets m_is_displayed to the return value of
  /// isVisible()
  /// and then hides the widget.
  ///
  /// When exiting map only mode, sets the widiget visible with
  /// setVisible(m_is_displayed).
  ///
  /// \param enabled
  void handleMapOnlyModeToggled(bool enabled);

private:
  bool m_is_displayed;
};
}

#endif // NAVG_NAVGDOCKWIDGET_H
