/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "NavGMapView.h"
#include "../include/navg/NavGMapView.h"
#include "NavGMapScene.h"
#include "ToolRangeBearing.h"

#include <dslmap/MapLayer.h>
#include <dslmap/MapScene.h>
#include <dslmap/Proj.h>
#include <dslmap/SingleTarget.h>
#include <dslmap/SymbolLayer.h>
#include <dslmap/TargetImport.h>

#include <QMenu>
#include <QWheelEvent>
#include <QMessageBox>

namespace navg {

Q_LOGGING_CATEGORY(navgmapview, "navg.mapview")

NavGMapView::NavGMapView(QWidget* parent)
  : dslmap::MapView(parent)
  , m_tool_range_bearing(new ToolRangeBearing(this))
{
  setGraticuleProjectionType(m_graticule_projection_type);
}

NavGMapView::~NavGMapView() = default;

void
NavGMapView::loadSettings(QSettings* settings)
{
  MapView::loadSettings(settings);
  auto value = settings->value("cursor_color");
  
  if (value.isValid()) {
    const QColor color = QColor::fromRgba(value.toUInt());
    if (color.isValid()) {
      m_cursor_color = color;
    }
    else {
      qCWarning(navgmapview) << "Failed to load crusor color settings";
    }
  }
  auto rangebearing_color_value = settings->value("rangebearing_color");
  if (rangebearing_color_value.isValid()) {
	  const QColor color = QColor::fromRgba(rangebearing_color_value.toUInt());
	  if (color.isValid()) {
		  m_rangebearing_color = color;
	  }
	  else {
		  qCWarning(navgmapview) << "Failed to load range bearing color settings";
}
}
}

void
NavGMapView::saveSettings(QSettings& settings)
{
  MapView::saveSettings(settings);
  settings.setValue("cursor_color", m_cursor_color.rgba());
  settings.setValue("rangebearing_color", m_rangebearing_color.rgba());
}

void
NavGMapView::wheelEvent(QWheelEvent* event)
{
  if (rangeBearingToolEnabled()){
    //block qwheel zooming event if range/bearing tool on;
    return;
  }
  const auto orig_anchor = transformationAnchor();
  setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
  if (event->angleDelta().y() > 0) {
    zoom(1, true);
  } else {
    zoom(-1, true);
  }
  setTransformationAnchor(orig_anchor);
}

void
NavGMapView::dropTarget()
{
  if (m_targetcursor == nullptr) {
    m_targetcursor = new dslmap::MarkerItem();
    //High z value to always draw on top
    m_targetcursor->setZValue(std::numeric_limits<qreal>::max());
    auto symbol = std::make_shared<dslmap::ShapeMarkerSymbol>(
      dslmap::ShapeMarkerSymbol::Shape::Cross, 50);
    symbol->setOutlineColor(m_cursor_color);
    m_targetcursor->setSymbol(symbol);
    m_targetcursor->setPos(m_currentpos);
    setCursorColor(m_cursor_color);
    scene()->addItem(m_targetcursor);
  } else {
    m_targetcursor->setZValue(std::numeric_limits<qreal>::max());
    m_targetcursor->setPos(m_currentpos);
  }
  emit cursorDropped(m_currentpos);
}

void 
NavGMapView::setCurrentPosition(const QPointF pos){
  m_currentpos = pos;
}

QPointF
NavGMapView::getCurrentPosition(){
  return m_currentpos;
}

void
NavGMapView::setTargetFileExportEnabled(bool enabled)
{
  targetexport_enabled = enabled;
}

void
NavGMapView::setTargetFileDirectoryChanged(const QString& filepath)
{
  targetfile_path = filepath;
}

void
NavGMapView::placeTargetAtPosition(QPointF position, QString layer_name)
{
  dslmap::MapScene* my_scene = qobject_cast<dslmap::MapScene*>(scene());
  if (!my_scene) {
    return;
  }

  dslmap::SymbolLayer* symbol_layer = nullptr;
  for (auto layer : my_scene->layers()) {
    if (layer->name() == layer_name) {
      symbol_layer = qobject_cast<dslmap::SymbolLayer*>(layer);
    }
  }

  // dont seem to need this block. Sometimes we do to get correct position
  // but we don't here for sure
  /*
  if (my_scene->projection()) {
    double x_val(position.x());
    double y_val(position.y());
    my_scene->projection()->transformFromLonLat(1, &x_val, &y_val);
    position.setX(x_val);
    position.setY(y_val);

  }
  */
  if (!symbol_layer) {

    // CREATE NEW SYMBOL LAYER

    symbol_layer = new dslmap::SymbolLayer();
    symbol_layer->setName(layer_name);

    QPainterPath path;
    symbol_layer->setShape(dslmap::ShapeMarkerSymbol::Shape::X, 15);
    auto symbol_ptr = symbol_layer->symbolPtr();
    symbol_ptr->setOutlineColor(Qt::red);
    symbol_ptr->setFillColor(Qt::red);
    symbol_ptr->setLabelColor(Qt::red);

    // Draw the markers scale invariant.

    symbol_layer->setScaleInvariant(true);

    if (my_scene->projection()) {
      symbol_layer->setProjection(my_scene->projection());
    }

    auto symbol_item = symbol_layer->addPoint(position, "", {});
    symbol_item->setFlag(QGraphicsItem::ItemIsMovable, false);
    symbol_layer->setLabelVisible(true);
    symbol_layer->setVisible(true);
    my_scene->addLayer(symbol_layer);

  } else {
    auto symbol_item = symbol_layer->addPoint(position, "", {});
    symbol_item->setFlag(QGraphicsItem::ItemIsMovable, true);
  }

  if (targetexport_enabled) {
    autosaveTargetExport(symbol_layer->getLatLonLabels(), symbol_layer->name());
  }
}

dslmap::MarkerItem*
NavGMapView::showSingleTargetDialog()
{
  QScopedPointer<dslmap::SingleTarget> widget(
    new dslmap::SingleTarget({ { "Current pos", m_currentpos } }, this));
  dslmap::MapScene* my_scene = qobject_cast<dslmap::MapScene*>(scene());
  Q_ASSERT(my_scene != nullptr);
  widget->setScene(my_scene);
  if (m_targetcursor != nullptr) {
    widget->setPositions({ { "Target cursor", m_targetcursor->pos() } });
  }

  if (widget->exec() == QDialog::Rejected) {
    return nullptr;
  }
  if (!widget) {
    qDebug(navgmapview) << "NULLPTR";
    return nullptr;
  }

  auto pos = widget->extractPos();
  auto layer = widget->extractSymbolLayer();
  auto label = widget->extractLabeltext();
  dslmap::MarkerItem* item;
  if (layer == nullptr) {
    // CREATE NEW SYMBOL LAYER
    auto name = widget->extractLayername();
    layer = new dslmap::SymbolLayer();
    layer->setName(name);
    // Create a red rectangle symbol to draw the points
    QPainterPath path;
    layer->setShape(dslmap::ShapeMarkerSymbol::Shape::X, 15);
    auto symbol_ptr = layer->symbolPtr();
    symbol_ptr->setOutlineColor(Qt::red);
    symbol_ptr->setFillColor(Qt::red);
    symbol_ptr->setLabelColor(Qt::red);

    // Draw the markers scale invariant.
    layer->setScaleInvariant(true);
    if (my_scene->projection()) {
      layer->setProjection(my_scene->projection());
    }
    item = layer->addPoint(pos, label, {});
    item->setFlag(QGraphicsItem::ItemIsMovable, false);
    layer->setLabelVisible(true);
    layer->setVisible(true);

    my_scene->addLayer(layer);
  } else {
    // EXISTING SYMBOL LAYER SELECTED
    if (my_scene->projection()) {
      layer->setProjection(my_scene->projection());
    }
    auto item = layer->addPoint(pos, label, {});
    item->setFlag(QGraphicsItem::ItemIsMovable, true);
  }

  if (targetexport_enabled) {
    autosaveTargetExport(layer->getLatLonLabels(), layer->name());
  }
  //Adding a return reference to the symbol layer. Need for hacking in vehicle attributes -TJ 2022-09-03.
  return item;
}

void
NavGMapView::autosaveTargetExport(QVector<QVector<QString>> tableData, QString layername)
{
  auto filename = layername;
  
  auto path = targetfile_path;
  if (path.isEmpty()) {
    path = default_path.absolutePath();
    if (!QDir(path).exists()) {
      QDir().mkdir(path);
    }
  }

  filepath = path + '/' + filename + QString::fromStdString(".csv");
  QFile file(filepath);
  QString delimStr(',');
  if (file.open(QFile::WriteOnly | QFile::Truncate)) {
    QTextStream stream(&file);
    for (auto row : tableData) {
      stream << row[0] << delimStr << row[1] << delimStr << row[2] << delimStr
             << row[3] << "\n";
    }
    file.close();
    qDebug(navgmapview) << "Exporting targets to..." << filepath;
    //layer->setFileName(filepath);

  } else {
    qWarning() << "Failed to open/write to..." << filepath;
  }
}

void
NavGMapView::setSceneTarget(QString filename)
{
  qDebug(navgmapview) << "Calling set target ini with filename of :" << filename;
  auto scene = qobject_cast<NavGMapScene*>(mapScene());
  if (!scene) {
    return;
  }
  if (!filename.isEmpty()){
    scene->setTargetPath(filename);
  }
  else {
    qDebug(navgmapview)<<"FILENAME IS EMPTY FOR THAT LAYER";
  }
}
void NavGMapView::hookUnderlayLayers(dslmap::MapLayer *layer){
  auto scene = qobject_cast<NavGMapScene*>(mapScene());
  if (!scene) {
    return;
  }
  
  auto symbol =
    qobject_cast<dslmap::SymbolLayer*>(layer);
  if (!symbol) {
    return;
  }
  
  //check cast is underlay layer we want.
  connect(symbol,
          &dslmap::SymbolLayer::saveTargetUnderlay,
          this,
          &NavGMapView::setSceneTarget);

}

void
NavGMapView::showTargetImportDialog()
{
 
  auto scene = qobject_cast<dslmap::MapScene*>(mapScene());
  if (!scene) {
    QMessageBox::critical(
      this,
      tr("No map scene available."),
      tr("Cannot import targets when no map scene is available."));
    return;
  }

  auto widget = std::make_unique<dslmap::TargetImport>(*scene, this);
  qDebug(navgmapview) << "Created targetimport dialog";

  // Execute the the dialog.  If successful the widget will add the layer to the
  // scene
  if (widget->exec() == QDialog::Accepted) {
    
    // the layer was not loaded as the "compatible" target underlay type
    if (!widget->isInUnderlayFormatting()) {
      auto layer = widget->targetLayer();
      if (!layer)
        return;
      qDebug(navgmapview) << "Imported target layer is not proper underlay formatting";
      layer->setIsUnderlayFormat(false);
    }
    else{
      qDebug(navgmapview) << "Imported target layer has proper formatting for underlay saving";
    }
    zoomExtents();
  }
  else {
    qDebug(navgmapview) << "Target import dialog not accepted";
  }
}

void
NavGMapView::contextMenuLaunch(QMouseEvent* event)
{

  // Do nothing if there's no scene attached.
  if (scene() == nullptr) {
    return;
  }

  m_currentpos = mapToScene(event->pos());

  QMenu contextMenu;

  // Zoom related
  auto zoomExtentsAction = contextMenu.addAction("&Zoom All");
  connect(zoomExtentsAction, &QAction::triggered, this, &MapView::zoomExtents);

  // Get layer objects beneath cursor

  auto layers = QList<dslmap::MapLayer*>{};

  auto all_layers = mapScene()->layers();
  for (auto layer : all_layers.values()) {
    if (layer->boundingRect().contains(m_currentpos)) {
      layers.append(layer);
    }
  }

  //  for (auto item : items(event->pos())) {
  //    if (item == m_targetcursor) {
  //      continue;
  //    }
  //    auto parent = item->parentItem();
  //    while (parent != nullptr) {
  //      item = parent;
  //      parent = item->parentItem();
  //    }
  //
  //    // Try casting to a maplayer
  //    auto layer_ptr = qgraphicsitem_cast<dslmap::MapLayer*>(item);
  //    if (!layer_ptr) {
  //      continue;
  //    }
  //
  //    if (!layers.contains(layer_ptr)) {
  //      qDebug(navgmapview) << "Layer name: " << layer_ptr->name();
  //      layers.append(layer_ptr);
  //    }
  //  }

  // Add an simple context menu for loading the Target Import dialog.
  contextMenu.addSeparator();
  auto target_import = contextMenu.addAction("&Import Targets");

  connect(target_import, &QAction::triggered, this,
          &NavGMapView::showTargetImportDialog);

  auto drop_cursor = contextMenu.addAction("&Drop Cursor");
  connect(drop_cursor, &QAction::triggered, this, &NavGMapView::dropTarget);

  auto single_target = contextMenu.addAction("&Single Target");
  connect(single_target, &QAction::triggered, this,
          &NavGMapView::showSingleTargetDialog);

  auto layer_action = static_cast<QAction*>(nullptr);
  if (!layers.isEmpty()) {
    //    foreach (auto layer, layers) {
    for (auto layer : layers) {
      if (!layer)
        continue;
      auto menu = layer->contextMenu();
      if (!menu) {
        continue;
      }

      if (!layer_action) {
        layer_action = contextMenu.addSection("Layers");
      }

      contextMenu.addMenu(menu);
    }
  }

  // Show the context menu
  contextMenu.exec(event->globalPos());
}

bool
NavGMapView::rangeBearingToolEnabled() const noexcept
{
  return m_tool_range_bearing_enabled;
}

void
NavGMapView::setRangeBearingToolEnabled(bool enabled)
{
  m_tool_range_bearing_enabled = enabled;
  if (enabled) {
    setCursor(Qt::CrossCursor);
  } else {
    setCursor(Qt::ArrowCursor);
  }
}

void
NavGMapView::mousePressEvent(QMouseEvent* event)
{

  if (event->flags() & Qt::MouseEventCreatedDoubleClick) {
    qDebug(navgmapview) << "Double click in single click callback";
    return;
  }

  QGraphicsView::mousePressEvent(event);

  auto event_button = event->button();
  if (m_mode == RANGE_MODE){
    if (event_button == Qt::LeftButton){
      event_button = Qt::RightButton;
    }
  }

  else if (m_mode == CURSOR_MODE){
    if (event_button == Qt::LeftButton){
      m_currentpos = mapToScene(event->pos());
      dropTarget();
      return;
    }
  }

  else if (m_mode == PLUGIN_CONTROL_MODE && event_button == Qt::LeftButton){
    // Do nothing for now
    // plugin going to handle this behavior
    plugin_pressed = true;
    emit mouseStartPos(event->pos());
    emit mouseMovePos(event->pos());
    setDragMode(DragMode::NoDrag);
    return;
  }

  if (m_tool_range_bearing_enabled) {
   
    m_tool_range_bearing->setP1(event->pos());
    m_tool_range_bearing->setP2(event->pos());
    if (m_mode == RANGE_MODE){
      m_tool_range_bearing->setTextOffset(60,60);
    }
    else {
      m_tool_range_bearing->setTextOffset(0,0);
    }
    return;
  }

   else if (event_button == Qt::LeftButton &&
             dragMode() == DragMode::NoDrag) {
    setDragMode(DragMode::ScrollHandDrag);
    mousePressEvent(event);
  } else if (event_button == Qt::MiddleButton &&
             !m_tool_range_bearing_visible) {
    m_tool_range_bearing_visible = true;
    setRangeBearingToolEnabled(true);
    mousePressEvent(event);
  } else if (event_button == Qt::RightButton) {
  
    m_launch_context_menu = true;
    m_tool_range_bearing_visible = true;
    setRangeBearingToolEnabled(true);
    mousePressEvent(event);
  }
}

void
NavGMapView::mouseDoubleClickEvent(QMouseEvent* event)
{

  QGraphicsView::mouseDoubleClickEvent(event);
  if (event->button() == Qt::LeftButton) {
    const auto scene_ = qobject_cast<NavGMapScene*>(mapScene());
    if (!scene_) {
      qCWarning(navgmapview) << "No MapScene. Skipping dropping cursor";
      return;
    }

    m_currentpos = mapToScene(event->pos());
    emit mouseDoubleClickedOnScene(m_currentpos);
    if (!m_currentpos.isNull()) {
      dropTarget();
    }
  }
}

void
NavGMapView::mouseMoveEvent(QMouseEvent* event)
{
  
  if (m_tool_range_bearing_enabled &&
      event->buttons() != Qt::MouseButton::NoButton) {
    m_tool_range_bearing->setP2(event->pos());
    if (m_launch_context_menu &&
        (m_tool_range_bearing->p2() - m_tool_range_bearing->p1())
            .manhattanLength() > 50) {
      m_launch_context_menu = false;
    }
    invalidateScene(sceneRect(), QGraphicsScene::ForegroundLayer);
    viewport()->update();
  }
  if (!m_tool_range_bearing_enabled){
    dslmap::MapView::mouseMoveEvent(event);
  }

  if (m_mode == PLUGIN_CONTROL_MODE){
    if (plugin_pressed){
     // dragMode() == DragMode::NoDrag
     //setDragMode(DragMode::ScrollHandDrag
     
    emit mouseMovePos(event->pos());
    invalidateScene(sceneRect(), QGraphicsScene::ForegroundLayer);
    viewport()->update();
    }
  }
}

void
NavGMapView::mouseReleaseEvent(QMouseEvent* event)
{
  plugin_pressed = false;
  auto event_button = event->button();
  if (m_mode == RANGE_MODE){
    if (event_button == Qt::LeftButton){
      event_button = Qt::RightButton;
    }
  }

  if (m_mode == PLUGIN_CONTROL_MODE){
    //setDragMode(DragMode::ScrollHandDrag);
    emit mouseMovePos(event->pos());
    invalidateScene(sceneRect(), QGraphicsScene::ForegroundLayer);
    viewport()->update();
  }

  QGraphicsView::mouseReleaseEvent(event);
  if (dragMode() == DragMode::ScrollHandDrag) {
    setDragMode(DragMode::NoDrag);
  }
  if (m_launch_context_menu && event_button == Qt::RightButton) {
    contextMenuLaunch(event);
  }

  if (m_tool_range_bearing_enabled) {
    m_tool_range_bearing->setP1(event->pos());
    invalidateScene(sceneRect(), QGraphicsScene::ForegroundLayer);
    viewport()->update();
  }
  if (m_tool_range_bearing_visible) {
    m_tool_range_bearing_visible = false;
    setRangeBearingToolEnabled(false);
  }
}

void
NavGMapView::drawForeground(QPainter* painter, const QRectF& rect)
{
  MapView::drawForeground(painter, rect);
  if (m_tool_range_bearing_enabled &&
      (m_tool_range_bearing->p1() != m_tool_range_bearing->p2())) {
    m_tool_range_bearing->draw(this, painter, rect);
  }
  // emit mouse held event to plugin control
  if (m_mode == PLUGIN_CONTROL_MODE && plugin_pressed){
    emit plugin_held_event();
  }
}

NavGMapView::GraticuleType
NavGMapView::graticuleProjectionType() const noexcept
{
  return m_graticule_projection_type;
}

bool
NavGMapView::setGraticuleProjectionType(
  NavGMapView::GraticuleType type) noexcept
{
  if (graticuleProjectionPtr() && (type == graticuleProjectionType())) {
    return true;
  }

  // Geodetic coordinates don't need existing data
  if (type == GraticuleType::LatLon) {
    setGraticuleProjection(dslmap::Proj::fromDefinition(PROJ_WGS84_DEFINITION));
  } else if (type == GraticuleType::AlvinXY) {
    const auto scene_ = qobject_cast<NavGMapScene*>(mapScene());
    if (!scene_) {
      qCWarning(navgmapview)
        << "No valid NavGMapScene object associated with this view.";
      return false;
    }

    if (!scene_->hasAlvinXYOrigin()) {
      qCWarning(navgmapview) << "No AlvinXY origin specified for scene.";
      return false;
    }

    const auto origin = scene_->alvinXYOrigin();
    const auto proj = dslmap::Proj::alvinXYFromOrigin(origin.x(), origin.y());
    setGraticuleProjection(proj);
  } else if (type == GraticuleType::UTM) {
    const auto scene_ = mapScene();
    if (!scene_) {
      qCWarning(navgmapview)
        << "No valid MapScene object associated with this view";
      return false;
    }

    const auto rect = scene_->itemsBoundingRect();
    if (rect.isEmpty()) {
      qCWarning(navgmapview)
        << "MapScene contains no items, cannot calculate UTM zone.";
      return false;
    }

    const auto utm_proj = [&]() {
      auto x = rect.center().x();
      auto y = rect.center().y();

      const auto scene_proj = scene_->projectionPtr();
      if (!scene_proj->isLatLon()) {
        scene_proj->transformToLonLat(1, &x, &y, true);
      }

      return dslmap::Proj::utmFromLonLat(x, y);
    }();

    setGraticuleProjection(utm_proj);
  }

  m_graticule_projection_type = type;
  emit graticuleTypeChanged(type);
  return true;
}

void NavGMapView::zoomFirstLayer(dslmap::MapLayer *layer){
  dslmap::MapScene *scene = mapScene();
  if (!scene) {
    return;
  }
  QHash<QUuid, dslmap::MapLayer*> layers = scene->layers();
  if (layers.count() == 1){
    //only layer here, so let's zoom to it
    qDebug(navgmapview) << "Zooming to layer" << layer->name();
    zoomToRect(layer->boundingRect());
  }
}

void NavGMapView::setMouseMode(MOUSE_MODE mode){
  m_mode = mode;
}

MOUSE_MODE NavGMapView::getMouseMode(){
  return m_mode;
}

void
NavGMapView::setCursorColor(QColor color)
{
  m_cursor_color = color;
  if (!m_targetcursor) {
    return;
  }

  const auto symbol = m_targetcursor->symbol();
  symbol->setOutlineColor(color);
}

QColor
NavGMapView::cursorColor()
{
  return m_cursor_color;
}

void NavGMapView::setRangeBearingColor(QColor color)
{
	m_rangebearing_color = color;
	m_tool_range_bearing->setColor(m_rangebearing_color);
}

QColor NavGMapView::rangeBearingColor()
{
	return m_rangebearing_color;
}

const dslmap::MarkerItem* 
NavGMapView::getTargetCursor(){
  return m_targetcursor;
}

} // namespace navg
