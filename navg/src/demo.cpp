/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "Config.h"

#include "MainWindow.h"

#include <BathyLayer.h>
#include <DslMap.h>
#include <MapScene.h>
#include <MapView.h>
#include <SymbolLayer.h>

#include <QApplication>
#include <QtPlugin>

int
main(int argc, char* argv[])
{

  dslmap::init();

  qSetMessagePattern("%{type} %{category} %{file}:%{line} %{message}");
  QCoreApplication::setApplicationName("NavG");
  QCoreApplication::setApplicationVersion(NAVG_VERSION);
  QCoreApplication::setOrganizationName("WHOI");
  QCoreApplication::setOrganizationDomain(".edu");
  QCoreApplication::setAttribute(Qt::AA_DontUseNativeMenuBar);

  QApplication app(argc, argv);

  auto main_window = new navg::MainWindow;

  main_window->setWindowTitle(QString("NavG v%1 DEMO").arg(NAVG_VERSION));

  auto map = qobject_cast<dslmap::MapView*>(main_window->centralWidget());
  map->setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);
  auto scene = new dslmap::MapScene(map);
  scene->setBackgroundBrush(QBrush{ Qt::black });

  map->setScene(scene);

  // Create a bathymetry layer
  auto bathy_layer = new dslmap::BathyLayer();
#if 1
  auto ok =
    bathy_layer->setRasterFile("/home/zac/scratch/SAtl25m_small_wgs84.grd");
#else
  auto ok = bathy_layer->setRasterFile("/home/zac/scratch/survey2.grd");
#endif
  Q_ASSERT(ok);

  scene->addLayer(bathy_layer);

  QPainterPath watch_circle_path;
  watch_circle_path.addEllipse(-100, -100, 200, 200);

  auto pen = QPen{};
  pen.setCosmetic(true);
  pen.setWidth(2);
  pen.setStyle(Qt::PenStyle::DashLine);
  pen.setColor(Qt::darkBlue);

  auto watch_circle_layer = new dslmap::SymbolLayer();
  watch_circle_layer->setProjection(scene->projection());

  watch_circle_layer->setPath(watch_circle_path);
  watch_circle_layer->setPen(pen);
  watch_circle_layer->setScaleInvariant(false);
  watch_circle_layer->setMinimumDrawSize(20);

  auto watch_circle = watch_circle_layer->addPoint(
    bathy_layer->sceneBoundingRect().center(), "200m watch circle");

  watch_circle->setFlag(QGraphicsItem::ItemIsMovable, true);

  watch_circle_layer->setLabelVisible(true);
  scene->addLayer(watch_circle_layer);

  main_window->show();

  app.exec();

  foreach (auto layer, scene->removeAllLayers()) {
    delete layer;
  }
}
