EXAMPLE NAVG PLUGIN

Rules:
- must subclass CorePluginInterface
- must connect to other plugins using signals and slots only
- can instantiate dslmap code, but shouldn't depend on NavG

Benefits:
