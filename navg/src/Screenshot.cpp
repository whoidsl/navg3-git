#include "Screenshot.h"

namespace navg {

Q_LOGGING_CATEGORY(navg_screenshot, "navg_screenshot");

Screenshot::Screenshot() {}

void
Screenshot::saveScreenshot()
{
  const QString format = "png";
  QString initialPath =
    QStandardPaths::writableLocation(QStandardPaths::PicturesLocation);
  if (initialPath.isEmpty())
    initialPath = QDir::currentPath();
  initialPath += tr("/untitled.") + format;

  QFileDialog fileDialog(this, tr("Save As"), initialPath);
  fileDialog.setAcceptMode(QFileDialog::AcceptSave);
  fileDialog.setFileMode(QFileDialog::AnyFile);
  fileDialog.setDirectory(initialPath);
  QStringList mimeTypes;
  const QList<QByteArray> baMimeTypes = QImageWriter::supportedMimeTypes();
  for (const QByteArray& bf : baMimeTypes)
    mimeTypes.append(QLatin1String(bf));
  fileDialog.setMimeTypeFilters(mimeTypes);
  fileDialog.selectMimeTypeFilter("image/" + format);
  fileDialog.setDefaultSuffix(format);
  if (fileDialog.exec() != QDialog::Accepted)
    return;
  const QString fileName = fileDialog.selectedFiles().first();
  if (!originalPixmap.save(fileName)) {
    QMessageBox::warning(this,
                         tr("Save Error"),
                         tr("The image could not be saved to \"%1\".")
                           .arg(QDir::toNativeSeparators(fileName)));
  }
}

void
Screenshot::saveScreenshot(QString name, QString directory)
{

  QString path = directory;
  const QString format = "png";

  if (path.isEmpty()) {
    path = default_path.absolutePath();
    if (!QDir(path).exists()) {
      QDir().mkdir(path);
    }
  }
  path += "/" + name + "." + format;
  qDebug(navg_screenshot) << "Saving screenshot to " << path;
  if (!originalPixmap.save(path)) {
    QMessageBox::warning(this,
                         tr("Save Error"),
                         tr("The image could not be saved to \"%1\".")
                           .arg(QDir::toNativeSeparators(path)));
  }
}

void
Screenshot::shootScreen()
{
  QScreen* screen = QGuiApplication::primaryScreen();
  if (const QWindow* window = windowHandle())
    screen = window->screen();
  if (!screen)
    return;

  originalPixmap = screen->grabWindow(0);
}

QPixmap 
Screenshot::getScreenshotPixmap(){
  return originalPixmap;
}

}