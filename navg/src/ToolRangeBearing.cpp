/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ToolRangeBearing.h"
#include <ToolRangeBearing.h>
#include <cmath>
#include <dslmap/MapScene.h>
#include <dslmap/MapView.h>
#include <dslmap/Proj.h>

namespace navg {

ToolRangeBearing::ToolRangeBearing(QObject* parent)
  : QObject(parent)
{
  // Setup pen
  m_pen.setCosmetic(true);
  m_pen.setWidth(2);
}

ToolRangeBearing::~ToolRangeBearing() = default;

void
ToolRangeBearing::setColor(QColor color)
{
  if (!color.isValid()) {
    return;
  }

  if (color == m_pen.color()) {
    return;
  }

  m_pen.setColor(color);
  emit colorChanged(color);
}

const QColor
ToolRangeBearing::color() const
{
  return m_pen.color();
}

bool
ToolRangeBearing::xyEnabled() const
{
  return m_xy_enabled;
}
bool
ToolRangeBearing::watchCircleEnabled() const
{
  return m_watch_circle_enabled;
}
void
ToolRangeBearing::setXYEnabled(bool enabled)
{
  if (enabled == m_xy_enabled) {
    return;
  }

  m_xy_enabled = enabled;
  emit drawXYChanged(enabled);
}

void
ToolRangeBearing::setWatchCircleEnabled(bool enabled)
{
  if (enabled == m_watch_circle_enabled) {
    return;
  }

  m_watch_circle_enabled = enabled;
  emit drawWatchCircleChanged(enabled);
}

void
ToolRangeBearing::draw(const dslmap::MapView* view, QPainter* painter,
                       const QRectF& rect) const
{

  const auto range_line =
    QLineF{ view->mapToScene(m_p1), view->mapToScene(m_p2) };
  if (range_line.length() == 0) {
    // qCDebug(navgmapview, "Will not draw range/bearing indicator; range is
    // 0");
    return;
  }

  const auto x_line = QLineF{ range_line.x1(), range_line.y1(), range_line.x2(),
                              range_line.y1() };
  const auto y_line = QLineF{ range_line.x2(), range_line.y1(), range_line.x2(),
                              range_line.y2() };

  const auto is_easterly = x_line.x2() >= x_line.x1();
  const auto is_northerly = y_line.y2() >= y_line.y1();

  auto map_scene = view->mapScene();
  if (!map_scene) {
    // qCDebug(navgmapview, "Cannot draw range/bearing indicator; invalid
    // scene");
    return;
  }

   if (!map_scene->projectionPtr()) {
    return;
  }

  // Ranges in meters
  const auto range_m = map_scene->projectionPtr()->geodeticDistance(
    range_line.p1(), range_line.p2());
  Q_UNUSED(range_m);
  const auto x_line_length_m =
    map_scene->projectionPtr()->geodeticDistance(x_line.p1(), x_line.p2());
  const auto y_line_length_m =
    map_scene->projectionPtr()->geodeticDistance(y_line.p1(), y_line.p2());

  painter->save();
  auto pen = m_pen;
  painter->setPen(pen);
  painter->drawLine(range_line);

  // Draw aux lines
  pen.setStyle(Qt::PenStyle::DashLine);
  painter->setPen(pen);
  if (m_watch_circle_enabled) {
    painter->drawEllipse(range_line.p1(), range_line.length(),
                         range_line.length());
  }

  if (m_xy_enabled) {

    if (x_line.length() != 0) {
      painter->drawLine(x_line);
    }
    if (y_line.length() != 0) {
      painter->drawLine(y_line);
    }
  }

  pen.setStyle(Qt::PenStyle::SolidLine);
  painter->setPen(pen);

  const auto label_fill = QBrush{ QColor{ 0, 0, 0, 150 }, Qt::SolidPattern };

  int factor_offset = 1;

  // Draw labels
  const auto orig_tf = painter->combinedTransform();
  painter->setWorldMatrixEnabled(false);
  if (m_xy_enabled) {
    auto length = x_line.length();
    if (length > 0) {
      const auto factor = x_line.x1() >= x_line.x2() ? -factor_offset : factor_offset;
      const auto factor_y = y_line.y1() >= y_line.y2() ? factor_offset : -factor_offset;
      const auto label = QString("x: %1 m").arg(x_line_length_m, 0, 'f', 1);
      const auto midpoint = orig_tf.map(
        QPointF{ x_line.x1() + factor * length / 2.0, x_line.y1() + (m_shift_y * factor_y) });
      auto rect_ = QFontMetricsF(painter->font())
                     .boundingRect(rect, Qt::TextExpandTabs, label);
      rect_.moveCenter(midpoint);
      if (is_northerly) {
        rect_.moveTop(midpoint.y() + 15);
      } else {
        rect_.moveBottom(midpoint.y() - 15);
      }
      painter->fillRect(rect_, label_fill);
      painter->drawText(rect_, label);
    }

    length = y_line.length();
    if (length > 0) {
      const auto factor = y_line.y1() >= y_line.y2() ? factor_offset : -factor_offset;
      const auto factor_x = x_line.x1() >= x_line.x2() ? -factor_offset : factor_offset;
      const auto label = QString("y: %1 m").arg(y_line_length_m, 0, 'f', 1);
      const auto midpoint = orig_tf.map(
        QPointF{ y_line.x1() + (m_shift_x * factor_x), y_line.y2() + factor * length / 2.0 });

      auto rect_ = QFontMetricsF(painter->font())
                     .boundingRect(rect, Qt::TextExpandTabs, label);
      rect_.moveCenter(midpoint);
      if (is_easterly) {
        rect_.moveLeft(midpoint.x() + 15);
      } else {
        rect_.moveRight(midpoint.x() - 15);
      }
      painter->fillRect(rect_, label_fill);
      painter->drawText(rect_, label);
    }
  }

  if (range_line.length() > 0) {
    const auto factor_x = x_line.x1() >= x_line.x2() ? -factor_offset : factor_offset;
    const auto factor_y = y_line.y1() >= y_line.y2() ? factor_offset : -factor_offset;
    const auto heading = std::fmod(range_line.angle() + 90, 360);
    const auto label = QString("r: %1 m\nb: %2\u00B0")
                         .arg(range_line.length(), 0, 'f', 1)
                         .arg(heading, 0, 'f', 1);
    auto rect_ = QFontMetricsF(painter->font())
                   .boundingRect(rect, Qt::TextExpandTabs, label);

    const auto label_point =
      orig_tf.map(QPointF{ range_line.x2() + (m_shift_x * factor_x),
                           range_line.y2() + (m_shift_y * -factor_y) });

    rect_.moveTo(label_point);

    if (is_easterly) {
      rect_.moveLeft(label_point.x() + 15);
    } else {
      rect_.moveRight(label_point.x() - 15);
    }

    if (is_northerly) {
      rect_.moveBottom(label_point.y() - 15);
    } else {
      rect_.moveTop(label_point.y() + 15);
    }

    // rect_.moveTo(label_point);
    painter->fillRect(rect_, label_fill);
    painter->drawText(rect_, Qt::AlignLeft, label);
  }

  painter->setWorldMatrixEnabled(true);
  painter->restore();
}

void ToolRangeBearing::setTextOffset(int x, int y){
  m_shift_x = x;
  m_shift_y = y;
}

QPoint
ToolRangeBearing::p1() const
{
  return m_p1;
}
QPoint
ToolRangeBearing::p2() const
{
  return m_p2;
}
void
ToolRangeBearing::setP1(const QPoint& p1)
{
  m_p1 = p1;
}
void
ToolRangeBearing::setP2(const QPoint& p2)
{
  m_p2 = p2;
}
}
