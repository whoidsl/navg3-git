/**
 * Copyright 2019 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "MainWindow.h"
#include "AboutPluginsDialog.h"
#include "AlvinXYOriginDialog.h"
#include "CoordinateStatusBarWidget.h"
#include "CorePluginInterface.h"
#include "DepthStatusBarWidget.h"
#include "MaxZoomOutDialog.h"
#include "GridSpacingStatusBarWidget.h"
#include "IniStatusBarWidget.h"
#include "MapLayerEditWidget.h"
#include "NavG.h"
#include "NavGDockWidget.h"
#include "NavGMapScene.h"
#include "NavGMapView.h"
#include "Screenshot.h"
#include <dslmap/RasterLayer.h>

#include <MainWindow.h>
#include <QApplication>
#include <QFileDialog>
#include <QJsonValue>
#include <QMenuBar>
#include <QMessageBox>
#include <QOpenGLWidget>
#include <QPluginLoader>
#include <QSettings>
#include <QShortcut>
#include <QStatusBar>
#include <QToolBar>
#include <dslmap/BathyImportDialog.h>
#include <dslmap/BathyObject.h>
#include <dslmap/ContourImportDialog.h>
#include <dslmap/ContourLayer.h>
#include <dslmap/GdalColorGradient.h>
#include <dslmap/GeoreferenceImageLayer.h>
#include <dslmap/GeoreferenceImageObject.h>
#include <dslmap/GeoreferenceImport.h>
#include <dslmap/ImageImportDialog.h>
#include <dslmap/MapScene.h>
#include <dslmap/ShapeMarkerSymbol.h>
#include <dslmap/TargetImport.h>

namespace navg {
Q_LOGGING_CATEGORY(navg, "navg")

struct MainWindow::Plugin
{
  QPluginLoader* loader;
  QDockWidget* dock;
};

void
MainWindow::closeEvent(QCloseEvent* event)
{
  event->ignore();
  if (QMessageBox::Ok ==
      QMessageBox::question(
        this, "Quit Application?", "All UNSAVED targets, layers, trail markers \nand settings will be lost", QMessageBox::Cancel | QMessageBox::Ok)) {
    event->accept();
  }
}

MainWindow::MainWindow(bool UseOpenGL, QWidget* parent, QString arg_settings_file)
  : QMainWindow(parent)
{

  setWindowTitle(
    QString("NavG v%1").arg(QCoreApplication::applicationVersion()));

  // Set up the mapview and scene as the sentral widget
  m_map_view = new NavGMapView();
  if (UseOpenGL) {
    m_map_view->setViewport(new QOpenGLWidget);
  }
  m_map_view->setLatLonFormat(dslmap::LatLonFormat::DD_NE);

  auto map_scene = new NavGMapScene;
  map_scene->setBackgroundBrush(QBrush{ Qt::black });

  m_map_view->setScene(map_scene);

  connect(map_scene,
          static_cast<void (dslmap::MapScene::*)(dslmap::MapLayer * layer)>(
            &dslmap::MapScene::layerAdded),
          m_map_view,
          &NavGMapView::zoomFirstLayer,
          Qt::QueuedConnection);

  connect(map_scene,
          static_cast<void (dslmap::MapScene::*)(dslmap::MapLayer * layer)>(
            &dslmap::MapScene::layerAdded),
          m_map_view,
          &NavGMapView::hookUnderlayLayers,
          Qt::QueuedConnection);

  //for underlay saving triggered when a layer gets added

  //
  // Setup the status bar
  //
  auto status_bar = new QStatusBar;
  connect(
    this, &MainWindow::mapOnlyModeToggled, status_bar, &QStatusBar::setHidden);
  auto scene_coordinate_widget = new CoordinateStatusBarWidget;
  scene_coordinate_widget->setSceneProjection(map_scene->projectionType());
  scene_coordinate_widget->setContextMenuPolicy(Qt::CustomContextMenu);
  connect(scene_coordinate_widget,
          &QWidget::customContextMenuRequested,
          [=](const QPoint& pos) {
            QMenu menu;
            menu.addActions(this->m_menu_map_projection->actions());
            menu.addSeparator();
            menu.addActions(scene_coordinate_widget->actions());
            menu.exec(scene_coordinate_widget->mapToGlobal(pos));
          });
  connect(map_scene,
          &NavGMapScene::projectionTypeChanged,
          scene_coordinate_widget,
          &CoordinateStatusBarWidget::setSceneProjection);
  connect(m_map_view,
          &NavGMapView::scenePositionUnderMouseChanged,
          scene_coordinate_widget,
          &CoordinateStatusBarWidget::setCoordinateValue);
  connect(m_map_view,
          &NavGMapView::latLonFormatChanged,
          scene_coordinate_widget,
          &CoordinateStatusBarWidget::setLatLonFormat);

  auto view_coordinate_widget = new CoordinateStatusBarWidget;
  view_coordinate_widget->setGraticuleProjection(
    m_map_view->graticuleProjectionType());
  view_coordinate_widget->setContextMenuPolicy(Qt::CustomContextMenu);
#if 0
  connect(view_coordinate_widget, &QWidget::customContextMenuRequested,
          [=](const QPoint& pos) {
            this->m_menu_map_graticule->exec(
              view_coordinate_widget->mapToGlobal(pos));
          });
#else
  connect(view_coordinate_widget,
          &QWidget::customContextMenuRequested,
          [=](const QPoint& pos) {
            QMenu menu;
            menu.addActions(this->m_menu_map_graticule->actions());
            menu.addSeparator();
            menu.addActions(view_coordinate_widget->actions());
            menu.exec(view_coordinate_widget->mapToGlobal(pos));
          });
#endif
  connect(m_map_view,
          &NavGMapView::graticuleTypeChanged,
          view_coordinate_widget,
          &CoordinateStatusBarWidget::setGraticuleProjection);
  connect(m_map_view,
          &NavGMapView::graticulePositionUnderMouseChanged,
          view_coordinate_widget,
          &CoordinateStatusBarWidget::setCoordinateValue);
  connect(m_map_view,
          &NavGMapView::latLonFormatChanged,
          view_coordinate_widget,
          &CoordinateStatusBarWidget::setLatLonFormat);

  auto depth_widget = new DepthStatusBarWidget;
  connect(m_map_view,
          &NavGMapView::depthUnderMouseChanged,
          depth_widget,
          &DepthStatusBarWidget::setDepthValue);

  auto grid_spacing_widget = new GridSpacingStatusBarWidget;
  connect(m_map_view, &NavGMapView::gridSpacingChanged, grid_spacing_widget,
          &GridSpacingStatusBarWidget::setSpacingValues);

  auto config_ini_status_widget = new IniStatusBarWidget;
  connect(this, &MainWindow::settingsFileChanged, config_ini_status_widget,
          &IniStatusBarWidget::setConfigFile);
  
  connect(this, &MainWindow::settingsFileChanged, this,
          &MainWindow::saveSettingsFile);
  
  connect(this, &MainWindow::targetfileExportEnabled, m_map_view,
          &NavGMapView::setTargetFileExportEnabled);
  connect(this, &MainWindow::targetfileDirectoryChanged, m_map_view,
          &NavGMapView::setTargetFileDirectoryChanged);
  
  status_bar->addWidget(config_ini_status_widget);
  status_bar->addPermanentWidget(grid_spacing_widget);
  status_bar->addPermanentWidget(depth_widget);
  status_bar->addPermanentWidget(scene_coordinate_widget);
  status_bar->addPermanentWidget(view_coordinate_widget);
  setStatusBar(status_bar);

  // Some extra plumbing to handle changes to the scene's AlvinXY origin at
  // runtime for the mapview graticule.
  connect(map_scene, &NavGMapScene::alvinXYOriginChanged, [=](QPointF origin) {
    // Nothing needed if we're not showing an AlvinXY graticule
    if (m_map_view->graticuleProjectionType() !=
        NavGMapView::GraticuleType::AlvinXY) {
      return;
    }

    // XY origin cleared? Reset to Lat/Lon graticule
    if (!NavGMapScene::isValidAlvinXYOrigin(origin)) {
      m_action_map_graticule_latlon->trigger();
      return;
    }

    // Otherwise, update the graticule projection
    const auto proj = dslmap::Proj::alvinXYFromOrigin(origin.x(), origin.y());
    m_map_view->setGraticuleProjection(proj);
  });

  setCentralWidget(m_map_view);

  // Set dockwidget options.
  setDockOptions(AllowNestedDocks | AllowTabbedDocks);

  //
  // MapLayer Widget and Model
  //
  dslmap::MapScene* scene = m_map_view->mapScene();
  m_layer_model = new MapLayerListModel(scene);
  m_layer_widget = new NavGDockWidget;
  m_layer_widget->setWindowTitle("Layers");
  m_layer_edit_widget = new MapLayerEditWidget(m_layer_model);
  m_layer_widget->setWidget(m_layer_edit_widget);
  m_layer_widget->setObjectName("MapLayersWidget");
  restoreDockWidget(m_layer_widget);

  if (dockWidgetArea(m_layer_widget) == Qt::NoDockWidgetArea &&
      !m_layer_widget->isWindow()) {
    addDockWidget(Qt::LeftDockWidgetArea, m_layer_widget);
  }
  connect(this,
          &MainWindow::mapOnlyModeToggled,
          m_layer_widget,
          &NavGDockWidget::handleMapOnlyModeToggled);

  // View toolbar connection for icon size
  connect(this,
          &MainWindow::iconSizeChanged,
          m_layer_edit_widget,
          &MapLayerEditWidget::setIconSize);

  //
  // Figure out the correct plugin directory
  //
  if (QCoreApplication::applicationDirPath() == NAVG_INSTALL_BINDIR) {
    // Use canonicalPath to avoid adding the same directory twice, even if
    // its added through multiple symlinks that point to the same location.
    // Double-adding plugins crashes navG at startup.
    auto pluginDir = QDir{ NAVG_PLUGIN_DIR };
    m_plugin_paths << pluginDir.canonicalPath();
  } else {
    auto dir = QDir{ QCoreApplication::applicationDirPath() +
                     QDir::separator() + "../plugins" };
    qDebug(navg) << "NavG Plugin directory:" << dir.absolutePath();
    for (const auto& i : dir.entryInfoList(QDir::Dirs | QDir::NoDotAndDotDot)) {
      const auto p = i.canonicalFilePath();
      if (!p.endsWith("meson.build")) {
        m_plugin_paths << p + QDir::separator() + "src";
      }
    }
  }

  //
  // Parse the NAVG_PLUGIN_PATH environment variable and add paths as
  // appropriate
  //
  QByteArray raw_plugin_path = qgetenv("NAVG_PLUGIN_PATH");
  if (!raw_plugin_path.isEmpty()) {
    QList<QByteArray> paths = raw_plugin_path.split(':');
    for (auto path : paths) {
      if (path.isEmpty()) {
        continue;
      }
      auto dir = QDir{ QString::fromUtf8(path) };
      qDebug(navg) << "Adding NavG Plugin Path: " << dir.canonicalPath();
      if (m_plugin_paths.contains(dir.canonicalPath())) {
        qDebug(navg) << "    directory already added, skipping: "
                     << dir.canonicalPath();
      } else {
        m_plugin_paths << dir.canonicalPath();
      }
    }
  }

  QSettings app_settings;
  if (!arg_settings_file.isEmpty()){
    QFileInfo check_file(arg_settings_file);
    if (check_file.exists() && check_file.isFile()) {
      app_settings.setValue("settings_file", arg_settings_file);
      app_settings.sync();
    }
  }
  
  //
  // Load the last-used settings file
  //
  const auto filename = app_settings.value("settings_file").toString();

  loadSettings(filename);

  scanPlugins();
  const auto settingGroups =
    m_settings ? m_settings->childGroups() : QStringList();
  for (const QString& name : m_plugins.keys()) {
    if (settingGroups.contains(name)) {
      loadPlugin(name, m_plugins.value(name));
    }
  }
  createActions();
  createMenus();
  createWidgets();

  //
  // Create toolbars
  //
  m_toolbar_layers = new QToolBar("Layer Tools", this);
  m_toolbar_layers->setObjectName("LayerToolBar");
  m_toolbar_layers->addAction(m_action_add_bathymetry_layer);
  m_toolbar_layers->addAction(m_action_add_contour_layer);
  //  m_toolbar_layers->addAction(m_action_add_vector_layer);
  m_toolbar_layers->addAction(m_action_add_image_layer);
  m_toolbar_layers->addAction(m_action_add_target_layer);
  m_toolbar_layers->addAction(m_action_add_georeference_image_layer);
  m_toolbar_layers->addAction(m_action_take_screenshot);
  m_toolbar_layers->addAction(m_action_cursor_color);
  m_toolbar_layers->addAction(m_action_rangebearing_color);
  m_toolbar_layers->addSeparator();
  m_toolbar_layers->addAction(m_action_toggle_layer_widget);
  connect(this,
          &MainWindow::mapOnlyModeToggled,
          m_toolbar_layers,
          &QToolBar::setHidden);
  addToolBar(m_toolbar_layers);

  m_toolbar_map_interaction = new QToolBar("Map Interaction Tools", this);
  m_toolbar_map_interaction->setObjectName("MapInteractionToolBar");
  m_toolbar_map_interaction->addAction(m_action_toggle_range_bearing_tool);
  connect(this,
          &MainWindow::mapOnlyModeToggled,
          m_toolbar_map_interaction,
          &QToolBar::setHidden);
  addToolBar(m_toolbar_map_interaction);

  // View toolbar connection
  connect(this, &MainWindow::iconSizeChanged, [this](auto size) {
    this->scaleActionIcons(m_toolbar_map_interaction, size);
    this->scaleActionIcons(m_toolbar_layers, size);
  });

  // Mapscene layer load connections

  if (m_map_view) {
    auto scene_ = qobject_cast<NavGMapScene*>(m_map_view->mapScene());
    if (scene_) {
      connect(scene_,
              &NavGMapScene::bathyPreloadReady,
              this,
              &MainWindow::loadBathyLayer);
      connect(scene_,
              &NavGMapScene::contourPreloadReady,
              this,
              &MainWindow::loadContourLayer);
      connect(scene_, &NavGMapScene::geoimagePreloadReady, [this](auto path) {
        this->loadGeoreferenceImageLayer(path);
      });
      connect(scene_,
              &NavGMapScene::imagePreloadReady,
              [this](QString path, QRectF rect) {
                this->loadImageLayer(path, rect);
              });
      connect(scene_,
              &NavGMapScene::targetPreloadReady,
              this,
              &MainWindow::loadTargetLayer);
    }
  }

  //
  // Apply settings
  //

  // Apply settings for loaded plugins.
  if (m_settings) {

    // MapView settings
    if (m_map_view) {
      m_map_view->loadSettings(m_settings.get());
      QColor color = m_map_view->cursorColor();
      QColor rangebearing_color = m_map_view->rangeBearingColor();
      cursor_color_widget->setColor(color);
      rangebearing_color_widget->setColor(rangebearing_color);
    }

    // MapScene settings
    if (m_map_view) {
      auto scene_ = qobject_cast<NavGMapScene*>(m_map_view->mapScene());
      if (scene_) {
        scene_->loadSettings(m_settings.get());
      }
    }
    m_settings->beginGroup("window");

    screenshot_directory = m_settings->value("screenshot_directory").toString();
    targetfile_enabled = m_settings->value("targetfile_enabled").toBool();
    m_action_settings_set_targetfile_enabled->setChecked(targetfile_enabled);

    emit targetfileExportEnabled(m_action_settings_set_targetfile_enabled->isChecked());      
    
    targetfile_directory = m_settings->value("targetfile_directory").toString();

    // icon size settings
    auto icon_size = m_settings->value("icons/size").toString();
    if (icon_size == "Large") {
      if (m_action_view_large_icons) {
        m_action_view_large_icons->setChecked(true);
      }
      emit iconSizeChanged(LARGE_ICON_SIZE);
    }

    // Restore window geometry
    bool lck = m_settings->value("lock_plugins", false).toBool();
    m_action_toggle_plugin_lock->setChecked(lck);
    restoreGeometry(m_settings->value("geometry").toByteArray());
    restoreState(m_settings->value("layout").toByteArray());
    for (auto toolbar : findChildren<QToolBar*>()) {
      const auto name = toolbar->objectName();
      if (name.isEmpty()) {
        continue;
      }
      const auto variant = m_settings->value(name);
      toolbar->restoreGeometry(m_settings->value(name).toByteArray());
    }
    m_settings->endGroup();
  }

  // Set the correct state for the full-screen action after loading the window
  // state
  m_action_window_full_screen->setChecked(
    (windowState() & Qt::WindowState::WindowFullScreen) != 0);
  m_action_window_map_only->setChecked(false);

}

MainWindow::~MainWindow() = default;

void
MainWindow::scanPlugins()
{
  for (const auto& path : m_plugin_paths) {
    qDebug(navg) << "Searching " << path;
    auto dir = QDir{ path };
    for (const auto& file :
         dir.entryInfoList(QStringList{ "libnavg_*.so" }, QDir::Files)) {
      auto plugin = std::make_unique<QPluginLoader>(file.absoluteFilePath());
      auto metadata = plugin->metaData();
      const auto name =
        metadata.value("MetaData").toObject().value("name").toString();
      if (name.isEmpty()) {
        qCWarning(navg) << "skipping class"
                        << plugin->metaData()
                             .value("MetaData")
                             .toObject()
                             .value("className")
                             .toString()
                        << " (no 'name' defined in metadata)";
        continue;
      }

      if (m_plugins.contains(name)) {
        qCCritical(navg) << "Duplicate plugin name: " << name
                         << "(in file:" << file.absoluteFilePath()
                         << "). Original entry from class name: "
                         << m_plugins.value(name)
                              ->metaData()
                              .value("MetaData")
                              .toObject()
                              .value("className")
                              .toString()
                         << "(in file:" << m_plugins.value(name)->fileName()
                         << ").";

        qFatal("Aborting due to duplicate plugin names");
      }

      plugin->setParent(this);
      m_plugins.insert(name, plugin.release());
      qCInfo(navg) << "Found" << name << ":" << file.absoluteFilePath();
    }
  }
}

void
MainWindow::loadPlugin(const QString& name, QPluginLoader* loader)
{
  if (!loader) {
    return;
  }

  // Already loaded?  Nothing to do.
  if (loader->isLoaded()) {
    return;
  }

  if (!loader->load()) {
    qCCritical(navg) << "Unable to load instance of plugin:" << name;
    return;
  }

  auto instance = loader->instance();
  if (!instance) {
    qCCritical(navg) << "Unable to get instance of plugin:" << name;
  }

  auto plugin = qobject_cast<CorePluginInterface*>(instance);
  if (!plugin) {
    qCCritical(navg) << name << "is not a valid CorePlugin";
    return;
  }

  if (m_settings) {
    m_settings->beginGroup(name);
    plugin->loadSettings(m_settings.get());
    m_settings->endGroup();
  }

  plugin->setMapView(m_map_view);

  auto widget = qobject_cast<QWidget*>(instance);
  if (widget) {
    auto dock_widget = new NavGDockWidget;
    dock_widget->setWindowTitle(name);
    dock_widget->setWidget(widget);
    dock_widget->setObjectName(name + "DockWidget");
    if (m_settings) {
      bool lck = m_settings->value("window/lock_plugins", false).toBool();
      if (lck) {
        dock_widget->setFeatures(QDockWidget::NoDockWidgetFeatures);
      }
    }
    connect(this,
            &MainWindow::mapOnlyModeToggled,
            dock_widget,
            &NavGDockWidget::handleMapOnlyModeToggled);

    if (!findChild<QDockWidget*>(dock_widget->objectName())) {
      addDockWidget(Qt::LeftDockWidgetArea, dock_widget);
    }
    if (!restoreDockWidget(dock_widget)) {
      dock_widget->setFloating(true);
      dock_widget->setVisible(false);
    }
  }
  addPluginMenuActions(name, m_plugins.value(name));

  for (auto& other : m_plugins) {
    if (!other->isLoaded()) {
      continue;
    }

    if (other == loader) {
      continue;
    }

    auto other_plugin = qobject_cast<CorePluginInterface*>(other->instance());
    if (!other_plugin) {
      continue;
    }

    other_plugin->connectPlugin(instance, loader->metaData());
    plugin->connectPlugin(other->instance(), other->metaData());
  }
}

void
MainWindow::unloadPlugin(const QString& name, QPluginLoader* loader)
{
  if (!loader->isLoaded()) {
    return;
  }

  // Remove the plugin's actions from the Plugin menu
  auto menuActions = m_menu_plugin->actions();
  auto it =
    std::find_if(std::begin(menuActions),
                 std::end(menuActions),
                 [&name](QAction* action) { return action->text() == name; });

  if (it == std::end(menuActions)) {
    return;
  }

  QAction* menuAction = *it;
  m_menu_plugin->removeAction(menuAction);
  menuAction->deleteLater();

  // Close/Destroy the plugin widget if necessary.
  auto dockWidget = findChild<QDockWidget*>(name + "DockWidget");
  if (dockWidget) {
    removeDockWidget(dockWidget);
    dockWidget->deleteLater();
  }

  loader->unload();
}

bool
MainWindow::saveSettings(const QString& filename)
{
  QSettings settings(filename, QSettings::IniFormat);
  return saveSettings(settings);
}

bool
MainWindow::saveSettings()
{

  if (m_settings) {
    return saveSettings(*m_settings);
  }

  auto filename = QFileDialog::getSaveFileName(this, "", "", "*.ini");
  if (filename.isEmpty()) {
    return false;
  }

  if (!filename.endsWith(".ini")) {
    filename.append(".ini");
  }

  const auto ok = saveSettings(filename);
  if (ok) {
    QSettings app_settings;
    app_settings.setValue("settings_file", filename);
    app_settings.sync();
  }

  return ok;
}

void
MainWindow::inhibitIniSetting(bool state)
{
  m_action_settings_import->setEnabled(!state);
  m_action_settings_save_as->setEnabled(!state);
}

bool
MainWindow::saveSettings(QSettings& settings)
{
  // Window and widget layout.
  settings.clear();

  // Mapview settings
  if (m_map_view) {
    m_map_view->saveSettings(settings);
  }

  // MapScene settings
  if (m_map_view) {
    auto scene_ = qobject_cast<NavGMapScene*>(m_map_view->mapScene());
    if (scene_) {
      scene_->saveSettings(settings);
    }
  }

  settings.beginGroup("window");
  if (m_action_toggle_plugin_lock) {
    settings.setValue("lock_plugins", m_action_toggle_plugin_lock->isChecked());
  }
  settings.setValue("geometry", saveGeometry());
  settings.setValue("layout", saveState());
  settings.setValue("screenshot_directory", screenshot_directory);
  settings.setValue("targetfile_directory", targetfile_directory);
  settings.setValue("targetfile_enabled", targetfile_enabled);

  // icon size
  if (m_action_view_large_icons) {
    if (m_action_view_large_icons->isChecked()) {
      settings.setValue("icons/size", "Large");
    } else {
      settings.setValue("icons/size", "Normal");
    }
  }
  // Toolbar layout
  for (const auto toolbar : findChildren<QToolBar*>()) {
    if (toolbar->objectName().isEmpty()) {
      continue;
    }
    settings.setValue(toolbar->objectName(), toolbar->saveGeometry());
  }
  settings.endGroup();

  for (auto it = std::begin(m_plugins); it != std::end(m_plugins); ++it) {
    auto& name = it.key();
    auto loader = it.value();
    if (!loader || !loader->isLoaded()) {
      qCInfo(navg) << "Skipping unloaded plugin:" << name;
      continue;
    }

    auto plugin = qobject_cast<CorePluginInterface*>(loader->instance());
    if (!plugin) {
      qCWarning(navg) << "Could not save settings for plugin:" << name;
      continue;
    }
    settings.beginGroup(name);
    plugin->saveSettings(settings);
    settings.endGroup();
  }

  settings.sync();

  switch (settings.status()) {
    case QSettings::NoError:
      qCDebug(navg) << "Emitting settings file changed in save settings for file " << settings.fileName();
      emit settingsFileChanged(settings.fileName());
      qCDebug(navg) << "Saved settings to file:" << settings.fileName();
      return true;
    case QSettings::AccessError:
      qCWarning(navg) << "Unable to access file:" << settings.fileName();
      return false;
    case QSettings::FormatError:
      qCWarning(navg) << "Format error writing file:" << settings.fileName();
      return false;
    default:
      qCCritical(
        navg,
        "Unexpected QSettings status code (%d) writing settings to file: %s",
        settings.status(),
        settings.fileName().toStdString().data());
      return false;
  }
}

QSettings::Status
MainWindow::loadSettings(const QString& filename)
{

  if (filename.isEmpty()) {
    qCDebug(navg) << "settings filename is empty, skipping...";
    return QSettings::AccessError;
  }

  qCDebug(navg) << "loading settings from " << filename;
  auto settings = std::make_unique<QSettings>(filename, QSettings::IniFormat);
  const auto status = settings->status();

  if (status == QSettings::NoError) {
    qCDebug(navg) << "Emitting settings file changed in load settings for file " << filename;
    emit settingsFileChanged(filename);
  }

  return status;
}

void
MainWindow::saveSettingsFile(const QString& filename)
{
  // This is a slot hooked up to the saved ini file name. Properly is able to
  // save ini settings now

  auto settings = std::make_unique<QSettings>(filename, QSettings::IniFormat);

  if (m_settings) {
    m_settings.release();
  }

  m_settings.swap(settings);
  
  // make sure to set the settings file value for reload persistence
  QSettings app_settings;
  if (app_settings.value("settings_file").toString() != filename){
    app_settings.setValue("settings_file", filename);
    app_settings.sync();
  }
}

void
MainWindow::restart()
{
  qCDebug(navg) << "Restarting application...";
  qApp->exit(REBOOT_CODE);
}

#if 0
void
MainWindow::loadStaticPlugins()
{
  qCInfo(navg) << "loading static plugins ...";

  if (!m_settings) {
    qCWarning(navg, "Empty settings; no static plugins will be loaded");
    return;
  }

  const auto groups = m_settings->childGroups();

  for (auto plugin : QPluginLoader::staticPlugins()) {
    const auto metadata = plugin.metaData();
    auto name = metadata.value("MetaData").toObject().value("name").toString();

    if (name.isNull()) {
      qCWarning(navg) << "skipping class"
                      << metadata.value("className").toString()
                      << " (no 'name' defined in metadata)";
      continue;
    }

    if (m_plugins.contains(name)) {
      qCCritical(navg)
        << "Duplicate plugin name: " << name
        << ". Original entry from class name: "
        << m_plugins.value(name).metadata.value("className").toString();
      qFatal("Aborting due to duplicate plugin names");
    }

    if (!groups.contains(name)) {
      qCDebug(navg, "Plugin '%s' not present in settings, skipping.",
              name.toStdString().data());
      continue;
    }

    auto instance = plugin.instance();
    if (instance == nullptr) {
      qFatal("Unable to get instance of plugin named '%s'",
             name.toStdString().data());
    }

    if (!qobject_cast<CorePluginInterface*>(instance)) {
      qCWarning(navg) << name << "is not a valid NavG Plugin";
      continue;
    }

    m_plugins.insert(name.toLower(), { "", instance, metadata });

    qCDebug(navg) << "Loaded plugin:" << name;
  }
}
#endif
void
MainWindow::addPluginMenuActions(const QString& name, QPluginLoader* loader)
{
  if (!loader->isLoaded()) {
    return;
  }

  if (!m_menu_plugin) {
    return;
  }

  auto plugin = qobject_cast<CorePluginInterface*>(loader->instance());
  if (!plugin) {
    qCCritical(navg) << name << "is not a valid CorePlugin";
    return;
  }

  //
  // Create plugin menu entry
  //
  auto menu = new QMenu(name);
  const auto menu_actions = plugin->pluginMenuActions();
  menu->addActions(menu_actions);

  // Add action to toggle visibilty of plugin docked widget
  auto widget = findChild<QDockWidget*>(name + "DockWidget");
  if (widget) {
    auto action = widget->toggleViewAction();
    action->setText("Visible");
    if (!menu->isEmpty()) {
      menu->addSeparator();
    }
    menu->addAction(action);
  }

  // No actions for plugin?  Delete the menu.  Otherwise add it.
  if (menu->isEmpty()) {
    delete menu;
  } else {
    m_menu_plugin->insertMenu(m_action_show_plugin_about, menu);
    m_menu_plugin->addMenu(menu);
  }

  //
  // Create plugin toolbar
  //
  const auto toolbar_actions = plugin->pluginToolbarActions();
  if (!toolbar_actions.isEmpty()) {
    auto toolbar = new QToolBar(name);
    toolbar->addActions(toolbar_actions);
    connect(
      this, &MainWindow::mapOnlyModeToggled, toolbar, &QToolBar::setHidden);
    // View toolbar connection
    connect(this, &MainWindow::iconSizeChanged, [this, toolbar](auto size) {
      this->scaleActionIcons(toolbar, size);
    });

    if (m_action_view_large_icons) {
      if (m_action_view_large_icons->isChecked()) {
        scaleActionIcons(toolbar, LARGE_ICON_SIZE);
      }
    }
    addToolBar(toolbar);
  }
}

void
MainWindow::createMenus()
{

  connect(
    this, &MainWindow::mapOnlyModeToggled, menuBar(), &QMenuBar::setHidden);
  m_menu_settings = menuBar()->addMenu("&Settings");
  m_menu_settings->addAction(m_action_settings_save);
  m_menu_settings->addAction(m_action_settings_import);
  m_menu_settings->addAction(m_action_settings_save_as);
  m_menu_settings->addSeparator();
  m_menu_settings->addAction(m_action_settings_set_screenshot_path);
  m_menu_settings->addSeparator();
  m_menu_settings->addAction(m_action_settings_set_targetfile_enabled);
  m_menu_settings->addAction(m_action_settings_set_targetfile_path);
  
  
  // Set up the "Map" menu
  m_menu_map = menuBar()->addMenu("&Map");

  // The graticule actions are combined into an ActionGroup
  // within a submenu.
  m_menu_map_graticule = m_menu_map->addMenu("&Graticule Projection");
  m_menu_map_graticule->setParent(m_menu_map);
  auto graticule_group = new QActionGroup(this);
  graticule_group->addAction(m_action_map_graticule_latlon);
  graticule_group->addAction(m_action_map_graticule_utm);
  graticule_group->addAction(m_action_map_graticule_alvinxy);
  m_action_map_graticule_latlon->setChecked(true);
  m_menu_map_graticule->addAction(m_action_map_graticule_latlon);
  m_menu_map_graticule->addAction(m_action_map_graticule_utm);
  m_menu_map_graticule->addAction(m_action_map_graticule_alvinxy);

  // The map projection actions are combined into an ActionGroup
  // within a submenu.
  m_menu_map_projection = m_menu_map->addMenu("&Map Projection");
  m_menu_map_projection->setParent(m_menu_map);
  auto map_proj_group = new QActionGroup(this);
  map_proj_group->addAction(m_action_map_scene_mercator);
  map_proj_group->addAction(m_action_map_scene_utm);
  map_proj_group->addAction(m_action_map_scene_alvinxy);
  m_action_map_scene_mercator->setChecked(true);
  m_menu_map_projection->addAction(m_action_map_scene_mercator);
  m_menu_map_projection->addAction(m_action_map_scene_utm);
  m_menu_map_projection->addAction(m_action_map_scene_alvinxy);

  m_menu_map->addSeparator();

  m_menu_map->addAction(m_action_edit_alvinxy_origin);
  m_menu_map->addAction(m_action_set_max_zoom_limit);
  m_menu_map->addAction(m_action_clear_mapscene_underlays);
  m_menu_map->addAction(m_action_clear_target_underlays);

  m_menu_map->addSeparator();
  auto coordinate_action_group = new QActionGroup(this);
  auto coordinate_menu = m_menu_map->addMenu("&Display Lat/Lon as...");
  auto coordinate_action =
    coordinate_action_group->addAction("&Decimal Degrees (North, East Only)");
  coordinate_action->setCheckable(true);
  coordinate_action->setChecked(true);
  connect(coordinate_action, &QAction::triggered, [=]() {
    m_map_view->setLatLonFormat(dslmap::LatLonFormat::DD_NE);
  });
  coordinate_menu->addAction(coordinate_action);

  coordinate_action = coordinate_action_group->addAction("D&ecimal Degrees");
  coordinate_action->setCheckable(true);
  connect(coordinate_action, &QAction::triggered, [=]() {
    m_map_view->setLatLonFormat(dslmap::LatLonFormat::DD);
  });
  coordinate_menu->addAction(coordinate_action);

  coordinate_action =
    coordinate_action_group->addAction("Decimal &Minutes (North, East Only)");
  coordinate_action->setCheckable(true);
  connect(coordinate_action, &QAction::triggered, [=]() {
    m_map_view->setLatLonFormat(dslmap::LatLonFormat::DM_NE);
  });
  coordinate_menu->addAction(coordinate_action);

  coordinate_action = coordinate_action_group->addAction("Decimal Min&utes");
  coordinate_action->setCheckable(true);
  connect(coordinate_action, &QAction::triggered, [=]() {
    m_map_view->setLatLonFormat(dslmap::LatLonFormat::DM);
  });
  coordinate_menu->addAction(coordinate_action);

  m_menu_layers = menuBar()->addMenu("&Layers");
  m_menu_layers->addAction(m_action_toggle_layer_widget);
  m_menu_layers->addSeparator();
  m_menu_layers->addAction(m_action_add_bathymetry_layer);
  m_menu_layers->addAction(m_action_add_contour_layer);
  //  m_menu_layers->addAction(m_action_add_vector_layer);
  m_menu_layers->addAction(m_action_add_image_layer);
  m_menu_layers->addAction(m_action_add_target_layer);

  m_menu_plugin = new QMenu;
  m_menu_plugin->setTitle("&Plugins");
  m_menu_plugin->addAction(m_action_show_plugin_about);
  m_menu_plugin->addAction(m_action_toggle_plugin_lock);
  for (const auto& name : m_plugins.keys()) {
    addPluginMenuActions(name, m_plugins.value(name));
  }
#if 0
  // createFileMenu();

  m_menu_file = menuBar()->addMenu("&File");

  auto quit_action = new QAction("&Quit", this);
  quit_action->setToolTip(tr("Quit NavG"));
  quit_action->setShortcut(QKeySequence::Quit);
  connect(quit_action, &QAction::triggered, this, &QMainWindow::close);
  m_menu_file->addAction(quit_action);

  m_menu_layout = menuBar()->addMenu("&Layout");
  auto lock_action = new QAction("&Locked", this);
  lock_action->setToolTip(tr("Lock or unlock plugin widget layout."));
  lock_action->setCheckable(true);
  lock_action->setChecked(true);
  connect(lock_action, &QAction::triggered, this, [lock_action, this](bool checked) {
    const auto features = checked ? QDockWidget::NoDockWidgetFeatures : QDockWidget::AllDockWidgetFeatures;
    for(auto w: this->findChildren<QDockWidget *>())
    {
      w->setFeatures(features);
    }
  });

  auto save_action = new QAction("&Save layout", this);
  save_action->setToolTip(tr("Save the current layout."));
  connect(save_action, &QAction::triggered, this, [save_action, this]() {
    auto ok = QMessageBox::warning(
        this, tr("Overwrite saved layout?"),
        tr("Are you sure you wish to overwrite the existing saved layout?"),
        QMessageBox::Save | QMessageBox::Cancel);

    if (!ok) {
      return;
    }

    QSettings settings;
    settings.setValue("layout", saveState());
    settings.setValue("geometry", saveGeometry());
    settings.sync();
  });


  auto saveas_action = new QAction("&Save layout as...", this);
  saveas_action->setToolTip(tr("Save the current layout to a new file."));
  connect(saveas_action, &QAction::triggered, this, [saveas_action, this]() {

    QSettings settings;
    const auto filename = QFileDialog::getSaveFileName(
        this,
        tr("Save Layout"),
        settings.fileName(),
        tr("*.ini"));

    if (filename.isEmpty())
    {
      return;
    }

    // Copy the current settings file to the new location
    if (!QFile::copy(settings.fileName(), filename))
    {
      QMessageBox::critical(this, tr("Error"), QString("Could not create file: %1").arg(filename), QMessageBox::Ok);
      return;
    }

    QSettings new_settings(filename);
    new_settings.setValue("layout", saveState());
    new_settings.setValue("geometry", saveGeometry());
    new_settings.sync();
  });

  m_menu_layout->addAction(lock_action);
  m_menu_layout->addAction(save_action);
  m_menu_layout->addAction(saveas_action);
#endif

  if (m_menu_plugin) {
    menuBar()->addMenu(m_menu_plugin);
  }

  // View Menu
  m_menu_view = menuBar()->addMenu("&View");

  m_menu_icon_size = m_menu_view->addMenu("Icon Size");
  m_menu_icon_size->setParent(m_menu_view);

  auto icon_group = new QActionGroup(this);
  icon_group->addAction(m_action_view_normal_icons);
  icon_group->addAction(m_action_view_large_icons);

  m_menu_icon_size->addAction(m_action_view_normal_icons);
  m_menu_icon_size->addAction(m_action_view_large_icons);
  m_action_view_normal_icons->setChecked(true);

  //
  // Window menu
  //

  m_menu_window = menuBar()->addMenu("&Window");
  m_menu_window->addAction(m_action_window_full_screen);
  m_menu_window->addAction(m_action_window_map_only);
}

void
MainWindow::createWidgets()
{

  cursor_color_widget = new dslmap::ColorPickButton();
  cursor_color_widget->setToolTip("Change the map cursor color...");
  auto pixmap = QPixmap{ QSize{ NORMAL_ICON_SIZE, NORMAL_ICON_SIZE } };
  cursor_color_widget->setIcon(QIcon{ pixmap });
  connect(cursor_color_widget,
          &dslmap::ColorPickButton::colorChanged,
          m_map_view,
          &NavGMapView::setCursorColor,
          Qt::QueuedConnection);
  connect(
    cursor_color_widget,
    &dslmap::ColorPickButton::colorChanged,
    [this](auto color) {
      QPixmap temp_pixmap;
      if (m_action_view_large_icons->isChecked()) {
        temp_pixmap = QPixmap{ QSize{ LARGE_ICON_SIZE, LARGE_ICON_SIZE } };
      } else {
        temp_pixmap = QPixmap{ QSize{ NORMAL_ICON_SIZE, NORMAL_ICON_SIZE } };
      }
      temp_pixmap.fill(color);
      cursor_color_widget->setIcon(QIcon{ temp_pixmap });
    });

  m_action_cursor_color = new QWidgetAction(this);
  m_action_cursor_color->setDefaultWidget(cursor_color_widget);
  m_action_cursor_color->setToolTip("Change the map cursor color...");

  rangebearing_color_widget = new dslmap::ColorPickButton();
  rangebearing_color_widget->setToolTip(
    "Change the Range/Bearing tool color...");
  auto rangebearing_color_pixmap =
    QPixmap{ QSize{ NORMAL_ICON_SIZE, NORMAL_ICON_SIZE } };
  rangebearing_color_widget->setIcon(QIcon{ rangebearing_color_pixmap });
  connect(rangebearing_color_widget,
          &dslmap::ColorPickButton::colorChanged,
          m_map_view,
          &NavGMapView::setRangeBearingColor,
          Qt::QueuedConnection);
  connect(
    rangebearing_color_widget,
    &dslmap::ColorPickButton::colorChanged,
    [this](auto color) {
      QPixmap temp_pixmap;
      if (m_action_view_large_icons->isChecked()) {
        temp_pixmap = QPixmap{ QSize{ LARGE_ICON_SIZE, LARGE_ICON_SIZE } };
      } else {
        temp_pixmap = QPixmap{ QSize{ NORMAL_ICON_SIZE, NORMAL_ICON_SIZE } };
      }
      // temp_pixmap.fill(color);
      rangebearing_color_widget->setIcon(QIcon{ temp_pixmap });
      rangebearing_color_widget->setIcon(
        QIcon{ ":GIS-icons/24x24/line-edit.png" });
    });

  m_action_rangebearing_color = new QWidgetAction(this);
  m_action_rangebearing_color->setDefaultWidget(rangebearing_color_widget);
  m_action_rangebearing_color->setToolTip(
    "Change the Range/Bearing tool color...");
}

void
MainWindow::createActions()
{

  //
  // Graticule actions.
  //

  // Switching to Lat/lon should never fail.
  m_action_map_graticule_latlon = new QAction("&Lat/Lon", this);
  m_action_map_graticule_latlon->setCheckable(true);
  connect(m_action_map_graticule_latlon, &QAction::triggered, [=]() {
    this->m_map_view->setGraticuleProjectionType(
      NavGMapView::GraticuleType::LatLon);
  });

  // UTM can fail if the scene is empty.
  m_action_map_graticule_utm = new QAction("&UTM", this);
  m_action_map_graticule_utm->setCheckable(true);
  connect(m_action_map_graticule_utm, &QAction::triggered, [=]() {
    if (!this->m_map_view->setGraticuleProjectionType(
          NavGMapView::GraticuleType::UTM)) {
      QMessageBox::critical(
        this,
        QStringLiteral("Error"),
        QStringLiteral(
          "Unable to set graticule to UTM projection.  Is the scene empty?"),
        QMessageBox::StandardButton::Close);
    }
  });

  // AlvinXY can fail if no origin is defined.
  m_action_map_graticule_alvinxy = new QAction("&Alvin XY", this);
  m_action_map_graticule_alvinxy->setCheckable(true);
  connect(m_action_map_graticule_alvinxy, &QAction::triggered, [=]() {
    if (!this->m_map_view->setGraticuleProjectionType(
          NavGMapView::GraticuleType::AlvinXY)) {
      QMessageBox::critical(
        this,
        QStringLiteral("Error"),
        QStringLiteral("Unable to set graticule to Alvin XY projection.  Is an "
                       "XY origin defined?"),
        QMessageBox::StandardButton::Close);
    }
  });

  //
  // Scene projection actions
  //

  // Switching to Mercator should never fail.
  m_action_map_scene_mercator = new QAction("&Mercator", this);
  m_action_map_scene_mercator->setCheckable(true);
  connect(m_action_map_scene_mercator, &QAction::triggered, [=]() {
    auto scene = qobject_cast<NavGMapScene*>(this->m_map_view->mapScene());
    if (!scene) {
      QMessageBox::critical(
        this,
        QStringLiteral("Error"),
        QStringLiteral("Unable to get map scene from view; this is a BUG and "
                       "should not be possible"),
        QMessageBox::StandardButton::Close);
    }
    scene->setProjectionType(NavGMapScene::ProjectionType::Mercator);
  });

  // UTM can fail if the scene is empty.
  m_action_map_scene_utm = new QAction("&UTM", this);
  m_action_map_scene_utm->setCheckable(true);
  connect(m_action_map_scene_utm, &QAction::triggered, [=]() {
    auto scene = qobject_cast<NavGMapScene*>(this->m_map_view->mapScene());
    if (!scene) {
      QMessageBox::critical(
        this,
        QStringLiteral("Error"),
        QStringLiteral("Unable to get map scene from view; this is a BUG and "
                       "should not be possible"),
        QMessageBox::StandardButton::Close);
    }
    if (!scene->setProjectionType(NavGMapScene::ProjectionType::UTM)) {
      QMessageBox::critical(
        this,
        QStringLiteral("Error"),
        QStringLiteral(
          "Unable to set map to UTM projection.  Is the scene empty?"),
        QMessageBox::StandardButton::Close);
    }
  });

  // AlvinXY can fail if no origin is defined.
  m_action_map_scene_alvinxy = new QAction("&Alvin XY", this);
  m_action_map_scene_alvinxy->setCheckable(true);
  connect(m_action_map_scene_alvinxy, &QAction::triggered, [=]() {
    auto scene = qobject_cast<NavGMapScene*>(this->m_map_view->mapScene());
    if (!scene) {
      QMessageBox::critical(
        this,
        QStringLiteral("Error"),
        QStringLiteral("Unable to get map scene from view; this is a BUG and "
                       "should not be possible"),
        QMessageBox::StandardButton::Close);
    }
    if (!scene->setProjectionType(NavGMapScene::ProjectionType::AlvinXY)) {
      QMessageBox::critical(
        this,
        QStringLiteral("Error"),
        QStringLiteral(
          "Unable to set map to AlvinXY projection.  Is an XY origin defined?"),
        QMessageBox::StandardButton::Close);
    }
  });

  m_action_edit_alvinxy_origin =
    new QAction(QStringLiteral("&AlvinXY Origin..."), this);
  connect(m_action_edit_alvinxy_origin, &QAction::triggered, [=]() {
    auto scene_ = qobject_cast<NavGMapScene*>(m_map_view->mapScene());
    if (!scene_) {
      return;
    }

    auto dialog = std::make_unique<AlvinXYOriginDialog>(this);
    if (scene_->hasAlvinXYOrigin()) {
      dialog->setOrigin(scene_->alvinXYOrigin());
    }
    if (scene_->hasStartCoords()) {
      dialog->setStart(scene_->XYStart());
    }

    dialog->setDiveNum(scene_->diveNum());
    dialog->setVehicleName(scene_->vehicleName());

    auto dialog_origin_init = dialog->origin();
    auto dialog_start_init = dialog->startCoords();
    auto dialog_divenum_init = dialog->diveNum();
    auto dialog_vehiclename_init = dialog->vehicleName();

    if (!dialog->exec()) {
      return;
    }

    if (QMessageBox::Ok !=
      QMessageBox::question(
        this, "AlvinXY Origin Confirmation", "Please confirm your entries for dive origin information. \n\nOrigin Latitude: "+QString::number(dialog->origin().y(), 'f', 8) + "\n\n Origin Longitude: " +QString::number(dialog->origin().x(), 'f', 8) 
                                                                                                        + "\n\nStart Latitude: "+QString::number(dialog->startCoords().y(), 'f', 8) + "\n\nStart Longitude: " +QString::number(dialog->startCoords().x(), 'f', 8)
                                                                                                        + "\n\nDive Number: "+QString::number(dialog->diveNum())
                                                                                                        + "\n\nVehicle Name: "+dialog->vehicleName(), QMessageBox::Cancel | QMessageBox::Ok)) {
        m_action_edit_alvinxy_origin->activate(QAction::Trigger);
        dialog->setOrigin(dialog_origin_init);
        dialog->setStart(dialog_start_init);
        dialog->setDiveNum(dialog_divenum_init);
        dialog->setVehicleName(dialog_vehiclename_init);
      }
    else {
      scene_->setAlvinXYOrigin(dialog->origin());
      scene_->setStartXY(dialog->startCoords());

      // divenum and vehicle name
      scene_->setDiveNumber(dialog->diveNum());
      scene_->setVehicleName(dialog->vehicleName());
    }
  });

  m_action_set_max_zoom_limit =
    new QAction(QStringLiteral("&Set Zoom Threshold"), this);
  connect(m_action_set_max_zoom_limit, &QAction::triggered, [=]() {
    auto scene_ = qobject_cast<NavGMapScene*>(m_map_view->mapScene());
    if (!scene_) {
      return;
    }

    auto dialog = std::make_unique<MaxZoomOutDialog>(this);
    if (m_map_view) {
      dialog->setZoomBox(m_map_view->getZoomThreshold());
    }

    if (!dialog->exec()) {
      return;
    }

    double zoom_value = dialog->getZoomValue();
    if (m_map_view) {
      m_map_view->setZoomThreshold(zoom_value);
    }
  });

  m_action_clear_mapscene_underlays =
    new QAction(QStringLiteral("&Clear Preload Underlays"), this);
  connect(m_action_clear_mapscene_underlays, &QAction::triggered, [=]() {
    auto scene_ = qobject_cast<NavGMapScene*>(m_map_view->mapScene());
    if (!scene_) {
      return;
    }

    scene_->clearUnderlayPreloads();
  });

  m_action_clear_target_underlays =
    new QAction(QStringLiteral("&Clear Target Underlays"), this);
  connect(m_action_clear_target_underlays, &QAction::triggered, [=]() {
    auto scene_ = qobject_cast<NavGMapScene*>(m_map_view->mapScene());
    if (!scene_) {
      return;
    }

    scene_->clearTargetPreloads();
  });

  m_action_add_bathymetry_layer =
    new QAction(QIcon{ ":GIS-icons/24x24/layer-raster-add.png" },
                QString{ "Add Bathymetry Layer" },
                this);
  m_action_add_bathymetry_layer->setToolTip("Add a bathymetry layer...");
  connect(m_action_add_bathymetry_layer,
          &QAction::triggered,
          this,
          &MainWindow::loadBathyLayerDialog);

  m_action_add_contour_layer =
    new QAction(QIcon{ ":GIS-icons/24x24/layer-contour-add.png" },
                QString{ "Add Contour Layer" },
                this);
  m_action_add_contour_layer->setToolTip("Add a contour layer...");
  connect(m_action_add_contour_layer,
          &QAction::triggered,
          this,
          &MainWindow::loadContourLayerDialog);

  m_action_add_vector_layer =
    new QAction(QIcon{ ":GIS-icons/24x24/layer-vector-add.png" },
                QString{ "Add Vector Layer" },
                this);
  m_action_add_vector_layer->setToolTip("Add a vector layer...");
  connect(m_action_add_vector_layer,
          &QAction::triggered,
          this,
          &MainWindow::loadVectorLayerDialog);

  m_action_add_image_layer =
    new QAction(QIcon{ ":GIS-icons/24x24/layer_to_preview_add.png" },
                QString{ "Add Image Layer" },
                this);
  m_action_add_image_layer->setToolTip("Add an image layer...");
  connect(m_action_add_image_layer,
          &QAction::triggered,
          this,
          &MainWindow::loadImageLayerDialog);

  m_action_add_target_layer =
    new QAction(QIcon{ ":GIS-icons/24x24/layer-target-add.png" },
                QString{ "Add Target Layer" },
                this);
  m_action_add_target_layer->setToolTip("Add a target layer...");
  connect(m_action_add_target_layer,
          &QAction::triggered,
          this,
          &MainWindow::loadTargetLayerDialog);

  m_action_add_georeference_image_layer =
    new QAction(QIcon{ ":GIS-icons/24x24/geoimage-add.png" },
                QString{ "Add Georeference Image Layer" },
                this);
  m_action_add_georeference_image_layer->setToolTip(
    "Add a georeferenced image layer...");
  connect(m_action_add_georeference_image_layer,
          &QAction::triggered,
          this,
          &MainWindow::loadGeoreferenceImageLayerDialog);

  m_action_toggle_layer_widget = m_layer_widget->toggleViewAction();
  m_action_toggle_layer_widget->setIcon(QIcon{ ":GIS-icons/24x24/layer.png" });
  m_action_toggle_layer_widget->setToolTip(
    "Show or hide the layer list widget.");

  m_action_toggle_range_bearing_tool =
    new QAction(QIcon{ ":GIS-icons/24x24/angle-measure.png" },
                QString{ "Measure Range and Bearing" },
                this);
  m_action_toggle_range_bearing_tool->setToolTip(
    "Measure range and bearing...");
  m_action_toggle_range_bearing_tool->setCheckable(true);
  m_action_toggle_range_bearing_tool->setChecked(false);
  m_action_toggle_range_bearing_tool->setEnabled(true);
  connect(m_action_toggle_range_bearing_tool,
          static_cast<void (QAction::*)(bool)>(&QAction::toggled),
          [this](bool enabled) {
            this->m_map_view->setRangeBearingToolEnabled(enabled);
          });

  m_action_take_screenshot = new QAction(QIcon{ ":GIS-icons/24x24/camera.png" },
                                         QString{ "Take a screenshot" },
                                         this);
  m_action_take_screenshot->setToolTip("Take a screenshot...");
  connect(m_action_take_screenshot,
          &QAction::triggered,
          this,
          &MainWindow::takeScreenshot);

  //
  // Settings pluginMenuActions
  //
  m_action_settings_import = new QAction("&Import...", this);
  m_action_settings_import->setToolTip(tr("Import settings from file"));
  connect(m_action_settings_import, &QAction::triggered, [this]() {
    const auto filename = QFileDialog::getOpenFileName(this, "", "", "*.ini");
    if (filename.isEmpty()) {
      return;
    }
    
    // This isnt safe to call even if called after message confirmation.
    // Will bug if canceled and you will have wrong settings. Preexisting bug on master

    //if (loadSettings(filename) != QSettings::NoError) {
    //  return;
    //}

    if (filename.isEmpty()) {
      return;
    }

    auto ok = QMessageBox::information(
      this,
      "Restart Required.",
      "NavG needs to restart to load the new settings.",
      QMessageBox::Ok | QMessageBox::Cancel);

    if (ok != QMessageBox::Ok) {
      return;
    }

    QSettings app_settings;
    app_settings.setValue("settings_file", filename);
    app_settings.sync();
    restart();
  });

  m_action_settings_set_screenshot_path =
    new QAction("Set screenshot path...", this);
  m_action_settings_set_screenshot_path->setToolTip(
    tr("Save settings to new file"));
  connect(m_action_settings_set_screenshot_path, &QAction::triggered, [this]() {
    QString path = QFileDialog::getExistingDirectory(
      this,
      tr("Open Directory"),
      QDir::currentPath(),
      QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    if (path.isEmpty()) {
      return;
    }
    screenshot_directory = path;
  });

  m_action_settings_set_targetfile_enabled = 
    new QAction("Set target file enabled", this);
  m_action_settings_set_targetfile_enabled->setCheckable(true);
  m_action_settings_set_targetfile_enabled->setToolTip(
    tr("Enable/Disable Auto target export"));
    connect(m_action_settings_set_targetfile_enabled, &QAction::triggered, [this]() {
      targetfile_enabled = m_action_settings_set_targetfile_enabled->isChecked();
      qDebug()<<"Target file enabled is: "<<targetfile_enabled;
      emit targetfileExportEnabled(m_action_settings_set_targetfile_enabled->isChecked());      
    });

  m_action_settings_set_targetfile_path =
    new QAction("Set target file path...", this);
  m_action_settings_set_targetfile_path->setToolTip(
    tr("Save Target layer to file"));
  connect(m_action_settings_set_targetfile_path, &QAction::triggered, [this]() {
    QString path = QFileDialog::getExistingDirectory(
      this,
      tr("Open Directory"),
      QDir::currentPath(),
      QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    if (path.isEmpty()) {
      return;
    }
    targetfile_directory = path;
    emit targetfileDirectoryChanged(targetfile_directory);
});

  m_action_settings_save_as = new QAction("Save &as...", this);
  m_action_settings_save_as->setToolTip(tr("Save settings to new file"));
  m_action_settings_save_as->setShortcut(QKeySequence::SaveAs);
  connect(m_action_settings_save_as, &QAction::triggered, [this]() {
    auto filename = QFileDialog::getSaveFileName(this, "", "", "*.ini");
    if (filename.isEmpty()) {
      return;
    }
    if (!filename.endsWith(".ini")) {
      filename.append(".ini");
    }
    this->saveSettings(filename);
  });

  m_action_settings_save = new QAction("&Save", this);
  m_action_settings_save->setToolTip(tr("Save settings"));
  m_action_settings_save->setShortcut(QKeySequence::Save);
  connect(m_action_settings_save,
          &QAction::triggered,
          this,
          static_cast<bool (MainWindow::*)()>(&MainWindow::saveSettings));

  m_action_show_plugin_about = new QAction("&Load More Plugins...", this);
  m_action_show_plugin_about->setToolTip(tr("View and load available plugins"));
  connect(m_action_show_plugin_about, &QAction::triggered, [this]() {
    auto dialog = new AboutPluginsDialog(this);
    dialog->setPlugins(m_plugins);
    if (dialog->exec() != QDialog::Accepted) {
      return;
    }

    const auto results = dialog->desiredPlugins();
    for (const auto& name : results.keys()) {
      if (results.value(name)) {
        loadPlugin(name, m_plugins.value(name));
      } else {
        unloadPlugin(name, m_plugins.value(name));
      }
    }
  });
  m_action_toggle_plugin_lock = new QAction("&Lock Plugins...", this);
  m_action_toggle_plugin_lock->setCheckable(true);
  m_action_toggle_plugin_lock->setToolTip(
    tr("Freeze the current visibility and layout of plugins"));
  connect(
    m_action_toggle_plugin_lock, &QAction::triggered, [this](bool checked) {
      if (m_settings) {
        m_settings->setValue("lock_plugins", checked);
      }
      for (auto child : this->children()) {
        auto dock = qobject_cast<QDockWidget*>(child);
        if (dock) {
          if (checked) {
            dock->setFeatures(QDockWidget::NoDockWidgetFeatures);
          } else {
            dock->setFeatures(QDockWidget::DockWidgetClosable |
                              QDockWidget::DockWidgetMovable |
                              QDockWidget::DockWidgetFloatable);
          }
        }
      }
    });

  //
  //  Window Actions
  //

  m_action_window_full_screen = new QAction("&Full Screen", this);
  m_action_window_full_screen->setShortcut(Qt::Key_F10);
  m_action_window_full_screen->setShortcutContext(
    Qt::ShortcutContext::WidgetShortcut);
  m_action_window_full_screen->setCheckable(true);
  connect(m_action_window_full_screen,
          static_cast<void (QAction::*)(bool)>(&QAction::toggled),
          [=](bool enabled) {
            if (enabled) {
              setWindowState(windowState() | Qt::WindowState::WindowFullScreen);
            } else {
              setWindowState(windowState() &
                             ~Qt::WindowState::WindowFullScreen);
            }
          });
  auto shortcut = new QShortcut(QKeySequence(tr("F10")),
                                this,
                                0,
                                0,
                                Qt::ShortcutContext::ApplicationShortcut);
  connect(shortcut,
          &QShortcut::activated,
          m_action_window_full_screen,
          &QAction::toggle);

  // The "fullscreen" and "map only" actions need two shortcuts:
  // one attached to the application which calls the action when activated,
  // and another attached to the action to be triggered throught the menu.
  //
  // We need to do this because the menu actions get disabled when the menus
  // themselves are hidden!
  m_action_window_map_only = new QAction("&Map Only", this);
  m_action_window_map_only->setCheckable(true);
  m_action_window_map_only->setShortcut(Qt::Key_F11);
  m_action_window_map_only->setShortcutContext(
    Qt::ShortcutContext::WidgetShortcut);
  shortcut = new QShortcut(QKeySequence(tr("F11")),
                           this,
                           0,
                           0,
                           Qt::ShortcutContext::ApplicationShortcut);
  connect(shortcut,
          &QShortcut::activated,
          m_action_window_map_only,
          &QAction::toggle);

  connect(m_action_window_map_only,
          static_cast<void (QAction::*)(bool)>(&QAction::toggled),
          this,
          &MainWindow::setMapOnlyModeEnabled);
  shortcut = new QShortcut(QKeySequence(tr("Esc")),
                           this,
                           0,
                           0,
                           Qt::ShortcutContext::ApplicationShortcut);
  connect(shortcut, &QShortcut::activated, [=]() {
    m_action_window_full_screen->setChecked(false);
    m_action_window_map_only->setChecked(false);
  });

  // View actions

  m_action_view_normal_icons = new QAction("Normal", this);
  m_action_view_normal_icons->setCheckable(true);
  m_action_view_large_icons = new QAction("Large", this);
  m_action_view_large_icons->setCheckable(true);

  connect(m_action_view_normal_icons, &QAction::triggered, [this]() {
    emit iconSizeChanged(NORMAL_ICON_SIZE);
  });
  connect(m_action_view_large_icons, &QAction::triggered, [this]() {
    emit iconSizeChanged(LARGE_ICON_SIZE);
  });
}

void
MainWindow::scaleActionIcons(QToolBar* toolbar, int size)
{

  if (!toolbar) {
    return;
  }

  toolbar->setIconSize(QSize(size, size));

  auto actions = toolbar->actions();
  for (auto action : actions) {
    if (action) {

      QWidgetAction* waction = qobject_cast<QWidgetAction*>(action);
      if (waction) {
        // Can handle qwidget actions separate ways
        auto action_widget = waction->defaultWidget();
        if (action_widget) {
          dslmap::ColorPickButton* cursor_color_pick =
            qobject_cast<dslmap::ColorPickButton*>(action_widget);
          dslmap::ColorPickButton* rangebearing_color_pick =
            qobject_cast<dslmap::ColorPickButton*>(action_widget);

          // special case to handle scaling of a colorpick widget in toolbar
          if (cursor_color_pick) {

            auto color = cursor_color_pick->color();
            auto pixmap = QPixmap{ QSize{ size, size } };
            pixmap.fill(color);
            cursor_color_pick->setIcon(QIcon{ pixmap });
            continue;
          }
          if (rangebearing_color_pick) {
            auto color = rangebearing_color_pick->color();
            auto pixmap = QPixmap{ QSize{ size, size } };
            pixmap.fill(color);
            rangebearing_color_pick->setIcon(QIcon{ pixmap });
            continue;
          }
        }
      }

      QPixmap scaled_pix = action->icon().pixmap(size, size);
      scaled_pix = scaled_pix.scaled(size, size, Qt::KeepAspectRatio);

      QIcon icon = QIcon(scaled_pix);
      action->setIcon(icon);
    }
  }
}

void
MainWindow::loadContourLayerDialog()
{
  QSettings app_settings;

  auto dlog = new dslmap::ContourImportDialog(this);

  if (dlog->exec() != QDialog::Accepted) {
    return;
  }

  auto filename = dlog->extractFilename();
  auto spacing = dlog->extractSpacing();

  loadContourLayer(filename, spacing);
}

void
MainWindow::loadContourLayer(QString filename, int spacing)
{
  if (filename.isEmpty()) {
    return;
  }
  auto layer = std::unique_ptr<dslmap::ContourLayer>{};
  if (filename.endsWith(".grd")) {
    layer = dslmap::ContourLayer::fromGrdFile(filename, spacing);
  } else if (filename.endsWith(".shp")) {
    layer = dslmap::ContourLayer::fromShapefile(filename);
  } else {
    return;
  }

  if (!layer) {
    qCCritical(navg,
               "Failed to load contours from file: %s",
               filename.toStdString().data());
    return;
  }
  auto scene = m_map_view->mapScene();
  auto scene_gradient = scene->gradient();
  if (scene_gradient) {
    layer->setGradient(scene_gradient);
  } else {
    auto grad = layer->gradient();
    grad->setGradient(dslmap::GdalColorGradient::GradientPreset::HAXBY);
    scene->setGradient(grad);
  }

  auto layername = QFileInfo(filename).baseName();
  layer->setName(layername);
  layer->setSpacing(spacing);
  layer->setSourceFile(filename);

  const auto uuid = scene->addLayer(layer.get());

  if (uuid.isNull()) {
    qCCritical(navg, "Failed to add contour layer to map scene.");
    return;
  }

  connect(layer.get(),
          &dslmap::ContourLayer::saveContourUnderlay,
          this,
          &MainWindow::setSceneContour);

  layer.release();
}

void
MainWindow::setSceneContour(QString filename, int spacing)
{
  auto scene = qobject_cast<NavGMapScene*>(m_map_view->mapScene());
  if (!scene) {
    return;
  }

  scene->setContourPath(filename, spacing);
}

void
MainWindow::loadGeoreferenceImageLayerDialog()
{
  QSettings app_settings;

  QScopedPointer<dslmap::GeoreferenceImport> widget(
    new dslmap::GeoreferenceImport(this));

  if (widget->exec() == QDialog::Rejected) {
    qCWarning(navg, "qdialog x'd out.");
    return;
  }
  if (widget->getFileName().isEmpty()) {
    // window closed but filename wasn't set
    qCWarning(navg, "No filename set for geoimage.");
    return;
  }

  auto filename = widget->getFileName();
  if (filename.isEmpty()) {
    return;
  }
  // ok so we want to get a layer from widget gui before deciding to make new
  // layer

  auto layerName = widget->getLayerName();

  auto transColor = widget->getTransColor();

  loadGeoreferenceImageLayer(filename, layerName, transColor);
}

void
MainWindow::loadGeoreferenceImageLayer(QString filename,
                                       QString layerName,
                                       QColor transColor)
{
  if (layerName.isEmpty()) {
    layerName = QFileInfo(filename).baseName();
  }
  auto layer = std::make_unique<dslmap::GeoreferenceImageLayer>();
  auto currProj = m_map_view->mapScene()->projection();

  if (!currProj) {
    if (!layer->addRasterFile(filename, 1)) {
      qCWarning(
        navg, "Failed to open raster file: %s", filename.toStdString().data());
      return;
    }
  } else {
    if (!layer->addRasterFile(filename, 1)) {
      qCWarning(
        navg, "Failed to open raster file: %s", filename.toStdString().data());
      return;
    }
  }

  if (!layerName.isEmpty()) {
    layer->setName(layerName);
  }
  // setting layer name if user defines one in gui

  auto objs = layer->georeferenceImageObjects();

  for (auto obj : objs) {
    obj->setTransparentColor(transColor);
  }

  const auto uuid = m_map_view->mapScene()->addLayer(layer.get());

  if (uuid.isNull()) {
    qCCritical(navg, "Failed to add raster layer to map scene.");
    return;
  }

  connect(layer.get(),
          &dslmap::GeoreferenceImageLayer::saveGeoimageUnderlay,
          this,
          &MainWindow::setSceneGeoimage);

  layer.release();
}

void
MainWindow::setSceneGeoimage(QString filename)
{
  auto scene = qobject_cast<NavGMapScene*>(m_map_view->mapScene());
  if (!scene) {
    return;
  }

  scene->setGeoImagePath(filename);
}

void
MainWindow::loadBathyLayerDialog()
{
  // Hey. Let's actually use the dialog

  QSettings app_settings;

  QScopedPointer<dslmap::BathyImportDialog> dialog(
    new dslmap::BathyImportDialog(this));

  if (dialog->exec() == QDialog::Rejected) {
    qCWarning(navg, "qdialog x'd out.");
    return;
  }

  auto filename = dialog->filename();

  loadBathyLayer(filename);
}

void
MainWindow::loadBathyLayer(QString filename)
{
  if (filename.isEmpty()) {
    return;
  }

  auto layer = std::make_unique<dslmap::BathyLayer>();

  if (!layer->addRasterFile(filename, 1)) {
    qCCritical(
      navg, "Failed to open raster file: %s", filename.toStdString().data());
    return;
  }

  auto scene = m_map_view->mapScene();
  auto scene_gradient = scene->gradient();
  if (scene_gradient) {
    layer->setGradient(scene_gradient);
  } else {
    auto grad = layer->gradient();
    grad->setGradient(dslmap::GdalColorGradient::GradientPreset::HAXBY);
    scene->setGradient(grad);
  }

  const auto uuid = m_map_view->mapScene()->addLayer(layer.get());

  if (uuid.isNull()) {
    qCCritical(navg, "Failed to add raster layer to map scene.");
    return;
  }

  connect(layer.get(),
          &dslmap::BathyLayer::saveBathyUnderlay,
          this,
          &MainWindow::setSceneBathy);

  layer.release();
}

void
MainWindow::loadTargetLayer(QString filename)
{
    // Add the layer to the MapScene.
  if (filename.isEmpty()) {
    qDebug(navg) << "Target Layer filename is empty. Aborting loading target underlay";
    return;
  }
  //return;
  auto scene = qobject_cast<dslmap::MapScene*>(m_map_view->mapScene());
  if (!scene) {
   
    return;
  }

  auto widget = std::make_unique<dslmap::TargetImport>(*scene,3,2,filename,"SavedTargetLayer", this);

  // Execute the the dialog.  If successful the widget will add the layer to the
  // scene
  /*
  if (widget->exec() != QDialog::Accepted) {
      return;
  }
  */
}

void
MainWindow::setSceneBathy(QString filename)
{
  auto scene = qobject_cast<NavGMapScene*>(m_map_view->mapScene());
  if (!scene) {
    return;
  }

  scene->setBathyPath(filename);
}

void
MainWindow::setSceneTarget(QString filename)
{
  qDebug() << "Calling set target ini with filename of :" << filename;
  auto scene = qobject_cast<NavGMapScene*>(m_map_view->mapScene());
  if (!scene) {
    return;
  }
  if (!filename.isEmpty()){
    scene->setTargetPath(filename);
  }
  else {
    qDebug()<<"FILENAME IS EMPTY FOR THAT LAYER eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee";
  }
}

void
MainWindow::loadVectorLayerDialog()
{
  QMessageBox::information(
    this, "Not Implemented", "Loading vector layers is not implemented yet.");
}
void
MainWindow::deleteImageSettings()
{
  if (!m_settings) {
    return;
  }
  m_settings->beginGroup("window");
  m_settings->remove("imagelayer/image");
  m_settings->remove("imagelayer/name");
  m_settings->remove("imagelayer/rect");
  m_settings->endGroup();

  image_path = "";
  image_layer_name = "";
  image_rect = QRectF();
}

void
MainWindow::loadImageLayerDialog()
{
  QSettings app_settings;

  QScopedPointer<dslmap::ImageImportDialog> widget(
    new dslmap::ImageImportDialog(this));

  connect(widget.data(),
          &dslmap::ImageImportDialog::clearSavedImage,
          this,
          &MainWindow::deleteImageSettings);

  if (widget->exec() == QDialog::Rejected) {
    return;
  }

  auto filename = widget->getFilename();
  if (filename.isEmpty()) {
    return;
  }

  auto rect = widget->getRect();
  if (rect.isNull()) {
    qDebug(navg) << "rect is null";
    return;
  }
  auto layername = widget->getLayerName();
  if (layername.isEmpty()) {
    return;
  }

  loadImageLayer(filename, rect, layername);
}

void
MainWindow::loadImageLayer(QString filename, QRectF rect, QString layername)
{

  if (filename.isEmpty()) {
    return;
  }
  if (rect.isNull()) {
    return;
  }
  if (layername.isEmpty()) {
    layername = QFileInfo(filename).baseName();
  }
  auto top_left = rect.topLeft();
  auto bottom_right = rect.bottomRight();

  auto layer = std::make_unique<dslmap::GeoreferenceImageLayer>();

  auto currProj = m_map_view->mapScene()->projection();
  if (!currProj) {
    if (!layer->addRasterFile(filename, top_left, bottom_right, 1)) {
      qCWarning(
        navg, "Failed to open raster file: %s", filename.toStdString().data());
      return;
    }
  } else {
    // can add alternate logic here if we do have current projection
    if (!layer->addRasterFile(filename, top_left, bottom_right, 1)) {
      qCWarning(
        navg, "Failed to open raster file: %s", filename.toStdString().data());
      return;
    }
  }

  if (!layername.isEmpty()) {
    layer->setName(layername);
  }

  const auto uuid = m_map_view->mapScene()->addLayer(layer.get());

  if (uuid.isNull()) {
    qCCritical(navg, "Failed to add raster layer to map scene.");
    return;
  }

  connect(layer.get(),
          &dslmap::GeoreferenceImageLayer::saveImageUnderlay,
          this,
          &MainWindow::setSceneImage);

  layer.release();
}

void
MainWindow::setSceneImage(QString filename,
                          QPointF topleft,
                          QPointF bottomright)
{
  auto scene = qobject_cast<NavGMapScene*>(m_map_view->mapScene());
  if (!scene) {
    return;
  }

  scene->setImagePath(filename, QRectF(topleft, bottomright));
}

void
MainWindow::loadTargetLayerDialog()
{

  // Add the layer to the MapScene.
  /*
  auto scene = qobject_cast<dslmap::MapScene*>(m_map_view->mapScene());
  if (!scene) {
    QMessageBox::critical(
      this,
      tr("No map scene available."),
      tr("Cannot import targets when no map scene is available."));
    return;
  }

  auto widget = std::make_unique<dslmap::TargetImport>(*scene, this);

  // Execute the the dialog.  If successful the widget will add the layer to the
  // scene
  if (widget->exec() != QDialog::Accepted) {
      return;
  }
  */
 if (!m_map_view) return;
 m_map_view->showTargetImportDialog();
  
}


void
MainWindow::takeScreenshot()
{
  QString file_name =
    QDateTime::currentDateTimeUtc().toString("yyyyMMdd.hhmmss");
  Screenshot shot;
  shot.shootScreen();
  shot.saveScreenshot(file_name, screenshot_directory);
}

bool
MainWindow::mapOnlyModeEnabled() const noexcept
{
  return m_map_only_mode_enabled;
}

void
MainWindow::setMapOnlyModeEnabled(bool enabled)
{
  if (enabled == m_map_only_mode_enabled) {
    return;
  }

  m_map_only_mode_enabled = enabled;
  emit mapOnlyModeToggled(enabled);
}
} // namespace navg
