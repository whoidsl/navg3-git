/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "AlvinXYOriginDialog.h"
#include "ui_AlvinXYOriginDialog.h"

namespace navg {

AlvinXYOriginDialog::AlvinXYOriginDialog(QWidget* parent)
  : QDialog(parent)
  , ui(new Ui::AlvinXYOriginDialog)
{
  ui->setupUi(this);
}

AlvinXYOriginDialog::~AlvinXYOriginDialog() = default;

void
AlvinXYOriginDialog::setOrigin(QPointF origin) noexcept
{
  ui->latitude->setValue(origin.y());
  ui->logitude->setValue(origin.x());
}

void
AlvinXYOriginDialog::setStart(QPointF coords) noexcept
{
  ui->start_latitude->setValue(coords.y());
  ui->start_longitude->setValue(coords.x());
}

QPointF
AlvinXYOriginDialog::origin() const noexcept
{
  auto lat = ui->latitude->value();
  if (ui->latitudeDirection->currentIndex() != 0) {
    lat = -lat;
  }

  auto lon = ui->logitude->value();
  if (ui->longitudeDirection->currentIndex() != 0) {
    lon = -lon;
  }

  return { lon, lat };
}

QPointF
AlvinXYOriginDialog::startCoords() const noexcept
{
  auto lat = ui->start_latitude->value();
  if (ui->startLatitudeDirection->currentIndex() != 0) {
    lat = -lat;
  }

  auto lon = ui->start_longitude->value();
  if (ui->startLongitudeDirection->currentIndex() != 0) {
    lon = -lon;
  }

  return { lon, lat };
}

void
AlvinXYOriginDialog::setDiveNum(int dive_num)
{
  ui->dive_number_spin->setValue(dive_num);
}
void
AlvinXYOriginDialog::setVehicleName(QString vehicle_name)
{
  ui->vehicle_name->setText(vehicle_name);
}

int
AlvinXYOriginDialog::diveNum() const noexcept
{
  return ui->dive_number_spin->value();
}
QString
AlvinXYOriginDialog::vehicleName() const
{
  return ui->vehicle_name->text();
}
} // NAMESPACE