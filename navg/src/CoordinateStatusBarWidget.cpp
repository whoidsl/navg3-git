/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "CoordinateStatusBarWidget.h"

#include <QAction>
#include <QHBoxLayout>
#include <QLabel>
#include <QPointF>

#include <dslmap/Util.h>

namespace navg {

CoordinateStatusBarWidget::CoordinateStatusBarWidget(QWidget* parent)
  : QWidget(parent)
  , m_name_label(new QLabel(this))
  , m_value_label(new QLabel(this))
{

  auto layout = new QHBoxLayout;

  layout->addWidget(m_name_label);
  layout->addWidget(m_value_label);

  setLayout(layout);
}

CoordinateStatusBarWidget::~CoordinateStatusBarWidget() = default;

void
CoordinateStatusBarWidget::setGraticuleProjection(
  NavGMapView::GraticuleType type) noexcept
{
  switch (type) {
    case NavGMapView::GraticuleType::UTM:
      m_name_label->setText(QStringLiteral("Grid (UTM):"));
      m_coordinate_type = CoordinateType::Meters;
      break;
    case NavGMapView::GraticuleType::LatLon:
      m_name_label->setText(QStringLiteral("Grid (WGS84):"));
      m_coordinate_type = CoordinateType::LatLon;
      break;
    case NavGMapView::GraticuleType::AlvinXY:
      m_name_label->setText(QStringLiteral("Grid (AlvinXY):"));
      m_coordinate_type = CoordinateType::Meters;
      break;
  }
}

void
CoordinateStatusBarWidget::setSceneProjection(
  NavGMapScene::ProjectionType type) noexcept
{
  switch (type) {
    case NavGMapScene::ProjectionType::UTM:
      m_name_label->setText(QStringLiteral("Map (UTM):"));
      m_coordinate_type = CoordinateType::Meters;
      break;
    case NavGMapScene::ProjectionType::Mercator:
      m_name_label->setText(QStringLiteral("Map (Merc):"));
      m_coordinate_type = CoordinateType::Meters;
      break;
    case NavGMapScene::ProjectionType::AlvinXY:
      m_name_label->setText(QStringLiteral("Map (AlvinXY):"));
      m_coordinate_type = CoordinateType::Meters;
      break;
  }
}

void
CoordinateStatusBarWidget::setCoordinateValue(QPointF value) noexcept
{
  if (m_coordinate_type == CoordinateType::LatLon) {
    m_value_label->setText(
      dslmap::formatLonLat(value.x(), value.y(), m_latlon_format, 6));
  } else {
    m_value_label->setText(QString("%1m x, %2m y")
                             .arg(value.x(), 6, 'f', 2)
                             .arg(value.y(), 6, 'f', 2));
  }
}

void
CoordinateStatusBarWidget::setLatLonFormat(dslmap::LatLonFormat format) noexcept
{
  m_latlon_format = format;
}

dslmap::LatLonFormat
CoordinateStatusBarWidget::latLonFormat() const noexcept
{
  return m_latlon_format;
}
} // NAMESPACE