/**
 * Copyright 2022 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "IniStatusBarWidget.h"

#include <QFileInfo>
#include <QHBoxLayout>
#include <QLabel>
#include <QPointF>
#include <QToolButton>

namespace navg {

IniStatusBarWidget::IniStatusBarWidget(QWidget* parent)
  : QWidget(parent)
  , path_button(new QToolButton(this))
  , filename_label(new QLabel(this))
{
  auto layout = new QHBoxLayout;

  path_button->setCheckable(true);
  path_button->setText("...");

  path_button->setStyleSheet(
    "font-weight: bold; font-style: italic; text-align:left");

  connect(path_button,
          &QToolButton::toggled,
          this,
          &IniStatusBarWidget::showFullPath);

  filename_label->setStyleSheet(
    "font-weight: bold; text-align:left");
 
  layout->addWidget(path_button);
  layout->addWidget(filename_label);
  layout->addStretch(1);

  setLayout(layout);
}

IniStatusBarWidget::~IniStatusBarWidget() = default;

void
IniStatusBarWidget::setFileName(const QString& name) noexcept
{
  filename_label->setText(name);
}
void
IniStatusBarWidget::setDirPath(const QString& path) noexcept
{
  stored_path = path;
}

void
IniStatusBarWidget::showFullPath(bool show)
{
  if (show) {
    path_button->setText(stored_path);
  } else {
    path_button->setText("...");
  }
}

void
IniStatusBarWidget::setConfigFile(const QString& full_path) noexcept
{

  QFileInfo fi(full_path);
  QString filename = fi.fileName();
  QString dir_path = fi.absolutePath() + "/";

  setFileName(filename);
  setDirPath(dir_path);
  if (path_button->isChecked()){
    showFullPath(true);
  }
}

} // navg
