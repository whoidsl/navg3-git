/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 9/17/18.
//

#include "AboutPluginsDialog.h"

#include "ui_AboutPluginsDialog.h"

#include <QDebug>
#include <QTableWidget>

namespace navg {
AboutPluginsDialog::AboutPluginsDialog(QWidget* parent)
  : QDialog(parent)
  , ui(new Ui::AboutPluginsDialog)
{
  ui->setupUi(this);
  ui->pluginList->setColumnCount(2);
  ui->pluginList->setHorizontalHeaderLabels({ "Name", "Description" });
}

AboutPluginsDialog::~AboutPluginsDialog()
{
  delete ui;
}

void
AboutPluginsDialog::setPlugins(const QMap<QString, QPluginLoader*>& plugins)
{
  ui->pluginList->setRowCount(plugins.size());

  auto i = 0;
  for (const auto& name : plugins.keys()) {
    auto item = new QTableWidgetItem(name);
    auto loader = plugins.value(name);
    item->setFlags(item->flags() | Qt::ItemIsUserCheckable);
    item->setCheckState(loader->isLoaded() ? Qt::Checked : Qt::Unchecked);

    ui->pluginList->setItem(i, 0, item);
    const auto description = loader->metaData()
                               .value("MetaData")
                               .toObject()
                               .value("description")
                               .toString();
    item = new QTableWidgetItem(description);
    item->setToolTip(description);
    ui->pluginList->setItem(i, 1, item);
    ui->pluginList->setWordWrap(false);
    ++i;
  }
}

QMap<QString, bool>
AboutPluginsDialog::desiredPlugins() const
{
  auto results = QMap<QString, bool>{};
  for (auto i = 0; i < ui->pluginList->rowCount(); ++i) {
    const auto item = ui->pluginList->item(i, 0);
    results[item->text()] = item->checkState() == Qt::Checked;
  }

  return results;
}
}
