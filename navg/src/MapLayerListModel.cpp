/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "MapLayerListModel.h"
#include <QGraphicsView>
#include <dslmap/MapItem.h>
#include <dslmap/MapView.h>

namespace navg {

// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
// MAPLAYERDATA

MapLayerData::MapLayerData() // : QObject()
{
  name = "";
  id = nullptr;
  Z = 0;
  isValid = false;
  isVisible = false;
}

MapLayerData::MapLayerData(dslmap::MapLayer* layer_ptr) //  : QObject()
{
  isValid = reload(layer_ptr);
}

MapLayerData::MapLayerData(QString _name, QUuid _id, qreal _Z, bool _isValid,
                           bool _isVisible)
  : name(_name)
  , id(_id)
  , Z(_Z)
  , isValid(_isValid)
  , isVisible(_isVisible)
{
}

MapLayerData::~MapLayerData()
{
}

void
MapLayerData::print()
{
  qInfo() << name << " with z val: " << Z;
}

bool
MapLayerData::reload(dslmap::MapLayer* layer_ptr)
{
  name = layer_ptr->name();
  id = layer_ptr->id();
  auto item = layer_ptr->toGraphicsObject();
  Z = item->zValue();
  isVisible = item->isVisible();
  if (name != "" && id != nullptr)
    return true;
  return false;
}

QVariant
MapLayerData::dataDisplay() const
{
  return QVariant(name);
}

QVariant
MapLayerData::dataEdit() const
{
  return QVariant(name);
}

QVariant
MapLayerData::dataForeground() const
{
  if (!isVisible) {
    return QVariant(QColor(Qt::darkGray));
  }
  return QVariant{};
}

// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
// MAPLAYERLISTMODEL

MapLayerListModel::MapLayerListModel(dslmap::MapScene* scene, QObject* parent)
  : QAbstractListModel(parent)
  , m_zbysort(false)
  , m_scene(scene)
{
  auto layer_hash = m_scene->layers();
  for (auto i = layer_hash.begin(); i != layer_hash.end(); ++i) {
    scene_layerAdded_uuid(i.key());
  }

  connect(m_scene, static_cast<void (dslmap::MapScene::*)(const QUuid&)>(
                     &dslmap::MapScene::layerAdded),
          this, &navg::MapLayerListModel::scene_layerAdded_uuid);
  connect(m_scene, static_cast<void (dslmap::MapScene::*)(const QUuid&)>(
                     &dslmap::MapScene::layerRemoved),
          this, &navg::MapLayerListModel::scene_layerRemoved_uuid);
}

MapLayerListModel::~MapLayerListModel()
{
}

int
MapLayerListModel::rowCount(const QModelIndex& parent) const
{
  Q_UNUSED(parent)
  return m_layers.size();
}

int
MapLayerListModel::rows()
{
  return m_layers.size();
}

QVariant
MapLayerListModel::data(const int row, int role) const
{
  if (row < 0 || row >= m_layers.size() || m_layers.isEmpty()) {
    return QVariant();
  }
  if (role == Qt::DisplayRole) {
    return m_layers[row].dataDisplay();
  } else if (role == Qt::EditRole) {
    return m_layers[row].dataEdit();
  } else if (role == Qt::ForegroundRole) {
    return m_layers[row].dataForeground();
  } else {
    return QVariant();
  }
}

QVariant
MapLayerListModel::data(const QModelIndex& index, int role) const
{
  return data(index.row(), role);
}

Qt::DropActions
MapLayerListModel::supportedDropActions() const
{
  return Qt::CopyAction | Qt::MoveAction;
}

bool
MapLayerListModel::setData(const QModelIndex& index, const QVariant& value,
                           int role)
{
  Q_UNUSED(role)
  if (!index.isValid()) {
    return false;
  }
  m_scene->layers()[m_layers[index.row()].id]->setName(value.toString());
  m_layers[index.row()].name =
    m_scene->layers()[m_layers[index.row()].id]->name();
  dataChanged(index, index);
  return m_layers[index.row()].name == value.toString();
}

Qt::ItemFlags
MapLayerListModel::flags(const QModelIndex& index) const
{
  if (index.isValid() && index.row() < m_layers.size()) {
    return Qt::ItemFlag::ItemIsEditable | Qt::ItemFlag::ItemIsSelectable |
           Qt::ItemFlag::ItemIsDragEnabled | Qt::ItemFlag::ItemIsEnabled;
  }
  return Qt::ItemFlag::ItemIsEditable | Qt::ItemFlag::ItemIsSelectable |
         Qt::ItemFlag::ItemIsDragEnabled | Qt::ItemFlag::ItemIsEnabled;
}

void
MapLayerListModel::zoomTo(int row)
{
  if (row >= rows() || row < 0) {
    qDebug() << "Row " << row << " is invalid";
    return;
  }

  QRectF rect = m_scene->layer(m_layers[row].id)->boundingRect();

  for (auto view : m_scene->views()) {
    auto mapview = qobject_cast<dslmap::MapView*>(view);
    if (mapview != nullptr) {
      mapview->zoomToRect(rect);
    }
  }
}

void
MapLayerListModel::zoomTo(QModelIndex index)
{
  zoomTo(index.row());
}

void
MapLayerListModel::orderSwap(int lower, int higher)
{
  if (lower < 0 || higher >= rows()) {
    return;
  }
  m_layers.swap(lower, higher);
  zBySort();
}

void
MapLayerListModel::orderSwap(QModelIndex index1, QModelIndex index2)
{
  if (index1.isValid() && index2.isValid()) {
    orderSwap(index1.row(), index2.row());
  }
}

void
MapLayerListModel::top(int row)
{
  zBySort();
  if (row == 0)
    return;
  m_scene->layer(m_layers[row].id)->toGraphicsObject()->setZValue(2 * rows());
}

void
MapLayerListModel::bottom(int row)
{
  zBySort();
  if (row == rows() - 1)
    return;
  m_scene->layer(m_layers[row].id)->toGraphicsObject()->setZValue(0);
}

bool
MapLayerListModel::removeRows(int row, int count, const QModelIndex& parent)
{
  Q_UNUSED(count)
  Q_UNUSED(parent)
  m_scene->removeLayer(m_layers[row].id);
  return true;
}

void
MapLayerListModel::sortByZ()
{
  if (m_zbysort)
    return;
  qSort(m_layers.begin(), m_layers.end());
  emit dataChanged(index(0), index(rows()));
}

void
MapLayerListModel::zBySort()
{
  m_zbysort = true;
  for (int i = 0; i < rows(); i++)
    m_scene->layer(m_layers[i].id)
      ->toGraphicsObject()
      ->setZValue(2 * (rows() - i) - 1);
  m_zbysort = false;
}

QMenu*
MapLayerListModel::getMenu(int row)
{
  if (row >= 0 && row < rows()) {
    return m_scene->layer(m_layers[row].id)->contextMenu();
  }
  return nullptr;
}

QMenu*
MapLayerListModel::getMenu(QModelIndex index)
{
  if (index.row() >= 0 && index.row() < rows()) {
    return m_scene->layer(m_layers[index.row()].id)->contextMenu();
  }
  return nullptr;
}

void
MapLayerListModel::showProperties(QModelIndex index)
{
  if (index.row() >= 0 && index.row() < rows()) {
    m_scene->layer(m_layers[index.row()].id)->showPropertiesDialog();
    //    reload();
  }
}

void
MapLayerListModel::changeVisible(int row)
{
  if (row >= 0 && row < rows()) {
    if (m_layers[row].isVisible) {
      m_scene->layer(m_layers[row].id)->toGraphicsObject()->hide();
    } else {
      m_scene->layer(m_layers[row].id)->toGraphicsObject()->show();
    }
    //  reload();
  }
}

void
MapLayerListModel::zChanged()
{
  auto layer = qobject_cast<dslmap::MapLayer*>(QObject::sender());
  if (layer == nullptr) {
    return;
  }
  auto uuid = layer->id();
  for (int i = 0; i < rows(); i++) {
    if (uuid == m_layers[i].id) {
      m_layers[i].Z = layer->toGraphicsObject()->zValue();
      dataChanged(index(i), index(i));
    }
  }
  sortByZ();
}

void
MapLayerListModel::nameChanged(QString new_name)
{
  auto layer = qobject_cast<dslmap::MapLayer*>(QObject::sender());
  if (layer == nullptr) {
    return;
  }
  auto uuid = layer->id();
  for (int i = 0; i < rows(); i++) {
    if (uuid == m_layers[i].id) {
      m_layers[i].name = new_name;
      dataChanged(index(i), index(i));
      return;
    }
  }
}

void
MapLayerListModel::visibleChanged()
{
  auto layer = qobject_cast<dslmap::MapLayer*>(QObject::sender());
  if (layer == nullptr) {
    return;
  }
  auto uuid = layer->id();
  for (int i = 0; i < rows(); i++) {
    if (uuid == m_layers[i].id) {
      m_layers[i].isVisible = layer->toGraphicsObject()->isVisible();
      dataChanged(index(i), index(i));
      return;
    }
  }
}

void
MapLayerListModel::scene_layerAdded_layer(dslmap::MapLayer* layer)
{
  beginInsertRows(index(rows()), 0, 0);
  MapLayerData new_layer(layer);
  m_layers.append(new_layer);

  auto item = dynamic_cast<QGraphicsObject*>(layer);
  connect(item, &QGraphicsObject::zChanged, this, &MapLayerListModel::zChanged);
  connect(item, &QGraphicsObject::objectNameChanged, this,
          &MapLayerListModel::nameChanged);
  connect(item, &QGraphicsObject::visibleChanged, this,
          &MapLayerListModel::visibleChanged);

  if (layer->type() == dslmap::MapItemType::ContourLayerType ||
      layer->type() == dslmap::MapItemType::BathymetryLayerType)
    bottom(rows() - 1);
  else if (new_layer.Z == 0)
    top(rows() - 1);
  else
    sortByZ();

  endInsertRows();
}

void
MapLayerListModel::scene_layerAdded_uuid(const QUuid& uuid)
{
  scene_layerAdded_layer(m_scene->layer(uuid));
}

void
MapLayerListModel::scene_layerRemoved_layer(dslmap::MapLayer* layer)
{
  beginRemoveRows(index(rows()), 0, 0);
  for (int i = rows() - 1; i >= 0; --i) {
    if (layer->id() == m_layers[i].id) {
      m_layers.removeAt(i);
    }
  }
  endRemoveRows();
}

void
MapLayerListModel::scene_layerRemoved_uuid(const QUuid& uuid)
{
  beginRemoveRows(index(rows()), 0, 0);
  for (int i = rows() - 1; i >= 0; --i) {
    if (uuid == m_layers[i].id) {
      m_layers.removeAt(i);
    }
  }
  endRemoveRows();
}
}
