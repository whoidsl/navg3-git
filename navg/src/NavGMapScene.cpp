/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 12/18/18.
//

#include "NavGMapScene.h"

#include <dslmap/Proj.h>

namespace navg {

Q_LOGGING_CATEGORY(navgmapscene, "navg.mapscene")

NavGMapScene::NavGMapScene(QObject* parent)
  : dslmap::MapScene(parent)
  , m_alvinxy_origin(qQNaN(), qQNaN())
{
}

NavGMapScene::~NavGMapScene() = default;

void
NavGMapScene::loadSettings(QSettings* settings)
{
  auto value = settings->value("mapscene/alvin_origin");
  auto string = value.toString();
  QStringList elements = string.split(',');
  if (elements.size() != 2)
    return;

  auto ok = false;
  auto lat = elements[0].toDouble(&ok);
  if (!ok) {
    return;
  }
  auto lon = elements[1].toDouble(&ok);
  if (!ok) {
    return;
  }
  m_alvinxy_origin = QPointF(lat, lon);
  //
  value = settings->value("mapscene/start_xy");
  string = value.toString();
  elements = string.split(',');
  if (elements.size() != 2)
    return;

  ok = false;
  lat = elements[0].toDouble(&ok);
  if (!ok) {
    return;
  }
  lon = elements[1].toDouble(&ok);
  if (!ok) {
    return;
  }
  m_startxy = QPointF(lat, lon);
  //

  value = settings->value("mapscene/dive_number");

  ok = false;
  int num = value.toInt(&ok);
  if (!ok) {
    return;
  }

  dive_number = num;
  //


  image_preload_path = settings->value("mapscene/imagelayer/path").toString();
  bath_preload_path = settings->value("mapscene/bathylayer/path").toString();
  contour_preload_path = settings->value("mapscene/contourlayer/path").toString();
  contour_preload_spacing = settings->value("mapscene/contourlayer/spacing").toInt();
  geoimage_preload_path = settings->value("mapscene/geoimagelayer/path").toString();
  target_preload_path = settings->value("mapscene/targetlayer/path").toString();

  image_rect_string = settings->value("mapscene/imagelayer/rect").toString();
  vehicle_name = settings->value("mapscene/vehicle_name").toString();

  QStringList rect_elements = image_rect_string.split(',');

  if (rect_elements.size() >= 4) {
    image_preload_rect =
      QRectF(QPointF(rect_elements[0].toDouble(), rect_elements[1].toDouble()),
             QPointF(rect_elements[2].toDouble(), rect_elements[3].toDouble()));
  }

  if (!image_preload_path.isEmpty()){
    emit imagePreloadReady(image_preload_path, image_preload_rect);
  }

  if (!bath_preload_path.isEmpty()){
    emit bathyPreloadReady(bath_preload_path);
  }

  if (!contour_preload_path.isEmpty()){
    emit contourPreloadReady(contour_preload_path, contour_preload_spacing);
  }

  if (!geoimage_preload_path.isEmpty()){
    emit geoimagePreloadReady(geoimage_preload_path);
  }
  if (!target_preload_path.isEmpty()){
    emit targetPreloadReady(target_preload_path);
  }

}

void
NavGMapScene::saveSettings(QSettings& settings)
{
  settings.beginGroup("mapscene");
  settings.setValue("alvin_origin",
                    QString::number(m_alvinxy_origin.x(), 'f', 8) + "," +
                      QString::number(m_alvinxy_origin.y(), 'f', 8));
  settings.setValue("start_xy",
                    QString::number(m_startxy.x(), 'f', 8) + "," +
                      QString::number(m_startxy.y(), 'f', 8));

  settings.setValue("bathylayer/path", bath_preload_path);
  settings.setValue("imagelayer/path", image_preload_path);
  settings.setValue("imagelayer/rect", QString(QString::number(image_preload_rect.topLeft().x() ,'f', 7) + "," 
  + QString::number(image_preload_rect.topLeft().y(), 'f', 7) + "," + QString::number(image_preload_rect.bottomRight().x(), 'f', 7) + "," 
  + QString::number(image_preload_rect.bottomRight().y(), 'f', 7)));
  settings.setValue("contourlayer/path", contour_preload_path);
  settings.setValue("contourlayer/spacing", contour_preload_spacing);
  settings.setValue("geoimagelayer/path", geoimage_preload_path);
  settings.setValue("targetlayer/path", target_preload_path);
  settings.setValue("dive_number", dive_number);
  settings.setValue("vehicle_name", vehicle_name);



  settings.endGroup();
}

QPointF
NavGMapScene::alvinXYOrigin() const noexcept
{
  return m_alvinxy_origin;
}

QPointF
NavGMapScene::XYStart() const noexcept
{
  return m_startxy;
}

void
NavGMapScene::clearAlvinXYOrigin() noexcept
{
  if (m_alvinxy_origin == QPointF{ qQNaN(), qQNaN() }) {
    return;
  }

  m_alvinxy_origin = QPointF{ qQNaN(), qQNaN() };
  emit alvinXYOriginChanged(m_alvinxy_origin);
}

void
NavGMapScene::setAlvinXYOrigin(QPointF origin) noexcept
{
  if (m_alvinxy_origin == origin) {
    return;
  }
  if (hasAlvinXYOrigin()) {
    clearAlvinXYOrigin();
  }

  m_alvinxy_origin = origin;
  emit alvinXYOriginChanged(m_alvinxy_origin);
}

void 
NavGMapScene::setStartXY(QPointF coords) noexcept
{
  m_startxy = coords;
}

bool
NavGMapScene::hasAlvinXYOrigin() const noexcept
{
  return isValidAlvinXYOrigin(m_alvinxy_origin);
}

bool
NavGMapScene::hasStartCoords() const noexcept
{
  return !(m_startxy.isNull());
}

bool
NavGMapScene::isValidAlvinXYOrigin(const QPointF& origin) noexcept
{
  return !(qIsNaN(origin.x()) || qIsNaN(origin.y()));
}

NavGMapScene::ProjectionType
NavGMapScene::projectionType() const noexcept
{
  return m_projection_type;
}

bool
NavGMapScene::setProjectionType(NavGMapScene::ProjectionType type) noexcept
{
  if (projectionPtr() && projectionType() == type) {
    return true;
  }

  if (type == NavGMapScene::ProjectionType::Mercator) {
    const auto rect = itemsBoundingRect();
    if (rect.isEmpty()) {
      qCWarning(navgmapscene) << "NavGMapScene contains no items, cannot "
                                 "calculate center for local mercator "
                                 "projection.";
      return false;
    }

    const auto proj = [&]() {
      auto x = rect.center().x();
      auto y = rect.center().y();

      const auto scene_proj = projectionPtr();
      if (!scene_proj->isLatLon()) {
        scene_proj->transformToLonLat(1, &x, &y, true);
      }

      return dslmap::Proj::alvinXYFromOrigin(x, y);
    }();

    qCDebug(navgmapscene) << "Changing map projection to:"
                          << proj->definition();
    setProjection(proj);
  } else if (type == NavGMapScene::ProjectionType::UTM) {

    const auto rect = itemsBoundingRect();
    if (rect.isEmpty()) {
      qCWarning(navgmapscene)
        << "NavGMapScene contains no items, cannot calculate UTM zone.";
      return false;
    }

    const auto proj = [&]() {
      auto x = rect.center().x();
      auto y = rect.center().y();

      const auto scene_proj = projectionPtr();
      if (!scene_proj->isLatLon()) {
        scene_proj->transformToLonLat(1, &x, &y, true);
      }

      return dslmap::Proj::utmFromLonLat(x, y);
    }();

    qCDebug(navgmapscene) << "Changing map projection to:"
                          << proj->definition();
    setProjection(proj);
  } else if (type == NavGMapScene::ProjectionType::AlvinXY) {
    if (!hasAlvinXYOrigin()) {
      qCWarning(navgmapscene) << "No AlvinXY origin specified for scene.";
      return false;
    }

    const auto origin = alvinXYOrigin();
    const auto proj = dslmap::Proj::alvinXYFromOrigin(origin.x(), origin.y());
    qCDebug(navgmapscene) << "Changing map projection to:"
                          << proj->definition();
    setProjection(proj);
  }
  m_projection_type = type;
  emit projectionTypeChanged(type);
  return true;
}

void NavGMapScene::setContourPath(QString path, int spacing){
  contour_preload_path = path;
  contour_preload_spacing = spacing;
}
void NavGMapScene::setBathyPath(QString path){
  bath_preload_path = path;
}
void
NavGMapScene::setImagePath(QString path, QRectF image_rect)
{
  image_preload_rect = image_rect;
  image_preload_path = path;
  image_rect_string = QString(QString::number(image_preload_rect.topLeft().x() ,'f', 7) + "," 
  + QString::number(image_preload_rect.topLeft().y(), 'f', 7) + "," + QString::number(image_preload_rect.bottomRight().x(), 'f', 7) + "," 
  + QString::number(image_preload_rect.bottomRight().y(), 'f', 7));
}
void NavGMapScene::setGeoImagePath(QString path){
  geoimage_preload_path = path;
}

void NavGMapScene::setTargetPath(QString path){
  target_preload_path = path;
}

void
NavGMapScene::clearUnderlayPreloads(){

  // Image
  image_preload_path = "";
  image_preload_rect = QRectF();

  // GeoImage
  geoimage_preload_path="";

  // Bathy
  bath_preload_path ="";

  // Contour
  contour_preload_path ="";
  contour_preload_spacing=0;

  // Target
  target_preload_path = "";
}

void
NavGMapScene::clearTargetPreloads(){
  // Target
  target_preload_path = "";
}

QString
NavGMapScene::bathyPath()
{
  return bath_preload_path;
}

QString
NavGMapScene::contourPath()
{
  return contour_preload_path;
}

QString
NavGMapScene::imagePath()
{
  return image_preload_path;
}

QString
NavGMapScene::geoimagePath()
{
  return geoimage_preload_path;
}

QString
NavGMapScene::targetPath()
{
  return target_preload_path;
}

int
NavGMapScene::contourSpacing()
{
  return contour_preload_spacing;
}

QString
NavGMapScene::imageCoords()
{
  return image_rect_string;
}

void
NavGMapScene::setDiveNumber(int num)
{
  dive_number = num;
}
void
NavGMapScene::setVehicleName(QString name)
{
  vehicle_name = name;
}

int
NavGMapScene::diveNum()
{
  return dive_number;
}

QString
NavGMapScene::vehicleName()
{
  return vehicle_name;
}
}
