/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "MainWindow.h"
#include "NavG.h"
#include "NavGMapView.h"

#include <dslmap/BathyLayer.h>
#include <dslmap/BeaconLayer.h>
#include <dslmap/ContourLayer.h>
#include <dslmap/GdalColorGradient.h>
#include <dslmap/MapScene.h>
#include <dslmap/MapView.h>
#include <dslmap/Proj.h>
#include <dslmap/ShapeMarkerSymbol.h>
#include <dslmap/SymbolLayer.h>
#include <dslmap/WktMarkerSymbol.h>

// #include "dslmaptest/DslMapTest.h"

#include <QApplication>
#include <QSettings>
#include <QtPlugin>

#include <QDir>
#include <QFile>
#include <QStyleFactory>

int
main(int argc, char* argv[])
{

  dslmap::init();
  // dslmaptest::init();

  qSetMessagePattern("%{type} %{category} %{file}:%{line} %{message}");
  QCoreApplication::setApplicationName("NavG");
  QCoreApplication::setApplicationVersion(NAVG_VERSION);
  QCoreApplication::setOrganizationName("WHOI");
  QCoreApplication::setOrganizationDomain(".edu");
  QCoreApplication::setAttribute(Qt::AA_DontUseNativeMenuBar);

  QSettings::setDefaultFormat(QSettings::IniFormat);

  auto exit_code = 0;
  //do {
    QApplication app(argc, argv);

    //arg parsing
    QCommandLineParser parser;
    parser.addHelpOption();
    parser.addVersionOption();
    
    // An option with no further values (-i, --inhibit)
    QCommandLineOption inhibitOption(QStringList() << "i" << "inhibit",
            QCoreApplication::translate("main", "Inhibit ability of importing inis or Save As. (Advanced Usage)"));
    parser.addOption(inhibitOption);

    // An option to load an ini file (-s, --settings_file)
    QCommandLineOption settingsOption(QStringList() << "s" << "settings_file",
            QCoreApplication::translate("main", "Load in settings file with <filepath> argument. (Advanced Usage)"),
            QCoreApplication::translate("main", "filepath"));
    parser.addOption(settingsOption);

    parser.process(app);

    QFile File(":/navg.qss");
    File.open(QFile::ReadOnly);
    QString StyleSheet = QLatin1String(File.readAll());

    navg::MainWindow main_window(true,0,parser.value(settingsOption));
    main_window.setStyleSheet(StyleSheet);

    if (parser.isSet(inhibitOption)) {
      main_window.inhibitIniSetting(true);
    }

#if 0
    const auto capecod = QDir::temp().filePath("cape_cod.grd");
    QFile::copy(":/test/cape_cod.grd", capecod);

    auto bathy_layer = new dslmap::BathyLayer;
    bathy_layer->setName("Bathy");
    bathy_layer->addRasterFile(capecod);
    auto view = qobject_cast<navg::NavGMapView*>(main_window.centralWidget());
    auto scene = view->mapScene();
    qDebug() << "Adding bathy_layer: " << bathy_layer->name();
    scene->addLayer(bathy_layer);

    // Create a layer for the points.
    auto symbol_layer = new dslmap::SymbolLayer;
    symbol_layer->setName("Beacon Layer");
    symbol_layer->setProjection(bathy_layer->projection());

    // Create a symbol to draw the points.  We'll make a small circle filled
    // with white.
    auto symbol = std::make_shared<dslmap::ShapeMarkerSymbol>(
      dslmap::ShapeMarkerSymbol::Shape::Circle, 100);
    symbol->setMinimumSize(15);
    symbol->setOutlineColor(Qt::darkYellow);
    symbol->setOutlineWidth(2);
    symbol->setLabelColor(Qt::red);
    symbol_layer->setSymbol(symbol);

    // Draw the markers scale invariant.
    symbol_layer->setScaleInvariant(false);

    // Add the points.
    const auto bounds = bathy_layer->boundingRect();
    for (auto i = 0; i < 10; i++) {
      auto x =
        qreal{ (static_cast<qreal>(qrand()) / RAND_MAX) * bounds.width() } +
        bounds.left();
      auto y =
        qreal{ (static_cast<qreal>(qrand()) / RAND_MAX) * bounds.height() } +
        bounds.top();
      symbol_layer->addPoint(x, y, QString("%1").arg(i), { { "id", i } });
    }

    // Set the labels visible, and the symbol_layer itself visible.
    symbol_layer->setVisible(true);

    // Add the symbol_layer to the scene
    scene->addLayer(symbol_layer);
#endif

#if 0
    const auto capecod = QDir::temp().filePath("cape_cod.grd");
    QFile::copy(":/test/cape_cod.grd", capecod);

    auto contour_layer = dslmap::ContourLayer::fromGrdFile(
      "/data/planning-bathy/new-costarics.grd", 10);
    Q_ASSERT(contour_layer);

    contour_layer->setName("contour");
    auto view = qobject_cast<navg::NavGMapView*>(main_window.centralWidget());
    auto scene = view->mapScene();
    qDebug() << "Adding contour_layer: " << contour_layer->name();
    scene->addLayer(contour_layer.release());

#endif
    // auto view =
    // qobject_cast<navg::NavGMapView*>(main_window.centralWidget());
    // view->setGraticuleProjectionType(navg::NavGMapView::GraticuleType::LatLon);
    // view->setGraticuleProjection(dslmap::Proj::fromDefinition(PROJ_WGS84_DEFINITION));
    main_window.show();

    exit_code = app.exec();
    if (exit_code == navg::MainWindow::REBOOT_CODE) {
      // restart
      QString program = qApp->arguments()[0];
      QStringList args = qApp->arguments().mid(1);
      qApp->quit();
      QProcess::startDetached(program, args);
    }
//  } while (exit_code == navg::MainWindow::REBOOT_CODE);
// That loop condition isnt useful anymore if handling the reboot code
}
