/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "GridSpacingStatusBarWidget.h"

#include <QHBoxLayout>
#include <QLabel>
#include <QPointF>

namespace navg {

GridSpacingStatusBarWidget::GridSpacingStatusBarWidget(QWidget* parent)
  : QWidget(parent)
  , w_value_label(new QLabel(this))
  , h_value_label(new QLabel(this))
{

  auto layout = new QHBoxLayout;

  layout->addWidget(new QLabel{ QStringLiteral("Grid W(m):") });
  layout->addWidget(w_value_label);
  layout->addWidget(new QLabel{ QStringLiteral("Grid H(m):")});
  layout->addWidget(h_value_label);


  setLayout(layout);
}

GridSpacingStatusBarWidget::~GridSpacingStatusBarWidget() = default;

void
GridSpacingStatusBarWidget::setSpacingValues(qreal width, qreal height) noexcept
{
  QFont f("sans", 10, QFont::Bold);
  const auto w_label = QString("%1m").arg(width, 6, 'f', 2);
  w_value_label->setText(w_label);
  w_value_label->setFont(f);
  const auto h_label = QString("%1m").arg(height, 6, 'f', 2);
  h_value_label->setText(h_label);
  h_value_label->setFont(f);
}

} // navg
