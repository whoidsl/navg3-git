# NavG Plugin Architecture

NavG is designed from the ground up to be extensible and adaptable to future needs and platforms through the use of
a plugin architecture.  Plugins allow for encapsulation of domain-specific functionality and promote a
"use only what you need" mentality.  Plugins can be developed in isolation, outside of the NavG source tree, allowing
greater flexibility for deployments and testing without interfering with existing behaviors.

## What Should Be a Plugin?

Anything that is specific to a vehicle or group of vehicles.  For example, NavG ships with a 'navest' plugin that plots
vehicle positions as reported by the DSL `navest` program.  However, `navest` is not a universal source of position
information even though it is common to all DSL vehicles.  Therefore, `navest` plotting is provided as a *plugin* for
NavG.

A more specific case of functionality provided by a plugin is the `sentrytracks` plugin which displays and updates 
mission trackline progress for the NDSF Sentry AUV.  Sentry is the *only* vehicle that currently uses generated, planned
tracklines and this behavior is not shared by any other DSL vehicle.

Going the other way:  The Range-Bearing tool is *built-into* NavG and is not a plugin.  Providing quick access to range
and bearing values on the map is *generic* to any vehicle use case and a good candidate for becoming a core feature of
NavG.  Similarly, the `gradient` plugin is generic enough to be considered a candidate for *built-in* refactoring at a
later date.

## Plugin Requirements and Limitations

- Must inherit from the navg::CorePluginInterface class defined in CorePluginInterface.h
- Must define metadata in a separate json file
- Can only provide a single widget to display in NavG's dockareas.  Nothing, however, prevents a plugin from spawning
  additional dialogs for additional information, such as settings or log displays.  
- *MUST* be built as a shared library at the current time.  This is not a hard limitation and should be able to be
  be relaxed with some effort in how NavG searches for and loads plugins at startup.
- The compiled library *MUST* have the prefix `libnavg`.  This means your CMake library target should start with `navg`
  Again, this is not a hard limitation but is tied to how NavG searches for and loads plugins at startup.

## Plugin Interactions

Plugins can interact with with NavG's main map view (as well as the underlying map scene) and with other plugins.
The plugin's `setMapView` method is called when it is loaded by NavG and passed a QPointer containing the NavGMapView
instance used by NavG.  This view can also be used to access the underlying NavGMapScene instance containing all objects
displayed by NavG.

Plugins will also have their `connectPlugin` method called whenever a new plugin is loaded.  As the docstrings state,
you **MUST** use signals and slots to share information between plugins.  **DO NOT** keep copies of the plugin pointer
locally -- you have no guarantees of the plugin's lifetime and keeping a local copy is sure to cause segfaults.

## Plugin Development

An example plugin project structure:

    example_plugin/
    ├── cmake
    │   └── navg_example_plugin-config.cmake.in  # 
    ├── CMakeLists.txt
    ├── include
    │   └── example_plugin
    │       ├── Example.h                        # Example plugin public header
    │       └── Example.json                     # Example plugin metadata
    ├── src
    │   ├── Example.cpp                          # Example plugin implementation
    │   ├── Example.ui                           # A UI for the plugin
    │   └── CMakeLists.txt
    └── tests                                    # Unit tests for the plugin
        └── test_example_plugin.cpp
        
Some important, non-code files:

- `navg_example_plugin-config.cmake.in`: A CMake-configured file that will let other projects that may need to build
  against your project know where to find your plugin's source code within CMake.
- `Example.json`:  Plugin metadata used by Qt's plugin machinery.

## Plugin Example

The `sentry_abort` plugin is heavily documented and serves as a simple example plugin that can be used to help guide
future plugin development.

## Displaying Latitude and Longitude in Plugins

If you want to display latitude and lognitude in your plugin, please subscribe to 
the `latLonFormatChanged(dslmap::LatLonFormat format)` signal on the map view in the
`setMapView` method of your plugin to be notified when the user changes how lat/lon
numbers should be displayed (decimal degrees, decimal minutes, etc.).

Use the functions defined in `dslmap/Util.h` to convert latitude and lognitude into
properly formatted strings.  See the `vehicle_usbl` plugin for as an example.