#! /usr/bin/env python2.7

# NOTE(LEL): I wasn't sure if this utility belonged in the repository or not,
#  but I committed it for now so that the reviewer could use it to generate
#  test data.

import argparse
import numpy as np
import os

def add_spoofed_sdq31(indir):
    outdir = indir.replace('sdyne', 'sdyne_sdq31')
    print "new directory!", outdir
    try:
        os.makedirs(outdir)
    except OSError:
        pass

    for infilename in os.listdir(indir):
        if '.DAT' not in infilename:
            continue
        outfilename = '{}/{}'.format(outdir, infilename)
        print "Creating output file", outfilename
        outfile = open(outfilename, 'w')
        for line in open('{}/{}'.format(indir, infilename)):
            outfile.write(line)
            if line[24] == '<' and '|SDQ 0:' in line:
                prefix = line.split('|')[0]
                # Generate spoofed random information, centered around
                # reasonable values.
                oxygen_concentration = 69 + np.random.randn()
                raw_obs = 0.02 + 0.01*np.random.randn()
                raw_orp = 3.35 + 0.01*np.random.randn()
                ctd_temperature = 3 + np.random.randn()
                ctd_salinity = 34 + np.random.randn()
                paro_depth = 2000 + 100*np.random.randn()
                newline = ('{}|SDQ 31:{:.02f} {:.04f} {:.04f} {:.02f} {:.04f} {:.02f}\n'.format(
                    prefix, oxygen_concentration, raw_obs, raw_orp,
                    ctd_temperature, ctd_salinity, paro_depth))
                outfile.write(newline)
        outfile.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("indir", help="Input sonardyne directory")
    args = parser.parse_args()
    add_spoofed_sdq31(args.indir)
