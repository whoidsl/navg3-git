/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef NAVG_SENTRYTOPSIDEREPLAY_H
#define NAVG_SENTRYTOPSIDEREPLAY_H

#include <QDateTime>
#include <QDialog>
#include <QTimer>
#include <QtNetwork/QHostAddress>
#include <QtNetwork/QUdpSocket>

#include <memory>

class QStatusBar;
class QTimerEvent;

namespace Ui {
class SentryTopsideReplay;
}

class SentryTopsideReplay final : public QDialog
{
  Q_OBJECT

public:
  enum SOURCE
  {
    NAVEST = 0,
    SMS = 1,
    UMODEM = 2,
  };

  struct LogEntry
  {
    QDateTime timestamp;
    SOURCE source;
    QString data;
  };

  Q_ENUM(SOURCE)

  explicit SentryTopsideReplay(QWidget* parent = nullptr, Qt::WindowFlags f = 0);
  ~SentryTopsideReplay() override;

  bool loadNavestRecords(QString path);
  bool loadSmsRecords(QString path);
  bool loadUModemRecords(QString path);

  void sendNextEntry();

  void startStopReplay(bool toggle);

  void updateEntries();
private:
  std::unique_ptr<Ui::SentryTopsideReplay> ui;
  QVector<LogEntry> m_entries;
  QTimer m_timer;
  QStatusBar* m_status_bar;
  QUdpSocket* m_socket;
  QHash<SOURCE, QHostAddress> m_remote_address;
  QHash<SOURCE, int> m_remote_port;
};

#endif // NAVG_SENTRYTOPSIDEREPLAY_H
