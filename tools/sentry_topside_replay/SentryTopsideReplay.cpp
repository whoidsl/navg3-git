/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 7/25/18.
//

#include "SentryTopsideReplay.h"
#include "ui_SentryTopsideReplay.h"

#include <QDir>
#include <QFileDialog>
#include <QPushButton>
#include <QStatusBar>
#include <QTextStream>
#include <QTimerEvent>
#include <QToolButton>
#include <QSettings>

SentryTopsideReplay::SentryTopsideReplay(QWidget* parent, Qt::WindowFlags f)
  : QDialog(parent, f)
  , ui(std::make_unique<Ui::SentryTopsideReplay>())
  , m_status_bar(new QStatusBar)
  , m_socket(new QUdpSocket)

{
  QSettings settings;

  ui->setupUi(this);
  m_status_bar->setParent(this);
  m_socket->setParent(this);

  ui->remote_address_navest->setText(settings.value("remote_address_navest", "127.0.0.1").toString());
  m_remote_address[SOURCE::NAVEST] = QHostAddress{ui->remote_address_navest->text()};

  ui->remote_address_sms->setText(settings.value("remote_address_sms", "127.0.0.1").toString());
  m_remote_address[SOURCE::SMS] = QHostAddress{ui->remote_address_sms->text()};

  ui->remote_address_umodem->setText(settings.value("remote_address_umodem", "127.0.0.1").toString());
  m_remote_address[SOURCE::UMODEM] = QHostAddress{ui->remote_address_umodem->text()};

  ui->remote_port_navest->setValue(settings.value("remote_port_navest", 0).toUInt());
  m_remote_port[SOURCE::NAVEST] = ui->remote_port_navest->value();

  ui->remote_port_sms->setValue(settings.value("remote_port_sms", 0).toUInt());
  m_remote_port[SOURCE::SMS] = ui->remote_port_sms->value();

  ui->remote_port_umodem->setValue(settings.value("remote_port_umodem", 0).toUInt());
  m_remote_port[SOURCE::UMODEM] = ui->remote_port_umodem->value();

  ui->statusBarLayout->addWidget(m_status_bar);

  connect(ui->directory_button_navest, &QToolButton::clicked, [this]() {
    QSettings settings;
    const auto path =
      QFileDialog::getExistingDirectory(this, "Navest Log Directory", settings.value("directory_navest").toString(), QFileDialog::ShowDirsOnly);
    if (path.isEmpty()) {
      return;
    }
    if(loadNavestRecords(path))
    {
      settings.setValue("directory_navest", path);
    }
  });

  connect(ui->directory_button_sms, &QToolButton::clicked, [this]() {
    QSettings settings;
    const auto path =
        QFileDialog::getExistingDirectory(this, "SMS Log Directory", settings.value("directory_sms").toString(), QFileDialog::ShowDirsOnly);
    if (path.isEmpty()) {
      return;
    }
    if(loadSmsRecords(path))
    {
      settings.setValue("directory_sms", path);
    }

  });

  connect(ui->directory_button_umodem, &QToolButton::clicked, [this]() {
    QSettings settings;
    const auto path =
        QFileDialog::getExistingDirectory(this, "UMODEM Log Directory", settings.value("directory_umodem").toString(), QFileDialog::ShowDirsOnly);
    if (path.isEmpty()) {
      return;
    }
    if(loadUModemRecords(path))
    {
      settings.setValue("directory_umodem", path);
    }
  });

  connect(ui->progress_bar, &QScrollBar::valueChanged, [this](int pos) {
    ui->label_current_time->setText(
      m_entries.at(pos).timestamp.toString("yyyy/MM/dd\nHH:mm:ss.z"));
  });

  m_timer.setSingleShot(true);
  connect(&m_timer, &QTimer::timeout, this, &SentryTopsideReplay::sendNextEntry);

  connect(ui->button_play, &QPushButton::toggled, this,
          &SentryTopsideReplay::startStopReplay);

  connect(ui->remote_port_navest, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), [this](const int& value) {
    m_remote_port[SOURCE::NAVEST] = value;
    QSettings settings;
    settings.setValue("remote_port_navest", value);
  });

  connect(ui->remote_port_sms, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), [this](const int& value) {
    m_remote_port[SOURCE::SMS] = value;
    QSettings settings;
    settings.setValue("remote_port_sms", value);
  });

  connect(ui->remote_port_umodem, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), [this](const int& value) {
    m_remote_port[SOURCE::UMODEM] = value;
    QSettings settings;
    settings.setValue("remote_port_umodem", value);
  });

  connect(ui->remote_address_navest, &QLineEdit::editingFinished, [this]() {
    const auto text = ui->remote_address_navest->text();
    const auto address = QHostAddress{ text };
    if (address.isNull()) {
      ui->remote_address_navest->setText(m_remote_address.value(SOURCE::NAVEST, {}).toString());
      m_status_bar->showMessage(
        QString("%1 is not a valid IP address").arg(text));
    }
    m_remote_address[SOURCE::NAVEST] = address;
    QSettings settings;
    settings.setValue("remote_port_navest", text);
  });

  connect(ui->remote_address_sms, &QLineEdit::editingFinished, [this]() {
    const auto text = ui->remote_address_sms->text();
    const auto address = QHostAddress{ text };
    if (address.isNull()) {
      ui->remote_address_navest->setText(m_remote_address.value(SOURCE::SMS, {}).toString());
      m_status_bar->showMessage(
          QString("%1 is not a valid IP address").arg(text));
    }
    m_remote_address[SOURCE::SMS] = address;
    QSettings settings;
    settings.setValue("remote_port_sms", text);
  });

  connect(ui->remote_address_umodem, &QLineEdit::editingFinished, [this]() {
    const auto text = ui->remote_address_umodem->text();
    const auto address = QHostAddress{ text };
    if (address.isNull()) {
      ui->remote_address_navest->setText(m_remote_address.value(SOURCE::UMODEM, {}).toString());
      m_status_bar->showMessage(
          QString("%1 is not a valid IP address").arg(text));
    }
    m_remote_address[SOURCE::UMODEM] = address;
    QSettings settings;
    settings.setValue("remote_port_umodem", text);
  });

  m_remote_address[SOURCE::NAVEST] = QHostAddress{ ui->remote_address_navest->text() };
  m_remote_address[SOURCE::SMS] = QHostAddress{ ui->remote_address_sms->text() };
  m_remote_address[SOURCE::UMODEM] = QHostAddress{ ui->remote_address_umodem->text() };

  m_status_bar->showMessage("Ready");
}

SentryTopsideReplay::~SentryTopsideReplay() = default;

void SentryTopsideReplay::updateEntries() {

  const auto size = m_entries.size();
  if (size == 0) {
    return;
  }

  m_status_bar->showMessage(QString("Sorting %1 entries").arg(m_entries.size()));
  std::sort(m_entries.begin(), m_entries.end(), [](LogEntry& a, LogEntry& b){return a.timestamp.msecsTo(b.timestamp) > 0;});
  ui->label_start_time->setText(
      m_entries.first().timestamp.toString("yyyy/MM/dd\nHH:mm:ss.z"));
  ui->label_end_time->setText(
      m_entries.last().timestamp.toString("yyyy/MM/dd\nHH:mm:ss.z"));
  ui->progress_bar->setMinimum(0);
  ui->progress_bar->setMaximum(size - 1);
  ui->progress_bar->setSliderPosition(0);
  m_status_bar->showMessage(QString("Done!"), 5000);
}

bool
SentryTopsideReplay::loadSmsRecords(QString path)
{
  m_timer.stop();

  // Remove existing navest entries.
  std::remove_if(m_entries.begin(), m_entries.end(), [](LogEntry& entry)
  {
    return entry.source == SOURCE::SMS;
  });

  auto dir = QDir{ path };

  if (!dir.exists(path)) {
    return false;
  }

  const auto entry_list =
      dir.entryInfoList({ "*.DAT" }, QDir::Files, QDir::Name);
  const auto total_num_files = entry_list.size();
  for (auto file_num = 0; file_num < total_num_files; ++file_num) {
    const auto& info = entry_list.at(file_num);
    QFile file(info.absoluteFilePath());
    if (!file.open(QFile::ReadOnly)) {
      continue;
    }

    m_status_bar->showMessage(QString("Reading: %1 (%2 of %3)")
                                  .arg(info.fileName())
                                  .arg(file_num + 1)
                                  .arg(total_num_files));
    QTextStream stream(&file);
    while (!stream.atEnd()) {
      const auto line = stream.readLine();
      if (line.length() < 25)
      {
        continue;
      }
      if (line.at(24) != '<')
      {
        continue;
      }
      const auto timestamp =
          QDateTime::fromString(line.left(22), "yyyy-MM-dd HH:mm:ss,z");

      const auto data = line.mid(25);

      m_entries.append({ timestamp, SOURCE::SMS, data });
    }

    file.close();
  }

  ui->directory_sms->setText(path);
  updateEntries();
  return true;
}

bool
SentryTopsideReplay::loadUModemRecords(QString path) {
  m_timer.stop();

  // Remove existing navest entries.
  std::remove_if(m_entries.begin(), m_entries.end(), [](LogEntry& entry)
  {
    return entry.source == SOURCE::UMODEM;
  });

  auto dir = QDir{ path };

  if (!dir.exists(path)) {
    return false;
  }

  const auto entry_list =
      dir.entryInfoList({ "*.ACM" }, QDir::Files, QDir::Name);
  const auto total_num_files = entry_list.size();
  for (auto file_num = 0; file_num < total_num_files; ++file_num) {
    const auto& info = entry_list.at(file_num);
    QFile file(info.absoluteFilePath());
    if (!file.open(QFile::ReadOnly)) {
      continue;
    }

    m_status_bar->showMessage(QString("Reading: %1 (%2 of %3)")
                                  .arg(info.fileName())
                                  .arg(file_num + 1)
                                  .arg(total_num_files));
    QTextStream stream(&file);
    while (!stream.atEnd()) {
      const auto line = stream.readLine();
      if (!line.startsWith("LOG"))
      {
        continue;
      }

      const auto timestamp =
          QDateTime::fromString(line.mid(4, 23), "yyyy/MM/dd HH:mm:ss.z");

      if (!timestamp.isValid()) {
        continue;
      }

      m_entries.append({ timestamp, SOURCE::UMODEM, line.mid(28) });

    }

    file.close();
  }

  ui->directory_umodem->setText(path);
  updateEntries();
  return true;

}


bool
SentryTopsideReplay::loadNavestRecords(QString path)
{

  m_timer.stop();

  // Remove existing navest entries.
  std::remove_if(m_entries.begin(), m_entries.end(), [](LogEntry& entry)
  {
    return entry.source == SOURCE::NAVEST;
  });

  auto dir = QDir{ path };

  if (!dir.exists(path)) {
    return false;
  }

  const auto entry_list =
    dir.entryInfoList({ "*.DAT" }, QDir::Files, QDir::Name);
  const auto total_num_files = entry_list.size();
  for (auto file_num = 0; file_num < total_num_files; ++file_num) {
    const auto& info = entry_list.at(file_num);
    QFile file(info.absoluteFilePath());
    if (!file.open(QFile::ReadOnly)) {
      continue;
    }

    m_status_bar->showMessage(QString("Reading: %1 (%2 of %3)")
                                .arg(info.fileName())
                                .arg(file_num + 1)
                                .arg(total_num_files));
    QTextStream stream(&file);
    while (!stream.atEnd()) {
      const auto line = stream.readLine();
      if (line.startsWith("VPR") || line.startsWith("VFR") ||
          line.startsWith("DDD") || line.startsWith("OCT") ||
          line.startsWith("NDS") || line.startsWith("TDR") ||
          line.startsWith("DEP")) {
        const auto timestamp =
            QDateTime::fromString(line.mid(4, 23), "yyyy/MM/dd HH:mm:ss.z");
        if (!timestamp.isValid()) {
          continue;
        }

        m_entries.append({timestamp, SOURCE::NAVEST, line});
      }
    }
    file.close();
  }

  ui->directory_navest->setText(path);
  updateEntries();
  return true;
}

void
SentryTopsideReplay::sendNextEntry()
{
  m_timer.stop();
  const auto next_pos = ui->progress_bar->sliderPosition() + 1;
  if (next_pos >= m_entries.size()) {
    return;
  }

  ui->progress_bar->setSliderPosition(next_pos);
  const auto& entry = m_entries.at(next_pos);

  ui->log->appendPlainText(entry.data);

  if (!m_remote_address.value(entry.source, {}).isNull()) {
    m_socket->writeDatagram(entry.data.toUtf8(), m_remote_address.value(entry.source),
                            m_remote_port.value(entry.source));
  }

  if (next_pos + 1 >= m_entries.size()) {
    return;
  }

  const auto dt = entry.timestamp.msecsTo(
                    m_entries.at(next_pos + 1).timestamp) /
                  ui->rate->value();
  if (dt == 0) {
    sendNextEntry();
    return;
  }

  m_timer.start(dt);
}

void
SentryTopsideReplay::startStopReplay(bool toggle)
{

  if (!toggle) {
    m_timer.stop();
    return;
  }

  if (m_entries.empty())
  {
    return;
  }

  const auto pos = ui->progress_bar->sliderPosition();
  if (pos >= ui->progress_bar->maximum()) {
    ui->button_play->setChecked(false);
    return;
  }

  const auto dt =
    m_entries.at(pos).timestamp.msecsTo(m_entries.at(pos + 1).timestamp) /
    ui->rate->value();
  if (dt == 0) {
    sendNextEntry();
    return;
  }
  m_timer.start(dt);
}
