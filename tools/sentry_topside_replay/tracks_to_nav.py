#
# Copyright 2019 Woods Hole Oceanographic Institution
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
import sys
from datetime import timedelta
from pyproj import Proj, transform

def xy_to_latlon(lat_o, lon_o, x, y):
    alvinxy_def = "+proj=tmerc +datum=WGS84 +units=m +lon_0={} +lat_0={}".format(lon_o, lat_o)
    latlon_def = "+proj=latlong +datum=WGS84 +ellps=WGS84 +towgs84=0,0,0 +no_defs"
    inProj = Proj(alvinxy_def)
    outProj = Proj(latlon_def)
    return transform(inProj, outProj, x, y)

if __name__=="__main__":

    if len(sys.argv) < 2:
        print("Not enough arguments! Please define a tracks file!")
    else:
        tfs = []
        for arg in sys.argv:
            if ".tracks" in arg:
                tfs.append(arg)
    mins = 0
    for mins, tf in enumerate(tfs):
        fr= open(tf,"r")
        fw= open(tf.replace(".tracks","_nav.DAT"),"w+")

        for secs, line in enumerate(fr):
            if "#LATITUDE_ORIGIN" in line:
                ls = line.split("=")
                lat_origin = float(ls[1])
            elif "#LONGITUDE_ORIGIN" in line:
                ls = line.split("=")
                lon_origin = float(ls[1])
                print("origin lat: ", lat_origin, " lon: ", lon_origin)
            elif "#START" in line:
                ls = line.split(" ")
                x_start = ls[1]
                y_start = ls[2]
                print("start x: ", x_start, " y: ", y_start)
            elif "TL 3" in line:
                ls = line.split(" ")
                x = float(ls[2]) + float(x_start)
                y = float(ls[3]) + float(y_start)
                print("x: ", x, " y: ", y)
                lon, lat = xy_to_latlon(lat_origin, lon_origin, x, y)
                # print("lat: ", lat, " lon: ", lon)
                t = timedelta(minutes=mins, seconds=secs)
                vpr_line = "VPR 2018/08/31 0" + str(t) + ".001 Sentry 0 " + "{:.20f}".format(lon) + " " + "{:.20f}".format(lat) + " 0.00 0.00 -0.00 0.00 \r\n\r\n"
                vfr_line = "VFR 2018/08/31 0" + str(t) + ".501 2 2 SOLN_GPS0 " + "{:.20f}".format(lon) + " " + "{:.20f}".format(lat) + " 0.000 5.461 100 0.00 0.00 \r\n\r\n"
                print(vfr_line)
                fw.write(vpr_line)
                fw.write(vfr_line)
        fr.close()
        fw.close()