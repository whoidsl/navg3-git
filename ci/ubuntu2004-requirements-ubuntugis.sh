#!/bin/bash

add-apt-repository ppa:ubuntugis/ppa
#extra update and upgrade after new ppa installation
apt-get update
apt-get upgrade -y

apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    build-essential \
    qtbase5-dev \
    libgdal-dev \
    gdal-bin \
    libgeos-dev \
    cmake \
    clang-format \
    mercurial
