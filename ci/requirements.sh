#!/bin/bash
apt-get update && apt-get install -y \
    build-essential \
    qtbase5-dev \
    libgdal-dev \
    gdal-bin \
    libproj-dev \
    libgeos-dev \
    cmake \
    clang-format \
    mercurial
