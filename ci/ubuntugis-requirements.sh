#!/bin/bash

apt-get update
apt-get install -y software-properties-common
apt-add-repository -y ppa:ubuntugis/ppa

apt-get update && apt-get install -y \
	build-essential \
	qtbase5-dev \
	libgdal-dev \
	gdal-bin \
	libproj-dev \
	cmake \
	clang-3.8 \
	xvfb
