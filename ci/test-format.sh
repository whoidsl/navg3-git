#!/usr/bin/env sh
echo "-- Running clang-format targets\n"

make dslmap-clang-format
make navg-clang-format

echo "\n-- Checking for changes in files after formatting"

hg st -m

NUM_MODIFIED=`hg st -m | wc -l`
if [ $NUM_MODIFIED -ne 0 ]
then
echo "\n**$NUM_MODIFIED** files were modified after formatting."
echo "-- Please run 'make dslmap-clang-format' or 'make navg-clang-format' to fix formatting"
else
echo "-- clang-format check passed."
fi
exit $NUM_MODIFIED
