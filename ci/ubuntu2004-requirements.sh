#!/bin/bash

apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    build-essential \
    qtbase5-dev \
    libgdal-dev \
    gdal-bin \
    libproj-dev \
    libgeos-dev \
    proj-data \
    cmake \
    clang-format \
    mercurial
   
