## NOTES

The requirements scripts here contain the required packages for operating systems Ubuntu 18.04 and Ubuntu 20.04.

For 18.04 the corresponding scripts is "ubuntu1804-requirements.sh"
and 20.04 would be "ubuntu2004-requirements.sh"

The usage of external ubuntugis ppas is strongly not recommended as compatibility issues may arise. Please use your operating systems mainline repos.
